[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol de Aguacate"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Arbol de Aguacate": 0
        }
    }

    , {
        "$addFields": {
            "campo_adultos": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$or": [
                                    { "$ne": [{ "$toDouble": "$Acaro" }, 0] }
                                    , { "$ne": [{ "$toDouble": "$Trips" }, 0] }
                                    , { "$ne": [{ "$toDouble": "$Marceño" }, 0] }
                                ]
                            }
                            , "then": "si_campo_adultos"
                        }

                    ],
                    "default": "no_campo_adultos"
                }
            }

        }
    },

    {
        "$match": {
            "campo_adultos": "si_campo_adultos"
        }
    }


    , {
        "$addFields": {
            "array_adultos": [
                {
                    "adultos_nombre": "Acaro"
                    , "valor": { "$ifNull": [{ "$toDouble": "$Acaro" }, 0] }
                },
                {
                    "adultos_nombre": "Trips"
                    , "valor": { "$ifNull": [{ "$toDouble": "$Trips" }, 0] }
                },
                {
                    "adultos_nombre": "Marceño"
                    , "valor": { "$ifNull": [{ "$toDouble": "$Marceño" }, 0] }
                }

            ]
        }
    }

    , {
        "$addFields": {
            "array_adultos": {
                "$filter": {
                    "input": "$array_adultos",
                    "as": "item",
                    "cond": { "$ne": ["$$item.valor", 0] }
                }
            }
        }
    }

    , {
        "$unwind": {
            "path": "$array_adultos",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$group": {
            "_id": {
                "planta": "$arbol"

                , "adultos_nombre": "$array_adultos.adultos_nombre"
            }
            , "numAdultos_x_nombre_x_planta": { "$sum": "$array_adultos.valor" }
            , "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "numAdultos_x_nombre_x_planta": "$numAdultos_x_nombre_x_planta"
                    }
                ]
            }
        }
    }





    , {
        "$project": {
            "finca": "$finca"
            , "bloque": "$bloque"
            , "lote": "$lote"
            , "linea": "$linea"
            , "arbol": "$arbol"

            , "adultos_nombre": "$array_adultos.adultos_nombre"
            , "adultos_cantidad": "$array_adultos.valor"

            , "numAdultos_x_nombre_x_planta": "$numAdultos_x_nombre_x_planta"

            , "fecha_rgDate": "$rgDate"

        }
    }


]
