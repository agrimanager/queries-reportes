db.form_monitoreodeplagasyenfermedades.aggregate(
    [

        //NOTA: formulario con 42 variables



        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------



        //=======CARTOGRAFIA
        {
            "$addFields": {
                "variable_cartografia": "$Arbol de Aguacate" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol de Aguacate": 0 //🚩editar : 0
            }
        }


        //=======LOGICA DE REPORTE

        //pct_incidencia = arboles_afectados / arboles_monitoreados

        //---arboles_monitoreados = plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }

        //---arboles_afectados (12 afectaciones y 4 precencias)
        , {
            "$addFields": {
                "array_afectaciones": [
                    //array objs
                    {
                        "afectacion_sanidad": "Afectacion Monalonion"
                        , "afectacion_nombre": "Monalonion"
                        , "afectacion_valor": "$Afectacion Monalonion"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Thrips"
                        , "afectacion_nombre": "Thrips"
                        , "afectacion_valor": "$Afectacion Thrips"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Hormiga Arriera"
                        , "afectacion_nombre": "Hormiga Arriera"
                        , "afectacion_valor": "$Afectacion Hormiga Arriera"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Chizas"
                        , "afectacion_nombre": "Chizas"
                        , "afectacion_valor": "$Afectacion Chizas"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Mosca del Ovario"
                        , "afectacion_nombre": "Mosca del Ovario"
                        , "afectacion_valor": "$Afectacion Mosca del Ovario"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Acaro"
                        , "afectacion_nombre": "Acaro"
                        , "afectacion_valor": "$Afectacion Acaro"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Insectos Defoliadores"
                        , "afectacion_nombre": "Insectos Defoliadores"
                        , "afectacion_valor": "$Afectacion Insectos Defoliadores"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Marceno"
                        , "afectacion_nombre": "Marceno"
                        , "afectacion_valor": "$Afectacion Marceno"
                    },
                    {
                        "afectacion_sanidad": "Afectacion de Phytophtora"
                        , "afectacion_nombre": "de Phytophtora"
                        , "afectacion_valor": "$Afectacion de Phytophtora"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Lenticelosis"
                        , "afectacion_nombre": "Lenticelosis"
                        , "afectacion_valor": "$Afectacion Lenticelosis"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Verticillium"
                        , "afectacion_nombre": "Verticillium"
                        , "afectacion_valor": "$Afectacion Verticillium"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Rona"
                        , "afectacion_nombre": "Rona"
                        , "afectacion_valor": "$Afectacion Rona"
                    }

                    //----Precencia
                    // ,
                    // {
                    //     "afectacion_sanidad": "Afectacion XXXXXX"
                    //     , "afectacion_nombre": "XXXXX"
                    //     , "afectacion_valor": "$Afectacion XXXXXX"
                    // },
                    // {
                    //     "afectacion_sanidad": "Afectacion XXXXXX"
                    //     , "afectacion_nombre": "XXXXX"
                    //     , "afectacion_valor": "$Afectacion XXXXXX"
                    // },
                    // {
                    //     "afectacion_sanidad": "Afectacion XXXXXX"
                    //     , "afectacion_nombre": "XXXXX"
                    //     , "afectacion_valor": "$Afectacion XXXXXX"
                    // },
                    // {
                    //     "afectacion_sanidad": "Afectacion XXXXXX"
                    //     , "afectacion_nombre": "XXXXX"
                    //     , "afectacion_valor": "$Afectacion XXXXXX"
                    // }

                ]
            }
        }

        //filtrar vacios
        , {
            "$addFields": {
                "array_afectaciones": {
                    "$filter": {
                        "input": "$array_afectaciones",
                        "as": "item",
                        "cond": { "$ne": ["$$item.afectacion_valor", []] }
                    }
                }
            }
        }


        //---old
        // //match
        // , {
        //     "$match": {
        //         "array_afectaciones": { "$ne": [] }
        //     }
        // }

        // //match
        // , {
        //     "$match": {
        //         "array_afectaciones.afectacion_valor": { "$ne": "" }
        //     }
        // }


        // //unwind
        // , {
        //     "$unwind": {
        //         "path": "$array_afectaciones",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // }


        //---new
        //unwind
        , {
            "$unwind": {
                "path": "$array_afectaciones",
                "preserveNullAndEmptyArrays": false
            }
        }
        //unwind
        , {
            "$unwind": {
                "path": "$array_afectaciones.afectacion_valor",
                "preserveNullAndEmptyArrays": false
            }
        }

        //match
        , {
            "$match": {
                "array_afectaciones.afectacion_valor": { "$ne": "" }
            }
        }


        //plantas_dif_afectadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"

                    , "afectacion": "$array_afectaciones.afectacion_nombre"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"

                    , "afectacion": "$_id.afectacion"
                },
                // "plantas_dif_afectadas_x_lote": { "$sum": 1 },
                "plantas_dif_afectadas_x_lote_x_afectacion": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            // "plantas_dif_afectadas_x_lote": "$plantas_dif_afectadas_x_lote"
                            "plantas_dif_afectadas_x_lote_x_afectacion": "$plantas_dif_afectadas_x_lote_x_afectacion"
                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "porc_incidencia": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$floor": {
                                        "$multiply": [


                                            {
                                                "$multiply": [
                                                    {
                                                        "$divide": [
                                                            // "$plantas_dif_afectadas_x_lote",
                                                            "$plantas_dif_afectadas_x_lote_x_afectacion",
                                                            "$plantas_dif_censadas_x_lote"
                                                        ]
                                                    },
                                                    100
                                                ]
                                            },


                                            100
                                        ]
                                    }
                                },
                                100
                            ]
                        }
                    }
                }
            }
        }



        , {
            "$project": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"
                , "linea": "$linea"
                , "arbol": "$arbol"

                , "afectacion_sanidad": "$array_afectaciones.afectacion_sanidad"
                , "afectacion_nombre": "$array_afectaciones.afectacion_nombre"
                , "afectacion_valor": "$array_afectaciones.afectacion_valor"

                // , "plantas_dif_afectadas_x_lote": "$plantas_dif_afectadas_x_lote"
                , "plantas_dif_afectadas_x_lote_x_afectacion": "$plantas_dif_afectadas_x_lote_x_afectacion"
                , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"

                , "porc_incidencia_lote": "$porc_incidencia"

                , "fecha_rgDate": "$rgDate"

            }
        }




    ]
)
