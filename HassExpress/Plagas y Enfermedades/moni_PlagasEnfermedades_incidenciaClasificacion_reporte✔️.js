[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol de Aguacate"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Arbol de Aguacate": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "planta": "$arbol"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }


    ,{
        "$match":{
            "Raices":{"$ne":""}
        }
    }


    , {
        "$addFields": {
            "array_objs": [

                {
                    "clasificacion_tipo": "Arbol Encunetado"
                    , "clasificacion_nombre": "Arbol Encunetado"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Arbol Encunetado", "Si"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Arbol Necesita Zanjas"
                    , "clasificacion_nombre": "Arbol Necesita Zanjas"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Arbol Necesita Zanjas", "Si"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Plateo Enmalezado"
                    , "clasificacion_nombre": "Plateo Enmalezado"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Plateo Enmalezado", "Si"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },


                {
                    "clasificacion_tipo": "Clasificacion de Arbol"
                    , "clasificacion_nombre": "Arbol Sano"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Clasificacion de Arbol", "Arbol Sano"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Clasificacion de Arbol"
                    , "clasificacion_nombre": "Arbol Amarillo"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Clasificacion de Arbol", "Arbol Amarillo"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Clasificacion de Arbol"
                    , "clasificacion_nombre": "Arbol Desfoliado"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Clasificacion de Arbol", "Arbol Desfoliado"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Clasificacion de Arbol"
                    , "clasificacion_nombre": "Arbol Hojas Tristes"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Clasificacion de Arbol", "Arbol Hojas Tristes"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Clasificacion de Arbol"
                    , "clasificacion_nombre": "Arbol Muerto"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": { "$eq": ["$Clasificacion de Arbol", "Arbol Muerto"] },
                            "then": 1,
                            "else": 0
                        }
                    }
                },

                {
                    "clasificacion_tipo": "Raices"
                    , "clasificacion_nombre": "Baja"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": {
                                "$eq": [
                                    {
                                        "$filter": {
                                            "input": "$Raices",
                                            "as": "item",
                                            "cond": { "$eq": ["$$item", "Baja"] }
                                        }
                                    }
                                    , ["Baja"]
                                ]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Raices"
                    , "clasificacion_nombre": "Media "
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": {
                                "$eq": [
                                    {
                                        "$filter": {
                                            "input": "$Raices",
                                            "as": "item",
                                            "cond": { "$eq": ["$$item", "Media "] }
                                        }
                                    }
                                    , ["Media "]
                                ]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "clasificacion_tipo": "Raices"
                    , "clasificacion_nombre": "Alta"
                    , "clasificacion_valor": {
                        "$cond": {
                            "if": {
                                "$eq": [
                                    {
                                        "$filter": {
                                            "input": "$Raices",
                                            "as": "item",
                                            "cond": { "$eq": ["$$item", "Alta"] }
                                        }
                                    }
                                    , ["Alta"]
                                ]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                }

            ]
        }
    }

    , {
        "$addFields": {
            "array_objs": {
                "$filter": {
                    "input": "$array_objs",
                    "as": "item",
                    "cond": { "$ne": ["$$item.clasificacion_valor", 0] }
                }
            }
        }
    }

    , {
        "$match": {
            "array_objs": { "$ne": [] }
        }
    }


    , {
        "$unwind": {
            "path": "$array_objs",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "planta": "$arbol"

                , "clasificacion_tipo": "$array_objs.clasificacion_tipo"
                , "clasificacion_nombre": "$array_objs.clasificacion_nombre"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"

                , "clasificacion_tipo": "$_id.clasificacion_tipo"
                , "clasificacion_nombre": "$_id.clasificacion_nombre"
            },

            "plantas_dif_censadas_x_lote_x_clasificacion": { "$sum": 1 },

            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote_x_clasificacion": "$plantas_dif_censadas_x_lote_x_clasificacion"
                    }
                ]
            }
        }
    }





    , {
        "$addFields": {
            "porc_incidencia": {
                "$cond": {
                    "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$floor": {
                                    "$multiply": [


                                        {
                                            "$multiply": [
                                                {
                                                    "$divide": [
                                                        "$plantas_dif_censadas_x_lote_x_clasificacion",
                                                        "$plantas_dif_censadas_x_lote"
                                                    ]
                                                },
                                                100
                                            ]
                                        },


                                        100
                                    ]
                                }
                            },
                            100
                        ]
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "finca": "$finca"
            , "bloque": "$bloque"
            , "lote": "$lote"
            , "linea": "$linea"
            , "arbol": "$arbol"

            , "clasificacion_tipo": "$array_objs.clasificacion_tipo"
            , "clasificacion_nombre": "$array_objs.clasificacion_nombre"
            , "clasificacion_valor": "$array_objs.clasificacion_valor"

            , "plantas_dif_censadas_x_lote_x_clasificacion": "$plantas_dif_censadas_x_lote_x_clasificacion"
            , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"

            , "pct_incidencia_lote_x_clasificacion": "$porc_incidencia"

            , "fecha_rgDate": "$rgDate"

        }
    }



]
