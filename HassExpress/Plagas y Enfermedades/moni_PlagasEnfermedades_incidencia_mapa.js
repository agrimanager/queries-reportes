
//---Reporte de mapa con censo por arbol y pintar mapa por lote

//CENSO = ARBOL
//GEOMETRIA = LOTE

db.form_monitoreodeplagasyenfermedades.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        // //FILTRO FINCA
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //                 {
        //                     "$eq": ["$Point.farm", "605126833abafc311c16a3f3"]
        //                 }
        //             ]
        //         }
        //     }
        // },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //================================
        //-----QUERY REPORTE
        {
            "$addFields": {
                "variable_cartografia": "$Arbol de Aguacate"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol de Aguacate": 0
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "array_afectaciones": [

                    {
                        "afectacion_sanidad": "Afectacion Monalonion"
                        , "afectacion_nombre": "Monalonion"
                        , "afectacion_valor": "$Afectacion Monalonion"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Thrips"
                        , "afectacion_nombre": "Thrips"
                        , "afectacion_valor": "$Afectacion Thrips"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Hormiga Arriera"
                        , "afectacion_nombre": "Hormiga Arriera"
                        , "afectacion_valor": "$Afectacion Hormiga Arriera"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Chizas"
                        , "afectacion_nombre": "Chizas"
                        , "afectacion_valor": "$Afectacion Chizas"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Mosca del Ovario"
                        , "afectacion_nombre": "Mosca del Ovario"
                        , "afectacion_valor": "$Afectacion Mosca del Ovario"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Acaro"
                        , "afectacion_nombre": "Acaro"
                        , "afectacion_valor": "$Afectacion Acaro"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Insectos Defoliadores"
                        , "afectacion_nombre": "Insectos Defoliadores"
                        , "afectacion_valor": "$Afectacion Insectos Defoliadores"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Marceno"
                        , "afectacion_nombre": "Marceno"
                        , "afectacion_valor": "$Afectacion Marceno"
                    },
                    {
                        "afectacion_sanidad": "Afectacion de Phytophtora"
                        , "afectacion_nombre": "de Phytophtora"
                        , "afectacion_valor": "$Afectacion de Phytophtora"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Lenticelosis"
                        , "afectacion_nombre": "Lenticelosis"
                        , "afectacion_valor": "$Afectacion Lenticelosis"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Verticillium"
                        , "afectacion_nombre": "Verticillium"
                        , "afectacion_valor": "$Afectacion Verticillium"
                    },
                    {
                        "afectacion_sanidad": "Afectacion Rona"
                        , "afectacion_nombre": "Rona"
                        , "afectacion_valor": "$Afectacion Rona"
                    }



                ]
            }
        }

        , {
            "$addFields": {
                "array_afectaciones": {
                    "$filter": {
                        "input": "$array_afectaciones",
                        "as": "item",
                        "cond": { "$ne": ["$$item.afectacion_valor", []] }
                    }
                }
            }
        }

        , {
            "$unwind": {
                "path": "$array_afectaciones",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$unwind": {
                "path": "$array_afectaciones.afectacion_valor",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$match": {
                "array_afectaciones.afectacion_valor": { "$ne": "" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"

                    , "afectacion": "$array_afectaciones.afectacion_nombre"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"

                    , "afectacion": "$_id.afectacion"
                },
                "plantas_dif_afectadas_x_lote_x_afectacion": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_afectadas_x_lote_x_afectacion": "$plantas_dif_afectadas_x_lote_x_afectacion"
                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "porc_incidencia": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$floor": {
                                        "$multiply": [


                                            {
                                                "$multiply": [
                                                    {
                                                        "$divide": [
                                                            "$plantas_dif_afectadas_x_lote_x_afectacion",
                                                            "$plantas_dif_censadas_x_lote"
                                                        ]
                                                    },
                                                    100
                                                ]
                                            },


                                            100
                                        ]
                                    }
                                },
                                100
                            ]
                        }
                    }
                }
            }
        }



        , {
            "$project": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"
                , "linea": "$linea"
                , "arbol": "$arbol"

                , "afectacion_sanidad": "$array_afectaciones.afectacion_sanidad"
                , "afectacion_nombre": "$array_afectaciones.afectacion_nombre"
                , "afectacion_valor": "$array_afectaciones.afectacion_valor"

                , "plantas_dif_afectadas_x_lote_x_afectacion": "$plantas_dif_afectadas_x_lote_x_afectacion"
                , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"

                , "porc_incidencia_lote": "$porc_incidencia"

                , "fecha_rgDate": "$rgDate"

                //mapa
                , "cartography_id": "$cartography_id"
                , "idform": "$idform"

            }
        }


        //================================


        //================================
        //-----MAPA

        //--indicador color
        , {
            "$addFields": {
                "indicador": "$porc_incidencia_lote"//🚩EDITAR🚩
            }
        }
        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
            }
        }



        //--agrupacion lote x elemento_agrupacion
        , {
            "$addFields": {
                "elemento_agrupacion": "$afectacion_nombre"//🚩EDITAR🚩
            }
        }
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                    , "arbol": "$arbol"

                    , "idform": "$idform"
                    , "elemento_agrupacion": "$elemento_agrupacion"
                }
                , "indicador": { "$min": "$indicador" }
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                    // ,"arbol": "$arbol"

                    , "idform": "$_id.idform"
                    , "elemento_agrupacion": "$_id.elemento_agrupacion"
                }
                , "indicador": { "$min": "$indicador" }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "indicador": "$indicador"

                            , "cartography_id": {
                                "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.cartography_id", 0] }, 0]
                            }
                        }
                    ]
                }
            }
        }



        //NOTA!!!!!!!!!
        //la geometria del lote rompia la agrupacion del lote
        //....===>>>entonces hacer lookup para sacar geometria
        //------MAPA_VARIABLE_GEOMETRY
        , {
            "$lookup": {
                "from": "cartography",
                "localField": "lote",
                "foreignField": "properties.name",
                "as": "info_lote"
            }
        }
        , { "$unwind": "$info_lote" }
        , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
        , {
            "$project": {
                "info_lote": 0
            }
        }
        //----------------


        //--leyenda
        // %Incidencia lote: #ffffff,[0% - 0.5%]: #008000,(0.5% - 1%]: #ffff00,(> 1%): #ff0000
        // %Incidencia lote: #ffffff,: #,: #,: #


        // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lte": ["$indicador", 0.5] }
                                    ]
                                }
                                , "then": "#008000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 0.5] }
                                        , { "$lte": ["$indicador", 1] }
                                    ]
                                }
                                , "then": "#ffff00"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 1] }
                                    ]
                                }
                                , "then": "#ff0000"
                            }

                        ],
                        "default": "#000000"
                    }
                }
                , "rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lte": ["$indicador", 0.5] }
                                    ]
                                }
                                , "then": "A-[0% - 0.5%]"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 0.5] }
                                        , { "$lte": ["$indicador", 1] }
                                    ]
                                }
                                , "then": "B-(0.5% - 1%]"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 1] }
                                    ]
                                }
                                , "then": "C-(> 1%)"
                            }

                        ],
                        "default": "otro"
                    }
                }



            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {
                    "Finca": "$finca",
                    "Bloque": "$bloque",
                    "Lote": "$lote",

                    "Afectacion": "$elemento_agrupacion",

                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color"

                    // , "Enfermedad": "$elemento_agrupacion" //🚩EDITAR🚩
                    , "% Incidencia": { "$concat": [{ "$toString": "$indicador" }, " %"] }//🚩EDITAR🚩
                }

            }
        }



    ]
)
