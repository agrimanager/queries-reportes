db.getCollection("suppliesTimeline_2021-09-15_old").aggregate(

    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        //--excluir variables
                        "_id": null
                        , "inAssociativeId": null
                        // , "supervisor": null
                        , "rgDate": null
                        , "dueDate": null
                        , "invoiceDate": null
                        , "invoiceNumber": null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": {
                        "$slice": [
                            "$data",
                            { "$subtract": [{ "$size": "$data" }, 1] }
                        ]
                    },
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": ["$$value", "$$this"] }
                }
            }
        }
    }
)
