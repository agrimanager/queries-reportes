

//NOTA: para empresas con 1 sola finca


//========PROCEDIMIENTO

//---1) Obtener fincas
//--------------------
var fincas = db.farms.aggregate(
    {
        $addFields: {
            "_id_str_farm": { "$toString": "$_id" }
        }
    }

    , {
        $project: {
            _id: 0,
            _id_str_farm: 1
        }
    }

);

//fincas


//---2) Obtener cartografia a borrar (sin finca)
//----------------------------------------------

var cartografia_sin_finca = null;

fincas.forEach(i => {
    cartografia_sin_finca = db.cartography.aggregate(

        //---- Seleccionar features
        { "$match": { "type": "Feature" } },

        {
            $match: {
                // path: { $not: { $regex: `^,5de906055342334375e0804d,` } }
                path: { $not: { $regex: `^,` + i._id_str_farm + `,` } }
            }
        }
        , {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$_id"
                }
            }
        }

    );
});

// cartografia_sin_finca


//borrar datos
cartografia_sin_finca.forEach(item => {
    db.cartography.remove(
        {
            "_id": {
                "$in": item.ids
            }
        }
    )
});

