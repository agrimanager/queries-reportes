
//--query para egresos manuales
db.suppliesTimeline.aggregate(
    {
        $match:{
            quantity:{$lt:0},
            deleted:false,
            lbid:{$eq:""}
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    {$unwind:"$productos"}
    ,{
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    {$unwind:"$bodegas"}
    ,{
        "$project": {
            "wid": "$bodegas.name",
            //"tax":"",
            "name": "$productos.name",
            "type":"Egresos",
            "quantity": "$productEquivalence",
            //"invoiceNumber":"",
            "inf":"$information",
            //"invoiceDate":"",
            //"dueDate":"",
            "inputmeasure": "$inputmeasure",
            "rgDate": {$dateToString: {format: "%Y-%m-%d", date: "$rgDate"}},
            "subtotal":"",
            "lbid":"",
            "lbdoce":""
        }
    }
);
