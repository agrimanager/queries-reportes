db.form_sanidadcacaoenfermedades.aggregate([


    //---!!!DANGER....OJO CON rgDate y uDate
    //---llegan rgDate dia, rgDate mes, rgDate año.....
     //-----!!!DANGER (desproyectar fechas)
    //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
    {
        "$project": {
            "rgDate día":0,
            "rgDate mes": 0,
            "rgDate año": 0,
            "rgDate hora": 0,

            "uDate día":0,
            "uDate mes": 0,
            "uDate año": 0,
            "uDate hora": 0
        }
    },



    //=====================================================
    //--nombre_maestro : aaaaa
    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
    //--nombre_maestro_enlazado : aaaaa_bbb bbb bbb
    //--valor_maestro_enlazado : ccc cc c
    //=====================================================

    //--Maestro principal
    {
        "$addFields": {
            "nombre_maestro_principal": "Enfermedad_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }


    //--maestro enlazado
    , {
        "$addFields": {
            "nombre_maestro_enlazado": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    //"then": "$$dataKV.k",
                                    "then": {
                                        "$substr": ["$$dataKV.k","$num_letras_nombre_maestro_principal",
                                        {"$strLenCP": "$$dataKV.k"}]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }


    //----FALLA EN LA WEB
    /*
    , {
        "$unwind": {
            "path": "$nombre_maestro_enlazado",
            "preserveNullAndEmptyArrays": true
        }
    }
    */

     //---//obtener el primero (aveces falla)
    , {
        "$addFields": {
            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] }
        }
    }
    , {
        "$addFields": {
            "nombre_maestro_enlazado": {"$ifNull":["$nombre_maestro_enlazado",""]}
        }
    }




    //--Valor maestro enlazado
    , {
        "$addFields": {
            "valor_maestro_enlazado": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_maestro_enlazado",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "valor_maestro_enlazado": {"$ifNull":["$valor_maestro_enlazado",""]}
        }
    }

    ,{
        "$project":{
            "nombre_maestro_principal" : 0,
            "num_letras_nombre_maestro_principal" : 0
        }
    }






])
