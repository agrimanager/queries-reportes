
//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();

//--ciclar bases de datos
bases_de_datos.forEach(db_name => {
    //   console.log(db_name) 

    //--obtener formularios
    //var formularios = db.forms.find({});
    var formularios = db.getSiblingDB(db_name).forms.aggregate();

    //--ciclar formularios
    formularios.forEach(item => {

        //var result = db.getCollection(item.anchor).aggregate(
        var result = db.getSiblingDB(db_name).getCollection(item.anchor).aggregate(
            {
                $match: {
                    uid: { $exists: false }
                }
            }

        ).count();

        if (result > 0) {
            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: item.name,
                registros_malos: result

            })
        }

    });



});

//--imprimir resultado
result_info