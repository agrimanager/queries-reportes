db.users.aggregate(
    [

        //---test filtro de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-04-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //---------------------------------------

        {
            "$lookup": {
                "from": "tasks",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //----filtro de fechas
                    { "$addFields": { "fecha_filtro": "$VARIABLE_DE_FECHA_FILTRO" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },
                    
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },



                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




        //---ojo variable rgDate
        //rgDate





    ]
)