// db.tracks.find({})
//   .projection({})
//   .sort({_id:-1})
//   .limit(100)
   
   
// el resultado son los repetidos
// db.tracks.aggregate(
//     {"$group" : { "_id": "$rgDate", "count": { "$sum": 1 } } },
//     {"$match": {"_id" :{ "$ne" : null } , "count" : {"$gt": 1} } }, 
//     {"$sort": {"count" : -1} },
//     {"$project": {"fecha" : "$_id", "_id" : 0,"num":"$count"} }     
// )


var cursor = db.tracks.aggregate([
    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        "_id": null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": {
                        "$slice": [
                            "$data",
                            { "$subtract": [ { "$size": "$data" }, 1 ] }
                        ]
                    },
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": [ "$$value", "$$this" ] }
                }
            }
        }
    }
], { "allowDiskUse": true });
var result = null;
cursor.forEach(item => {
    result = db.tracks.remove(
        {
            "_id": {
                "$in": item.ids
            }
        }
    );
});
cursor.close();
result;