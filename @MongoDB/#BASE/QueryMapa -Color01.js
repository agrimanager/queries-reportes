[

    {
        "$addFields": {
            "num_color": {
                "$divide": ["$Cantidad", "$Edad"]
            }
        }
    },

    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {"$and": [{"$gt": ["$num_color", 0]}, {"$lt": ["$num_color", 0.3]}]},
                    "then": "#666666",
                    "else": {
                        "$cond": {
                            "if": {"$and": [{"$gte": ["$num_color", 0.3]}, {"$lt": ["$num_color", 0.6]}]},
                            "then": "#9ACD32",
                            "else": "#FFFD1F"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "_id": "$Cartography._id",
            "idform": "$idform",
            "type": "Feature",
            "properties": {
                "color": "$color"
            },
            "geometry": "$Cartography.geometry"
        }
    }
]