
//--query para ingresos
db.suppliesTimeline_new0.aggregate(
    {
        $match:{
            quantity:{$gt:0},
            deleted:false,
            lbid : ""
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    {$unwind:"$productos"}
    ,{
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    {$unwind:"$bodegas"}
    ,{
        "$project": {
            "wid": "$bodegas.name",
            "tax":{
                $cond: {
                    if: {
                        $eq: ["$tax", ""]
                    },
                    then: 0,
                    else: "$tax"
                }
            },
            "name": "$productos.name",
            "type":"Ingresos",
            "quantity": "$productEquivalence",
            "invoiceNumber":"$invoiceNumber",
            "inf":"$information",
            "invoiceDate": {
                $cond: {
                    if: {
                        $eq: ["$invoiceDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$invoiceDate"}}
                }
            },
            "dueDate":{
                $cond: {
                    if: {
                        $eq: ["$dueDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$dueDate"}}
                }
            },
            "inputmeasure": "$inputmeasure",
            "rgDate": {$dateToString: {format: "%Y-%m-%d", date: "$rgDate"}},
            "subtotal":"$subtotal",
            "lbid":"",
            "lbdoce":""
        }
    }
);