var cacheMaestrosCampo = {};
var cacheMaestrosOpcion = {};

var obtenerMaestrosOpcion = opcion => {
    if (!cacheMaestrosOpcion[opcion.master]) {
        cacheMaestrosOpcion[opcion.master] = db.masters
            .findOne({
                _id: ObjectId(opcion.master)
            }, {name: 1});
    }
    return {
        name: opcion.name,
        master: cacheMaestrosOpcion[opcion.master]
    };
};

var obtenerMaestrosCampo = campo => {
    if (!cacheMaestrosCampo[campo.master]) {
        var master = db.masters
            .findOne({
                _id: ObjectId(campo.master)
            }, {name: 1, options: 1});
        master.options = master.options
            .filter(opcion => opcion.master)
            .map(obtenerMaestrosOpcion);
        cacheMaestrosCampo[campo.master] = master;
    }
    campo.master = cacheMaestrosCampo[campo.master]
    return campo;
};

var transformarOpcionMovil = opcion => {
    return {
        de: opcion.name,
        a: opcion.master.name
    };
}

var transformacionCampoMovil = campo => {
    var transformacionesCampo = campo.master.options.map(opcion => {
        var transformacionOpcion = transformarOpcionMovil(opcion);
        transformacionOpcion.a = campo.name + "_" + transformacionOpcion.a
        return {
            filtro: {
                [transformacionOpcion.de]: {$exists: true},
                [campo.name]: transformacionOpcion.de
            },
            transformacion: {
                $rename: {
                    [transformacionOpcion.de]: transformacionOpcion.a
                }
            }
        };
    });
    if (!transformacionesCampo.length) return null;
    return {
        campo: campo.name,
        transformaciones: transformacionesCampo
    };
}

var result = [];
var generarQueryActualizacion = form => {
    var transformaciones = form.fstructure
        .map(campo => JSON.parse(campo))
        .filter(campo => campo.master)
        .map(obtenerMaestrosCampo)
        .map(transformacionCampoMovil)
        .filter(t => t)
        .flatMap(t => t.transformaciones);
    if (!transformaciones.length) return;
    
    transformaciones.forEach(t => {
        t.resultado = db.getCollection(form.anchor).updateMany(t.filtro, t.transformacion)
    });
    result.push({
        _id: form._id,
        nombre: form.name,
        coleccion: form.anchor,
        transformaciones
    });
};

var form_pipeline = [
    {
        $match: {
            name: {$in: ["Modulo de Plagas", "Sanidad Enfermedades"]}
        }
    },
    {
        $project: {
            name: 1,
            anchor: 1,
            fstructure: 1
        }
    }
];

db.forms.aggregate(form_pipeline).forEach(generarQueryActualizacion);
result;
