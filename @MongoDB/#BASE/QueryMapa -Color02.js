[
        {
            "$sort":{
                "Fecha ciclo":1
            }
        },
        {
            "$group": {
                "_id": {
                    "lote": "$Cartography._id",
                    "geometry": "$Cartography.geometry",
                    "today":"$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        },

        {
            "$addFields": {
                "estado_ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo cosecha",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha_ciclo_inicio": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },
        
        {
            "$addFields": {
                "penultimo_censo": {
                     "$arrayElemAt": ["$data", { "$subtract": [{"$size": "$data"},2]}]
                }
            }
        },
        
        {
            "$addFields": {
                "fecha_ciclo_inicio": {
                    "$cond": {
                        "if": { "$eq": ["$estado_ciclo", "inicia"] },
                        "then": "$fecha_ciclo_inicio",
                        "else": "$penultimo_censo.Fecha ciclo"
                    }
                }
            
            }
        },

        {
            "$addFields": {
                "dias": {
                    "$cond": {
                        "if": { "$eq": ["$estado_ciclo", "inicia"] },
                        "then": -999,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha_ciclo_inicio"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias", -999]
                        },
                        "then": "#666666",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias", 0] }, { "$lt": ["$dias", 9] }]
                                },
                                "then": "#2570A9",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias", 9] }, { "$lt": ["$dias", 12] }]
        
                                        },
                                        "then": "#9ACD32",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias", 12] }, { "$lt": ["$dias", 15] }]
                                                },
                                                "then": "#FFFD1F",
                                                "else": "#E84C3F"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        
        
        {
            "$project": {
                "_id": "$_id.lote",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias", -999] },
                            "then": "En proceso",
                            "else": {
                                "$concat": [
                                    {"$toString": "$dias"},
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": "$_id.geometry"

            }
        }
    ]