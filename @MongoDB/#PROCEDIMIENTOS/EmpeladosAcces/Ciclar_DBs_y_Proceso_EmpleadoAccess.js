//----Ojo usar desde db local

//============ QUERY
var query1 = [
    {
        $match: {
            "onlineAccess" : true
        }
    }
]

//============ DB
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["lukeragricola"];

//============ COLECCION
var nombre_coleccion = "employees";


//============ Resultado
var result_info = [];


//============ PROCESO
//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {

    var result = db.getSiblingDB(db_name)
        .getCollection(nombre_coleccion)
        .aggregate(query1)
        .allowDiskUse()
        .count();

    if (result > 0) {
        result_info.push({
            database: db_name,
            empleados_Access: result

        })
    }

});

//--imprimir resultado
result_info