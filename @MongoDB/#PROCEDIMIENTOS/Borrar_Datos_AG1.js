
//----Ojo usar desde db local

var query_pipeline = [

    {
        $match: {
            capture:"AG1"
        }
    },
    
    {
        $project: {
            _id: 1
        }
    }

    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": 1
        }
    }

];



//====== Resultado
var result_info = [];


//--obtener bases de datos
//var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["tarapaca"]

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;


        //---Ejecutar query
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline)
            .allowDiskUse();

        //---Borrar
        var result = null;
        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                registros_malos: result

            })
        });
        data_query.close();
    });



});

//--imprimir resultado
result_info