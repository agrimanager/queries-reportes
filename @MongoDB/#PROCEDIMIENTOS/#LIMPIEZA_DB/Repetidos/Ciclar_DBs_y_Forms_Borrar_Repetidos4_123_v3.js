
//----Ojo usar desde db local

//---query1 //repetidos full
var query_pipeline1 = [
    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        //--excluir variables
                        "_id": null
                        //,"supervisor":null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": {
                        "$slice": [
                            "$data",
                            { "$subtract": [{ "$size": "$data" }, 1] }
                        ]
                    },
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": ["$$value", "$$this"] }
                }
            }
        }
    }
];

//---query2 //repetidos supervisor [null,""]
var query_pipeline2 = [

    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        //--excluir variables
                        "_id": null
                        , "supervisor": null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },


    //---verificar el dato que tenga supervisor
    {
        $addFields: {
            "data2": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$in": [
                            "$$censo.supervisor",
                            [null, ""]
                        ]
                    }
                }
            }
        }
    }
    , {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": "$data2",
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },

    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": ["$$value", "$$this"] }
                }
            }
        }
    }
];

//---query2 //repetidos supervisor [null,""]
var query_pipeline2_2 = [


        {
            "$group": {
                "_id": {
                    "$mergeObjects": [
                        "$$ROOT",
                        {
                            //--excluir variables
                            "_id": null
                            , "supervisor": null
                        }
                    ]
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        },
        {
            "$match": {
                "$expr": {
                    "$gt": [{ "$size": "$data" }, 1]
                }
            }
        },



        {
            "$project": {
                "_id": 0,
                "ids": {
                    "$map": {
                        "input": {
                            "$slice": [
                                "$data",
                                { "$subtract": [{ "$size": "$data" }, 1] }
                            ]
                        },
                        "as": "item",
                        "in": "$$item._id"
                    }
                }
            }
        },
        {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$ids"
                }
            }
        },
        {
            "$project": {
                "_id": 0,
                "ids": {
                    "$reduce": {
                        "input": "$ids",
                        "initialValue": [],
                        "in": { "$concatArrays": ["$$value", "$$this"] }
                    }
                }
            }
        }
];

//---query3 //no existe uid
var query_pipeline3 = [

    {
        $match: {
            uid: { $exists: false }
        }
    },
    {
        $project: {
            _id: 1
        }
    }

    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": 1
        }
    }

];


//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

  console.log(db_name_aux);

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline1)
            .allowDiskUse();


        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                tquery: "query1 -- repetidos full",
                registros_malos: result.nRemoved

            })
        });
        data_query.close();

        //===================query2
        data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline2)
            .allowDiskUse();

        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                tquery: "query2 -- repetidos supervisor null o vacio",
                registros_malos: result.nRemoved

            })
        });
        data_query.close();

        //===================query2_2
        data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline2_2)
            .allowDiskUse();

        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                tquery: "query2_2 -- repetidos excluyendo supervisor",
                registros_malos: result.nRemoved

            })
        });
        data_query.close();


        //===================query3
        data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline3)
            .allowDiskUse();

        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                tquery: "query3 -- sin uid",
                registros_malos: result.nRemoved

            })
        });
        data_query.close();





    });



});

//--imprimir resultado
result_info
