//----Ojo usar desde db local

var db_name = "agricolapersea";

var coleccion_name = "form_monitoreodeenfermedades";

var query_pipeline = [

    {
        $match: {
            uid: { $exists: false }
        }
    },
    {
        $project: {
            _id: 1
        }
    }

    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": 1
        }
    }

];


//---Ejecutar query
var data_query = db.getSiblingDB(db_name)
    .getCollection(coleccion_name)
    .aggregate(query_pipeline)
    .allowDiskUse();


//data_query


//---Borrar
var result = null;
//--🔄 data
data_query.forEach(item => {

    result = db.getSiblingDB(db_name)
        .getCollection(coleccion_name)
        .remove(
            {
                "_id": item._id
            }
        );
});
data_query.close();
result;