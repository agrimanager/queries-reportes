//----Ojo usar desde db local

var db_name = "lukeragricola";

var coleccion_name = "tasks";

var query_pipeline = [
    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        //--excluir variables
                        "_id": null
                        , "cod": null
                        , "rgDate": null
                        , "uDate": null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": {
                        "$slice": [
                            "$data",
                            { "$subtract": [{ "$size": "$data" }, 1] }
                        ]
                    },
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": ["$$value", "$$this"] }
                }
            }
        }
    }
];


//---Ejecutar query
var data_query = db.getSiblingDB(db_name)
    .getCollection(coleccion_name)
    .aggregate(query_pipeline)
    .allowDiskUse();


data_query


// //---Ver
// var result = null;
// //--🔄 data
// data_query.forEach(item => {

//     result = db.getSiblingDB(db_name)
//         .getCollection(coleccion_name)
//         .aggregate(
//             {
//                 $match: {
//                     "_id": {
//                         "$in": item.ids
//                     }
//                 }
//             }
//         );
// });
// data_query.close();
// result;

//---Borrar
var result = null;
//--🔄 data
data_query.forEach(item => {

    result = db.getSiblingDB(db_name)
        .getCollection(coleccion_name)
        .remove(
            {
                "_id": {
                    "$in": item.ids
                }
            }
        );
});
data_query.close();
result;