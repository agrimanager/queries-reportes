//----Ojo usar desde db local

var db_name = "agricolapersea";

var coleccion_name = "form_monitoreodeenfermedades";

var query_pipeline = [

    {
        "$group": {
            "_id": {
                "$mergeObjects": [
                    "$$ROOT",
                    {
                        //--excluir variables
                        "_id": null
                        , "supervisor": null
                    }
                ]
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$match": {
            "$expr": {
                "$gt": [{ "$size": "$data" }, 1]
            }
        }
    },


    //---verificar el dato que tenga supervisor
    {
        $addFields: {
            "data2": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$in": [
                            "$$censo.supervisor",
                            [null,""]
                        ]
                    }
                }
            }
        }
    }
    , {
        "$project": {
            "_id": 0,
            "ids": {
                "$map": {
                    "input": "$data2",
                    "as": "item",
                    "in": "$$item._id"
                }
            }
        }
    },

    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$ids"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": {
                "$reduce": {
                    "input": "$ids",
                    "initialValue": [],
                    "in": { "$concatArrays": ["$$value", "$$this"] }
                }
            }
        }
    }
];


//---Ejecutar query
var data_query = db.getSiblingDB(db_name)
    .getCollection(coleccion_name)
    .aggregate(query_pipeline)
    .allowDiskUse();


//data_query


//---Borrar
var result = null;
//--🔄 data
data_query.forEach(item => {

    result = db.getSiblingDB(db_name)
        .getCollection(coleccion_name)
        .remove(
            {
                "_id": {
                    "$in": item.ids
                }
            }
        );
});
data_query.close();
result;