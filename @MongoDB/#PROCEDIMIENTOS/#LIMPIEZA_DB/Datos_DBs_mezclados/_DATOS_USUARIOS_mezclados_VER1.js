
//DB_USUARIOS
//===============================================================

//----Ojo usar desde db local

//====== Resultado
var db_usuarios = [];

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["acacias", "agricolaocoa", "agricolapersea"];

var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local"

    , "finca_alejo"
]

var colecciones_lista_negra = [
    "system.views"

    , "tokens"
    , "notifications"
]


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name))
        return;

    console.log(db_name);

    var datos_usuario = db.getSiblingDB(db_name).users.aggregate(
        [

            {
                $addFields: {
                    _id_str: { $toString: "$_id" }
                }
            },
            {
                $group: {
                    _id: null
                    , _ids: { $push: "$_id" }
                    , _ids_str: { $push: "$_id_str" }
                }
            }

        ]
    );


    datos_usuario.forEach(item => {

        db_usuarios.push({
            database: db_name,
            _ids: item._ids,
            _ids_str: item._ids_str
        })

    });



});

//--imprimir resultado
// db_usuarios

console.log("-------------------------------");

//DB_COLECCIONES
//===============================================================
// console.log(bases_de_datos)//TEST

//====== Resultado
var data_usuarios_mezclados = [];

db_usuarios.forEach(item_usuario => {
    // console.log(item)

    console.log("---->");
    console.log(item_usuario.database);

    //--obtener nombres de colecciones
    var colecciones = db.getSiblingDB(item_usuario.database).getCollectionInfos();

    colecciones.forEach(item_coleccion => {

        if (colecciones_lista_negra.includes(item_coleccion.name))
            return;

        console.log(item_coleccion.name);

        if (item_coleccion.name === "syncdata") {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [



                        {
                            $match: {
                                "syncInfo.uid": {
                                    $not: {
                                        $in: item_usuario._ids_str
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                _id: null
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        } else {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [

                        {
                            $match: {
                                uid: { $exists: true }
                            }
                        },

                        {
                            $match: {
                                uid: {
                                    $not: {
                                        $in: item_usuario._ids
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                _id: null
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        }





        datos_coleccion.forEach(item_datos_coleccion => {

            data_usuarios_mezclados.push({
                database: item_usuario.database,
                coleccion: item_coleccion.name,
                cantidad: item_datos_coleccion.cantidad,
                max_fecha: item_datos_coleccion.max_fecha,
                min_fecha: item_datos_coleccion.min_fecha
            })

        });


    });

})


data_usuarios_mezclados
