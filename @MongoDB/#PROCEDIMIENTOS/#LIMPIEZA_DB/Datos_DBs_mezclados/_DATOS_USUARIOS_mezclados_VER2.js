
//DB_USUARIOS
//===============================================================

//----Ojo usar desde db local

//====== Resultado
var db_usuarios = [];

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["acacias", "agricolaocoa", "agricolapersea"];

var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local"

    , "finca_alejo"
    ,"invcamaru_testingNoBorrar"
    ,"lukeragricola"
]

var colecciones_lista_negra = [
    "system.views"

    , "tokens"
    , "notifications"

]


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name))
        return;

    console.log(db_name);

    var datos_usuario = db.getSiblingDB(db_name).users.aggregate(
        [

            {
                $addFields: {
                    _id_str: { $toString: "$_id" }
                }
            },
            {
                $group: {
                    _id: null
                    , _ids: { $push: "$_id" }
                    , _ids_str: { $push: "$_id_str" }
                }
            }

        ]
    );


    datos_usuario.forEach(item => {

        db_usuarios.push({
            database: db_name,
            _ids: item._ids,
            _ids_str: item._ids_str
        })

    });



});

//--imprimir resultado
// db_usuarios

console.log("-------------------------------");

//DB_COLECCIONES
//===============================================================
// console.log(bases_de_datos)//TEST

//====== Resultado
var data_usuarios_mezclados = [];

db_usuarios.forEach(item_usuario => {
    // console.log(item)

    console.log("---->");
    console.log(item_usuario.database);

    //--obtener nombres de colecciones
    var colecciones = db.getSiblingDB(item_usuario.database).getCollectionInfos();

    colecciones.forEach(item_coleccion => {

        if (colecciones_lista_negra.includes(item_coleccion.name))
            return;

        console.log(item_coleccion.name);

        if (item_coleccion.name === "syncdata") {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [



                        {
                            $match: {
                                "syncInfo.uid": {
                                    $not: {
                                        $in: item_usuario._ids_str
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                //_id: null
                                //_id: { "$toObjectId": "$syncInfo.uid" }
                                _id: "$syncInfo.uid"
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        } else {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [

                        {
                            $match: {
                                uid: { $exists: true }
                            }
                        },

                        {
                            $match: {
                                uid: {
                                    $not: {
                                        $in: item_usuario._ids
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                // _id: null
                                // _id: "$uid"
                                _id: { "$toString": "$uid" }
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        }





        datos_coleccion.forEach(item_datos_coleccion => {

            data_usuarios_mezclados.push({
                database: item_usuario.database,
                coleccion: item_coleccion.name,
                uid: item_datos_coleccion._id,
                cantidad: item_datos_coleccion.cantidad,
                max_fecha: item_datos_coleccion.max_fecha,
                min_fecha: item_datos_coleccion.min_fecha
            })

        });


    });

})



//DATOS MEZCLADOS ENTRE DBS Y COLECCIONES
//===============================================================
//====== Resultado
var data_usuarios_mezclados_uids = [];

data_usuarios_mezclados.forEach(item_data_usuarios_mezclados => {

    var database_uid = null

    db_usuarios.forEach(item_usuario => {

        if (item_usuario._ids_str.includes(item_data_usuarios_mezclados.uid)) {
            database_uid = item_usuario.database
            return;
        }


    })

    data_usuarios_mezclados_uids.push({

        coleccion: item_data_usuarios_mezclados.coleccion,
        db_origen: item_data_usuarios_mezclados.database,
        db_destino: database_uid,
        uid_destino: item_data_usuarios_mezclados.uid,
        cantidad_datos: item_data_usuarios_mezclados.cantidad,
        max_fecha: item_data_usuarios_mezclados.max_fecha,
        min_fecha: item_data_usuarios_mezclados.min_fecha

    })


})


data_usuarios_mezclados_uids
