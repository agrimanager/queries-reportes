
//----Ojo usar desde db local

//---query //para seleccionar ids a borrar
var query_pipeline = [
        //----filtro de fechas
        {
            $addFields: {
                //NOTA: se eliminan sincronizaciones MENORES a esta fecha_desde
                "fecha_desde": ISODate("2021-04-01T12:00:00.000-05:00")//🚩...EDITAR (2 meses atras)
            }
        },
        {
            "$match": {
                "$expr": {
                    "$lt": [
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                        ,
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_desde" } } }
                    ]
                }
            }
        }


        //----agrupar ids
        , {
            $project: {
                _id: 1
            }
        }

        , {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$_id"
                }
            }
        },
        {
            "$project": {
                "_id": 0,
                "ids": 1
            }
        }
    ];

//====== Resultado
var result_info = [];



//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //--obtener tablas
    var colecciones = ["syncdata","logs"];//SINCRONIZACIONES y LOGS
    //notifications
    //tokens

    //--🔄 ciclar tablas
    colecciones.forEach(nombre_coleccion => {

        var db_name = db_name_aux;
        var coleccion_name = nombre_coleccion;
        var result = null;


        //---Ejecutar queries

        //===================query
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline)
            .allowDiskUse();


        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .remove(
                    {
                        "_id": {
                            "$in": item_data.ids
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: coleccion_name,
                cantidad: result.nRemoved

            })
        });
        data_query.close();

    });



});

//--imprimir resultado
result_info
