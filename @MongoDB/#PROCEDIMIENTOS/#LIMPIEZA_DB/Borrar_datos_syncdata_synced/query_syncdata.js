db.syncdata.aggregate(

    [
        //----filtro de fechas
        {
            $addFields: {
                //NOTA: se eliminan sincronizaciones MENORES a esta fecha_desde
                "fecha_desde": ISODate("2021-04-01T12:00:00.000-05:00")//🚩...EDITAR (2 meses atras)
            }
        },
        {
            "$match": {
                "$expr": {
                    "$lt": [
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                        ,
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_desde" } } }
                    ]
                }
            }
        }


        //----agrupar ids
        , {
            $project: {
                _id: 1
            }
        }

        , {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$_id"
                }
            }
        },
        {
            "$project": {
                "_id": 0,
                "ids": 1
            }
        }
    ]



)

// .count()
