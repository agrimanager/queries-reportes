
//----Ojo usar desde db local

//---query1 //repetidos full
var query_pipeline1 = [];

//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //--obtener formularios
    //var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();
    var colecciones = ["syncdata","synced"];

    //--🔄 ciclar formularios
    colecciones.forEach(nombre_coleccion => {

        var db_name = db_name_aux;
        var coleccion_name = nombre_coleccion;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .drop();


        result_info.push({
            database: db_name,
            colleccion: coleccion_name,
            cantidad: data_query

        })

    });



});

//--imprimir resultado
result_info