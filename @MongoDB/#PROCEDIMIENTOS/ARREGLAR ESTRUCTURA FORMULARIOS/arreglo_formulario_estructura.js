var data = db.forms.aggregate(

    {
        $addFields: {
            tipo_variable_estructura: { "$type": "$fstructure" }

        }
    },

    {
        $match:{
            tipo_variable_estructura:"object"
        }
    },

    {
        $addFields: {
            array_variable_estructura: { "$objectToArray": "$fstructure" }

        }
    }

)

// data

data.forEach(item_data => {

    var array_variable_estructura_new = []

    item_data.array_variable_estructura.forEach(item => {
        array_variable_estructura_new.push(item.v)
    })

    db.forms.update(
        {
            _id: item_data._id
        },
        {
            $set: {
                "fstructure": array_variable_estructura_new
            }
        }
    )


})
