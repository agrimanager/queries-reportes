//----Ojo usar desde db local

//....query de condicionales
var query_pipeline = [
    //--VARIABLES
    {
        "$addFields": {
            "Tipo de transaccion": {
                "$cond": {
                    "if": {
                        "$gt": ["$quantity", 0]
                    },
                    "then": "ingreso",
                    "else": "egreso"
                }
            }
        }
    }

    //--CONDICIONES
    , {
        "$match": {
            "deleted": { "$eq": false }
            , "transfer": { "$eq": true }
            , "Tipo de transaccion": { "$eq": "ingreso" }

            , "total": {
                "$lt": 0
            }

        }
    }

];



//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["agricolapersea"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //--obtener formularios
    //var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();
    var formularios = ["suppliesTimeline"];

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        //var coleccion_name = item.anchor;
        var coleccion_name = item;


        //---Ejecutar query
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline)
            .allowDiskUse();

        var result = null;
        var data1_new = 0;
        var data2_new = 0;
        //--🔄 data
        data_query.forEach(item_data => {

            data1_new = item_data.total * -1;
            data2_new = item_data.subtotal * -1;

            //---actualizar
            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .update(
                    {
                        "_id": item_data._id
                    },
                    {
                        $set: {
                            //multiplicar por -1 a total y subtotal
                            total: data1_new,
                            subtotal: data2_new
                        }
                    }
                );

            result_info.push({
                database: db_name,
                coleccion: coleccion_name,
                registro_id: item_data._id
                // registros: result

            })
        });
        data_query.close();
    });



});

//--imprimir resultado
result_info