
//----Ojo usar desde db local

//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["lukeragricola"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {


    //--obtener formularios
    var fincas = db.getSiblingDB(db_name).farms.aggregate(
        {
            $addFields: {
                finca_id: { $toString: "$_id" }
            }
        }
    ).allowDiskUse();

    //--🔄 ciclar formularios
    fincas.forEach(item => {

        result_info.push({
            database: db_name,
            finca: item.name,
            finca_oid: item._id,
            finca_id: item.finca_id

        })

    });

});

//--imprimir resultado
result_info
