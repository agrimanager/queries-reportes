//----Ojo usar desde db local

//============ QUERY
var query1 = [
    //ordenar
    { $sort: { "rgDate": 1 } }
    //seleccionar 1ro
    , { $limit: 1 }
];

//========= DB
 var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["lukeragricola", "agricolapersea","acacias"];

//============ COLECCION = formularios
var nombre_coleccion = "";
var nombre_formulario = "";


//============ Resultado
var result_info = [];           //Todas las DBs
var result_info_aux = [];       //Una DB


//============ PROCESO
//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {

    //reset x cada DB
    result_info_aux = [];

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name).forms.aggregate().allowDiskUse();

    //--🔄 ciclar formularios
    formularios.forEach(item_form => {

        nombre_coleccion = item_form.anchor;
        nombre_formulario = item_form.name;

        var result = db.getSiblingDB(db_name)
            .getCollection(nombre_coleccion)
            .aggregate(query1)
            .allowDiskUse();


        //--🔄 ciclar 
        result.forEach(item_result => {
            result_info_aux.push({
                database: db_name,
                colleccion: nombre_coleccion,
                formulario: nombre_formulario,
                INFORMACION: item_result.rgDate
            });
        });

    });


    //============ORDENAR ARRAY SEGUN FECHA
    //---sort array by fecha
    result_info_aux.sort(function(a, b) {
        var keyA = new Date(a.INFORMACION),
            keyB = new Date(b.INFORMACION);
        // Compare the 2 dates
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
    });

    //--OBTENER 1RO
    result_info.push(result_info_aux[0]);


});

//--imprimir resultado
result_info