//----Ojo usar desde db local

//============ QUERY
var query1 = [
    //ordenar
    { $sort: { "rgDate": 1 } }
    //seleccionar 1ro
    , { $limit: 1 }
]

//============ DB
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["lukeragricola"];

//============ COLECCION = formularios
var nombre_coleccion = "";
var nombre_formulario = "";


//============ Resultado
var result_info = [];


//============ PROCESO
//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name).forms.aggregate().allowDiskUse();

    //--🔄 ciclar formularios
    formularios.forEach(item_form => {

        nombre_coleccion = item_form.anchor;
        nombre_formulario = item_form.name;

        var result = db.getSiblingDB(db_name)
            .getCollection(nombre_coleccion)
            .aggregate(query1)
            .allowDiskUse();


        //--🔄 ciclar formularios
        result.forEach(item_result => {
            result_info.push({
                database: db_name,
                colleccion: nombre_coleccion,
                formulario: nombre_formulario,
                INFORMACION: item_result.rgDate
            });
        });

    });

});

//--imprimir resultado
result_info