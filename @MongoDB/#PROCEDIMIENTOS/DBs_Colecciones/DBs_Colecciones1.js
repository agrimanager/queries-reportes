
//----Ojo usar desde db local

//====== Resultado
var result_info = [];

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["oleocaribe"];

var bases_de_datos_sistema = ["admin", "config", "local"];
var colecciones_sistema = ["system.views"];


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    if (bases_de_datos_sistema.includes(db_name) === false) {

        console.log(db_name)

        //--obtener colecciones
        var db_colecciones = db.getSiblingDB(db_name).getCollectionInfos();

        //--🔄 ciclar colecciones
        db_colecciones.forEach(item => {

            if (colecciones_sistema.includes(item.name) === false) {

                result_info.push({
                    database: db_name,
                    colleccion: item.name,

                    //---indicadores
                    num_datos: db.getSiblingDB(db_name).getCollection(item.name).count(),

                    size: db.getSiblingDB(db_name).getCollection(item.name).totalSize(),
                    indexSizes: db.getSiblingDB(db_name).getCollection(item.name).totalIndexSize(),
                    storageSize: db.getSiblingDB(db_name).getCollection(item.name).storageSize(),


                })
            }

        });
    }



});

//--imprimir resultado
result_info
