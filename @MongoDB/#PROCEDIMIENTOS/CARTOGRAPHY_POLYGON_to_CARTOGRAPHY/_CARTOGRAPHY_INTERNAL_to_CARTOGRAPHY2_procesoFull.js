

//1) Tirar query de cartography_internal

db.cartography.aggregate(

    //---- Seleccionar poligonos y lineas
    {
        "$match": {
            "type": "Feature",
            "geometry.type":{ $ne: "Point" }
        }
    }

    //---- Agregar valores nuevos
    , {
        "$addFields": {
            "_id_string": { "$toString": "$_id" }
        }
    }

    //---- Obtener features internos
    , {
        "$lookup": {
            "from": "cartography",
            "as": "internal_features",
            "let": {
                "feature_id_string": "$_id_string"
            },
            "pipeline": [{
                "$match": {
                    "$expr": {
                        "$gt": [{
                            "$indexOfCP": ["$path", "$$feature_id_string"]
                        },
                        -1
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": "$properties.type",
                    "count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "type_features": "$_id",
                    "count_features": "$count"
                }
            }
            ]
        }
    }




    //---- Seleccionar valores a guardar
    , {
        $project: {
            "_id": 1,
            "type": "$properties.type",
            "name": "$properties.name",
            "internal_features": "$internal_features",
            "path": "$path"
        }
    }

    // //---- Insertar en nueva coleccion
    ,{ $out : "cartography_internal" }


)




//2) Query para cartography_internal
var cursor_cartography_internal = db.cartography_internal.aggregate(
    [
        // //--obtener internal_features
        { $unwind: "$internal_features" }

    ]
)


//ver -- test
// cursor_cartography_internal





//3) Proceso de update
cursor_cartography_internal.forEach(item_cartography_internal => {
    // console.log(item_cartography_internal.name); // test ver


    if (item_cartography_internal.type == "blocks") {

        //blocks --> (lotes, lineas, arboles)
        if (item_cartography_internal.internal_features.type_features == "lot") {

            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lotes": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )

        } else if (item_cartography_internal.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )

        }
        else if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }


    }
    else if (item_cartography_internal.type == "lot") {

        //lot --> (lineas, arboles)
        if (item_cartography_internal.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        } else if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }

    }

    else if (item_cartography_internal.type == "lines") {

        if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }

    }


})


// 4-reporte de inventario del cultivo

// --obtener usuario
var usuarios_ = db.users.find({}).limit(1);
var user_id = null;

usuarios_.forEach(usr=>{
    user_id = usr._id
})
user_id


//---insert report
db.getCollection("reports").insert({
    //"uid": ObjectId("XXXXXXX"),
    "uid": user_id,
    "name": "📝Inventario del Cultivo",
    "description": "",
    "form": NumberInt(0),
    "anchor": "",
    "query": "[\n\n\n    { \"$limit\": 1 },\n    {\n        \"$lookup\": {\n            \"from\": \"cartography\",\n            \"as\": \"data\",\n            \"let\": {\n                \"filtro_fecha_inicio\": \"$Busqueda inicio\",\n                \"filtro_fecha_fin\": \"$Busqueda fin\"\n            },\n            \"pipeline\": [\n\n                {\n                    \"$match\": {\n                        \"properties.type\": { \"$in\": [\"blocks\", \"lot\"] }\n                    }\n                },\n\n\n                {\n                    \"$addFields\": {\n                        \"split_path_padres\": { \"$split\": [{ \"$trim\": { \"input\": \"$path\", \"chars\": \",\" } }, \",\"] }\n                    }\n                },\n                {\n                    \"$addFields\": {\n                        \"split_path_padres_oid\": { \"$map\": { \"input\": \"$split_path_padres\", \"as\": \"strid\", \"in\": { \"$toObjectId\": \"$$strid\" } } }\n                    }\n                },\n                {\n                    \"$addFields\": {\n                        \"variable_cartografia_oid\": [{ \"$toObjectId\": \"$_id\" }]\n                    }\n                },\n                {\n                    \"$addFields\": {\n                        \"split_path_oid\": {\n                            \"$concatArrays\": [\n                                \"$split_path_padres_oid\",\n                                \"$variable_cartografia_oid\"\n                            ]\n                        }\n                    }\n                },\n\n\n                {\n                    \"$lookup\": {\n                        \"from\": \"cartography\",\n                        \"localField\": \"split_path_oid\",\n                        \"foreignField\": \"_id\",\n                        \"as\": \"objetos_del_cultivo\"\n                    }\n                },\n\n                {\n                    \"$addFields\": {\n                        \"tiene_variable_cartografia\": {\n                            \"$cond\": {\n                                \"if\": { \"$eq\": [{ \"$size\": \"$split_path_oid\" }, { \"$size\": \"$objetos_del_cultivo\" }] },\n                                \"then\": \"si\",\n                                \"else\": \"no\"\n                            }\n                        }\n                    }\n                },\n\n                {\n                    \"$addFields\": {\n                        \"objetos_del_cultivo\": {\n                            \"$cond\": {\n                                \"if\": { \"$eq\": [\"$tiene_variable_cartografia\", \"si\"] },\n                                \"then\": \"$objetos_del_cultivo\",\n                                \"else\": {\n                                    \"$concatArrays\": [\n                                        \"$objetos_del_cultivo\",\n                                        [\"$variable_cartografia.features\"]\n                                    ]\n                                }\n                            }\n                        }\n                    }\n                },\n\n                {\n                    \"$addFields\": {\n                        \"finca\": {\n                            \"$filter\": {\n                                \"input\": \"$objetos_del_cultivo\",\n                                \"as\": \"item_cartografia\",\n                                \"cond\": { \"$eq\": [\"$$item_cartografia.type\", \"Farm\"] }\n                            }\n                        }\n                    }\n                },\n                {\n                    \"$unwind\": {\n                        \"path\": \"$finca\",\n                        \"preserveNullAndEmptyArrays\": true\n                    }\n                },\n                {\n                    \"$lookup\": {\n                        \"from\": \"farms\",\n                        \"localField\": \"finca._id\",\n                        \"foreignField\": \"_id\",\n                        \"as\": \"finca\"\n                    }\n                },\n                { \"$unwind\": \"$finca\" },\n\n                { \"$addFields\": { \"finca\": { \"$ifNull\": [\"$finca.name\", \"no existe\"] } } },\n\n                {\n                    \"$project\": {\n                        \"variable_cartografia\": 0,\n                        \"split_path_padres\": 0,\n                        \"split_path_padres_oid\": 0,\n                        \"variable_cartografia_oid\": 0,\n                        \"split_path_oid\": 0,\n                        \"objetos_del_cultivo\": 0,\n                        \"tiene_variable_cartografia\": 0\n                    }\n                }\n\n\n\n                ,{\n                    \"$project\": {\n                        \"finca\": \"$finca\"\n\n                        ,\"cartografia_nombre\": \"$properties.name\"\n                        ,\"cartografia_tipo\": {\n                            \"$cond\": {\n                                \"if\": { \"$eq\": [\"$properties.type\", \"lot\"] },\n                                \"then\": \"Lote\",\n                                \"else\": {\n                                    \"$cond\": {\n                                        \"if\": { \"$eq\": [\"$properties.type\", \"blocks\"] },\n                                        \"then\": \"Bloque\",\n                                        \"else\": \"otro\"\n                                    }\n                                }\n                            }\n                        }\n                        ,\"cartografia_production\": {\n                            \"$cond\": {\n                                \"if\": { \"$eq\": [\"$properties.production\", true] },\n                                \"then\": \"SI\",\n                                \"else\": \"NO\"\n                            }\n                        }\n                        ,\"cartografia_status\": {\n                            \"$cond\": {\n                                \"if\": { \"$eq\": [\"$properties.status\", true] },\n                                \"then\": \"SI\",\n                                \"else\": \"NO\"\n                            }\n                        }\n\n\n\n                        ,\"num_lotes\": {\n                            \"$ifNull\": [\"$properties.custom.num_lotes.value\",0]\n                        }\n\n                        ,\"num_lineas\": {\n                            \"$ifNull\": [\"$properties.custom.num_lineas.value\",0]\n                        }\n\n                        ,\"num_arboles\": {\n                            \"$ifNull\": [\"$properties.custom.num_arboles.value\",0]\n                        }\n\n                    }\n                }\n\n\n\n                ,{\n                    \"$addFields\": {\n                        \"rgDate\": \"$$filtro_fecha_inicio\"\n                    }\n                }\n\n            ]\n        }\n    }\n\n\n    , {\n        \"$project\":\n        {\n            \"datos\": {\n                \"$concatArrays\": [\n                    \"$data\"\n                    , []\n                ]\n            }\n        }\n    }\n\n    , { \"$unwind\": \"$datos\" }\n    , { \"$replaceRoot\": { \"newRoot\": \"$datos\" } }\n\n\n]\n",
    "pivotConf": null,
    "map": "",
    "querymap": "",
    "legend": "",
    "hidemap": false,
    "precompiled": "",
    "rgDate": new Date,
    "uDate": new Date
})
