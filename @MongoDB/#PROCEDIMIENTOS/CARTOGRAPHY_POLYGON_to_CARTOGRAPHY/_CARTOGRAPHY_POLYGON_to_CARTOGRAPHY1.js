

//1) Tirar query de cartography_plygon
//....
db.cartography.aggregate(

    //---- Seleccionar poligonos
    {
        "$match": {
            "type": "Feature",
            "geometry.type": "Polygon"
        }
    }

    //---- Agregar valores nuevos
    , {
        "$addFields": {
            "_id_string": { "$toString": "$_id" }
        }
    }

    //---- Obtener features internos
    , {
        "$lookup": {
            "from": "cartography",
            "as": "internal_features",
            "let": {
                "feature_id_string": "$_id_string"
            },
            "pipeline": [{
                "$match": {
                    "$expr": {
                        "$gt": [{
                            "$indexOfCP": ["$path", "$$feature_id_string"]
                        },
                        -1
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": "$properties.type",
                    "count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "type_features": "$_id",
                    "count_features": "$count"
                }
            }
            ]
        }
    }




    //---- Seleccionar valores a guardar
    , {
        $project: {
            "_id": 1,
            "type": "$properties.type",
            "name": "$properties.name",
            "internal_features": "$internal_features",
            "path": "$path"

            ,"fecha_data":new Date
        }
    }

    // //---- Insertar en nueva coleccion
    , { $out: "cartography_polygon" }


)





//2) Query para cartography_polygon
var cursor_cartography_polygon = db.cartography_polygon.aggregate(
    [

        //filtrar (blocks, lot)
        {
            "$match": {
                "type": { "$in": ["blocks", "lot"] }
            }
        }

        //--obtener internal_features
        , {
            "$unwind": {
                "path": "$internal_features",
                "preserveNullAndEmptyArrays": true
            }
        }


        //--filtrar internal_features (lotes, lineas, arboles)
        , {
            "$match": {
                "internal_features.type_features": { "$in": ["lot", "lines", "trees"] }
            }
        }

        // //TEST
        // , {
        //     $match: {
        //         // type: "blocks",
        //         path: {
        //             $regex: `^,5db1c0198b222e25759e6086,` ////----FINCA
        //         }
        //     }
        // },

    ]
)

//ver -- test
// cursor_cartography_polygon





//3) Proceso de update
cursor_cartography_polygon.forEach(item_cartography_polygon => {
    // console.log(item_cartography_polygon.name); // test ver


    if (item_cartography_polygon.type == "blocks") {

        //blocks --> (lotes, lineas, arboles)
        if (item_cartography_polygon.internal_features.type_features == "lot") {

            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_polygon._id },
                {
                    $set: {
                        "properties.custom.num_lotes": {
                            "type": "number",
                            "value": item_cartography_polygon.internal_features.count_features
                        }
                    }
                }
            )

        } else if (item_cartography_polygon.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_polygon._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_polygon.internal_features.count_features
                        }
                    }
                }
            )

        }
        else if (item_cartography_polygon.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_polygon._id },
                {
                    $set: {
                    "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_polygon.internal_features.count_features
                        }
                    }
                }
            )
        }


    } else if (item_cartography_polygon.type == "lot") {

        //lot --> (lineas, arboles)
        if (item_cartography_polygon.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_polygon._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_polygon.internal_features.count_features
                        }
                    }
                }
            )
        } else if (item_cartography_polygon.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_polygon._id },
                {
                    $set: {
                    "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_polygon.internal_features.count_features
                        }
                    }
                }
            )
        }

    }


})
