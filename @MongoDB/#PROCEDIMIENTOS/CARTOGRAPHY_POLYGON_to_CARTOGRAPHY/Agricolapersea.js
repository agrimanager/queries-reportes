

//1) Tirar query de cartography_internal
//....





//2) Query para cartography_internal
var cursor_cartography_internal = db.cartography_internal.aggregate(
    [
        // //--obtener internal_features
        { $unwind: "$internal_features" }

    ]
)


//ver -- test
// cursor_cartography_internal





//3) Proceso de update
cursor_cartography_internal.forEach(item_cartography_internal => {
    // console.log(item_cartography_internal.name); // test ver


    if (item_cartography_internal.type == "blocks") {

        //blocks --> (lotes, lineas, arboles)
        if (item_cartography_internal.internal_features.type_features == "lot") {

            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lotes": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )

        } else if (item_cartography_internal.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )

        }
        else if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }


    }
    else if (item_cartography_internal.type == "lot") {

        //lot --> (lineas, arboles)
        if (item_cartography_internal.internal_features.type_features == "lines") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_lineas": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        } else if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }

    }

    else if (item_cartography_internal.type == "lines") {

        if (item_cartography_internal.internal_features.type_features == "trees") {
            //====UPDATE -- agregar propiedad
            db.cartography.update(
                { _id: item_cartography_internal._id },
                {
                    $set: {
                        "properties.custom.num_arboles": {
                            "type": "number",
                            "value": item_cartography_internal.internal_features.count_features
                        }
                    }
                }
            )
        }

    }


})
