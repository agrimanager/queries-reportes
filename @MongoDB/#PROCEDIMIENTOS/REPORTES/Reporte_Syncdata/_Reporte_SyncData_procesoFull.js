
//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

  //--obtener usuario
  var usuarios_ = db.getSiblingDB(db_name_aux).users.find({}).limit(1);
  var user_id = null;
  usuarios_.forEach(usr=>{
      user_id = usr._id
  })

  //--insertar reporte
  db.getSiblingDB(db_name_aux).getCollection("reports").insert(
    {
    	// "uid" : ObjectId("5d138cc4430d802507dfcb25"),
      "uid": user_id,
    	"name" : "🎲 Sincronizaciones App Movil",
    	"description" : "",
    	"form" : 0,
    	"anchor" : "",
    	"query" : "[\n\n\n      { \"$limit\": 1 },\n      {\n          \"$lookup\": {\n              \"from\": \"syncdata\",\n              \"as\": \"data\",\n              \"let\": {\n                  \"filtro_fecha_inicio\": \"$Busqueda inicio\",\n                  \"filtro_fecha_fin\": \"$Busqueda fin\"\n              },\n\n              \"pipeline\": [\n\n                  {\n                      \"$project\": {\n                          \"supervisor sincronizacion\": \"$syncInfo.supervisor\",\n                          \"app version\": \"$syncInfo.version\",\n                          \"cantidad_censos\": { \"$size\": \"$Forms\" },\n                          \"rgDate\": \"$rgDate\"\n                      }\n                  }\n\n              ]\n          }\n      }\n\n\n      , {\n          \"$project\":\n          {\n              \"datos\": {\n                  \"$concatArrays\": [\n                      \"$data\"\n                      , []\n                  ]\n              }\n          }\n      }\n\n      , { \"$unwind\": \"$datos\" }\n      , { \"$replaceRoot\": { \"newRoot\": \"$datos\" } }\n\n\n  ]\n",
    	"pivotConf" : null,
    	"map" : "",
    	"querymap" : "",
    	"legend" : "",
    	"hidemap" : false,
    	"precompiled" : "",
    	// "rgDate" : ISODate("2021-09-30T13:17:29.784-05:00"),
    	// "uDate" : ISODate("2021-09-30T13:17:29.784-05:00")
      "rgDate": new Date,
      "uDate": new Date
    }
  )


});
