
//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //---Ejecutar queries

    //===================query1
    var data_query = db.getSiblingDB(db_name_aux)
        .getCollection("syncdata")
        .aggregate(
            [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2022-10-31T01:21:23.000-05:00") } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2022-11-20T01:21:23.000-05:00") } } }
                                    ]
                                }
                            ]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
                    }
                }


                , {
                    $project: {
                        "supervisor": "$syncInfo.supervisor"
                        , "version": "$syncInfo.version"
                        , "fecha": "$Fecha_Txt"
                    }
                }



            ]
        )
        .allowDiskUse();


    //--🔄 data
    data_query.forEach(item_data => {

        result_info.push({
            database: db_name_aux,
            supervisor: item_data.supervisor,
            version: item_data.version,
            fecha: item_data.fecha,

        })
    });
    data_query.close();



});

//--imprimir resultado
result_info
