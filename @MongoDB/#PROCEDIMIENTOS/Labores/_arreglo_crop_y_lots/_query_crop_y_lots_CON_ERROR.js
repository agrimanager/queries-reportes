db.tasks.aggregate(
    [
        {
            "$match": {
                "$expr": {
                    "$or": [
                        { "$eq": [{ "$type": "$crop" }, "object"] },
                        { "$eq": [{ "$type": "$lots" }, "object"] }
                    ]

                }
            }
        }


        //---ids
        , {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$_id"
                }
            }
        }

    ]
)