
//----Ojo usar desde db local

var query_pipeline = [
    {
        "$match": {
            "$expr": {
                "$or": [
                    { "$eq": [{ "$type": "$crop" }, "object"] },
                    { "$eq": [{ "$type": "$lots" }, "object"] }
                ]

            }
        }
    }


    //---ids
    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    }

];



//====== Resultado
var result_info = [];
var result = null;


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
var db_name = "";

var coleccion_name = "tasks";

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    result = null;
    db_name = db_name_aux;

    //---Ejecutar query
    var data_query = db.getSiblingDB(db_name)
        .getCollection(coleccion_name)
        .aggregate(query_pipeline)
        .allowDiskUse();



    // //---Ver
    // data_query.forEach(item_data => {

    //     result = db.getSiblingDB(db_name)
    //         .getCollection(coleccion_name)
    //         .aggregate(
    //             {
    //                 "_id": {
    //                     "$in": item_data.ids
    //                 }
    //             }
    //         )
    //         .count();

    //     result_info.push({
    //         database: db_name,
    //         colleccion: coleccion_name,
    //         CRUD: "Select",
    //         resultado: result

    //     })
    // });



    //---Actualizar
    data_query.forEach(item_data => {

        result = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .updateMany(
                //-query update
                {
                    "_id": {
                        "$in": item_data.ids
                    }
                },
                {
                    $set: {
                        //---CAMPOS A MODIFICAR
                        //"supervisor": "PAL20 WILSON ALEJANDR
                        "crop": [],
                        "lots": []
                    }
                }
            );

        result_info.push({
            database: db_name,
            colleccion: coleccion_name,
            CRUD: "Update",
            result: result.modifiedCount

        })
    });


    // //---Borrar
    // data_query.forEach(item_data => {
    //     result = db.getSiblingDB(db_name)
    //         .getCollection(coleccion_name)
    //         .remove(
    //             {
    //                 "_id": {
    //                     "$in": item_data.ids
    //                 }
    //             }
    //         );

    //     result_info.push({
    //         database: db_name,
    //         colleccion: coleccion_name,
    //         CRUD: "Remove",
    //         result: result.nRemoved

    //     })
    // });


    data_query.close();

});

//--imprimir resultado
result_info