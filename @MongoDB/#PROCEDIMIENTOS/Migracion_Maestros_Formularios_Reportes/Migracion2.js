// //----Ojo usar desde db local
//
// // DBs
// var db_desde = "cluster";//db1
//
// var db_hacia = "freshteruma"; //db2
// var uid_hacia = ObjectId("604bb7baad63f348bf58a0e2");
// var supervisores_hacia = [
//         ObjectId("607df3fd71890240bc6cc55c")
//     ];
//
// //============= Maestros
//
// var mestros_desde = db.getSiblingDB(db_desde)
//     .getCollection("masters")
//     .aggregate()
//     .allowDiskUse();
//
//
// //--🔄 data
// mestros_desde.forEach(item_maestro => {
//
//     //modificar uid
//     item_maestro.uid = uid_hacia;
//
//     //insertar maestro
//     db.getSiblingDB(db_hacia)
//         .getCollection("masters")
//         .insert(item_maestro);
// });
// // mestros_desde.close();
//
//
// //============= Formularios
//
// var formularios_desde = db.getSiblingDB(db_desde)
//     .getCollection("forms")
//     .aggregate()
//     .allowDiskUse();
//
//
// //--🔄 data
// formularios_desde.forEach(item_formulario => {
//
//     //modificar uid
//     item_formulario.uid = uid_hacia;
//     //modificar supervisors
//     item_formulario.supervisors = supervisores_hacia;
//
//     //crear coleccion
//     db.getSiblingDB(db_hacia).createCollection(item_formulario.anchor).ok;
//
//     //insertar formulario
//     db.getSiblingDB(db_hacia)
//         .getCollection("forms")
//         .insert(item_formulario);
// });
// // formularios_desde.close();
//


//-----------------new

//----Ojo usar desde db local

// DBs
var db_desde = "capacitacion";//db1
var uid_desde = ObjectId("63f5101255e44b2efe76f090");

var db_hacia = "westpak"; //db2
var uid_hacia = ObjectId("64663d5c77665631c0f7bc1a");
var supervisores_hacia = [
        ObjectId("6499f0a786a93763351af9a1")
    ];

// Maestros

var mestros_desde = db.getSiblingDB(db_desde)
    .getCollection("masters")
    .aggregate(
          {
              $match:{
                  uid:uid_desde
              }
          }
        )
    .allowDiskUse();


//--🔄 data
mestros_desde.forEach(item_maestro => {

    //modificar uid
    item_maestro.uid = uid_hacia;

    //insertar maestro
    db.getSiblingDB(db_hacia)
        .getCollection("masters")
        .insert(item_maestro);
});
// mestros_desde.close();




//============= Formularios

var formularios_desde = db.getSiblingDB(db_desde)
    .getCollection("forms")
    .aggregate(
      {
          $match:{
              uid:uid_desde

              ,name:{
                $in:[
                  // "xxxxxx",
                  "Censo de plagas aguacate",
                  "Censo de trampas de aguacate",
                  "Datos base de variables aguacate",
                  "Datos de cosecha en campo aguacate",
                  "Despacho de cosecha aguacate",
                  "Modulo de Enfermedades Aguacate",


                ]
              }

          }
      }
    )
    .allowDiskUse();


//--🔄 data
formularios_desde.forEach(item_formulario => {

    //modificar uid
    item_formulario.uid = uid_hacia;
    //modificar supervisors
    item_formulario.supervisors = supervisores_hacia;

    //crear coleccion
    db.getSiblingDB(db_hacia).createCollection(item_formulario.anchor).ok;

    //insertar formulario
    db.getSiblingDB(db_hacia)
        .getCollection("forms")
        .insert(item_formulario);
});
// formularios_desde.close();
