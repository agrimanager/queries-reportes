//----Ojo usar desde db local

// DBs
var db_desde = "capacitacion";//db1
var uid_desde = ObjectId("63f5101255e44b2efe76f090");

var db_hacia = "westpak"; //db2
var uid_hacia = ObjectId("64663d5c77665631c0f7bc1a");


// Maestros

var mestros_desde = db.getSiblingDB(db_desde)
    .getCollection("masters")
    .aggregate(
        {
            $match:{
                uid:uid_desde
            }
        }
        )
    .allowDiskUse();


//--🔄 data
mestros_desde.forEach(item_maestro => {

    //modificar uid
    item_maestro.uid = uid_hacia;

    //insertar maestro
    db.getSiblingDB(db_hacia)
        .getCollection("masters")
        .insert(item_maestro);
});
// mestros_desde.close();
