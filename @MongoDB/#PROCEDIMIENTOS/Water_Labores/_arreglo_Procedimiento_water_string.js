var data_labores = db.tasks.aggregate(

    {
        $match: {
            "water": { $ne: [] }
        }
    },

    //----arreglo array con mas de 20 elemntos
    {
        "$addFields": {
            "water": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$water" }, "array"] },
                    "then": "$water",
                    "else": { "$map": { "input": { "$objectToArray": "$water" }, "as": "dataKV", "in": "$$dataKV.v" } }
                }
            }
        }
    },


    {
        "$addFields": {
            "water_tipo_variable": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$water",
                        "initialValue": "",
                        "in": { "$type": "$$this" }
                    }
                }, "sin_tipo"]
            }
        }
    },

    {
        $match: {
            "water_tipo_variable": { $eq: "string" }
        }
    },

    {
        $project: {
            "_id": 1,
            "water": 1
        }
    }

);


data_labores.forEach(i => {
    var array_water = []

    i.water.forEach(w => {
        array_water.push(JSON.parse(w))
    });

    //console.log(array_water);

    db.tasks.update(
        {
            _id: i._id
        },
        {
            $set: {
                water: array_water
            }
        }
    )

})
