db.tasks.aggregate(

    {
        $match: {
            "water": { $ne: [] }
        }
    },

    //----arreglo array con mas de 20 elemntos
    {
        "$addFields": {
            "water": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$water" }, "array"] },
                    "then": "$water",
                    "else": { "$map": { "input": { "$objectToArray": "$water" }, "as": "dataKV", "in": "$$dataKV.v" } }
                }
            }
        }
    },


    {
        "$addFields": {
            "water_tipo_variable": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$water",
                        "initialValue": "",
                        "in": { "$type": "$$this" }
                    }
                }, "sin_tipo"]
            }
        }
    },
    
    // //----test
    // {
    //     $group: {
    //         _id: "$water_tipo_variable"
    //         ,cantidad :{$sum:1}
    //     }
    // }
    
    
    {
        $match: {
            "water_tipo_variable": { $eq: "string" }
        }
    },
    
    
    
    

)