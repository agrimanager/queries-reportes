//----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- "rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0,
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        ,{
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte


        //----->>>VARIABLES_QUE_NO_SE_USAN
        {
            "$project": {

                "user": 0,
                "Keys": 0,
                "Finca nombre": 0,
                "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
                "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,

                "Formula": 0,
                "uDate": 0,
                "Busqueda inicio": 0,
                "Busqueda fin": 0,
                // "today": 0,
                "FincaID": 0,
                "uid": 0

                , "Point": 0
                , "capture": 0

                , "_id": 0
            }
        },
