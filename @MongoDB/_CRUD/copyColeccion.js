



//version1
// db.<collection_name>.find().forEach(function(d){ db.getSiblingDB('<new_database>')['<collection_name>'].insert(d); });
//db.dashboard.find().forEach(function(d){ db.getSiblingDB('finca_alejo')['dashboard'].insert(d); });


//version2
//---variables nombres de DB
var db_origen = "agricolapersea";
var db_destino = "finca";

var coleccion_name = "COLECCION";

//---clonar coleccion desde db_origen hacia db_destino
db.getSiblingDB(db_origen).getCollection(coleccion_name).find()
    .forEach(function(d) {
        db.getSiblingDB(db_destino).getCollection(coleccion_name).insert(d);
        // db.getSiblingDB(db_destino)[coleccion_name].insert(d);
    });
