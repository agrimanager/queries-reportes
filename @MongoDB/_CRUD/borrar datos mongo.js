

--productos y transacciones

db.suppliesTimeline.deleteMany({});
db.supplies.deleteMany({});
db.supplies.updateMany(		{},      { $set: { "quantity" : 0 } }   );


//--cartografia

db.farms.deleteMany({});
db.cartography.deleteMany({});
db.weather.deleteMany({});


//--formularios

db.forms.remove({})
db.masters.remove({})
db.reports.remove({})


db.form_xx.drop()
db.form_modulodetrampas.drop()
db.form_modulodesensores.drop()
db.form_modulodepolinizacion.drop()
db.form_modulodeplagas.drop()
db.form_modulodeplagas.drop()
db.form_modulodeenfermedades.drop()
db.form_modulodenfermedades.drop()
db.form_modulodemedidasvegetativas.drop()
db.form_modulodeaspersores.drop()
db.form_modulodehumedad.drop()
db.form_modulodefenologia.drop()
db.form_modulodecosecha.drop()
db.form_modulodecosechapalmas.drop()
db.form_modulodecosechafrutales.drop()
db.form_modulodeproduccion.drop()
db.form_modulodeproduccionpalmas.drop()
db.form_modulodeproduccionfrutales.drop()


db.form_detallemedidavegetativa.drop()
db.form_detallehumedad.drop()
db.form_detallecensotrampas.drop()
db.form_detallecensosensor.drop()
db.form_detallecensoproduccion.drop()
db.form_detallecensopolinizacion.drop()
db.form_detallecensoplaga.drop()
db.form_detallecensoenfermedad.drop()
db.form_detallecensocosecha.drop()
db.form_detalleaspersor.drop()




// Ejemplo DELETE (muchos)
db.cartography_oleo_caribe.remove(
    // where
    {
        "properties.name": {
            $in: [
                "5-3-U5-1-","5-4-U42-1","5-4-U42-1-1","5-4-U42-1-2","5-4-U42-1-3","5-4-U42-1-4","5-4-U42-1-5","5-4-U42-1-6","5-4-U42-1-7","5-4-U42-1-8","5-5-U14-1","5-5-U14-1-1","6-4-U11-10","6-4-U11-10-1","8-10-U8"
                ]
        }
    }
)