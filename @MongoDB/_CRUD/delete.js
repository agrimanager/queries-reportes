

// Ejemplo DELETE (muchos)
db.weather.remove(
		// where
		{name: "Quimbaya"}
	)

//--cartografia
db.farms.deleteMany({});
db.cartography.deleteMany({});
db.weather.deleteMany({});

//--formularios
db.forms.remove({})
db.masters.remove({})
db.reports.remove({})



// Ejemplo DELETE (muchos)
db.cartography_oleo_caribe.remove(
    // where
    {
        "properties.name": {
            $in: [
                "5-3-U5-1-","5-4-U42-1","5-4-U42-1-1","5-4-U42-1-2","5-4-U42-1-3","5-4-U42-1-4","5-4-U42-1-5","5-4-U42-1-6","5-4-U42-1-7","5-4-U42-1-8","5-5-U14-1","5-5-U14-1-1","6-4-U11-10","6-4-U11-10-1","8-10-U8"
                ]
        }
    }
)