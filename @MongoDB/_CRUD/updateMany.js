
// Ejemplo UPDATE (muchos)
db.myColl.updateMany(
   { category: "cafe" },
   { $set: { status: "Updated" } },
);

// Ejemplo Arrays de objetos UPDATE (muchos)
db.tasks.update(
  { "productivityReport.quantity":-2147483648 },
  { "$set": { "productivityReport.$[elem].quantity": 0 } },
  { "arrayFilters": [{ "elem.quantity": -2147483648 }], "multi": true }
)

// Ejemplo UPDATE (muchos)
db.weather.updateMany({ name: "Quimbaya" },{ $set: { fid: ObjectId("5d2e50f2e3bc05f994d4282b") }},);

// Ejemplo UPDATE (muchos)
db.weather.updateMany(
    // where
    { name: "Quimbaya" },
    // set
    { $set:
        { fid: ObjectId("5d2e50f2e3bc05f994d4282b") }
    },
);

// Ejemplo Agregar elementos a array
db.ciudades.update({ciudad:"Zaragoza"},{$push:{monumentos:"Basílica del Pilar"}})
db.cartography.update({_id:ObjectId("5d56fb6df874d9ee520f94af")},{$push:{children:ObjectId("5d56fb6df874d9ee520f94af")}})
var fincas = db.farms.find({});

fincas.forEach((item_finca) => {

    db.farms.update(
        { _id: item_finca._id },
        {
            $push: {
                //---blocks
                "defaultProperties.blocks": {
                    $each: [
                        //lotes
                        {
                            "name": "num_lotes",
                            "type": "number"
                        },
                        //lineas
                        {
                            "name": "num_lineas",
                            "type": "number"
                        },
                        //arboles
                        {
                            "name": "num_arboles",
                            "type": "number"
                        }
                    ]
                },
                //---lot
                "defaultProperties.lot": {
                    $each: [
                        //lineas
                        {
                            "name": "num_lineas",
                            "type": "number"
                        },
                        //arboles
                        {
                            "name": "num_arboles",
                            "type": "number"
                        }
                    ]
                },
                //---lines
                "defaultProperties.lines": {
                    $each: [
                        //arboles
                        {
                            "name": "num_arboles",
                            "type": "number"
                        }
                    ]
                }
            }
        }
    )
});




// Borrar columna
db.form_cosechafrutapalmadeaceite.update( {"Racimos Recolector 3":{$exists:true}}, { $unset: { "Racimos Recolector 1": 1 } }, false, true);


db.cartography.update(
    {
        "properties.custom.Mes de siembra": { $exists: true }

    }
    ,
    {
        $unset: {
            "properties.custom.Mes de siembra": 1
        }

    }, false, true
);

//renombrar columna
db.form_abc.updateMany(
    {}
    ,
    {
        $rename: {
            //"campo_viejo":"campo_nuevo"
            "A": "Z"
        }
    }
)


// agregar campos custom a cartografia


db.cartography.updateMany(
    {
        "properties.type": "sensors"
    },
    {
        $set: {
            "properties.custom": {
                "Tipo": {
                    "type": "string",
                    "value": "Freatimetro"
                },
                "Nombre": {
                    "type": "string",
                    "value": ""
                },
                "color": {
                    "type": "color",
                    "value": "#FF9191"
                }
            }
        }
    }
)
