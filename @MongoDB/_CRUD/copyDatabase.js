

//version1
//db.copyDatabase("lukeragricola","lukeragricola_backup_2020-07-15")


//version2
//---variables nombres de DB
var db_origen = "agricolapersea";
var db_destino = "finca";

//---borrar db_destino
db.getSiblingDB(db_destino).dropDatabase();

//---clonar desde db_origen hacia db_destino
db.copyDatabase(db_origen, db_destino);
