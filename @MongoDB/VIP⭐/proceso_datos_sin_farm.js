
//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["ganaderialapalma"];


var bases_de_datos_lista_negra = [


    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate([

                {
                    $match: {
                        "Point.farm": {
                            $exists: false
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            $and: [
                                { "$ne": ["$Point", ""] },
                                //{ "$exists": ["$Point.farm", false] },
                                { "$ne": [{ "$type": "$Point.farm" }, "string"] },
                                { "$ne": ["$Point.farm", ""] }
                            ]
                        }
                    }
                },

                {
                    "$project": {
                        "data_array": { "$objectToArray": "$$ROOT" },
                        "data": "$$ROOT"
                    }
                },




                // encuentra el elemento con un path unico para ellos
                {
                    "$addFields": {
                        "cartografia": {
                            "$filter": {
                                "input": "$data_array",
                                "as": "item",
                                "cond": {
                                    "$eq": [
                                        { "$arrayElemAt": ["$$item.v.features.type", 0] },
                                        "Feature"
                                    ]
                                }
                            }
                        }
                    }
                },
                // catografia.k = nombre del elemento catografico
                // cartografia.v = objeto del elemeto cartografico

                {
                    "$project": {
                        "data_array": 0
                    }
                },


                // nombra el elemnto de cultivo con un nombre estandar
                {
                    "$addFields": {
                        "elem_cartografia": { "$arrayElemAt": ["$cartografia.v", 0] }
                    }
                },


                // se genra el documenot inicial con un elemento de cultivo con nombre estandar
                {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data",
                                { "elem_cartografia": "$elem_cartografia" }
                            ]
                        }
                    }
                },


                // // verificacion de existencia de elemento cartografico
                {
                    "$match": {
                        "elem_cartografia": { "$exists": true }
                    }
                },


                // extraccion del farm_id desde la cartografia
                {
                    "$addFields": {
                        "cartografia_path": {
                            "$arrayElemAt": [
                                "$elem_cartografia.features.path",
                                0
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "farm_id": {
                            "$arrayElemAt": [
                                {
                                    "$split": [
                                        { "$trim": { "input": "$cartografia_path", "chars": "," } },
                                        ","
                                    ]
                                },
                                0
                            ]
                        }
                    }
                },

                {
                    "$project": {
                        "_id": 1,
                        "farm_id": 1
                    }
                }



            ]
            )
            .allowDiskUse();




        //--🔄 data
        data_query.forEach(item_data => {

            //console.log(item_data)

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .update(
                    {
                        "_id": item_data._id
                    },
                    {
                        $set: {
                            "Point.farm": item_data.farm_id
                        }
                    }
                );

            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                cantidad: result.nModified

            })
        });
        data_query.close();





    });



});

//--imprimir resultado
result_info
