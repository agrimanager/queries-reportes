
{
    "$addFields": {
        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
    }
},
{
    "$addFields": {
        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
    }
},
{
    "$addFields": {
        "split_path_oid": {
            "$concatArrays": [
                "$split_path_padres_oid",
                ["$_id"]
            ]
        }
    }
},

{
    "$lookup": {
        "from": "cartography",
        "localField": "split_path_oid",
        "foreignField": "_id",
        "as": "objetos_del_cultivo"
    }
},


{
    "$addFields": {
        "finca": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
            }
        }
    }
},
{
    "$unwind": {
        "path": "$finca",
        "preserveNullAndEmptyArrays": true
    }
},
{
    "$lookup": {
        "from": "farms",
        "localField": "finca._id",
        "foreignField": "_id",
        "as": "finca"
    }
},
{ "$unwind": "$finca" },

{ "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


{
    "$addFields": {
        "bloque": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
            }
        }
    }
},
{
    "$unwind": {
        "path": "$bloque",
        "preserveNullAndEmptyArrays": true
    }
},
{ "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

{
    "$addFields": {
        "lote": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
            }
        }
    }
},
{
    "$unwind": {
        "path": "$lote",
        "preserveNullAndEmptyArrays": true
    }
},
{ "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

{
    "$addFields": {
        "linea": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
            }
        }
    }
},
{
    "$unwind": {
        "path": "$linea",
        "preserveNullAndEmptyArrays": true
    }
},
{ "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


{
    "$addFields": {
        "arbol": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
            }
        }
    }
},
{
    "$unwind": {
        "path": "$arbol",
        "preserveNullAndEmptyArrays": true
    }
},
{ "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




{
    "$project": {
        "variable_cartografia": 0,
        "split_path_padres": 0,
        "split_path_padres_oid": 0,
        "variable_cartografia_oid": 0,
        "split_path_oid": 0,
        "objetos_del_cultivo": 0,
    }
}
