var data = db.tasks.aggregate(

    [


        {
            "$match": {
                "$and": [
                    {
                        "rgDate": {
                            //"$gte": "2020-01-01T21:10:17.000Z"
                            //"$gte": {"$toDate":"2020-01-01T21:10:17.000Z"}
                            "$gte": ISODate("2022-10-01T11:44:17.117-05:00")
                        }
                    },
                    {
                        "rgDate": {
                            //"$lte": {"$toDate":"2020-02-20T21:10:17.000Z"}
                            "$lte": ISODate("2022-11-01T11:44:17.117-05:00")
                        }
                    }
                ]
            }
        },


        {
            "$group": {
                "_id": {
                    "$mergeObjects": [
                        "$$ROOT",
                        {
                            //--excluir variables
                            "_id": null
                            , "cod": null
                            // , "when": null
                            , "rgDate": null
                            , "uDate": null
                        }
                    ]
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        },
        {
            "$match": {
                "$expr": {
                    "$gt": [{ "$size": "$data" }, 1]
                }
            }
        },
        // {
        //     "$project": {
        //         "_id": 0,
        //         "ids": {
        //             "$map": {
        //                 "input": {
        //                     "$slice": [
        //                         "$data",
        //                         { "$subtract": [{ "$size": "$data" }, 1] }
        //                     ]
        //                 },
        //                 "as": "item",
        //                 "in": "$$item._id"
        //             }
        //         }
        //     }
        // },
        // {
        //     "$group": {
        //         "_id": null,
        //         "ids": {
        //             "$push": "$ids"
        //         }
        //     }
        // },
        // {
        //     "$project": {
        //         "_id": 0,
        //         "ids": {
        //             "$reduce": {
        //                 "input": "$ids",
        //                 "initialValue": [],
        //                 "in": { "$concatArrays": ["$$value", "$$this"] }
        //             }
        //         }
        //     }
        // }
    ]

)

data

// data.forEach(item => {
//     db.tasks.remove(
//         {
//             "_id": {
//                 "$in": item.ids
//             }
//         }
//     )
// })
