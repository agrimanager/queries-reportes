const params = {
    filepath: "C:\\Users\\Alejandro.Villa\\Documents\\#Reporsitorios Agrimanager\\queries-reportes\\RioPaila\\_INTERPOLACION\\Produccion\\riopaila.form_censodeproduccion.csv",
    connection: "IBM Agrolevels",
    db: "riopaila",
    type: "csv",
    fields: [],
    batchSize: 2000,
    delimiter: ",",
    //Use the transformer to customize the export result
    //transformer:(doc)=>{ //async (doc)=>{
    //   doc["field"]= "new value";
    //   return doc; //return null skips this doc
    //},
};
params.queryScript = `db.getCollection("form_censodeproduccion").aggregate(
 [
	{
		"$addFields" : {
			"Busqueda inicio" : ISODate("2021-01-01T16:44:17.117Z"),
			"Busqueda fin" : ISODate("2021-01-05T16:44:17.117Z"),
			"today" : ISODate("2021-05-07T01:16:35.086Z")
		}
	},
	{
		"$match" : {
			"$expr" : {
				"$and" : [
					{
						"$gte" : [
							{
								"$toDate" : {
									"$dateToString" : {
										"format" : "%Y-%m-%d",
										"date" : "$rgDate"
									}
								}
							},
							{
								"$toDate" : {
									"$dateToString" : {
										"format" : "%Y-%m-%d",
										"date" : "$Busqueda inicio"
									}
								}
							}
						]
					},
					{
						"$lte" : [
							{
								"$toDate" : {
									"$dateToString" : {
										"format" : "%Y-%m-%d",
										"date" : "$rgDate"
									}
								}
							},
							{
								"$toDate" : {
									"$dateToString" : {
										"format" : "%Y-%m-%d",
										"date" : "$Busqueda fin"
									}
								}
							}
						]
					}
				]
			}
		}
	},
	{
		"$addFields" : {
			"variable_cartografia" : "$Palma"
		}
	},
	{
		"$unwind" : "$variable_cartografia.features"
	},
	{
		"$addFields" : {
			"split_path_padres" : {
				"$split" : [
					{
						"$trim" : {
							"input" : "$variable_cartografia.path",
							"chars" : ","
						}
					},
					","
				]
			}
		}
	},
	{
		"$addFields" : {
			"split_path_padres_oid" : {
				"$map" : {
					"input" : "$split_path_padres",
					"as" : "strid",
					"in" : {
						"$toObjectId" : "$$strid"
					}
				}
			}
		}
	},
	{
		"$addFields" : {
			"variable_cartografia_oid" : [
				{
					"$toObjectId" : "$variable_cartografia.features._id"
				}
			]
		}
	},
	{
		"$addFields" : {
			"split_path_oid" : {
				"$concatArrays" : [
					"$split_path_padres_oid",
					"$variable_cartografia_oid"
				]
			}
		}
	},
	{
		"$lookup" : {
			"from" : "cartography",
			"localField" : "split_path_oid",
			"foreignField" : "_id",
			"as" : "objetos_del_cultivo"
		}
	},
	{
		"$addFields" : {
			"tiene_variable_cartografia" : {
				"$cond" : {
					"if" : {
						"$eq" : [
							{
								"$size" : "$split_path_oid"
							},
							{
								"$size" : "$objetos_del_cultivo"
							}
						]
					},
					"then" : "si",
					"else" : "no"
				}
			}
		}
	},
	{
		"$addFields" : {
			"objetos_del_cultivo" : {
				"$cond" : {
					"if" : {
						"$eq" : [
							"$tiene_variable_cartografia",
							"si"
						]
					},
					"then" : "$objetos_del_cultivo",
					"else" : {
						"$concatArrays" : [
							"$objetos_del_cultivo",
							[
								"$variable_cartografia.features"
							]
						]
					}
				}
			}
		}
	},
	{
		"$addFields" : {
			"Finca" : {
				"$filter" : {
					"input" : "$objetos_del_cultivo",
					"as" : "item_cartografia",
					"cond" : {
						"$eq" : [
							"$$item_cartografia.type",
							"Farm"
						]
					}
				}
			}
		}
	},
	{
		"$unwind" : {
			"path" : "$Finca",
			"preserveNullAndEmptyArrays" : true
		}
	},
	{
		"$lookup" : {
			"from" : "farms",
			"localField" : "Finca._id",
			"foreignField" : "_id",
			"as" : "Finca"
		}
	},
	{
		"$unwind" : "$Finca"
	},
	{
		"$addFields" : {
			"Finca" : {
				"$ifNull" : [
					"$Finca.name",
					"no existe"
				]
			}
		}
	},
	{
		"$addFields" : {
			"Bloque" : {
				"$filter" : {
					"input" : "$objetos_del_cultivo",
					"as" : "item_cartografia",
					"cond" : {
						"$eq" : [
							"$$item_cartografia.properties.type",
							"blocks"
						]
					}
				}
			}
		}
	},
	{
		"$unwind" : {
			"path" : "$Bloque",
			"preserveNullAndEmptyArrays" : true
		}
	},
	{
		"$addFields" : {
			"Bloque" : {
				"$ifNull" : [
					"$Bloque.properties.name",
					"no existe"
				]
			}
		}
	},
	{
		"$addFields" : {
			"Lote" : {
				"$filter" : {
					"input" : "$objetos_del_cultivo",
					"as" : "item_cartografia",
					"cond" : {
						"$eq" : [
							"$$item_cartografia.properties.type",
							"lot"
						]
					}
				}
			}
		}
	},
	{
		"$unwind" : {
			"path" : "$Lote",
			"preserveNullAndEmptyArrays" : true
		}
	},
	{
		"$addFields" : {
			"Lote" : {
				"$ifNull" : [
					"$Lote.properties.name",
					"no existe"
				]
			}
		}
	},
	{
		"$addFields" : {
			"Linea" : {
				"$filter" : {
					"input" : "$objetos_del_cultivo",
					"as" : "item_cartografia",
					"cond" : {
						"$eq" : [
							"$$item_cartografia.properties.type",
							"lines"
						]
					}
				}
			}
		}
	},
	{
		"$unwind" : {
			"path" : "$Linea",
			"preserveNullAndEmptyArrays" : true
		}
	},
	{
		"$addFields" : {
			"Linea" : {
				"$ifNull" : [
					"$Linea.properties.name",
					"no existe"
				]
			}
		}
	},
	{
		"$addFields" : {
			"Planta" : {
				"$filter" : {
					"input" : "$objetos_del_cultivo",
					"as" : "item_cartografia",
					"cond" : {
						"$eq" : [
							"$$item_cartografia.properties.type",
							"trees"
						]
					}
				}
			}
		}
	},
	{
		"$unwind" : {
			"path" : "$Planta",
			"preserveNullAndEmptyArrays" : true
		}
	},
	{
		"$addFields" : {
			"lon" : {
				"$arrayElemAt" : [
					"$Planta.geometry.coordinates",
					0
				]
			},
			"lat" : {
				"$arrayElemAt" : [
					"$Planta.geometry.coordinates",
					1
				]
			}
		}
	},
	{
		"$addFields" : {
			"Planta" : {
				"$ifNull" : [
					"$Planta.properties.name",
					"no existe"
				]
			}
		}
	},
	{
		"$project" : {
			"variable_cartografia" : 0,
			"split_path_padres" : 0,
			"split_path_padres_oid" : 0,
			"variable_cartografia_oid" : 0,
			"split_path_oid" : 0,
			"objetos_del_cultivo" : 0,
			"tiene_variable_cartografia" : 0,
			"Palma" : 0,
			"Formula" : 0,
			"Point" : 0,
			"Produccion" : 0,
			"uid" : 0,
			"supervisor" : 0,
			"uDate" : 0
		}
	},
	{
		"$project" : {
			"cartografia" : "$Planta",
			"longitud" : "$lon",
			"latitud" : "$lat",
			"peso" : {
				"$toDouble" : "$Numero de Racimos verdes"
			},
			"rgDate" : "$rgDate",
			"Busqueda inicio" : "$Busqueda inicio",
			"Busqueda fin" : "$Busqueda fin"
		}
	},
	{
		"$lookup" : {
			"from" : "form_censodeproduccion",
			"as" : "data",
			"let" : {
				"filtro_fecha_inicio" : "$Busqueda inicio",
				"filtro_fecha_fin" : "$Busqueda fin",
				"fecha" : "$rgDate",
				"cartografia" : "$cartografia"
			},
			"pipeline" : [
				{
					"$match" : {
						"$expr" : {
							"$and" : [
								{
									"$gte" : [
										{
											"$toDate" : {
												"$dateToString" : {
													"format" : "%Y-%m-%d",
													"date" : "$rgDate"
												}
											}
										},
										{
											"$toDate" : {
												"$dateToString" : {
													"format" : "%Y-%m-%d",
													"date" : "$$filtro_fecha_inicio"
												}
											}
										}
									]
								},
								{
									"$lte" : [
										{
											"$toDate" : {
												"$dateToString" : {
													"format" : "%Y-%m-%d",
													"date" : "$rgDate"
												}
											}
										},
										{
											"$toDate" : {
												"$dateToString" : {
													"format" : "%Y-%m-%d",
													"date" : "$$filtro_fecha_fin"
												}
											}
										}
									]
								}
							]
						}
					}
				},
				{
					"$addFields" : {
						"variable_cartografia" : "$Palma"
					}
				},
				{
					"$unwind" : "$variable_cartografia.features"
				},
				{
					"$addFields" : {
						"split_path_padres" : {
							"$split" : [
								{
									"$trim" : {
										"input" : "$variable_cartografia.path",
										"chars" : ","
									}
								},
								","
							]
						}
					}
				},
				{
					"$addFields" : {
						"split_path_padres_oid" : {
							"$map" : {
								"input" : "$split_path_padres",
								"as" : "strid",
								"in" : {
									"$toObjectId" : "$$strid"
								}
							}
						}
					}
				},
				{
					"$addFields" : {
						"variable_cartografia_oid" : [
							{
								"$toObjectId" : "$variable_cartografia.features._id"
							}
						]
					}
				},
				{
					"$addFields" : {
						"split_path_oid" : {
							"$concatArrays" : [
								"$split_path_padres_oid",
								"$variable_cartografia_oid"
							]
						}
					}
				},
				{
					"$lookup" : {
						"from" : "cartography",
						"localField" : "split_path_oid",
						"foreignField" : "_id",
						"as" : "objetos_del_cultivo"
					}
				},
				{
					"$addFields" : {
						"tiene_variable_cartografia" : {
							"$cond" : {
								"if" : {
									"$eq" : [
										{
											"$size" : "$split_path_oid"
										},
										{
											"$size" : "$objetos_del_cultivo"
										}
									]
								},
								"then" : "si",
								"else" : "no"
							}
						}
					}
				},
				{
					"$addFields" : {
						"objetos_del_cultivo" : {
							"$cond" : {
								"if" : {
									"$eq" : [
										"$tiene_variable_cartografia",
										"si"
									]
								},
								"then" : "$objetos_del_cultivo",
								"else" : {
									"$concatArrays" : [
										"$objetos_del_cultivo",
										[
											"$variable_cartografia.features"
										]
									]
								}
							}
						}
					}
				},
				{
					"$addFields" : {
						"Finca" : {
							"$filter" : {
								"input" : "$objetos_del_cultivo",
								"as" : "item_cartografia",
								"cond" : {
									"$eq" : [
										"$$item_cartografia.type",
										"Farm"
									]
								}
							}
						}
					}
				},
				{
					"$unwind" : {
						"path" : "$Finca",
						"preserveNullAndEmptyArrays" : true
					}
				},
				{
					"$lookup" : {
						"from" : "farms",
						"localField" : "Finca._id",
						"foreignField" : "_id",
						"as" : "Finca"
					}
				},
				{
					"$unwind" : "$Finca"
				},
				{
					"$addFields" : {
						"Finca" : {
							"$ifNull" : [
								"$Finca.name",
								"no existe"
							]
						}
					}
				},
				{
					"$addFields" : {
						"Bloque" : {
							"$filter" : {
								"input" : "$objetos_del_cultivo",
								"as" : "item_cartografia",
								"cond" : {
									"$eq" : [
										"$$item_cartografia.properties.type",
										"blocks"
									]
								}
							}
						}
					}
				},
				{
					"$unwind" : {
						"path" : "$Bloque",
						"preserveNullAndEmptyArrays" : true
					}
				},
				{
					"$addFields" : {
						"Bloque" : {
							"$ifNull" : [
								"$Bloque.properties.name",
								"no existe"
							]
						}
					}
				},
				{
					"$addFields" : {
						"Lote" : {
							"$filter" : {
								"input" : "$objetos_del_cultivo",
								"as" : "item_cartografia",
								"cond" : {
									"$eq" : [
										"$$item_cartografia.properties.type",
										"lot"
									]
								}
							}
						}
					}
				},
				{
					"$unwind" : {
						"path" : "$Lote",
						"preserveNullAndEmptyArrays" : true
					}
				},
				{
					"$addFields" : {
						"Lote" : {
							"$ifNull" : [
								"$Lote.properties.name",
								"no existe"
							]
						}
					}
				},
				{
					"$addFields" : {
						"Linea" : {
							"$filter" : {
								"input" : "$objetos_del_cultivo",
								"as" : "item_cartografia",
								"cond" : {
									"$eq" : [
										"$$item_cartografia.properties.type",
										"lines"
									]
								}
							}
						}
					}
				},
				{
					"$unwind" : {
						"path" : "$Linea",
						"preserveNullAndEmptyArrays" : true
					}
				},
				{
					"$addFields" : {
						"Linea" : {
							"$ifNull" : [
								"$Linea.properties.name",
								"no existe"
							]
						}
					}
				},
				{
					"$addFields" : {
						"Planta" : {
							"$filter" : {
								"input" : "$objetos_del_cultivo",
								"as" : "item_cartografia",
								"cond" : {
									"$eq" : [
										"$$item_cartografia.properties.type",
										"trees"
									]
								}
							}
						}
					}
				},
				{
					"$unwind" : {
						"path" : "$Planta",
						"preserveNullAndEmptyArrays" : true
					}
				},
				{
					"$addFields" : {
						"lon" : {
							"$arrayElemAt" : [
								"$Planta.geometry.coordinates",
								0
							]
						},
						"lat" : {
							"$arrayElemAt" : [
								"$Planta.geometry.coordinates",
								1
							]
						}
					}
				},
				{
					"$addFields" : {
						"Planta" : {
							"$ifNull" : [
								"$Planta.properties.name",
								"no existe"
							]
						}
					}
				},
				{
					"$project" : {
						"variable_cartografia" : 0,
						"split_path_padres" : 0,
						"split_path_padres_oid" : 0,
						"variable_cartografia_oid" : 0,
						"split_path_oid" : 0,
						"objetos_del_cultivo" : 0,
						"tiene_variable_cartografia" : 0,
						"Palma" : 0,
						"Formula" : 0,
						"Point" : 0,
						"uid" : 0,
						"uDate" : 0
					}
				},
				{
					"$project" : {
						"cartografia" : "$Planta",
						"longitud" : "$lon",
						"latitud" : "$lat",
						"peso" : {
							"$toDouble" : "$Numero de Racimos verdes"
						},
						"rgDate" : "$rgDate"
					}
				},
				{
					"$match" : {
						"$expr" : {
							"$lte" : [
								"$rgDate",
								"$$fecha"
							]
						}
					}
				},
				{
					"$match" : {
						"$expr" : {
							"$ne" : [
								"$cartografia",
								"$$cartografia"
							]
						}
					}
				}
			]
		}
	},
	{
		"$unwind" : {
			"path" : "$data",
			"preserveNullAndEmptyArrays" : false
		}
	}
],
 { "allowDiskUse" : true })`;

mb.exportQueryToFile(params);

sleep(100);
mb.openFolder(require("path").dirname(params.filepath));
