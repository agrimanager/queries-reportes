import * as path from "path";
import * as fs from "fs";
const promisify = require("bluebird").promisify;
const appendFileAsync = promisify(fs.appendFile);


const BATCH_SIZE = 2000;

const connection = "IBM Agrolevels";
const db = "cluster";
let targetPath = "C:\\Users\\Alejandro.Villa\\Documents";

if (!fs.existsSync(targetPath))
    require("mkdirp").sync(targetPath);

let exportCollections = [
    { collection: "form_registrodeprecipitacion", query: {}, projection: {}, sort: {}, skip: 0, limit: 0, filename: "cluster.form_registrodeprecipitacion.csv", delimiter: ",", fields: [] }
];
let totalDocs = 0;
let collectionResult = {};//collectionResult:{[name:string]:number}

function exportCollection(collectionParams) {
    let { collection, filename, query, projection, sort, skip, limit, fields, delimiter } = collectionParams;

    let filepath = path.resolve(targetPath, mb.sanitizeFile(filename || (db + "." + collection + ".csv")));

    console.log(`export docs from ${connection}:${db}:${collection} to ${filepath} start...`);

    if (fs.existsSync(filepath))
        fs.unlinkSync(filepath);

    if (!_.isEmpty(fields)) {
        projection = {};
        fields.forEach(field => {
            projection[field] = 1;
        })
    } else {
        fields = mb.tryGetFields({ connection, db, collection });
    }

    collectionResult[collection] = 0;

    let cursor = mb.getCursorFromCollection({ connection, db, collection, sort, skip, limit, query, projection });
    let isFirstRead = true;

    const totalCount = cursor.size ? cursor.size() : cursor.count();
    const appendFilePromises = [];

    await(mb.batchReadFromCursor(cursor, BATCH_SIZE, (docs) => {
        return async(() => {
            let readLength = docs.length;
            if (!readLength) {
                return;
            }

            let csvContent = mb.docsToCSV({ docs, fields, delimiter, withColumnTitle: isFirstRead });
            isFirstRead = false;
            appendFilePromises.push(appendFileAsync(filepath, csvContent));
            collectionResult[collection] += readLength;
            const percent = (collectionResult[collection] / totalCount * 100).toFixed(1);
            console.log(`${percent}%	 ${collectionResult[collection]}/${totalCount} docs exported to "${path.basename(filepath)}".`);
            totalDocs += docs.length;
        })();
    }));

    await(Promise.all(appendFilePromises));
    sleep(100);
    console.log(`export ${collectionResult[collection]} docs from ${connection}:${db}:${collection} to ${filepath} finished.`);
}

exportCollections.forEach(it => exportCollection(it));
_.delay(() => mb.openFolder(targetPath), 1000);

if (exportCollections.length > 1)
    console.log(`Total ${totalDocs} document(s) of ${exportCollections.length} collections successfully exported.`, collectionResult);
