//----Querys de cuentas contables


// costos admin
db.costs.aggregate(
    {"$unwind": "$information"},
    {
        $project: {
            _id :0,
            "CUC": "$cuc",
            "Tipo de costo": "Costo Administrativo",
            "Nombre": "$name",
            " ($) Costo Inventario": {$toDouble: 0},
            " ($) Costo Inventario Labores": {$toDouble: 0},
            " ($) Costo Mano de Obra": {$toDouble: 0},
            " ($) Costo Administrativo": {$toDouble: "$information.amount"}
        }
    }    
)


// costos de inventario
db.supplies.aggregate(
    //-- JOIN Transacciones
    {
        $lookup: {
            from: "suppliesTimeline",
            localField: "_id",
            foreignField: "sid",
            as: "transacciones_productos"
        }
    },
    { $unwind: "$transacciones_productos" },
    // obtener solo los ingresos
    { $match: { "transacciones_productos.quantity": { $gt: 0 } }},
    // obtener solo los ingresos que no son tranferencias
    { $match: { "transacciones_productos.transfer": false }},

    {
        $project: {
            _id: 0,
            "CUC": "$puc",
            "Tipo de costo": "Inventario",
            "Nombre": "$name",
            " ($) Costo Inventario": { $toDouble: "$transacciones_productos.total" },
            " ($) Costo Inventario Labores": { $toDouble: 0 },
            " ($) Costo Mano de Obra": { $toDouble: 0 },
            " ($) Costo Administrativo": { $toDouble: 0 }
        }
    }
)



// costos de mano de obra
db.activities.aggregate(
    // //-- JOIN Labores
    {
        $lookup: {
            from: "tasks",
            localField: "_id",
            foreignField: "activity",
            as: "labores_actividad"
        }
    },
    { "$unwind": "$labores_actividad" },
    // obtener labores terminadas
    { $match: { "labores_actividad.status": "Done" } },

    //-- calcular costo de Labor
    {
        "$addFields":
            {
                costo_actividad: {
                    $multiply: [
                        "$labores_actividad.productivityPrice.expectedValue",
                        "$labores_actividad.productivityPrice.price"
                    ],
                },
            }
    },

    {
        $project: {
            _id: 0,
            "CUC": "$cuc",
            "Tipo de costo": "Labores",
            "Nombre": "$name",
            " ($) Costo Inventario": { $toDouble: 0 },
            " ($) Costo Inventario Labores": { $toDouble: "$labores_actividad.totalSupplies" },
            " ($) Costo Mano de Obra": { $toDouble: "$costo_actividad" },
            " ($) Costo Administrativo": { $toDouble: 0 }
        }
    }
)





//COSTOS DE MANO DE OBRA (nOMINA DE EMPLEADOS
db.employees.aggregate(
    //-- JOIN companies
    {
        $lookup: {
            from: "companies",
            localField: "cid",
            foreignField: "_id",
            as: "companyEmployee"
        }
    }
    , { "$unwind": "$companyEmployee" }

    //-- JOIN farms
    , {
        $lookup: {
            from: "farms",
            localField: "fids",
            foreignField: "_id",
            as: "farmsEmployee"
        }
    }


    //-- calcular Total salario
    , {
        "$addFields":
            {
                salaryPartial: {
                    $add: [
                        "$salary",
                        "$otherSalaryIncome",
                    ],
                },
            }
    }

    //-- calcular Porcentaje 
    , {
        "$addFields":
            {
                salaryPercent: {
                    $divide: [
                        "$companyEmployee.compensatoryMeasuresPercent",
                        100
                    ],
                },
            }
    }

    //-- calcular costo de Porcentaje 
    , {
        "$addFields":
            {
                salaryPercentCost: {
                    $multiply: [
                        "$salaryPartial",
                        "$salaryPercent"
                    ],
                },
            }
    }


    //-- SELECT valores
    , {
        "$project": {
            _id: 0,
            "Nombre": "$firstName",
            "Apellido": "$lastName",
            "Identificacion": "$numberID",
            //"Tipo identificacion": "$typeID",
            "Cargo": "$job",
            "Fecha de nacimiento": "$birthDate",
            "Direccion": "$homeAddress",
            "Celular": "$cellphone",
            "Estado": { $cond: { if: { $eq: ["$status", true] }, then: "Activo", else: "Inactivo" } },

            "Tipo de contrato": "$contractType",//TRADUCIR
            "Fecha inicio contrato": "$hireDate",
            "Fecha fin contrato": "$contractTerminationDate",
            "Periodicidad del salario": "$periodicity", //TRADUCIR

            // SALARIO
            "Empresa": "$companyEmployee.name",
            "Empresa CUC Mano obra": "$companyEmployee.puc_employees",
            "Empresa CUC Inventario": "$companyEmployee.puc_inventary",
            "Factor prestacional (%)": "$companyEmployee.compensatoryMeasuresPercent",
            // Calcular salario total
            "Salario ($)": "$salary",
            "Otros ingresos salariales ($)": "$otherSalaryIncome",
            "Salario sin factor prestacional ($)": "$salaryPartial",
            "Valor factor prestacional ($)": "$salaryPercentCost",
            "TOTAL salario con factor prestacional ($)": {
                $add: [
                    "$salaryPartial",
                    "$salaryPercentCost",
                ],
            },
            "Otros no ingresos salariales ($)": "$otherNonSalaryIncome",
            // "Deducciones": {
            //     "$reduce": {
            //         "input": "$employeesProtections",
            //         "initialValue": "",
            //         "in": {
            //             "$cond": {
            //                 "if": {"$eq": [{"$indexOfArray": ["$employeesProtections", "$$this"]}, 0]},
            //                 "then": {"$concat": ["$$value", "$$this"]},
            //                 "else": {"$concat": ["$$value", ";", "$$this"]}
            //             }
            //         }
            //     }
            // },

            "#Horas trabajo por semana": "$hoursByWeek",
            "#Dias vacaciones": "$vacationDays",
            "Subsidio de transporte": {
                $cond: {
                    if: { $eq: ["$transportationSubsidy", true] },
                    then: "Aplica",
                    else: "No aplica"
                }
            },

            "Metodo de pago": "$paymentMethods", //TRADUCIR
            "Banco": "$bank",
            "Tipo de cuenta": "$accountType", //TRADUCIR
            "Numero de cuenta": "$ accountNumber",

            "Fincas": {
                "$reduce": {
                    "input": "$farmsEmployee.name",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$indexOfArray": ["$farms", "$$this"] }, 0] },
                            "then": { "$concat": ["$$value", "$$this"] },
                            "else": { "$concat": ["$$value", ";", "$$this"] }
                        }
                    }
                }
            },
        }
    },



    //-- GROUP BY (CALCULAR TOTAL NOMINA)
    {
        $group: {
            _id: {
                _id: "$Empresa CUC Mano obra",
            },
            "Total_nomina": { $sum: "$TOTAL salario con factor prestacional ($)" },
        },
    },
    { $addFields: { "_id.Total_nomina": "$Total_nomina" } },
    { $replaceRoot: { newRoot: "$_id" } },

    {
        $project: {
             _id: 0,
            "CUC": "$_id",
            "Tipo de costo": "Nomina",
            "Nombre": "SALARIO EMPLEADOS",
            " ($) Costo Inventario": { $toDouble: 0 },
            " ($) Costo Inventario Labores": { $toDouble: 0 },
            " ($) Costo Mano de Obra": { $toDouble: "$Total_nomina" },
            " ($) Costo Administrativo": { $toDouble: 0 }
        }
    },
    { $match: { "CUC": {$ne:"undefined" }} },
)