{
"$cond": {
    "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
    "then": "$$this",
    "else": {"$concat": ["$$value", "; ", "$$this"]}
}