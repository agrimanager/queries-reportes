db.cartography.aggregate([
    {
        $match: {
            path: /^,5d099eace6b5e0833ea93b2e,5d099eace6b5e0833ea93b31,([0-9a-f]{24},){0,}$/,
            "properties.status": true,
            "properties.type": {
                "$nin": ["trees", "lines"]
            }
        }
    }
])