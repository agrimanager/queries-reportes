db.cartography.aggregate(

    //---- Seleccionar poligonos
    {
        "$match": {
            "type": "Feature",
            "geometry.type":{ $ne: "Point" } 
        }
    }

    //---- Agregar valores nuevos
    , {
        "$addFields": {
            "_id_string": { "$toString": "$_id" }
        }
    }

    //---- Obtener features internos
    , {
        "$lookup": {
            "from": "cartography",
            "as": "internal_features",
            "let": {
                "feature_id_string": "$_id_string"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$gt": [
                                {
                                    "$indexOfCP": ["$path", "$$feature_id_string"]
                                },
                                -1
                            ]
                        }
                    }
                },
                {
                    "$group": {
                        "_id": "$properties.type",
                        "count": { "$sum": 1 }
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "type_features": "$_id",
                        "count_features": "$count"
                    }
                }
            ]
        }
    }




    //---- Seleccionar valores a guardar
    , {
        $project: {
            "_id": 1,
            //"_id_string": 1,
            "type":"$properties.type",
            "name":"$properties.name",
            "internal_features":"$internal_features",
            //"geometry":"$geometry",
            "path":"$path"
            // "children":"$children"
        }
    }

    // //---- Insertar en nueva coleccion
    ,{ $out : "cartography_internal" }


)
// .limit(10)