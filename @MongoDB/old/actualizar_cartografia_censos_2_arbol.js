// Borrar registros sin arbol
var resultado_remove = db.form_tratamientossanidad.remove(
    {
        "Arbol": ""
    }
);

var result = db.form_tratamientossanidad.aggregate([
    // Agrupar toda la masa en un registro, y sacar la cartografia y los ids de cartografia
    {
        "$group": {
            "_id": "_",
            "data": {
                "$push": "$$ROOT"
            },
            "arboles": {
                "$push": {
                    "$arrayElemAt": ["$Arbol.features", 0]
                }
            },
            "ids_arboles": {
                "$push": {
                    "$toObjectId": {
                        "$arrayElemAt": ["$Arbol.features._id", 0]
                    }
                }
            }
        }
    },
    
    // Sacar los ids de los nuevos arboles
    {
        "$lookup": {
            "from": "cartography",
            "as": "ids_arboles_nuevos",
            "let": {
                "ids_arboles_censos": "$ids_arboles"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$in": ["$_id", "$$ids_arboles_censos"]
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 1
                    }
                }
            ]
        }
    },
    
    // Excluir censos con cartografia al dia
    {
        "$project": {
            "data": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$not": {
                            "$in": [
                                {
                                    "$toObjectId": {
                                        "$arrayElemAt": ["$$censo.Arbol.features._id", 0]
                                    }
                                },
                                "$ids_arboles_nuevos._id"
                            ]
                        }
                    }
                }
            }
        }
    },

    
    // extraer cartografia de los censos restantes
    {
        "$addFields": {
            "arboles_censos": {
                "$map": {
                    "input": "$data",
                    "as": "censo",
                    "in": {
                        "$arrayElemAt": ["$$censo.Arbol.features", 0]
                    }
                }
            }
        }
    },
    
    // Buscar cartografia nueva segun geometria
    {
        "$lookup": {
            "from": "cartography",
            "as": "arboles_nuevos",
            "let": {
                "arboles_censos": "$arboles_censos"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {"$eq": ["$properties.type", "trees"]},
                                {"$in": ["$geometry", "$$arboles_censos.geometry"]}
                            ]
                        }
                    }
                },
                {
                    "$project": {
                        "children": 0
                    }
                },
                {
                    "$addFields": {
                        "_id": { "$toString": "$_id" }
                    }
                }
            ]
        }
    },
    
    // Desagrupar masa
    {
        "$unwind": "$data"
    },
    
    // Generar resultado:
    // - ID del registro
    // - Nuevo elemento cartografico (elemento con misma geometria)
    {
        "$replaceRoot": {
            "newRoot": {
                "_id": "$data._id",
                "arboles_nuevos": {
                    "$filter": {
                        "input": "$arboles_nuevos",
                        "as": "arbol",
                        "cond": {
                            "$in": ["$$arbol.geometry", "$data.Arbol.features.geometry"]
                        }
                    }
                }
            }
        }
    }
]);

var resultado_update = [];
result.forEach(item => {
    resultado_update.push(db.form_tratamientossanidad.update(
        {
            "_id": item._id
        },
        {
            "$set": {
                "Arbol.features": item.arboles_nuevos
            }
        }
    ));
});

// Logear resultado remove y update
var remove_ok = {
    ok: resultado_remove.ok,
    n: resultado_remove.nRemoved
};
var update_oks = resultado_update.map(r => r.ok);
console.log({
    remove_ok,
    update_oks
});


//-- Actualizar path
db.form_tratamientossanidad.aggregate(
    {
        "$addFields": {
            "Arbol.path": { "$arrayElemAt": ["$Arbol.features.path", 0]}
        }
    },
    {
        "$out": "form_tratamientossanidad"
    }
);
