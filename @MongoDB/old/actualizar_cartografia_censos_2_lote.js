
// Borrar registros sin Lote
var resultado_remove = db.form_modulodeplagas.remove(
    {
        "Lote": ""
    }
);

var result = db.form_modulodeplagas.aggregate([
    // Agrupar toda la masa en un registro, y sacar la cartografia y los ids de cartografia
    {
        "$group": {
            "_id": "_",
            "data": {
                "$push": "$$ROOT"
            },
            "arboles": {
                "$push": {
                    "$arrayElemAt": ["$Lote.features", 0]
                }
            },
            "ids_lotes": {
                "$push": {
                    "$toObjectId": {
                        "$arrayElemAt": ["$Lote.features._id", 0]
                    }
                }
            }
        }
    },
    
    // Sacar los ids de los nuevos lotes
    {
        "$lookup": {
            "from": "cartography",
            "as": "ids_lotes_nuevos",
            "let": {
                "ids_lotes_censos": "$ids_lotes"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$in": ["$_id", "$$ids_lotes_censos"]
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 1
                    }
                }
            ]
        }
    },
    
    // Excluir censos con cartografia al dia
    {
        "$project": {
            "data": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$not": {
                            "$in": [
                                {
                                    "$toObjectId": {
                                        "$arrayElemAt": ["$$censo.Lote.features._id", 0]
                                    }
                                },
                                "$ids_lotes_nuevos._id"
                            ]
                        }
                    }
                }
            }
        }
    },

    
    // extraer cartografia de los censos restantes
    {
        "$addFields": {
            "lotes_censos": {
                "$map": {
                    "input": "$data",
                    "as": "censo",
                    "in": {
                        "$arrayElemAt": ["$$censo.Lote.features", 0]
                    }
                }
            }
        }
    },
    
    // Buscar cartografia nueva segun NOMBRE
    {
        "$lookup": {
            "from": "cartography",
            "as": "lotes_nuevos",
            "let": {
                "lotes_censos": "$lotes_censos"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {"$eq": ["$properties.type", "lot"]},
                                // {"$in": ["$geometry", "$$lotes_censos.geometry"]}
                                {"$in": ["$properties.name", "$$lotes_censos.properties.name"]}
                            ]
                        }
                    }
                },
                {
                    "$project": {
                        "children": 0
                    }
                },
                {
                    "$addFields": {
                        "_id": { "$toString": "$_id" }
                    }
                }
            ]
        }
    },
    
    // Desagrupar masa
    {
        "$unwind": "$data"
    },
    
    // Generar resultado:
    // - ID del registro
    // - Nuevo elemento cartografico (elemento con misma geometria)
    {
        "$replaceRoot": {
            "newRoot": {
                "_id": "$data._id",
                "lotes_nuevos": {
                    "$filter": {
                        "input": "$lotes_nuevos",
                        "as": "lote",
                        "cond": {
                            "$in": ["$$lote.properties.name", "$data.Lote.features.properties.name"]
                            //"$in": ["$properties.name", "$$lotes_censos.properties.name"]
                        }
                    }
                }
            }
        }
    }
]);

var resultado_update = [];
result.forEach(item => {
    resultado_update.push(db.form_modulodeplagas.update(
        {
            "_id": item._id
        },
        {
            "$set": {
                "Lote.features": item.lotes_nuevos
            }
        }
    ));
});

// Logear resultado remove y update
var remove_ok = {
    ok: resultado_remove.ok,
    n: resultado_remove.nRemoved
};
var update_oks = resultado_update.map(r => r.ok);
console.log({
    remove_ok,
    update_oks
});


//-- Actualizar path
db.form_modulodeplagas.aggregate(
    {
        "$addFields": {
            "Lote.path": { "$arrayElemAt": ["$Lote.features.path", 0]}
        }
    },
    {
        "$out": "form_modulodeplagas"
    }
);
