
//----regex
db.cartography.aggregate(
    {
        $match: {
            path: {
            	//$regex: `^,${farm},`
                $regex: `^,5d2fb26fb2c83dd78fa594ad,`
            }
        }
    }
)

//----contar elementos subarbol por tipo
db.cartography.aggregate(
    {
        $match: {
            path: {
                $regex: ",5d2e547aef9c08bca80f88ad,"
            }
        }
    },
    {
        $group: {
            _id: "$properties.type",
            cantidad: {
                $sum: 1
            }
        }
    },
    {
        $project: {
            _id: 0,
            tipo: "$_id",
            cantidad: "$cantidad"
        }
    }
)

//----coordenadas de un punto
db.cartography.aggregate(
    {
        $match: {
            "geometry.type": "Point"
        }
    },
    {
        $project: {
            _id: 0,
            lon: {$arrayElemAt: ["$geometry.coordinates", 0]},
            lat: {$arrayElemAt: ["$geometry.coordinates", 1]},
        }
    }
)


//---Arbol numerico
db.cartography.aggregate(

    {
        $match: {
            "properties.type": "trees"
        }
    },

    {
        "$addFields": {
            "arbol_numerico": {
                "$reduce": {
                    "input": { "$split": ["$properties.name", "-"] },
                    "initialValue": "",
                    "in": "$$this"
                }
            }
        }
    },

)




//--------------
db.form_modulodeenfermedades.aggregate(


    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "lote_id": { "$toString": "$lote._id" }
            }
        },

        {
            "$addFields": {
                "lote_id_regex": { $concat: [",", "$lote_id", ","] }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "as": "datos_cartography",
                "let": {
                    // "lote_censo": "$lote_id"
                    "lote_censo": "$lote_id_regex"
                },
                "pipeline": [
                    {
                        $match: {
                            $expr: {
                                $gt: [
                                    {
                                        $indexOfCP: ["$path", "$$lote_censo"]
                                    },
                                    -1
                                ]
                            }
                            // path: new RegExp("$$lote_censo")
                            // path: {
                                //$regex: ",5d2fb26fb2c83dd78fa594ad,"
                                // $regex: { "$toString": "$$lote_censo" }
                                // $regex: "$$lote_censo"
                                // $regex: "properties.type"
                                // ,$options: "i"
                                
                                //$regex: new RegExp("$$lote_censo")
                                
                                // $regex: "" + "$$lote_censo" + ""
                            // }
                            
                        }
                    },
                    {
                        $group: {
                            _id: "$properties.type",
                            cantidad: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            tipo: "$_id",
                            cantidad: "$cantidad"
                        }
                    }
                ]
            }
        },



        // {
        //     "$addFields": {
        //         "bloque": "$bloque.properties.name",
        //         "lote": "$lote.properties.name",
        //         "linea": "$linea.properties.name",
        //         "planta": "$planta.properties.name"
        //     }
        // },

        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "finca._id",
        //         "foreignField": "_id",
        //         "as": "finca"
        //     }
        // },

        // {
        //     "$addFields": {
        //         "finca": "$finca.name"
        //     }
        // },
        // { "$unwind": "$finca" },


        // {
        //     "$project": {
        //         //"TEST_OBJ_LOTE":"$lote",
        //         "split_path": 0,
        //         "split_path_oid": 0,
        //         "objetos_del_cultivo": 0,
        //         "features_oid": 0
        //     }
        // }

    ]


)