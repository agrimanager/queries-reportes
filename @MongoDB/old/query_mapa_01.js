    [
        {
            "$addFields": {
                "ALEJO": "CAMPO_ALEJO"
            }
        },
        {
            "$project": {
                "_id": "$Cartography._id",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "ALEJO": "$ALEJO",
                    "color": "#cccccc"
                },
                "geometry": "$Cartography.geometry"
            }
        }
    ]