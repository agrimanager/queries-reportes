db.supplies.aggregate(

    {
        "$lookup": {
            "from": "suppliesTimeline",
            "as": "transacciones",
            "let": {
                "id": "$_id"
            },
            "pipeline": [{
                "$match": {
                    $expr: {
                        $and: [
                            { $eq: ["$sid", "$$id"] },
                            { $gt: ["$quantity", 0] }
                        ]
                    }
                }
            }
            ]
        }
    },

    {
        $match: {
            $expr: {
                $gt: [{ $size: "$transacciones" }, 0]
            }
        }
    },
    
    {
        $project:{
            "transacciones":0
        }
    }
)