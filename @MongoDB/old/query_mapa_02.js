    [
        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{
                                "$gte": [{
                                    "$floor": {
                                        "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                    }
                                }, 0]
                            }
                                , {
                                    "$lt": [{
                                        "$floor": {
                                            "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                        }
                                    }, 9]
                            }]
                        },
                        "then": "#2570A9",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{
                                        "$gte": [{
                                            "$floor": {
                                                "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                            }
                                        }, 9]
                                    },
                                    {
                                        "$lt": [{
                                            "$floor": {
                                                "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                            }
                                        }, 12]
                                    }]
                                },
                                "then": "#9ACD32",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{
                                                "$gte": [{
                                                    "$floor": {
                                                        "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                                    }
                                                }, 12]
                                            },
                                            {
                                                "$lt": [{
                                                    "$floor": {
                                                        "$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                                    }
                                                }, 15]
                                            }]
                                        },
                                        "then": "#FFFD1F",
                                        "else": "#E84C3F"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            "$project": {
                "_id": "$Cartography._id",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "color": "$color"
                },
                "geometry": "$Cartography.geometry"
            }
        }
    ]