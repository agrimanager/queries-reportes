// Borrar registros sin arbol
db.form_modulodeenfermedades.remove(
    {
        "Arbol": ""
    }
)

var result = db.form_modulodeenfermedades.aggregate([
    // Agrupar toda la masa en un registro, y sacar la cartografia y los ids de cartografia
    {
        "$group": {
            "_id": "_",
            "data": {
                "$push": "$$ROOT"
            },
            "arboles": {
                "$push": {
                    "$arrayElemAt": ["$Arbol.features", 0]
                }
            },
            "ids_arboles": {
                "$push": {
                    "$toObjectId": {
                        "$arrayElemAt": ["$Arbol.features._id", 0]
                    }
                }
            }
        }
    },
    
    // Sacar los ids de los nuevos arboles
    {
        "$lookup": {
            "from": "cartography",
            "as": "ids_arboles_nuevos",
            "let": {
                "ids_arboles_censos": "$ids_arboles"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$in": ["$_id", "$$ids_arboles_censos"]
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 1
                    }
                }
            ]
        }
    },
    
    // Excluir censos con cartografia al dia
    {
        "$project": {
            "data": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$not": {
                            "$in": [
                                {
                                    "$toObjectId": {
                                        "$arrayElemAt": ["$$censo.Arbol.features._id", 0]
                                    }
                                },
                                "$ids_arboles_nuevos._id"
                            ]
                        }
                    }
                }
            }
        }
    },

    
    // extraer cartografia de los censos restantes
    {
        "$addFields": {
            "arboles_censos": {
                "$map": {
                    "input": "$data",
                    "as": "censo",
                    "in": {
                        "$arrayElemAt": ["$$censo.Arbol.features", 0]
                    }
                }
            }
        }
    },
    
    // Buscar cartografia nueva segun geometria
    {
        "$lookup": {
            "from": "cartography",
            "as": "arboles_nuevos",
            "let": {
                "arboles_censos": "$arboles_censos"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {"$eq": ["$properties.type", "trees"]},
                                {"$in": ["$geometry", "$$arboles_censos.geometry"]}
                            ]
                        }
                    }
                },
                {
                    "$project": {
                        "children": 0
                    }
                },
                {
                    "$addFields": {
                        "_id": { "$toString": "$_id" }
                    }
                }
            ]
        }
    },
    
    // Desagrupar masa
    {
        "$unwind": "$data"
    },
    
    // Generar resultado:
    // - ID del registro
    // - Nuevo elemento cartografico (elemento con misma geometria)
    {
        "$replaceRoot": {
            "newRoot": {
                "_id": "$data._id",
                "arboles_nuevos": {
                    "$filter": {
                        "input": "$arboles_nuevos",
                        "as": "arbol",
                        "cond": {
                            "$in": ["$$arbol.geometry", "$data.Arbol.features.geometry"]
                        }
                    }
                }
            }
        }
    }
])

result.forEach(item => {
    db.form_modulodeenfermedades.update(
        {
            "_id": item._id
        },
        {
            "$set": {
                "Arbol.features": item.arboles_nuevos
            }
        }
    )
});



//-- Actualizar path
db.form_modulodeenfermedades.aggregate(
    {
        "$addFields": {
            "Arbol.path": { "$arrayElemAt": ["$Arbol.features.path", 0]}
        }
    },
    {
        "$out": "form_modulodeenfermedades"
    }
)