
{
    "$cond": {
        "if": { "$gt": ["$numero", 6] },
        "then": "#2871b1",
        "else": "#c51919"
    }
}






db.form_color.aggregate(

    [
        {
            "$addFields": {
                "color":
                    {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$numero", 0] }, { "$lt": ["$numero", 9] }],
                                "then": "#2570a9",//azul
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$numero", 9] }, { "$lt": ["$numero", 12] }],
                                            "then": "#9acd32",//verde
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gte": ["$numero", 12] }, { "$lt": ["$numero", 15] }],
                                                        "then": "#fffd1f",//amarillo
                                                        "else": "#e84c3f"//rojo
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

    ]
)