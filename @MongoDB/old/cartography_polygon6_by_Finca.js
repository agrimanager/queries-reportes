var result = db.cartography.aggregate([

    //---- Seleccionar finca
    {
        $match: {
            path: {
                $regex: `^,5d70423b18117af527a16bb2,`
            }
        }
    },

    //---- Seleccionar poligonos
    {
        "$match": {
            "type": "Feature",
            "geometry.type": "Polygon"
        }
    }

    //---- Agregar valores nuevos
    , {
        "$addFields": {
            "_id_string": { "$toString": "$_id" }
        }
    }

    //---- Obtener features internos
    , {
        "$lookup": {
            "from": "cartography",
            "as": "internal_features",
            "let": {
                "feature_id_string": "$_id_string"
            },
            "pipeline": [{
                "$match": {
                    path: {
                        $regex: `^,5d70423b18117af527a16bb2,`
                    },
                    "$expr": {
                        "$gt": [{
                            "$indexOfCP": ["$path", "$$feature_id_string"]
                        },
                        -1
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": "$properties.type",
                    "count": { "$sum": 1 }
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "type_features": "$_id",
                    "count_features": "$count"
                }
            }
            ]
        }
    }




    //---- Seleccionar valores a guardar
    , {
        $project: {
            "_id": 1,
            "type": "$properties.type",
            "name": "$properties.name",
            "internal_features": "$internal_features",
            "path": "$path"
        }
    }

])

db.cartography_polygon.insert(result.toArray());