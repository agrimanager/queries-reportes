
// Borrar registros sin Lote
var resultado_remove = db.form_modulodeplagas.remove(
    {
        "Trampa": ""
    }
);

var result = db.form_modulodeplagas.aggregate([
    // Agrupar toda la masa en un registro, y sacar la cartografia y los ids de cartografia
    {
        "$group": {
            "_id": "_",
            "data": {
                "$push": "$$ROOT"
            },
            "arboles": {
                "$push": {
                    "$arrayElemAt": ["$Trampa.features", 0]
                }
            },
            "ids_trampas": {
                "$push": {
                    "$toObjectId": {
                        "$arrayElemAt": ["$Trampa.features._id", 0]
                    }
                }
            }
        }
    },
    
    // Sacar los ids de los nuevos lotes
    {
        "$lookup": {
            "from": "cartography",
            "as": "ids_trampas_nuevos",
            "let": {
                "ids_trampas_censos": "$ids_trampas"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$in": ["$_id", "$$ids_trampas_censos"]
                        }
                    }
                },
                {
                    "$project": {
                        "_id": 1
                    }
                }
            ]
        }
    },
    
    // Excluir censos con cartografia al dia
    {
        "$project": {
            "data": {
                "$filter": {
                    "input": "$data",
                    "as": "censo",
                    "cond": {
                        "$not": {
                            "$in": [
                                {
                                    "$toObjectId": {
                                        "$arrayElemAt": ["$$censo.Trampa.features._id", 0]
                                    }
                                },
                                "$ids_trampas_nuevos._id"
                            ]
                        }
                    }
                }
            }
        }
    },

    
    // extraer cartografia de los censos restantes
    {
        "$addFields": {
            "trampas_censos": {
                "$map": {
                    "input": "$data",
                    "as": "censo",
                    "in": {
                        "$arrayElemAt": ["$$censo.Trampa.features", 0]
                    }
                }
            }
        }
    },
    
    // Buscar cartografia nueva segun NOMBRE
    {
        "$lookup": {
            "from": "cartography",
            "as": "trampas_nuevos",
            "let": {
                "trampas_censos": "$trampas_censos"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {"$eq": ["$properties.type", "traps"]},
                                // {"$in": ["$geometry", "$$lotes_censos.geometry"]}
                                {"$in": ["$properties.name", "$$trampas_censos.properties.name"]}
                            ]
                        }
                    }
                },
                {
                    "$project": {
                        "children": 0
                    }
                },
                {
                    "$addFields": {
                        "_id": { "$toString": "$_id" }
                    }
                }
            ]
        }
    },
    
    // Desagrupar masa
    {
        "$unwind": "$data"
    },
    
    // Generar resultado:
    // - ID del registro
    // - Nuevo elemento cartografico (elemento con misma geometria)
    {
        "$replaceRoot": {
            "newRoot": {
                "_id": "$data._id",
                "trampas_nuevos": {
                    "$filter": {
                        "input": "$trampas_nuevos",
                        "as": "trampa",
                        "cond": {
                            "$in": ["$$trampa.properties.name", "$data.Trampa.features.properties.name"]
                            //"$in": ["$properties.name", "$$lotes_censos.properties.name"]
                        }
                    }
                }
            }
        }
    }
]);

var resultado_update = [];
result.forEach(item => {
    resultado_update.push(db.form_modulodeplagas.update(
        {
            "_id": item._id
        },
        {
            "$set": {
                "Trampa.features": item.trampas_nuevos
            }
        }
    ));
});

// Logear resultado remove y update
var remove_ok = {
    ok: resultado_remove.ok,
    n: resultado_remove.nRemoved
};
var update_oks = resultado_update.map(r => r.ok);
console.log({
    remove_ok,
    update_oks
});


//-- Actualizar path
db.form_modulodeplagas.aggregate(
    {
        "$addFields": {
            "Trampa.path": { "$arrayElemAt": ["$Trampa.features.path", 0]}
        }
    },
    {
        "$out": "form_modulodeplagas"
    }
);
