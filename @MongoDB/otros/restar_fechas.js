 //---restar dias
, {
    "$addFields": {
        "total_dias_enfermedad": {
            "$divide": [
                {
                    "$subtract": [
                        "$fecha_recuperacion",
                        "$fecha_enfermedad"
                    ]
                },
                86400000
            ]
        }
    }
}

//---redondear hacia abajo
 , {
    "$addFields": {
        "total_dias_enfermedad": {"$floor":"$total_dias_enfermedad"}
    }
 }