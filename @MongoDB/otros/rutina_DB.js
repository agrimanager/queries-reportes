console.log("Watching for new changes in this database 'frutygreen'. Will ignore all changes to system collections...");

db.watch([{
    $match: {
        //operationType: "update", //update|insert|delete|replace|invalidate
    }
    }],
    {
        fullDocument: "updateLookup", //default|updateLookup
    })
    .on("change", (data) => {
        console.log(tojson(data))
    })
    .on("error", (err)=>{
        console.error(err)
   });

while (true) {
 sleep(1000)
}
