//ARREGLO DE URL DE FOTOS

//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];

var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar"

    , "localhost"
    , "localhost3"
    , "localhost:3001"

    , "sanjose_2023-07-12"

]



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);


    //===================query1
    var data_query = db.getSiblingDB(db_name_aux)
        .getCollection("users")
        .aggregate(
            [

                {
                    $match: {
                        avatar: {
                            $regex: `^https://clients.agrimanager.app//`
                        }
                    }
                },

            ]
        )



    //--🔄 data
    data_query.forEach(item_data => {

        result_info.push({
            database: db_name_aux,
            coleccion: "users",
            id: item_data._id,
            rgDate: item_data.rgDate

            , URL: item_data.avatar
            , name_image: ""

        })
    });
    data_query.close();


    //===================query2
    var data_query = db.getSiblingDB(db_name_aux)
        .getCollection("photosReports")
        .aggregate(
            [

                {
                    $match: {
                        url: {
                            $regex: `^https://clients.agrimanager.app//`
                        }
                    }
                },

            ]
        )



    //--🔄 data
    data_query.forEach(item_data => {

        result_info.push({
            database: db_name_aux,
            coleccion: "photosReports",
            id: item_data._id,
            rgDate: item_data.rgDate

            , URL: item_data.url
            , name_image: ""

        })
    });
    data_query.close();


    //===================query3
    var data_query = db.getSiblingDB(db_name_aux)
        .getCollection("masters")
        .aggregate(
            [

                {
                    $match: {
                        "images": true
                    }
                },


                {
                    $unwind: "$options"
                },



                {
                    $match: {
                        "options.image": {
                            $regex: `^https://clients.agrimanager.app//`
                        }
                    }
                }

            ]
        )



    //--🔄 data
    data_query.forEach(item_data => {

        result_info.push({
            database: db_name_aux,
            coleccion: "masters",
            id: item_data._id,
            rgDate: item_data.rgDate

            , URL: item_data.options.image
            , name_image: item_data.options.name

        })
    });
    data_query.close();




});

//--imprimir resultado
// result_info

/*
https://clients.agrimanager.app//5dde7df9d4db191919dadcbc-report-eeba8f92-c8d9-46e7-a8d6-35bcc50ebf71.jpg
https://clients.agrimanager.app//
*/


var result_info_aux = result_info


result_info.forEach(item => {

    var item_actual = item.URL
    //var item_nuevo = item_actual.replace('.', '')
    var item_nuevo = item_actual.replace('https://clients.agrimanager.app//', 'https://clients.agrimanager.app/')

    if (item.coleccion === "photosReports") {
        //update URL
        db.getSiblingDB(item.database)
            .getCollection(item.coleccion)
            .update(
                {
                    _id: item.id
                },
                {
                    $set: {
                        url: item_nuevo
                    }

                }

            )

    } else if (item.coleccion === "users") {

        //update URL
        db.getSiblingDB(item.database)
            .getCollection(item.coleccion)
            .update(
                {
                    _id: item.id
                },
                {
                    $set: {
                        avatar: item_nuevo
                    }

                }

            )

    } else if (item.coleccion === "masters") {

        //update URL
        db.getSiblingDB(item.database)
            .getCollection(item.coleccion)
            .update(
                {
                    _id: item.id
                    , "options.name": item.name_image
                },
                {
                    $set: {
                        "options.$.image": item_nuevo
                    }

                }

            )

    }



})



result_info_aux
