db.form_formulariofecha.aggregate(

    {
        "$addFields": {
            "resta_dias_unix": {
                "$subtract": [
                    {"$divide": [{"$divide": [{"$divide": [{"$divide": [{ "$toDouble": "$FECHA2" }, 1000]},60]},60]},24]},
                    {"$divide": [{"$divide": [{"$divide": [{"$divide": [{ "$toDouble": "$FECHA1" }, 1000]},60]},60]},24]},
                ]
            }
        }
    }
);

// "$floor": {
//         "$divide": [{ "$subtract": ["$_id.today", "$fecha inicio de ciclo"] }, 86400000]
//     }