db.farms.aggregate(

    [

        // {
        //     $limit:100
        // },


        //----->>>VARIABLES_DEL_SISTEMA
        {
            "$project": {

                // "_id": 0,
                "uid": 0,
                "rgDate": 0,
                "uDate": 0,

                "Point": 0,
                "Formula": 0,
                "capture": 0,
                "supervisor": 0,

            }
        },

        {
            "$addFields": {
                "array_variables_key_value": {
                    "$map": {
                        "input": { "$objectToArray": "$$ROOT" },
                        "as": "dataKV",
                        "in": {
                            "key": "$$dataKV.k"
                            , "type": { "$type": "$$dataKV.v" }
                            , "value": "$$dataKV.v"

                        }
                    }

                }
            }
        }

        , {
            "$project": {
                "array_variables_key_value": 1
            }
        }


        , {
            "$addFields": {
                fecha: { $toDate: "$_id" }
            }
        }

        , {
            "$project": {
                "_id": 0
            }
        }


        , {
            $unwind: "$array_variables_key_value"
        }


        , {
            $group: {
                _id: {
                    "variable_nombre": "$array_variables_key_value.key"
                    , "variable_tipo": "$array_variables_key_value.type"
                }
                , cantidad: { $sum: 1 }
                , fecha_max: { $max: "$fecha" }
            }
        }




    ]
    , { allowDiskUse: true }
)
