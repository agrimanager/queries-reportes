var cursor = db.form_evaluaciondeinsectosplagas.aggregate([
    {
        "$replaceRoot": {
            "newRoot": { "objArr": { "$objectToArray": "$$ROOT" } }
        }
    },
    {
        "$addFields": {
            "objArr": {
                "$map": {
                    "input": "$objArr",
                    "as": "kv",
                    "in": {
                        "$mergeObjects":[
                            "$$kv",
                            {
                                "v": {
                                    "$cond": {
                                        "if": { "$eq": [{"$type": "$$kv.v"}, "int"] },
                                        "then": { "$toString": "$$kv.v" },
                                        "else": "$$kv.v"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$arrayToObject": "$objArr"
            }
        }
    },
    {
        "$out": "form_evaluaciondeinsectosplagas"
    }
]);
cursor;