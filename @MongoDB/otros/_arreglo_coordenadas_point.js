var data = db.form_registroprediodeaguacate.aggregate(

    {
        "$addFields": {
            lon: { $toDouble: { $arrayElemAt: ["$Point.geometry.coordinates", 0] } },
            lat: { $toDouble: { $arrayElemAt: ["$Point.geometry.coordinates", 1] } },

        }
    }

    // lat: 6.xxxxx
    // lon: -75.xxxxxx
    //--obtener los registros malos
    , {
        $match: {
            "lon": { $gt: 0 }
        }
    }

    , {
        "$addFields": {
            coordinates_new: [
                { $toDouble: { $arrayElemAt: ["$Point.geometry.coordinates", 1] } },
                { $toDouble: { $arrayElemAt: ["$Point.geometry.coordinates", 0] } },
            ]
        }
    }


)

// data


data.forEach(i => {


    db.form_registroprediodeaguacate.update(
        {
            _id: i._id
        },
        {
            $set:{
                "Point.geometry.coordinates":i.coordinates_new
            }

        }
    )

})
