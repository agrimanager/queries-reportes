
//--variables de formulario
const Formularios = db.forms.find()
    .map(f => {
        if (!f.fstructure.length) {
            f.fstructure = Object.values(f.fstructure);
        }
        return {
            nombre: f.name,
            coleccion: f.anchor,
            estructura: f.fstructure.map(s => JSON.parse(s))
        };
    });
    
//ver resultados
Formularios




//--formularios con fechas
const Formularios = db.forms.find()
    .map(f => {
        if (!f.fstructure.length) {
            f.fstructure = Object.values(f.fstructure);
        }
        return {
            nombre: f.name,
            coleccion: f.anchor,
            estructura: f.fstructure.map(s => JSON.parse(s)).filter(s => s.type == "Date")
        };
    })
    .filter(f => f.estructura.length)
    
//ver resultados
Formularios