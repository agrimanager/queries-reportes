//----🚩 (REPORTE SIN FORMULARIO) ---- SIN FILTRO DE FECHAS
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "FORMULARIO",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                 


                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




    ]
)