
//--variale de resultado
var result_info = [];

//--obtener formularios
// var formularios = db.forms.find({})
var formularios = db.forms.aggregate(
     {
            "$lookup": {
                "from": "reports",
                "as": "reportes",
                "let": {
                    "form": "$anchor"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {"$eq": ["$anchor", "$$form"]}
                        }
                    },
                    {
                        "$project":{
                            "reporte":"$name"
                        }
                    },
                    {
                        "$project":{
                            "_id":0
                        }
                    }
                ]
            }
        },
        // {
        //     "$unwind": {
        //         "path": "$reportes",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }
    
)

// formularios

//--ciclar
formularios.forEach(item => {
    result_info.push({
        formulario : item.name,
        coleccion : item.anchor,
        num_registros : db.getCollection(item.anchor).count(),
        tamanio : db.getCollection(item.anchor).totalSize(),
        mapa : item.queryMap == "" ? "no" : "si",
        reportes: item.reportes
        
    })

});

//--imprimir resultado
result_info
