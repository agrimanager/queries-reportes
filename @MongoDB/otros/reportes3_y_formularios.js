
//--variale de resultado
var result_info = [];

//--obtener reportes
var reportes = db.reports.aggregate(
    {
        "$lookup": {
            "from": "forms",
            "as": "formulario",
            "let": {
                "form": "$anchor"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": { "$eq": ["$anchor", "$$form"] }
                    }
                },
                {
                    "$project": {
                        "nombre_formulario": "$name",
                        "queryMap":"$queryMap"
                    }
                },
                {
                    "$project": {
                        "_id": 0
                    }
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$formulario",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "queryMap": "$formulario.queryMap",
            "formulario": "$formulario.nombre_formulario"
        }
    }

)

// reportes

//--ciclar
reportes.forEach(item => {
    result_info.push({
        reporte: item.name,
        formulario:item.formulario,
        coleccion: item.anchor,

        //---si anchor existe
        num_registros: item.anchor == "" ? 0 : db.getCollection(item.anchor).count(),
        tamanio: item.anchor == "" ? 0 : db.getCollection(item.anchor).totalSize(),
        
        //mapa: item.queryMap == "" ? "no" : "si",
        mapa: (item.queryMap == "" || item.anchor == "") ? "no" : "si",
        query: item.query == "[]" ? "no" : "si",
        legend: item.legend == "" ? "no" : "si",
        pivotConf: (item.pivotConf == "{}" || item.pivotConf == null) ? "no" : "si",


    })

});

//--imprimir resultado
result_info
