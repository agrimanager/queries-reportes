
//--variale de resultado
var result_info = [];

//--obtener formularios
var formularios = db.forms.aggregate(
    {
        "$lookup": {
            "from": "reports",
            "as": "reportes",
            "let": {
                "form": "$anchor"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": { "$eq": ["$anchor", "$$form"] }
                    }
                },
                {
                    "$project": {
                        "reporte": "$name"
                    }
                },
                {
                    "$project": {
                        "_id": 0
                    }
                }
            ]
        }
    },
    {
        "$addFields": {
            "nombre_reportes": {
                "$reduce": {
                    "input": "$reportes.reporte",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },
        }
    }


)

// formularios

//--ciclar
formularios.forEach(item => {
    result_info.push({
        formulario: item.name,
        coleccion: item.anchor,
        num_registros: db.getCollection(item.anchor).count(),
        tamanio: db.getCollection(item.anchor).totalSize(),
        num_supervisores: item.supervisors.length,
        mapa: item.queryMap == "" ? "no" : "si",
        //--reportes
        num_reportes: item.reportes.length,
        reportes: item.nombre_reportes

    })

});

//--imprimir resultado
result_info
