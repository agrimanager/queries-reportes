
db.form_sanidadenfermedades_new.aggregate(
    {
        $group: {
            _id: {
                year: {$year:"$rgDate"},
                dia: {$dayOfYear: "$rgDate"}
            },
            registros: { $sum: 1 }
        }

    }
)