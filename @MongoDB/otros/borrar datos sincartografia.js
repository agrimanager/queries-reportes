//var results = [];
// db.getMongo().getDBNames()
//     .filter(dbName => !/backup/.test(dbName) && db.getSiblingDB(dbName).forms);
//db.getSiblingDB("finca").getCollectionNames()
var results = [];
db.forms.aggregate([
    {
        $project: {
            name: 1,
            fstructure: 1,
            anchor: 1
        }
    }
])
    .map(i => {
        if (i.fstructure.length === undefined) i.fstructure = Object.values(i.fstructure);
        i.fstructure = i.fstructure.map(s => JSON.parse(s)).filter(s => s.type === "Cartography");
        return i;
    })
    .filter(i => i.fstructure.length)
    .map(i => {
        i.cartographyField = i.fstructure[0].name;
        delete i.fstructure;
        return i;
    })
    .forEach(i => {
        var result = db.getCollection(i.anchor).aggregate([
            {
                $match: {
                    $expr: {
                        $or: [
                            {$eq: ["$" + i.cartographyField, ""]},
                            {$eq: ["$" + i.cartographyField + ".features", []]}
                        ]
                    }
                }
            },
            {
                $project: {
                    _id: 1
                }
            }
        ]).map(data => data._id);
        if (result.length) {
            i.result = result;
            results.push(i);
        }
    });
// results.map(item => {
//     db.getCollection(item.anchor).deleteMany({_id: {$in: item.result}})
// });
results;