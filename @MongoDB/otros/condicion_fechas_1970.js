
    //---condicion fechas 1970
    { "$addFields": { "data_fecha": "$FECHA"  } },

    { "$addFields": { "data_fecha_type": { "$type": "$data_fecha" } } },
    { "$match": { "data_fecha_type": { "$eq": "date" } } },

    { "$addFields": { "data_fecha_anio": { "$year": "$data_fecha" } } },
    { "$match": { "data_fecha_anio": { "$gt": 2000 } } },
    { "$match": { "data_fecha_anio": { "$lt": 3000 } } },
