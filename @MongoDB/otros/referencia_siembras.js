,{
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },
        
        
        
        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },
        
        {
            "$project": {
                "referencia_siembras": 0
            }
        }