db.employees.aggregate(

[ 
  { 
      "$match":{ 
         "onlineAccess":{ 
            "$eq":true
         }
      }
  },
   
   //-----ARREGLAR FECHAS MALAS
    { 
      "$addFields":{ 
         "fecha_nacimiento":{$ifNull: ["$birthDate",""]},
      }
   },
   
   { 
      "$addFields":{ 
         "tipo_fecha_nacimiento":{ $type: "$fecha_nacimiento" },
      }
   },
   
    { 
      "$addFields":{ 
         "fecha_nacimiento_str":{
                $cond: {
                    if: {$eq: ["$tipo_fecha_nacimiento", "string"]},
                    then: "$fecha_nacimiento",
                    else:{
                        $cond: {
                            if: {$and :[ 
                                {$eq: ["$tipo_fecha_nacimiento", "date"]},
                                {$gt: ["$fecha_nacimiento", ISODate("1970-01-01T00:00:00")]}
                                //{$fecha_nacimiento :{"$gt" : ISODate("1970-01-01T00:00:00")}}
                            ]},
                            then:{$dateToString: {format: "%Y-%m-%d", date: "$fecha_nacimiento"}},
                            //then:"fecha buena",
                            else:""
                        }
                    }
                }
            }
      }
   },
   
   //-----------------------------
   
   
               
   
         

   
   
   
   
   
   { 
      "$lookup":{ 
         "from":"companies",
         "localField":"cid",
         "foreignField":"_id",
         "as":"companyEmployee"
      }
   },
   { 
      "$unwind":"$companyEmployee"
   },
   { 
      "$lookup":{ 
         "from":"farms",
         "localField":"fids",
         "foreignField":"_id",
         "as":"farmsEmployee"
      }
   },
   { 
      "$lookup":{ 
         "from":"forms",
         "localField":"_id",
         "foreignField":"supervisors",
         "as":"formsEmployee"
      }
   },
   { 
      "$lookup":{ 
         "from":"tasks",
         "localField":"_id",
         "foreignField":"supervisor",
         "as":"tasksEmployee"
      }
   },
   { 
      "$lookup":{ 
         "from":"warehouses",
         "localField":"_id",
         "foreignField":"supervisors",
         "as":"warehousesEmployee"
      }
   },
   { 
      "$addFields":{ 
         "salaryPartial":{ 
            "$add":[ 
               "$salary",
               "$otherSalaryIncome"
            ]
         }
      }
   },
   { 
      "$addFields":{ 
         "salaryPercent":{ 
            "$divide":[ 
               "$companyEmployee.compensatoryMeasuresPercent",
               100
            ]
         }
      }
   },
   { 
      "$addFields":{ 
         "salaryPercentCost":{ 
            "$multiply":[ 
               "$salaryPartial",
               "$salaryPercent"
            ]
         }
      }
   },
  { 
      "$project":{ 
         "_id":0,
         "Nombre":"$firstName",
         "Apellido":"$lastName",
         "Identificacion":"$numberID",
         "Cargo":"$job",
         "Empresa":"$companyEmployee.name",
         "Fincas":{ 
            "$reduce":{ 
              "input":"$farmsEmployee.name",
              "initialValue":"",
              "in":{ 
                  "$cond":{ 
                     "if":{ 
                        "$eq":[ 
                          { 
                              "$indexOfArray":[ 
                                 "$farms",
                                 "$$this"
                              ]
                          },
                          0
                        ]
                     },
                     "then":{ 
                        "$concat":[ 
                          "$$value",
                          "$$this"
                        ]
                     },
                     "else":{ 
                        "$concat":[ 
                          "$$value",
                          ";",
                          "$$this"
                        ]
                     }
                  }
              }
            }
         },
         "Formularios":{ 
            "$reduce":{ 
              "input":"$formsEmployee.name",
              "initialValue":"",
              "in":{ 
                  "$cond":{ 
                     "if":{ 
                        "$eq":[ 
                          { 
                              "$indexOfArray":[ 
                                 "$forms",
                                 "$$this"
                              ]
                          },
                          0
                        ]
                     },
                     "then":{ 
                        "$concat":[ 
                          "$$value",
                          "$$this"
                        ]
                     },
                     "else":{ 
                        "$concat":[ 
                          "$$value",
                          ";",
                          "$$this"
                        ]
                     }
                  }
              }
            }
         },
         "Labores":{ 
            "$reduce":{ 
              "input":"$tasksEmployee.cod",
              "initialValue":"",
              "in":{ 
                  "$cond":{ 
                     "if":{ 
                        "$eq":[ 
                          { 
                              "$indexOfArray":[ 
                                 "$tasks",
                                 "$$this"
                              ]
                          },
                          0
                        ]
                     },
                     "then":{ 
                        "$concat":[ 
                          "$$value",
                          "$$this"
                        ]
                     },
                     "else":{ 
                        "$concat":[ 
                          "$$value",
                          ";",
                          "$$this"
                        ]
                     }
                  }
              }
            }
         },
         "Bodegas":{ 
            "$reduce":{ 
              "input":"$warehousesEmployee.name",
              "initialValue":"",
              "in":{ 
                  "$cond":{ 
                     "if":{ 
                        "$eq":[ 
                          { 
                              "$indexOfArray":[ 
                                 "$warehouses",
                                 "$$this"
                              ]
                          },
                          0
                        ]
                     },
                     "then":{ 
                        "$concat":[ 
                          "$$value",
                          "$$this"
                        ]
                     },
                     "else":{ 
                        "$concat":[ 
                          "$$value",
                          ";",
                          "$$this"
                        ]
                     }
                  }
              }
            }
         },
        //  "Fecha de nacimiento":"$birthDate",
        "Fecha de nacimiento":"$fecha_nacimiento_str",
        
         "Direccion":"$homeAddress",
         "Celular":"$cellphone",
         "Estado":{ 
            "$cond":{ 
              "if":{ 
                  "$eq":[ 
                     "$status",
                     true
                  ]
              },
              "then":"Activo",
              "else":"Inactivo"
            }
         },
         "Tipo de contrato":{ 
            "$switch":{ 
              "branches":[ 
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$contractType",
                          "fixed"
                        ]
                     },
                     "then":"Contrato fijo"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$contractType",
                          "temporary"
                        ]
                     },
                     "then":"Contrato temporal"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$contractType",
                          "indefinite"
                        ]
                     },
                     "then":"Contrato indefinido"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$contractType",
                          "services"
                        ]
                     },
                     "then":"Contrato por servicios"
                  }
              ],
              "default":"--------"
            }
         },
        "Fecha inicio contrato":"$fecha_inicio_contrato_str",
        "Fecha fin contrato":"$fecha_fin_contrato_str",
         "Periodicidad del salario":{ 
            "$switch":{ 
              "branches":[ 
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$periodicity",
                          "weekly"
                        ]
                     },
                     "then":"Semanal"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$periodicity",
                          "biweekly"
                        ]
                     },
                     "then":"Quincenal"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$periodicity",
                          "monthly"
                        ]
                     },
                     "then":"Mensual"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$periodicity",
                          "fourteenth"
                        ]
                     },
                     "then":"Catorcenal"
                  }
              ],
              "default":"--------"
            }
         },
         "Factor prestacional (%)":"$companyEmployee.compensatoryMeasuresPercent",
         "Salario ($)":"$salary",
         "Otros ingresos salariales ($)":"$otherSalaryIncome",
         "Salario sin factor prestacional ($)":"$salaryPartial",
         "Valor factor prestacional ($)":"$salaryPercentCost",
         "TOTAL salario con factor prestacional ($)":{ 
            "$add":[ 
              "$salaryPartial",
              "$salaryPercentCost"
            ]
         },
         "Otros no ingresos salariales ($)":"$otherNonSalaryIncome",
         "#Horas trabajo por semana":"$hoursByWeek",
         "#Dias vacaciones":"$vacationDays",
         "Subsidio de transporte":{ 
            "$cond":{ 
              "if":{ 
                  "$eq":[ 
                     "$transportationSubsidy",
                     true
                  ]
              },
              "then":"Aplica",
              "else":"No aplica"
            }
         },
         "Metodo de pago":{ 
            "$switch":{ 
              "branches":[ 
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$paymentMethods",
                          "cash"
                        ]
                     },
                     "then":"Efectivo"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$paymentMethods",
                          "check"
                        ]
                     },
                     "then":"Cheque"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$paymentMethods",
                          "direct deposit"
                        ]
                     },
                     "then":"Deposito directo"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$paymentMethods",
                          "transference"
                        ]
                     },
                     "then":"Transferencia"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$paymentMethods",
                          "payment card"
                        ]
                     },
                     "then":"Tarjetas de pago"
                  }
              ],
              "default":"--------"
            }
         },
         "Banco":"$bank",
         "Tipo de cuenta":{ 
            "$switch":{ 
              "branches":[ 
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$accountType",
                          "current acount"
                        ]
                     },
                     "then":"Cuenta corriente"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$accountType",
                          "savings acount"
                        ]
                     },
                     "then":"Cuenta de ahorros"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$accountType",
                          "payroll acount"
                        ]
                     },
                     "then":"Cuenta de nomina"
                  },
                  { 
                     "case":{ 
                        "$eq":[ 
                          "$accountType",
                          "does not apply"
                        ]
                     },
                     "then":"No aplica"
                  }
              ],
              "default":"--------"
            }
         },
         "Numero de cuenta":"$ accountNumber"
      }
  }
] 
    
)