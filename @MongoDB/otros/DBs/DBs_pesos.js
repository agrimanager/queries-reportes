//----Ojo usar desde db local

//====== Resultado
var result_info = [];

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["lukeragricola"];


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    // //--obtener info db
    var db_info = db.getSiblingDB(db_name).stats();
    // console.log(db_info.storageSize)

    result_info.push({
        database: db_name,
        peso: db_info.storageSize
    })


});

//--imprimir resultado
result_info
