//----Ojo usar desde db local

//====== Resultado
var result_info = [];

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["oleocaribe"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    // "capacitacion",
    // "finca",
    // "lukeragricola",
    // "invcamaru_testingNoBorrar",

]


var colecciones_lista_negra = [
    "system.views",

]


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name))
        return;

    console.log(db_name);


    //--obtener nombres de colecciones
    var datos = db.getSiblingDB(db_name).users.aggregate();

    datos.forEach(item => {

        result_info.push({
            database: db_name,
            id: item._id,
            user: item.user,
            PIN: item.PIN
        })

    });


});

//--imprimir resultado
result_info
