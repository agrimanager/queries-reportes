//----Ojo usar desde db local

//====== Resultado
var result_info = [];

//--obtener DBs
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["oleocaribe"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]


var colecciones_lista_negra = [
    "system.views",

]


//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name))
        return;

    console.log(db_name);


    //--obtener nombres de colecciones
    var formularios = db.getSiblingDB(db_name).forms.aggregate();

    formularios.forEach(item => {

        if (colecciones_lista_negra.includes(item.anchor))
            return;


        result_info.push({
            database: db_name,
            name: item.anchor,
            num_registros: db.getSiblingDB(db_name).getCollection(item.anchor).count(),
            tamanio: db.getSiblingDB(db_name).getCollection(item.anchor).totalSize()
        })

    });


});

//--imprimir resultado
result_info
