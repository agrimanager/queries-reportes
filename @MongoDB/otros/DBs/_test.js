


db.tasks.aggregate(

    {
        $group: {
            _id: {
                "usuario": "$uid"
            }
            , cantidad: { $sum: 1 }
            , max_fecha: { $max: "$rgDate" }
            , min_fecha: { $min: "$rgDate" }

            , data: { $push: "$$ROOT" }

        }
    }

)


//------------------


db.syncdata.aggregate(

    {
        $group: {
            _id: {
                "usuario": "$syncInfo.uid"
            }
            , cantidad: { $sum: 1 }
            , max_fecha: { $max: "$rgDate" }
            , min_fecha: { $min: "$rgDate" }

        }
    }

)
