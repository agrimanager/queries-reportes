db.logs.aggregate(


      //========= GENERAR CODIGO UNICO POR REGISTRO
      {
          "$addFields": {
              //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
              "_id_date": { "$toDate": "$rgDate" }
          }
      }
      //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

      , {
          "$addFields": {
              "date_unix": { "$toLong": "$_id_date" }
          }
      }
      //"date_unix" : 1618521716000,  // 13 numeros



      //----Codigo a generar
      //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
      // 1618521716000 = XX18521716XXX = 18521716
      , {
          "$addFields": {
              "date_unix_min": {
                  "$substr": [{ "$toString": "$date_unix" }, 2, 8]
              }
          }
      }


)
