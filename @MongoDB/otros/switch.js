  {
        "$switch": {
            "branches": [
                {
                    "case": {
                        "$eq": ["$$this", "coffee"]
                    },
                    "then": "Café"
                }
                , {
                    "case": {
                        "$eq": ["$$this", "banana"]
                    },
                    "then": "Banana"
                }
                , {
                    "case": {
                        "$eq": ["$$this", "avocado"]
                    },
                    "then": "Aguacate"
                }
            ],
            "default": null
        }
    }
