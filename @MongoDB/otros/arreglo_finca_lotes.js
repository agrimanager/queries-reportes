var fincas = db.farms.aggregate(

    {
        $addFields: {
            lotes_array: { "$objectToArray": "$lots" }

        }
    }

)

// fincas

fincas.forEach(item_finca => {

    var lots_array = []
    item_finca.lotes_array.forEach(item => {
        lots_array.push(item.v)
    })

    console.log(lots_array)

    db.farms.update(
        {
            _id: item_finca._id
        },
        {
            $set: {
                "lots": lots_array
            }
        }
    )


})
