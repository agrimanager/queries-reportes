

  //---NUMERO PEDIDO (CONSECUTIVO UNICO 4 DIGITOS)
  , {
      "$lookup": {
          "from": "form_pedidodecafe",
          "let": {
              "id": "$_id"
          },
          "pipeline": [
              {
                  "$match": {
                      "$expr": {
                          "$and": [
                              { "$lte": ["$_id", "$$id"] }
                          ]
                      }
                  }
              },

              { "$count": "count" },
              { "$project": { "count": { "$toString": "$count" } } }
          ],
          "as": "data_consecutivo"
      }
  },

  {
      "$addFields": {
          "consecutivo": {
              "$arrayElemAt": [
                  "$data_consecutivo.count",
                  0
              ]
          }
      }
  },


  {
      "$addFields": {
          "consecutivo": {
              "$switch": {
                  "branches": [
                      {
                          "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                          "then": { "$concat": ["000", "$consecutivo"] }
                      },
                      {
                          "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                          "then": { "$concat": ["00", "$consecutivo"] }
                      },
                      {
                          "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                          "then": { "$concat": ["0", "$consecutivo"] }
                      }
                  ],
                  "default": "$consecutivo"
              }
          }
      }
  },


  {
      "$project": {
          "data_consecutivo": 0
      }
  }
