var forms_con_fechas = [
    "form_balancehidrico",
    "form_balancelnlb",
    "form_cacaodestructordetermitas",
    "form_cacaoplagas",
    "form_cacaoplateoquimicoresiembra",
    "form_cacaotoconeomelina",
    "form_calidadpolinizacioninductores",
    "form_ciclocontrolguadanacacao",
    "form_ciclocontrolquimicocacao",
    "form_ciclodecosechacacao",
    "form_cicloderiegopalma",
    "form_ciclodeschuponecacao",
    "form_ciclopodacacao",
    "form_cicloscosecha",
    "form_ciclosdearvencescacao",
    "form_ciclosdehormigascacao",
    "form_ciclosdemonilia",
    "form_ciclosderiegocacao",
    "form_ciclosdetermita",
    "form_ciclosplateomecanico",
    "form_ciclosplateoquimico",
    "form_ciclospoda",
    "form_condicionesinseguras",
    "form_despachodecosecha",
    "form_erradicacindepalmas",
    "form_evaluaciondecalidad",
    "form_evaluacionpostcosechaalce",
    "form_formularioevaluacionpostcosecha",
    "form_inspeccionesepps",
    "form_investigaciondeaccidentesincidentes",
    "form_modulocensoenfermedadplagas",
    "form_modulodeplagas",
    "form_moduloderhynchophorus",
    "form_modulopolinizacioninductores",
    "form_novedadestrabajador",
    "form_otroscomportamientosinseguros",
    "form_palmaaplicacioninductores",
    "form_plandetrabajo",
    "form_presupuestocacao",
    "form_producciondecacao",
    "form_programasol",
    "form_recolecciondecosechaxempleados",
    "form_registrocontrolcosechacacao",
    "form_registrodehectmojadas",
    "form_registrodehumedad",
    "form_sanidadcacaoenfermedades",
    "form_sanidadenfermedades",
];


var result = db.forms.aggregate(
    {
        $match:{
            "anchor":{
                $in: forms_con_fechas
            }
        }
    }
);


result