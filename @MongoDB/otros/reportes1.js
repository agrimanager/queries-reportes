
//--variale de resultado
var result_info = [];

//--obtener reportes
// var reportes = db.reports.find({})
var reportes = db.reports.aggregate(
    {
        "$lookup": {
            "from": "forms",
            "as": "formulario",
            "let": {
                "form": "$anchor"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": { "$eq": ["$anchor", "$$form"] }
                    }
                },
                {
                    "$project": {
                        "nombre_formulario": "$name"
                    }
                },
                {
                    "$project": {
                        "_id": 0
                    }
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$formulario",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "formulario": "$formulario.nombre_formulario"
        }
    }

)

reportes

// //--ciclar
// reportes.forEach(item => {
//     result_info.push({
//         formulario: item.name,
//         coleccion: item.anchor,
//         num_registros: db.getCollection(item.anchor).count(),
//         tamanio: db.getCollection(item.anchor).totalSize(),
//         num_supervisores: item.supervisors.length,
//         mapa: item.queryMap == "" ? "no" : "si",
//         //--reportes
//         num_reportes: item.reportes.length,
//         reportes: item.nombre_reportes

//     })

// });

// //--imprimir resultado
// result_info
