
//--obtener nombres de colecciones
var tablas = db.getCollectionInfos();

//--ciclar
var info_array = [];
tablas.forEach(item => {
    info_array.push({
        name : item.name,
        num_registros : db.getCollection(item.name).count(),
        tamanio : db.getCollection(item.name).totalSize()
    })

});

//--imprimir resultado
info_array
