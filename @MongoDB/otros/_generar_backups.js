

//---obtener fecha actual
var fecha_txt = ""
let date = new Date()

let day = date.getDate()
let month = date.getMonth() + 1
let year = date.getFullYear()

if (month < 10) {
    fecha_txt = `${year}-0${month}-${day}`
} else {
    fecha_txt = `${year}-${month}-${day}`
}

// console.log(fecha_txt)


//---colecciones para hacer backups
var tablas = [
    "cartography",
    "form_registroprediodeaguacate",
    "form_seguimientoregistroprediodeaguacate"
]


tablas.forEach(i=>{

    var nombre_backup = i + "_" + fecha_txt
    // console.log(nombre_backup)


    db.getCollection(i).aggregate(
        {
            $out:nombre_backup
        }
    )


})
