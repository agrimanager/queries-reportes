db.logs.aggregate(

    //---log reportes
    {
        $match: {
            "log.path": {
                //---> /auth/reports/view/5dcb068b4ad8a0471888fd91
                $regex: `/auth/reports/view/`
            }
        }
    },

    //---excluir Support team
    {
        $match: {
            "log.name": {
                $nin: [
                    "Support team"
                    , "Lukeragricola" //opcional
                ]
            }
        }
    },

    //---obtener id del reporte
    {
        $addFields: {
            "reporte_id": {
                "$substr": ["$log.path", 19, 99]
            }
        }
    },
    {
        $addFields: {
            "reporte_oid": { "$toObjectId": "$reporte_id" }
        }
    },

    //---obtener nombre del reporte
    {
        $lookup: {
            from: "reports",
            localField: "reporte_oid",
            foreignField: "_id",
            as: "reporte"
        }
    },
    { $unwind: "$reporte" },
    {
        $addFields: {
            "reporte_form": "$reporte.anchor"
            , "reporte": "$reporte.name"
        }
    },

)
    .sort({ rgDate: -1 })
