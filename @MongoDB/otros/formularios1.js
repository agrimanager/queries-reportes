
//--variale de resultado
var result_info = [];

//--obtener formularios
var formularios = db.forms.find({})

//--ciclar
formularios.forEach(item => {
    result_info.push({
        formulario : item.name,
        coleccion : item.anchor,
        num_registros : db.getCollection(item.anchor).count(),
        tamanio : db.getCollection(item.anchor).totalSize(),
        mapa : item.queryMap == "" ? "no" : "si",
        
    })

});

//--imprimir resultado
result_info
