db.users.aggregate(
    [
        {
            "$addFields": {

                "keys": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": "$$dataKV.k"
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                },
                "values": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": "$$dataKV.v"
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

    ]
)