db.tracks.aggregate(

    {
        $project:{
            inicio: { $arrayElemAt: ["$points", 0] },
            fin :{ $arrayElemAt: ["$points", {$subtract:[{"$size":"$points"},1]}] }
        }
    },
    
    {
        $project:{
            dias:{
                $subtract:[
                    { $dayOfYear: "$fin.properties.timestamp" },
                    { $dayOfYear: "$inicio.properties.timestamp" }
                ]
            }
        }
    },
    
    {
        $match:{
            dias : {$gt:0}
        }
    },
    
    {
        $sort:{
            dias:-1
        }
    }
    
)