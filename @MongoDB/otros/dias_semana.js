

, {
    "$addFields": {
        "anio": { "$year": { "date": "$fecha", "timezone": "-0500" } },
        "mes": { "$month": { "date": "$fecha", "timezone": "-0500" } },
        "semana": { "$week": { "date": "$fecha", "timezone": "-0500" } },
        "dia": { "$dayOfYear": { "date": "$fecha", "timezone": "-0500" } },
        "dia_semana": { "$dayOfWeek": { "date": "$fecha", "timezone": "-0500" } }
    }
}


, {
    "$addFields": {
        "dia_semana_txt": {
            "$switch": {
                "branches": [
                    { "case": { "$eq": ["$dia_semana", 2] }, "then": "01-Lunes" },
                    { "case": { "$eq": ["$dia_semana", 3] }, "then": "02-Martes" },
                    { "case": { "$eq": ["$dia_semana", 4] }, "then": "03-Miercoles" },
                    { "case": { "$eq": ["$dia_semana", 5] }, "then": "04-Jueves" },
                    { "case": { "$eq": ["$dia_semana", 6] }, "then": "05-Viernes" },
                    { "case": { "$eq": ["$dia_semana", 7] }, "then": "06-sabado" },
                    { "case": { "$eq": ["$dia_semana", 1] }, "then": "07-Domingo" }
                ],
                    "default": "dia de la semana desconocido"
            }
        }
    }
}