//---filtro fechas malas
 ,{ "$addFields": { "variable_fecha": "$Fecha Medicion" } }
 ,{ "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
 ,{ "$match": { "type_variable_fecha": { "$eq": "date" } } },


 { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
 { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
 { "$match": { "anio_variable_fecha": { "$lt": 3000 } } },

 {
     "$project": {
         "variable_fecha": 0
         , "type_variable_fecha": 0
         , "anio_variable_fecha": 0
     }
 }
