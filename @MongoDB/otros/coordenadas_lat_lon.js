//----coordenadas de un punto
db.cartography.aggregate(
    {
        $match: {
            "geometry.type": "Point"
        }
    },
    {
        $project: {
            _id: 0,
            lon: {$arrayElemAt: ["$geometry.coordinates", 0]},
            lat: {$arrayElemAt: ["$geometry.coordinates", 1]},
        }
    }
)
