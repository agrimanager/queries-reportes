db.logs.aggregate(

    //---VARIABLES INYECTADAS
    {
        $addFields: {
            "Busqueda inicio": ISODate("2023-03-01T06:00:00.000-05:00"),
            "Busqueda fin": new Date,
        }
    },
    //----FILTRO FECHAS Y FINCA
    {
        "$match": {
            "$expr": {
                "$and": [

                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },
                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------
    //....query reporte

    //---log reportes
    {
        $match: {
            "log.path": {
                //---> /auth/reports/view/5dcb068b4ad8a0471888fd91
                $regex: `/auth/reports/view/`
            }
        }
    },

    //---excluir Support team
    {
        $match: {
            "log.name": {
                $nin: [
                    "Support team"
                    , "Lukeragricola" //opcional
                ]
            }
        }
    },

    //---obtener id del reporte
    {
        $addFields: {
            "reporte_id": {
                "$substr": ["$log.path", 19, 99]
            }
        }
    },
    {
        $addFields: {
            "reporte_oid": { "$toObjectId": "$reporte_id" }
        }
    },

    //---obtener nombre del reporte
    {
        $lookup: {
            from: "reports",
            localField: "reporte_oid",
            foreignField: "_id",
            as: "reporte"
        }
    },
    { $unwind: "$reporte" },
    {
        $addFields: {
            "reporte_form": "$reporte.anchor"
            , "reporte": "$reporte.name"
        }
    },


    {
        $group: {
            _id: "$reporte"
            , cantidad: { $sum: 1 }
        }
    }

)
    // .sort({ rgDate: -1 })
