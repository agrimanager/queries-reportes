
//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["cluster"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]



//---query1 //repetidos full
var query_pipeline = [

    {
        "$match": {
            // "Point.farm":""
            "Point.farm":{$exists: false}
        }
    }


];



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if(bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(query_pipeline)
            .allowDiskUse();


        //--🔄 data
        data_query.forEach(item_data => {


            result_info.push({
                database: db_name,
                formulario: coleccion_name,
                id: item_data._id,
                rgDate: item_data.rgDate

            })
        });
        data_query.close();




    });



});

//--imprimir resultado
result_info




//arreglo de datos
/*

use("agricolaocoa")
var coleccion_name = "form_registrarhumedad"

//===================query1
db.getCollection(coleccion_name).aggregate(
    [
        {
            "$match": {
                // "Point.farm":""
                "Point.farm": { $exists: false }
            }
        }
    ]
)


// //===================query1
// var data = db.getCollection(coleccion_name).aggregate(
//     [
//         {
//             "$match": {
//                 // "Point.farm":""
//                 "Point.farm": { $exists: false }
//             }
//         }
//     ]
// )


// data

// // var farm_id = "5f176a5df138a357c27162ef"

// // data.forEach(i => {

// //     db.getCollection(coleccion_name).update(
// //         {
// //             _id:i._id
// //         },
// //         {
// //             $set:{
// //                 "Point.farm": farm_id
// //             }
// //         }

// //     )
// // })



*/
