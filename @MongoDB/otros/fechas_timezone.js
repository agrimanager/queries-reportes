
//---info fechas
,{
    "$addFields": {
        "num_anio": { "$year": { "date": "$rgDate","timezone": "America/Bogota" } },
        "num_mes": { "$month": { "date": "$rgDate","timezone": "America/Bogota" } },
        "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate","timezone": "America/Bogota" } },
        "num_semana": { "$week": { "date": "$rgDate","timezone": "America/Bogota" } },
        "num_dia_semana": { "$dayOfWeek": { "date": "$rgDate","timezone": "America/Bogota" } }
    }
}

, {
    "$addFields": {
        "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate","timezone": "America/Bogota" } }

        , "Mes_Txt": {
            "$switch": {
                "branches": [
                    { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                    { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                    { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                    { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                    { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                    { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                    { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                    { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                    { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                    { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                    { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                    { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                ],
                "default": "Mes desconocido"
            }
        }


        , "Dia_Txt": {
            "$switch": {
                "branches": [
                    { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                    { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                    { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                    { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                    { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                    { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                    { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                ],
                "default": "dia de la semana desconocido"
            }
        }
    }
},

//--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
{ "$project": { "num_dia_semana": 0 } },