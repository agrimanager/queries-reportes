 {
    "$match": {
        "$expr": {
            "$and": [
                {
                    "$gte": [
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA_FORMULARIO_FILTRO" } } }
                        ,
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                    ]
                },

                {
                    "$lte": [
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA_FORMULARIO_FILTRO" } } }
                        ,
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                    ]
                }
            ]
        }
    }
},



db.form_modulopolinizacioninductores.aggregate(

    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2020-04-03T01:21:23.000-05:00") } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2020-05-03T01:21:23.000-05:00") } } }
                        ]
                    }
                ]
            }
        }
    },

)



//"Fecha de Registro" : ISODate("1969-12-31T19:00:00.000-05:00"),