, "(%) Alzados x lote": {
    "$cond": {
        "if": {
            "$eq": ["$total_peso_aproximado_racimos_alzados_viaje", 0]
        },
        "then": 0,
        "else": {
            "$divide": ["$total_peso_aproximado_racimos_alzados_viaje_lote", "$total_peso_aproximado_racimos_alzados_viaje"]
        }
    }
}



 , {
      "$addFields": {
          "costo_x_kilo_final": {
              "$cond": {
                  "if": { "$eq": ["$recoleccion_total_kg", 0] },
                  "then": 0,
                  "else": {
                      "$divide": ["$valor_total_final",
                          "$recoleccion_total_kg"]
                  }
              }
          }
      }
  }