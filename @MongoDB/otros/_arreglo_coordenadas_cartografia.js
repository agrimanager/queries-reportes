var data = db.cartography.aggregate(

    {
        $match: {
            type:"Feature",
            "geometry.type":"Point"
        }
    }


    , {
        "$addFields": {
            lon: { $arrayElemAt: ["$geometry.coordinates", 0] },
            lat: { $arrayElemAt: ["$geometry.coordinates", 1] },

        }
    }

    // lat: 6.xxxxx
    // lon: -75.xxxxxx
    //--obtener los registros malos
    , {
        $match: {
            // "lon": {$gt:"$lat"}
            "lon": { $gt: 0 }
        }
    }

    , {
        "$addFields": {
            coordinates_new: [
                { $arrayElemAt: ["$geometry.coordinates", 1] },
                { $arrayElemAt: ["$geometry.coordinates", 0] }
            ]
        }
    }


)

data


// data.forEach(i => {


//     db.cartography.update(
//         {
//             _id: i._id
//         },
//         {
//             $set:{
//                 "geometry.coordinates":i.coordinates_new
//             }

//         }
//     )

// })
