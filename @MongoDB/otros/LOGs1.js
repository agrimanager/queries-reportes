db.logs.aggregate(

    {
        "$addFields": {
            "fecha": {
                "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" }
            },
            "anio": { "$year": { "date": "$rgDate" } },
            "mes": { "$month": { "date": "$rgDate" } },
            "dia_anio": { "$dayOfYear": { "date": "$rgDate" } }
        }
    }
    
    ,{
        "$addFields": {
            "usuario": "$log.name",
            "path": "$log.path",
        }
    }
    
    
    ,{
        $project:{
            _id:0,
            uid:0,
            log:0
        }
    }

)
