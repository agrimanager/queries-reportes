const maestrosConFormulario = db.forms.find()
    .map(f => {
        if (!f.fstructure.length) {
            f.fstructure = Object.values(f.fstructure);
        }
        return {
            nombre: f.name,
            coleccion: f.anchor,
            estructura: f.fstructure.map(s => JSON.parse(s)).filter(s => s.master)
        };
    })
    .filter(f => f.estructura.length)
    .reduce((acumulador, valor) => {
        valor.estructura.forEach(s => acumulador.add(s.master))
        return acumulador;
    }, new Set());
console.log("maestrosConFormulario.size: ", maestrosConFormulario.size);


const maestrosConMaestroAsociado = db.masters.find()
    .map(m => {
        m._id = ""+m._id;
        m.options = m.options.filter(o => o.master);
        return m;
    })
    .filter(m => m.options.length && maestrosConFormulario.has(m._id))
    .reduce((acumulador, valor) => {
        valor.options.forEach(o => acumulador.add(o.master));
        return acumulador;
    }, new Set());
console.log("maestrosConMaestroAsociado.size: ", maestrosConMaestroAsociado.size);


// const aBorrar = db.masters.find()
//     .map(m => {
//         m._id = ""+m._id;
//         return m._id;
//     })
//     .filter(m => !maestrosConFormulario.has(m) && !maestrosConMaestroAsociado.has(m))
//     .map(m => ObjectId(m));
// console.log("aBorrar.length: ", aBorrar.length);

// db.masters.deleteMany({_id: {$in: aBorrar}});

