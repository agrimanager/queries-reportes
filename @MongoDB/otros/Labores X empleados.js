 [

        //-- Filtrar finca
        {$match: {"farm": new ObjectID(filter_dates.farm)}},


        {$match: {farm: {$ne: ""}}},

        //-- JOIN activities
        {
            $lookup: {
                from: "activities",
                localField: "activity",
                foreignField: "_id",
                as: "activity"
            }
        },
        {"$unwind": "$activity"},
        //-- JOIN centersCosts
        {
            $lookup: {
                from: "costsCenters",
                localField: "ccid",
                foreignField: "_id",
                as: "costsCenter"
            }
        },
        {"$unwind": "$costsCenter"},
        //-- JOIN farm
        {
            $lookup: {
                from: "farms",
                localField: "farm",
                foreignField: "_id",
                as: "farm"
            }
        },
        {"$unwind": "$farm"},


        //-- JOIN employees ( supervisor)
        {
            $lookup: {
                from: "employees",
                localField: "supervisor",
                foreignField: "_id",
                as: "supervisor"
            }
        },
        //{"$unwind": "$supervisor"},
        {
            "$unwind": {
                path: "$supervisor",
                preserveNullAndEmptyArrays: true
            }
        },


        //-------- Empleado y #productividad
        // Array con mas de 20 se vuelve OBJETO
        //..comprobar si es array u objeto
        {
            $addFields: {
                "employees": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$employees"}, "array"]
                        },
                        then: "$employees",
                        else: {
                            $map: {
                                input: {$objectToArray: "$employees"},
                                as: "employeeKV",
                                in: "$$employeeKV.v"
                            }
                        }
                    }
                },
                "productivityReport": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$productivityReport"}, "array"]
                        },
                        then: "$productivityReport",
                        else: {
                            $map: {
                                input: {$objectToArray: "$productivityReport"},
                                as: "productivityReportKV",
                                in: "$$productivityReportKV.v"
                            }
                        }
                    }
                },
                "lots": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$lots"}, "array"]
                        },
                        then: "$lots",
                        else: {
                            $map: {
                                input: {$objectToArray: "$lots"},
                                as: "lotKV",
                                in: "$$lotKV.v"
                            }
                        }
                    }
                }
            }
        },

        //-- total de productos y empelados SELECCIONADOS a la labor
        {
            "$addFields": {
                "total_empleados_seleccionados_labor": {$size: "$employees"},
                "total_productos_seleccionados_labor": {$size: "$supplies"}
            }
        },

        //--- Converitir de ($supplies) _id strings en objectId
        {
            '$unwind': {
                'path': '$employees',
                'includeArrayIndex': 'arrayIndex',
                'preserveNullAndEmptyArrays': false
            }
        },
        {
            "$addFields": {
                "employees": {
                    $toObjectId: "$employees"
                }
            }
        },
        {
            "$addFields": {
                "productivityReport": {
                    "$map": {
                        "input": "$productivityReport",
                        "as": "item",
                        "in": {
                            "$mergeObjects": [
                                "$$item",
                                {
                                    "employee": {"$toObjectId": "$$item.employee"}
                                }
                            ]
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "productivityReport": {
                    "$filter": {
                        "input": "$productivityReport",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.employee", "$employees"]
                        }
                    }
                }
            }
        },
        {
            "$unwind": {
                path: "$productivityReport",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            "$addFields": {
                "ProductividadEmpleado": {$toDouble: "$productivityReport.quantity"}
            }
        },


        //-- JOIN employees ( empleados de labor)
        {
            "$lookup": {
                "from": "employees",
                "localField": "employees",
                "foreignField": "_id",
                "as": "Empleado"
            }
        },
        {
            "$unwind": {
                path: "$Empleado",
                preserveNullAndEmptyArrays: true
            }
        },

        //-- calcular Total Salario
        {
            "$addFields":
                {
                    "TotalPagoEmpleado": {
                        "$multiply": [
                            {$ifNull: [{$toDouble: "$ProductividadEmpleado"}, 0]},
                            {$ifNull: [{$toDouble: "$productivityPrice.price"}, 0]},
                        ],
                    },
                }
        },


        //-- productos
        // //----------------------------------------------------------------
        {
            $addFields: {
                "productos": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$supplies"}, "array"]
                        },
                        then: "$supplies",
                        else: {
                            $map: {
                                input: {$objectToArray: "$employees"},
                                as: "suppliesKV",
                                in: "$$suppliesKV.v"
                            }
                        }
                    }
                }
            }
        },


        //-- convertir id en ObjectId
        {
            "$addFields": {
                "productos": {"$map": {input: "$productos._id", as: "strid", in: {"$toObjectId": "$$strid"}}},
            }
        },

        //-- JOIN supplies ( productos de labor)
        {
            "$lookup": {
                "from": "supplies",
                "localField": "productos",
                "foreignField": "_id",
                "as": "productos"
            }
        },

        // // //----------------------------------------------------------------


        //-- arrays de strings (separados por ;)
        {
            "$addFields": {

                "productos": {
                    "$reduce": {
                        "input": "$productos.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },

                "Etiquetas": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$tags",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                    "then": "$$this",
                                    "else": {"$concat": ["$$value", "; ", "$$this"]}
                                }
                            }
                        }
                    }, "--sin etiquetas--"]
                },


                "Tipo_cultivo": {
                    "$reduce": {
                        "input": "$crop",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                // "then": "$$this",
                                "then": switch_translate_crop_type_es("$$this"),
                                // "else": { "$concat": ["$$value", "; ", "$$this"] }
                                "else": {"$concat": ["$$value", "; ", switch_translate_crop_type_es("$$this")]}
                            }
                        }
                    }
                },

                "Blancos_biologicos": {
                    "$reduce": {
                        "input": "$biologicalTarget",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },

                //---quitar duplicados
                "Cartografia_seleccionada_mapa_tipos": {
                    "$reduce": {
                        "input": {
                            "$reduce": {
                                "input": "$cartography.features.properties.type",
                                "initialValue": [],
                                "in": {
                                    "$cond": {
                                        "if": {"$lt": [{"$indexOfArray": ["$$value", "$$this"]}, 0]},
                                        "then": {"$concatArrays": ["$$value", ["$$this"]]},
                                        "else": "$$value"
                                    }
                                }
                            }
                        },
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                // "then": "$$this",
                                "then": switch_translate_feature_type_es("$$this"),
                                // "else": { "$concat": ["$$value", "; ", "$$this"] }
                                "else": {"$concat": ["$$value", "; ", switch_translate_feature_type_es("$$this")]}
                            }
                        }
                    }
                },

                "Cartografia_seleccionada_mapa_elementos": {
                    "$reduce": {
                        "input": "$cartography.features.properties.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },


                "Cartografia_seleccionada_lista_lotes_y_estados": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lots",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                    "then": "$$this",
                                    "else": {"$concat": ["$$value", "; ", "$$this"]}
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },

                "lista_lots": {
                    "$map": {
                        "input": "$lots",
                        "as": "lot",
                        "in": {
                            "$split": ["$$lot", ", "]
                        }
                    }
                }


            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 0]
                        }
                    }
                },
                "lista_estados": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 1]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lista_lotes",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },
                "lista_estados": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lista_estados",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin estados seleccionados--"]
                }
            }
        },


        //-- SELECT valores
        {
            "$project": {
                _id: 0,
                "Actividad": "$activity.name",
                "Codigo Labor": "$cod",
                "Estado Labor": {
                    $switch: {
                        branches: [
                            {case: {$eq: ["$status", "To do"]}, then: "⏰ Por hacer"},
                            {case: {$eq: ["$status", "Doing"]}, then: "💪 En progreso"},
                            {case: {$eq: ["$status", "Done"]}, then: "✔ Listo"}
                        ]
                    }
                },
                "Finca": "$farm.name",

                //---CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",

                //---SIN CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                "Lista lotes": "$lista_lotes",
                "Lista estados": "$lista_estados",

                "Tipo cultivo labor": {$ifNull: ["$Tipo_cultivo", "--sin tipo cultivo--"]},//--array_string
                "Etiquetas": {
                    $cond: {
                        if: {$eq: ["$Etiquetas", ""]},
                        then: "--sin etiquetas--",
                        else: "$Etiquetas"
                    }
                },//--array_string


                "Centro de costos": "$costsCenter.name",
                // productividad
                "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                // "[Unidad] Medida de Actividad": "$productivityPrice.measure",//TRADUCIR
                "[Unidad] Medida de Actividad": switch_translate_measure_type_units_es("$productivityPrice.measure"),
                "($) Precio x unidad de Actividad": "$productivityPrice.price",

                "Semana de inicio labor": { $sum: [ { $week: { $arrayElemAt: [ "$when.start", 0 ] } }, 1 ] },
                "Semana de registro labor": { $sum: [ { $week: "$rgDate" }, 1 ] },
                "Fecha inicio": {$max: '$when.start'},
                "Fecha fin": {$max: '$when.finish'},

                // strings
                "Blancos biologicos de labor": {$ifNull: ["$Blancos_biologicos", "--sin blancos biologicos--"]},//--array_string
                "Observaciones": "$observation",


                // empleados
                // "(#) Empleados asignados": "$planningEmployees",
                "Nombre de Empleado": {$ifNull: [{$concat: ["$Empleado.firstName", " ", "$Empleado.lastName"]}, "--sin empleados--"]},
                "Identificacion": {$ifNull: ["$Empleado.numberID", "--"]},
                "(#) Productividad de Empleado": {$ifNull: ["$ProductividadEmpleado", 0]},
                "($) Total Pago Empleado": "$TotalPagoEmpleado",
                "Firma de empleado": "* ",
                "Aviso legal": "* ",

                "Supervisor": {$ifNull: [{$concat: ["$supervisor.firstName", " ", "$supervisor.lastName"]}, "--sin supervisor--"]},

                "(#) Productos asignados": "$total_productos_seleccionados_labor",
                "Productos asignados": {
                    $cond: {
                        if: {$eq: ["$productos", ""]},
                        then: "--sin productos--",
                        else: "$productos"
                    }
                },//--array_string


                "(#) Empleados asignados": "$total_empleados_seleccionados_labor"
            }
        },
        {$sort: {"Codigo Labor": 1}},


        // //--Filtro de fechas
        filter_dates_reports("Fecha inicio", filter_dates)

    ]