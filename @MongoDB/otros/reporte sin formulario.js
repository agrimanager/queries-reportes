db.users.aggregate(

    //-----join de usuario con labores 
    //--(para simular como si estuviese tirando el aggregate desde labores)

    //---join con labores
    {
        "$lookup": {
            "from": "tasks",
            "localField": "_id",
            "foreignField": "uid",
            "as": "labores"
        }
    }
    , {
        "$unwind": {
            "path": "$labores"
        }
    }
    
    //--remplazar raiz
    ,{
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$labores"
                    // ,{
                    //     "Point": "$Point",
                    //     "rgDate": "$rgDate"
                    // }
                ]
            }
        }
    }
)