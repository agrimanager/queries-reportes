let data_reporte;
let dataId;
let dataToShow;

// Funcion para cargar los datos del reporte
function loadData() {
  data_reporte = window.__preCompiledDataReference;
  dataId = undefined;
  setSelect("CONSECUTIVO_UNICO", "filter-one");

  // document.getElementById("load-btn").style.display = "none"
  cleanData();
}

// Funcion que incerta la data en los selects
async function setSelect(type, id) {
  let data = (await dataId)
    ? data_reporte.filter((item) => {
        return item[type] === dataProveedor;
      })
    : data_reporte;

  let dataArray = [];

  // // selecciona la data no repetida segun su tipo
  await data
    .map((item) => {
      return item[type];
    })
    .map((item) => {
      if (!dataArray.includes(item)) {
        dataArray.push(item);
      }
    });

  // //ordenar
  dataArray.sort();
  dataArray.unshift("Selecciona");

  document.getElementById(id).innerHTML = await dataArray.map((item) => {
    return `<option value="${item}">${item}</option>`;
  });
}

//  Funcionamiento del select tag
function selectId() {
  const sb = document.querySelector("#filter-one");
  dataId = sb.value;
  selectData();
}

// Filtra la data segun el numero de liquidacion para mostrarla en el reporte
function selectData() {
  let data_filter = data_reporte.filter((e) => {
    return e["CONSECUTIVO_UNICO"] === dataId;
  })[0];
  dataToShow = data_filter;

  //mostrar reporte
  showData();
  formatNumberData();
}

function showData() {
  //tabla
  document.getElementById("id_codigo").innerHTML =
    dataToShow["CONSECUTIVO_UNICO"];
  document.getElementById("id_tipo_fruta").innerHTML =
    dataToShow["Tipo de fruta"];
  document.getElementById("id_fecha_despacho").innerHTML =
    dataToShow["despacho_fecha"];
  document.getElementById("id_nombre_conductor").innerHTML =
    dataToShow["Nombre Conductor"];
  document.getElementById("id_placa_vehiculo").innerHTML =
    dataToShow["Placa Vehiculo"];
  document.getElementById("id_nombre_proveedor").innerHTML =
    dataToShow["Nombre proveedor"];
  document.getElementById("id_cedula").innerHTML = dataToShow["Cedula NIT"];
  document.getElementById("id_predio").innerHTML = dataToShow["Predio"];
  document.getElementById("id_lote").innerHTML = dataToShow["Lotes"];
  document.getElementById("id_ica").innerHTML = dataToShow["Codigo ICA"];
  document.getElementById("id_municipio").innerHTML = dataToShow["Municipio"];
  document.getElementById("id_rainforest").innerHTML = dataToShow["Codigo RF"];
  document.getElementById("id_global").innerHTML = dataToShow["Codigo GAP"];
  document.getElementById("id_fecha_cosecha").innerHTML =
    dataToShow["cosecha_fecha"];
  document.getElementById("id_hora_cosecha").innerHTML =
    dataToShow["cosecha_hora"];
  document.getElementById("id_hora_despacho").innerHTML =
    dataToShow["despacho_hora"];
  document.getElementById("id_kilos_despachados").innerHTML =
    dataToShow["Kilos Despachados"];
  document.getElementById("id_num_canastillas").innerHTML =
    dataToShow["Canastillas Despachadas"];
  document.getElementById("id_observaciones").innerHTML =
    dataToShow["Observaciones"];
}

function cleanData() {
  document.getElementById("id_tipo_fruta").innerHTML = "";
  document.getElementById("id_fecha_despacho").innerHTML = "";
  document.getElementById("id_nombre_conductor").innerHTML = "";
  document.getElementById("id_placa_vehiculo").innerHTML = "";
  document.getElementById("id_nombre_proveedor").innerHTML = "";
  document.getElementById("id_cedula").innerHTML = "";
  document.getElementById("id_predio").innerHTML = "";
  document.getElementById("id_lote").innerHTML = "";
  document.getElementById("id_ica").innerHTML = "";
  document.getElementById("id_municipio").innerHTML = "";
  document.getElementById("id_rainforest").innerHTML = "";
  document.getElementById("id_global").innerHTML = "";
  document.getElementById("id_fecha_cosecha").innerHTML = "";
  document.getElementById("id_hora_cosecha").innerHTML = "";
  document.getElementById("id_hora_despacho").innerHTML = "";
  document.getElementById("id_kilos_despachados").innerHTML = "";
  document.getElementById("id_num_canastillas").innerHTML = "";
  document.getElementById("id_observaciones").innerHTML = "";
}

function formatNumberData() {
  //numeros
  formatNumberById("id_kilos_despachados");
  formatNumberById("id_num_canastillas");
}

//numeros (millares y 2decimales)
function formatNumberById(id_element) {
  //Intl.NumberFormat('es-CO').format((5000000.009897).toFixed(2))
  document.getElementById(id_element).innerHTML = Intl.NumberFormat(
    "es-CO"
  ).format(
    parseFloat(document.getElementById(id_element).innerHTML).toFixed(2)
  );
}
