let data_reporte;
let dataProveedor;
let dataLiquidacion;
let dataToShow;

// Funcion para cargar los datos del reporte
function loadData() {
    data_reporte = window.__preCompiledDataReference;
    dataProveedor = undefined;
    setSelect("Nombre proveedor", "filter-one");
}

// Funcion que incerta la data en los selects
async function setSelect(type, id) {
    let data = (await dataProveedor)
        ? data_reporte.filter((item) => {
            return item["Nombre proveedor"] === dataProveedor;
        })
        : data_reporte;

    //let dataArray = ["Selecciona"];
    let dataArray = [];

    // selecciona la data no repetida segun su tipo
    let resultTwo = await data
        .map((item) => {
            return item[type];
        })
        .map((item) => {
            if (!dataArray.includes(item)) {
                dataArray.push(item);
            }
        });

    //ordenar
    dataArray.sort();
    dataArray.unshift("Selecciona")


    document.getElementById(id).innerHTML = await dataArray.map((item) => {
        return `<option value="${item}">${item}</option>`;
    });
}

// La funcion de Slect para proveedores
function selectPoveedores() {
    const sb = document.querySelector("#filter-one");
    dataProveedor = sb.value;
    setSelect("No liquidación", "filter-two");

    //limpiar reporte
    cleanData();

}

// La funcion de Slect para liquidacion
function selectLiquidacion() {
    const sb = document.querySelector("#filter-two");

    dataLiquidacion = sb.value
    selectData();
}


// Filtra la data segun el numero de liquidacion para mostrarla en el reporte
function selectData() {
    var text_proveedor_liquidacion = dataProveedor + "_" + dataLiquidacion;

    let  data_proveedor_liquidacion = data_reporte.filter((e) => {
        return e["PROVEEDOR_NUM_LIQUIDACION"] === text_proveedor_liquidacion;
    })[0];

    dataToShow = data_proveedor_liquidacion;

    //mostrar reporte
    showData();

    formatNumberData();
}


function showData() {
    //---cambiar valores de variables

    //tabla1
    document.getElementById("id_cedula").innerHTML = dataToShow["Productor cedula"];
    document.getElementById("id_direccion").innerHTML = dataToShow["Direccion"];
    document.getElementById("id_ica").innerHTML = dataToShow["No reg ICA"];
    document.getElementById("id_liquidacion").innerHTML = dataToShow["No liquidación"];
    document.getElementById("id_predio").innerHTML = dataToShow["Nombre predio"];
    document.getElementById("id_proveedor").innerHTML = dataToShow["Nombre proveedor"];

    //tabla2
    document.getElementById("id_canastillas").innerHTML = dataToShow["Canastillas ingresadas"];
    document.getElementById("id_descarte_proveedor").innerHTML = dataToShow["Recoleccion descarte por el productor"];
    document.getElementById("id_deshidratacion").innerHTML = dataToShow["Kg merma"];
    document.getElementById("id_fecha_liquidacion").innerHTML = dataToShow["Fecha ingreso de fruta"];
    document.getElementById("id_kg_descarte").innerHTML = dataToShow["Kg descartados"];
    document.getElementById("id_kg_exportables").innerHTML = dataToShow["Kg exportados"];
    document.getElementById("id_kg_ingresados").innerHTML = dataToShow["Kg ingresados"];
    document.getElementById("id_valor_descarte").innerHTML = dataToShow["Total valor descarte"];


    //tabla3

    //kg_x_calibre
    document.getElementById("kg_calibre10").innerHTML = dataToShow["Kg calibre 10"];
    document.getElementById("kg_calibre12").innerHTML = dataToShow["Kg calibre 12"];
    document.getElementById("kg_calibre14").innerHTML = dataToShow["Kg calibre 14"];
    document.getElementById("kg_calibre16").innerHTML = dataToShow["Kg calibre 16"];
    document.getElementById("kg_calibre18").innerHTML = dataToShow["Kg calibre 18"];
    document.getElementById("kg_calibre20").innerHTML = dataToShow["Kg calibre 20"];
    document.getElementById("kg_calibre22").innerHTML = dataToShow["Kg calibre 22"];
    document.getElementById("kg_calibre24").innerHTML = dataToShow["Kg calibre 24"];
    document.getElementById("kg_calibre26").innerHTML = dataToShow["Kg calibre 26"];
    document.getElementById("kg_calibre28").innerHTML = dataToShow["Kg calibre 28"];
    document.getElementById("kg_calibre30").innerHTML = dataToShow["Kg calibre 30"];
    document.getElementById("kg_calibre32").innerHTML = dataToShow["Kg calibre 32"];
    document.getElementById("kg_calibre_categoría").innerHTML = 0;

    //precio_x_calibre
    document.getElementById("precio_calibre10").innerHTML = dataToShow["Costo calibre 10"];
    document.getElementById("precio_calibre12").innerHTML = dataToShow["Costo calibre 12"];
    document.getElementById("precio_calibre14").innerHTML = dataToShow["Costo calibre 14"];
    document.getElementById("precio_calibre16").innerHTML = dataToShow["Costo calibre 16"];
    document.getElementById("precio_calibre18").innerHTML = dataToShow["Costo calibre 18"];
    document.getElementById("precio_calibre20").innerHTML = dataToShow["Costo calibre 20"];
    document.getElementById("precio_calibre22").innerHTML = dataToShow["Costo calibre 22"];
    document.getElementById("precio_calibre24").innerHTML = dataToShow["Costo calibre 24"];
    document.getElementById("precio_calibre26").innerHTML = dataToShow["Costo calibre 26"];
    document.getElementById("precio_calibre28").innerHTML = dataToShow["Costo calibre 28"];
    document.getElementById("precio_calibre30").innerHTML = dataToShow["Costo calibre 30"];
    document.getElementById("precio_calibre32").innerHTML = dataToShow["Costo calibre 32"];
    document.getElementById("precio_calibre_categoría").innerHTML = 0;

    //total_x_calibre
    document.getElementById("total_calbre10").innerHTML = parseFloat(dataToShow["Kg calibre 10"]) * parseFloat(dataToShow["Costo calibre 10"]);
    document.getElementById("total_calbre12").innerHTML = parseFloat(dataToShow["Kg calibre 12"]) * parseFloat(dataToShow["Costo calibre 12"]);
    document.getElementById("total_calbre14").innerHTML = parseFloat(dataToShow["Kg calibre 14"]) * parseFloat(dataToShow["Costo calibre 14"]);
    document.getElementById("total_calbre16").innerHTML = parseFloat(dataToShow["Kg calibre 16"]) * parseFloat(dataToShow["Costo calibre 16"]);
    document.getElementById("total_calbre18").innerHTML = parseFloat(dataToShow["Kg calibre 18"]) * parseFloat(dataToShow["Costo calibre 18"]);
    document.getElementById("total_calbre20").innerHTML = parseFloat(dataToShow["Kg calibre 20"]) * parseFloat(dataToShow["Costo calibre 20"]);
    document.getElementById("total_calbre22").innerHTML = parseFloat(dataToShow["Kg calibre 22"]) * parseFloat(dataToShow["Costo calibre 22"]);
    document.getElementById("total_calbre24").innerHTML = parseFloat(dataToShow["Kg calibre 24"]) * parseFloat(dataToShow["Costo calibre 24"]);
    document.getElementById("total_calbre26").innerHTML = parseFloat(dataToShow["Kg calibre 26"]) * parseFloat(dataToShow["Costo calibre 26"]);
    document.getElementById("total_calbre28").innerHTML = parseFloat(dataToShow["Kg calibre 28"]) * parseFloat(dataToShow["Costo calibre 28"]);
    document.getElementById("total_calbre30").innerHTML = parseFloat(dataToShow["Kg calibre 30"]) * parseFloat(dataToShow["Costo calibre 30"]);
    document.getElementById("total_calbre32").innerHTML = parseFloat(dataToShow["Kg calibre 32"]) * parseFloat(dataToShow["Costo calibre 32"]);
    document.getElementById("total_calbre_categoría").innerHTML = 0


    //cajas_x_calibre
    document.getElementById("cajas10").innerHTML = 0;
    document.getElementById("cajas12").innerHTML = parseFloat(dataToShow["Kg calibre 12"]) / 10;
    document.getElementById("cajas14").innerHTML = parseFloat(dataToShow["Kg calibre 14"]) / 10;
    document.getElementById("cajas16").innerHTML = parseFloat(dataToShow["Kg calibre 16"]) / 10;
    document.getElementById("cajas18").innerHTML = parseFloat(dataToShow["Kg calibre 18"]) / 10;
    document.getElementById("cajas20").innerHTML = parseFloat(dataToShow["Kg calibre 20"]) / 10;
    document.getElementById("cajas22").innerHTML = parseFloat(dataToShow["Kg calibre 22"]) / 10;
    document.getElementById("cajas24").innerHTML = parseFloat(dataToShow["Kg calibre 24"]) / 10;
    document.getElementById("cajas26").innerHTML = parseFloat(dataToShow["Kg calibre 26"]) / 10;
    document.getElementById("cajas28").innerHTML = parseFloat(dataToShow["Kg calibre 28"]) / 10;
    document.getElementById("cajas30").innerHTML = parseFloat(dataToShow["Kg calibre 30"]) / 10;
    document.getElementById("cajas32").innerHTML = parseFloat(dataToShow["Kg calibre 32"]) / 10;
    document.getElementById("cajas_categoría").innerHTML = 0;

    //otras
    document.getElementById("promedio_fruta").innerHTML = dataToShow["Promedio fruta exportada"];
    document.getElementById("total_total").innerHTML = dataToShow["Total valor exportados"];
    if(parseFloat(dataToShow["Kg ingresados"]) !== 0){
        document.getElementById("prc_rendimiento").innerHTML = (parseFloat(dataToShow["Kg exportados"]) * 100) / parseFloat(dataToShow["Kg ingresados"]);
    }else{
        document.getElementById("prc_rendimiento").innerHTML = 0;
    }


    //tabla4
    document.getElementById("id_exportacion").innerHTML = dataToShow["Total valor exportados"];
    document.getElementById("id_descarte").innerHTML = dataToShow["Total valor descarte"];
    document.getElementById("id_subtotal").innerHTML = dataToShow["Subtotal a pagar"];
    document.getElementById("id_retefuente").innerHTML = dataToShow["Retefuente"];
    document.getElementById("id_asohofrucol").innerHTML = dataToShow["Ashofrucol"];
    document.getElementById("id_rete_ica").innerHTML = dataToShow["ReteICA"];
    document.getElementById("id_total").innerHTML = dataToShow["Total a pagar"];
    document.getElementById("id_total_retenciones").innerHTML = parseFloat(dataToShow["Retefuente"])+parseFloat(dataToShow["Ashofrucol"])+parseFloat(dataToShow["ReteICA"]);
    document.getElementById("id_residualidad").innerHTML = 0;


}


function cleanData(){

    document.getElementById("id_cedula").innerHTML = "";
    document.getElementById("id_direccion").innerHTML = "";
    document.getElementById("id_ica").innerHTML = "";
    document.getElementById("id_liquidacion").innerHTML = "";
    document.getElementById("id_predio").innerHTML = "";
    document.getElementById("id_proveedor").innerHTML = "";

    document.getElementById("id_canastillas").innerHTML = "";
    document.getElementById("id_descarte_proveedor").innerHTML = "";
    document.getElementById("id_deshidratacion").innerHTML = "";
    document.getElementById("id_fecha_liquidacion").innerHTML = "";
    document.getElementById("id_kg_descarte").innerHTML = "";
    document.getElementById("id_kg_exportables").innerHTML = "";
    document.getElementById("id_kg_ingresados").innerHTML = "";
    document.getElementById("id_valor_descarte").innerHTML = "";

    document.getElementById("cajas_categoría").innerHTML = "";
    document.getElementById("cajas10").innerHTML = "";
    document.getElementById("cajas12").innerHTML = "";
    document.getElementById("cajas14").innerHTML = "";
    document.getElementById("cajas16").innerHTML = "";
    document.getElementById("cajas18").innerHTML = "";
    document.getElementById("cajas20").innerHTML = "";
    document.getElementById("cajas22").innerHTML = "";
    document.getElementById("cajas24").innerHTML = "";
    document.getElementById("cajas26").innerHTML = "";
    document.getElementById("cajas28").innerHTML = "";
    document.getElementById("cajas30").innerHTML = "";
    document.getElementById("cajas32").innerHTML = "";
    document.getElementById("kg_calibre_categoría").innerHTML = "";
    document.getElementById("kg_calibre10").innerHTML = "";
    document.getElementById("kg_calibre12").innerHTML = "";
    document.getElementById("kg_calibre14").innerHTML = "";
    document.getElementById("kg_calibre16").innerHTML = "";
    document.getElementById("kg_calibre18").innerHTML = "";
    document.getElementById("kg_calibre20").innerHTML = "";
    document.getElementById("kg_calibre22").innerHTML = "";
    document.getElementById("kg_calibre24").innerHTML = "";
    document.getElementById("kg_calibre26").innerHTML = "";
    document.getElementById("kg_calibre28").innerHTML = "";
    document.getElementById("kg_calibre30").innerHTML = "";
    document.getElementById("kg_calibre32").innerHTML = "";
    document.getElementById("prc_rendimiento").innerHTML = "";
    document.getElementById("precio_calibre_categoría").innerHTML = "";
    document.getElementById("precio_calibre10").innerHTML = "";
    document.getElementById("precio_calibre12").innerHTML = "";
    document.getElementById("precio_calibre14").innerHTML = "";
    document.getElementById("precio_calibre16").innerHTML = "";
    document.getElementById("precio_calibre18").innerHTML = "";
    document.getElementById("precio_calibre20").innerHTML = "";
    document.getElementById("precio_calibre22").innerHTML = "";
    document.getElementById("precio_calibre24").innerHTML = "";
    document.getElementById("precio_calibre26").innerHTML = "";
    document.getElementById("precio_calibre28").innerHTML = "";
    document.getElementById("precio_calibre30").innerHTML = "";
    document.getElementById("precio_calibre32").innerHTML = "";
    document.getElementById("promedio_fruta").innerHTML = "";
    document.getElementById("total_calbre_categoría").innerHTML = "";
    document.getElementById("total_calbre10").innerHTML = "";
    document.getElementById("total_calbre12").innerHTML = "";
    document.getElementById("total_calbre14").innerHTML = "";
    document.getElementById("total_calbre16").innerHTML = "";
    document.getElementById("total_calbre18").innerHTML = "";
    document.getElementById("total_calbre20").innerHTML = "";
    document.getElementById("total_calbre22").innerHTML = "";
    document.getElementById("total_calbre24").innerHTML = "";
    document.getElementById("total_calbre26").innerHTML = "";
    document.getElementById("total_calbre28").innerHTML = "";
    document.getElementById("total_calbre30").innerHTML = "";
    document.getElementById("total_calbre32").innerHTML = "";
    document.getElementById("total_total").innerHTML = "";

    document.getElementById("id_exportacion").innerHTML = "";
    document.getElementById("id_descarte").innerHTML = "";
    document.getElementById("id_subtotal").innerHTML = "";
    document.getElementById("id_retefuente").innerHTML = "";
    document.getElementById("id_asohofrucol").innerHTML = "";
    document.getElementById("id_rete_ica").innerHTML = "";
    document.getElementById("id_total_retenciones").innerHTML = "";
    document.getElementById("id_residualidad").innerHTML = "";
    document.getElementById("id_total").innerHTML = "";

}



function formatNumberData(){

    //numeros
    formatNumberById("prc_rendimiento");
    formatNumberById("id_canastillas");
    formatNumberById("id_deshidratacion");
    formatNumberById("id_kg_descarte");
    formatNumberById("id_kg_exportables");
    formatNumberById("id_kg_ingresados");
    formatNumberById("cajas_categoría");
    formatNumberById("cajas10");
    formatNumberById("cajas12");
    formatNumberById("cajas14");
    formatNumberById("cajas16");
    formatNumberById("cajas18");
    formatNumberById("cajas20");
    formatNumberById("cajas22");
    formatNumberById("cajas24");
    formatNumberById("cajas26");
    formatNumberById("cajas28");
    formatNumberById("cajas30");
    formatNumberById("cajas32");
    formatNumberById("kg_calibre_categoría");
    formatNumberById("kg_calibre10");
    formatNumberById("kg_calibre12");
    formatNumberById("kg_calibre14");
    formatNumberById("kg_calibre16");
    formatNumberById("kg_calibre18");
    formatNumberById("kg_calibre20");
    formatNumberById("kg_calibre22");
    formatNumberById("kg_calibre24");
    formatNumberById("kg_calibre26");
    formatNumberById("kg_calibre28");
    formatNumberById("kg_calibre30");
    formatNumberById("kg_calibre32");
    formatNumberById("promedio_fruta");



    //numeros moneda
    //formatNumberMoneyById("total_total");
    formatNumberMoneyById("id_valor_descarte");
    //formatNumberMoneyById("prc_rendimiento");
    formatNumberMoneyById("precio_calibre_categoría");
    formatNumberMoneyById("precio_calibre10");
    formatNumberMoneyById("precio_calibre12");
    formatNumberMoneyById("precio_calibre14");
    formatNumberMoneyById("precio_calibre16");
    formatNumberMoneyById("precio_calibre18");
    formatNumberMoneyById("precio_calibre20");
    formatNumberMoneyById("precio_calibre22");
    formatNumberMoneyById("precio_calibre24");
    formatNumberMoneyById("precio_calibre26");
    formatNumberMoneyById("precio_calibre28");
    formatNumberMoneyById("precio_calibre30");
    formatNumberMoneyById("precio_calibre32");
    formatNumberMoneyById("total_calbre_categoría");
    formatNumberMoneyById("total_calbre10");
    formatNumberMoneyById("total_calbre12");
    formatNumberMoneyById("total_calbre14");
    formatNumberMoneyById("total_calbre16");
    formatNumberMoneyById("total_calbre18");
    formatNumberMoneyById("total_calbre20");
    formatNumberMoneyById("total_calbre22");
    formatNumberMoneyById("total_calbre24");
    formatNumberMoneyById("total_calbre26");
    formatNumberMoneyById("total_calbre28");
    formatNumberMoneyById("total_calbre30");
    formatNumberMoneyById("total_calbre32");
    formatNumberMoneyById("total_total");
    formatNumberMoneyById("id_exportacion");
    formatNumberMoneyById("id_descarte");
    formatNumberMoneyById("id_subtotal");
    formatNumberMoneyById("id_retefuente");
    formatNumberMoneyById("id_asohofrucol");
    formatNumberMoneyById("id_rete_ica");
    formatNumberMoneyById("id_total_retenciones");
    formatNumberMoneyById("id_residualidad");
    formatNumberMoneyById("id_total");

}

//numeros (millares y 2decimales)
function formatNumberById(id_element) {
    //Intl.NumberFormat('es-CO').format((5000000.009897).toFixed(2))
    document.getElementById(id_element).innerHTML = Intl.NumberFormat('es-CO').format((parseFloat(document.getElementById(id_element).innerHTML)).toFixed(2));

}

//moneda ($)
function formatNumberMoneyById(id_element) {
    //"$" + Intl.NumberFormat('es-CO').format((5000000.009897).toFixed(2))
    document.getElementById(id_element).innerHTML = "$ " + Intl.NumberFormat('es-CO').format((parseFloat(document.getElementById(id_element).innerHTML)).toFixed(2));
}
