db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_produccionhistorico",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //--variables
                    //"lote id" : "5d78370b275981a0c35fd15a",
                    //"kg cosecha principal" : 0,
                    //"kg produccion traviesa" : 0,
                    //"anio" : 2022,

                    {
                        "$project": {
                            "Point": 0
                            , "supervisor": 0
                            , "uDate": 0
                            , "capture": 0
                            , "uid": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "lote_oid": { "$toObjectId": "$lote id" }
                        }
                    }




                    //--obtener (num arboles productivos) x (lote id,anio)
                    //form_puentearbolesproductivos
                    , {
                        "$lookup": {
                            "from": "form_puentearbolesproductivos",
                            "as": "data_arboles_productivos",
                            "let": {
                                "lote_id": "$lote id"
                                , "anio": "$anio"
                            },
                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [

                                                { "$eq": ["$lote id", "$$lote_id"] }
                                                ,
                                                { "$eq": ["$anio", "$$anio"] }

                                            ]
                                        }
                                    }
                                }

                            ]
                        }
                    }

                    , { "$unwind": "$data_arboles_productivos" }


                    , {
                        "$addFields": {
                            "num_arboles_productivos": { "$ifNull": ["$data_arboles_productivos.num arboles productivos", 0] }
                        }
                    }

                    , {
                        "$project": {
                            "data_arboles_productivos": 0
                        }
                    }



                    //--obtener Ha (Numero de hectareas) de lote en cartografia
                    , {
                        "$lookup": {
                            "from": "cartography",
                            "as": "info_lote",
                            "let": {
                                "lote_oid": "$lote_oid"
                            },
                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$_id", "$$lote_oid"] }
                                            ]
                                        }
                                    }
                                }

                                ,
                                {
                                    "$addFields": {
                                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                                    }
                                },
                                {
                                    "$addFields": {
                                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                                    }
                                },
                                {
                                    "$addFields": {
                                        "split_path_oid": {
                                            "$concatArrays": [
                                                "$split_path_padres_oid",
                                                ["$_id"]
                                            ]
                                        }
                                    }
                                },

                                {
                                    "$lookup": {
                                        "from": "cartography",
                                        "localField": "split_path_oid",
                                        "foreignField": "_id",
                                        "as": "objetos_del_cultivo"
                                    }
                                },


                                {
                                    "$addFields": {
                                        "finca": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$finca",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                                {
                                    "$lookup": {
                                        "from": "farms",
                                        "localField": "finca._id",
                                        "foreignField": "_id",
                                        "as": "finca"
                                    }
                                },
                                { "$unwind": "$finca" },

                                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                                {
                                    "$addFields": {
                                        "bloque": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$bloque",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                                {
                                    "$addFields": {
                                        "lote": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$lote",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                                {
                                    "$project": {
                                        "variable_cartografia": 0,
                                        "split_path_padres": 0,
                                        "split_path_padres_oid": 0,
                                        "variable_cartografia_oid": 0,
                                        "split_path_oid": 0,
                                        "objetos_del_cultivo": 0,
                                    }
                                }




                            ]
                        }
                    }

                    , { "$unwind": "$info_lote" }


                    , {
                        "$addFields": {
                            "area_Ha": { "$ifNull": ["$info_lote.properties.custom.Numero de hectareas.value", 0] }

                            , "finca": { "$ifNull": ["$info_lote.finca", ""] }
                            , "bloque": { "$ifNull": ["$info_lote.bloque", ""] }
                            , "lote": { "$ifNull": ["$info_lote.lote", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "info_lote": 0
                        }
                    }



                    //---variables calculadas
                    //-kg_cosecha
                    //-kg_traviesa
                    //-kg_TOTAL

                    //*kg_arbol
                    //*kg_ha

                    , {
                        "$addFields": {
                            "kg TOTAL": {
                                "$sum": [
                                    { "$toDouble": "$kg cosecha principal" }
                                    , { "$toDouble": "$kg produccion traviesa" }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "kg_arbol_cosecha": {
                                "$cond": {
                                    "if": { "$eq": ["$num_arboles_productivos", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg cosecha principal", "$num_arboles_productivos"]
                                    }
                                }
                            }
                            , "kg_ha_cosecha": {
                                "$cond": {
                                    "if": { "$eq": ["$area_Ha", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg cosecha principal", "$area_Ha"]
                                    }
                                }
                            },

                            "kg_arbol_traviesa": {
                                "$cond": {
                                    "if": { "$eq": ["$num_arboles_productivos", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg produccion traviesa", "$num_arboles_productivos"]
                                    }
                                }
                            }
                            , "kg_ha_traviesa": {
                                "$cond": {
                                    "if": { "$eq": ["$area_Ha", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg produccion traviesa", "$area_Ha"]
                                    }
                                }
                            },

                            "kg_arbol_TOTAL": {
                                "$cond": {
                                    "if": { "$eq": ["$num_arboles_productivos", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg TOTAL", "$num_arboles_productivos"]
                                    }
                                }
                            }
                            , "kg_ha_TOTAL": {
                                "$cond": {
                                    "if": { "$eq": ["$area_Ha", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$kg TOTAL", "$area_Ha"]
                                    }
                                }
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }


                    , {
                        "$project": {
                            "lote id": 0
                            , "lote_oid": 0

                            , "_id": 0
                        }
                    }



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
