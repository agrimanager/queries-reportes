


  db.users.aggregate(
    [


          { "$limit": 1 },
          {
              "$lookup": {
                  "from": "form_registrodeclima",
                  "as": "data",
                  "let": {},
                  "pipeline": [

                      {
                          "$project": {
                              "Finca": 1,
                              "Precipitacion mm": 1,
                              "Fecha": 1
                          }
                      },

                      {
                          "$project": {
                              "_id": 0
                          }
                      }

                  ]
              }
          }


          , {
              "$project":
              {
                  "datos": {
                      "$concatArrays": [
                          "$data"
                          , []
                      ]
                  }
              }
          }

          , { "$unwind": "$datos" }
          , { "$replaceRoot": { "newRoot": "$datos" } }


          , { "$addFields": { "variable_fecha": "$Fecha" } }
          , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
          , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


          { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
          { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
          { "$match": { "anio_variable_fecha": { "$lt": 3000 } } },

          {
              "$project": {
                  "variable_fecha": 0
                  , "type_variable_fecha": 0
                  , "anio_variable_fecha": 0
              }
          }




          , { "$addFields": { "rgDate": "$Fecha" } }
          , {
              "$addFields": {
                  "num_anio": { "$year": { "date": "$rgDate" } },
                  "num_mes": { "$month": { "date": "$rgDate" } },
                  "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
                  "num_semana": { "$week": { "date": "$rgDate" } }
              }
          }


          , {
              "$addFields": {
                  "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                  , "Mes_Txt": {
                      "$switch": {
                          "branches": [
                              { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                              { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                              { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                              { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                              { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                              { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                              { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                              { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                              { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                              { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                              { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                              { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                          ],
                          "default": "Mes desconocido"
                      }
                  }
              }
          }




      ]

  )
