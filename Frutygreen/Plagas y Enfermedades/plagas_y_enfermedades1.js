db.form_plagasyenfermedades.aggregate(
    [

        //cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0
            }
        }



        //arboles_censados_x_lote
        //....
        //....
        //....



        //----------------Plagas

        // 	"Monalonion" : "0",
        // 	"Mosca Blanca" : "0",
        // 	"Heilipus" : "0",
        // 	"Masticadores" : "0",
        // 	"Marceño" : "0",
        // 	"Copturominus" : "0",
        // 	"Compsus" : "0",
        // 	"Mosca Ovario" : "0",
        // 	"BombaCoccus" : "0",
        // 	"Escama" : "0",
        // 	"Arrieras" : "0",

        //--con sector

        //  "Trips" : "0",
        // 	"Trips en malezas" : "0",

        ////---acaro
        // 	"Acaro" : "0",
        // 	"Huevo" : "2",
        // 	"Movil" : "2",

        ////---Stenoma
        // 	"Stenoma" : "0",
        // 	"Rama" : "2",
        // 	"Fruto" : "2",



        //----------------Enfermedades

        // 	"Verticillium en raiz" : "0",
        // 	"Roña en fruta" : "0",
        // 	"Lenticelosis en fruta" : "0",
        // 	"Chancro en tallo" : "0",
        // 	"Lasidiplodia en tallo" : "0",
        // 	"Phytophthora en raiz" : "0",
        // 	"Fusarium en raiz" : "0",

        //--con sector

        // 	"Antracnosis en rama" : "0",
        // 	"Antracnosis en fruta" : "0",
        // 	"Antracnosis en pedunculo" : "0",

        // 	"Fumagina en fruta" : "0",
        // 	"Fumagina en hoja" : "0",

        // 	"Pseudocercospora en fruto" : "0",
        // 	"Pseudocercospora en hoja" : "0",







        //=========ARRAY CON DATOS
        , {
            "$addFields": {
                "data_sanidad": [

                    //PLAGAS
                    // {
                    //     "tipo": "Plaga",
                    //     "sector": "xxxx",
                    //     "nombre": "xxxx",
                    //     "cantidad": { "$ifNull": [{ "$toDouble": "$xxxx" }, 0] }
                    // },

                    //---CONCAT
                    // {"tipo": "Plaga","sector": "unico","nombre": "
                    // xxxx
                    // ","cantidad": { "$ifNull": [{ "$toDouble": "$
                    // xxxx
                    // " }, 0] }},

                    { "tipo": "Plaga", "sector": "unico", "nombre": "Arrieras", "cantidad": { "$ifNull": [{ "$toDouble": "$Arrieras" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "BombaCoccus", "cantidad": { "$ifNull": [{ "$toDouble": "$BombaCoccus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Compsus", "cantidad": { "$ifNull": [{ "$toDouble": "$Compsus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Copturominus", "cantidad": { "$ifNull": [{ "$toDouble": "$Copturominus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Escama", "cantidad": { "$ifNull": [{ "$toDouble": "$Escama" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Heilipus", "cantidad": { "$ifNull": [{ "$toDouble": "$Heilipus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Marceño", "cantidad": { "$ifNull": [{ "$toDouble": "$Marceño" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Masticadores", "cantidad": { "$ifNull": [{ "$toDouble": "$Masticadores" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Monalonion", "cantidad": { "$ifNull": [{ "$toDouble": "$Monalonion" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Mosca Blanca", "cantidad": { "$ifNull": [{ "$toDouble": "$Mosca Blanca" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Mosca Ovario", "cantidad": { "$ifNull": [{ "$toDouble": "$Mosca Ovario" }, 0] } },

                    //---con sector
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Trips", "cantidad": { "$ifNull": [{ "$toDouble": "$Trips" }, 0] } },
                    { "tipo": "Plaga", "sector": "Malezas", "nombre": "Trips", "cantidad": { "$ifNull": [{ "$toDouble": "$Trips en malezas" }, 0] } },

                    // { "tipo": "Plaga", "sector": "unico", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Acaro" }, 0] } },
                    { "tipo": "Plaga", "sector": "Huevo", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Huevo" }, 0] } },
                    { "tipo": "Plaga", "sector": "Movil", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Movil" }, 0] } },

                    // { "tipo": "Plaga", "sector": "unico", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Stenoma" }, 0] } },
                    { "tipo": "Plaga", "sector": "Rama", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Rama" }, 0] } },
                    { "tipo": "Plaga", "sector": "Fruto", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Fruto" }, 0] } },




                    //ENFERMEDADES
                    //---CONCAT
                    // {"tipo": "Enfermedad","sector": "
                    // SECTOR
                    // ","nombre": "
                    // NOMBRE
                    // ","cantidad": { "$ifNull": [{ "$toDouble": "$
                    // VARIABLE
                    // " }, 0] }},


                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "pedunculo", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en pedunculo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "rama", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en rama" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "tallo", "nombre": "Chancro", "cantidad": { "$ifNull": [{ "$toDouble": "$Chancro en tallo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Fumagina", "cantidad": { "$ifNull": [{ "$toDouble": "$Fumagina en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "hoja", "nombre": "Fumagina", "cantidad": { "$ifNull": [{ "$toDouble": "$Fumagina en hoja" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Fusarium", "cantidad": { "$ifNull": [{ "$toDouble": "$Fusarium en raiz" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "tallo", "nombre": "Lasidiplodia", "cantidad": { "$ifNull": [{ "$toDouble": "$Lasidiplodia en tallo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Lenticelosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Lenticelosis en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Phytophthora", "cantidad": { "$ifNull": [{ "$toDouble": "$Phytophthora en raiz" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Pseudocercospora", "cantidad": { "$ifNull": [{ "$toDouble": "$Pseudocercospora en fruto" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "hoja", "nombre": "Pseudocercospora", "cantidad": { "$ifNull": [{ "$toDouble": "$Pseudocercospora en hoja" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Roña", "cantidad": { "$ifNull": [{ "$toDouble": "$Roña en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Verticillium", "cantidad": { "$ifNull": [{ "$toDouble": "$Verticillium en raiz" }, 0] } },


                ]
            }
        }








    ]

)
