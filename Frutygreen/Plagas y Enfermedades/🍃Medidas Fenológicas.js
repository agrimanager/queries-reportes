db.form_plagasyenfermedades.aggregate(
    [


        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-31T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5d783735b5bf83f78c49029a"),//la estrella
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte


        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "$_id"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Point": 0,
                "Arbol": 0,

                "uid": 0
            }
        },


        {
            "$addFields": {
                "anio": { "$year": "$rgDate" },
                "mes": { "$month": "$rgDate" },
                "semana": { "$isoWeek": "$rgDate" }
            }
        },

        {
            "$addFields": {
                "mes_txt": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$mes", 1] },
                                "then": "01-Enero"
                            },
                            {
                                "case": { "$eq": ["$mes", 2] },
                                "then": "02-Febrero"
                            },
                            {
                                "case": { "$eq": ["$mes", 3] },
                                "then": "03-Marzo"
                            },
                            {
                                "case": { "$eq": ["$mes", 4] },
                                "then": "04-Abril"
                            },
                            {
                                "case": { "$eq": ["$mes", 5] },
                                "then": "05-Mayo"
                            },
                            {
                                "case": { "$eq": ["$mes", 6] },
                                "then": "06-Junio"
                            },
                            {
                                "case": { "$eq": ["$mes", 7] },
                                "then": "07-Julio"
                            },
                            {
                                "case": { "$eq": ["$mes", 8] },
                                "then": "08-Agosto"
                            },
                            {
                                "case": { "$eq": ["$mes", 9] },
                                "then": "09-Septiembre"
                            },
                            {
                                "case": { "$eq": ["$mes", 10] },
                                "then": "10-Octubre"
                            },
                            {
                                "case": { "$eq": ["$mes", 11] },
                                "then": "11-Noviembre"
                            },
                            {
                                "case": { "$eq": ["$mes", 12] },
                                "then": "12-Diciembre"
                            }
                        ],
                        "default": "sin datos"
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "form_registrodeclima",
                "let": {
                    "anio": "$anio",
                    "mes": "$mes",
                    "finca": "$finca"
                },

                "pipeline": [
                    { "$addFields": { "farm_id": { "$toObjectId": "$Point.farm" } } },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm_id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },
                    {
                        "$addFields": {
                            "anio": { "$year": "$rgDate" },
                            "mes": { "$month": "$rgDate" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$anio", "$$anio"] },
                                    { "$eq": ["$mes", "$$mes"] },
                                    { "$eq": ["$finca", "$$finca"] }
                                ]
                            }
                        }
                    },

                    {
                        "$group": {
                            "_id": null,
                            "precipitaciones": { "$sum": "$Precipitacion mm" }
                            , "precipitaciones prom": { "$avg": "$Precipitacion mm" }
                        }
                    }
                ],
                "as": "data_clima"
            }
        },

        {
            "$addFields": {
                "precipitaciones_mm": {
                    "$ifNull": [{ "$arrayElemAt": ["$data_clima.precipitaciones", 0] }, 0]
                }
                , "precipitaciones_mm PROM": {
                    "$ifNull": [{ "$arrayElemAt": ["$data_clima.precipitaciones prom", 0] }, 0]
                }
            }
        },


        {
            "$addFields": {
                "precipitaciones_mm PROM": { "$divide": [{ "$subtract": [{ "$multiply": ["$precipitaciones_mm PROM", 100] }, { "$mod": [{ "$multiply": ["$precipitaciones_mm PROM", 100] }, 1] }] }, 100] }
            }
        },

        {
            "$addFields": {
                "data_arr": [
                    {
                        "type": "vegetativo",
                        "value": "$Vegetativo"
                    },
                    {
                        "type": "hinchamiento_yemas",
                        "value": "$Hinchamiento de yemas"
                    },
                    {
                        "type": "apertura_floral",
                        "value": "$Apertura floral"
                    },
                    {
                        "type": "cuaje",
                        "value": "$Cuaje"
                    },
                    {
                        "type": "fruta_canica",
                        "value": "$Fruta canica"
                    },
                    {
                        "type": "fruta_pimpon",
                        "value": "$Fruta pimpon"
                    },
                    {
                        "type": "madurez_fisiologica",
                        "value": "$Madurez fisiologica"
                    },
                    {
                        "type": "flujo_radiculares",
                        "value": "$Flujo radiculares"
                    },
                    {
                        "type": "enlongacion_yema_floracion",
                        "value": "$Enlongacion de yemas y floracion"
                    },
                    {
                        "type": "aborto_frutas",
                        "value": "$Aborto de frutos"
                    }
                ]
            }
        },

        { "$unwind": "$data_arr" },

        {
            "$addFields": {
                "fenologia_tipo": "$data_arr.type",
                "fenologia_valor": {
                    "$ifNull": [{ "$toInt": "$data_arr.value" }, 0]
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "anio": "$anio",
                    "mes": "$mes",
                    "semana": "$semana",
                    "fenologia_tipo": "$fenologia_tipo"
                },
                "fenologia_prom": { "$avg": "$fenologia_valor" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "fenologia_promedio_semana": "$fenologia_prom" }
                    ]
                }
            }
        },



        {
            "$project": {
                "anio": 1,
                "mes": 1,
                "semana": 1,
                "mes_txt": 1,
                "finca": 1,
                "bloque": 1,
                "lote": 1,
                "linea": 1,
                "arbol": 1,
                "precipitaciones_mm": 1,
                "precipitaciones_mm PROM": 1,
                "fenologia_tipo": 1,
                "fenologia_valor": 1,
                "fenologia_promedio_semana": 1,
                "supervisor": 1,
                "rgDate": "$rgDate",
                "uDate": "$uDate"
            }
        }



    ]

)
