db.form_plagasyenfermedades.aggregate(
    [

        //cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0
            }
        }


        // //fechas

        // //---info fechas
        // , { "$addFields": { "variable_fecha": "$rgDate" } }
        // , {
        //     "$addFields": {
        //         "num_anio": { "$year": { "date": "$variable_fecha" } },
        //         "num_mes": { "$month": { "date": "$variable_fecha" } },
        //         "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
        //         "num_semana": { "$week": { "date": "$variable_fecha" } },
        //         "num_dia_semana": { "$dayOfWeek": { "date": "$variable_fecha" } }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }

        //         , "Mes_Txt": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
        //                     { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
        //                     { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
        //                     { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
        //                     { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
        //                     { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
        //                     { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
        //                     { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
        //                     { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
        //                     { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
        //                     { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
        //                     { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
        //                 ],
        //                 "default": "Mes desconocido"
        //             }
        //         }


        //         , "Dia_Txt": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
        //                     { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
        //                     { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
        //                     { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
        //                     { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
        //                     { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
        //                     { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
        //                 ],
        //                 "default": "dia de la semana desconocido"
        //             }
        //         }
        //     }
        // },

        // //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        // { "$project": { "num_dia_semana": 0, "variable_fecha": 0 } }




        //plantas_dif_censadas_x_lote_x_fecha
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    // "linea": "$linea",
                    "arbol": "$arbol"

                    // , "fecha": "$Fecha_Txt"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                    // "linea": "$_id.linea",

                    // , "fecha": "$_id.fecha"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_fecha": { "$sum": 1 }

            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote_x_fecha": "$plantas_dif_censadas_x_lote_x_fecha"
                        }
                    ]
                }
            }
        }



        //----------------Plagas

        // 	"Monalonion" : "0",
        // 	"Mosca Blanca" : "0",
        // 	"Heilipus" : "0",
        // 	"Masticadores" : "0",----
        // 	"Marceño" : "0",
        // 	"Copturominus" : "0",
        // 	"Compsus" : "0",
        // 	"Mosca Ovario" : "0",
        // 	"BombaCoccus" : "0",
        // 	"Escama" : "0",
        // 	"Arrieras" : "0",

        //--con sector

        //  "Trips" : "0",
        // 	"Trips en malezas" : "0",

        ////---acaro
        // 	"Acaro" : "0",
        // 	"Huevo" : "2",
        // 	"Movil" : "2",

        ////---Stenoma
        // 	"Stenoma" : "0",
        // 	"Rama" : "2",
        // 	"Fruto" : "2",



        //----------------Enfermedades

        // 	"Verticillium en raiz" : "0",
        // 	"Roña en fruta" : "0",
        // 	"Lenticelosis en fruta" : "0",
        // 	"Chancro en tallo" : "0",
        // 	"Lasidiplodia en tallo" : "0",
        // 	"Phytophthora en raiz" : "0",
        // 	"Fusarium en raiz" : "0",

        //--con sector

        // 	"Antracnosis en rama" : "0",
        // 	"Antracnosis en fruta" : "0",
        // 	"Antracnosis en pedunculo" : "0",

        // 	"Fumagina en fruta" : "0",
        // 	"Fumagina en hoja" : "0",

        // 	"Pseudocercospora en fruto" : "0",
        // 	"Pseudocercospora en hoja" : "0",




        //=========ARREGLO DE DATOS
        //Acaro
        , { "$addFields": { "Acaro": { "$ifNull": ["$Acaro", ""] } } }
        , {
            "$addFields": {
                "Acaro": {
                    "$cond": {
                        "if": { "$eq": ["$Acaro", ""] },
                        "then": "0",
                        "else": "$Acaro"
                    }
                }
            }
        }
        //Stenoma
        , { "$addFields": { "Stenoma": { "$ifNull": ["$Stenoma", ""] } } }
        , {
            "$addFields": {
                "Stenoma": {
                    "$cond": {
                        "if": { "$eq": ["$Stenoma", ""] },
                        "then": "0",
                        "else": "$Stenoma"
                    }
                }
            }
        }



        //=========ARRAY CON DATOS
        , {
            "$addFields": {
                "data_sanidad": [

                    //PLAGAS
                    // {
                    //     "tipo": "Plaga",
                    //     "sector": "xxxx",
                    //     "nombre": "xxxx",
                    //     "cantidad": { "$ifNull": [{ "$toDouble": "$xxxx" }, 0] }
                    // },

                    //---CONCAT
                    // {"tipo": "Plaga","sector": "unico","nombre": "
                    // xxxx
                    // ","cantidad": { "$ifNull": [{ "$toDouble": "$
                    // xxxx
                    // " }, 0] }},

                    { "tipo": "Plaga", "sector": "unico", "nombre": "Arrieras", "cantidad": { "$ifNull": [{ "$toDouble": "$Arrieras" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "BombaCoccus", "cantidad": { "$ifNull": [{ "$toDouble": "$BombaCoccus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Compsus", "cantidad": { "$ifNull": [{ "$toDouble": "$Compsus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Copturominus", "cantidad": { "$ifNull": [{ "$toDouble": "$Copturominus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Escama", "cantidad": { "$ifNull": [{ "$toDouble": "$Escama" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Heilipus", "cantidad": { "$ifNull": [{ "$toDouble": "$Heilipus" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Marceño", "cantidad": { "$ifNull": [{ "$toDouble": "$Marceño" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Masticadores", "cantidad": { "$ifNull": [{ "$toDouble": "$Masticadores" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Monalonion", "cantidad": { "$ifNull": [{ "$toDouble": "$Monalonion" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Mosca Blanca", "cantidad": { "$ifNull": [{ "$toDouble": "$Mosca Blanca" }, 0] } },//danger (-)
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Mosca Ovario", "cantidad": { "$ifNull": [{ "$toDouble": "$Mosca Ovario" }, 0] } },
                    //new
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Comedores de follaje", "cantidad": { "$ifNull": [{ "$toDouble": "$Comedores de follaje" }, 0] } },
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Comedores de follaje", "cantidad": { "$ifNull": [{ "$toDouble": "$Comedor de follaje" }, 0] } },

                    //---con sector
                    { "tipo": "Plaga", "sector": "unico", "nombre": "Trips", "cantidad": { "$ifNull": [{ "$toDouble": "$Trips" }, 0] } },
                    { "tipo": "Plaga", "sector": "Malezas", "nombre": "Trips", "cantidad": { "$ifNull": [{ "$toDouble": "$Trips en malezas" }, 0] } },

                    { "tipo": "Plaga", "sector": "unico", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Acaro" }, 0] } },//arreglo
                    { "tipo": "Plaga", "sector": "Huevo", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Huevo" }, 0] } },
                    { "tipo": "Plaga", "sector": "Movil", "nombre": "Acaro", "cantidad": { "$ifNull": [{ "$toDouble": "$Movil" }, 0] } },

                    { "tipo": "Plaga", "sector": "unico", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Stenoma" }, 0] } },//arreglo
                    { "tipo": "Plaga", "sector": "Rama", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Rama" }, 0] } },
                    { "tipo": "Plaga", "sector": "Fruto", "nombre": "Stenoma", "cantidad": { "$ifNull": [{ "$toDouble": "$Fruto" }, 0] } },




                    //ENFERMEDADES
                    //---CONCAT
                    // {"tipo": "Enfermedad","sector": "
                    // SECTOR
                    // ","nombre": "
                    // NOMBRE
                    // ","cantidad": { "$ifNull": [{ "$toDouble": "$
                    // VARIABLE
                    // " }, 0] }},


                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "pedunculo", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en pedunculo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "rama", "nombre": "Antracnosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Antracnosis en rama" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "tallo", "nombre": "Chancro", "cantidad": { "$ifNull": [{ "$toDouble": "$Chancro en tallo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Fumagina", "cantidad": { "$ifNull": [{ "$toDouble": "$Fumagina en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "hoja", "nombre": "Fumagina", "cantidad": { "$ifNull": [{ "$toDouble": "$Fumagina en hoja" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Fusarium", "cantidad": { "$ifNull": [{ "$toDouble": "$Fusarium en raiz" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "tallo", "nombre": "Lasidiplodia", "cantidad": { "$ifNull": [{ "$toDouble": "$Lasidiplodia en tallo" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Lenticelosis", "cantidad": { "$ifNull": [{ "$toDouble": "$Lenticelosis en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Phytophthora", "cantidad": { "$ifNull": [{ "$toDouble": "$Phytophthora en raiz" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Pseudocercospora", "cantidad": { "$ifNull": [{ "$toDouble": "$Pseudocercospora en fruto" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "hoja", "nombre": "Pseudocercospora", "cantidad": { "$ifNull": [{ "$toDouble": "$Pseudocercospora en hoja" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "fruta", "nombre": "Roña", "cantidad": { "$ifNull": [{ "$toDouble": "$Roña en fruta" }, 0] } },
                    { "tipo": "Enfermedad", "sector": "raiz", "nombre": "Verticillium", "cantidad": { "$ifNull": [{ "$toDouble": "$Verticillium en raiz" }, 0] } }


                ]
            }
        }



        , {
            "$addFields": {
                "data_sanidad": {
                    "$filter": {
                        "input": "$data_sanidad",
                        "as": "item",
                        "cond": { "$ne": ["$$item.cantidad", 0] }
                    }
                }
            }
        }


        , {
            "$match": {
                "data_sanidad": { "$ne": [] }
            }
        }

        , {
            "$unwind": {
                "path": "$data_sanidad",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$project": {
                "finca": 1,
                "bloque": 1,
                "lote": 1,
                "linea": 1,
                "arbol": 1,
                // "num_anio": 1,
                // "num_mes": 1,
                // "num_dia_mes": 1,
                // "num_semana": 1,
                // "Fecha_Txt": 1,
                // "Mes_Txt": 1,
                // "Dia_Txt": 1,
                "plantas_dif_censadas_x_lote_x_fecha": 1,
                "data_sanidad": 1
            }
        }


        , {
            "$addFields": {
                "patogeno_nombre": "$data_sanidad.nombre",
                "tipo": "$data_sanidad.tipo",
                "sector": "$data_sanidad.sector",
                "cantidad": "$data_sanidad.cantidad"
            }
        }


        , {
            "$project": {
                "data_sanidad": 0
            }
        }




        //---AGRUPACIONES

        //plantas_dif_censadas_x_lote_x_fecha_x_patogeno
        // , {
        //     "$group": {
        //         "_id": {
        //             "finca": "$finca",
        //             "bloque": "$bloque",
        //             "lote": "$lote",
        //             // "linea": "$linea",
        //             "arbol": "$arbol"

        //             , "fecha": "$Fecha_Txt"
        //             , "patogeno_nombre": "$patogeno_nombre"
        //         }
        //         , "data": { "$push": "$$ROOT" }
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "finca": "$_id.finca",
        //             "bloque": "$_id.bloque",
        //             "lote": "$_id.lote"
        //             // "linea": "$_id.linea",
        //             // "arbol": "$arbol"

        //             , "fecha": "$_id.fecha"
        //             , "patogeno_nombre": "$_id.patogeno_nombre"

        //         }
        //         , "data": { "$push": "$$ROOT" }
        //         , "plantas_dif_censadas_x_lote_x_fecha_x_patogeno": { "$sum": 1 }

        //     }
        // }

        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data.data",
        //                 {
        //                     "plantas_dif_censadas_x_lote_x_fecha_x_patogeno": "$plantas_dif_censadas_x_lote_x_fecha_x_patogeno"
        //                 }
        //             ]
        //         }
        //     }
        // }


        //---
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    // "linea": "$linea",
                    "arbol": "$arbol"

                    , "patogeno_nombre": "$patogeno_nombre"
                    , "patogeno_tipo": "$tipo"

                    // , "num_anio": "$num_anio"
                    // , "mes": "$Mes_Txt"
                    // , "fecha": "$Fecha_Txt"

                    , "plantas_dif_censadas_x_lote_x_fecha": "$plantas_dif_censadas_x_lote_x_fecha"

                }
                , "sum_cantidad_x_arbol": { "$sum": "$cantidad" }
                // , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                    // "linea": "$_id.linea",
                    // "arbol": "$arbol"

                    , "patogeno_nombre": "$_id.patogeno_nombre"
                    , "patogeno_tipo": "$_id.patogeno_tipo"


                    // , "num_anio": "$_id.num_anio"
                    // , "mes": "$_id.mes"
                    // , "fecha": "$_id.fecha"

                    , "plantas_dif_censadas_x_lote_x_fecha": "$_id.plantas_dif_censadas_x_lote_x_fecha"


                }
                , "plantas_dif_censadas_x_lote_x_fecha_x_patogeno": { "$sum": 1 }
                , "sum_cantidad_x_lote": { "$sum": "$sum_cantidad_x_arbol" }
                // , "data": { "$push": "$$ROOT" }

            }
        }

        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_lote_x_fecha_x_patogeno": "$plantas_dif_censadas_x_lote_x_fecha_x_patogeno",
                            "sum_cantidad_x_lote": "$sum_cantidad_x_lote"
                        }
                    ]
                }
            }
        }


        //---indicadores
        , {
            "$addFields": {
                "num_arboles_censados": "$plantas_dif_censadas_x_lote_x_fecha"
                , "num_arboles_patogeno": "$plantas_dif_censadas_x_lote_x_fecha_x_patogeno"
                , "cantidad_infestacion": "$sum_cantidad_x_lote"
            }
        }

        , {
            "$project": {
                "plantas_dif_censadas_x_lote_x_fecha": 0,
                "plantas_dif_censadas_x_lote_x_fecha_x_patogeno": 0,
                "sum_cantidad_x_lote": 0
            }
        }


        //1)incidencia
        , {
            "$addFields": {
                "pct_incidencia": {
                    "$cond": {
                        "if": { "$eq": ["$num_arboles_censados", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [{ "$divide": ["$num_arboles_patogeno", "$num_arboles_censados"] }, 100]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "pct_incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia", 100] }, 1] }] }, 100] }
            }
        }


        //2)infestacion
        //2.1)infestacion_general
        , {
            "$addFields": {
                "infestacion_general": {
                    "$cond": {
                        "if": { "$eq": ["$num_arboles_censados", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$cantidad_infestacion", { "$multiply": ["$num_arboles_censados", 4] }]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                //"infestacion_general": { "$divide": [{ "$subtract": [{ "$multiply": ["$infestacion_general", 100] }, { "$mod": [{ "$multiply": ["$infestacion_general", 100] }, 1] }] }, 100] }
                "infestacion_general": { "$divide": [{ "$subtract": [{ "$multiply": ["$infestacion_general", 10000] }, { "$mod": [{ "$multiply": ["$infestacion_general", 10000] }, 1] }] }, 10000] }
            }
        }


        //2.2)infestacion_especifica
        , {
            "$addFields": {
                "infestacion_especifica": {
                    "$cond": {
                        "if": { "$eq": ["$num_arboles_patogeno", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$cantidad_infestacion", { "$multiply": ["$num_arboles_patogeno", 4] }]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "infestacion_especifica": { "$divide": [{ "$subtract": [{ "$multiply": ["$infestacion_especifica", 10000] }, { "$mod": [{ "$multiply": ["$infestacion_especifica", 10000] }, 1] }] }, 10000] }
            }
        }










    ]

)
