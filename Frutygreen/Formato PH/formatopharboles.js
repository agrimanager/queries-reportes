db.users.aggregate(
    [



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_formatopharboles",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },

                "pipeline": [

                    {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } }


                    , {
                        "$project": {
                            "id_finca": 0
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_puenteformatopharboles",
                            "as": "data_puente",
                            "let": {
                                "num_arbol": "$numero arbol",
                                "finca": "$finca"
                            },

                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$finca", "$$finca"] },
                                                { "$eq": ["$numero arbol", "$$num_arbol"] }
                                            ]
                                        }
                                    }
                                }

                                , { "$unwind": "$arbol cartografia.features" }


                                , { "$addFields": { "nombre_arbol": { "$ifNull": ["$arbol cartografia.features.properties.name", "no existe"] } } }




                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$data_puente",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , { "$addFields": { "nombre_arbol": { "$ifNull": ["$data_puente.nombre_arbol", "no existe"] } } }

                    , {
                        "$project": {
                            "data_puente": 0

                            , "Point": 0
                        }
                    }





                    , {
                        "$addFields": {
                            "rgDate": "$Fecha"
                        }
                    }




                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$rgDate" } },
                            "num_mes": { "$month": { "date": "$rgDate" } }
                        }
                    }

                    , {
                        "$addFields": {
                            "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                            , "Mes_Txt": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                                    ],
                                    "default": "Mes desconocido"
                                }
                            }

                        }
                    }


                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    , {
                        "$project": {
                            "Finca": "$finca"
                            , "Arbol": "$nombre_arbol"
                            , "numero arbol": "$numero arbol"
                            , "PH": "$PH"
                            , "Fecha": "$Fecha"

                            , "num_anio": "$num_anio"
                            , "num_mes": "$num_mes"
                            , "Mes_Txt": "$Mes_Txt"

                            , "supervisor": "$supervisor"
                            , "capture": "$capture"

                            , "rgDate": "$rgDate"


                        }
                    }



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
