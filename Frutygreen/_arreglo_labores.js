db.tasks.find({

    // "productivityPrice.price":NaN

    // "supplies.valueByUnity":{$in:[NaN]}

    // "supplies.quantity":{$in:[NaN]}

    "supplies.total":{$in:[NaN]}

})
   .projection({})
   .sort({_id:-1})



 db.getCollection("tasks_2023-01-11").aggregate(

     {
         "$match": {
             "$expr": {
                 "$or": [
                     {"$eq": ["$productivityPrice.price", NaN]},
                     {$in:[NaN,"$supplies.valueByUnity"]},
                     {$in:[NaN,"$supplies.quantity"]},
                     {$in:[NaN,"$supplies.total"]},
                 ]
             }
         }
     },

 )



//---proceso
// db.getCollection("tasks_2023-01-11").aggregate(

//     {
//         "$match": {
//             "$expr": {
//                 "$or": [
//                     {"$eq": ["$productivityPrice.price", NaN]},
//                     {$in:[NaN,"$supplies.valueByUnity"]},
//                     {$in:[NaN,"$supplies.quantity"]},
//                     {$in:[NaN,"$supplies.total"]},
//                 ]
//             }
//         }
//     },

// )



//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["capacitacion"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    // "capacitacion",
    // "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener formularios con la estructura en objeto y no en array
    var data = db.getSiblingDB(db_name_aux)
        .tasks
        .aggregate([

            {
                "$match": {
                    "$expr": {
                        "$or": [
                            { "$eq": ["$productivityPrice.price", NaN] },
                            { $in: [NaN, "$supplies.valueByUnity"] },
                            { $in: [NaN, "$supplies.quantity"] },
                            { $in: [NaN, "$supplies.total"] },
                        ]
                    }
                }
            },


        ]).allowDiskUse();

    //--🔄 ciclar formularios
    data.forEach(item_data => {

        // var array_variable_estructura_new = []

        // item_data.array_variable_estructura.forEach(item => {
        //     array_variable_estructura_new.push(item.v)
        // })


        // db.getSiblingDB(db_name_aux).forms.update(
        //     {
        //         _id: item_data._id
        //     },
        //     {
        //         $set: {
        //             "fstructure": array_variable_estructura_new
        //         }
        //     }
        // )


        result_info.push({
            database: db_name_aux,
            id: item_data._id

        })

    })

    data.close();





});

//--imprimir resultado
result_info

//
//
//
// "supplies" : [
// 		{
// 			"_id" : ObjectId("62cc9f1461b06f7afea3d539"),
// 			"wid" : ObjectId("63bdd4bb6f0d708ac0cdb8c8"),
// 			"quantity" : NaN,
// 			"inputmeasure" : "cuc",
// 			"valueByUnity" : NaN,
// 			"dose" : 0,
// 			"total" : NaN,
// 			"typeSupply" : "Consumables"
// 		}
// 	],
//
//
// 	"productivityPrice" : {
// 		"expectedValue" : 0,
// 		"price" : NaN,
// 		"measure" : "kg"
// 	},
