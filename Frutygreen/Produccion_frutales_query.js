  [
        
        
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
             "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
              "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.properties.name",
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "linea": "$linea.properties.name",
            "arbol": "$arbol.properties.name"
        }
    },
{
            "$match":{
                "Lote":{"$exists":true}      
            }
        },
    {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "finca": "$Point.farm",
                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",
                "FincaID": "$FincaID",
                "Finca nombre": "$Finca nombre"
            },
            "Número de frutos": { "$sum": "$Número de frutos"},
            "data": {
                "$push": "$$ROOT"
            }
        }
    },
    {
        "$lookup": {
            "from": "form_pesodelfruto",
            "as": "kg",
            "let": {
                "lote": "$_id.lote",
                "fecha_inicio": "$_id.Busqueda inicio",
                "fecha_fin": "$_id.Busqueda fin",
                "finca_id": {"$toString": "$_id.FincaID"}
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$lte": ["$rgDate", "$$fecha_fin"]
                                },
                                {
                                    "$eq": ["$Point.farm", "$$finca_id"]
                                }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": "$kg"
    },
    {
        "$addFields": {
            "kg": {
                "$cond": {
                    "if": { "$ne": ["$kg.Frutos por kg", 0]},
                    "then": {
                        "$divide": [
                            "$Número de frutos",
                            "$kg.Frutos por kg"                     
                        ]
                    },
                    "else": -1
                }
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "Finca": "$_id.Finca nombre",
            "Lote": "$_id.lote",
            "Frutos": "$Número de frutos",
            "kg": "$kg",
            "Point": {
                "farm": "$_id.FincaID"
            }
        }
    }
    
    
]