//ticket#1191
db.form_reportemaquila.aggregate(
    [
        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // , {
                        //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        // }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte



        //array cedulas
        {
            "$addFields": {
                "array_cedulas": [
                    {
                        "item": "CEDULA"
                        , "cedula": { "$ifNull": ["$CEDULA", 0] }
                    },
                    {
                        "item": "CEDULA 2"
                        , "cedula": { "$ifNull": ["$CEDULA 2", 0] }
                    },
                    {
                        "item": "CEDULA 3"
                        , "cedula": { "$ifNull": ["$CEDULA 3", 0] }
                    }
                ]
            }
        },
        {
            "$addFields": {
                "array_cedulas": {
                    "$filter": {
                        "input": "$array_cedulas",
                        "as": "item",
                        "cond": { "$ne": ["$$item.cedula", 0] }
                    }
                }
            }
        },
        {
            "$match": {
                "array_cedulas": { "$ne": [] }
            }
        },


        //caso de cedula1,cedula2 y cedula3 ====>> cedula2 y cedula3
        {
            "$addFields": {
                "array_cedulas_retencion": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$array_cedulas" }, 3] },
                        "then": [
                            { "$arrayElemAt": ["$array_cedulas.item", 1] },
                            { "$arrayElemAt": ["$array_cedulas.item", 2] }
                        ],
                        "else": [
                            { "$arrayElemAt": ["$array_cedulas.item", -1] }
                        ]
                    }
                }
            }
        },


        // {
        //     "$addFields": {
        //         "CEDULA REGISTRO RETENCION": { "$arrayElemAt": ["$array_cedulas", -1] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "CEDULA REGISTRO RETENCION": "$CEDULA REGISTRO RETENCION.item"
        //     }
        // },




        //sumar KG segun calibre
        {
            "$addFields": {
                "KG_EXPORTADOS": {
                    "$sum": [
                        { "$ifNull": ["$Kg CALIBRE 10", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 12", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 14", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 16", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 18", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 20", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 22", 0] }
                        , { "$ifNull": ["$Kg CALIBRE  24", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 26", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 28", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 30", 0] }
                        , { "$ifNull": ["$Kg CALIBRE 32", 0] }
                    ]
                }
            }
        },

        //DUDA SI CALCULAR VARIABLE
        {
            "$addFields": {
                "KG_DESCARTE": {
                    "$subtract": [
                        { "$ifNull": ["$Kg INGRESADOS", 0] }
                        , { "$ifNull": ["$KG_EXPORTADOS", 0] }
                    ]
                }
            }
        },
        {
            "$addFields": {
                "KG_DESCARTE": {
                    "$subtract": [
                        { "$ifNull": ["$KG_DESCARTE", 0] }
                        , { "$ifNull": ["$Kg DESHIDRATACION", 0] }
                    ]
                }
            }
        },


        {
            "$unwind": {
                "path": "$array_cedulas",
                "preserveNullAndEmptyArrays": false
            }
        },

        //cruzar con proveedor
        {
            "$lookup": {
                "from": "form_proveedoresfresh",
                "localField": "array_cedulas.cedula",
                "foreignField": "CEDULA",
                "as": "data_proveedores"
            }
        },


        //test
        {
            "$match": {
                "data_proveedores": { "$ne": [] }
            }
        },
        //------






        // //desagregar proveedores para cruzar con PROPUESTA COMERCIAL
        // {
        //     "$unwind": {
        //         "path": "$data_proveedores",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // },

        //provedores repetidos
        {
            "$addFields": {
                "data_proveedores": { "$arrayElemAt": ["$data_proveedores", -1] }
            }
        },




        {
            "$addFields": {
                "CEDULA REGISTRO": "$array_cedulas.item"

                , "PRODUCTOR NOMBRE": { "$ifNull": ["$data_proveedores.NOMBRE PRODUCTOR", ""] }
                , "PRODUCTOR HECTAREAS": { "$ifNull": ["$data_proveedores.HECTAREAS", 0] }
                , "PRODUCTOR TELEFONO": { "$ifNull": ["$data_proveedores.TELEFONO", 0] }
                , "PRODUCTOR CEDULA": { "$ifNull": ["$data_proveedores.CEDULA", 0] }
                , "PRODUCTOR DIRECCION": { "$ifNull": ["$data_proveedores.DIRECCION", ""] }
                , "PRODUCTOR NOMBRE PREDIO": { "$ifNull": ["$data_proveedores.NOMBRE PREDIO", ""] }
                , "PRODUCTOR REG ICA": { "$ifNull": ["$data_proveedores.REG ICA", 0] }
                , "PRODUCTOR ALTURA SNM": { "$ifNull": ["$data_proveedores.ALTURA SNM", 0] }
            }
        },


        {
            "$project": {
                "array_cedulas": 0,
                "data_proveedores": 0

                , "CEDULA": 0
                , "CEDULA 2": 0
                , "CEDULA 3": 0
            }
        },




        //=====cruzar para obtener precios

        {
            "$lookup": {
                "from": "form_propuestacomercialporcalibre",
                "localField": "PRODUCTOR CEDULA",
                "foreignField": "CEDULA",
                "as": "data_propuestacomercialporcalibre"
            }
        },

        {
            "$lookup": {
                "from": "form_propuestacomercialfija",
                "localField": "PRODUCTOR CEDULA",
                "foreignField": "CEDULA",
                "as": "data_propuestacomercialfija"
            }
        },


        //tipo precio
        {
            "$addFields": {
                "tipo_propuesta_comercial": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$data_propuestacomercialporcalibre", []] }
                                , { "$eq": ["$data_propuestacomercialfija", []] }

                            ]
                        },
                        "then": "ninguna",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                        , { "$ne": ["$data_propuestacomercialfija", []] }

                                    ]
                                },
                                //"then": "multiple",//DUDA EN CUAL ELEGIR
                                "then": "propuestacomercialporcalibre",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [
                                                { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                            ]
                                        },
                                        "then": "propuestacomercialporcalibre",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [
                                                        { "$ne": ["$data_propuestacomercialfija", []] }
                                                    ]
                                                },
                                                "then": "propuestacomercialfija",
                                                "else": "otra"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },


        {
            "$match": {
                "tipo_propuesta_comercial": { "$ne": "ninguna" }
            }
        },

        //data precio
        {
            "$addFields": {
                "data_propuesta_comercial": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialporcalibre"] }
                                , { "$eq": [{ "$type": "$data_propuestacomercialporcalibre" }, "array"] }

                            ]
                        },
                        "then": { "$arrayElemAt": ["$data_propuestacomercialporcalibre", 0] },
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                                        , { "$eq": [{ "$type": "$data_propuestacomercialfija" }, "array"] }

                                    ]
                                },
                                "then": { "$arrayElemAt": ["$data_propuestacomercialfija", 0] },
                                "else": {}
                            }

                        }
                    }
                }
            }
        },


        {
            "$project": {
                "data_propuestacomercialfija": 0,
                "data_propuestacomercialporcalibre": 0
            }
        },




        //precios

        //---DESCARTE
        {
            "$addFields": {
                "total_costo_descarte": {
                    "$multiply": [
                        { "$ifNull": ["$KG_DESCARTE", 0] }
                        , { "$ifNull": ["$data_propuesta_comercial.DESCARTE", 0] }
                    ]
                }
            }
        },



        //---EXPORTADOS
        {
            "$addFields": {
                "total_costo_exportados": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                            ]
                        },
                        //multiplicar constantemente
                        "then": {

                            "$multiply": [
                                { "$ifNull": ["$KG_EXPORTADOS", 0] }
                                , { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] }
                            ]

                        },
                        //multiplicar dinamicamente
                        //"else": {}
                        "else": {
                            "$sum": [
                                { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 10", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 10", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 12", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 12", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 14", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 14", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 16", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 16", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 18", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 18", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 20", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 20", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 22", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 22", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE  24", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 24", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 26", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 26", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 28", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 28", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 30", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 30", 0] }] }
                                , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 32", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 32", 0] }] }
                            ]
                        }
                    }

                }
            }
        },


        //---Costos de propuesta comercial
        {
            "$addFields": {
                "costo_calibre_10": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 10", 0] }
                    }
                },
                "costo_calibre_12": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 12", 0] }
                    }
                },
                "costo_calibre_14": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 14", 0] }
                    }
                },
                "costo_calibre_16": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 16", 0] }
                    }
                },
                "costo_calibre_18": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 18", 0] }
                    }
                },
                "costo_calibre_20": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 20", 0] }
                    }
                },
                "costo_calibre_22": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 22", 0] }
                    }
                },
                "costo_calibre_24": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 24", 0] }
                    }
                },
                "costo_calibre_26": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 26", 0] }
                    }
                },
                "costo_calibre_28": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 28", 0] }
                    }
                },
                "costo_calibre_30": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 30", 0] }
                    }
                },
                "costo_calibre_32": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                        "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                        "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 32", 0] }
                    }
                }

            }
        },





        {
            "$addFields": {
                "promedio_costo_exportados": {
                    "$cond": {
                        "if": { "$eq": ["$KG_EXPORTADOS", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$total_costo_exportados",
                                "$KG_EXPORTADOS"]
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "SUBTOTAL_A_PAGAR": {
                    //"$subtract": [
                    "$sum": [
                        { "$ifNull": ["$total_costo_exportados", 0] }
                        , { "$ifNull": ["$total_costo_descarte", 0] }
                    ]
                }
            }
        },

        //old
        // //----CASOS PARA APLICAR RETENCIONES
        // //PARA APLICAR LAS RETENCIONES DE ASOHOFRUCOL Y RETEFUNETE CUANDO APAREZCA DILIGENCIADO SOLO CEDULA SE LE APLICARA A ESTE,
        // //EN CASO DE QUE APAREZCA CEDULA Y CEDULA 2 DILIGENCIADOS SE LE DEBE APLICAR A CEDULA 2
        // //CUANDO APAREZCA CEDULA, CEDULA 2 Y CEDULA 3 ESTAS RETENCIONES SE LE APLICARA A CEDULA 3.

        //new
        //caso de cedula1,cedula2 y cedula3 ===> cedula2 y cedula3


        {
            "$addFields": {
                "RETEFUENTE": {
                    "$cond": {
                        // "if": { "$eq": ["$CEDULA REGISTRO RETENCION", "$CEDULA REGISTRO"] },
                        "if": { "$in": ["$CEDULA REGISTRO", "$array_cedulas_retencion"] },
                        "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.015] },
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "ASOHOFRUCOL": {
                    "$cond": {
                        //"if": { "$eq": ["$CEDULA REGISTRO RETENCION", "$CEDULA REGISTRO"] },
                        "if": { "$in": ["$CEDULA REGISTRO", "$array_cedulas_retencion"] },
                        "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.01] },
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "aplica_retica": { "$indexOfCP": ["$PRODUCTOR DIRECCION", "MANIZALES"] }
            }
        },

        {
            "$addFields": {
                "RETEICA": {
                    "$cond": {
                        "if": {
                            //si DIRECCION proveedor es de MANIZALES
                            "$eq": ["$aplica_retica", -1]
                        },
                        "then": 0,
                        "else": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.0035] }
                    }
                }
            }
        },


        {
            "$addFields": {
                "TOTAL_A_PAGAR": {
                    "$subtract": [
                        { "$ifNull": ["$SUBTOTAL_A_PAGAR", 0] }
                        , {
                            "$sum": [
                                "$RETEFUENTE"
                                , "$ASOHOFRUCOL"
                                , "$RETEICA"
                            ]
                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "PROMEDIO_FRUTA_INGRESADA": {
                    "$cond": {
                        "if": { "$eq": [{ "$ifNull": ["$Kg INGRESADOS", 0] }, 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$TOTAL_A_PAGAR",
                                "$Kg INGRESADOS"]
                        }
                    }
                }
            }
        },


        //FIRMA_PRODUCTO Y FIRMA_FRESH_TERUMA
        {
            "$addFields": {
                "FIRMA_PRODUCTO": ""
                , "FIRMA_FRESH_TERUMA": ""
            }
        },




        {
            "$project": {
                "data_propuesta_comercial": 0
                , "Point": 0
            }
        }


        ,{
            "$addFields": {
                "FECHA INGRESO FRUTA": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA INGRESO FRUTA" }
                }
            }
        }


        // // , {
        // //     "$project": {
        // //         "NUMERO DE LIQUIDACION": "$NUMERO DE LIQUIDACION",
        // //         "FECHA INGRESO FRUTA": "$FECHA INGRESO FRUTA",
        // //         "PRODUCTOR NOMBRE": "$PRODUCTOR NOMBRE",
        // //         "PRODUCTOR CEDULA": "$PRODUCTOR CEDULA",
        // //         "PRODUCTOR NOMBRE PREDIO": "$PRODUCTOR NOMBRE PREDIO",
        // //         "PRODUCTOR REG ICA": "$PRODUCTOR REG ICA",
        // //         "PRODUCTOR DIRECCION": "$PRODUCTOR DIRECCION",
        // //         "PRODUCTOR HECTAREAS": "$PRODUCTOR HECTAREAS",
        // //         "PRODUCTOR TELEFONO": "$PRODUCTOR TELEFONO",
        // //         "PRODUCTOR ALTURA SNM": "$PRODUCTOR ALTURA SNM",
        // //         "tipo_propuesta_comercial": "$tipo_propuesta_comercial",
        // //         "COMERCIAL REPSONSABLE": "$COMERCIAL REPSONSABLE",
        // //         "CANASTILLAS INGRESADAS": "$CANASTILLAS INGRESADAS",
        // //         "Kg INGRESADOS": "$Kg INGRESADOS",
        // //         "KG_EXPORTADOS": "$KG_EXPORTADOS",
        // //         "KG_DESCARTE": "$KG_DESCARTE",
        // //         "Kg DESHIDRATACION": "$Kg DESHIDRATACION",
        // //         "Kg CALIBRE 10": "$Kg CALIBRE 10",
        // //         "Kg CALIBRE 12": "$Kg CALIBRE 12",
        // //         "Kg CALIBRE 14": "$Kg CALIBRE 14",
        // //         "Kg CALIBRE 16": "$Kg CALIBRE 16",
        // //         "Kg CALIBRE 18": "$Kg CALIBRE 18",
        // //         "Kg CALIBRE 20": "$Kg CALIBRE 20",
        // //         "Kg CALIBRE 22": "$Kg CALIBRE 22",
        // //         "Kg CALIBRE  24": "$Kg CALIBRE  24",
        // //         "Kg CALIBRE 26": "$Kg CALIBRE 26",
        // //         "Kg CALIBRE 28": "$Kg CALIBRE 28",
        // //         "Kg CALIBRE 30": "$Kg CALIBRE 30",
        // //         "Kg CALIBRE 32": "$Kg CALIBRE 32",
        // //         "total_costo_exportados": "$total_costo_exportados",
        // //         "total_costo_descarte": "$total_costo_descarte",
        // //         "SUBTOTAL_A_PAGAR": "$SUBTOTAL_A_PAGAR",
        // //         "RETEFUENTE": "$RETEFUENTE",
        // //         "ASOHOFRUCOL": "$ASOHOFRUCOL",
        // //         "aplica_retica": "$aplica_retica",
        // //         "RETEICA": "$RETEICA",
        // //         "Residualidad": "$Residualidad",
        // //         "PROMEDIO_FRUTA_INGRESADA": "$PROMEDIO_FRUTA_INGRESADA",
        // //         "promedio_costo_exportados": "$promedio_costo_exportados",
        // //         "TOTAL_A_PAGAR": "$TOTAL_A_PAGAR",
        // //         "FIRMA_PRODUCTO": "$FIRMA_PRODUCTO",
        // //         "FIRMA_FRESH_TERUMA": "$FIRMA_FRESH_TERUMA"



        // //     }
        // // }


        , {
            "$project": {
                "No liquidación": "$NUMERO DE LIQUIDACION",
                "Fecha ingreso de fruta": "$FECHA INGRESO FRUTA",
                "Nombre proveedor": "$PRODUCTOR NOMBRE",
                "Productor cedula": "$PRODUCTOR CEDULA",
                "Nombre predio": "$PRODUCTOR NOMBRE PREDIO",
                "No reg ICA": "$PRODUCTOR REG ICA",
                "Direccion": "$PRODUCTOR DIRECCION",
                "Productor Hectareas": "$PRODUCTOR HECTAREAS",
                "Productor Telefono": "$PRODUCTOR TELEFONO",
                "Productor ASNM": "$PRODUCTOR ALTURA SNM",
                "Tipo propuesta comercial": "$tipo_propuesta_comercial",
                "Comercial Responsable": "$COMERCIAL REPSONSABLE",
                "Canastillas ingresadas": "$CANASTILLAS INGRESADAS",
                "Kg ingresados": "$Kg INGRESADOS",
                "Kg exportados": "$KG_EXPORTADOS",
                "Kg descartados": "$KG_DESCARTE",
                "Kg merma": "$Kg DESHIDRATACION",
                "Kg calibre 10": "$Kg CALIBRE 10",
                "Kg calibre 12": "$Kg CALIBRE 12",
                "Kg calibre 14": "$Kg CALIBRE 14",
                "Kg calibre 16": "$Kg CALIBRE 16",
                "Kg calibre 18": "$Kg CALIBRE 18",
                "Kg calibre 20": "$Kg CALIBRE 20",
                "Kg calibre 22": "$Kg CALIBRE 22",
                "Kg calibre 24": "$Kg CALIBRE  24",
                "Kg calibre 26": "$Kg CALIBRE 26",
                "Kg calibre 28": "$Kg CALIBRE 28",
                "Kg calibre 30": "$Kg CALIBRE 30",
                "Kg calibre 32": "$Kg CALIBRE 32",

                "Costo calibre 10": "$costo_calibre_10",
                "Costo calibre 12": "$costo_calibre_12",
                "Costo calibre 14": "$costo_calibre_14",
                "Costo calibre 16": "$costo_calibre_16",
                "Costo calibre 18": "$costo_calibre_18",
                "Costo calibre 20": "$costo_calibre_20",
                "Costo calibre 22": "$costo_calibre_22",
                "Costo calibre 24": "$costo_calibre_24",
                "Costo calibre 26": "$costo_calibre_26",
                "Costo calibre 28": "$costo_calibre_28",
                "Costo calibre 30": "$costo_calibre_30",
                "Costo calibre 32": "$costo_calibre_32",

                "Total valor exportados": "$total_costo_exportados",
                "Total valor descarte": "$total_costo_descarte",
                "Subtotal a pagar": "$SUBTOTAL_A_PAGAR",
                "Retefuente": "$RETEFUENTE",
                "Ashofrucol": "$ASOHOFRUCOL",
                // "Aplica reteICA": "$aplica_retica",
                "ReteICA": "$RETEICA",
                "Residualidad": "$Residualidad",
                "Promedio fruta ingresada": "$PROMEDIO_FRUTA_INGRESADA",
                "Promedio fruta exportada": "$promedio_costo_exportados",
                "Total a pagar": "$TOTAL_A_PAGAR",
                "Firma productor": "$FIRMA_PRODUCTO",
                "Firma Fresh Teruma": "$FIRMA_FRESH_TERUMA"

            }
        }




    ]


)
