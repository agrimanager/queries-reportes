[

    {
        "$addFields": {
            "array_cedulas": [
                {
                    "item": "CEDULA"
                    , "cedula": { "$ifNull": ["$CEDULA", 0] }
                },
                {
                    "item": "CEDULA 2"
                    , "cedula": { "$ifNull": ["$CEDULA 2", 0] }
                },
                {
                    "item": "CEDULA 3"
                    , "cedula": { "$ifNull": ["$CEDULA 3", 0] }
                }
            ]
        }
    },
    {
        "$addFields": {
            "array_cedulas": {
                "$filter": {
                    "input": "$array_cedulas",
                    "as": "item",
                    "cond": { "$ne": ["$$item.cedula", 0] }
                }
            }
        }
    },
    {
        "$match": {
            "array_cedulas": { "$ne": [] }
        }
    },


    {
        "$addFields": {
            "CEDULA REGISTRO RETENCION": { "$arrayElemAt": ["$array_cedulas", -1] }
        }
    },
    {
        "$addFields": {
            "CEDULA REGISTRO RETENCION": "$CEDULA REGISTRO RETENCION.item"
        }
    },



    {
        "$addFields": {
            "KG_EXPORTADOS": {
                "$sum": [
                    { "$ifNull": ["$Kg CALIBRE 10", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 12", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 14", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 16", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 18", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 20", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 22", 0] }
                    , { "$ifNull": ["$Kg CALIBRE  24", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 26", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 28", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 30", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 32", 0] }
                ]
            }
        }
    },

    {
        "$addFields": {
            "KG_DESCARTE": {
                "$subtract": [
                    { "$ifNull": ["$Kg INGRESADOS", 0] }
                    , { "$ifNull": ["$KG_EXPORTADOS", 0] }
                ]
            }
        }
    },
    {
        "$addFields": {
            "KG_DESCARTE": {
                "$subtract": [
                    { "$ifNull": ["$KG_DESCARTE", 0] }
                    , { "$ifNull": ["$Kg DESHIDRATACION", 0] }
                ]
            }
        }
    },


    {
        "$unwind": {
            "path": "$array_cedulas",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$lookup": {
            "from": "form_proveedoresfresh",
            "localField": "array_cedulas.cedula",
            "foreignField": "CEDULA",
            "as": "data_proveedores"
        }
    },



    {
        "$unwind": {
            "path": "$data_proveedores",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "CEDULA REGISTRO": "$array_cedulas.item"

            , "PRODUCTOR NOMBRE": { "$ifNull": ["$data_proveedores.NOMBRE PRODUCTOR", ""] }
            , "PRODUCTOR HECTAREAS": { "$ifNull": ["$data_proveedores.HECTAREAS", 0] }
            , "PRODUCTOR TELEFONO": { "$ifNull": ["$data_proveedores.TELEFONO", 0] }
            , "PRODUCTOR CEDULA": { "$ifNull": ["$data_proveedores.CEDULA", 0] }
            , "PRODUCTOR DIRECCION": { "$ifNull": ["$data_proveedores.DIRECCION", ""] }
            , "PRODUCTOR NOMBRE PREDIO": { "$ifNull": ["$data_proveedores.NOMBRE PREDIO", ""] }
            , "PRODUCTOR REG ICA": { "$ifNull": ["$data_proveedores.REG ICA", 0] }
            , "PRODUCTOR ALTURA SNM": { "$ifNull": ["$data_proveedores.ALTURA SNM", 0] }
        }
    },


    {
        "$project": {
            "array_cedulas": 0,
            "data_proveedores": 0

            , "CEDULA": 0
            , "CEDULA 2": 0
            , "CEDULA 3": 0
        }
    },




    {
        "$lookup": {
            "from": "form_propuestacomercialporcalibre",
            "localField": "PRODUCTOR CEDULA",
            "foreignField": "CEDULA",
            "as": "data_propuestacomercialporcalibre"
        }
    },

    {
        "$lookup": {
            "from": "form_propuestacomercialfija",
            "localField": "PRODUCTOR CEDULA",
            "foreignField": "CEDULA",
            "as": "data_propuestacomercialfija"
        }
    },


    {
        "$addFields": {
            "tipo_propuesta_comercial": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$data_propuestacomercialporcalibre", []] }
                            , { "$eq": ["$data_propuestacomercialfija", []] }

                        ]
                    },
                    "then": "ninguna",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [
                                    { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                    , { "$ne": ["$data_propuestacomercialfija", []] }

                                ]
                            },
                            "then": "multiple",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [
                                            { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                        ]
                                    },
                                    "then": "propuestacomercialporcalibre",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [
                                                    { "$ne": ["$data_propuestacomercialfija", []] }
                                                ]
                                            },
                                            "then": "propuestacomercialfija",
                                            "else": "otra"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },


    {
        "$match": {
            "tipo_propuesta_comercial": { "$ne": "ninguna" }
        }
    },

    {
        "$addFields": {
            "data_propuesta_comercial": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialporcalibre"] }
                            , { "$eq": [{ "$type": "$data_propuestacomercialporcalibre" }, "array"] }

                        ]
                    },
                    "then": { "$arrayElemAt": ["$data_propuestacomercialporcalibre", 0] },
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [
                                    { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                                    , { "$eq": [{ "$type": "$data_propuestacomercialfija" }, "array"] }

                                ]
                            },
                            "then": { "$arrayElemAt": ["$data_propuestacomercialfija", 0] },
                            "else": {}
                        }

                    }
                }
            }
        }
    },


    {
        "$project": {
            "data_propuestacomercialfija": 0,
            "data_propuestacomercialporcalibre": 0
        }
    },



    {
        "$addFields": {
            "total_costo_descarte": {
                "$multiply": [
                    { "$ifNull": ["$KG_DESCARTE", 0] }
                    , { "$ifNull": ["$data_propuesta_comercial.DESCARTE", 0] }
                ]
            }
        }
    },



    {
        "$addFields": {
            "total_costo_exportados": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                        ]
                    },

                    "then": {

                        "$multiply": [
                            { "$ifNull": ["$KG_EXPORTADOS", 0] }
                            , { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] }
                        ]

                    },

                    "else": {
                        "$sum": [
                            { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 10", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 10", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 12", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 12", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 14", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 14", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 16", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 16", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 18", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 18", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 20", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 20", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 22", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 22", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE  24", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 24", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 26", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 26", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 28", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 28", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 30", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 30", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 32", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 32", 0] }] }
                        ]
                    }
                }

            }
        }
    },





    {
        "$addFields": {
            "promedio_costo_exportados": {
                "$cond": {
                    "if": { "$eq": ["$KG_EXPORTADOS", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$total_costo_exportados",
                            "$KG_EXPORTADOS"]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "SUBTOTAL_A_PAGAR": {
                "$sum": [
                    { "$ifNull": ["$total_costo_exportados", 0] }
                    , { "$ifNull": ["$total_costo_descarte", 0] }
                ]
            }
        }
    },




    {
        "$addFields": {
            "RETEFUENTE": {
                "$cond": {
                    "if": { "$eq": ["$CEDULA REGISTRO RETENCION", "$CEDULA REGISTRO"] },
                    "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.015] },
                    "else": 0
                }
            }
        }
    },

    {
        "$addFields": {
            "ASOHOFRUCOL": {
                "$cond": {
                    "if": { "$eq": ["$CEDULA REGISTRO RETENCION", "$CEDULA REGISTRO"] },
                    "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.01] },
                    "else": 0
                }
            }
        }
    },

    {
        "$addFields": {
            "aplica_retica": { "$indexOfCP": ["$PRODUCTOR DIRECCION", "MANIZALES"] }
        }
    },

    {
        "$addFields": {
            "RETEICA": {
                "$cond": {
                    "if": {
                        "$eq": ["$aplica_retica", -1]
                    },
                    "then": 0,
                    "else": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.0045] }
                }
            }
        }
    },


    {
        "$addFields": {
            "TOTAL_A_PAGAR": {
                "$subtract": [
                    { "$ifNull": ["$SUBTOTAL_A_PAGAR", 0] }
                    , {
                        "$sum": [
                            "$RETEFUENTE"
                            , "$ASOHOFRUCOL"
                            , "$RETEICA"
                        ]
                    }
                ]
            }
        }
    },


    {
        "$addFields": {
            "PROMEDIO_FRUTA_INGRESADA": {
                "$cond": {
                    "if": { "$eq": [{ "$ifNull": ["$Kg INGRESADOS", 0] }, 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$TOTAL_A_PAGAR",
                            "$Kg INGRESADOS"]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "FIRMA_PRODUCTO": ""
            , "FIRMA_FRESH_TERUMA": ""
        }
    },




    {
        "$project": {
            "data_propuesta_comercial": 0
            , "Point": 0
        }
    }


    , {
        "$project": {
          "No liquidación": "$NUMERO DE LIQUIDACION",
              "Fecha ingreso de fruta": "$FECHA INGRESO FRUTA",
              "Nombre proveedor": "$PRODUCTOR NOMBRE",
              "Productor cedula": "$PRODUCTOR CEDULA",
              "Nombre predio": "$PRODUCTOR NOMBRE PREDIO",
              "No reg ICA": "$PRODUCTOR REG ICA",
              "Direccion": "$PRODUCTOR DIRECCION",
              "Productor Hectareas": "$PRODUCTOR HECTAREAS",
              "Productor Telefono": "$PRODUCTOR TELEFONO",
              "Productor ASNM": "$PRODUCTOR ALTURA SNM",
              "Tipo propuesta comercial": "$tipo_propuesta_comercial",
              "Comercial Responsable": "$COMERCIAL REPSONSABLE",
              "Canastillas ingresadas": "$CANASTILLAS INGRESADAS",
              "Kg ingresados": "$Kg INGRESADOS",
              "Kg exportados": "$KG_EXPORTADOS",
              "Kg descartados": "$KG_DESCARTE",
              "Kg merma": "$Kg DESHIDRATACION",
              "Kg calibre 10": "$Kg CALIBRE 10",
              "Kg calibre 12": "$Kg CALIBRE 12",
              "Kg calibre 14": "$Kg CALIBRE 14",
              "Kg calibre 16": "$Kg CALIBRE 16",
              "Kg calibre 18": "$Kg CALIBRE 18",
              "Kg calibre 20": "$Kg CALIBRE 20",
              "Kg calibre 22": "$Kg CALIBRE 22",
              "Kg calibre 24": "$Kg CALIBRE  24",
              "Kg calibre 26": "$Kg CALIBRE 26",
              "Kg calibre 28": "$Kg CALIBRE 28",
              "Kg calibre 30": "$Kg CALIBRE 30",
              "Kg calibre 32": "$Kg CALIBRE 32",
              "Total valor exportados": "$total_costo_exportados",
              "Total valor descarte": "$total_costo_descarte",
              "Subtotal a pagar": "$SUBTOTAL_A_PAGAR",
              "Retefuente": "$RETEFUENTE",
              "Ashofrucol": "$ASOHOFRUCOL",
              "Aplica reteICA": "$aplica_retica",
              "ReteICA": "$RETEICA",
              "Residualidad": "$Residualidad",
              "Promedio fruta ingresada": "$PROMEDIO_FRUTA_INGRESADA",
              "Promedio fruta exportada": "$promedio_costo_exportados",
              "Total a pagar": "$TOTAL_A_PAGAR",
              "Firma productor": "$FIRMA_PRODUCTO",
              "Firma Fresh Teruma": "$FIRMA_FRESH_TERUMA"

        }
    }







]
