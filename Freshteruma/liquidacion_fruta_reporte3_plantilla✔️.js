[

    {
        "$addFields": {
            "array_cedulas": [
                {
                    "item": "CEDULA"
                    , "cedula": { "$ifNull": ["$CEDULA", 0] }
                },
                {
                    "item": "CEDULA 2"
                    , "cedula": { "$ifNull": ["$CEDULA 2", 0] }
                },
                {
                    "item": "CEDULA 3"
                    , "cedula": { "$ifNull": ["$CEDULA 3", 0] }
                }
            ]
        }
    },
    {
        "$addFields": {
            "array_cedulas": {
                "$filter": {
                    "input": "$array_cedulas",
                    "as": "item",
                    "cond": { "$ne": ["$$item.cedula", 0] }
                }
            }
        }
    },
    {
        "$match": {
            "array_cedulas": { "$ne": [] }
        }
    },


    {
        "$addFields": {
            "array_cedulas_retencion": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$array_cedulas" }, 3] },
                    "then": [
                        { "$arrayElemAt": ["$array_cedulas.item", 1] },
                        { "$arrayElemAt": ["$array_cedulas.item", 2] }
                    ],
                    "else": [
                        { "$arrayElemAt": ["$array_cedulas.item", -1] }
                    ]
                }
            }
        }
    },


    {
        "$addFields": {
            "KG_EXPORTADOS": {
                "$sum": [
                    { "$ifNull": ["$Kg CALIBRE 10", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 12", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 14", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 16", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 18", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 20", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 22", 0] }
                    , { "$ifNull": ["$Kg CALIBRE  24", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 26", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 28", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 30", 0] }
                    , { "$ifNull": ["$Kg CALIBRE 32", 0] }
                ]
            }
        }
    },

    {
        "$addFields": {
            "KG_DESCARTE": {
                "$subtract": [
                    { "$ifNull": ["$Kg INGRESADOS", 0] }
                    , { "$ifNull": ["$KG_EXPORTADOS", 0] }
                ]
            }
        }
    },
    {
        "$addFields": {
            "KG_DESCARTE": {
                "$subtract": [
                    { "$ifNull": ["$KG_DESCARTE", 0] }
                    , { "$ifNull": ["$Kg DESHIDRATACION", 0] }
                ]
            }
        }
    },


    {
        "$unwind": {
            "path": "$array_cedulas",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$lookup": {
            "from": "form_proveedoresfresh",
            "localField": "array_cedulas.cedula",
            "foreignField": "CEDULA",
            "as": "data_proveedores"
        }
    },


    {
        "$match": {
            "data_proveedores": { "$ne": [] }
        }
    },

    {
        "$addFields": {
            "data_proveedores": { "$arrayElemAt": ["$data_proveedores", -1] }
        }
    },




    {
        "$addFields": {
            "CEDULA REGISTRO": "$array_cedulas.item"

            , "PRODUCTOR NOMBRE": { "$ifNull": ["$data_proveedores.NOMBRE PRODUCTOR", ""] }
            , "PRODUCTOR HECTAREAS": { "$ifNull": ["$data_proveedores.HECTAREAS", 0] }
            , "PRODUCTOR TELEFONO": { "$ifNull": ["$data_proveedores.TELEFONO", 0] }
            , "PRODUCTOR CEDULA": { "$ifNull": ["$data_proveedores.CEDULA", 0] }
            , "PRODUCTOR DIRECCION": { "$ifNull": ["$data_proveedores.DIRECCION", ""] }
            , "PRODUCTOR NOMBRE PREDIO": { "$ifNull": ["$data_proveedores.NOMBRE PREDIO", ""] }
            , "PRODUCTOR REG ICA": { "$ifNull": ["$data_proveedores.REG ICA", 0] }
            , "PRODUCTOR ALTURA SNM": { "$ifNull": ["$data_proveedores.ALTURA SNM", 0] }
        }
    },


    {
        "$project": {
            "array_cedulas": 0,
            "data_proveedores": 0

            , "CEDULA": 0
            , "CEDULA 2": 0
            , "CEDULA 3": 0
        }
    },



    {
        "$lookup": {
            "from": "form_propuestacomercialporcalibre",
            "localField": "PRODUCTOR CEDULA",
            "foreignField": "CEDULA",
            "as": "data_propuestacomercialporcalibre"
        }
    },

    {
        "$lookup": {
            "from": "form_propuestacomercialfija",
            "localField": "PRODUCTOR CEDULA",
            "foreignField": "CEDULA",
            "as": "data_propuestacomercialfija"
        }
    },


    {
        "$addFields": {
            "tipo_propuesta_comercial": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$data_propuestacomercialporcalibre", []] }
                            , { "$eq": ["$data_propuestacomercialfija", []] }

                        ]
                    },
                    "then": "ninguna",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [
                                    { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                    , { "$ne": ["$data_propuestacomercialfija", []] }

                                ]
                            },

                            "then": "propuestacomercialporcalibre",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [
                                            { "$ne": ["$data_propuestacomercialporcalibre", []] }
                                        ]
                                    },
                                    "then": "propuestacomercialporcalibre",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [
                                                    { "$ne": ["$data_propuestacomercialfija", []] }
                                                ]
                                            },
                                            "then": "propuestacomercialfija",
                                            "else": "otra"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },


    {
        "$match": {
            "tipo_propuesta_comercial": { "$ne": "ninguna" }
        }
    },


    {
        "$addFields": {
            "data_propuesta_comercial": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialporcalibre"] }
                            , { "$eq": [{ "$type": "$data_propuestacomercialporcalibre" }, "array"] }

                        ]
                    },
                    "then": { "$arrayElemAt": ["$data_propuestacomercialporcalibre", 0] },
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [
                                    { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                                    , { "$eq": [{ "$type": "$data_propuestacomercialfija" }, "array"] }

                                ]
                            },
                            "then": { "$arrayElemAt": ["$data_propuestacomercialfija", 0] },
                            "else": {}
                        }

                    }
                }
            }
        }
    },


    {
        "$project": {
            "data_propuestacomercialfija": 0,
            "data_propuestacomercialporcalibre": 0
        }
    },



    {
        "$addFields": {
            "total_costo_descarte": {
                "$multiply": [
                    { "$ifNull": ["$KG_DESCARTE", 0] }
                    , { "$ifNull": ["$data_propuesta_comercial.DESCARTE", 0] }
                ]
            }
        }
    },


    {
        "$addFields": {
            "total_costo_exportados": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] }
                        ]
                    },

                    "then": {

                        "$multiply": [
                            { "$ifNull": ["$KG_EXPORTADOS", 0] }
                            , { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] }
                        ]

                    },

                    "else": {
                        "$sum": [
                            { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 10", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 10", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 12", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 12", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 14", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 14", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 16", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 16", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 18", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 18", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 20", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 20", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 22", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 22", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE  24", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 24", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 26", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 26", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 28", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 28", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 30", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 30", 0] }] }
                            , { "$multiply": [{ "$ifNull": ["$Kg CALIBRE 32", 0] }, { "$ifNull": ["$data_propuesta_comercial.CALIBRE 32", 0] }] }
                        ]
                    }
                }

            }
        }
    },



    {
        "$addFields": {
            "costo_calibre_10": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 10", 0] }
                }
            },
            "costo_calibre_12": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 12", 0] }
                }
            },
            "costo_calibre_14": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 14", 0] }
                }
            },
            "costo_calibre_16": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 16", 0] }
                }
            },
            "costo_calibre_18": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 18", 0] }
                }
            },
            "costo_calibre_20": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 20", 0] }
                }
            },
            "costo_calibre_22": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 22", 0] }
                }
            },
            "costo_calibre_24": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 24", 0] }
                }
            },
            "costo_calibre_26": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 26", 0] }
                }
            },
            "costo_calibre_28": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 28", 0] }
                }
            },
            "costo_calibre_30": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 30", 0] }
                }
            },
            "costo_calibre_32": {
                "$cond": {
                    "if": { "$eq": ["$tipo_propuesta_comercial", "propuestacomercialfija"] },
                    "then": { "$ifNull": ["$data_propuesta_comercial.PRECIO FIJO", 0] },
                    "else": { "$ifNull": ["$data_propuesta_comercial.CALIBRE 32", 0] }
                }
            }

        }
    },





    {
        "$addFields": {
            "promedio_costo_exportados": {
                "$cond": {
                    "if": { "$eq": ["$KG_EXPORTADOS", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$total_costo_exportados",
                            "$KG_EXPORTADOS"]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "SUBTOTAL_A_PAGAR": {
                "$sum": [
                    { "$ifNull": ["$total_costo_exportados", 0] }
                    , { "$ifNull": ["$total_costo_descarte", 0] }
                ]
            }
        }
    },


    {
        "$addFields": {
            "RETEFUENTE": {
                "$cond": {

                    "if": { "$in": ["$CEDULA REGISTRO", "$array_cedulas_retencion"] },
                    "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.015] },
                    "else": 0
                }
            }
        }
    },

    {
        "$addFields": {
            "ASOHOFRUCOL": {
                "$cond": {
                    "if": { "$in": ["$CEDULA REGISTRO", "$array_cedulas_retencion"] },
                    "then": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.01] },
                    "else": 0
                }
            }
        }
    },

    {
        "$addFields": {
            "aplica_retica": { "$indexOfCP": ["$PRODUCTOR DIRECCION", "MANIZALES"] }
        }
    },

    {
        "$addFields": {
            "RETEICA": {
                "$cond": {
                    "if": {

                        "$eq": ["$aplica_retica", -1]
                    },
                    "then": 0,
                    "else": { "$multiply": ["$SUBTOTAL_A_PAGAR", 0.0035] }
                }
            }
        }
    },


    {
        "$addFields": {
            "TOTAL_A_PAGAR": {
                "$subtract": [
                    { "$ifNull": ["$SUBTOTAL_A_PAGAR", 0] }
                    , {
                        "$sum": [
                            "$RETEFUENTE"
                            , "$ASOHOFRUCOL"
                            , "$RETEICA"
                        ]
                    }
                ]
            }
        }
    },


    {
        "$addFields": {
            "PROMEDIO_FRUTA_INGRESADA": {
                "$cond": {
                    "if": { "$eq": [{ "$ifNull": ["$Kg INGRESADOS", 0] }, 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$TOTAL_A_PAGAR",
                            "$Kg INGRESADOS"]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "FIRMA_PRODUCTO": ""
            , "FIRMA_FRESH_TERUMA": ""
        }
    },




    {
        "$project": {
            "data_propuesta_comercial": 0
            , "Point": 0
        }
    }


    ,{
            "$addFields": {
                "FECHA INGRESO FRUTA": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA INGRESO FRUTA" }
                }
            }
        }


    , {
        "$project": {
            "Nombre proveedor": "$PRODUCTOR NOMBRE",
            "No liquidación": "$NUMERO DE LIQUIDACION",
            "PROVEEDOR_NUM_LIQUIDACION": { "$concat": ["$PRODUCTOR NOMBRE", "_", { "$toString": "$NUMERO DE LIQUIDACION" }] },

            "Fecha ingreso de fruta": "$FECHA INGRESO FRUTA",
            "Productor cedula": "$PRODUCTOR CEDULA",
            "Nombre predio": "$PRODUCTOR NOMBRE PREDIO",
            "No reg ICA": "$PRODUCTOR REG ICA",
            "Direccion": "$PRODUCTOR DIRECCION",
            "Productor Hectareas": "$PRODUCTOR HECTAREAS",
            "Productor Telefono": "$PRODUCTOR TELEFONO",
            "Productor ASNM": "$PRODUCTOR ALTURA SNM",
            "Tipo propuesta comercial": "$tipo_propuesta_comercial",
            "Comercial Responsable": "$COMERCIAL REPSONSABLE",
            "Canastillas ingresadas": "$CANASTILLAS INGRESADAS",
            "Kg ingresados": "$Kg INGRESADOS",
            "Kg exportados": "$KG_EXPORTADOS",
            "Kg descartados": "$KG_DESCARTE",
            "Kg merma": "$Kg DESHIDRATACION",
            "Kg calibre 10": "$Kg CALIBRE 10",
            "Kg calibre 12": "$Kg CALIBRE 12",
            "Kg calibre 14": "$Kg CALIBRE 14",
            "Kg calibre 16": "$Kg CALIBRE 16",
            "Kg calibre 18": "$Kg CALIBRE 18",
            "Kg calibre 20": "$Kg CALIBRE 20",
            "Kg calibre 22": "$Kg CALIBRE 22",
            "Kg calibre 24": "$Kg CALIBRE  24",
            "Kg calibre 26": "$Kg CALIBRE 26",
            "Kg calibre 28": "$Kg CALIBRE 28",
            "Kg calibre 30": "$Kg CALIBRE 30",
            "Kg calibre 32": "$Kg CALIBRE 32",

            "Costo calibre 10": "$costo_calibre_10",
            "Costo calibre 12": "$costo_calibre_12",
            "Costo calibre 14": "$costo_calibre_14",
            "Costo calibre 16": "$costo_calibre_16",
            "Costo calibre 18": "$costo_calibre_18",
            "Costo calibre 20": "$costo_calibre_20",
            "Costo calibre 22": "$costo_calibre_22",
            "Costo calibre 24": "$costo_calibre_24",
            "Costo calibre 26": "$costo_calibre_26",
            "Costo calibre 28": "$costo_calibre_28",
            "Costo calibre 30": "$costo_calibre_30",
            "Costo calibre 32": "$costo_calibre_32",

            "Total valor exportados": "$total_costo_exportados",
            "Total valor descarte": "$total_costo_descarte",
            "Subtotal a pagar": "$SUBTOTAL_A_PAGAR",
            "Retefuente": "$RETEFUENTE",
            "Ashofrucol": "$ASOHOFRUCOL",

            "ReteICA": "$RETEICA",
            "Residualidad": "$Residualidad",
            "Promedio fruta ingresada": "$PROMEDIO_FRUTA_INGRESADA",
            "Promedio fruta exportada": "$promedio_costo_exportados",
            "Total a pagar": "$TOTAL_A_PAGAR",
            "Firma productor": "$FIRMA_PRODUCTO",
            "Firma Fresh Teruma": "$FIRMA_FRESH_TERUMA"

        }
    }




]
