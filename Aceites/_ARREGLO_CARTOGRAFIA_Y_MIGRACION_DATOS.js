
//===================PASO1 (BORRAR CARTOGRAFIA VIEJA)
var cursor = db.cartography.aggregate(

    {
        $match: {
            path: {
                // $regex: `^,61eab2d609c9c914d817afb5,` ////----LIBANO
                $regex: `^,61eab38709c9c914d817afc0,` ////----SAN CARLOS

                // $regex: `^,622bbeac4923b04bb83ee42a,` ////----test
            }
        }
    },


    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },

)


// cursor

cursor.forEach(i => {
    db.cartography.remove(
        {
            "_id": {
                "$in": i.ids
            }
        }
    );
})



//===================PASO2 (ACTUALIZAR CARTOGRAFIA NUEVA)
var cursor = db.cartography.aggregate(

    {
        $match: {
            path: {
                // $regex: `^,61eab2d609c9c914d817afb5,` ////----LIBANO
                // $regex: `^,61eab38709c9c914d817afc0,` ////----SAN CARLOS

                // $regex: `^,622bbeac4923b04bb83ee42a,` ////----test

                $regex: `^,622bbeac4923b04bb83ee42a,622bbec6ffb65ef66467d393,` ////----test --BLOQUE1 -- LIBANO
                // $regex: `^,622bbeac4923b04bb83ee42a,622bbec6ffb65ef66467d394,` ////----test --BLOQUE2 -- SAN_CARLOS
            }
        }
    },


    // {$limit:100}

)


// cursor

cursor.forEach(i => {
    var item_actual = i.path


    //libano
    var item_nuevo = item_actual.replace(
        ',622bbeac4923b04bb83ee42a,',
        ',61eab2d609c9c914d817afb5,'
    )

    // //san_carlos
    // var item_nuevo = item_actual.replace(
    //     ',622bbeac4923b04bb83ee42a,',
    //     ',61eab38709c9c914d817afc0,'
    // )



    // console.log("-------")
    // console.log(item_actual)
    // console.log(item_nuevo)
    // console.log("-------")

    db.cartography.update(
        {
            _id: i._id
        },
        {
            $set: {
                "path": item_nuevo
            }
        }
    )
})
