[


    { "$addFields": { "variable_fecha": "$Fecha de recoleccion" } },
    { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    {
        "$sort": {
            "variable_fecha": -1
        }
    },
    {
        "$addFields": { "Cartography": "$Lote" }
    },
    {
        "$unwind": "$Cartography.features"
    }
    , {
        "$group": {
            "_id": {
                "nombre_lote": "$Cartography.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data": { "$arrayElemAt": ["$data", 0] }
        }
    },

    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$data.variable_fecha"] }, 86400000]
                }
            }
        }
    },


    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 8] }]
                    },
                    "then": "#008000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 9] }, { "$lte": ["$dias de ciclo", 12] }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                    },
                                    "then": "#ffa500",
                                    "else": "#ff0000"
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 8] }]
                    },
                    "then": "A- [0 - 8] Dias",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 9] }, { "$lte": ["$dias de ciclo", 12] }]
                            },
                            "then": "B- [9 - 12] Dias",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                    },
                                    "then": "C- [13 - 15] Dias",
                                    "else": "D- ( > 15) Dias"
                                }
                            }
                        }
                    }
                }
            }

        }
    },




    {
        "$project": {

            "_id": { "$toObjectId": "$data.Cartography.features._id" },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.nombre_lote",
                "Rango": "$rango",
                "Dias Ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$dias de ciclo", -1] },
                        "then": "-1",
                        "else": {
                            "$concat": [
                                { "$toString": "$dias de ciclo" },
                                " dias"
                            ]
                        }
                    }
                },
                "color": "$color"
            },
            "geometry": "$data.Cartography.features.geometry"
        }
    }
]
