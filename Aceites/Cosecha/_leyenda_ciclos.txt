https://www.eniun.com/lista-codigos-de-colores-html-css/
//============== COLORES
//---bases
#ffffff : blanco
#000000 : negro
#808080 : gris
#c65911 : cafe
//---primarios
#ffff00 : amarillo
#0000ff : azul
#ff0000 : rojo
//---secundarios
#008000 : verde
#ffa500 : naranja
#ee82ee : morado o violeta

//============== LEYENDA DE REPORTE
//--parte1
#Dias Cilo Cosecha: #blanco,
[0 - 7] Dias: #verde,
[8 - 10] Dias: #amarillo,
[11 - 12] Dias: #naranja,
( > 12) Dias: #rojo

//--parte2
#Dias Cilo Cosecha: #ffffff,
[0 - 7] Dias: #008000,
[8 - 10] Dias: #ffff00,
[11 - 12] Dias: #ffa500,
( > 12) Dias: #ff0000


//--parte Final
#Dias Cilo Cosecha: #ffffff,[0 - 7] Dias: #008000,[8 - 10] Dias: #ffff00,[11 - 12] Dias: #ffa500,( > 12) Dias: #ff0000



//-----

//--parte2
#Dias Ciclo Cosecha a HOY: #ffffff,
[0 - 8] Dias: #008000,
[9 - 12] Dias: #ffff00,
[13 - 15] Dias: #ffa500,
( > 15) Dias: #ff0000

//--parte Final
#Dias Ciclo Cosecha a HOY: #ffffff,[0 - 8] Dias: #008000,[9 - 12] Dias: #ffff00,[13 - 15] Dias: #ffa500,( > 15) Dias: #ff0000
