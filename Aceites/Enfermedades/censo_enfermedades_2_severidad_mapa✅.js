[



    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },


    { "$addFields": { "arboles_x_lote": { "$ifNull": ["$lote.properties.custom.Numero de plantas.value", 0] } } },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },



    { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Palma": 0
        }
    }


    , {
        "$match": {
            "Enfermedad": {
                "$in": [
                    "pestalotiopsis"
                ]
            }
        }
    }



    , {
        "$addFields": {
            "nivel": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad",
                                    "pestalotiopsis"
                                ]
                            },
                            "then": {
                                "$ifNull": ["$Enfermedad_Niveles pestalotiopsis", "SIN NIVEL"]
                            }
                        }

                    ],
                    "default": "SIN NIVEL"
                }
            }
        }
    }


    , { "$sort": { "rgDate": -1 } }



    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "arbol": "$arbol"

            }
            , "data": { "$push": "$$ROOT" }

        }
    }

    , {
        "$addFields": {
            "data": { "$arrayElemAt": ["$data", 0] }
        }
    }





    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {}

                ]
            }
        }
    }

    , {
        "$lookup": {
            "from": "cartography",
            "localField": "cartography_id",
            "foreignField": "_id",
            "as": "info_cartografia"
        }
    }
    , { "$unwind": "$info_cartografia" }
    , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_cartografia.geometry", {}] } } }
    , {
        "$project": {
            "info_cartografia": 0
        }
    }

    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$nivel", "SIN NIVEL"] }
                            , "then": "#000000"
                        },
                        {
                            "case": { "$eq": ["$nivel", "Nivel 33"] }
                            , "then": "#008000"
                        },
                        {
                            "case": { "$eq": ["$nivel", "Nivel 25"] }
                            , "then": "#ffff00"
                        },
                        {
                            "case": { "$eq": ["$nivel", "Nivel 17"] }
                            , "then": "#ffa500"
                        },
                        {
                            "case": { "$eq": ["$nivel", "Nivel 9"] }
                            , "then": "#ff0000"
                        }

                    ],
                    "default": "#000000"
                }
            }
        }
    }





    , {
        "$addFields": {
            "idform": "$idform",
            "cartography_id": "$cartography_id",
            "cartography_geometry": "$cartography_geometry",
            "color": "$color"
        }
    }


    , {
        "$project": {
            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },
            "type": "Feature",

            "properties": {
                "Lote": "$lote",
                "Palma": "$arbol",

                "Nivel": "$nivel",

                "color": "$color"
            }
        }
    }







]
