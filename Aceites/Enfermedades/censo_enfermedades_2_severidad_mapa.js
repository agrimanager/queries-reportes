db.form_censodeenfermedades.aggregate(
    [

        // ========= analisis reporte mapa
        // //------OBSERVACIONES QUERIES REPORTES DE MAPA

        // -Variables inyectadas
        // -unwind de campos de seleccion multiple
        // -size de cartografia
        // -NO project
        // -agrupar y desagrupar


        // 1)filtro de fechas
        // 2)unwind de campos de seleccion multiple
        // 3)filtro finca
        // 4)user_timezone
        // 5)cartography y color


        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        // 1)filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },


        // 2)unwind de campos de seleccion multiple
        //   {
        //     "$unwind": {
        //         "path": "$Sintomas determinantes de PCHC",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },


        // // 3)filtro finca
        // {
        //     "$match": {
        //         "Point.farm": "5d13d8c6091c0677116d6fe5"
        //     }
        // },

        // // 4)user_timezone
        // {
        //     "$lookup": {
        //         "from": "users",
        //         "localField": "uid",
        //         "foreignField": "_id",
        //         "as": "user"
        //     }
        // },
        // {
        //     "$addFields": {
        //         "user": {
        //             "$arrayElemAt": [
        //                 "$user.timezone",
        //                 0
        //             ]
        //         },
        //         "today": {
        //             "$toDate": "2022-02-17T22:23:06.290Z"
        //         },
        //         "idform": "5f972c9d92240c46d530ca4f",
        //         "Busqueda inicio": "2022-02-10T22:21:57.000Z",
        //         "Busqueda fin": "2022-02-17T22:21:57.000Z",
        //         "FincaID": "5d13d8c6091c0677116d6fe5"
        //     }
        // },

        // // 5)cartography y color
        // {
        //     "$addFields": {
        //         "Cartography": "$Palma.features",
        //         "color": {
        //             "$switch": {
        //                 "branches": [
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Pudricion de cogollo"
        //                             ]
        //                         },
        //                         "then": "#FFFB02"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Anillo rojo"
        //                             ]
        //                         },
        //                         "then": "#882424"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Marchitez sorpresiva"
        //                             ]
        //                         },
        //                         "then": "#77FF00"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Pudricion basal H"
        //                             ]
        //                         },
        //                         "then": "#8000FF"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Pudricion basal S"
        //                             ]
        //                         },
        //                         "then": "#8F3813"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Pudricion alta Estipite"
        //                             ]
        //                         },
        //                         "then": "#220B63"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Ganoderma"
        //                             ]
        //                         },
        //                         "then": "#FF0095"
        //                     },
        //                     {
        //                         "case": {
        //                             "$eq": [
        //                                 "$Enfermedad",
        //                                 "Pestalotiopsis"
        //                             ]
        //                         },
        //                         "then": "#00FF95"
        //                     }
        //                 ],
        //                 "default": "#cccccc"
        //             }
        //         }
        //     }
        // },

        //----------------------------------


        //================================
        //-----QUERY REPORTE


        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },


        { "$addFields": { "arboles_x_lote": { "$ifNull": ["$lote.properties.custom.Numero de plantas.value", 0] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },


        //mapa
        { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Palma": 0
            }
        }


        , {
            "$match": {
                "Enfermedad": {
                    "$in": [
                        // "pudricion de cogollo pc",
                        "pestalotiopsis"
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "nivel": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "pestalotiopsis"
                                    ]
                                },
                                "then": {
                                    "$ifNull": ["$Enfermedad_Niveles pestalotiopsis", "SIN NIVEL"]
                                }
                            }

                        ],
                        "default": "SIN NIVEL"
                    }
                }
            }
        }


        , { "$sort": { "rgDate": -1 } }



        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "arbol": "$arbol"

                    // , "arboles_x_lote": "$arboles_x_lote"
                    // , "enfermedad": "$Enfermedad"
                    // , "grado_nivel": "$grado_nivel"
                }
                , "data": { "$push": "$$ROOT" }

            }
        }

        //obtener el ultimo
        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$_id.lote"
        //             , "arboles_x_lote": "$_id.arboles_x_lote"
        //             , "enfermedad": "$_id.enfermedad"
        //             , "grado_nivel": "$_id.grado_nivel"
        //         },
        //         "plantas_enfermedad_x_grado_x_lote": { "$sum": 1 }

        //     }
        // }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {}
                        // {"plantas_enfermedad_x_grado_x_lote": "$plantas_enfermedad_x_grado_x_lote"}
                    ]
                }
            }
        }


        //NOTA!!!!!!!!!
        //la geometria del lote rompia la agrupacion del lote
        //....===>>>entonces hacer lookup para sacar geometria
        //------MAPA_VARIABLE_GEOMETRY
        , {
            "$lookup": {
                "from": "cartography",
                "localField": "cartography_id",
                "foreignField": "_id",
                "as": "info_cartografia"
            }
        }
        , { "$unwind": "$info_cartografia" }
        , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_cartografia.geometry", {}] } } }
        , {
            "$project": {
                "info_cartografia": 0
            }
        }
        //----------------



        // //--leyenda
        //Nivel Pestalotiopsis: #ffffff,SIN NIVEL: #000000,Nivel 33: #008000,Nivel 25: #ffff00,Nivel 17: #ffa500,Nivel 9: #ff0000

        // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$nivel", "SIN NIVEL"] }
                                , "then": "#000000"
                            },
                            {
                                "case": { "$eq": ["$nivel", "Nivel 33"] }
                                , "then": "#008000"
                            },
                            {
                                "case": { "$eq": ["$nivel", "Nivel 25"] }
                                , "then": "#ffff00"
                            },
                            {
                                "case": { "$eq": ["$nivel", "Nivel 17"] }
                                , "then": "#ffa500"
                            },
                            {
                                "case": { "$eq": ["$nivel", "Nivel 9"] }
                                , "then": "#ff0000"
                            }

                        ],
                        "default": "#000000"
                    }
                }
            }
        }





        //MAPA FINAL
        , {
            "$addFields": {
                "idform": "$idform",
                "cartography_id": "$cartography_id",
                "cartography_geometry": "$cartography_geometry",
                "color": "$color"
            }
        }


        , {
            "$project": {
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },
                "type": "Feature",

                "properties": {
                    "Lote": "$lote",
                    "Palma": "$arbol",

                    "Nivel": "$nivel",

                    "color": "$color"
                }
            }
        }







    ]

)
