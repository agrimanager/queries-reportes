db.form_instacropsdatosestaciones.aggregate(
    [
        /*
        Déficit de presión de vapor
        Humedad Relativa
        Precipitacion
        Precipitación - Diaria
        Precipitación - Hora
        Precipitación - Temporada
        Presión
        Temperatura Ambiente
        */

        //-----CONDICION INICIAL DE PARAMETRO
        {
            "$match": {
                // "Parametro": "XXXXXX"

                "Parametro": "Temperatura Ambiente"
            }

        },


        {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }

            }
        },


        {
            "$project": {
                "num_dia_semana": 0
                , "_id": 0
                , "Point": 0
                , "uid": 0

                , "supervisor": 0
                , "capture": 0
                , "uDate": 0

                , "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0

                , "Estacion ID": 0
                , "Parametro ID": 0

                , "Estacion": 0
            }
        }


        , {
            "$group": {
                "_id": {
                    "parametro": "$Parametro"
                    , "unidad": "$Unidad Medida"
                    , "fecha": "$Fecha_Txt"
                }

                , "valor_agrupado": { "$avg": "$Valor Medicion" }
            }
        }


        //         // collection: form_instacropsdatosestaciones
        // {
        // 	"Parametro" : "Temperatura Ambiente",
        // 	"Unidad Medida" : "°C",
        // 	"Valor Medicion" : 14.74,
        // 	"Fecha" : "2022-10-18T00:07:17.000Z",
        // 	"rgDate" : ISODate("2022-10-18T00:07:17.000-05:00"),
        // 	"num_anio" : 2022,
        // 	"num_mes" : 10,
        // 	"num_dia_mes" : 18,
        // 	"Fecha_Txt" : "2022-10-18",
        // 	"Mes_Txt" : "10-Octubre"
        // }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "valor_agrupado": "$valor_agrupado"
                        }
                    ]
                }
            }
        }






    ]
)
