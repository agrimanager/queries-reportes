db.form_censogeneral.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$Censo"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "-"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "-"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "-"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "-"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "-"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Censo": 0,
                "Point": 0,
                "bloque_id": 0
            }
        },



        {
            "$addFields": {
                "cantidad de fruta": { "$ifNull": ["$cantidad de fruta", ""] },

                "Ciclo Stenoma": { "$ifNull": ["$Ciclo Stenoma", ""] },
                "Observaciones stenoma": { "$ifNull": ["$Observaciones stenoma", ""] },
                "Cuarentenarias": { "$ifNull": ["$Cuarentenarias", ""] }
            }
        }

        , {
            "$addFields": {
                "Fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%Y-%m-%d",
                        "timezone": "America/Bogota"
                    }
                }
            }
        }



        , {
            "$addFields": {
                "Hora": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%H",
                        "timezone": "America/Bogota"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "rango_hora": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$gte": [{ "$toDouble": "$Hora" }, 6] }
                                , { "$lte": [{ "$toDouble": "$Hora" }, 12] }
                            ]
                        },
                        "then": "A-Dia-(6-12)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gte": [{ "$toDouble": "$Hora" }, 13] }
                                        , { "$lte": [{ "$toDouble": "$Hora" }, 16] }
                                    ]
                                },
                                "then": "B-Tarde-(1-4)",
                                "else": "C-(otro)"
                            }
                        }
                    }
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "fecha_censo": "$Fecha",
                    "hora": "$Hora",
                    "rango_hora": "$rango_hora",
                    "empleado": "$supervisor"

                }
                ,"cantidad_censos":{"$sum":1}

            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "cantidad_censos": "$cantidad_censos"
                        }
                    ]
                }
            }
        }




    ]
)
