db.form_censogeneral.aggregate(

    [


        //CONDICION REPORTE
        {
            "$match": {
                "Plagas": { "$ne": "" },
                "Enfermedades": { "$ne": "" }
            }
        },

        {
            "$addFields": {
                "Cuarentenarias": { "$ifNull": ["$Cuarentenarias", []] }

                , "Phytohpthora": { "$ifNull": ["$Phytohpthora", ""] }
                , "Resiembra": { "$ifNull": ["$Resiembra", ""] }

            }
        },
        {
            "$addFields": {
                "Cuarentenarias": {
                    "$cond": {
                        "if": { "$eq": ["$Cuarentenarias", ""] },
                        "then": [],
                        "else": "$Cuarentenarias"
                    }
                }
            }
        },
        {
            "$addFields": {
                "Phytohpthora": {
                    "$cond": {
                        "if": { "$eq": ["$Phytohpthora", ""] },
                        "then": [],
                        "else": [
                            // {"$concat": ["Phytohpthora ", "$Phytohpthora"]}
                            "Phytohpthora"
                        ]
                    }
                }
            }
        },

        {
            "$addFields": {
                "Resiembra": {
                    "$cond": {
                        "if": { "$eq": ["$Resiembra", ""] },
                        "then": [],
                        "else": [
                            {
                                "$concat": ["Resiembra ", "$Resiembra"]
                            }
                        ]
                    }
                }
            }
        },

        {
            "$match": {
                "$expr": {
                    "$or": [
                        //Plagas
                        { "$in": ["Acaros", "$Plagas"] },
                        { "$in": ["Compsus", "$Plagas"] },
                        { "$in": ["Pandeleteius", "$Plagas"] },
                        { "$in": ["Monalonion", "$Plagas"] },

                        //Enfermedades
                        { "$in": ["Fruto rojo", "$Enfermedades"] },
                        { "$in": ["Chancro", "$Enfermedades"] },
                        { "$in": ["Verticillum", "$Enfermedades"] },

                        //Cuarentenarias
                        { "$in": ["Stenoma", "$Cuarentenarias"] }

                        //Resiembra
                        //,{ "$in": ["Mala", "$Resiembra"] }
                        , { "$in": ["Resiembra Mala", "$Resiembra"] }
                    ]
                }
            }
        },





        //---cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Censo"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "-"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "-"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "-"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "-"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "-"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Censo": 0,
                "Point": 0,
                "bloque_id": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0

                // , "Resiembra": 0
                // , "Phytohpthora": 0
                , "Soca": 0
                , "Adulto": 0

            }
        }


        //---info fechas
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_semana": { "$week": { "date": "$rgDate" } }
            }
        }


        //juntar datos
        , {
            "$addFields": {

                "array_data_concat": {
                    //"$concatArrays": ["$Plagas", "$Enfermedades", "$Cuarentenarias"]
                    "$concatArrays": ["$Plagas", "$Enfermedades", "$Cuarentenarias", "$Phytohpthora", "$Resiembra"]
                }
            }

        }

        , {
            "$addFields": {
                "array_data_concat": {
                    "$filter": {
                        "input": "$array_data_concat",
                        "as": "item",
                        //"cond": { "$in": ["$$item", ["Acaros", "Compsus", "Fruto rojo", "Chancro", "Stenoma"]] }
                        "cond": {
                            "$in": ["$$item", [
                                "Acaros", "Compsus", "Fruto rojo", "Chancro", "Stenoma", "Pandeleteius", "Monalonion", "Verticillum"
                                , "Phytohpthora"
                                , "Resiembra Mala"
                            ]]
                        }
                    }
                }
            }
        }


        , {
            "$unwind": {
                "path": "$array_data_concat",
                "preserveNullAndEmptyArrays": false
            }
        }






        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    // "linea": "$linea",
                    "arbol": "$arbol",

                    "num_anio": "$num_anio",
                    "num_semana": "$num_semana",

                    "array_data_concat": "$array_data_concat"

                }
                , "cantidad": { "$sum": 1 }

            }
        }

        , {
            "$group": {
                "_id": {
                    // "finca": "$finca",
                    // "bloque": "$bloque",
                    // "lote": "$lote",
                    // // "linea": "$linea",
                    // "arbol": "$arbol",

                    "num_anio": "$_id.num_anio",
                    "num_semana": "$_id.num_semana",

                    "array_data_concat": "$_id.array_data_concat"

                }
                , "cantidad_agrupada": { "$sum": 1 }

            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "cantidad_agrupada": "$cantidad_agrupada"
                        }
                    ]
                }
            }
        }




    ]

)

    // .count()
