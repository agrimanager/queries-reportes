
//====================================
//---ejemplo sanjose
//method: POST
//finca: Semana Santa
//finca_id: 5d27d22e793a4867b305012c
//Request URL: https://map.agrimanager.app/sanjose/api/mobile/5d27d22e793a4867b305012c.sqlite

/*
https://map.agrimanager.app/
DB_NAME
/api/mobile/
FINCA_ID
.sqlite
*/



//----Ojo usar desde db local

//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["lukeragricola"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {


    //--obtener formularios
    var fincas = db.getSiblingDB(db_name).farms.aggregate(
        {
            $addFields: {
                finca_id: { $toString: "$_id" }
            }
        }
    ).allowDiskUse();

    //--🔄 ciclar formularios
    fincas.forEach(item => {

        result_info.push({
            database: db_name,
            finca: item.name,
            finca_oid: item._id,
            finca_id: item.finca_id

            //,url_generar_cartografia : "https://map.agrimanager.app/" + db_name + "/api/mobile/" + item.finca_id + ".sqlite"
            ,servicio_generar_cartografia : "Invoke-WebRequest -Uri https://map.agrimanager.app/" + db_name + "/api/mobile/" + item.finca_id + ".sqlite  -Method POST"

        })

    });

});

//--imprimir resultado
result_info
