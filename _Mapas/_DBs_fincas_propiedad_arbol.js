
//----Ojo usar desde db local

//====== Resultado
var result_info = [];


//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["jaremar", "lukeragricola"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {


    //--obtener formularios
    var fincas = db.getSiblingDB(db_name).farms.aggregate(
        {
            $addFields: {
                finca_id: { $toString: "$_id" }
            }
        }
    ).allowDiskUse();

    //--🔄 ciclar formularios
    fincas.forEach(item => {


        if (Array.isArray(item.defaultProperties.trees) && item.defaultProperties.trees.length  === 0) {
            result_info.push({
                database: db_name,
                finca: item.name,
                finca_propiedad_arbol: "NO",
                // finca_propiedad_arbol_aux: item.defaultProperties.trees,
            })

        } else {
            result_info.push({
                database: db_name,
                finca: item.name,
                finca_propiedad_arbol: "SI",
                // finca_propiedad_arbol_aux: item.defaultProperties.trees,
            })
        }



    });

});

//--imprimir resultado
result_info
