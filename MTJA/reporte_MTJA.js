db.form_sanidadmtja.aggregate(

    [

        {
            "$unwind": "$Enfermedades"
        },
        {
            "$unwind": "$Plagas"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Acaros": 0,
                "%Fenologia": 0,
                "%Estao del fruto": 0
            }
        }



        //--- GROUP BY
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                },
                "data": {
                    // "$push": {
                    //     "_id": "$_id",
                    //     "Postura de acaros": "$Postura de acaros",
                    //     "Acaros_Blanco": "$Acaros_Blanco",
                    //     "Acaros_Rojo": "$Acaros_Rojo",
                    //     "Acaros_Tostador": "$Acaros_Tostador",
                    //     "%Fenologia_%Brotación": "$%Fenologia_%Brotación",
                    //     "%Fenologia_%Floración": "$%Fenologia_%Floración",
                    //     "%Fenologia_%Llenado": "$%Fenologia_%Llenado",
                    //     "%Estado del fruto_F1": "$%Estado del fruto_F1",
                    //     "%Estado del fruto_F2": "$%Estado del fruto_F2",
                    //     "%Estado del fruto_F3": "$%Estado del fruto_F3",
                    //     "Arbol": "$Arbol",
                    //     "Enfermedades": "$Enfermedades",
                    //     "Plagas": "$Plagas",
                    //     "Orthezia": "$Orthezia",
                    //     "Observaciones": "$Observaciones",
                    //     "Point": "$Point",
                    //     "uid": "$uid",
                    //     "supervisor": "$supervisor",
                    //     "rgDate": "$rgDate",
                    //     "uDate": "$uDate",
                    //     "capture": "$capture",
                    //     "finca": "$finca",
                    //     "bloque": "$bloque",
                    //     "lote": "$lote",
                    //     "linea": "$linea",
                    //     "planta": "$planta"
                    // }
                    "$push": "$$ROOT"
                },
                "plantas": {
                    "$addToSet": "$planta"
                }
            }
        },
        {
            "$addFields": {
                "data.diferentes plantas": { "$size": "$plantas" }
            }
        },
        {
            "$project": {
                "_id": 0,
                "plantas": 0,
            }
        },
        {
            "$unwind": "$data"
        },
        {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }



    ]

)