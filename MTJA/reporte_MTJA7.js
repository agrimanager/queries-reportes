db.form_sanidadmtja.aggregate(

    [
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
    
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
    
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
    
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name",
                "id_lote": "$lote._id"
            }
        },
    
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
    
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" }
    
        , {
            "$addFields": {
                "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }
    
    
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "fecha": "$fecha"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "plantas": {
                    "$addToSet": "$planta"
                }
            }
        },
        {
            "$addFields": {
                "data.diferentes_plantas": { "$size": "$plantas" }
            }
        },
        {
            "$project": {
                "_id": 0,
                "plantas": 0
            }
        },
        {
            "$unwind": "$data"
        },
        {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }
        ,{
            "$group": {
                "_id": "$planta",
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }
        ,{
            "$addFields": {
                "nombres_plagas": {
                    "$reduce": {
                        "input": "$data.Plagas",
                        "initialValue": [],
                        "in": { "$setUnion": ["$$value", "$$this"] }
                    }
                },
                "nombres_enfermedades": {
                    "$reduce": {
                        "input": "$data.Enfermedades",
                        "initialValue": [],
                        "in": { "$setUnion": ["$$value", "$$this"] }
                    }
                }
            }
        }
        ,{
            "$addFields": {
                "data.nombres_plagas": {
                    "$map": {
                        "input": "$nombres_plagas",
                        "as": "plaga",
                        "in": [
                            "$$plaga",
                            0
                        ]
                    }
                },
                "data.nombres_enfermedades": {
                    "$map": {
                        "input": "$nombres_enfermedades",
                        "as": "enfermedad",
                        "in": [
                            "$$enfermedad",
                            0
                        ]
                    }
                }
            }
        }
        ,{
            "$addFields": {
                "data.cantidad_total_plagas": {
                    "$map": {
                        "input": "$nombres_plagas",
                        "as": "plaga",
                        "in": [
                            "$$plaga",
                            {
                                "$reduce": {
                                    "input": "$data.Plagas",
                                    "initialValue": 0,
                                    "in": {
                                        "$cond": {
                                            "if": { "$in": ["$$plaga", "$$this"] },
                                            "then": { "$sum": ["$$value", 1] },
                                            "else": { "$sum": ["$$value", 0] }
                                        }
                                    }
                                }
                            }
                        ]
                    }
                },
                "data.cantidad_total_enfermedades": {
                    "$map": {
                        "input": "$nombres_enfermedades",
                        "as": "enfermedad",
                        "in": [
                            "$$enfermedad",
                            {
                                "$reduce": {
                                    "input": "$data.Enfermedades",
                                    "initialValue": 0,
                                    "in": {
                                        "$cond": {
                                            "if": { "$in": ["$$enfermedad", "$$this"] },
                                            "then": { "$sum": ["$$value", 1] },
                                            "else": { "$sum": ["$$value", 0] }
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
        ,{
            "$unwind": "$data"
        }
        ,{
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }
        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Acaros": 0
            }
        }
        ,{
            "$group": {
                "_id": "_",
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }
        ,{
            "$addFields": {
                "data.nombres_plagas": {
                    "$reduce": {
                        "input": "$data.Plagas",
                        "initialValue": [],
                        "in": { "$setUnion": ["$$value", "$$this"] }
                    }
                },
                "data.nombres_enfermedades": {
                    "$reduce": {
                        "input": "$data.Enfermedades",
                        "initialValue": [],
                        "in": { "$setUnion": ["$$value", "$$this"] }
                    }
                }
            }
        }
        ,{
            "$unwind": "$data"
        }
        ,{
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }
        ,{
            "$addFields": {
                "nombres_plagas": {
                    "$arrayToObject": {
                        "$map": {
                            "input": "$nombres_plagas",
                            "as": "nombre_plaga",
                            "in": ["$$nombre_plaga", 0]
                        }
                    }
                },
                "nombres_enfermedades": {
                    "$arrayToObject": {
                        "$map": {
                            "input": "$nombres_enfermedades",
                            "as": "nombre_enfermedad",
                            "in": ["$$nombre_enfermedad", 0]
                        }
                    }
                },
                "Plagas": {
                    "$arrayToObject": {
                        "$map": {
                            "input": "$Plagas",
                            "as": "plaga",
                            "in": [
                                "$$plaga",
                                {
                                    "$multiply": [
                                        100,
                                        {
                                            "$divide": [
                                                1,
                                                {
                                                    "$reduce": {
                                                        "input": "$cantidad_total_plagas",
                                                        "initialValue": "$diferentes_plantas",
                                                        "in": {
                                                            "$cond": {
                                                                "if": {
                                                                    "$and": [
                                                                        { "$in": ["$$plaga", "$$this"] },
                                                                        { "$gt": [{"$arrayElemAt": ["$$this", 1]}, 1]}
                                                                    ]
                                                                },
                                                                "then": {"$sum": ["$$value", {"$arrayElemAt": ["$$this", 1]}]},
                                                                "else": "$$value"
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                },
                "Enfermedades": {
                    "$arrayToObject": {
                        "$map": {
                            "input": "$Enfermedades",
                            "as": "enfermedad",
                            "in": [
                                "$$enfermedad",
                                {
                                    "$multiply": [
                                        100,
                                        {
                                            "$divide": [
                                                1,
                                                {
                                                    "$reduce": {
                                                        "input": "$cantidad_total_enfermedades",
                                                        "initialValue": "$diferentes_plantas",
                                                        "in": {
                                                            "$cond": {
                                                                "if": {
                                                                    "$and": [
                                                                        { "$in": ["$$enfermedad", "$$this"] },
                                                                        { "$gt": [{"$arrayElemAt": ["$$this", 1]}, 1]}
                                                                    ]
                                                                },
                                                                "then": {"$sum": ["$$value", {"$arrayElemAt": ["$$this", 1]}]},
                                                                "else": "$$value"
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                }
            }
        }
        ,{
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$$ROOT",
                        "$nombres_plagas",
                        "$nombres_enfermedades",
                        "$Plagas",
                        "$Enfermedades"
                    ]
                }
            }
        }
        ,{
            "$project": {
                "nombres_plagas": 0,
                "nombres_enfermedades": 0,
                "cantidad_total_plagas": 0,
                "cantidad_total_enfermedades": 0,
                "Plagas": 0,
                "Enfermedades": 0
            }
        }
        ,{
            "$lookup": {
                "from": "form_formulariofenologia",
                "as": "datos_fenologia",
                "let": {
                    "fecha_sanidad": "$fecha",
                    "id_lote_sanidad": "$id_lote"
                    
                },
                "pipeline": [
                    {
                        "$addFields": {
                            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$fecha", "$$fecha_sanidad"]
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote o Arbol.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote o Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
    
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },
    
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },
                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "objeto_del_cultivo",
                                    "cond": {
                                        "$eq": ["$$objeto_del_cultivo.properties.type", "lot"]
                                    }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": "$lote"
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$lote._id", "$$id_lote_sanidad"]
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "lote": "$lote._id",
                                "fecha": "$fecha"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_registros_fenologia": {
                                "$sum": 1
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "data.total_registros_fenologia": "$total_registros_fenologia"
                        }
                    },
                    {
                        "$unwind": "$data"
                    },
                    {
                        "$replaceRoot": {
                            "newRoot": "$data"
                        }
                    },
                    {
                        "$project": {
                            "_id": 0,
                            "%Fenologia_%Brotación": 1,
                            "%Fenologia_%Floración": 1,
                            "%Fenologia_%Llenado": 1,
                            "Estado del fruto_F1": 1,
                            "Estado del fruto_F2": 1,
                            "Estado del fruto_F3": 1,
                            "total_registros_fenologia": 1
                        }
                    }
                ]
            }
        },
        {
            "$addFields": {
                "total_registros_fenologia": {
                    "$arrayElemAt": ["$datos_fenologia.total_registros_fenologia", 0]
                }
            }
        },
        {
            "$addFields": {
                "datos_fenologia": {
                    "$reduce": {
                        "input": "$datos_fenologia",
                        "initialValue": {
                            "%Fenologia_%Brotación": 0,
                            "%Fenologia_%Floración": 0,
                            "%Fenologia_%Llenado": 0,
                            "Estado del fruto_F1": 0,
                            "Estado del fruto_F2": 0,
                            "Estado del fruto_F3": 0
                        },
                        "in": {
                            "%Fenologia_%Brotación": {
                                "$sum": ["$$value.%Fenologia_%Brotación", {"$toDouble": "$$this.%Fenologia_%Brotación"}]
                            },
                            "%Fenologia_%Floración": {
                                "$sum": ["$$value.%Fenologia_%Floración", {"$toDouble": "$$this.%Fenologia_%Floración"}]
                            },
                            "%Fenologia_%Llenado": {
                                "$sum": ["$$value.%Fenologia_%Llenado", {"$toDouble": "$$this.%Fenologia_%Llenado"}]
                            },
                            "Estado del fruto_F1": {
                                "$sum": ["$$value.Estado del fruto_F1", {"$toDouble": "$$this.Estado del fruto_F1"}]
                            },
                            "Estado del fruto_F2": {
                                "$sum": ["$$value.Estado del fruto_F2", {"$toDouble": "$$this.Estado del fruto_F2"}]
                            },
                            "Estado del fruto_F3": {
                                "$sum": ["$$value.Estado del fruto_F3", {"$toDouble": "$$this.Estado del fruto_F3"}]
                            }
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "datos_fenologia": {
                    "%Fenologia_%Brotación": {
                        "$divide": ["$datos_fenologia.%Fenologia_%Brotación", "$total_registros_fenologia"]
                    },
                    "%Fenologia_%Floración": {
                        "$divide": ["$datos_fenologia.%Fenologia_%Floración", "$total_registros_fenologia"]
                    },
                    "%Fenologia_%Llenado": {
                        "$divide": ["$datos_fenologia.%Fenologia_%Llenado", "$total_registros_fenologia"]
                    },
                    "Estado del fruto_F1": {
                        "$divide": ["$datos_fenologia.Estado del fruto_F1", "$total_registros_fenologia"]
                    },
                    "Estado del fruto_F2": {
                        "$divide": ["$datos_fenologia.Estado del fruto_F2", "$total_registros_fenologia"]
                    },
                    "Estado del fruto_F3": {
                        "$divide": ["$datos_fenologia.Estado del fruto_F3", "$total_registros_fenologia"]
                    }
                }
            }
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$$ROOT",
                        "$datos_fenologia"
                    ]
                }
            }
        }
        ,{
            "$project": {
                "total_registros_fenologia": 0,
                "datos_fenologia": 0
            }
        }
    ]
)