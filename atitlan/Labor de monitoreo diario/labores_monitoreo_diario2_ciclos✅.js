[


    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote_hectareas": { "$ifNull": ["$lote.properties.custom.hectareas.value", 0] } } },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Lote": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0


            , "Busqueda inicio": 0
            , "Busqueda fin": 0
            , "today": 0

        }
    }


    , {
        "$addFields": {
            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }



    , {
        "$addFields": {
            "Año": { "$year": { "date": "$rgDate" } }
        }
    }



    , {
        "$sort": {
            "rgDate": 1
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "lote": "$lote"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }





    , {
        "$addFields": {
            "datos_inicio": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Estado del Lote", "Inicio de lote"] }
                }
            }
        }
    }
    , {
        "$match": {
            "datos_inicio": { "$ne": [] }
        }
    }

    , {
        "$addFields": {
            "fechas_inicio": {
                "$map": {
                    "input": "$datos_inicio",
                    "as": "item",
                    "in": "$$item.rgDate"
                }
            }
        }
    }


    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$fechas_inicio",
                    "as": "item_fechas_inicio",
                    "in": {
                        "fecha_inicio": "$$item_fechas_inicio",

                        "datos_ciclo": {

                            "$filter": {
                                "input": "$data",
                                "as": "item_data",
                                "cond": {
                                    "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$data_ciclos",
                    "as": "item_data_ciclos",
                    "in": {
                        "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                        "datos_ciclo": {


                            "$reduce": {
                                "input": "$$item_data_ciclos.datos_ciclo",
                                "initialValue": {
                                    "estado_ciclo": "Inicio de lote"
                                    , "datos": []
                                },

                                "in": {
                                    "estado_ciclo": {
                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Cierre de lote"] },
                                            "then": "$$value.estado_ciclo",
                                            "else": "$$this.Estado del Lote"
                                        }
                                    }

                                    , "datos": {

                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Cierre de lote"] },
                                            "then": "$$value.datos",
                                            "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
    }



    , {
        "$project": {
            "data": 0
        }
    }


    , { "$unwind": "$data_ciclos" }


    , {
        "$addFields": {
            "fecha_fin": {
                "$reduce": {
                    "input": "$data_ciclos.datos_ciclo.datos",
                    "initialValue": "",
                    "in": "$$this.rgDate"
                }
            }
        }
    }


    , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_ciclos.datos_ciclo.datos",
                    {

                        "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                        , "ciclo_fecha_fin": "$fecha_fin"
                        , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "ciclo_año": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                }
            }
            , "ciclo_semana": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }


    , {
        "$addFields": {
            "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
            , "ciclo_fecha_fin": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }



    , {
        "$group": {
            "_id": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"



                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"
                , "ciclo_año": "$ciclo_año"
                , "ciclo_semana": "$ciclo_semana"

                , "lote_hectareas": "$lote_hectareas"



            }

            , "supervisor": { "$max": "$supervisor" }
            , "data": { "$push": "$$ROOT" }
        }
    }



    , {
        "$addFields": {
            "dias_ciclo": {
                "$divide": [
                    {
                        "$subtract": [
                            { "$dateFromString": { "dateString": "$_id.ciclo_fecha_fin", "format": "%d/%m/%Y" } },
                            { "$dateFromString": { "dateString": "$_id.ciclo_fecha_inicio", "format": "%d/%m/%Y" } }
                        ]
                    },
                    86400000
                ]
            }
        }
    }


    , {
        "$addFields": {
            "dias_ciclo": { "$floor": "$dias_ciclo" }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {

                        "dias_ciclo": "$dias_ciclo"
                        , "supervisor": "$supervisor"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "dias_ciclo": { "$add": ["$dias_ciclo", 1] }
        }
    }


    , {
        "$addFields": {
            "rendimiento medido": {
                "$cond": {
                    "if": { "$eq": ["$dias_ciclo", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$lote_hectareas",
                            "$dias_ciclo"]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "rendimiento medido": { "$divide": [{ "$subtract": [{ "$multiply": ["$rendimiento medido", 100] }, { "$mod": [{ "$multiply": ["$rendimiento medido", 100] }, 1] }] }, 100] }
        }
    }




]
