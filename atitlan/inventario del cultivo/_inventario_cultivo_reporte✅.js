[




    { "$limit": 1 },
    {
        "$lookup": {
            "from": "cartography",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["lot"] }
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote arboles Totales": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0

                        , "type": 0
                        , "properties": 0
                        , "geometry": 0
                        , "path": 0
                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartografia",
            "let": {
                "lote_id": "$_id",
                "lote_name": "$lote"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }

                        , "properties.custom.Muerto.value": true
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote_id": { "$ifNull": ["$lote._id", ""] } } },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": ["$$lote_id", "$lote_id"]
                                },
                                {
                                    "$eq": ["$$lote_name", "$lote"]
                                }
                            ]
                        }
                    }
                }


                , {
                    "$project": {
                        "_id": 1
                    }
                }

                , {
                    "$group": {
                        "_id": null
                        , "cantidad_arboles": { "$sum": 1 }
                    }
                }


            ]
        }
    }


    , {
        "$unwind": {
            "path": "$data_cartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "num_Muerto": { "$ifNull": ["$data_cartografia.cantidad_arboles", 0] }
        }
    }
    , {
        "$project": {
            "data_cartografia": 0
        }
    }

    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartografia",
            "let": {
                "lote_id": "$_id",
                "lote_name": "$lote"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }

                        , "properties.custom.Hoyo.value": true
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote_id": { "$ifNull": ["$lote._id", ""] } } },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": ["$$lote_id", "$lote_id"]
                                },
                                {
                                    "$eq": ["$$lote_name", "$lote"]
                                }
                            ]
                        }
                    }
                }


                , {
                    "$project": {
                        "_id": 1
                    }
                }

                , {
                    "$group": {
                        "_id": null
                        , "cantidad_arboles": { "$sum": 1 }
                    }
                }


            ]
        }
    }


    , {
        "$unwind": {
            "path": "$data_cartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "num_Hoyo": { "$ifNull": ["$data_cartografia.cantidad_arboles", 0] }
        }
    }
    , {
        "$project": {
            "data_cartografia": 0
        }
    }

    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartografia",
            "let": {
                "lote_id": "$_id",
                "lote_name": "$lote"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }
                        , "properties.custom.No Existe.value": true
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote_id": { "$ifNull": ["$lote._id", ""] } } },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": ["$$lote_id", "$lote_id"]
                                },
                                {
                                    "$eq": ["$$lote_name", "$lote"]
                                }
                            ]
                        }
                    }
                }


                , {
                    "$project": {
                        "_id": 1
                    }
                }

                , {
                    "$group": {
                        "_id": null
                        , "cantidad_arboles": { "$sum": 1 }
                    }
                }


            ]
        }
    }


    , {
        "$unwind": {
            "path": "$data_cartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "num_No Existe": { "$ifNull": ["$data_cartografia.cantidad_arboles", 0] }
        }
    }
    , {
        "$project": {
            "data_cartografia": 0
        }
    }

    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartografia",
            "let": {
                "lote_id": "$_id",
                "lote_name": "$lote"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }

                        , "properties.custom.Resiembra.value": true
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote_id": { "$ifNull": ["$lote._id", ""] } } },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": ["$$lote_id", "$lote_id"]
                                },
                                {
                                    "$eq": ["$$lote_name", "$lote"]
                                }
                            ]
                        }
                    }
                }


                , {
                    "$project": {
                        "_id": 1
                    }
                }

                , {
                    "$group": {
                        "_id": null
                        , "cantidad_arboles": { "$sum": 1 }
                    }
                }


            ]
        }
    }


    , {
        "$unwind": {
            "path": "$data_cartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "num_Resiembra": { "$ifNull": ["$data_cartografia.cantidad_arboles", 0] }
        }
    }
    , {
        "$project": {
            "data_cartografia": 0
        }
    }

    , {
        "$addFields": {
            "lote arboles ACTIVOS": {
                "$add": [
                    "$lote arboles Totales"

                    , { "$multiply": ["$num_Muerto", -1] }
                    , { "$multiply": ["$num_Hoyo", -1] }
                    , { "$multiply": ["$num_No Existe", -1] }
                ]
            }
        }
    }




]
