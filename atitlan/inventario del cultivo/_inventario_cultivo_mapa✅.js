[




    { "$limit": 1 },
    {
        "$lookup": {
            "from": "cartography",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"

                , "idform": "$idform"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },




                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },


                { "$addFields": { "arbol_Muerto": { "$ifNull": ["$arbol.properties.custom.Muerto.value", false] } } },
                { "$addFields": { "arbol_Hoyo": { "$ifNull": ["$arbol.properties.custom.Hoyo.value", false] } } },
                { "$addFields": { "arbol_No Existe": { "$ifNull": ["$arbol.properties.custom.No Existe.value", false] } } },
                { "$addFields": { "arbol_Resiembra": { "$ifNull": ["$arbol.properties.custom.Resiembra.value", false] } } },


                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },





                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0

                        , "type": 0
                        , "properties": 0
                        , "path": 0


                        , "finca": 0
                    }
                }

                , {
                    "$addFields": {
                        "color": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": ["$arbol_Muerto", true] }
                                        , "then": "#0B0000"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_Hoyo", true] }
                                        , "then": "#CC0001"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_No Existe", true] }
                                        , "then": "#03A9F4"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_Resiembra", true] }
                                        , "then": "#B350DC"
                                    }

                                ],
                                "default": "#39AE37"
                            }
                        }

                        , "rango": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": ["$arbol_Muerto", true] }
                                        , "then": "Muerto"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_Hoyo", true] }
                                        , "then": "Hoyo"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_No Existe", true] }
                                        , "then": "No Existe"
                                    },
                                    {
                                        "case": { "$eq": ["$arbol_Resiembra", true] }
                                        , "then": "Resiembra"
                                    }

                                ],
                                "default": "sin_propiedades"
                            }
                        }

                    }
                }


                , {
                    "$project": {

                        "_id": "$_id",
                        "idform": "$$idform",
                        "geometry": { "$ifNull": ["$geometry", {}] },

                        "type": "Feature",


                        "properties": {

                            "Bloque": "$bloque",
                            "Lote": "$lote",
                            "PROPIEDAD": { "$ifNull": ["$rango", "SIN DATOS"] },
                            "color": "$color"
                        }

                    }
                }



            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }






]
