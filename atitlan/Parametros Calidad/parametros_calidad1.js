db.form_parametrosdecalidad.aggregate(

    [
        {
            "$addFields": {
                "variable_cartografia": "$Bloque o Lote"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Bloque o Lote": 0

                , "uid": 0
                , "uDate": 0


                , "Herbicida en 6 Surco": 0,
                "Plateo Quimico en 100 platos": 0,
                "Herbicida en 6 Calles": 0,
                "Chapia Mecanizada  en 6 Calles": 0,
                "Correccion de Tutorado en 100 arboles": 0,
                "Fertilizacion Edafica en 100 arboles": 0,
                "Aplicacion de Mulch en 100 platos": 0
            }
        }



        //fecha
        , {
            "$addFields": {
                "Fecha de Evaluacion de Labor": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Evaluacion de Labor", "timezone": "America/Bogota" } }
            }
        }

        , {
            "$addFields": {
                "rgDate_txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }



        //datos_maestro numericos
        , {
            "$addFields": {
                "data_array_parametros": [

                    // {
                    //     "tipo_parametro": "XXX"
                    //     , "nombre_parametro": "XXX1"
                    //     , "valor_parametro": "$XXX1"
                    //     , "constante_divisor": 100
                    // },


                    // {"tipo_parametro": "
                    // XXX
                    // ", "nombre_parametro": "
                    // XXX1
                    // ", "valor_parametro": "$
                    // XXX1
                    // ", "constante_divisor":
                    // 100
                    // },

                    { "tipo_parametro": "Herbicida en 6 Surco", "nombre_parametro": "Surco Excelente", "valor_parametro": "$Surco Excelente", "constante_divisor": 6 },
                    { "tipo_parametro": "Herbicida en 6 Surco", "nombre_parametro": "Surco Bueno", "valor_parametro": "$Surco Bueno", "constante_divisor": 6 },
                    { "tipo_parametro": "Herbicida en 6 Surco", "nombre_parametro": "Surco Malo", "valor_parametro": "$Surco Malo", "constante_divisor": 6 },
                    { "tipo_parametro": "Plateo Quimico en 100 platos", "nombre_parametro": "Platos Excelente", "valor_parametro": "$Platos Excelente", "constante_divisor": 100 },
                    { "tipo_parametro": "Plateo Quimico en 100 platos", "nombre_parametro": "Platos Buenos", "valor_parametro": "$Platos Buenos", "constante_divisor": 100 },
                    { "tipo_parametro": "Plateo Quimico en 100 platos", "nombre_parametro": "Platos Malos", "valor_parametro": "$Platos Malos", "constante_divisor": 100 },
                    { "tipo_parametro": "Herbicida en 6 Calles", "nombre_parametro": "Calles Excelentes", "valor_parametro": "$Calles Excelentes", "constante_divisor": 6 },
                    { "tipo_parametro": "Herbicida en 6 Calles", "nombre_parametro": "Calles Buenas", "valor_parametro": "$Calles Buenas", "constante_divisor": 6 },
                    { "tipo_parametro": "Herbicida en 6 Calles", "nombre_parametro": "Calles Malas", "valor_parametro": "$Calles Malas", "constante_divisor": 6 },
                    { "tipo_parametro": "Chapia Mecanizada  en 6 Calles", "nombre_parametro": "Chapia Excelente", "valor_parametro": "$Chapia Excelente", "constante_divisor": 6 },
                    { "tipo_parametro": "Chapia Mecanizada  en 6 Calles", "nombre_parametro": "Chapia Buena", "valor_parametro": "$Chapia Buena", "constante_divisor": 6 },
                    { "tipo_parametro": "Chapia Mecanizada  en 6 Calles", "nombre_parametro": "Chapia Mala", "valor_parametro": "$Chapia Mala", "constante_divisor": 6 },
                    { "tipo_parametro": "Correccion de Tutorado en 100 arboles", "nombre_parametro": "Tutorado Excelente", "valor_parametro": "$Tutorado Excelente", "constante_divisor": 100 },
                    { "tipo_parametro": "Correccion de Tutorado en 100 arboles", "nombre_parametro": "Tutorado Bueno", "valor_parametro": "$Tutorado Bueno", "constante_divisor": 100 },
                    { "tipo_parametro": "Correccion de Tutorado en 100 arboles", "nombre_parametro": "Tutorado Malo", "valor_parametro": "$Tutorado Malo", "constante_divisor": 100 },
                    { "tipo_parametro": "Fertilizacion Edafica en 100 arboles", "nombre_parametro": "Arboles Excelentes", "valor_parametro": "$Arboles Excelentes", "constante_divisor": 100 },
                    { "tipo_parametro": "Fertilizacion Edafica en 100 arboles", "nombre_parametro": "Arboles Buenos", "valor_parametro": "$Arboles Buenos", "constante_divisor": 100 },
                    { "tipo_parametro": "Fertilizacion Edafica en 100 arboles", "nombre_parametro": "Arboles Malos", "valor_parametro": "$Arboles Malos", "constante_divisor": 100 },
                    { "tipo_parametro": "Aplicacion de Mulch en 100 platos", "nombre_parametro": "Mulch Excelente", "valor_parametro": "$Mulch Excelente", "constante_divisor": 100 },
                    { "tipo_parametro": "Aplicacion de Mulch en 100 platos", "nombre_parametro": "Mulch Bueno", "valor_parametro": "$Mulch Bueno", "constante_divisor": 100 },
                    { "tipo_parametro": "Aplicacion de Mulch en 100 platos", "nombre_parametro": "Mulch Malo", "valor_parametro": "$Mulch Malo", "constante_divisor": 100 }
                    // ,{ "tipo_parametro": "Fertilizacin Foliar", "nombre_parametro": "Fertilizacin Foliar", "valor_parametro": "$Fertilizacin Foliar", "constante_divisor": 0 }



                ]
            }
        }


        , {
            "$project": {
                "Surco Excelente": 0,
                "Surco Bueno": 0,
                "Surco Malo": 0,
                "Platos Excelente": 0,
                "Platos Buenos": 0,
                "Platos Malos": 0,
                "Calles Excelentes": 0,
                "Calles Buenas": 0,
                "Calles Malas": 0,
                "Chapia Excelente": 0,
                "Chapia Buena": 0,
                "Chapia Mala": 0,
                "Tutorado Excelente": 0,
                "Tutorado Bueno": 0,
                "Tutorado Malo": 0,
                "Arboles Excelentes": 0,
                "Arboles Buenos": 0,
                "Arboles Malos": 0,
                "Mulch Excelente": 0,
                "Mulch Bueno": 0,
                "Mulch Malo": 0

            }
        }


        , {
            "$unwind": {
                "path": "$Empleado Evaluado",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "Empleado Evaluado": "$Empleado Evaluado.name"
            }
        }


        , {
            "$unwind": {
                "path": "$data_array_parametros",
                "preserveNullAndEmptyArrays": true
            }
        }



        //         "data_array_parametros" : {
        // 		"tipo_parametro" : "Herbicida en 6 Surco",
        // 		"nombre_parametro" : "Surco Excelente",
        // 		"valor_parametro" : "11",
        // 		"constante_divisor" : 6
        // 	}


        , {
            "$addFields": {
                "tipo_parametro": "$data_array_parametros.tipo_parametro",
                "nombre_parametro": "$data_array_parametros.nombre_parametro",
                // "valor_parametro": "$data_array_parametros.valor_parametro",
                "valor_parametro": { "$toDouble": { "$ifNull": ["$data_array_parametros.valor_parametro", 0] } },
                "constante_divisor": "$data_array_parametros.constante_divisor"
            }
        }


        , {
            "$project": {
                "data_array_parametros": 0
            }
        }


        , {
            "$addFields": {
                "pct_parametro": {
                    "$cond": {
                        "if": { "$eq": ["$constante_divisor", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$valor_parametro",
                                "$constante_divisor"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "pct_parametro": {
                    "$multiply": ["$pct_parametro", 100]
                }
            }
        }

        , {
            "$addFields": {
                "pct_parametro": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_parametro", 100] }, { "$mod": [{ "$multiply": ["$pct_parametro", 100] }, 1] }] }, 100] }
            }
        }








    ]

)
