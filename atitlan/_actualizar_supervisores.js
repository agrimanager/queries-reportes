



//----Ojo usar desde db local

//--obtener bases de datos
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["atitlan"];

//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    console.log(db_name_aux);

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .aggregate(
                [

                    {
                        $match: {
                            "supervisor": {
                                $in: [
                                    "Monitor 1", "Monitor 2", "Monitor 3", "Monitor 4"
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "supervisor_new": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$supervisor", "Monitor 1"] }
                                            , "then": "Angela Maria Fino Castañeda"
                                        },
                                        {
                                            "case": { "$eq": ["$supervisor", "Monitor 2"] }
                                            , "then": "Duverley Gonzales Rojas"
                                        },
                                        {
                                            "case": { "$eq": ["$supervisor", "Monitor 3"] }
                                            , "then": "Jeyson Garcia Vivas"
                                        },
                                        {
                                            "case": { "$eq": ["$supervisor", "Monitor 4"] }
                                            , "then": "Juan Gabriel Santacruz López"
                                        }


                                    ],
                                    "default": ""
                                }
                            }


                        }
                    }

                    , {
                        "$group": {
                            "_id": "$supervisor_new",
                            "ids": {
                                "$push": "$_id"
                            }
                        }
                    }



                ]

            )
            .allowDiskUse();


        //--🔄 data
        data_query.forEach(item_data => {

            result = db.getSiblingDB(db_name)
                .getCollection(coleccion_name)
                .updateMany(
                    {
                        "_id": {
                            $in:item_data.ids
                        }

                    },
                    {
                        $set: {
                            "supervisor": item_data._id
                        }
                    }
                );

        });
        data_query.close();



    });



});

//--imprimir resultado
// result_info
