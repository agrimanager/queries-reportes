var data = db.form_actualizacioncartografica.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-08-11T06:00:00.000-05:00"),
                "Busqueda fin": ISODate("2023-08-12T06:00:00.000-05:00"),
                "today": new Date,
            }
        },
        //----FILTRO FECHAS Y FINCA

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": ["$rgDate", "$Busqueda inicio"]
                        },
                        {
                            "$lte": ["$rgDate", "$Busqueda fin"]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            $unwind: "$Arbol.features"
        },



        {
            $addFields: {
                arbol: { "$toObjectId": "$Arbol.features._id" }
            }
        }




        , {
            $group: {
                _id: {
                    "nombre": "$Condicion del arbol"
                }
                , arboles: { $push: "$arbol" }
                , cant: { $sum: 1 }
            }
        }

        , {
            "$match": {
                "_id.nombre": { $ne: "" }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }




    ], { allowDiskUse: true }
)
// .count()

// data




data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
