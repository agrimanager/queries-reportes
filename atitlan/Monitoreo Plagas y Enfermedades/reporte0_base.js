db.form_monitoreodeplagasyenfermedades2.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0

                , "uid": 0
                , "uDate": 0
                , "supervisor_u": 0
            }
        }



        //========= logica de reporte



        // , {
        //     "$addFields": {
        //         "array_data": [

        //             // {
        //             //     "nombre":"xxxxxx"
        //             //     ,"valor":{ "$ifNull": ["$xxxxxx", "sin_valor"] }
        //             //     ,"tipo":"aaaaaaa"
        //             //     ,"orden":123

        //             // },

        //             // {"nombre":"
        //             // xxxxxx
        //             // ","valor":{ "$ifNull": ["$
        //             // xxxxxx
        //             // ", "sin_valor"] },"tipo":"
        //             // aaaaaaa
        //             // ","orden":
        //             // 123
        //             // },

        //             { "nombre": "Incidencia Acaro Blanco", "valor": { "$ifNull": ["$Incidencia Acaro Blanco", "sin_valor"] }, "tipo": "rangos", "orden": 1 },
        //             { "nombre": "Severidad Acaro Blanco", "valor": { "$ifNull": ["$Severidad Acaro Blanco", "sin_valor"] }, "tipo": "rangos", "orden": 2 },
        //             { "nombre": "Incidencia Acaro Rojo", "valor": { "$ifNull": ["$Incidencia Acaro Rojo", "sin_valor"] }, "tipo": "rangos", "orden": 3 },
        //             { "nombre": "Severidad Acaro Rojo", "valor": { "$ifNull": ["$Severidad Acaro Rojo", "sin_valor"] }, "tipo": "rangos", "orden": 4 },
        //             { "nombre": "Incidencia Afido", "valor": { "$ifNull": ["$Incidencia Afido", "sin_valor"] }, "tipo": "rangos", "orden": 5 },
        //             { "nombre": "Severidad Afido", "valor": { "$ifNull": ["$Severidad Afido", "sin_valor"] }, "tipo": "rangos", "orden": 6 },
        //             { "nombre": "Cantidad de Chizas", "valor": { "$ifNull": ["$Cantidad de Chizas", "sin_valor"] }, "tipo": "rangos", "orden": 7 },
        //             { "nombre": "Evaluacion de Escama", "valor": { "$ifNull": ["$Evaluacion de Escama", "sin_valor"] }, "tipo": "check", "orden": 8 },
        //             { "nombre": "Incidencia de Escama", "valor": { "$ifNull": ["$Incidencia de Escama", "sin_valor"] }, "tipo": "rangos", "orden": 9 },
        //             { "nombre": "Hormiga Arriera", "valor": { "$ifNull": ["$Hormiga Arriera", "sin_valor"] }, "tipo": "check", "orden": 10 },
        //             { "nombre": "Larvas Defoliadoras", "valor": { "$ifNull": ["$Larvas Defoliadoras", "sin_valor"] }, "tipo": "rangos", "orden": 11 },
        //             { "nombre": "Incidencia de Marceno", "valor": { "$ifNull": ["$Incidencia de Marceno", "sin_valor"] }, "tipo": "rangos", "orden": 12 },
        //             { "nombre": "Frutos Afectados por Monalonion", "valor": { "$ifNull": ["$Frutos Afectados por Monalonion", "sin_valor"] }, "tipo": "rangos", "orden": 13 },
        //             { "nombre": "Numero Hojas Afectadas Mosca Blanca", "valor": { "$ifNull": ["$Numero Hojas Afectadas Mosca Blanca", "sin_valor"] }, "tipo": "rangos", "orden": 14 },
        //             { "nombre": "Flores Afectadas por Mosca del Ovario", "valor": { "$ifNull": ["$Flores Afectadas por Mosca del Ovario", "sin_valor"] }, "tipo": "rangos", "orden": 15 },
        //             { "nombre": "Pegador", "valor": { "$ifNull": ["$Pegador", "sin_valor"] }, "tipo": "check", "orden": 16 },
        //             { "nombre": "Incidencia de Picudo", "valor": { "$ifNull": ["$Incidencia de Picudo", "sin_valor"] }, "tipo": "rangos", "orden": 17 },
        //             { "nombre": "Evaluacion de Trips en Terminales", "valor": { "$ifNull": ["$Evaluacion de Trips en Terminales", "sin_valor"] }, "tipo": "rangos", "orden": 18 },
        //             { "nombre": "Evaluacion de Trips en Flores", "valor": { "$ifNull": ["$Evaluacion de Trips en Flores", "sin_valor"] }, "tipo": "rangos", "orden": 19 },
        //             { "nombre": "Pasador de Fruta Stenoma catenifer", "valor": { "$ifNull": ["$Pasador de Fruta Stenoma catenifer", "sin_valor"] }, "tipo": "check", "orden": 20 },
        //             { "nombre": "Barrenador del Tallo Heilipus elegans", "valor": { "$ifNull": ["$Barrenador del Tallo Heilipus elegans", "sin_valor"] }, "tipo": "check", "orden": 21 },
        //             { "nombre": "Barrenador de Semilla Heilipus lauri", "valor": { "$ifNull": ["$Barrenador de Semilla Heilipus lauri", "sin_valor"] }, "tipo": "check", "orden": 22 },
        //             { "nombre": "Barrenador de Semilla Heilipus trifasciatus", "valor": { "$ifNull": ["$Barrenador de Semilla Heilipus trifasciatus", "sin_valor"] }, "tipo": "check", "orden": 23 },
        //             { "nombre": "Antracnosis en Terminal", "valor": { "$ifNull": ["$Antracnosis en Terminal", "sin_valor"] }, "tipo": "rangos", "orden": 24 },
        //             { "nombre": "Antracnosis en Flor", "valor": { "$ifNull": ["$Antracnosis en Flor", "sin_valor"] }, "tipo": "rangos", "orden": 25 },
        //             { "nombre": "Chancro Bacteriano", "valor": { "$ifNull": ["$Chancro Bacteriano", "sin_valor"] }, "tipo": "check", "orden": 26 },
        //             { "nombre": "Declinios", "valor": { "$ifNull": ["$Declinios", "sin_valor"] }, "tipo": "check", "orden": 27 },
        //             { "nombre": "Muerte Descendente en Ramas Lasiodiplodia", "valor": { "$ifNull": ["$Muerte Descendente en Ramas Lasiodiplodia", "sin_valor"] }, "tipo": "rangos", "orden": 28 },
        //             { "nombre": "Mancha Angular Pseudocercospora", "valor": { "$ifNull": ["$Mancha Angular Pseudocercospora", "sin_valor"] }, "tipo": "rangos", "orden": 29 },
        //             { "nombre": "Rona", "valor": { "$ifNull": ["$Rona", "sin_valor"] }, "tipo": "rangos", "orden": 30 },
        //             { "nombre": "Muerte de Ramas Verticillium", "valor": { "$ifNull": ["$Muerte de Ramas Verticillium", "sin_valor"] }, "tipo": "check", "orden": 31 },
        //             { "nombre": "Asfixia Radicular", "valor": { "$ifNull": ["$Asfixia Radicular", "sin_valor"] }, "tipo": "check", "orden": 32 },
        //             { "nombre": "Fruta Bolita", "valor": { "$ifNull": ["$Fruta Bolita", "sin_valor"] }, "tipo": "check", "orden": 33 },
        //             { "nombre": "Dano por Viento", "valor": { "$ifNull": ["$Dano por Viento", "sin_valor"] }, "tipo": "check", "orden": 34 },
        //             { "nombre": "Golpe por Frio", "valor": { "$ifNull": ["$Golpe por Frio", "sin_valor"] }, "tipo": "check", "orden": 35 },
        //             { "nombre": "Golpe de Sol", "valor": { "$ifNull": ["$Golpe de Sol", "sin_valor"] }, "tipo": "check", "orden": 36 },
        //             { "nombre": "Vigor Color de las Hojas", "valor": { "$ifNull": ["$Vigor Color de las Hojas", "sin_valor"] }, "tipo": "rangos", "orden": 37 },
        //             { "nombre": "Vigor Tamano de las Hojas", "valor": { "$ifNull": ["$Vigor Tamano de las Hojas", "sin_valor"] }, "tipo": "rangos", "orden": 38 },
        //             { "nombre": "Vigor Tamano y Ramas Cuarteadas", "valor": { "$ifNull": ["$Vigor Tamano y Ramas Cuarteadas", "sin_valor"] }, "tipo": "rangos", "orden": 39 }


        //         ]
        //     }
        // }


        //juntar valores
        , {
            "$addFields": {
                "array_data": [

                    {
                        "nombre": "Incidencia Acaro Blanco",
                        "valor": {
                            "$ifNull": ["$Incidencia Acaro Blanco", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 1
                    },
                    {
                        "nombre": "Severidad Acaro Blanco",
                        "valor": {
                            "$ifNull": ["$Severidad Acaro Blanco", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 2
                    },
                    {
                        "nombre": "Incidencia Acaro Rojo",
                        "valor": {
                            "$ifNull": ["$Incidencia Acaro Rojo", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 3
                    },
                    {
                        "nombre": "Severidad Acaro Rojo",
                        "valor": {
                            "$ifNull": ["$Severidad Acaro Rojo", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 4
                    },
                    {
                        "nombre": "Incidencia Afido",
                        "valor": {
                            "$ifNull": ["$Incidencia Afido", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 5
                    },
                    {
                        "nombre": "Severidad Afido",
                        "valor": {
                            "$ifNull": ["$Severidad Afido", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 6
                    },
                    {
                        "nombre": "Cantidad de Chizas",
                        "valor": {
                            "$ifNull": ["$Cantidad de Chizas", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 7
                    },
                    {
                        "nombre": "Evaluacion de Escama",
                        "valor": {
                            "$ifNull": ["$Evaluacion de Escama", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 8
                    },
                    {
                        "nombre": "Incidencia de Escama",
                        "valor": {
                            "$ifNull": ["$Incidencia de Escama", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 9
                    },
                    {
                        "nombre": "Hormiga Arriera",
                        "valor": {
                            "$ifNull": ["$Hormiga Arriera", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 10
                    },
                    {
                        "nombre": "Larvas Defoliadoras",
                        "valor": {
                            "$ifNull": ["$Larvas Defoliadoras", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 11
                    },
                    {
                        "nombre": "Incidencia de Marceno",
                        "valor": {
                            "$ifNull": ["$Incidencia de Marceno", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 12
                    },
                    {
                        "nombre": "Frutos Afectados por Monalonion",
                        "valor": {
                            "$ifNull": ["$Frutos Afectados por Monalonion", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 13
                    },
                    {
                        "nombre": "Numero Hojas Afectadas Mosca Blanca",
                        "valor": {
                            "$ifNull": ["$Numero Hojas Afectadas Mosca Blanca", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 14
                    },
                    {
                        "nombre": "Flores Afectadas por Mosca del Ovario",
                        "valor": {
                            "$ifNull": ["$Flores Afectadas por Mosca del Ovario", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 15
                    },
                    {
                        "nombre": "Pegador",
                        "valor": {
                            "$ifNull": ["$Pegador", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 16
                    },
                    {
                        "nombre": "Incidencia de Picudo",
                        "valor": {
                            "$ifNull": ["$Incidencia de Picudo", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 17
                    },
                    {
                        "nombre": "Evaluacion de Trips en Terminales",
                        "valor": {
                            "$ifNull": ["$Evaluacion de Trips en Terminales", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 18
                    },
                    {
                        "nombre": "Evaluacion de Trips en Flores",
                        "valor": {
                            "$ifNull": ["$Evaluacion de Trips en Flores", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 19
                    },
                    {
                        "nombre": "Pasador de Fruta Stenoma catenifer",
                        "valor": {
                            "$ifNull": ["$Pasador de Fruta Stenoma catenifer", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 20
                    },
                    {
                        "nombre": "Barrenador del Tallo Heilipus elegans",
                        "valor": {
                            "$ifNull": ["$Barrenador del Tallo Heilipus elegans", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 21
                    },
                    {
                        "nombre": "Barrenador de Semilla Heilipus lauri",
                        "valor": {
                            "$ifNull": ["$Barrenador de Semilla Heilipus lauri", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 22
                    },
                    {
                        "nombre": "Barrenador de Semilla Heilipus trifasciatus",
                        "valor": {
                            "$ifNull": ["$Barrenador de Semilla Heilipus trifasciatus", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 23
                    },
                    {
                        "nombre": "Antracnosis en Terminal",
                        "valor": {
                            "$ifNull": ["$Antracnosis en Terminal", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 24
                    },
                    {
                        "nombre": "Antracnosis en Flor",
                        "valor": {
                            "$ifNull": ["$Antracnosis en Flor", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 25
                    },
                    {
                        "nombre": "Chancro Bacteriano",
                        "valor": {
                            "$ifNull": ["$Chancro Bacteriano", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 26
                    },
                    {
                        "nombre": "Declinios",
                        "valor": {
                            "$ifNull": ["$Declinios", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 27
                    },
                    {
                        "nombre": "Muerte Descendente en Ramas Lasiodiplodia",
                        "valor": {
                            "$ifNull": ["$Muerte Descendente en Ramas Lasiodiplodia", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 28
                    },
                    {
                        "nombre": "Mancha Angular Pseudocercospora",
                        "valor": {
                            "$ifNull": ["$Mancha Angular Pseudocercospora", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 29
                    },
                    {
                        "nombre": "Rona",
                        "valor": {
                            "$ifNull": ["$Rona", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 30
                    },
                    {
                        "nombre": "Muerte de Ramas Verticillium",
                        "valor": {
                            "$ifNull": ["$Muerte de Ramas Verticillium", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 31
                    },
                    {
                        "nombre": "Asfixia Radicular",
                        "valor": {
                            "$ifNull": ["$Asfixia Radicular", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 32
                    },
                    {
                        "nombre": "Fruta Bolita",
                        "valor": {
                            "$ifNull": ["$Fruta Bolita", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 33
                    },
                    {
                        "nombre": "Dano por Viento",
                        "valor": {
                            "$ifNull": ["$Dano por Viento", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 34
                    },
                    {
                        "nombre": "Golpe por Frio",
                        "valor": {
                            "$ifNull": ["$Golpe por Frio", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 35
                    },
                    {
                        "nombre": "Golpe de Sol",
                        "valor": {
                            "$ifNull": ["$Golpe de Sol", "sin_valor"]
                        },
                        "tipo": "check",
                        "orden": 36
                    },
                    {
                        "nombre": "Vigor Color de las Hojas",
                        "valor": {
                            "$ifNull": ["$Vigor Color de las Hojas", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 37
                    },
                    {
                        "nombre": "Vigor Tamano de las Hojas",
                        "valor": {
                            "$ifNull": ["$Vigor Tamano de las Hojas", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 38
                    },
                    {
                        "nombre": "Vigor Tamano y Ramas Cuarteadas",
                        "valor": {
                            "$ifNull": ["$Vigor Tamano y Ramas Cuarteadas", "sin_valor"]
                        },
                        "tipo": "rangos",
                        "orden": 39
                    }


                ]
            }
        }




        , {
            "$project": {
                "Incidencia Acaro Blanco": 0,
                "Severidad Acaro Blanco": 0,
                "Incidencia Acaro Rojo": 0,
                "Severidad Acaro Rojo": 0,
                "Incidencia Afido": 0,
                "Severidad Afido": 0,
                "Cantidad de Chizas": 0,
                "Evaluacion de Escama": 0,
                "Incidencia de Escama": 0,
                "Hormiga Arriera": 0,
                "Larvas Defoliadoras": 0,
                "Incidencia de Marceno": 0,
                "Frutos Afectados por Monalonion": 0,
                "Numero Hojas Afectadas Mosca Blanca": 0,
                "Flores Afectadas por Mosca del Ovario": 0,
                "Pegador": 0,
                "Incidencia de Picudo": 0,
                "Evaluacion de Trips en Terminales": 0,
                "Evaluacion de Trips en Flores": 0,
                "Pasador de Fruta Stenoma catenifer": 0,
                "Barrenador del Tallo Heilipus elegans": 0,
                "Barrenador de Semilla Heilipus lauri": 0,
                "Barrenador de Semilla Heilipus trifasciatus": 0,
                "Antracnosis en Terminal": 0,
                "Antracnosis en Flor": 0,
                "Chancro Bacteriano": 0,
                "Declinios": 0,
                "Muerte Descendente en Ramas Lasiodiplodia": 0,
                "Mancha Angular Pseudocercospora": 0,
                "Rona": 0,
                "Muerte de Ramas Verticillium": 0,
                "Asfixia Radicular": 0,
                "Fruta Bolita": 0,
                "Dano por Viento": 0,
                "Golpe por Frio": 0,
                "Golpe de Sol": 0,
                "Vigor Color de las Hojas": 0,
                "Vigor Tamano de las Hojas": 0,
                "Vigor Tamano y Ramas Cuarteadas": 0

            }
        }

        // , { "$unwind": "$array_data" }


        //--tipos de variables
        //check
        //rangos
        //check-rangos




        //--grados
        //grado 0 = 0   = verde
        //grado 1 = 1   = amarillo
        //grado 2 = 2   = naranja
        //grado 3 = >3  = rojo













    ]

)

    .sort({ "_id": -1 })
