//--reset cantidad de productos
db.supplies.updateMany(
    {}
    ,
    {$set:{quantity:0}}
);


//--borrar TODAS las transacciones
db.suppliesTimeline.deleteMany({});


//--query para ingresos
db.suppliesTimeline_new0.aggregate(
    {
        $match:{
            quantity:{$gt:0},
            deleted:false,
            lbid : ""
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    {$unwind:"$productos"}
    ,{
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    {$unwind:"$bodegas"}
    ,{
        "$project": {
            "wid": "$bodegas.name",
            "tax":{
                $cond: {
                    if: {
                        $eq: ["$tax", ""]
                    },
                    then: 0,
                    else: "$tax"
                }
            },
            "name": "$productos.name",
            "type":"Ingresos",
            "quantity": "$productEquivalence",
            "invoiceNumber":"$invoiceNumber",
            "inf":"$information",
            "invoiceDate": {
                $cond: {
                    if: {
                        $eq: ["$invoiceDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$invoiceDate"}}
                }
            },
            "dueDate":{
                $cond: {
                    if: {
                        $eq: ["$dueDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$dueDate"}}
                }
            },
            "inputmeasure": "$inputmeasure",
            "rgDate": {$dateToString: {format: "%Y-%m-%d", date: "$rgDate"}},
            "subtotal":"$subtotal",
            "lbid":"",
            "lbdoce":""
        }
    }
);


//....importar datos en la web


//....cambiar typeSupply=Ingresos de transaccion de ingreso
db.suppliesTimeline.aggregate([
    {
        $match: {
            typeSupply: "Ingresos"
        }
    },
    {
        $project: {
            sid: 1
        }
    },
    {
        $lookup: {
            from: "supplies",
            as: "supply",
            localField: "sid",
            foreignField: "_id"
        }
    },
    {
        $unwind: "$supply"
    },
    {
        $project: {
            supplyType: "$supply.type"
        }
    }
]).forEach(cambio => {
    db.suppliesTimeline.update({_id: cambio._id}, {$set: {typeSupply: cambio.supplyType}})
});










//----otros
//--labores finalizadas que usaron inventario
db.tasks.aggregate(
    {
        $match:{
            status:"Done",
            supplies: {$ne: []}
        }
    }
);


//--editar estado de labores cerradas
db.tasks.updateMany(
    {
        status:"Done"
    },
    {
        $set:{
            status:"Doing"
        }
    }

)


//---reset tablas (por si algo salio mal cerrando las labores)

//--labores
db.tasks_new3_empleados.aggregate({$out:"tasks"});

//transacciones
db.suppliesTimeline_new2_ingresos.aggregate({$out:"suppliesTimeline"});

//productos
db.supplies_new2_ingresos.aggregate({$out:"supplies"});

