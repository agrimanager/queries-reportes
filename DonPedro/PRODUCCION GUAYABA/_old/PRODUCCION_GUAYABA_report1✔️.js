[



    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },




    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_lote": 0

            , "Producto": 0
            , "Precio": 0
        }
    }

    , {
        "$match": {
            "lote": { "$exists": true }
        }
    }

    , {
        "$addFields": {
            "TOTAL Guayaba Primera": {
                "$multiply": [
                    { "$toDouble": "$Guayaba Primera Kg" },
                    { "$toDouble": "$Precio Guayaba Primera" }
                ]
            },
            "TOTAL Guayaba Segunda": {
                "$multiply": [
                    { "$toDouble": "$Guayaba Segunda Kg" },
                    { "$toDouble": "$Precio Guayaba Segunda" }
                ]
            },
            "TOTAL Guayaba Primera Madura": {
                "$multiply": [
                    { "$toDouble": "$Guayaba Primera Madura Kg" },
                    { "$toDouble": "$Precio Guayaba Primera Madura" }
                ]
            },
            "TOTAL Guayaba Industrial": {
                "$multiply": [
                    { "$toDouble": "$Guayaba Industrial Kg" },
                    { "$toDouble": "$Precio Guayaba Industrial" }
                ]
            },
            "TOTAL Guayaba Balin": {
                "$multiply": [
                    { "$toDouble": "$Guayaba Balin Kg" },
                    { "$toDouble": "$Precio Guayaba Balin" }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "TOTAL Kg Cosechados": {
                "$sum": [
                    { "$toDouble": "$Guayaba Primera Kg" },
                    { "$toDouble": "$Guayaba Segunda Kg" },
                    { "$toDouble": "$Guayaba Primera Madura Kg" },
                    { "$toDouble": "$Guayaba Industrial Kg" },
                    { "$toDouble": "$Guayaba Balin Kg" }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "TOTAL costo al contrato": {
                "$ifNull": [
                    {
                        "$multiply": [
                            { "$toDouble": "$Costo por Kilo al contrato" },
                            { "$toDouble": "$Kilos al Contrato" }
                        ]
                    }
                    , -1]
            },
            "Kilos al dia": {
                "$ifNull": [
                    {
                        "$subtract": [
                            { "$toDouble": "$TOTAL Kg Cosechados" },
                            { "$toDouble": "$Kilos al Contrato" }
                        ]
                    }
                    , -1]
            }
        }
    }



    , {
        "$addFields": {
            "Costo Total de la Labor": {
                "$ifNull": ["$Costo Total de la Labor", -1]
            },
            "Total de Jornales": {
                "$ifNull": ["$Total de Jornales", -1]
            },
            "Costo por Kilo al contrato": {
                "$ifNull": ["$Costo por Kilo al contrato", -1]
            },
            "Kilos al Contrato": {
                "$ifNull": ["$Kilos al Contrato", -1]
            }
        }
    }



]