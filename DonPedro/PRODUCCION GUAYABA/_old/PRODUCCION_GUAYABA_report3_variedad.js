db.form_guayabaproduccion.aggregate(
    [

        //----test
        // {
        //     $match: {
        //         "_id": ObjectId("5f4061d6d1de982b8ff2f7b2")
        //     }
        // },


        //---cartografia

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },




        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_lote": 0

                , "Producto": 0
                , "Precio": 0
            }
        }

        , {
            "$match": {
                "lote": { "$exists": true }
            }
        }



        //---estandarizar

        //---1)juntar los datos en array
        , {
            "$addFields": {

                "array_variedad_guayaba": [

                    {
                        "variedad": "PRIMERA",
                        "cantidad": { "$toDouble": "$Guayaba Primera Kg" }
                    },
                    {
                        "variedad": "SEGUNDA",
                        "cantidad": { "$toDouble": "$Guayaba Segunda Kg" }
                    },
                    {
                        "variedad": "PRIMERA MADURA",
                        "cantidad": { "$toDouble": "$Guayaba Primera Madura Kg" }
                    },
                    {
                        "variedad": "INDUSTRIA",
                        "cantidad": { "$toDouble": "$Guayaba Industrial Kg" }
                    },
                    {
                        "variedad": "BALIN",
                        "cantidad": { "$toDouble": "$Guayaba Balin Kg" }
                    }
                    // ,{
                    //     "variedad": "A3",
                    //     "cantidad": 0
                    // }


                ]

            }
        }


        , { "$unwind": "$array_variedad_guayaba" }



        , {
            "$addFields": {
                "variedad": "$array_variedad_guayaba.variedad",
                "cantidad": "$array_variedad_guayaba.cantidad"
            }
        },

        {
            "$project": {
                "array_variedad_guayaba": 0
            }
        }


        //---FECHAS
        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
            }
        }







    ]
)