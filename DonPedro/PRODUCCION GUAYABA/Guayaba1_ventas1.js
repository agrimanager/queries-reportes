db.form_guayabaproduccion.aggregate(
    [

        //=====FECHAS
        //--condicion
        { "$addFields": { "anio_filtro": { "$year": "$Fecha Inicial" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        //.....f1 y f2
        {
            "$addFields": {
                "fecha": "$Fecha Inicial"
            }
        },

        //---ojo con timezone

        {
            "$addFields": {
                "fecha": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" }
                }
            }
        },


        //=====CARTOGRAFIA

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0


                //--otros
                , "Producto": 0
                , "Precio": 0

                , "Lote": 0
                , "Point": 0
                , "uid": 0
            }
        }

        , {
            "$match": {
                "lote": { "$exists": true }
            }
        }


        // //=====PROCESO

        // //---estandarizar---inicializar array x variedad
        , {
            "$addFields": {
                "array_variedad": [

                    {
                        "variedad": "Guayaba 1ra",
                        "cantidad": { "$toDouble": "$Guayaba Primera Kg" },
                        "precio": { "$toDouble": "$Precio Guayaba Primera" }
                    },
                    {
                        "variedad": "Guayaba 2da",
                        "cantidad": { "$toDouble": "$Guayaba Segunda Kg" },
                        "precio": { "$toDouble": "$Precio Guayaba Segunda" }
                    },
                    {
                        "variedad": "Guayaba 1ra Madura",
                        "cantidad": { "$toDouble": "$Guayaba Primera Madura Kg" },
                        "precio": { "$toDouble": "$Precio Guayaba Primera Madura" }
                    },
                    {
                        "variedad": "Guayaba Industrial",
                        "cantidad": { "$toDouble": "$Guayaba Industrial Kg" },
                        "precio": { "$toDouble": "$Precio Guayaba Industrial" }
                    },
                    {
                        "variedad": "Guayaba Balin",
                        "cantidad": { "$toDouble": "$Guayaba Balin Kg" },
                        "precio": { "$toDouble": "$Precio Guayaba Balin" }
                    },
                    {
                        "variedad": "Sin Clasificar",
                        "cantidad": { "$toDouble": "$Sin Clasificar" },
                        "precio": { "$toDouble": "$Precio Sin Clasificar" }
                    }

                ]

            }
        }


        , { "$unwind": "$array_variedad" }

        , {
            "$addFields": {
                "variedad": "$array_variedad.variedad",
                "cantidad": "$array_variedad.cantidad",
                "precio": "$array_variedad.precio"
            }
        }



        , { "$project": { "array_variedad": 0 } }


        //---calculos

        , {
            "$addFields": {
                "valor": {
                    "$multiply": [
                        { "$toDouble": "$cantidad" },
                        { "$toDouble": "$precio" }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "costo_retefuente": {
                    "$cond": {
                        "if": { "$eq": ["$Retefuente", "No Aplica"] },
                        "then": 0,
                        "else": {
                            "$multiply": ["$valor", 0.015]
                        }
                    }
                },
                "costo_asohofrucol": {
                    "$cond": {
                        "if": { "$eq": ["$Asohofrucol", "No Aplica"] },
                        "then": 0,
                        "else": {
                            "$multiply": ["$valor", 0.01]
                        }
                    }
                }

            }
        }


        , {
            "$addFields": {
                "valor_con_descuentos": {
                    "$subtract": ["$valor",
                        { "$sum": ["$costo_retefuente", "$costo_asohofrucol"] }
                    ]
                }
            }
        }


        , {
            "$match": {
                "lote": {
                    "$in": [
                        "1-40A",
                        "1-40G",
                        "1-41",
                        "1-42",
                        "1-43",
                        "1-44G",
                        "1-45G"
                    ]
                }
            }
        }






    ]

)