db.users.aggregate(
    [

        //---test filtro de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //---------------------------------------

        {
            "$lookup": {
                "from": "form_aguacateproduccion",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //----filtro de fechas
                    { "$addFields": { "fecha_filtro": "$Fecha Inicial" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "fecha": "$fecha_filtro"
                        }
                    },

                    //---ojo con timezone

                    {
                        "$addFields": {
                            "fecha": {
                                "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" }
                            }
                        }
                    },


                    //=====CARTOGRAFIA

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "lote": "$lote.properties.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    },
                    { "$unwind": "$Finca" },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0


                            //--otros
                            , "Producto": 0
                            , "Precio": 0

                            , "Lote": 0
                            , "Point": 0
                            , "uid": 0
                        }
                    }

                    , {
                        "$match": {
                            "lote": { "$exists": true }
                        }
                    }


                    //=====PROCESO

                    //---estandarizar---inicializar array x variedad
                    , {
                        "$addFields": {
                            "array_variedad": [

                                //--tipo1
                                {
                                    "tipo": "Papelillo",
                                    "variedad": "Papelillo 1ra",
                                    "cantidad": { "$toDouble": "$Papelillo Primera Kg" },
                                    "precio": { "$toDouble": "$Precio Papelillo Primera" }
                                },
                                {
                                    "tipo": "Papelillo",
                                    "variedad": "Papelillo 2da",
                                    "cantidad": { "$toDouble": "$Papelillo Segunda Kg" },
                                    "precio": { "$toDouble": "$Precio Papelillo Segunda" }
                                },
                                {
                                    "tipo": "Papelillo",
                                    "variedad": "Papelillo 3ra",
                                    "cantidad": { "$toDouble": "$Papelillo Tercera Kg" },
                                    "precio": { "$toDouble": "$Precio Papelillo Tercera" }
                                },

                                // //--tipo2
                                {
                                    "tipo": "Santana",
                                    "variedad": "Santana 1ra",
                                    "cantidad": { "$toDouble": "$Santana Primera Kg" },
                                    "precio": { "$toDouble": "$Precio Santana Primera" }
                                },
                                {
                                    "tipo": "Santana",
                                    "variedad": "Santana 2da",
                                    "cantidad": { "$toDouble": "$Santana Segunda Kg" },
                                    "precio": { "$toDouble": "$Precio Santana Segunda" }
                                },
                                {
                                    "tipo": "Santana",
                                    "variedad": "Santana 3ra",
                                    "cantidad": { "$toDouble": "$Santana Tercera Kg" },
                                    "precio": { "$toDouble": "$Precio Santana Tercera" }
                                }
                            ]

                        }
                    }


                    , { "$unwind": "$array_variedad" }

                    , {
                        "$addFields": {
                            "tipo": "$array_variedad.tipo",
                            "variedad": "$array_variedad.variedad",
                            "cantidad": "$array_variedad.cantidad",
                            "precio": "$array_variedad.precio"
                        }
                    }



                    , { "$project": { "array_variedad": 0 } }


                    //---calculos

                    , {
                        "$addFields": {
                            "valor": {
                                "$multiply": [
                                    { "$toDouble": "$cantidad" },
                                    { "$toDouble": "$precio" }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "costo_retefuente": {
                                "$cond": {
                                    "if": { "$eq": ["$Retefuente", "No Aplica"] },
                                    "then": 0,
                                    "else": {
                                        "$multiply": ["$valor", 0.015]
                                    }
                                }
                            },
                            "costo_asohofrucol": {
                                "$cond": {
                                    "if": { "$eq": ["$Asohofrucol", "No Aplica"] },
                                    "then": 0,
                                    "else": {
                                        "$multiply": ["$valor", 0.01]
                                    }
                                }
                            }

                        }
                    }


                    , {
                        "$addFields": {
                            "valor_con_descuentos": {
                                "$subtract": ["$valor",
                                    { "$sum": ["$costo_retefuente", "$costo_asohofrucol"] }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "Busqueda inicio": "$$filtro_fecha_inicio",
                            "Busqueda fin": "$$filtro_fecha_fin"

                            //anio
                            , "num_anio": { "$year": { "date": "$fecha_filtro" } }
                            //semana       
                            , "num_semana": { "$week": { "date": "$fecha_filtro" } }
                        }
                    }

                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




        //---ojo variable rgDate
        //rgDate
        , {
            "$addFields": {
                "rgDate": "$fecha_filtro"
            }
        }

        //===== SOLO LOTES DE AGUACATE

        , {
            "$match": {
                "lote": {
                    "$in": [
                        "1-53",
                        "1-55",
                        "1-56",
                        "1-57",
                        "1-58",
                        "1-59"
                    ]
                }
            }
        }






        //dato1
        , {
            "$addFields": {
                "valor_recoleccion": {
                    "$multiply": [
                        { "$toDouble": "$cantidad" },
                        { "$toDouble": "$Costo por Kilo" }
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote"

                    , "num_anio": "$num_anio"
                    , "num_semana": "$num_semana"

                    //---para filtro final
                    , "rgDate": "$rgDate"
                }
                , "recoleccion_total_kg": { "$sum": "$cantidad" }
                , "recoleccion_total_valor": { "$sum": "$valor_recoleccion" }

                // , "data": { "$push": "$$ROOT" }
            }
        }


        // //------------LABORES



        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {
                    "filtro_lote": "$_id.lote",
                    "filtro_fecha": "$_id.rgDate"
                },
                "pipeline": [

                    //============================ Condiciones
                    //-----CRUCE LOTE
                    {
                        "$match": {
                            "$expr": {
                                "$in": ["$$filtro_lote", "$cartography.features.properties.name"]
                            }
                        }
                    },


                    //----CRUCE FECHAS

                    {
                        "$addFields": {
                            "Fecha inicio": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    },


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //anio
                                    { "$eq": [{ "$year": { "date": "$$filtro_fecha" } }, { "$year": { "date": "$Fecha inicio" } }] },
                                    //semana
                                    { "$eq": [{ "$week": { "date": "$$filtro_fecha" } }, { "$week": { "date": "$Fecha inicio" } }] }
                                ]
                            }
                        }
                    },

                    //================================================


                    //----reporte de labores (calcular MO e INV)
                    //---------------NEW


                    //=========-MO
                    {
                        "$addFields": {
                            "ProductividadEmpleados": { "$sum": "$productivityReport.quantity" }
                        }
                    },
                    { "$addFields": { "TotalPagoEmpleado": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$ProductividadEmpleados" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] } } }

                    //========= INV
                    //--(si la labor esta cerrada)
                    , {
                        "$lookup": {
                            "from": "suppliesTimeline",
                            "localField": "_id",
                            "foreignField": "lbid",
                            "as": "suppliesTimeline_reference"
                        }
                    }

                    , {
                        "$addFields": {
                            "TotalCostoProducto": {
                                "$reduce": {
                                    "input": "$suppliesTimeline_reference",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": [
                                            "$$value",
                                            {
                                                "$multiply": [
                                                    { "$ifNull": [{ "$toDouble": "$$this.productEquivalence" }, 0] }
                                                    , { "$ifNull": [{ "$toDouble": "$$this.valueByUnity" }, 0] }
                                                ]
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    }


                    //============================ valores a mostrar
                    , {
                        "$addFields": {
                            "labores_valor_mo": "$TotalPagoEmpleado",
                            "labores_valor_inv": "$TotalCostoProducto"
                        }
                    }

                ]

            }
        }



        //----sumar valores de array
        , {
            "$addFields": {

                "labores_total_valor_mo": { "$sum": "$data_labores.labores_valor_mo" }
                , "labores_total_valor_inv": { "$sum": "$data_labores.labores_valor_inv" }
            }
        }



        //datos finales
        , {
            "$addFields": {
                "valor_total_final": {
                    "$sum": [
                        { "$toDouble": "$recoleccion_total_valor" },
                        { "$toDouble": "$labores_total_valor_mo" },
                        { "$toDouble": "$labores_total_valor_inv" }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "costo_x_kilo_final": {
                    "$cond": {
                        "if": { "$eq": ["$recoleccion_total_kg", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$valor_total_final",
                                "$recoleccion_total_kg"]
                        }
                    }
                }

            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "recoleccion_total_kg": "$recoleccion_total_kg",
                            "recoleccion_total_valor": "$recoleccion_total_valor",
                            "labores_total_valor_mo": "$labores_total_valor_mo",
                            "labores_total_valor_inv": "$labores_total_valor_inv",

                            "valor_total_final": "$valor_total_final",
                            "costo_x_kilo_final": "$costo_x_kilo_final"
                        }
                    ]
                }
            }
        }







    ]
)