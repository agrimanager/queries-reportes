[

    {
        "$lookup": {
            "from": "form_aguacateproduccion",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                
                { "$addFields": { "fecha_filtro": "$Fecha Inicial" } },
                { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                { "$match": { "anio_filtro": { "$gt": 2000 } } },
                { "$match": { "anio_filtro": { "$lt": 3000 } } },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_filtro" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "fecha": "$fecha_filtro"
                    }
                },

                

                {
                    "$addFields": {
                        "fecha": {
                            "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" }
                        }
                    }
                },


                

                {
                    "$addFields": {
                        "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_oid",
                                "$features_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                        "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                        "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                    }
                },

                {
                    "$addFields": {
                        "Bloque": "$Bloque.properties.name",
                        "lote": "$lote.properties.name"
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "Finca._id",
                        "foreignField": "_id",
                        "as": "Finca"
                    }
                },

                {
                    "$addFields": {
                        "Finca": "$Finca.name"
                    }
                },
                { "$unwind": "$Finca" },


                {
                    "$project": {
                        "split_path": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "features_oid": 0


                        
                        , "Producto": 0
                        , "Precio": 0

                        , "Lote": 0
                        , "Point": 0
                        , "uid": 0
                    }
                }

                , {
                    "$match": {
                        "lote": { "$exists": true }
                    }
                }

                , {
                    "$addFields": {
                        "array_variedad": [

                            
                            {
                                "tipo": "Papelillo",
                                "variedad": "Papelillo 1ra",
                                "cantidad": { "$toDouble": "$Papelillo Primera Kg" },
                                "precio": { "$toDouble": "$Precio Papelillo Primera" }
                            },
                            {
                                "tipo": "Papelillo",
                                "variedad": "Papelillo 2da",
                                "cantidad": { "$toDouble": "$Papelillo Segunda Kg" },
                                "precio": { "$toDouble": "$Precio Papelillo Segunda" }
                            },
                            {
                                "tipo": "Papelillo",
                                "variedad": "Papelillo 3ra",
                                "cantidad": { "$toDouble": "$Papelillo Tercera Kg" },
                                "precio": { "$toDouble": "$Precio Papelillo Tercera" }
                            },

                            
                            {
                                "tipo": "Santana",
                                "variedad": "Santana 1ra",
                                "cantidad": { "$toDouble": "$Santana Primera Kg" },
                                "precio": { "$toDouble": "$Precio Santana Primera" }
                            },
                            {
                                "tipo": "Santana",
                                "variedad": "Santana 2da",
                                "cantidad": { "$toDouble": "$Santana Segunda Kg" },
                                "precio": { "$toDouble": "$Precio Santana Segunda" }
                            },
                            {
                                "tipo": "Santana",
                                "variedad": "Santana 3ra",
                                "cantidad": { "$toDouble": "$Santana Tercera Kg" },
                                "precio": { "$toDouble": "$Precio Santana Tercera" }
                            }
                        ]

                    }
                }


                , { "$unwind": "$array_variedad" }

                , {
                    "$addFields": {
                        "tipo": "$array_variedad.tipo",
                        "variedad": "$array_variedad.variedad",
                        "cantidad": "$array_variedad.cantidad",
                        "precio": "$array_variedad.precio"
                    }
                }



                , { "$project": { "array_variedad": 0 } }


                

                , {
                    "$addFields": {
                        "valor": {
                            "$multiply": [
                                { "$toDouble": "$cantidad" },
                                { "$toDouble": "$precio" }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "costo_retefuente": {
                            "$cond": {
                                "if": { "$eq": ["$Retefuente", "No Aplica"] },
                                "then": 0,
                                "else": {
                                    "$multiply": ["$valor", 0.015]
                                }
                            }
                        },
                        "costo_asohofrucol": {
                            "$cond": {
                                "if": { "$eq": ["$Asohofrucol", "No Aplica"] },
                                "then": 0,
                                "else": {
                                    "$multiply": ["$valor", 0.01]
                                }
                            }
                        }

                    }
                }


                , {
                    "$addFields": {
                        "valor_con_descuentos": {
                            "$subtract": ["$valor",
                                { "$sum": ["$costo_retefuente", "$costo_asohofrucol"] }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "Busqueda inicio": "$$filtro_fecha_inicio",
                        "Busqueda fin": "$$filtro_fecha_fin"

                        
                        , "num_anio": { "$year": { "date": "$fecha_filtro" } }
                        
                        , "num_semana": { "$week": { "date": "$fecha_filtro" } }
                    }
                }

            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }




    , {
        "$addFields": {
            "rgDate": "$fecha_filtro"
        }
    }


    
    , {
        "$addFields": {
            "valor_recoleccion": {
                "$multiply": [
                    { "$toDouble": "$cantidad" },
                    { "$toDouble": "$Costo por Kilo" }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$lote"

                , "num_anio": "$num_anio"
                , "num_semana": "$num_semana"

                
                , "rgDate": "$rgDate"
            }
            , "recoleccion_total_kg": { "$sum": "$cantidad" }
            , "recoleccion_total_valor": { "$sum": "$valor_recoleccion" }

            , "data": { "$push": "$$ROOT" }
        }
    }



    , {
        "$lookup": {
            "from": "tasks",
            "as": "data_labores",
            "let": {
                "filtro_lote": "$_id.lote",
                "filtro_fecha": "$_id.rgDate"
            },
            "pipeline": [
             

                {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                }, {
                    "$unwind": "$activity"
                }, {
                    "$lookup": {
                        "from": "costsCenters",
                        "localField": "ccid",
                        "foreignField": "_id",
                        "as": "costsCenter"
                    }
                }, {
                    "$unwind": "$costsCenter"
                }, {
                    "$lookup": {
                        "from": "farms",
                        "localField": "farm",
                        "foreignField": "_id",
                        "as": "farm"
                    }
                }, {
                    "$unwind": "$farm"
                }, {
                    "$lookup": {
                        "from": "employees",
                        "localField": "supervisor",
                        "foreignField": "_id",
                        "as": "supervisor"
                    }
                }, {
                    "$unwind": "$supervisor"
                }

                
                , {
                    "$match": {
                        "employees": { "$ne": "" }
                    }
                }

                , {
                    "$addFields": {
                        "employees": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$type": "$employees"
                                    }, "array"]
                                },
                                "then": "$employees",
                                "else": {
                                    "$map": {
                                        "input": {
                                            "$objectToArray": "$employees"
                                        },
                                        "as": "employeeKV",
                                        "in": "$$employeeKV.v"
                                    }
                                }
                            }
                        },
                        "productivityReport": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$type": "$productivityReport"
                                    }, "array"]
                                },
                                "then": "$productivityReport",
                                "else": {
                                    "$map": {
                                        "input": {
                                            "$objectToArray": "$productivityReport"
                                        },
                                        "as": "productivityReportKV",
                                        "in": "$$productivityReportKV.v"
                                    }
                                }
                            }
                        },
                        "lots": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$type": "$lots"
                                    }, "array"]
                                },
                                "then": "$lots",
                                "else": {
                                    "$map": {
                                        "input": {
                                            "$objectToArray": "$lots"
                                        },
                                        "as": "lotKV",
                                        "in": "$$lotKV.v"
                                    }
                                }
                            }
                        }
                    }
                }, {
                    "$addFields": {
                        "total_empleados_seleccionados_labor": {
                            "$size": "$employees"
                        }
                    }
                }, {
                    "$addFields": {
                        "employees": {
                            "$map": {
                                "input": "$employees",
                                "as": "strid",
                                "in": {
                                    "$toObjectId": "$$strid"
                                }
                            }
                        }
                    }
                }, {
                    "$lookup": {
                        "from": "employees",
                        "localField": "employees",
                        "foreignField": "_id",
                        "as": "Empleados"
                    }
                }, {
                    "$addFields": {
                        "Empleados_fullNames": {
                            "$map": {
                                "input": "$Empleados",
                                "as": "empleado",
                                "in": {
                                    "$concat": ["$$empleado.firstName", " ", "$$empleado.lastName"]
                                }
                            }
                        }
                    }
                }, {
                    "$addFields": {
                        "productos": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$type": "$supplies"
                                    }, "array"]
                                },
                                "then": "$supplies",
                                "else": {
                                    "$map": {
                                        "input": {
                                            "$objectToArray": "$employees"
                                        },
                                        "as": "suppliesKV",
                                        "in": "$$suppliesKV.v"
                                    }
                                }
                            }
                        }
                    }
                }, {
                    "$addFields": {
                        "productos": {
                            "$map": {
                                "input": "$productos._id",
                                "as": "strid",
                                "in": {
                                    "$toObjectId": "$$strid"
                                }
                            }
                        }
                    }
                }, {
                    "$lookup": {
                        "from": "supplies",
                        "localField": "productos",
                        "foreignField": "_id",
                        "as": "productos"
                    }
                }, {
                    "$addFields": {
                        "productos": {
                            "$reduce": {
                                "input": "$productos.name",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$strLenCP": "$$value"
                                            }, 0]
                                        },
                                        "then": "$$this",
                                        "else": {
                                            "$concat": ["$$value", "; ", "$$this"]
                                        }
                                    }
                                }
                            }
                        },
                        "Empleados": {
                            "$reduce": {
                                "input": "$Empleados_fullNames",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$strLenCP": "$$value"
                                            }, 0]
                                        },
                                        "then": "$$this",
                                        "else": {
                                            "$concat": ["$$value", "; ", "$$this"]
                                        }
                                    }
                                }
                            }
                        },
                        "Etiquetas": {
                            "$ifNull": [{
                                "$reduce": {
                                    "input": "$tags",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": [{
                                                    "$strLenCP": "$$value"
                                                }, 0]
                                            },
                                            "then": "$$this",
                                            "else": {
                                                "$concat": ["$$value", "; ", "$$this"]
                                            }
                                        }
                                    }
                                }
                            }, "--sin etiquetas--"]
                        },
                        "Blancos_biologicos": {
                            "$reduce": {
                                "input": {
                                    "$cond": {
                                        "if": { "$eq": ["$biologicalTarget", ""] },
                                        "then": null,
                                        "else": "$biologicalTarget"
                                    }
                                },
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$strLenCP": "$$value"
                                            }, 0]
                                        },
                                        "then": "$$this",
                                        "else": {
                                            "$concat": ["$$value", "; ", "$$this"]
                                        }
                                    }
                                }
                            }
                        },

                        "Cartografia_seleccionada_mapa_elementos": {
                            "$reduce": {
                                "input": "$cartography.features.properties.name",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$strLenCP": "$$value"
                                            }, 0]
                                        },
                                        "then": "$$this",
                                        "else": {
                                            "$concat": ["$$value", "; ", "$$this"]
                                        }
                                    }
                                }
                            }
                        },
                        "Cartografia_seleccionada_lista_lotes_y_estados": {
                            "$ifNull": [{
                                "$reduce": {
                                    "input": "$lots",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": [{
                                                    "$strLenCP": "$$value"
                                                }, 0]
                                            },
                                            "then": "$$this",
                                            "else": {
                                                "$concat": ["$$value", "; ", "$$this"]
                                            }
                                        }
                                    }
                                }
                            }, "--sin lotes seleccionados--"]
                        },
                        "lista_lots": {
                            "$map": {
                                "input": "$lots",
                                "as": "lot",
                                "in": {
                                    "$split": ["$$lot", ", "]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "lista_lotes": {
                            "$map": {
                                "input": "$lista_lots",
                                "as": "lot",
                                "in": {
                                    "$arrayElemAt": ["$$lot", 0]
                                }
                            }
                        },
                        "lista_estados": {
                            "$map": {
                                "input": "$lista_lots",
                                "as": "lot",
                                "in": {
                                    "$arrayElemAt": ["$$lot", 1]
                                }
                            }
                        }
                    }
                }, {
                    "$addFields": {
                        "lista_lotes": {
                            "$ifNull": [{
                                "$reduce": {
                                    "input": "$lista_lotes",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": [{
                                                    "$strLenCP": "$$value"
                                                }, 0]
                                            },
                                            "then": "$$this",
                                            "else": {
                                                "$concat": ["$$value", "; ", "$$this"]
                                            }
                                        }
                                    }
                                }
                            }, "--sin lotes seleccionados--"]
                        },
                        "lista_estados": {
                            "$ifNull": [{
                                "$reduce": {
                                    "input": "$lista_estados",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": [{
                                                    "$strLenCP": "$$value"
                                                }, 0]
                                            },
                                            "then": "$$this",
                                            "else": {
                                                "$concat": ["$$value", "; ", "$$this"]
                                            }
                                        }
                                    }
                                }
                            }, "--sin estados seleccionados--"]
                        }
                    }
                }, {
                    "$addFields": {
                        "TotalPagoEmpleado": {
                            "$multiply": [{
                                "$ifNull": [{
                                    "$toDouble": "$productivityAchieved"
                                }, 0]
                            }, {
                                "$ifNull": [{
                                    "$toDouble": "$productivityPrice.price"
                                }, 0]
                            }]
                        }
                    }
                }, {
                    "$addFields": {
                        "TOTAL LABOR": {
                            "$sum": [{
                                "$ifNull": [{
                                    "$toDouble": "$TotalPagoEmpleado"
                                }, 0]
                            }, {
                                "$ifNull": [{
                                    "$toDouble": "$totalSupplies"
                                }, 0]
                            }]
                        }
                    }
                }
                , {
                    "$project": {
                        "_id": 0,
                        "Actividad": "$activity.name",
                        "Codigo Labor": "$cod",
                        "Estado Labor": {
                            "$switch": {
                                "branches": [{
                                    "case": {
                                        "$eq": ["$status", "To do"]
                                    },
                                    "then": "⏰ Por hacer"
                                }, {
                                    "case": {
                                        "$eq": ["$status", "Doing"]
                                    },
                                    "then": "💪 En progreso"
                                }, {
                                    "case": {
                                        "$eq": ["$status", "Done"]
                                    },
                                    "then": "✔ Listo"
                                }]
                            }
                        },
                        "Centro de costos": "$costsCenter.name",
                        "Finca": "$farm.name",
                        "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                        "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                        "Tipo cultivo labor": {
                            "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"]
                        },
                        "Semana": {
                            "$week": {
                                "$min": "$when.start"
                            }
                        },

                        "Fecha inicio": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] },
                        "Fecha fin": { "$arrayElemAt": ["$when.finish", { "$subtract": [{ "$size": "$when.finish" }, 1] }] },

                        "Supervisor": {
                            "$ifNull": [{
                                "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"]
                            }, "--sin supervisor--"]
                        },
                        "num Productividad esperada de Labor": "$productivityPrice.expectedValue",
                        "($) Precio x unidad de Actividad": "$productivityPrice.price",
                        
                        "(#)Empleados en labor": {
                            "$size": "$employees"
                        },
                        "(#)Total productividad Empleados": {
                            "$ifNull": [{
                                "$toDouble": "$productivityAchieved"
                            }, 0]
                        },
                        "($)Total Pago Empleados": {
                            "$divide": [{
                                "$subtract": [{
                                    "$multiply": ["$TotalPagoEmpleado", 100]
                                }, {
                                    "$mod": [{
                                        "$multiply": ["$TotalPagoEmpleado", 100]
                                    }, 1]
                                }]
                            }, 100]
                        },
                        "Empleados asignados": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$Empleados", ""]
                                },
                                "then": "--sin empleados--",
                                "else": "$Empleados"
                            }
                        },
                        "(#)Productos en labor": {
                            "$size": "$supplies"
                        },
                        "($)Total Productos": {
                            "$divide": [{
                                "$subtract": [{
                                    "$multiply": ["$totalSupplies", 100]
                                }, {
                                    "$mod": [{
                                        "$multiply": ["$totalSupplies", 100]
                                    }, 1]
                                }]
                            }, 100]
                        },
                        "Productos asignados": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$productos", ""]
                                },
                                "then": "--sin productos--",
                                "else": "$productos"
                            }
                        },
                        "TOTAL LABOR": {
                            "$divide": [{
                                "$subtract": [{
                                    "$multiply": ["$TOTAL LABOR", 100]
                                }, {
                                    "$mod": [{
                                        "$multiply": ["$TOTAL LABOR", 100]
                                    }, 1]
                                }]
                            }, 100]
                        },
                        "Observaciones": "$observation",
                        "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                        "Lista lotes": "$lista_lotes",
                        "Lista estados": "$lista_estados",
                        "Blancos biologicos de labor": {
                            "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"]
                        },
                        "Etiquetas": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$Etiquetas", ""]
                                },
                                "then": "--sin etiquetas--",
                                "else": "$Etiquetas"
                            }
                        }
                    }
                }



                , {
                    "$match": {
                        "$expr": {
                            "$eq": ["$elementos Cartografia", "$$filtro_lote"]
                        }
                    }
                }

                , {
                    "$match": {
                        "$expr": {
                            "$and": [
                                
                                { "$eq": [{ "$year": { "date": "$$filtro_fecha" } }, { "$year": { "date": "$Fecha inicio" } }] },
                                
                                { "$eq": [{ "$week": { "date": "$$filtro_fecha" } }, { "$week": { "date": "$Fecha inicio" } }] }
                            ]
                        }
                    }
                }


                
                , {
                    "$addFields": {
                        "labores_valor_mo": "$($)Total Pago Empleados",
                        "labores_valor_inv": "$($)Total Productos"
                    }
                }

            ]

        }
    }



    
    , {
        "$addFields": {

            "labores_total_valor_mo": { "$sum": "$data_labores.labores_valor_mo" }
            , "labores_total_valor_inv": { "$sum": "$data_labores.labores_valor_inv" }
        }
    }



    
    , {
        "$addFields": {
            "valor_total_final": {
                "$sum": [
                    { "$toDouble": "$recoleccion_total_valor" },
                    { "$toDouble": "$labores_total_valor_mo" },
                    { "$toDouble": "$labores_total_valor_inv" }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "costo_x_kilo_final": {
                "$cond": {
                    "if": { "$eq": ["$recoleccion_total_kg", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$valor_total_final",
                            "$recoleccion_total_kg"]
                    }
                }
            }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "recoleccion_total_kg": "$recoleccion_total_kg",
                        "recoleccion_total_valor": "$recoleccion_total_valor",
                        "labores_total_valor_mo": "$labores_total_valor_mo",
                        "labores_total_valor_inv": "$labores_total_valor_inv",

                        "valor_total_final": "$valor_total_final",
                        "costo_x_kilo_final": "$costo_x_kilo_final"
                    }
                ]
            }
        }
    }


]