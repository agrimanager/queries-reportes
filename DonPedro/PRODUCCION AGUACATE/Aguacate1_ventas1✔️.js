[

        { "$addFields": { "anio_filtro": { "$year": "$Fecha Inicial" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        {
            "$addFields": {
                "fecha": "$Fecha Inicial"
            }
        },

        {
            "$addFields": {
                "fecha": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" }
                }
            }
        },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0


            
            , "Producto": 0
            , "Precio": 0

            , "Lote": 0
            , "Point": 0
            , "uid": 0
        }
    }

    , {
        "$match": {
            "lote": { "$exists": true }
        }
    }



    , {
        "$addFields": {
            "array_variedad": [

                
                {
                    "tipo": "Papelillo",
                    "variedad": "Papelillo 1ra",
                    "cantidad": { "$toDouble": "$Papelillo Primera Kg" },
                    "precio": { "$toDouble": "$Precio Papelillo Primera" }
                },
                {
                    "tipo": "Papelillo",
                    "variedad": "Papelillo 2da",
                    "cantidad": { "$toDouble": "$Papelillo Segunda Kg" },
                    "precio": { "$toDouble": "$Precio Papelillo Segunda" }
                },
                {
                    "tipo": "Papelillo",
                    "variedad": "Papelillo 3ra",
                    "cantidad": { "$toDouble": "$Papelillo Tercera Kg" },
                    "precio": { "$toDouble": "$Precio Papelillo Tercera" }
                },

                
                {
                    "tipo": "Santana",
                    "variedad": "Santana 1ra",
                    "cantidad": { "$toDouble": "$Santana Primera Kg" },
                    "precio": { "$toDouble": "$Precio Santana Primera" }
                },
                {
                    "tipo": "Santana",
                    "variedad": "Santana 2da",
                    "cantidad": { "$toDouble": "$Santana Segunda Kg" },
                    "precio": { "$toDouble": "$Precio Santana Segunda" }
                },
                {
                    "tipo": "Santana",
                    "variedad": "Santana 3ra",
                    "cantidad": { "$toDouble": "$Santana Tercera Kg" },
                    "precio": { "$toDouble": "$Precio Santana Tercera" }
                }
            ]

        }
    }


    , { "$unwind": "$array_variedad" }

    , {
        "$addFields": {
            "tipo": "$array_variedad.tipo",
            "variedad": "$array_variedad.variedad",
            "cantidad": "$array_variedad.cantidad",
            "precio": "$array_variedad.precio"
        }
    }



    , { "$project": { "array_variedad": 0 } }




    , {
        "$addFields": {
            "valor": {
                "$multiply": [
                    { "$toDouble": "$cantidad" },
                    { "$toDouble": "$precio" }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "costo_retefuente": {
                "$cond": {
                    "if": { "$eq": ["$Retefuente", "No Aplica"] },
                    "then": 0,
                    "else": {
                        "$multiply": ["$valor", 0.015]
                    }
                }
            },
            "costo_asohofrucol": {
                "$cond": {
                    "if": { "$eq": ["$Asohofrucol", "No Aplica"] },
                    "then": 0,
                    "else": {
                        "$multiply": ["$valor", 0.01]
                    }
                }
            }

        }
    }


    , {
        "$addFields": {
            "valor_con_descuentos": {
                "$subtract": ["$valor",
                    { "$sum": ["$costo_retefuente", "$costo_asohofrucol"] }
                ]
            }
        }
    }


     , {
        "$match": {
            "lote": {
                "$in": [
                    "1-53",
                    "1-55",
                    "1-56",
                    "1-57",
                    "1-58",
                    "1-59"
                ]
            }
        }
    }



]