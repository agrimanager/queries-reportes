db.form_aguacateproduccion.aggregate(
    [

        //---cartografia

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },




        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_lote": 0
            }
        }

        , {
            "$match": {
                "lote": { "$exists": true }
            }
        }

        /*
        // "Papelillo Primera Kg" : "66",
        // "Precio Papelillo Primera" : "66",
        
    //  "Papelillo Segunda Kg" : "66",
    //  "Precio Papelillo Segunda" : "66",
        
    //  "Papelillo Tercera Kg" : "66",
    //  "Precio Papelillo Tercera" : "66",
        
    //  "Santana Primera Kg" : "66",
    //  "Precio Santana Primera" : "66",
        
    //  "Santana Segunda Kg" : "66",
    //  "Precio Santana Segunda" : "66",
        
    //  "Santana Tercera Kg" : "66",
    //  "Precio Santana Tercera" : "66",
        */


        //---CALCULAR total precios por tipo (cantidad_tipo * precio_tipo)
        , {
            "$addFields": {
                "TOTAL Papelillo Primera": {
                    "$multiply": [
                        { "$toDouble": "$Papelillo Primera Kg" },
                        { "$toDouble": "$Precio Papelillo Primera" }
                    ]
                },
                "TOTAL Papelillo Segunda": {
                    "$multiply": [
                        { "$toDouble": "$Papelillo Segunda Kg" },
                        { "$toDouble": "$Precio Papelillo Segunda" }
                    ]
                },
                "TOTAL Papelillo Tercera": {
                    "$multiply": [
                        { "$toDouble": "$Papelillo Tercera Kg" },
                        { "$toDouble": "$Precio Papelillo Tercera" }
                    ]
                },
                "TOTAL Santana Primera": {
                    "$multiply": [
                        { "$toDouble": "$Santana Primera Kg" },
                        { "$toDouble": "$Precio Santana Primera" }
                    ]
                },
                "TOTAL Santana Segunda": {
                    "$multiply": [
                        { "$toDouble": "$Santana Segunda Kg" },
                        { "$toDouble": "$Precio Santana Segunda" }
                    ]
                },
                "TOTAL Santana Tercera": {
                    "$multiply": [
                        { "$toDouble": "$Santana Tercera Kg" },
                        { "$toDouble": "$Precio Santana Tercera" }
                    ]
                }
            }
        }

        //---CALCULAR total cantidad 
        , {
            "$addFields": {
                "TOTAL Kg Cosechados": {
                    "$sum": [
                        { "$toDouble": "$Papelillo Primera Kg" },
                        { "$toDouble": "$Papelillo Segunda Kg" },
                        { "$toDouble": "$Papelillo Tercera Kg" },

                        { "toDouble": "$Santana Primera Kg" },
                        { "$toDouble": "$Santana Segunda Kg" },
                        { "$toDouble": "$Santana Tercera Kg" },
                    ]
                }
            }
        }






    ]

)