//mapa
db.form_censodeproduccion.aggregate(
    [


        {
            "$addFields": { "Cartography": "$Arbol" }
        },

        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },


        {
            "$addFields": {
                "split_path": {
                    "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","]
                }
            }
        },

        {
            "$addFields": {
                "split_path_o_id": {
                    "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } }
                }
            }
        },

        {
            "$addFields": {
                "palma_o_id": {
                    "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } }
                }
            }
        },

        {
            "$addFields": {
                "split_path_o_id": {
                    "$concatArrays": [
                        "$split_path_o_id",
                        "$palma_o_id"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_o_id",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name",
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name",
                "resiembra": "$Planta.properties.custom.Resiembra.value"
            }
        },

        { "$unwind": "$Finca" },

        {
            "$project": {
                "split_path": 0,
                "split_path_o_id": 0,
                "palma_o_id": 0,
                "objetos_del_cultivo": 0,
                "Arbol": 0,
                "Point": 0
            }
        },

        {
            "$addFields": {
                "dato_mapa": [
                    { "racimos": "$Racimos" },
                    { "chombas": "$Chombas" },
                    { "flores_femeninas": "$Inflorescencias femeninas polinizadas" },
                    { "flores_masculinas": "$Inflorescencias Masculinas" },
                    { "improductivas": "$Improductivas" },
                    { "resiembra": "$resiembra" }
                ]
            }
        },

        { "$unwind": "$dato_mapa" },

        {
            "$match": {
                "dato_mapa": { "$ne": {} }
            }
        },





        {
            "$addFields": {
                "rango racimos": {
                    "$switch": {
                        "branches": [

                            {
                                "case": { "$gte": ["$dato_mapa.racimos", 0] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$Racimos", 0] },
                                                "then": "0"
                                            },
                                            {
                                                "case": { "$in": ["$Racimos", [1, 2]] },
                                                "then": "1 - 2"
                                            },
                                            {
                                                "case": { "$in": ["$Racimos", [3, 4]] },
                                                "then": "3 - 4"
                                            },
                                            {
                                                "case": { "$in": ["$Racimos", [5, 6]] },
                                                "then": "5 - 6"
                                            },
                                            {
                                                "case": { "$in": ["$Racimos", [7, 8]] },
                                                "then": "7 - 8"
                                            },
                                            {
                                                "case": { "$in": ["$Racimos", [9, 10]] },
                                                "then": "9 - 10"
                                            },
                                            {
                                                "case": {
                                                    "$gt": ["$Racimos", 10]
                                                },
                                                "then": "> 10"
                                            }
                                        ],
                                        "default": ""
                                    }
                                }
                            }
                        ],
                        "default": "-"
                    }
                },


                "rango flores femeninas": {
                    "$switch": {
                        "branches": [

                            {
                                "case": { "$gte": ["$dato_mapa.flores_femeninas", 0] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$Inflorescencias femeninas polinizadas", 0] },
                                                "then": "0"
                                            },
                                            {
                                                "case": { "$in": ["$Inflorescencias femeninas polinizadas", [1, 2]] },
                                                "then": "1 - 2"
                                            },
                                            {
                                                "case": { "$in": ["$Inflorescencias femeninas polinizadas", [3, 4]] },
                                                "then": "3 - 4"
                                            }
                                        ],
                                        "default": "> 4"
                                    }
                                }
                            }

                        ],
                        "default": "-"
                    }
                },


                "rango flores masculinas": {
                    "$switch": {
                        "branches": [

                            {
                                "case": { "$gte": ["$dato_mapa.flores_masculinas", 0] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$Inflorescencias Masculinas", 0] },
                                                "then": "0"
                                            },
                                            {
                                                "case": { "$in": ["$Inflorescencias Masculinas", [1, 2]] },
                                                "then": "1 - 2"
                                            },
                                            {
                                                "case": { "$in": ["$Inflorescencias Masculinas", [3, 4]] },
                                                "then": "3 - 4"
                                            }
                                        ],
                                        "default": "> 4"
                                    }
                                }
                            }

                        ],
                        "default": "-"
                    }
                },


                "rango chombas": {
                    "$switch": {
                        "branches": [

                            {
                                "case": { "$gte": ["$dato_mapa.chombas", 0] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$Chombas", 0] },
                                                "then": "0"
                                            },
                                            {
                                                "case": { "$in": ["$Chombas", [1, 2]] },
                                                "then": "1 - 2"
                                            },
                                            {
                                                "case": { "$in": ["$Chombas", [3, 4]] },
                                                "then": "3 - 4"
                                            }
                                        ],
                                        "default": "> 4"
                                    }
                                }
                            }

                        ],
                        "default": "-"
                    }
                },


                "improductivas": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$gt": ["$dato_mapa.improductivas", 0] },
                                "then": "improductivas"
                            },
                            {
                                "case": { "$eq": ["$dato_mapa.improductivas", 0] },
                                "then": false
                            }
                        ],
                        "default": "-"
                    }
                },


                "resiembra": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$dato_mapa.resiembra", true] },
                                "then": "resiembra"
                            },
                            {
                                "case": { "$eq": ["$dato_mapa.resiembra", false] },
                                "then": false
                            }
                        ],
                        "default": "-"
                    }
                }

            }
        },



        {
            "$match": {
                "improductivas": { "$ne": false },
                "resiembra": { "$ne": false }
            }
        },



        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [

                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$rango racimos", "0"] },
                                        { "$eq": ["$rango flores masculinas", "0"] },
                                        { "$eq": ["$rango chombas", "0"] }
                                    ]
                                },
                                "then": "#FF0000"
                            },

                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$rango racimos", "1 - 2"] },
                                        { "$eq": ["$rango flores masculinas", "1 - 2"] },
                                        { "$eq": ["$rango chombas", "1 - 2"] }
                                    ]
                                },
                                "then": "#98ff98"
                            },

                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$rango racimos", "3 - 4"] },
                                        { "$eq": ["$rango flores masculinas", "3 - 4"] },
                                        { "$eq": ["$rango chombas", "3 - 4"] }
                                    ]
                                },
                                "then": "#ffff00"
                            },

                            {
                                "case": { "$eq": ["$rango racimos", "5 - 6"] },
                                "then": "#359144"
                            },
                            {
                                "case": { "$eq": ["$rango racimos", "7 - 8"] },
                                "then": "#804000"
                            },
                            {
                                "case": { "$eq": ["$rango racimos", "9 - 10"] },
                                "then": "#ff8000"
                            },
                            {
                                "case": { "$eq": ["$rango racimos", "> 10"] },
                                "then": "#0833a2"
                            },



                            {
                                "case": { "$eq": ["$rango flores femeninas", "0"] },
                                "then": "#51d1f6"
                            },
                            {
                                "case": { "$eq": ["$rango flores femeninas", "1 - 2"] },
                                "then": "#cdcdcd"
                            },
                            {
                                "case": { "$eq": ["$rango flores femeninas", "3 - 4"] },
                                "then": "#ffc0cb"
                            },



                            {
                                "case": { "$eq": ["$improductivas", "improductivas"] },
                                "then": "#572364"
                            },



                            {
                                "case": { "$eq": ["$resiembra", "resiembra"] },
                                "then": "#ffc0cb"
                            }
                        ],
                        "default": ""
                    }
                }
            }
        },




        {
            "$match": {
                "color": { "$ne": "" }
            }
        },




        {
            "$project": {
                "_id": "$elemnq",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "Bloque": "$Bloque",
                    "Lote": "$Lote",
                    "rango racimos": "$rango racimos",
                    "rango flores femeninas": "$rango flores femeninas",
                    "rango flores masculinas": "$rango flores masculinas",
                    "rango chombas": "$rango chombas",
                    "improductivas": "$improductivas",
                    "resiembra": "$resiembra",

                    "color": "$color"
                },
                "geometry": "$Cartography.features.geometry"
            }
        }



    ]
)
