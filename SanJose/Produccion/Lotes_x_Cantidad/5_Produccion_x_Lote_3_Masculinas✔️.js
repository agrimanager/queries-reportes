[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Arbol": 0
            , "Point": 0
            , "uid": 0
            , "Formula": 0

            , "Observaciones": 0
            , "Otros": 0
            , "supervisor": 0
            , "uDate": 0
            , "capture": 0
        }
    }








    , {
        "$addFields": {


          "variable_produccion_cantidad": { "$toDouble": { "$ifNull": ["$Inflorescencias Masculinas", 0] } }
          ,"variable_produccion_nombre": "MASCULINAS"

        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$lote"
                , "variable_produccion_cantidad": "$variable_produccion_cantidad"
                , "variable_produccion_nombre": "$variable_produccion_nombre"
            }
            , "cantidad_lote_variable": { "$sum": 1 }

        }
    },



    {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            }
            , "sum_cantidad_lote": { "$sum": "$cantidad_lote_variable" }
            , "acum_cantidad_lote": {
                "$sum": {
                    "$multiply": ["$cantidad_lote_variable", "$_id.variable_produccion_cantidad"]
                }

            }
            , "data": { "$push": "$$ROOT" }
        }
    }



    , {
        "$addFields": {
            "cantidad_variable_x_cantidad": {
                "$cond": {
                    "if": { "$eq": ["$sum_cantidad_lote", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$acum_cantidad_lote",
                            "$sum_cantidad_lote"]
                    }
                }
            }
        }
    }


    , { "$unwind": "$data" }

    , {
        "$addFields": {
            "pct_lote_variable": {
                "$cond": {
                    "if": { "$eq": ["$sum_cantidad_lote", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$data.cantidad_lote_variable",
                                    "$sum_cantidad_lote"]
                            }
                            , 100]
                    }
                }
            }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    {
                        "opcion": "$data._id.variable_produccion_nombre"
                    },

                    "$_id",
                    {

                        "Num Masculinas": "$data._id.variable_produccion_cantidad"
                        , "Num Palmas": "$data.cantidad_lote_variable"

                        , "Total Palmas": "$sum_cantidad_lote"

                        , "Total Masculinas": "$acum_cantidad_lote"

                        , "Promedio Masculinas X Palma": { "$divide": [{ "$subtract": [{ "$multiply": ["$cantidad_variable_x_cantidad", 100] }, { "$mod": [{ "$multiply": ["$cantidad_variable_x_cantidad", 100] }, 1] }] }, 100] }

                        , "Porcentaje Masculinas X Palma": "$pct_lote_variable"


                    }
                ]
            }
        }
    }


]
