[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "resiembra": { "$ifNull": ["$arbol.properties.custom.Resiembra.value", false] } } },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {

            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Arbol": 0
            , "Point": 0
            , "uid": 0
            , "Formula": 0


        }
    }


    , {
        "$group": {
            "_id": {


                "bloque": "$bloque"
                , "lote": "$lote"
                , "arbol": "$arbol"


                , "resiembra": "$resiembra"


                , "idform": "$idform"
                , "elemnq": "$variable_cartografia.features._id"
                , "geometry": "$variable_cartografia.features.geometry"

            }


            , "femeninas": { "$sum": { "$toDouble": { "$ifNull": ["$Inflorescencias femeninas polinizadas", 0] } } }

            , "masculinas": { "$sum": { "$toDouble": { "$ifNull": ["$Inflorescencias Masculinas", 0] } } }

            , "racimos": { "$sum": { "$toDouble": { "$ifNull": ["$Racimos", 0] } } }

            , "chombas": { "$sum": { "$toDouble": { "$ifNull": ["$Chombas", 0] } } }

            , "improductivas": { "$sum": { "$toDouble": { "$ifNull": ["$Improductivas", 0] } } }



        }
    },


    {
        "$addFields": {
            "rango racimos": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$gte": ["$racimos", 0] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$racimos", 0] },
                                            "then": "0"
                                        },
                                        {
                                            "case": { "$in": ["$racimos", [1, 2]] },
                                            "then": "1 - 2"
                                        },
                                        {
                                            "case": { "$in": ["$racimos", [3, 4]] },
                                            "then": "3 - 4"
                                        },
                                        {
                                            "case": { "$in": ["$racimos", [5, 6]] },
                                            "then": "5 - 6"
                                        },
                                        {
                                            "case": { "$in": ["$racimos", [7, 8]] },
                                            "then": "7 - 8"
                                        },
                                        {
                                            "case": { "$in": ["$racimos", [9, 10]] },
                                            "then": "9 - 10"
                                        },
                                        {
                                            "case": {
                                                "$gt": ["$racimos", 10]
                                            },
                                            "then": "> 10"
                                        }
                                    ],
                                    "default": "0"
                                }
                            }
                        }
                    ],
                    "default": "-rango negativo"
                }
            },


            "rango flores femeninas": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$gte": ["$femeninas", 0] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$femeninas", 0] },
                                            "then": "0"
                                        },
                                        {
                                            "case": { "$in": ["$femeninas", [1, 2]] },
                                            "then": "1 - 2"
                                        },
                                        {
                                            "case": { "$in": ["$femeninas", [3, 4]] },
                                            "then": "3 - 4"
                                        }
                                    ],
                                    "default": "> 4"
                                }
                            }
                        }

                    ],
                    "default": "-rango negativo"
                }
            },


            "rango flores masculinas": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$gte": ["$masculinas", 0] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$masculinas", 0] },
                                            "then": "0"
                                        },
                                        {
                                            "case": { "$in": ["$masculinas", [1, 2]] },
                                            "then": "1 - 2"
                                        },
                                        {
                                            "case": { "$in": ["$masculinas", [3, 4]] },
                                            "then": "3 - 4"
                                        }
                                    ],
                                    "default": "> 4"
                                }
                            }
                        }

                    ],
                    "default": "-rango negativo"
                }
            },


            "rango chombas": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$gte": ["$chombas", 0] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$chombas", 0] },
                                            "then": "0"
                                        },
                                        {
                                            "case": { "$in": ["$chombas", [1, 2]] },
                                            "then": "1 - 2"
                                        },
                                        {
                                            "case": { "$in": ["$chombas", [3, 4]] },
                                            "then": "3 - 4"
                                        }
                                    ],
                                    "default": "> 4"
                                }
                            }
                        }

                    ],
                    "default": "-rango negativo"
                }
            },


            "rango improductivas": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$gt": ["$improductivas", 0] },
                            "then": "improductivas"
                        },
                        {
                            "case": { "$eq": ["$improductivas", 0] },
                            "then": false
                        }
                    ],
                    "default": "-otro"
                }
            },


            "rango resiembra": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$_id.resiembra", true] },
                            "then": "resiembra"
                        },
                        {
                            "case": { "$eq": ["$_id.resiembra", false] },
                            "then": false
                        }
                    ],
                    "default": "-otro"
                }
            }




        }
    }




    ,{
        "$addFields": {
            "dato_mapa": [

                {
                    "rango tipo":"racimos"
                    ,"rango":"$rango racimos"
                    ,"rango cantidad":"$racimos"
                },
                {
                    "rango tipo":"chombas"
                    ,"rango":"$rango chombas"
                    ,"rango cantidad":"$chombas"
                },
                {
                    "rango tipo":"femeninas"
                    ,"rango":"$rango flores femeninas"
                    ,"rango cantidad":"$femeninas"
                },
                {
                    "rango tipo":"masculinas"
                    ,"rango":"$rango flores masculinas"
                    ,"rango cantidad":"$masculinas"
                },
                {
                    "rango tipo":"improductivas"
                    ,"rango":"$rango improductivas"
                    ,"rango cantidad":"$improductivas"
                },
                {
                    "rango tipo":"resiembra"
                    ,"rango":"$rango resiembra"
                    ,"rango cantidad":"$_id.resiembra"
                }
            ]
        }
    },


    { "$unwind": "$dato_mapa" },

    {
        "$match": {
            "dato_mapa": { "$ne": {} }
        }
    },


    {
        "$match": {
            "dato_mapa.rango cantidad":{ "$ne": false }
        }
    },


    {
        "$match": {
            "dato_mapa.rango":{ "$ne": false }
        }
    },



    {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [


                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "0"] }
                                    ,{ "$ne": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#FF0000"
                        },

                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "1 - 2"] }
                                    ,{ "$ne": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#98ff98"
                        },

                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "3 - 4"] }
                                    ,{ "$ne": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#ffff00"
                        },

                        {
                            "case": { "$eq": ["$dato_mapa.rango", "5 - 6"] },
                            "then": "#359144"
                        },


                        {
                            "case": { "$eq": ["$dato_mapa.rango", "7 - 8"] },
                            "then": "#804000"
                        },
                        {
                            "case": { "$eq": ["$dato_mapa.rango", "9 - 10"] },
                            "then": "#ff8000"
                        },
                        {
                            "case": { "$eq": ["$dato_mapa.rango", "> 10"] },
                            "then": "#0833a2"
                        },


                         {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "0"] }
                                    ,{ "$eq": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#51d1f6"
                        },

                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "1 - 2"] }
                                    ,{ "$eq": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#cdcdcd"
                        },

                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$dato_mapa.rango", "3 - 4"] }
                                    ,{ "$eq": ["$dato_mapa.rango tipo", "femeninas"] }
                                ]
                            },
                            "then": "#ffc0cb"
                        },



                        {
                            "case": { "$eq": ["$dato_mapa.rango", "improductivas"] },
                            "then": "#572364"
                        },



                        {
                            "case": { "$eq": ["$dato_mapa.rango", "resiembra"] },
                            "then": "#ffc0cb"
                        }
                    ],
                    "default": "#0833a2"
                }
            }
        }
    },




    {
        "$project": {
            "_id": { "$toObjectId": "$_id.elemnq" },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Bloque": "$_id.bloque",
                "Lote": "$_id.lote",

                "tipo":"$dato_mapa.rango tipo",
                "rango":"$dato_mapa.rango",
                "cantidad":"$dato_mapa.rango cantidad",


                "color": "$color"
            },
            "geometry": "$_id.geometry"
        }
    }



]
