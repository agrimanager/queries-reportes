[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "coordenadas": "$variable_cartografia.features.geometry.coordinates"
        }
    },
    {
        "$addFields": {
            "coordenada x": { "$arrayElemAt": ["$coordenadas", 0] },
            "coordenada y": { "$arrayElemAt": ["$coordenadas", 1] }
        }
    },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Arbol": 0
            , "Point": 0
            , "uid": 0
            , "Formula": 0

            , "coordenadas": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_inventariodelcultivo",
            "as": "info_lote",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": [{ "$toString": "$$nombre_lote" }, { "$toString": "$Lote" }] }
                            ]
                        }
                    }
                }
                , {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$info_lote",
            "preserveNullAndEmptyArrays": true

        }
    },

    {
        "$addFields": {
            "lote_num_palmas": { "$ifNull": ["$info_lote.Palmas", 0] }
            , "lote_ha": { "$ifNull": ["$info_lote.Hectareas", 0] }
            , "lote_variedad": { "$ifNull": ["$info_lote.Variedad", "--sin informacion"] }
            , "lote_zona": { "$ifNull": ["$info_lote.Zona", "--sin informacion"] }


        }
    }

    , {
        "$project": {
            "info_lote": 0
        }
    }


    , {
          "$group": {
              "_id": {
                  "lote": "$lote",
                  "arbol": "$arbol"
              }
              , "data": { "$push": "$$ROOT" }
          }
      },
      {
          "$group": {
              "_id": {
                  "lote": "$_id.lote"
              }
              , "data": { "$push": "$$ROOT" }
              , "plantas_dif_censadas_x_lote": { "$sum": 1 }
          }
      }

      , { "$unwind": "$data" }
      , { "$unwind": "$data.data" }
      , {
          "$replaceRoot": {
              "newRoot": {
                  "$mergeObjects": [
                      "$data.data",
                      {
                          "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                      }
                  ]
              }
          }
      }


      , {
          "$addFields": {
              "sum_racimos_muestreo_x_censo": {
                  "$sum": [
                      { "$toDouble": "$Racimos" }
                      , { "$toDouble": "$Inflorescencias femeninas polinizadas" }
                  ]
              }
          }

      }



]
