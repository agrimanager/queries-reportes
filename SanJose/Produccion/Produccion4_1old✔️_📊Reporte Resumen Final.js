[

        {
            "$addFields": {
                "split_path": {
                    "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","]
                }
            }
        },

        {
            "$addFields": {
                "split_path_o_id": {
                    "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } }
                }
            }
        },

        {
            "$addFields": {
                "palma_o_id": {
                    "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } }
                }
            }
        },

        {
            "$addFields": {
                "split_path_o_id": {
                    "$concatArrays": [
                        "$split_path_o_id",
                        "$palma_o_id"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_o_id",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name",
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },

        { "$unwind": "$Finca" },

        {
            "$project": {
                "split_path": 0,
                "split_path_o_id": 0,
                "palma_o_id": 0,
                "objetos_del_cultivo": 0,
                "Arbol": 0,
                "Point": 0
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$Finca",
                    "lote": "$Lote",
                    "planta": "$Planta"
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "lote": "$_id.lote"
                },
                "numero_de_palmas_censadas_por_lote": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "#Palmas muestreo": "$numero_de_palmas_censadas_por_lote" }
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "form_formulariopuente",
                "localField": "Lote",
                "foreignField": "Lote",
                "as": "array_puente"
            }
        },

        {
            "$addFields": {
                "objeto_puente": {
                    "$arrayElemAt": ["$array_puente", { "$subtract": [ { "$size": "$array_puente" }, 1 ] }]
                }
            }
        },

        {
            "$addFields": {
                "Ano siembra": "$objeto_puente.Ano siembra",
                "Zona": "$objeto_puente.Zona",
                "Hectareas Netas": "$objeto_puente.Hectareas Netas",
                "Numero palmas": "$objeto_puente.Numero palmas",
                "Variedad": "$objeto_puente.Variedad",
                "Peso Promedio Racimo Kg": "$objeto_puente.Peso Promedio Racimo Kg"
            }
        },

        {
            "$project": {
                "array_puente": 0,
                "objeto_puente": 0
            }
        },



        {
            "$addFields": {
                "ano_registro": { "$year": "$rgDate" },
                "ano_siembra": { "$year": "$Ano siembra" },
                "mes_registro": { "$month": "$rgDate" },
                "mes_siembra": { "$month": "$Ano siembra" }
            }
        },

        {
            "$addFields": {
                "edad": { "$subtract": ["$ano_registro", "$ano_siembra"] },
                "diferencia_mes": { "$subtract": ["$mes_registro", "$mes_siembra"] },
                "meses_a_sumar": { "$subtract": [12, "$mes_siembra"] }
            }
        },

        {
            "$addFields": {
                "edad": {
                    "$cond": {
                        "if": { "$lt": ["$diferencia_mes", 0] },
                        "then": { "$subtract": ["$edad", 1] },
                        "else": "$edad"
                    }
                }
            }
        },

        {
            "$addFields": {
                "Edad en Meses (segun ultimo censo)":{
                   "$sum": [{ "$sum": [{ "$multiply": ["$edad", 12] }, "$mes_registro"] }, "$meses_a_sumar"]
                }
            }
        },

        {
            "$project": {
                "ano_registro": 0,
                "ano_siembra": 0,
                "mes_registro": 0,
                "mes_siembra": 0,
                "edad": 0,
                "diferencia_mes": 0,
                "meses_a_sumar": 0
            }
        },


        {
            "$addFields": {

                "%Muestreo": {
                    "$cond": {
                        "if": { "$eq": ["$Numero palmas", 0] },
                        "then": 0,
                        "else": {
                          "$divide": [{
                              "$floor": {
                                  "$multiply": [{
                                      "$multiply": [
                                       { "$divide": ["$#Palmas muestreo", "$Numero palmas"] }, 100
                                    ]
                                  }, 100]
                              }
                          }, 100]
                        }
                    }
                },

                "IFAS/Ha": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$#Palmas muestreo", 0] }, { "$eq": ["$Hectareas Netas", 0] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                       "$divide": [
                                           {
                                                "$multiply": [
                                                    { "$divide": ["$Inflorescencias femeninas polinizadas", "$#Palmas muestreo"] },
                                                    "$Numero palmas"
                                                ]
                                            },
                                            "$Hectareas Netas"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "Total Muestreo (IFAS+Racimos)": {
                    "$sum": ["$Inflorescencias femeninas polinizadas", "$Racimos"]
                }
            }
        },


        {
            "$addFields": {

                "Total (IFAS + Racimos proyectados)": {
                    "$cond": {
                        "if": { "$eq": ["$#Palmas muestreo", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                       "$multiply": [
                                           { "$divide": ["$Total Muestreo (IFAS+Racimos)", "$#Palmas muestreo"] },
                                           "$Numero palmas"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "%Palmas improductivas muestra": {
                    "$cond": {
                        "if": { "$eq": ["$#Palmas muestreo", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$multiply": [
                                          { "$divide": ["$Improductivas", "$#Palmas muestreo"] },
                                          100
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "IMAS/Ha": {
                    "$cond": {
                        "if": {
                            "$or": [{ "$eq": ["$#Palmas muestreo", 0] }, { "$eq": ["$Hectareas Netas", 0] }]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": [
                                           {
                                                "$multiply": [
                                                    { "$divide": ["$Inflorescencias Masculinas", "$#Palmas muestreo"] },
                                                    "$Numero palmas"
                                                ]
                                            },
                                            "$Hectareas Netas"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "Produccion estimada (Kg)": {
                    "$multiply": ["$Total (IFAS + Racimos proyectados)", "$Peso Promedio Racimo Kg"]
                }
            }
        },


        {
            "$addFields": {
                "Produccion estimada (Kg)": {
                    "$cond": {
                        "if": { "$eq": ["$Produccion estimada (Kg)", null] },
                        "then": "-",
                        "else": "$Produccion estimada (Kg)"
                    }
                }
            }
        },


        {
            "$addFields": {

                "Produccion estimada (t-RFF)": {
                    "$cond": {
                        "if": { "$eq": ["$Produccion estimada (Kg)", "-"] },
                        "then": "-",
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": ["$Produccion estimada (Kg)", 1000]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                }
            }
        },


        {
            "$addFields": {

                "Semestre Estimado Ton/Ha": {
                    "$cond": {
                        "if": { "$eq": ["$Produccion estimada (Kg)", "-"] },
                        "then": "-",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$Hectareas Netas", 0] },
                                "then": 0,
                                "else": {
                                    "$divide": [{
                                        "$floor": {
                                            "$multiply": [{
                                                "$divide": ["$Produccion estimada (t-RFF)", "$Hectareas Netas"]
                                            }, 100]
                                        }
                                    }, 100]
                                }
                            }
                        }
                    }
                },

                "Rac/Palma A Semestre": {
                    "$cond": {
                        "if": { "$eq": ["$Numero palmas", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": ["$Total (IFAS + Racimos proyectados)", "$Numero palmas"]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                }
            }
        }
    ]
