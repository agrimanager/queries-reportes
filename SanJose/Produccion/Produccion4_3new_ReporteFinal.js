//reporte
db.form_censodeproduccion.aggregate(
    [

        //=============CARTOGRAFIA nueva
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        //--palntas_x_lote
        //no todos tienen el dato
        // { "$addFields": { "Palmas x Lote": { "$ifNull": ["$lote.properties.custom.Numero de plantas.value", 0] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


        // //---coordenadas
        // {
        //     "$addFields": {
        //         "coordenadas": "$variable_cartografia.features.geometry.coordinates"
        //     }
        // },
        // {
        //     "$addFields": {
        //         "coordenada x": { "$arrayElemAt": ["$coordenadas", 0] },
        //         "coordenada y": { "$arrayElemAt": ["$coordenadas", 1] }
        //     }
        // },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Arbol": 0
                , "Point": 0
                , "uid": 0
                , "Formula": 0

                // , "coordenadas": 0

                //---reporte final
                , "Observaciones": 0
                , "Otros": 0
                , "supervisor": 0
                , "uDate": 0
                , "capture": 0
            }
        }


        //=============CRUCES

        //---cruce con info_lote
        , {
            "$lookup": {
                "from": "form_inventariodelcultivo",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{ "$toString": "$$nombre_lote" }, { "$toString": "$Lote" }] }
                                ]
                            }
                        }
                    }
                    , {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": true
                // "preserveNullAndEmptyArrays": false
            }
        },

        //--"Lote" : "1-1",
        //  "Zona" : "1",
        //  "Variedad" : "HIBRIDO Coari x La Me",
        //  "Hectareas" : 28.26,
        //  "Palmas" : 3532,


        //---sacar variables
        {
            "$addFields": {
                "lote_num_palmas": { "$ifNull": ["$info_lote.Palmas", 0] }
                , "lote_ha": { "$ifNull": ["$info_lote.Hectareas", 0] }
                , "lote_variedad": { "$ifNull": ["$info_lote.Variedad", "--sin informacion"] }
                , "lote_zona": { "$ifNull": ["$info_lote.Zona", "--sin informacion"] }

                //--obtener de formulario de Pesos Promedios
                // , "lote_peso_promedio": { "$ifNull": ["$info_lote.Peso promedio", 0] }
                // , "lote_fecha_siembra": { "$ifNull": ["$info_lote.Fecha Siembra", null] }


            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }


        //---cruce con PESO PROMEDIO
        , {
            "$lookup": {
                "from": "form_pesospromedios",
                "as": "info_pesospromedios",
                "let": {
                    "nombre_lote": "$lote"
                    , "rgDate_censo": "$rgDate"
                },
                "pipeline": [
                    //--filtro lote
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{ "$toString": "$$nombre_lote" }, { "$toString": "$Lote" }] }
                                ]
                            }
                        }
                    }

                    //filtro fechas
                    , {
                        "$addFields": {
                            "resta_fechas": {
                                "$subtract": ["$$rgDate_censo", "$rgDate"]
                            }
                        }
                    }
                    
                    //---OJO
                    //1) resta_fechas < 0 ===> peso_prom con fecha superior
                    //2) resta_fechas > 0 ===> peso_prom con fecha inferior
                    
                    //---->>omitir (resta_fechas NEGATIVA)
                    ,{
                      "$match":{
                          "resta_fechas":{"$gt":0}
                      }  
                    }
                    
                    //seleccionar MINIMO DE (resta_fechas POSITIVA)
                    ,{
                      "$sort":{
                          "resta_fechas": 1
                      }  
                    }

                    , {
                        "$limit": 1
                    }
                ]
            }
        },
        
        //---TEST
        {
            $match:{
                "lote":"1-1"
            }
        },
        //-------------
        
        {
            "$unwind": {
                "path": "$info_pesospromedios",
                "preserveNullAndEmptyArrays": true
                // "preserveNullAndEmptyArrays": false
            }
        },


        //---sacar variables
        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_pesospromedios.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_pesospromedios": 0
            }
        }



        //=============AGRUPACIONES

        //agrupacion1 -- plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        // {
        //  "_id" : ObjectId("6093289fc96d0a335dd5da73"),
        //  "rgDate" : ISODate("2021-05-05T11:17:48.000-05:00"),

        ////------1)BASE
        //  "finca" : "Semana Santa",
        //  "bloque" : "2",
        //  "lote" : "02-29",
        //  "linea" : "02-29-72",
        //  "arbol" : "02-29-72-21",

        ////------2)INFO
        //  "lote_num_palmas" : 3211,
        //  "lote_ha" : 25.69,
        //  "lote_variedad" : "HIBRIDO Coari x La Me",
        //  "lote_zona" : "1",
        //  "plantas_dif_censadas_x_lote" : 213
        // }

        ////------3)SUMATORIAS
        //  "Inflorescencias femeninas polinizadas" : 0,
        //  "Inflorescencias Masculinas" : 0,
        //  "Racimos" : 1,
        //  "Chombas" : 0,
        //  "Improductivas" : 0,


        // //===========REPORTE FINAL

        //--agrupacion FINAL
        , {
            "$group": {
                "_id": {

                    //1)variables base
                    "lote": "$lote"

                    //2)variables info
                    , "lote_num_palmas": "$lote_num_palmas"
                    , "lote_ha": "$lote_ha"
                    , "lote_variedad": "$lote_variedad"
                    , "lote_zona": "$lote_zona"
                    , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    , "lote_peso_promedio": "$lote_peso_promedio"
                    , "lote_fecha_siembra": "$lote_fecha_siembra"

                }


                //3)variables agrupadas (5 variables)

                //--femeninas (IFAS)
                , "femeninas": { "$sum": { "$toDouble": { "$ifNull": ["$Inflorescencias femeninas polinizadas", 0] } } }
                //--masculinas (IMAS)
                , "masculinas": { "$sum": { "$toDouble": { "$ifNull": ["$Inflorescencias Masculinas", 0] } } }
                //--racimos
                , "racimos": { "$sum": { "$toDouble": { "$ifNull": ["$Racimos", 0] } } }
                //--chombas
                , "chombas": { "$sum": { "$toDouble": { "$ifNull": ["$Chombas", 0] } } }
                //--improductivas
                , "improductivas": { "$sum": { "$toDouble": { "$ifNull": ["$Improductivas", 0] } } }


                // , "data": { "$push": "$$ROOT" }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "femeninas": "$femeninas",
                            "masculinas": "$masculinas",
                            "racimos": "$racimos",
                            "chombas": "$chombas",
                            "improductivas": "$improductivas"
                        }
                    ]
                }
            }
        }



        // //agrupacion2 -- Racimos_muestrados_x_lote
        // //--new
        // , {
        //     "$addFields": {
        //         "sum_racimos_muestreo_x_censo": {
        //             "$sum": [
        //                 { "$toDouble": "$Racimos" }
        //                 , { "$toDouble": "$Inflorescencias femeninas polinizadas" }

        //             ]
        //         }
        //     }

        // }

        // xxx
        // // ,{
        // //     "$addFields": {
        // //         "ano_registro": { "$year": "$rgDate" },
        // //         "ano_siembra": { "$year": "$Ano siembra" },
        // //         "mes_registro": { "$month": "$rgDate" },
        // //         "mes_siembra": { "$month": "$Ano siembra" }
        // //     }
        // // },

        // // {
        // //     "$addFields": {
        // //         "edad": { "$subtract": ["$ano_registro", "$ano_siembra"] },
        // //         "diferencia_mes": { "$subtract": ["$mes_registro", "$mes_siembra"] },
        // //         "meses_a_sumar": { "$subtract": [12, "$mes_siembra"] }
        // //     }
        // // },

        // // {
        // //     "$addFields": {
        // //         "edad": {
        // //             "$cond": {
        // //                 "if": { "$lt": ["$diferencia_mes", 0] },
        // //                 "then": { "$subtract": ["$edad", 1] },
        // //                 "else": "$edad"
        // //             }
        // //         }
        // //     }
        // // },

        // // {
        // //     "$addFields": {
        // //         "Edad en Meses (segun ultimo censo)":{
        // //           "$sum": [{ "$sum": [{ "$multiply": ["$edad", 12] }, "$mes_registro"] }, "$meses_a_sumar"]
        // //         }
        // //     }
        // // },

        // // {
        // //     "$project": {
        // //         "ano_registro": 0,
        // //         "ano_siembra": 0,
        // //         "mes_registro": 0,
        // //         "mes_siembra": 0,
        // //         "edad": 0,
        // //         "diferencia_mes": 0,
        // //         "meses_a_sumar": 0
        // //     }
        // // },


        , {
            "$addFields": {

                "pct_Muestreo": {
                    "$cond": {
                        "if": { "$eq": ["$lote_num_palmas", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$multiply": [
                                            { "$divide": ["$plantas_dif_censadas_x_lote", "$lote_num_palmas"] }, 100
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "IFAS/Ha": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$plantas_dif_censadas_x_lote", 0] }, { "$eq": ["$lote_ha", 0] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": [
                                            {
                                                "$multiply": [
                                                    { "$divide": ["$femeninas", "$plantas_dif_censadas_x_lote"] },
                                                    "$lote_num_palmas"
                                                ]
                                            },
                                            "$lote_ha"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "Total Muestreo IFAS_y_Racimos": {
                    "$sum": ["$femeninas", "$racimos"]
                }
            }
        },


        {
            "$addFields": {

                "Total IFAS_y_RacimosProyectados": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$multiply": [
                                            { "$divide": ["$Total Muestreo IFAS_y_Racimos", "$plantas_dif_censadas_x_lote"] },
                                            "$lote_num_palmas"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "pct_Palmas improductivas muestra": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$multiply": [
                                            { "$divide": ["$improductivas", "$plantas_dif_censadas_x_lote"] },
                                            100
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "IMAS/Ha": {
                    "$cond": {
                        "if": {
                            "$or": [{ "$eq": ["$plantas_dif_censadas_x_lote", 0] }, { "$eq": ["$lote_ha", 0] }]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": [
                                            {
                                                "$multiply": [
                                                    { "$divide": ["$masculinas", "$plantas_dif_censadas_x_lote"] },
                                                    "$lote_num_palmas"
                                                ]
                                            },
                                            "$lote_ha"
                                        ]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                }

            }
        },



        //!!!!!!!!!DANGER si no hay peso promedio
        {
            "$addFields": {
                "Produccion estimada_Kg": {
                    "$multiply": ["$Total Muestreo IFAS_y_Racimos", "$lote_peso_promedio"]
                }
            }
        },


        {
            "$addFields": {
                "Produccion estimada_Kg": {
                    "$cond": {
                        "if": { "$eq": ["$Produccion estimada_Kg", null] },
                        "then": 0,
                        "else": "$Produccion estimada_Kg"
                    }
                }
            }
        },


        {
            "$addFields": {

                "Produccion estimada t_RFF": {
                    "$divide": [{
                        "$floor": {
                            "$multiply": [{
                                "$divide": ["$Produccion estimada_Kg", 1000]
                            }, 100]
                        }
                    }, 100]
                }
            }
        },


        {
            "$addFields": {

                "Semestre Estimado Ton/Ha": {
                    "$cond": {
                        "if": { "$eq": ["$lote_ha", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": ["$Produccion estimada t_RFF", "$lote_ha"]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                },

                "Rac/Palma A Semestre": {
                    "$cond": {
                        "if": { "$eq": ["$lote_num_palmas", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{
                                "$floor": {
                                    "$multiply": [{
                                        "$divide": ["$Total IFAS_y_RacimosProyectados", "$lote_num_palmas"]
                                    }, 100]
                                }
                            }, 100]
                        }
                    }
                }
            }
        }





    ]
    , { allowDiskUse: true })
