[

    { "$addFields": { "variable_fecha": "$rgDate" } },
    { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
    { "$addFields": { "mes": { "$month": "$variable_fecha" } } },

    { "$match": { "anio": { "$gt": 2000 } } },
    { "$match": { "anio": { "$lt": 3000 } } },
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },


    {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    },



    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": { "Cartography": "$arbol" }
    },
    {
        "$addFields": { "elemnq": { "$toObjectId": "$Cartography._id" } }
    },


    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Palma": 0
            , "Point": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
        }
    }


    , {
        "$addFields": {
            "sum_machos_y_hembras": {
                "$sum": [
                    { "$toDouble": { "$ifNull": ["$Machos", 0] } },
                    { "$toDouble": { "$ifNull": ["$Hembras", 0] } }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$eq": ["$sum_machos_y_hembras", 0] },
                    "then": "#ff0000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$sum_machos_y_hembras", 0] }, {
                                    "$lt": ["$sum_machos_y_hembras", 4]
                                }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$sum_machos_y_hembras", 4] }, {
                                            "$lt": ["$sum_machos_y_hembras", 11]
                                        }]
                                    },
                                    "then": "#0000ff",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$sum_machos_y_hembras", 11] }, {
                                                    "$lt": ["$sum_machos_y_hembras", 21]
                                                }]
                                            },
                                            "then": "#008000",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gte": ["$sum_machos_y_hembras", 21] }, {
                                                            "$lte": ["$sum_machos_y_hembras", 50]
                                                        }]
                                                    },
                                                    "then": "#c65911",
                                                    "else": "#808080"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$eq": ["$sum_machos_y_hembras", 0] },
                    "then": "A-(=0)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$sum_machos_y_hembras", 0] }, {
                                    "$lt": ["$sum_machos_y_hembras", 4]
                                }]
                            },
                            "then": "B-(1-3)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$sum_machos_y_hembras", 4] }, {
                                            "$lt": ["$sum_machos_y_hembras", 11]
                                        }]
                                    },
                                    "then": "C-(4-10)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$sum_machos_y_hembras", 11] }, {
                                                    "$lt": ["$sum_machos_y_hembras", 21]
                                                }]
                                            },
                                            "then": "D-(11-20)",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gte": ["$sum_machos_y_hembras", 21] }, {
                                                            "$lte": ["$sum_machos_y_hembras", 50]
                                                        }]
                                                    },
                                                    "then": "E-(21-50)",
                                                    "else": "F-(>50)"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "_id": "$elemnq",
            "idform": "$idform",
            "type": "Feature",
            "properties": {
                "Año": { "$toString": "$anio" },
                "Mes": "$Mes_Txt",

                "Rango": "$rango",
                "#Capturas": "$sum_machos_y_hembras",

                "Bloque": "$bloque",
                "Lote": "$lote",
                "Linea": "$linea",
                "Palma": "$arbol",

                "color": "$color"
            },
            "geometry": "$Cartography.geometry"
        }
    }



]
