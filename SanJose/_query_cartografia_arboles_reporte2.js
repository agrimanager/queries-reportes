db.form_monitoreoytratamientodeenfermedades.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS

        //🚩editar fechas segun form_ciclotratamiento
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-12T06:00:00.000-05:00"),
                "Busqueda fin": ISODate("2023-07-22T06:00:00.000-05:00"),//+1dia
                // "Busqueda fin": new Date,
                "today": new Date,

                "FincaID": ObjectId("5d27cd41995a863de81993e7"), //lapas
                // "FincaID": ObjectId("5d27d22e793a4867b305012c"), //semana santa
                // "FincaID": ObjectId("5d1395d3430d802507dfcb4f"), //areno
                // "FincaID": ObjectId("60ba34e5f14ef50dbf240d8c"), //los planes
                // "FincaID": ObjectId("5da0e388cbdafefbc05669bf"), //chalmeca

            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": ["$rgDate", "$Busqueda inicio"]
                        },
                        {
                            "$lte": ["$rgDate", "$Busqueda fin"]
                        }
                        , { "$eq": ["$Point.farm", { "$toString": "$FincaID" }] }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte






        { "$limit": 1 },




        {
            "$lookup": {
                "from": "cartography",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"

                    , "finca_id_str": "$Point.farm"
                },

                "pipeline": [


                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },


                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": "si"
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },


                    //condicion
                    { "$addFields": { "finca_id_string": { "$ifNull": [{ "$toString": "$finca._id" }, "no existe"] } } },

                    {
                        "$match": {
                            "$expr": { "$eq": ["$finca_id_string", "$$finca_id_str"] }
                        }
                    },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0
                        }
                    },

                    {
                        "$project": {
                            // "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "arbol": "$arbol"
                        }
                    },

                    {
                        "$project": {
                            "_id": 0
                        }
                    }

                    // ,{
                    //     "$addFields": {
                    //         "rgDate": "$$filtro_fecha_inicio"
                    //     }
                    // }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , [
                            {
                                "rgDate": "$Busqueda inicio"
                            }
                        ]
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

    ]
    , { allowDiskUse: true }
)
