db.users.aggregate(
    [


        //=====BASE0 ---- inyeccion de variables (❌ BORRAR)
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        //============================================
        //============================================
        //=====BASE1  ---- variables principal_concat
        { "$limit": 1 },
        {
            "$addFields": {
                "data_final": []
            }
        }
        , {
            "$addFields": {
                "user_timezone": "$timezone"
            }
        }



        /*
        //------FORMS

        form_controlcasassopopos
        form_erradicaciondelcultivo
        form_evaluaciondezompopos
        form_mantenimientodeequipos
        form_microinyeccion
        form_monitoreodeenfermedades
        form_monitoreodeopsiphanes
        form_monitoreodeplagas
        form_monitoreoderhinchophoruspalmarum
        form_monitoreoytratamientodeenfermedades
        form_ordendetrabajoenelcultivodepalmadeaceite
        form_aplicacindecultivodepalmas
        form_registroverificacionactividades
        form_SiembrayMantenimientoplantasnectariferas
        form_tratamientodestrategus
        form_tratamientodezompopos
        */

        //============================================
        //=====BASE2_i  ---- data_form
        , {
            "$lookup": {
                // "from": "XXXXXXXXXX___FORM",
                "from": "form_monitoreodeplagas",  //(🚩 EDITAR)
                "as": "data_form",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2

                    , "user_timezone": "$user_timezone"         //--filtro_timezone
                    , "formulario": "XXXXXXXXXX___FORM"         //--formulario
                },
                //query
                "pipeline": [


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    //----------------------------------------------------------------


                    //....query ---- (🚩 EDITAR)
                    {
                        "$addFields": {
                            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } }
                            ,"anio": { "$dateToString": { "format": "%Y", "date": "$rgDate", "timezone": "$$user_timezone" } }
                            ,"mes": { "$dateToString": { "format": "%m", "date": "$rgDate", "timezone": "$$user_timezone" } }
                            ,"dia_mes": { "$dateToString": { "format": "%d", "date": "$rgDate", "timezone": "$$user_timezone" } }
                            ,"hora": { "$dateToString": { "format": "%H", "date": "$rgDate", "timezone": "$$user_timezone" } }

                            ,"formulario": "$$formulario"
                        }
                    },



                    //....proyeccion ---- (🚩 EDITAR)
                    {
                        "$project": {
                            "_id": 0

                            ,"supervisor": 1
                            ,"formulario": 1

                            //..segun addFields
                            ,"fecha": 1
                            ,"anio": 1
                            ,"mes": 1
                            ,"dia_mes": 1
                            ,"hora": 1



                        }
                    }


                ]
            }
        }


        , {
            "$project": {

                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data_final"
                        , "$data_form"
                    ]
                }

                //----variables bases
                , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
                , "Busqueda fin": "$Busqueda fin"               //--filtro_fecha2
                , "user_timezone": "$user_timezone",         //--filtro_timezone
            }
        }

        //....ciclar varias veces segun forms


        //============================================
        //============================================
        //=====BASE3  ---- PROYECCIONFINAL FINAL

        //desagregar datos
        , { "$unwind": "$data_final" }

        //mostrar datos con variables de CONDICIONALES DE FECHA
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final",
                        {
                            "rgDate": "$Busqueda inicio"
                        }
                    ]
                }
            }
        }



    ], { allowDiskUse: true }
)
