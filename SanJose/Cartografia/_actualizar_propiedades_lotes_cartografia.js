

var data = [
    //     {
    //         lote: "xxxxx",
    //         anio_siembra: 2020,
    //         variedad: "xxxxx",
    //         material: "xxxxx",
    //         num_palmas: 123,
    //         densidad: 123,
    //         area: 123
    //     },


    //     {lote: "
    //     xxxxx
    //     ",anio_siembra:
    //     2020
    //     ,variedad: "
    //     xxxxx
    //     ",material: "
    //     xxxxx
    //     ",num_palmas:
    //     123
    //     ,densidad:
    //     123
    //     ,area:
    //     123
    //     },


    // {lote: "
    // xxxxx
    // ",anio_siembra:
    // 2020
    // ,variedad: "
    // xxxxx
    // ",material: "
    // xxxxx
    // ",num_palmas:
    // 123
    // ,densidad:
    // 123
    // ,area:
    // 123
    // },

    { lote: "1-1", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3459, densidad: 125, area: 27.67 },
    { lote: "1-1A", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 829, densidad: 125, area: 6.63 },
    { lote: "1-2", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2681, densidad: 125, area: 21.45 },
    { lote: "1-3", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3159, densidad: 125, area: 25.27 },
    { lote: "1-4", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3799, densidad: 125, area: 30.39 },
    { lote: "1-5", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1770, densidad: 125, area: 14.16 },
    { lote: "1-6", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2274, densidad: 125, area: 18.19 },
    { lote: "1-7", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 5656, densidad: 125, area: 45.25 },
    { lote: "1-8", anio_siembra: 2014, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2092, densidad: 125, area: 16.74 },
    { lote: "1-9", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2030, densidad: 125, area: 16.24 },
    { lote: "1-10", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3190, densidad: 125, area: 25.52 },
    { lote: "1-11", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3940, densidad: 125, area: 31.52 },
    { lote: "1-12", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2019, densidad: 125, area: 16.15 },
    { lote: "1-13", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2415, densidad: 125, area: 19.32 },
    { lote: "1-13A", anio_siembra: 2015, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 784, densidad: 125, area: 6.27 },
    { lote: "1-14", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1277, densidad: 125, area: 10.22 },
    { lote: "1-15", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3535, densidad: 125, area: 28.28 },
    { lote: "1-15A", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1379, densidad: 125, area: 11.03 },
    { lote: "1-15B", anio_siembra: 2014, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2276, densidad: 125, area: 18.21 },
    { lote: "1-16", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3151, densidad: 125, area: 25.21 },
    { lote: "1-16A", anio_siembra: 2014, variedad: "D x P", material: "Guineensis", num_palmas: 707, densidad: 160, area: 4.42 },
    { lote: "1-17", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 4747, densidad: 125, area: 37.98 },
    { lote: "1-18", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2103, densidad: 125, area: 16.82 },
    { lote: "1-19", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 4071, densidad: 125, area: 32.57 },
    { lote: "1-20", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1567, densidad: 125, area: 12.54 },
    { lote: "1-21", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2905, densidad: 125, area: 23.24 },
    { lote: "1-22", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 4513, densidad: 125, area: 36.1 },
    { lote: "1-23", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2206, densidad: 125, area: 17.65 },
    { lote: "1-24", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3743, densidad: 125, area: 29.94 },
    { lote: "1-25", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1666, densidad: 125, area: 13.33 },
    { lote: "1-26", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 7927, densidad: 125, area: 63.42 },
    { lote: "1-27", anio_siembra: 2014, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 6657, densidad: 125, area: 53.26 },
    { lote: "2-19", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3946, densidad: 170, area: 23.21 },
    { lote: "02-20", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4335, densidad: 170, area: 25.5 },
    { lote: "02-21", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2846, densidad: 125, area: 22.77 },
    { lote: "02-22", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 5178, densidad: 125, area: 41.42 },
    { lote: "02-23", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3771, densidad: 125, area: 30.17 },
    { lote: "02-24", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3720, densidad: 125, area: 29.76 },
    { lote: "02-25", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1553, densidad: 125, area: 12.42 },
    { lote: "02-26", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 5235, densidad: 125, area: 41.88 },
    { lote: "02-27", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2690, densidad: 125, area: 21.52 },
    { lote: "02-28", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3398, densidad: 125, area: 27.18 },
    { lote: "02-29", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3313, densidad: 125, area: 26.5 },
    { lote: "02-30", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 6790, densidad: 143, area: 47.48 },
    { lote: "2-31", anio_siembra: 2014, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3989, densidad: 125, area: 31.91 },
    { lote: "2-31A", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3097, densidad: 170, area: 18.22 },
    { lote: "02-32", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 4150, densidad: 143, area: 29.02 },
    { lote: "02-33", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5638, densidad: 170, area: 33.16 },
    { lote: "02-34", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4744, densidad: 170, area: 27.91 },
    { lote: "02-35", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4136, densidad: 170, area: 24.33 },
    { lote: "02-36", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 3986, densidad: 143, area: 27.87 },
    { lote: "02-37", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 4783, densidad: 143, area: 33.45 },
    { lote: "02-38", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 4266, densidad: 143, area: 29.83 },
    { lote: "2-39", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4634, densidad: 143, area: 32.41 },
    { lote: "02-40", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 1866, densidad: 143, area: 13.05 },
    { lote: "02-41", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4034, densidad: 143, area: 28.21 },
    { lote: "2-42", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 2933, densidad: 143, area: 20.51 },
    { lote: "02-43", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 1992, densidad: 143, area: 13.93 },
    { lote: "02-47", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3359, densidad: 143, area: 23.49 },
    { lote: "02-48", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4217, densidad: 143, area: 29.49 },
    { lote: "2-49", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 2889, densidad: 143, area: 20.2 },
    { lote: "02-61", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 6086, densidad: 170, area: 35.8 },
    { lote: "02-62", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3038, densidad: 170, area: 17.87 },
    { lote: "02-63", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5984, densidad: 170, area: 35.2 },
    { lote: "02-64", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 4177, densidad: 143, area: 29.21 },
    { lote: "02-65", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 3903, densidad: 143, area: 27.29 },
    { lote: "02-66", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 5287, densidad: 143, area: 36.97 },
    { lote: "2-67", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 3759, densidad: 143, area: 26.29 },
    { lote: "02-68", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 4893, densidad: 143, area: 34.22 },
    { lote: "02-01", anio_siembra: 2012, variedad: "Deli x LaMè; Deli x Ghana", material: "Guineensis", num_palmas: 3683, densidad: 143, area: 25.76 },
    { lote: "02-02", anio_siembra: 2012, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3549, densidad: 143, area: 24.82 },
    { lote: "02-03", anio_siembra: 2012, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4868, densidad: 143, area: 34.04 },
    { lote: "02-04", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5102, densidad: 170, area: 30.01 },
    { lote: "2-5", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3216, densidad: 170, area: 18.92 },
    { lote: "2-6", anio_siembra: 2012, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 5565, densidad: 143, area: 38.92 },
    { lote: "02-07", anio_siembra: 2012, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4602, densidad: 143, area: 32.18 },
    { lote: "02-08", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1775, densidad: 125, area: 14.2 },
    { lote: "02-09", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2032, densidad: 125, area: 16.26 },
    { lote: "02-10", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 3988, densidad: 125, area: 31.9 },
    { lote: "02-11", anio_siembra: 2012, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5093, densidad: 170, area: 29.96 },
    { lote: "02-12", anio_siembra: 2012, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4356, densidad: 170, area: 25.62 },
    { lote: "02-13", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 2687, densidad: 125, area: 21.5 },
    { lote: "02-14", anio_siembra: 2012, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 1647, densidad: 125, area: 13.18 },
    { lote: "02-15", anio_siembra: 2013, variedad: "CoarÌ x LaMè", material: "Hibrido", num_palmas: 5839, densidad: 125, area: 46.71 },
    { lote: "02-16", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5255, densidad: 170, area: 30.91 },
    { lote: "2-16A", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 2215, densidad: 170, area: 13.03 },
    { lote: "2-17", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 6980, densidad: 170, area: 41.06 },
    { lote: "02-18", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 2425, densidad: 170, area: 14.26 },
    { lote: "02-44", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3036, densidad: 143, area: 21.23 },
    { lote: "02-45", anio_siembra: 2013, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3872, densidad: 143, area: 27.08 },
    { lote: "02-46", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 2795, densidad: 143, area: 19.55 },
    { lote: "02-50", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 2573, densidad: 143, area: 17.99 },
    { lote: "2-51", anio_siembra: 2013, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 2662, densidad: 143, area: 18.62 },
    { lote: "2-52", anio_siembra: 2013, variedad: "Deli x Ghana", material: "Guineensis", num_palmas: 3708, densidad: 160, area: 23.18 },
    { lote: "02-53", anio_siembra: 2013, variedad: "Deli x Ghana", material: "Guineensis", num_palmas: 4649, densidad: 160, area: 29.06 },
    { lote: "02-54", anio_siembra: 2013, variedad: "Deli x Ghana", material: "Guineensis", num_palmas: 3775, densidad: 160, area: 23.59 },
    { lote: "02-55", anio_siembra: 2013, variedad: "Compacta x Ghana", material: "Guineensis", num_palmas: 4773, densidad: 170, area: 28.08 },
    { lote: "02-56", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4984, densidad: 170, area: 29.32 },
    { lote: "02-57", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3002, densidad: 170, area: 17.66 },
    { lote: "02-58", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5459, densidad: 170, area: 32.11 },
    { lote: "02-59", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 6080, densidad: 170, area: 35.76 },
    { lote: "02-60", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4608, densidad: 170, area: 27.11 },
    { lote: "02-69", anio_siembra: 2014, variedad: "Deli x LaMè 2501", material: "Guineensis", num_palmas: 2918, densidad: 143, area: 20.41 },
    { lote: "02-70", anio_siembra: 2014, variedad: "Deli x LaMè 6858", material: "Guineensis", num_palmas: 4111, densidad: 143, area: 28.75 },
    { lote: "3-1", anio_siembra: 2014, variedad: "D x P", material: "Guineensis", num_palmas: 4389, densidad: 143, area: 30.69 },
    { lote: "3-2", anio_siembra: 2014, variedad: "Evolution", material: "Guineensis", num_palmas: 4479, densidad: 143, area: 31.32 },
    { lote: "3-3", anio_siembra: 2014, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 4257, densidad: 143, area: 29.77 },
    { lote: "3-4", anio_siembra: 2014, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 4091, densidad: 143, area: 28.61 },
    { lote: "3-5", anio_siembra: 2014, variedad: "D x A", material: "Guineensis", num_palmas: 7107, densidad: 143, area: 49.7 },
    { lote: "3-6", anio_siembra: 2014, variedad: "D x A", material: "Guineensis", num_palmas: 7429, densidad: 143, area: 51.95 },
    { lote: "3-7", anio_siembra: 2014, variedad: "D x A", material: "Guineensis", num_palmas: 7404, densidad: 143, area: 51.78 },
    { lote: "3-8", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 5256, densidad: 143, area: 36.76 },
    { lote: "3-9", anio_siembra: 2013, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 4706, densidad: 143, area: 32.91 },
    { lote: "3-10", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 6493, densidad: 143, area: 45.41 },
    { lote: "3-11", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 4683, densidad: 143, area: 32.75 },
    { lote: "3-12", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 2428, densidad: 143, area: 16.98 },
    { lote: "3-13", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 5979, densidad: 143, area: 41.81 },
    { lote: "3-14", anio_siembra: 2013, variedad: "Deli x Ghana", material: "Guineensis", num_palmas: 6002, densidad: 160, area: 37.51 },
    { lote: "3-15", anio_siembra: 2013, variedad: "Deli x Ghana", material: "Guineensis", num_palmas: 5937, densidad: 160, area: 37.11 },
    { lote: "3-16", anio_siembra: 2014, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 4908, densidad: 143, area: 34.32 },
    { lote: "3-17", anio_siembra: 2014, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 5374, densidad: 143, area: 37.58 },
    { lote: "3-18", anio_siembra: 2014, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 6669, densidad: 143, area: 46.64 },
    { lote: "3-19", anio_siembra: 2014, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 7028, densidad: 143, area: 49.15 },
    { lote: "3-20", anio_siembra: 2014, variedad: "Evolution", material: "Guineensis", num_palmas: 6502, densidad: 143, area: 45.47 },
    { lote: "3-21", anio_siembra: 2014, variedad: "Evolution", material: "Guineensis", num_palmas: 4619, densidad: 143, area: 32.3 },
    { lote: "3-22", anio_siembra: 2014, variedad: "Deli x Nigeria", material: "Guineensis", num_palmas: 5471, densidad: 143, area: 38.26 },
    { lote: "3-23", anio_siembra: 2014, variedad: "D x P", material: "Guineensis", num_palmas: 4198, densidad: 143, area: 29.36 },
    { lote: "3-25", anio_siembra: 2014, variedad: "D x P", material: "Guineensis", num_palmas: 6637, densidad: 143, area: 46.41 },
    { lote: "3-26", anio_siembra: 2014, variedad: "Deli x LaMè C0731 El Dorado", material: "Guineensis", num_palmas: 5285, densidad: 143, area: 36.96 },
    { lote: "3-27", anio_siembra: 2014, variedad: "Deli x LaMè C0731 El Dorado", material: "Guineensis", num_palmas: 7053, densidad: 143, area: 49.32 },
    { lote: "4-1", anio_siembra: 2013, variedad: "Deli x Ekona", material: "Guineensis", num_palmas: 2839, densidad: 143, area: 19.85 },
    { lote: "4-2", anio_siembra: 2014, variedad: "Deli x LaMè C26304 El Dorado ", material: "Guineensis", num_palmas: 5397, densidad: 143, area: 37.74 },
    { lote: "4-3", anio_siembra: 2014, variedad: "Deli x LaMè C26304 El Dorado ", material: "Guineensis", num_palmas: 5626, densidad: 143, area: 39.34 },
    { lote: "4-4", anio_siembra: 2014, variedad: "D x A", material: "Guineensis", num_palmas: 5655, densidad: 143, area: 39.55 },
    { lote: "4-5", anio_siembra: 2014, variedad: "Deli x LaMè C26304 El Dorado ", material: "Guineensis", num_palmas: 7519, densidad: 143, area: 52.58 },
    { lote: "4-6", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 6852, densidad: 170, area: 40.31 },
    { lote: "4-7", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 7879, densidad: 170, area: 46.35 },
    { lote: "4-8", anio_siembra: 2014, variedad: "Deli x LaMè 1001", material: "Guineensis", num_palmas: 5943, densidad: 143, area: 41.56 },
    { lote: "4-9", anio_siembra: 2013, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 7011, densidad: 170, area: 41.24 },
    { lote: "4-10", anio_siembra: 2014, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3920, densidad: 170, area: 23.06 },
    { lote: "4-11", anio_siembra: 2015, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 5768, densidad: 170, area: 33.93 },
    { lote: "4-12", anio_siembra: 2015, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 2126, densidad: 170, area: 12.51 },
    { lote: "4-13", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 4285, densidad: 170, area: 25.21 },
    { lote: "4-14", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3534, densidad: 170, area: 20.79 },
    { lote: "4-15", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 3757, densidad: 170, area: 22.1 },
    { lote: "4-16", anio_siembra: 2016, variedad: "Deli x Compacta", material: "Guineensis", num_palmas: 2150, densidad: 170, area: 12.65 },
    { lote: "05-19", anio_siembra: 2016, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 1839, densidad: 143, area: 12.86 },
    { lote: "05-20", anio_siembra: 2016, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 2089, densidad: 143, area: 14.61 },
    { lote: "05-21", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3449, densidad: 143, area: 24.12 },
    { lote: "05-22", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3558, densidad: 143, area: 24.88 },
    { lote: "05-23", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 2876, densidad: 143, area: 20.11 },
    { lote: "05-24", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4367, densidad: 143, area: 30.54 },
    { lote: "05-25", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 3680, densidad: 143, area: 25.73 },
    { lote: "05-26", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 2837, densidad: 143, area: 19.84 },
    { lote: "05-27", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 2265, densidad: 143, area: 15.84 },
    { lote: "05-28", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 2910, densidad: 143, area: 20.35 },
    { lote: "05-29", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4167, densidad: 143, area: 29.14 },
    { lote: "05-30", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4445, densidad: 143, area: 31.08 },
    { lote: "05-31", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 6423, densidad: 143, area: 44.92 },
    { lote: "05-32", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4658, densidad: 143, area: 32.57 },
    { lote: "05-33", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4965, densidad: 143, area: 34.72 },
    { lote: "05-34", anio_siembra: 2015, variedad: "Deli x LaMè", material: "Guineensis", num_palmas: 4261, densidad: 143, area: 29.8 }


]




data.forEach(i => {

    db.cartography.update(
        {
            "properties.name": i.lote
        },
        {
            $set: {
                "properties.custom": {

                    "Año de siembra": {
                        "type": "number",
                        "value": i.anio_siembra
                    },
                    "Variedad": {
                        "type": "string",
                        "value": i.variedad
                    },
                    "Material": {
                        "type": "string",
                        "value": i.material
                    },
                    "Numero de plantas": {
                        "type": "number",
                        "value": i.num_palmas
                    },
                    "Densidad": {
                        "type": "number",
                        "value": i.densidad
                    },
                    "Numero de hectareas": {
                        "type": "number",
                        "value": i.area
                    }

                }
            }
        }

    )


})


// 			"Año de siembra" : {
// 				"type" : "number",
// 				"value" : 0
// 			},
// 			"Numero de hectareas" : {
// 				"type" : "number",
// 				"value" : 15.96
// 			},
// 			"Numero de plantas" : {
// 				"type" : "number",
// 				"value" : 1761
// 			},
// 			"Numero de plantas erradicadas" : {
// 				"type" : "number",
// 				"value" : 9
// 			},
// 			"Material" : {
// 				"type" : "string",
// 				"value" : "HIBRIDO Coari x La Me"
// 			}
