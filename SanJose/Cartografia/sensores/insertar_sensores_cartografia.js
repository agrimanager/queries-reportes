var data = db.form_creaciondepozosnuevos.aggregate(


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2020-07-26T06:00:00.000-05:00"),
            // "Busqueda fin": ISODate("2023-07-25T06:00:00.000-05:00")
            "Busqueda fin": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------



    {
        $unwind: "$POZO.features"
    }



    , {
        $addFields: {
            path_bloque: "$POZO.path",
            _id_str_lote: "$POZO.features._id"
        }
    }

    , {
        "$project": {
            "POZO": 0
        }
    }



    , {
        $match: {
            "NOMBRE DE POZO": {
                $nin: [
                    "",
                    "1",
                    "Prueba 1",
                    "Prueba 2",
                    "Prueba 3",
                    "Prueba 4",
                    "Prueba 5",
                ]
            }

        }
    }


    , {
        "$addFields": {
            "ITEM_CARTOGRAFIA_INSERT": {
                //"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 0] },
                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 1] }
                    ]
                }
                , "properties": {
                    "type": "sensors",
                    "name": "$NOMBRE DE POZO",
                    "production": true,
                    "status": true,
                    "custom": {
                        "Tipo de sensor": {
                            "value": "Pozo",
                            "type": "string"
                        }
                    }
                },
                "path": {
                    $concat: [
                        "$path_bloque",
                        "$_id_str_lote",
                        ","
                    ]
                }
            }
        }
    }




)


// data


data.forEach(item_data_form => {


    //====2)insertar en cartografia
    db.cartography.insert(
        item_data_form.ITEM_CARTOGRAFIA_INSERT
    )

})
