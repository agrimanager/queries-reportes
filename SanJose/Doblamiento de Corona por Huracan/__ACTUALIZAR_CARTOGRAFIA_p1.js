

//378.026 total
//375.658 arboles
//347.541 arboles_enfermedad
//135.756 arboles_enfermedad_huracan

//1.813 = "Inestable por huracan" : "Solo mantenimiento"
var data = db.form_danoporhuracan.aggregate(

    [

        { $unwind: "$Lote.features" }

        , {
        $match: {
            "Lote.features.properties.type": { $eq: "trees" }

            , "Enfermedad": { $exists: true }

            // ,"Inestable por huracan":{$exists:true}
            , "Inestable por huracan": "Solo mantenimiento"
        }
    }

        , {
        $addFields: {
            palma: "$Lote.features.properties.name"
        }
    }

        , {
        $group: {
            _id: "$palma"
            , cantidad: { $sum: 1 }
        }
    }


    ]
    , { allowDiskUse: true }
)

// data

data.forEach(i => {

    db.cartography.update(
        {
            "properties.name": i._id
        },
        {
            $set: {
                "properties.custom.Erradicada": {
                    "type": "bool",
                    "value": false,
                    "color": "#F8F8F8"
                },
                "properties.custom.Resiembra": {
                    "type": "bool",
                    "value": false
                },
                "properties.custom.Palmas enfermas por erradicar": {
                    "type": "bool",
                    "value": false,
                    "color": "#7f7f80"
                },
                "properties.custom.Casos nuevos": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                },
                "properties.custom.Palma enferma": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                },
                "properties.custom.Palmas enfermas casos nuevos": {
                    "type": "bool",
                    "value": false,
                    "color": "#CC00FF"
                },
                "properties.custom.Palmas en recuperación menor a 16 hojas sanas": {
                    "type": "bool",
                    "value": false,
                    "color": "#fdfd01"
                },
                "properties.custom.Recuperación vegetativa mayor a 16 hojas sanas": {
                    "type": "bool",
                    "value": false,
                    "color": "#0070bf"
                },
                "properties.custom.Palmas en recuperación productiva": {
                    "type": "bool",
                    "value": false,
                    "color": "#ec7d31"
                },
                "properties.custom.Palmas de alta": {
                    "type": "bool",
                    "value": false,
                    "color": "#0df20d"
                },
                "properties.custom.Doblamiento de corona": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                }


                //---nuevos de huracan
                , "properties.custom.Doblamiento de flecha": {
                    "type": "bool",
                    "value": false,
                    "color": "#931bb4"
                }
                , "properties.custom.Cogollo doblado": {
                    "type": "bool",
                    "value": false,
                    "color": "#00afef"
                }
                , "properties.custom.Cogollo podrido": {
                    "type": "bool",
                    "value": false,
                    "color": "#fd00fe"
                }
                , "properties.custom.Cogollo quebrado": {
                    "type": "bool",
                    "value": false,
                    "color": "#cbfd33"
                }


                , "properties.custom.Solo mantenimiento": {
                    "type": "bool",
                    "value": true,//------------------------------------------>>>TRUE
                    "color": "#a2785d"
                }




            }
        }

    )

})


/*
[
    {
        "name": "Erradicada",----
        "type": "bool",
        "color": "#F8F8F8"
    },
    {
        "name": "Resiembra",----
        "type": "bool"
    },
    {
        "name": "Casos nuevos",----
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Palma enferma",----
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Palmas enfermas casos nuevos",---
        "type": "bool",
        "color": "#CC00FF"
    },
    {
        "name": "Palmas enfermas por erradicar",----
        "type": "bool",
        "color": "#7f7f80"
    },
    {
        "name": "Palmas en recuperación menor a 16 hojas sanas",----
        "type": "bool",
        "color": "#fdfd01"
    },
    {
        "name": "Recuperación vegetativa mayor a 16 hojas sanas",----
        "type": "bool",
        "color": "#0070bf"
    },
    {
        "name": "Palmas en recuperación productiva",----
        "type": "bool",
        "color": "#ec7d31"
    },
    {
        "name": "Palmas de alta",----
        "type": "bool",
        "color": "#0df20d"
    },
    {
        "name": "Doblamiento de corona",----
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Doblamiento de flecha",
        "type": "bool",
        "color": "#931bb4"
    },
    {
        "name": "Cogollo doblado",
        "type": "bool",
        "color": "#00afef"
    },
    {
        "name": "Cogollo podrido",
        "type": "bool",
        "color": "#fd00fe"
    },
    {
        "name": "Solo mantenimiento",
        "type": "bool",
        "color": "#a2785d"
    },
    {
        "name": "Cogollo quebrado",
        "type": "bool",
        "color": "#cbfd33"
    }
]

*/




////----------------------------------------


// {
// 	"Erradicada" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#F8F3F3",
// 		"id" : NumberInt(0)
// 	},
// 	"Resiembra" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"id" : NumberInt(2)
// 	},
// 	"Palmas enfermas por erradicar" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#B0B7B6",
// 		"id" : NumberInt(4)
// 	},
// 	"Casos nuevos" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#FFF200",
// 		"id" : NumberInt(6)
// 	},
// 	"Palma enferma" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#F40A0A",
// 		"id" : NumberInt(8)
// 	},
// 	"Palmas enfermas casos nuevos" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#BB18DC",
// 		"id" : NumberInt(10)
// 	},
// 	"Palmas en recuperación menor a 16 hojas sanas" : {
// 		"type" : "bool",
// 		"value" : false,
// 		"color" : "#F1C73C",
// 		"id" : NumberInt(12)
// 	},
// 	"Recuperación vegetativa mayor a 16 hojas sanas" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#0400FF",
// 		"id" : NumberInt(14)
// 	},
// 	"Palmas en recuperación productiva" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#FF8C00",
// 		"id" : NumberInt(16)
// 	},
// 	"Palmas de alta" : {
// 		"type" : "bool",
// 		"value" : [ ],
// 		"color" : "#40FF00",
// 		"id" : NumberInt(18)
// 	},
// 	"Doblamiento de corona" : {
// 		"type" : "bool",
// 		"value" : true,
// 		"color" : "#30BEF1",
// 		"id" : NumberInt(20)
// 	},
// 	"color" : {
// 		"type" : "color",
// 		"value" : "#F1C73C",
// 		"id" : NumberInt(22)
// 	}
// }
