var data = db.form_danoporhuracan_data.aggregate(
    [

        //NOTA: se creo un indice en cartografia de en "properties.name" para optimizar el tiempo
        {
            "$lookup": {
                "from": "cartography",
                "as": "info_cartografia",
                "let": {
                    "arbol": "$Lote"
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$properties.name", "$$arbol"]
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { "$split": [{ "$trim": { "input": "$info_cartografia.path", "chars": "," } }, ","] }
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { $arrayElemAt: ["$id_str_farm", 0] }
            }
        }





        //datos
        , {
            $addFields: {
                item_insert_form:
                {
                    // "_id": ObjectId("64375a8f68c42eb0b7f32e57"),
                    "Lote": {
                        "type": "selection",
                        "features": [
                            {
                                "_id": { $toString: "$info_cartografia._id" },
                                "type": "Feature",
                                "properties": {
                                    "name": "$info_cartografia.properties.name",
                                    "type": "trees",
                                    "production": "$info_cartografia.properties.production",
                                    "status": "$info_cartografia.properties.status",
                                    "custom": "$info_cartografia.properties.custom",
                                },
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": "$info_cartografia.geometry.coordinates"
                                },
                                "path": "$info_cartografia.path"
                            }
                        ],
                        "path": "$info_cartografia.path"
                    },
                    "Enfermedad": "$Enfermedad",
                    "Numero de hojas": 0,
                    "Solo mantenimiento": "$Solo mantenimiento",
                    "Inestable por huracan": "$Inestable por huracan",
                    "Point": {
                        "type": "Feature",
                        "geometry": {
                            "coordinates": "$info_cartografia.geometry.coordinates",
                            "type": "point"
                        },
                        //"farm": "5d1395d3430d802507dfcb4f"
                        "farm": "$id_str_farm"
                    },
                    "uid": ObjectId("5d138cc4430d802507dfcb25"),
                    "supervisor": "Support team",
                    // rgDate: {
                    //     $dateFromString: {
                    //         dateString: "$Fecha"
                    //     }
                    // },
                    // uDate: {
                    //     $dateFromString: {
                    //         dateString: "$Fecha"
                    //     }
                    // },
                    rgDate: new Date,
                    uDate: new Date,

                    "capture": "W"
                }



            }
        }


        , {
            "$project": {
                "item_insert_form": 1
            }
        }


    ], { allowDiskUse: true }
)
// .count()

//--BULK INSERT
var bulk = db.form_danoporhuracan.initializeUnorderedBulkOp();

data.forEach(i => {

    // db.form_danoporhuracan.insert(
    //     i.item_insert_form
    // )

    bulk.insert(i.item_insert_form);
})


bulk.execute();


/*
// collection: form_danoporhuracan
{
	"_id" : ObjectId("64375a8f68c42eb0b7f32e57"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5ea0af10c1d28fdafe64c8a4",
				"type" : "Feature",
				"properties" : {
					"name" : "1-1-99-4",
					"type" : "trees",
					"production" : true,
					"status" : true,
					"custom" : {

					}
				},
				"geometry" : {
					"type" : "Point",
					"coordinates" : [
						-84.2231222397562,
						12.2293984551316
					]
				},
				"path" : ",5d1395d3430d802507dfcb4f,5da0e23eb74d2f472e6a23b8,5ea0aed5c1d28fdafe64bffa,5ea0aed8c1d28fdafe64c02c,"
			}
		],
		"path" : ",5d1395d3430d802507dfcb4f,5da0e23eb74d2f472e6a23b8,5ea0aed5c1d28fdafe64bffa,5ea0aed8c1d28fdafe64c02c,"
	},
	-"Enfermedad" : "Cogollo podrido",
	-"Numero de hojas" : 0,
	-"Solo mantenimiento" : "",
	-"Inestable por huracan" : "",
	"Point" : {
		"type" : "Feature",
		"geometry" : {
			"coordinates" : [
				"-84.26290056752062",
				"12.175703322931682"
			],
			"type" : "point"
		},
		"farm" : "5d1395d3430d802507dfcb4f"
	},
	"uid" : ObjectId("5d138cc4430d802507dfcb25"),
	"supervisor" : "Support team",
	"rgDate" : ISODate("2023-04-12T20:27:43.917-05:00"),
	"uDate" : ISODate("2023-04-12T20:27:43.917-05:00"),
	"capture" : "W"
}

*/
