
var data = db.form_danoporhuracan.aggregate(
    [


        // //----------------------------------------------------------------
        // //---VARIABLES INYECTADAS
        // {
        //     $addFields: {
        //         "Busqueda inicio": ISODate("2023-01-01T06:00:00.000-05:00"),
        //         "Busqueda fin": new Date,
        //         "today": new Date,
        //         // "FincaID": ObjectId("5d27cd41995a863de81993e7"), //lapas
        //     }
        // },
        // //----FILTRO FECHAS Y FINCA
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //
        //                 {
        //                     "$gte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
        //                     ]
        //                 },
        //                 {
        //                     "$lte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
        //                     ]
        //                 }
        //                 // , {"$eq": ["$Point.farm", { "$toString": "$FincaID" }]}
        //             ]
        //         }
        //     }
        // },
        // //----------------------------------------------------------------
        // //....query reporte


        {
            $match: {
                supervisor: "Support team"
            }
        },


        {
            $unwind: "$Lote.features"
        },



        {
            $addFields: {
                arbol: "$Lote.features.properties.name"
            }
        }


        , {
            "$project": {
                "supervisor": 0,
                "capture": 0,
                "Lote": 0,
                "Point": 0,
                "uid": 0,
                "Formula": 0
            }
        }





        , {
            "$match": {
                "Enfermedad": { "$exists": true }
                , "Solo mantenimiento": { "$exists": true }
            }
        }



        , {
            "$addFields": {

                "Enfermedad": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$Enfermedad", ""] },
                                { "$eq": ["$Solo mantenimiento", "Si"] },
                            ]
                        },
                        "then": "Solo mantenimiento",
                        "else": "$Enfermedad"
                    }

                }

            }
        }





        , {
            "$match": {
                "$expr": {
                    "$or": [
                        { "$ne": ["$Enfermedad", ""] }
                    ]
                }

            }
        }



        , {
            $group: {
                _id: {
                    "arbol": "$arbol"
                }
                , data: { $push: "$$ROOT" }
                , fecha_max: { $max: "$rgDate" }
            }
        }



        , {
            "$addFields": {
                "data": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.rgDate", "$fecha_max"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // ,{$unwind: "$data"}

        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }


        /*
        ,{
			"name" : "",
			"type" : "bool",
			"color" : "#"
		},
		{
			"name" : "",
			"type" : "bool",
			"color" : "#"
		},
		{
			"name" : "",
			"type" : "bool",
			"color" : "#"
		},
		{
			"name" : "",
			"type" : "bool",
			"color" : "#"
		},
		{
			"name" : "",
			"type" : "bool",
			"color" : "#"
		}

        */




        //color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$Enfermedad", "Doblamiento de flecha"] },
                                "then": "#A921A2"
                            },
                            {
                                "case": { "$eq": ["$Enfermedad", "Cogollo doblado"] },
                                "then": "#47BDCF"
                            },
                            {
                                "case": { "$eq": ["$Enfermedad", "Cogollo podrido"] },
                                "then": "#E774E1"
                            },
                            {
                                "case": { "$eq": ["$Enfermedad", "Solo mantenimiento"] },
                                "then": "#AD986A"
                            },
                            {
                                "case": { "$eq": ["$Enfermedad", "Cogollo quebrado"] },
                                "then": "#E8E265"
                            }
                        ]
                        , "default": "otro_caso_sin_color"//olvidada,por altura,palma caida...etc
                    }
                }

            }

        }


        , {
            "$match": {
                "color": {
                    $ne: "otro_caso_sin_color"
                }
            }
        }


        , {
            "$project": {
                "Enfermedad": 1,
                "arbol": 1,
                "color": 1
            }
        }


        , {
            "$project": {
                "_id": 0
            }
        }




        // , {
        //     "$addFields": {
        //         "variable_custom": {
        //             "$concat": ["properties.custom.", "$Enfermedad"]
        //         }
        //     }
        // }


        //test
        // , { $limit: 100 }


        //OBTENER LOS QUE NO TIENEN PROPIEDADES CUSTOM DE ENFERMEDADES
        , {
            "$lookup": {
                "from": "cartography",
                "as": "data_cartografia",
                "let": {
                    "arbol": "$arbol"
                },
                //query
                "pipeline": [

                    {
                        $match: {
                            "properties.type": "trees"
                            // , "properties.custom": { $ne: {} }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$properties.name", "$$arbol"]
                                    }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "array_properties_custom": {
                                "$objectToArray": "$properties.custom"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "array_properties_custom_filter": {
                                "$filter": {
                                    "input": "$array_properties_custom",
                                    "as": "item",
                                    "cond": { "$eq": ["$$item.v.value", true] }
                                }
                            }
                        }
                    }


                    , {
                        "$match": {
                            "array_properties_custom_filter": { $eq: [] }
                        }
                    }



                ]
            }
        }

        , {
            $unwind: "$data_cartografia"
        }

        , {
            "$project": {
                "data_cartografia": 0
            }
        }


        , {
            $group: {
                _id: {
                    "nombre": "$Enfermedad",
                    "color": "$color",
                }
                , arboles: { $push: "$arbol" }
                // , data: { $push: "$$ROOT" }
                // , fecha_max: { $max: "$rgDate" }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }




    ], { allowDiskUse: true }
)
// .count()

// data




data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
