[
    {
        "name": "Erradicada",
        "type": "bool",
        "color": "#F8F8F8"
    },
    {
        "name": "Resiembra",
        "type": "bool"
    },
    {
        "name": "Casos nuevos",
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Palma enferma",
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Palmas enfermas casos nuevos",
        "type": "bool",
        "color": "#CC00FF"
    },
    {
        "name": "Palmas enfermas por erradicar",
        "type": "bool",
        "color": "#7f7f80"
    },
    {
        "name": "Palmas en recuperación menor a 16 hojas sanas",
        "type": "bool",
        "color": "#fdfd01"
    },
    {
        "name": "Recuperación vegetativa mayor a 16 hojas sanas",
        "type": "bool",
        "color": "#0070bf"
    },
    {
        "name": "Palmas en recuperación productiva",
        "type": "bool",
        "color": "#ec7d31"
    },
    {
        "name": "Palmas de alta",
        "type": "bool",
        "color": "#0df20d"
    },
    {
        "name": "Doblamiento de corona",
        "type": "bool",
        "color": "#ff0000"
    },
    {
        "name": "Doblamiento de flecha",
        "type": "bool",
        "color": "#931bb4"
    },
    {
        "name": "Cogollo doblado",
        "type": "bool",
        "color": "#00afef"
    },
    {
        "name": "Cogollo podrido",
        "type": "bool",
        "color": "#fd00fe"
    },
    {
        "name": "Solo mantenimiento",
        "type": "bool",
        "color": "#a2785d"
    },
    {
        "name": "Cogollo quebrado",
        "type": "bool",
        "color": "#cbfd33"
    }
]


// [
//     {
//         "name": "Erradicada",
//         "type": "bool",
//         "color": "#F8F8F8"
//     },
//     {
//         "name": "Resiembra",
//         "type": "bool"
//     },
//     {
//         "name": "Casos nuevos",
//         "type": "bool",
//         "color": "#ff0000"
//     },
//     {
//         "name": "Palma enferma",
//         "type": "bool",
//         "color": "#ff0000"
//     },
//
//     //---sin caso
//     {
//         "name": "Palmas enfermas casos nuevos",
//         "type": "bool",
//         "color": "#CC00FF"
//     },
//     {
//         "name": "Palmas enfermas por erradicar",
//         "type": "bool",
//         "color": "#7f7f80"
//     },
//     {
//         "name": "Palmas en recuperación menor a 16 hojas sanas",
//         "type": "bool",
//         "color": "#fdfd01"
//     },
//     {
//         "name": "Recuperación vegetativa mayor a 16 hojas sanas",
//         "type": "bool",
//         "color": "#0070bf"
//     },
//     {
//         "name": "Palmas en recuperación productiva",
//         "type": "bool",
//         "color": "#ec7d31"
//     },
//     {
//         "name": "Palmas de alta",
//         "type": "bool",
//         "color": "#0df20d"
//     },
//     {
//         "name": "Doblamiento de corona",
//         "type": "bool",
//         "color": "#ff0000"
//     },
//
//     //------nuevos
//     //----------
//     {
//         "name": "Doblamiento de flecha",
//         "type": "bool",
//         "color": "#931bb4"
//     },
//     {
//         "name": "Cogollo doblado",
//         "type": "bool",
//         "color": "#00afef"
//     },
//     {
//         "name": "Cogollo podrido",
//         "type": "bool",
//         "color": "#fd00fe"
//     },
//     {
//         "name": "Solo mantenimiento",
//         "type": "bool",
//         "color": "#a2785d"
//     },
//     {
//         "name": "Cogollo quebrado",
//         "type": "bool",
//         "color": "#cbfd33"
//     }
// ]

// [
//     {
//         "name" : "Erradicada",
//         "type" : "bool",
//         "color" : "#F8F8F8"
//     },
//     {
//         "name" : "Resiembra",
//         "type" : "bool"
//     },
//     {
//         "name" : "Casos nuevos",
//         "type" : "bool",
//         "color" : "#FBFF00"
//     },
//     {
//         "name" : "Palma enferma",
//         "type" : "bool",
//         "color" : "#F90A0A"
//     },
//     {
//         "name" : "Palmas enfermas casos nuevos",
//         "type" : "bool",
//         "color" : "#CC00FF"
//     },
//     {
//         "name" : "Palmas enfermas por erradicar",
//         "type" : "bool",
//         "color" : "#AAA7A7"
//     },
//     {
//         "name" : "Palmas en recuperación menor a 16 hojas sanas",
//         "type" : "bool",
//         "color" : "#EBCD07"
//     },
//     {
//         "name" : "Recuperación vegetativa mayor a 16 hojas sanas",
//         "type" : "bool",
//         "color" : "#0004FF"
//     },
//     {
//         "name" : "Palmas en recuperación productiva",
//         "type" : "bool",
//         "color" : "#FF7300"
//     },
//     {
//         "name" : "Palmas de alta",
//         "type" : "bool",
//         "color" : "#6EFF00"
//     },
//     {
//         "name" : "Doblamiento de corona",
//         "type" : "bool",
//         "color" : "#72CCF2"
//     },
//     {
//         "name" : "Doblamiento de flecha",
//         "type" : "bool",
//         "color" : "#B324B5"
//     },
//     {
//         "name" : "Cogollo doblado",
//         "type" : "bool",
//         "color" : "#2FC2D9"
//     },
//     {
//         "name" : "Cogollo podrido",
//         "type" : "bool",
//         "color" : "#EB54D4"
//     },
//     {
//         "name" : "Solo mantenimiento",
//         "type" : "bool",
//         "color" : "#A98047"
//     },
//     {
//         "name" : "Cogollo quebrado",
//         "type" : "bool",
//         "color" : "#EDEF6D"
//     }
// ]