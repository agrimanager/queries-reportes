


//335.460 arboles_enfermedad
//123.675 arboles_enfermedad_huracan

//1.813 = "Inestable por huracan" : "Solo mantenimiento"
var data = db.form_danoporhuracan.aggregate(

    [

        { $unwind: "$Lote.features" }

        , {
        $match: {
            "Lote.features.properties.type": { $eq: "trees" }

            // , "Enfermedad": { $exists: true }

            // ,"Inestable por huracan":{$exists:true}
            // , "Inestable por huracan": "Solo mantenimiento"


            // , "Enfermedad": "Cogollo quebrado"
            // , "Enfermedad": "Cogollo podrido"
            // , "Enfermedad": "Cogollo doblado"
            , "Enfermedad": "Doblamiento de flecha"
        }
    }

        , {
        $addFields: {
            palma: "$Lote.features.properties.name"
        }
    }

        , {
        $group: {
            _id: null
            , ids: { $push: "$palma" }
        }
    }


    ]
    , { allowDiskUse: true }
)

// data

data.forEach(i => {

    db.cartography.updateMany(
        {
            "properties.name": { $in: i.ids }
        },
        {
            $set: {
                "properties.custom.Erradicada": {
                    "type": "bool",
                    "value": false,
                    "color": "#F8F8F8"
                },
                "properties.custom.Resiembra": {
                    "type": "bool",
                    "value": false
                },
                "properties.custom.Palmas enfermas por erradicar": {
                    "type": "bool",
                    "value": false,
                    "color": "#7f7f80"
                },
                "properties.custom.Casos nuevos": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                },
                "properties.custom.Palma enferma": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                },
                "properties.custom.Palmas enfermas casos nuevos": {
                    "type": "bool",
                    "value": false,
                    "color": "#CC00FF"
                },
                "properties.custom.Palmas en recuperación menor a 16 hojas sanas": {
                    "type": "bool",
                    "value": false,
                    "color": "#fdfd01"
                },
                "properties.custom.Recuperación vegetativa mayor a 16 hojas sanas": {
                    "type": "bool",
                    "value": false,
                    "color": "#0070bf"
                },
                "properties.custom.Palmas en recuperación productiva": {
                    "type": "bool",
                    "value": false,
                    "color": "#ec7d31"
                },
                "properties.custom.Palmas de alta": {
                    "type": "bool",
                    "value": false,
                    "color": "#0df20d"
                },
                "properties.custom.Doblamiento de corona": {
                    "type": "bool",
                    "value": false,
                    "color": "#ff0000"
                }


                //---nuevos de huracan
                , "properties.custom.Doblamiento de flecha": {
                    "type": "bool",
                    "value": true,//----------------------true
                    "color": "#931bb4"
                }
                , "properties.custom.Cogollo doblado": {
                    "type": "bool",
                    "value": false,
                    "color": "#00afef"
                }
                , "properties.custom.Cogollo podrido": {
                    "type": "bool",
                    "value": false,
                    "color": "#fd00fe"
                }
                , "properties.custom.Cogollo quebrado": {
                    "type": "bool",
                    "value": false,
                    "color": "#cbfd33"
                }


                , "properties.custom.Solo mantenimiento": {
                    "type": "bool",
                    "value": false,
                    "color": "#a2785d"
                }




            }
        }

    )

})

// , "Enfermedad": "Cogollo quebrado"---
// , "Enfermedad": "Cogollo podrido" ---
// , "Enfermedad": "Cogollo doblado" ---
// , "Enfermedad": "Doblamiento de flecha" ---