db.form_monitoreodeenfermedades.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-05-22T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,

                // "FincaID": ObjectId("5d27cd41995a863de81993e7"), //lapas
                "FincaID": ObjectId("5d27d22e793a4867b305012c"), //semana santa
                // "FincaID": ObjectId("5d1395d3430d802507dfcb4f"), //areno
                // "FincaID": ObjectId("60ba34e5f14ef50dbf240d8c"), //los planes
                // "FincaID": ObjectId("5da0e388cbdafefbc05669bf"), //chalmeca

            }
        },


        //----FILTRO FECHAS Y FINCA
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [

        //                 {
        //                     "$gte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
        //                     ]
        //                 },
        //                 {
        //                     "$lte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
        //                     ]
        //                 }
        //                 , {"$eq": ["$Point.farm", { "$toString": "$FincaID" }]}
        //             ]
        //         }
        //     }
        // },


        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": ["$rgDate", "$Busqueda inicio"]
                        },
                        {
                            "$lte": ["$rgDate", "$Busqueda fin"]
                        }
                        , { "$eq": ["$Point.farm", { "$toString": "$FincaID" }] }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte


        {
            "$addFields": {
                "_id_finca_string": "$Point.farm"
            }
        },




        //----->>>VARIABLES_QUE_NO_SE_USAN
        {
            "$project": {

                "user": 0,
                "Keys": 0,
                "Finca nombre": 0,
                "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
                "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,

                "Formula": 0,
                "uDate": 0,
                "Busqueda inicio": 0,
                "Busqueda fin": 0,
                // "today": 0,
                "FincaID": 0,
                "uid": 0

                , "Point": 0
                , "capture": 0

                , "_id": 0
            }
        },


        // //test
        // {
        //     "$project": {
        //         "_id": 1
        //     }
        // },


        // //----->>>CARTOGRAFIA

        ////VARIABLES_BASES_CARTOGRAFIA
        // { "$unwind": "$Palma.features" },

        // {
        //     "$addFields": {
        //         "palma_nombre": "$Palma.features.properties.name"

        //         , "palma_string_id": { "$toString": "$Palma.features._id" }
        //         , "palma_object_id": { "$toObjectId": "$Palma.features._id" }
        //         , "palma_path": "$Palma.path"
        //     }
        // },

        // {
        //     "$project": {
        //         "Palma": 0
        //     }
        // },




        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },
        {
            "$project": {
                "Palma": 0
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                // ,"Palma": 0
            }
        }


        // //test
        // ,{
        //     "$project": {
        //         "_id": 1
        //     }
        // }



        //----->>>VARIABLES_DE_FORMULARIO
        //19 variables_totales (point y cartografia)
        //17 variables_datos//array (3)//number (1)//string (13)


        //variables_bases
        /*
        	"_id" : ObjectId("646d28130f5400cfd07cbe44"),
        	"supervisor" : "SJ-7544 Nelson Josue Urbina Lacayo",
        	"rgDate" : ISODate("2023-05-23T08:35:32.000-05:00"),
        	"capture" : "M",
        	"finca" : "Chalmeca",
        	"bloque" : "4",
        	"lote" : "4-7",
        	"linea" : "4-7-321",
        	"arbol" : "4-7-321-1"
    	*/



        //-----------ciclo
        , {
            "$lookup": {
                "from": "form_ciclos",
                "as": "data_ciclos",
                "let": {
                    "rgDate": "$rgDate",
                    "farm_id": "$_id_finca_string"
                    , "today": "$today"
                },
                "pipeline": [



                    {
                        "$addFields": {
                            "Fin de Ciclo": {
                                "$cond": {
                                    "if": { "$lt": [{ "$year": "$Fin de Ciclo" }, 2000] },
                                    "then": "$$today",
                                    "else": "$Fin de Ciclo"
                                }
                            }
                        }
                    },


                    //----FILTRO FECHAS Y FINCA
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Point.farm", "$$farm_id"] },
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$rgDate" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio de Ciclo" } } }
                                        ]
                                    },
                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$rgDate" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fin de Ciclo" } } }
                                        ]
                                    }

                                ]
                            }
                        }
                    },

                    {
                        "$limit": 1
                    }


                    , {
                        "$project": {
                            "Ciclo": 1
                        }
                    }


                ]
            }
        }


        , {
            "$unwind": {
                "path": "$data_ciclos",
                "preserveNullAndEmptyArrays": true
            }
        }

        , { "$addFields": { "Ciclo": { "$ifNull": [{ "$toString": "$data_ciclos.Ciclo" }, "sin_ciclo"] } } }


        , {
            "$project": {
                "data_ciclos": 0
                , "today": 0
                , "_id_finca_string": 0
            }
        }






        , {
            "$addFields": {
                "variables_bases": [
                    "_id",
                    "supervisor",
                    "rgDate",
                    "capture",
                    "finca",
                    "bloque",
                    "lote",
                    "linea",
                    "arbol"

                    , "variables_bases"
                    , "Ciclo"
                ]
            }
        }


        //variables_formulario
        , {
            "$addFields": {
                "variables_formulario": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "key": "$$dataKV.k"
                                    , "type": { "$type": "$$dataKV.v" }
                                    // , "value": "$$dataKV.v"
                                    //, "value": ["$$dataKV.v"]
                                    , "value": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$type": "$$dataKV.v" }, "array"] },
                                            "then": "$$dataKV.v",
                                            "else": ["$$dataKV.v"]
                                        }
                                    }

                                }
                            }
                        },
                        "as": "item",
                        "cond": {
                            "$not": { "$in": ["$$item.key", "$variables_bases"] }
                        }
                    }
                }
            }
        }

        , {
            "$project": {
                "variables_bases": 0
            }
        }


        , {
            "$addFields": {
                "variables_formulario": {
                    "$filter": {
                        "input": "$variables_formulario",
                        "as": "item",
                        "cond": {
                            "$not": {
                                "$in": [
                                    "$$item.value",
                                    [
                                        // []
                                        // ,""
                                        // , 0 //!!!!DANGER


                                        []
                                        , [""]
                                        , [0] //!!!!DANGER
                                    ]
                                ]
                            }
                        }
                    }
                }
            }
        }


        , {
            "$unwind": {
                "path": "$variables_formulario",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$unwind": {
                "path": "$variables_formulario.value",
                "preserveNullAndEmptyArrays": false
            }
        }




        //---info fechas
        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        //---info_cartografia_aux
        , {
            "$addFields": {
                "split_arbol": { "$split": [{ "$trim": { "input": "$arbol", "chars": "-" } }, "-"] }
            }
        }


        //
        , {
            "$addFields": {
                "num_linea": { "$arrayElemAt": ["$split_arbol", -2] }
                , "num_arbol": { "$arrayElemAt": ["$split_arbol", -1] }
            }
        }




        //----->>>VARIABLES_FINALES
        , {
            "$project": {
                "Finca": "$finca",

                "Año": "$num_anio",
                "Mes": "$Mes_Txt",
                "Dia": "$num_dia_mes",
                "Fecha": "$Fecha_Txt",
                "Ciclo": "$Ciclo",

                "Lote": "$lote",
                "Linea": "$linea",
                "Linea_num": "$num_linea",
                "Arbol": "$arbol",
                "Arbol_num": "$num_arbol"

                //variables_normalizadas
                , "Descripcion Rango": "$variables_formulario.key"
                , "Enfermedad": "$variables_formulario.value"

                , "Evaluador": "$supervisor"

            }
        }



    ]

)
