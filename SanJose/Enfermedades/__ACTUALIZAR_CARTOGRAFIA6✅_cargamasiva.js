/*
actualizar cartografia desde TABLA TEMPORAL DE CARGA_MASIVA
form_monitoreodeenfermedades_cartografia_cargamasiva
variables:
arbol
variable
*/

//var data = db.getCollection("form_monitoreodeenfermedades_data_2023-07-27_SS").aggregate(
//var data = db.getCollection("form_monitoreodeenfermedades_data_2023-07-27_lapas").aggregate(
var data = db.getCollection("form_monitoreodeenfermedades_cartografia_cargamasiva").aggregate(
    [


        {
            $group: {
                _id: {
                    "nombre": "$variable"
                }

                , arboles: { $push: "$arbol" }
                , cant: { $sum: 1 }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }




    ], { allowDiskUse: true }
)
// .count()

// data




data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
