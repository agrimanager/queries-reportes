

var data = db.form_monitoreodeenfermedades.aggregate(
    [

        // {
        //     $sort: {
        //         "_id": -1
        //     }
        // },

        // //test
        // {
        //     $match: {
        //         "Palma.features.properties.name": "2-52-203-9"
        //     }
        // },



        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5d27cd41995a863de81993e7"), //lapas
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // , {"$eq": ["$Point.farm", { "$toString": "$FincaID" }]}
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            $unwind: "$Palma.features"
        },



        {
            $addFields: {
                arbol: "$Palma.features.properties.name"
            }
        }



        /*
        Casos Nuevos
        Palmas Enfermas
        PC Inestable
        -Palmas Enfermas Por Erradicar
        -Palmas en recuperacion menor a 16 hojas sanas
        -Palma en recuperacion vegetativa mayor a 16 hojas sanas
        -Palmas en recuperacion productiva
        -Palmas De Alta
        */



        , {
            "$match": {
                "Casos Nuevos": { "$exists": true }
                , "Palmas Enfermas": { "$exists": true }
                , "Palmas en recuperacion menor a 16 hojas sanas": { "$exists": true }
                , "Palma en recuperacion vegetativa mayor a 16 hojas sanas": { "$exists": true }
                , "Palmas en recuperacion productiva": { "$exists": true }
                , "Palmas De Alta": { "$exists": true }

                // , "PC Inestable": { "$exists": true }
                // , "Palmas Enfermas Por Erradicar": { "$exists": true }
            }
        }



        , {
            "$addFields": {


                "Palmas en recuperacion menor a 16 hojas sanas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Palmas en recuperacion menor a 16 hojas sanas" }, "array"] },
                        "then": { "$arrayElemAt": ["$Palmas en recuperacion menor a 16 hojas sanas", 0] },
                        "else": "$Palmas en recuperacion menor a 16 hojas sanas"
                    }

                },

                "Palma en recuperacion vegetativa mayor a 16 hojas sanas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Palma en recuperacion vegetativa mayor a 16 hojas sanas" }, "array"] },
                        "then": { "$arrayElemAt": ["$Palma en recuperacion vegetativa mayor a 16 hojas sanas", 0] },
                        "else": "$Palma en recuperacion vegetativa mayor a 16 hojas sanas"
                    }

                },

                "Palmas en recuperacion productiva": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Palmas en recuperacion productiva" }, "array"] },
                        "then": { "$arrayElemAt": ["$Palmas en recuperacion productiva", 0] },
                        "else": "$Palmas en recuperacion productiva"
                    }

                },

                "Palmas De Alta": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Palmas De Alta" }, "array"] },
                        "then": { "$arrayElemAt": ["$Palmas De Alta", 0] },
                        "else": "$Palmas De Alta"
                    }

                }

                ,
                "Palmas Enfermas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Palmas Enfermas" }, "array"] },
                        "then": { "$arrayElemAt": ["$Palmas Enfermas", 0] },
                        "else": "$Palmas Enfermas"
                    }

                }


                ,
                "PC Inestable": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$PC Inestable" }, "array"] },
                        "then": { "$arrayElemAt": ["$PC Inestable", 0] },
                        "else": "$PC Inestable"
                    }

                }


            }
        }





        , {
            "$match": {
                "$expr": {
                    "$or": [
                        { "$ne": ["$Casos Nuevos", ""] }
                        , { "$ne": ["$Palmas Enfermas", ""] }
                        , { "$ne": ["$Palmas en recuperacion menor a 16 hojas sanas", ""] }
                        , { "$ne": ["$Palma en recuperacion vegetativa mayor a 16 hojas sanas", ""] }
                        , { "$ne": ["$Palmas en recuperacion productiva", ""] }
                        , { "$ne": ["$Palmas De Alta", ""] }

                        , { "$ne": ["$Palmas Enfermas Por Erradicar", ""] }
                        , { "$ne": ["$PC Inestable", ""] }
                    ]
                }

            }
        }

        , {
            "$project": {
                // "supervisor": 0,
                "capture": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "Formula": 0
            }
        }


        // //test
        // ,{$limit:100}





        , {
            $group: {
                _id: {
                    "arbol": "$arbol"
                }
                , data: { $push: "$$ROOT" }
                , fecha_max: { $max: "$rgDate" }
            }
        }



        , {
            "$addFields": {
                "data": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.rgDate", "$fecha_max"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // ,{$unwind: "$data"}

        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }




        //datos bool
        , {
            "$addFields": {

                "Palmas Enfermas Por Erradicar": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palmas Enfermas Por Erradicar", ""] }, ""] },
                        "then": false,
                        "else": true
                    }
                },

                "Casos Nuevos": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Casos Nuevos", ""] }, ""] },
                        "then": false,
                        "else": true
                    }
                },


                "Palmas Enfermas": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palmas Enfermas", ""] }, ""] },
                        "then": false,
                        "else": true
                    }
                },


                "Palmas en recuperacion menor a 16 hojas sanas": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palmas en recuperacion menor a 16 hojas sanas", ""] }, ""] },
                        "then": false,
                        "else": true
                    }

                },


                "Palma en recuperacion vegetativa mayor a 16 hojas sanas": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palma en recuperacion vegetativa mayor a 16 hojas sanas", ""] }, ""] },
                        "then": false,
                        "else": true
                    }

                },

                "Palmas en recuperacion productiva": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palmas en recuperacion productiva", ""] }, ""] },
                        "then": false,
                        "else": true
                    }

                },


                "Palmas De Alta": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$Palmas De Alta", ""] }, ""] },
                        "then": false,
                        "else": true
                    }

                }




                ,"PC Inestable": {
                    "$cond": {
                        "if": { "$eq": [{ $ifNull: ["$PC Inestable", ""] }, ""] },
                        "then": false,
                        "else": true
                    }

                }



            }
        }



        //array_data
        , {
            "$addFields": {
                "arra_data": [

                    {
                        nombre: "Casos Nuevos"
                        , valor: "$Casos Nuevos"
                        , color: "#FF0000"
                    },

                    {
                        nombre: "Palmas Enfermas"
                        , valor: "$Palmas Enfermas"
                        , color: "#FF0000"
                    },

                    {
                        nombre: "Palmas Enfermas Por Erradicar"
                        , valor: "$Palmas Enfermas Por Erradicar"
                        , color: "#7F7F80"
                    },
                    {
                        nombre: "Palmas en recuperacion menor a 16 hojas sanas"
                        , valor: "$Palmas en recuperacion menor a 16 hojas sanas"
                        , color: "#FDFD01"
                    },
                    {
                        nombre: "Palma en recuperacion vegetativa mayor a 16 hojas sanas"
                        , valor: "$Palma en recuperacion vegetativa mayor a 16 hojas sanas"
                        , color: "#0070BF"
                    },
                    {
                        nombre: "Palmas en recuperacion productiva"
                        , valor: "$Palmas en recuperacion productiva"
                        , color: "#EC7D31"
                    },
                    {
                        nombre: "Palmas De Alta"
                        , valor: "$Palmas De Alta"
                        , color: "#0DF20D"
                    }

                    ,
                    {
                        nombre: "PC Inestable"
                        , valor: "$PC Inestable"
                        , color: "#B0B418"
                    }

                ]

            }

        }


        , {
            "$addFields": {
                "arra_data": {
                    "$filter": {
                        "input": "$arra_data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.valor", true] }
                    }
                }
            }
        }

        , {
            $unwind: "$arra_data"
        }



        , {
            $group: {
                _id: {
                    "nombre": "$arra_data.nombre",
                    "color": "$arra_data.color",
                }
                , arboles: { $push: "$arbol" }
                // , data: { $push: "$$ROOT" }
                // , fecha_max: { $max: "$rgDate" }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }







    ], { allowDiskUse: true }
)
// .count()

// data




data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "properties.name": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
