//---mapa
db.form_monitoreodeenfermedades.aggregate(
    [

    //---condicion base de exists
    {
        "$match": {
            "Casos Nuevos": { "$exists": true },
            "Palmas Enfermas": { "$exists": true },
            "Recuperacion Vegetativa": { "$exists": true },
            "Recuperacion Productiva": { "$exists": true },
            "Palmas de altas": { "$exists": true }
        }
    }

    //---condicion base de con valor
    , {
        "$match": {
            "$expr": {
                "$or": [
                    { "$ne": ["$Casos Nuevos", ""] },
                    { "$ne": ["$Palmas Enfermas", ""] },
                    { "$ne": ["$Recuperacion Vegetativa", ""] },
                    { "$ne": ["$Recuperacion Productiva", ""] },
                    { "$ne": ["$Palmas de altas", ""] },
                ]
            }

        }
    }


    //=====CARTOGRAFIA

    //--paso1 (cartografia-nombre variable y ids)


    , {
        "$addFields": {
            "variable_cartografia": "$Palma" //🚩editar
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    //--id mapa
    {
        "$addFields": { "elemnq": { "$toObjectId": "$variable_cartografia.features._id" } }
    },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    //--paso2 (cartografia-cruzar informacion)
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    // //--paso3 (cartografia-obtener informacion)

    //--finca
    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    //--bloque
    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    //--lote
    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    //--linea
    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    //--arbol
    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            // "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Palma": 0
            , "Point": 0
            , "Formula": 0
            , "uid": 0
        }
    }


    //=====COLOR
    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$ne": ["$Casos Nuevos", ""] },
                    "then": "#1a73e8",
                    "else": {
                        "$cond": {
                            "if": { "$ne": ["$Palmas Enfermas", ""] },
                            "then": "#ff0000",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$or": [{ "$ne": ["$Recuperacion Vegetativa", ""] }
                                            , { "$ne": ["$Recuperacion Productiva", ""] }]
                                    },
                                    "then": "#ff8000",
                                    "else": "#00FF00"
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$ne": ["$Casos Nuevos", ""] },
                    "then": "Casos Nuevos",
                    "else": {
                        "$cond": {
                            "if": { "$ne": ["$Palmas Enfermas", ""] },
                            "then": "Palmas Enfermas",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$or": [{ "$ne": ["$Recuperacion Vegetativa", ""] }
                                            , { "$ne": ["$Recuperacion Productiva", ""] }]
                                    },
                                    "then": "Recuperacion Vegetativa o Productiva",
                                    "else": "Palmas de altas"
                                }
                            }
                        }
                    }
                }
            }
        }
    },


    //-----PROYECCION FINAL
    {
        "$project": {
            "_id": "$elemnq",
            "idform": "$idorm",
            "type": "Feature",
            "properties": {
                "color": "$color"
                , "rango": "$rango"

                , "bloque": "$bloque"
                , "lote": "$lote"
                , "linea": "$linea"
                , "palma": "$arbol"
            },
            "geometry": "$variable_cartografia.features.geometry"
        }
    }




]

)