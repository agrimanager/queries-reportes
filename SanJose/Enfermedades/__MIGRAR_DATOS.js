var data = db.form_monitoreodeenfermedades_data.aggregate(
    [

        //NOTA: se creo un indice en cartografia de en "properties.name" para optimizar el tiempo
        {
            "$lookup": {
                "from": "cartography",
                "as": "info_cartografia",
                "let": {
                    "arbol": "$Palma"
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$properties.name", "$$arbol"]
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { "$split": [{ "$trim": { "input": "$info_cartografia.path", "chars": "," } }, ","] }
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { $arrayElemAt: ["$id_str_farm", 0] }
            }
        }



        // //test
        // , {
        //     $limit: 100
        // }



        //datos
        , {
            $addFields: {
                item_insert_form:
                {
                    // "_id": ObjectId("64375a8f68c42eb0b7f32e57"),
                    "Palma": {
                        "type": "selection",
                        "features": [
                            {
                                "_id": { $toString: "$info_cartografia._id" },
                                "type": "Feature",
                                "properties": {
                                    "name": "$info_cartografia.properties.name",
                                    "type": "trees",
                                    "production": "$info_cartografia.properties.production",
                                    "status": "$info_cartografia.properties.status",
                                    "custom": "$info_cartografia.properties.custom",
                                },
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": "$info_cartografia.geometry.coordinates"
                                },
                                "path": "$info_cartografia.path"
                            }
                        ],
                        "path": "$info_cartografia.path"
                    },

                    // //variables_formulario
                    "Casos nuevos": "$Casos Nuevos",
                    "Palmas enfermas": "$Palmas Enfermas",
                    //"PC Inestable": "",//[]
                    "PC Inestable": {
                        "$cond": {
                            "if": { "$eq": ["$PC Inestable", ""] },
                            "then": [],
                            "else": ["$PC Inestable"]
                        }
                    },
                    "Palmas en recuperacion menor a 16 hojas sanas": "$Palmas en recuperacion menor a 16 hojas sanas",
                    "Palma en recuperacion vegetativa mayor a 16 hojas sanas": "$Palma en recuperacion vegetativa mayor a 16 hojas sanas",
                    "Palmas en recuperacion productiva": "$Palmas en recuperacion productiva",
                    "Palmas De Alta": "$Palmas De Alta",


                    "Deficiencia de Boro": [],
                    "Daño nuevo de zompopo": [],
                    "Erradicar por": "",
                    "Secamiento basal de hojas intermedias": "",
                    "Otras": "",
                    "Observaciones": "",


                    //otros
                    "Point": {
                        "type": "Feature",
                        "geometry": {
                            "coordinates": "$info_cartografia.geometry.coordinates",
                            "type": "point"
                        },
                        "farm": "$id_str_farm"
                    },
                    "uid": ObjectId("5d138cc4430d802507dfcb25"),
                    "supervisor": "Support team",

                    //!!!!!OJO con fechas del MES
                    rgDate: {
                        $dateFromString: {
                            dateString: {
                                $concat: ["2023-0", "$Mes", "-", "$Dia"]
                            }
                            , format: "%Y-%m-%d"
                            , timezone: "America/Managua"
                        }
                    },
                    uDate: new Date,
                    "capture": "W"
                },

            }

        }




        , {
            "$project": {
                "item_insert_form": 1
            }
        }


    ], { allowDiskUse: true }
)
// .count()


// data

//--BULK INSERT
var bulk = db.form_monitoreodeenfermedades.initializeUnorderedBulkOp();

data.forEach(i => {

    bulk.insert(i.item_insert_form);
})


bulk.execute();


/*
// collection: form_monitoreodeenfermedades
{
	"_id" : ObjectId("647e8465019b214afdceda45"),
	"Palma" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5da0e282b74d2f472e6abec0",
				"type" : "Feature",
				"properties" : {
					"name" : "1-26-253-7",
					"type" : "trees",
					"production" : true,
					"status" : true,
					"custom" : {
						"color" : {
							"type" : "color",
							"value" : "#39AE37"
						}
					},
					"_id" : "5da0e282b74d2f472e6abec0",
					"path" : ",5d1395d3430d802507dfcb4f,5da0e23eb74d2f472e6a23b8,5da0e23fb74d2f472e6a23cf,5da0e24ab74d2f472e6a2857,"
				},
				"geometry" : {
					"type" : "Point",
					"coordinates" : [
						-84.224296454109,
						12.209541209555
					]
				},
				"path" : ",5d1395d3430d802507dfcb4f,5da0e23eb74d2f472e6a23b8,5da0e23fb74d2f472e6a23cf,5da0e24ab74d2f472e6a2857,"
			}
		],
		"path" : ",5d1395d3430d802507dfcb4f,5da0e23eb74d2f472e6a23b8,5da0e23fb74d2f472e6a23cf,5da0e24ab74d2f472e6a2857,"
	},
	"Casos nuevos" : "Pudricion de cogollo",
	"Palmas enfermas" : "",
	"Palmas en recuperacion menor a 16 hojas sanas" : "",
	"Palma en recuperacion vegetativa mayor a 16 hojas sanas" : "",
	"Palmas en recuperacion productiva" : "",
	"Palmas De Alta" : "",
	"Erradicar por" : "",
	"Secamiento basal de hojas intermedias" : "",
	"Otras" : "",
	"Observaciones" : "xxx",
	"Point" : {
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-84.26290056752062,
				12.175703322931682
			]
		},
		"farm" : "5d1395d3430d802507dfcb4f"
	},
	"PC Inestable" : "",
	"Deficiencia de Boro" : "",
	"Daño nuevo de zompopo" : "",
	"uid" : ObjectId("5d138cc4430d802507dfcb25"),
	"supervisor" : "Support team",
	"rgDate" : ISODate("2023-06-05T19:57:09.391-05:00"),
	"uDate" : ISODate("2023-06-05T19:57:09.391-05:00"),
	"capture" : "W"
}

*/
