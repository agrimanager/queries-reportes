

var data = db.form_monitoreodeenfermedades.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,

                // "FincaID": ObjectId("5d27cd41995a863de81993e7"), //lapas
                // "FincaID": ObjectId("5d27d22e793a4867b305012c"), //semana santa
                // "FincaID": ObjectId("5d1395d3430d802507dfcb4f"), //areno
                // "FincaID": ObjectId("60ba34e5f14ef50dbf240d8c"), //los planes
                // "FincaID": ObjectId("5da0e388cbdafefbc05669bf"), //chalmeca

                "fincas_id_str": [
                    "5d27cd41995a863de81993e7", //lapas
                    "5d27d22e793a4867b305012c", //semana santa
                    // "5d1395d3430d802507dfcb4f", //areno
                    // "60ba34e5f14ef50dbf240d8c", //los planes
                    "5da0e388cbdafefbc05669bf", //chalmeca

                ]
            }
        },
        //----FILTRO FECHAS Y FINCA

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": ["$rgDate", "$Busqueda inicio"]
                        },
                        {
                            "$lte": ["$rgDate", "$Busqueda fin"]
                        }
                        //, { "$eq": ["$Point.farm", { "$toString": "$FincaID" }] }
                        , { "$in": ["$Point.farm", "$fincas_id_str"] }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            $unwind: "$Palma.features"
        },



        {
            $addFields: {
                arbol: "$Palma.features.properties.name"
            }
        }



        //----->>>VARIABLES_QUE_NO_SE_USAN
        , {
            "$project": {

                "user": 0,
                "Keys": 0,
                "Finca nombre": 0,
                "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
                "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,

                "Formula": 0,
                "uDate": 0,
                "Busqueda inicio": 0,
                "Busqueda fin": 0,
                "today": 0,
                "FincaID": 0,
                "uid": 0

                , "Point": 0
                , "capture": 0

                , "_id": 0
                , "Palma": 0
            }
        }


        , {
            $group: {
                _id: {
                    "arbol": "$arbol"
                }
                , data: { $push: "$$ROOT" }
                , fecha_max: { $max: "$rgDate" }
            }
        }



        , {
            "$addFields": {
                "data": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.rgDate", "$fecha_max"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // ,{$unwind: "$data"}

        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }


        //new
        // , {
        //     "$addFields": {
        //         "variables_bases": [
        //             "_id",
        //             "supervisor",
        //             "rgDate",
        //             "capture",
        //             "finca",
        //             "bloque",
        //             "lote",
        //             "linea",
        //             "arbol"

        //             , "variables_bases"
        //             , "Ciclo"
        //         ]
        //     }
        // }

        , {
            "$addFields": {
                "variables_cartografia": [
                    "Casos nuevos",  //--rep
                    "Casos Nuevos",//--rep
                    "Erradicada por PC",
                    "Erradicar por",
                    "Espacio vacio",
                    "Huracan",
                    "Palma en recuperacion vegetativa mayor a 16 hojas sanas",
                    "Palma sana",
                    "Palmas De Alta",
                    "Palmas en recuperacion menor a 16 hojas sanas",
                    "Palmas en recuperacion productiva",
                    "Palmas enfermas",//--rep
                    "Palmas Enfermas",//--rep
                    "PC Inestable",

                ]
            }
        }



        //variables_formulario
        , {
            "$addFields": {
                "variables_formulario": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "key": "$$dataKV.k"
                                    , "type": { "$type": "$$dataKV.v" }
                                    // , "value": "$$dataKV.v"
                                    //, "value": ["$$dataKV.v"]
                                    , "value": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$type": "$$dataKV.v" }, "array"] },
                                            "then": "$$dataKV.v",
                                            "else": ["$$dataKV.v"]
                                        }
                                    }

                                }
                            }
                        },
                        "as": "item",
                        "cond": {
                            //"$not": { "$in": ["$$item.key", "$variables_bases"] }
                            "$in": ["$$item.key", "$variables_cartografia"]
                        }
                    }
                }
            }
        }

        , {
            "$project": {
                // "variables_bases": 0
                "variables_cartografia": 0

            }
        }


        , {
            "$addFields": {
                "variables_formulario": {
                    "$filter": {
                        "input": "$variables_formulario",
                        "as": "item",
                        "cond": {
                            "$not": {
                                "$in": [
                                    "$$item.value",
                                    [
                                        // []
                                        // ,""
                                        // , 0 //!!!!DANGER


                                        []
                                        , [""]
                                        , [0] //!!!!DANGER
                                    ]
                                ]
                            }
                        }
                    }
                }
            }
        }


        , {
            "$unwind": {
                "path": "$variables_formulario",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$unwind": {
                "path": "$variables_formulario.value",
                "preserveNullAndEmptyArrays": false
            }
        }


        // //test
        // , {
        //     $group: {
        //         _id: "$variables_formulario.key"
        //         // , data: { $push: "$$ROOT" }
        //         , fecha_max: { $max: "$rgDate" }
        //         , cant: { $sum: 1 }
        //     }
        // }


        , {
            $group: {
                _id: "$arbol"
                , cant: { $sum: 1 }
                , data: { $push: "$$ROOT" }
            }
        }

        // ,{
        //     $sort:{
        //         cant:-1
        //     }
        // }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // ,{$unwind: "$data"}

        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }

        //arreglo casos nuevos
        , {
            "$addFields": {
                "variables_formulario.key": {
                    "$cond": {
                        "if": { "$eq": ["$variables_formulario.key", "Casos Nuevos"] },
                        "then": "Casos nuevos",
                        "else": "$variables_formulario.key"
                    }
                }
            }
        }

        //arreglo palmas enfermas
        , {
            "$addFields": {
                "variables_formulario.key": {
                    "$cond": {
                        "if": { "$eq": ["$variables_formulario.key", "Palmas Enfermas"] },
                        "then": "Palmas enfermas",
                        "else": "$variables_formulario.key"
                    }
                }
            }
        }


        // //test
        // , {
        //     $group: {
        //         _id: "$variables_formulario.key"
        //         // , data: { $push: "$$ROOT" }
        //         // , fecha_max: { $max: "$rgDate" }
        //         , cant: { $sum: 1 }
        //     }
        // }





        , {
            $group: {
                _id: {
                    "nombre": "$variables_formulario.key"
                }

                // , data: { $push: "$$ROOT" }
                // , fecha_max: { $max: "$rgDate" }

                , arboles: { $push: "$arbol" }

                , cant: { $sum: 1 }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }




    ], { allowDiskUse: true }
)
// .count()

data




// data.forEach(i => {


//     //1) borrar propiedades old
//     db.cartography.updateMany(
//         {
//             "properties.name": {
//                 $in: i.arboles
//             }
//         },
//         {
//             $set: {
//                 "properties.custom": {}
//             }
//         }

//     )



//     //2) agregar propiedad new
//     db.cartography.updateMany(
//         {
//             "properties.name": {
//                 $in: i.arboles
//             }
//         },
//         {
//             $set: {
//                 //new
//                 [i.variable_custom]: {
//                     "type": "bool",
//                     "value": true
//                 }


//             }
//         }

//     )

// })
