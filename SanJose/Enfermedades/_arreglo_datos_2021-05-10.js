

//---tcikets  892 y 893

//NOTA!!!: Eliminar la opcion del maestro "Lista de enfermedades"


//========enfermedades

//--ver
db.form_monitoreodeenfermedades.aggregate(

    {
        $group: {
            // _id: "$Otras"
            _id: "$Nombre de la Enfermedad"
            , cantidad: { $sum: 1 }
        }
    }

)

//--editar
db.form_monitoreodeenfermedades.updateMany(
    {
        // "Nombre de la Enfermedad": "palmas por erradicar"
        // "Nombre de la Enfermedad": "espacios vacios"
        "Nombre de la Enfermedad": "anillo rojo"
    },
    {
        $set: {
            "Nombre de la Enfermedad": "",
            // "Otras": "Palmas Erradicadas"
            // "Otras": "Espacios vacios"
            "Otras": "Anillo rojo"
        }
    }

)








//========tratamientos

//--ver
db.form_monitoreoytratamientodeenfermedades.aggregate(

    {
        $group: {
            // _id: "$Otras"
            _id: "$Nombre de la enfermedad"
            , cantidad: { $sum: 1 }
        }
    }

)


//--editar



//--editar
db.form_monitoreoytratamientodeenfermedades.updateMany(
    {
        // "Nombre de la enfermedad": {$in:["pudricion basal humeda","pudricion basal seca"]}
        // "Nombre de la enfermedad": {$in:["espacios vacios"]}
        // "Nombre de la enfermedad": {$in:["muerta por rayo"]}
        "Nombre de la enfermedad": {$in:["anillo rojo"]}

    },
    {
        $set: {
            "Nombre de la enfermedad": "",
            // "Otras": "Pudricion basal"
            // "Otras": "Espacios vacios"
            // "Otras": "Muerte por rayo"
            "Otras": "Anillo rojo"
        }
    }

)
