//---mapa
db.form_monitoreodeenfermedades.aggregate(
    [
        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        //FILTRO FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$eq": ["$Point.farm", "5d1395d3430d802507dfcb4f"] //areno
                        }
                    ]
                }
            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------



        {
            "$addFields": { "Cartography": "$Palma" }
        },
        {
            "$unwind": "$Cartography.features"
        },
        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0,
                "lote_info": 0
            }
        },


        {
            "$replaceRoot": {
                "newRoot": "$$ROOT"
            }
        },


        {
            "$addFields": {
                "data_plaga": [
                    {
                        "type": "Palmas en recuperacion menor a 16 hojas sanas",
                        "value": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Palmas en recuperacion menor a 16 hojas sanas" }, "array"] },
                                "then": { "$arrayElemAt": ["$Palmas en recuperacion menor a 16 hojas sanas", 0] },
                                "else": "$Palmas en recuperacion menor a 16 hojas sanas"
                            }
                        }
                    },
                    {
                        "type": "Palma en recuperacion vegetativa mayor a 16 hojas sanas",
                        "value": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Palma en recuperacion vegetativa mayor a 16 hojas sanas" }, "array"] },
                                "then": { "$arrayElemAt": ["$Palma en recuperacion vegetativa mayor a 16 hojas sanas", 0] },
                                "else": "$Palma en recuperacion vegetativa mayor a 16 hojas sanas"
                            }
                        }
                    },
                    {
                        "type": "Palmas en recuperacion productiva",
                        "value": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Palmas en recuperacion productiva" }, "array"] },
                                "then": { "$arrayElemAt": ["$Palmas en recuperacion productiva", 0] },
                                "else": "$Palmas en recuperacion productiva"
                            }
                        }
                    },
                    {
                        "type": "Palmas De Alta",
                        "value": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Palmas De Alta" }, "array"] },
                                "then": { "$arrayElemAt": ["$Palmas De Alta", 0] },
                                "else": "$Palmas De Alta"
                            }
                        }
                    },
                    {
                        "type": "Casos Nuevos",
                        "value": "$Casos Nuevos"
                    },
                    {
                        "type": "Palmas Enfermas",
                        "value": "$Palmas Enfermas"
                    },
                    {
                        "type": "Palmas Enfermas Casos Nuevos",
                        "value": "$Palmas Enfermas Casos Nuevos"
                    },
                    {
                        "type": "Palmas Enfermas Por Erradicar",
                        "value": "$Palmas Enfermas Por Erradicar"
                    },
                    {
                        "type": "Doblamiento De Corona",
                        "value": "$Doblamiento De Corona"
                    },
                    {
                        "type": "Secamiento basal de hojas intermedias",
                        "value": "$Secamiento basal de hojas intermedias"
                    }
                ]
            }
        },

        {
            "$project": {
                "finca": 1,
                "lote": 1,
                "bloque": 1,
                "data_plaga": 1,
                "elemnq": 1,
                "formid": 1,
                "Cartography": 1
            }
        },

        {
            "$addFields": {
                "data_plaga": {
                    "$filter": {
                        "input": "$data_plaga",
                        "as": "item",
                        "cond": {
                            "$and": [
                                { "$eq": [{ "$type": "$$item.value" }, "string"] },
                                { "$ne": ["$$item.value", ""] }
                            ]
                        }
                    }
                }
            }
        },
        { "$unwind": "$data_plaga" },



        {
            "$addFields": {
                "casos": "$data_plaga.type"
            }
        },



        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$casos", "Casos Nuevos"] },
                                "then": "#9801fe"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas Enfermas"] },
                                "then": "#ff0000"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas Enfermas Casos Nuevos"] },
                                "then": "#fd514d"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas Enfermas Por Erradicar"] },
                                "then": "#808080"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas en recuperacion menor a 16 hojas sanas"] },
                                "then": "#ffff00"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palma en recuperacion vegetativa mayor a 16 hojas sanas"] },
                                "then": "#0072c6"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas en recuperacion productiva"] },
                                "then": "#ffc100"
                            },
                            {
                                "case": { "$eq": ["$casos", "Palmas De Alta"] },
                                "then": "#00ff00"
                            },
                            {
                                "case": { "$eq": ["$casos", "Doblamiento De Corona"] },
                                "then": "#02aff3"
                            },
                            {
                                "case": { "$eq": ["$casos", "Secamiento basal de hojas intermedias"] },
                                "then": "#b4af1f"
                            }
                        ],
                        "default": null
                    }
                }
            }
        },

        {
            "$match": {
                "color": { "$nin": ["", null] }
            }
        },

        {
            $group: {
                _id: "$casos"
                , cantidad: { $sum: 1 }
            }
        }



        // {
        //     "$project": {
        //         "_id": "$elemnq",
        //         "idform": "$idform",
        //         "type": "Feature",
        //         "properties": {
        //             "bloque": "$bloque",
        //             "lote": "$lote",
        //             "casos": "$casos",

        //             "color": "$color"
        //         },
        //         "geometry": "$Cartography.features.geometry"
        //     }
        // }
    ]

)
