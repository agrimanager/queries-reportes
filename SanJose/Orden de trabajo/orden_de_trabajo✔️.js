[


    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Lote": 0
            , "Point": 0
            , "uid": 0


            , "Salida de producto fitosanitario a campo": 0
            , "Retorno de producto fitosanitario a campo": 0

        }
    }



    , {
        "$project": {


            "finca": "$finca",
            "bloque": "$bloque",
            "lote": "$lote",
            "linea": "$linea",
            "arbol": "$arbol"


            , "Lotes": { "$toString": { "$ifNull": ["$Lotes", ""] } }
            , "Lotes aplicados": { "$toString": { "$ifNull": ["$Lotes aplicados", ""] } }


            , "Actividad": "$Actividad"
            , "Equipo": "$Equipo"
            , "N de personas": "$N de personas",



            "Orthene": { "$toDouble": { "$ifNull": ["$Orthene", 0] } },
            "Zompot": { "$toDouble": { "$ifNull": ["$Zompot", 0] } },
            "Mirex": { "$toDouble": { "$ifNull": ["$Mirex", 0] } },
            "Chuspis": { "$toDouble": { "$ifNull": ["$Chuspis", 0] } },
            "keltfly": { "$toDouble": { "$ifNull": ["$keltfly", 0] } },
            "Delthroi": { "$toDouble": { "$ifNull": ["$Delthroi", 0] } },
            "Fipronil": { "$toDouble": { "$ifNull": ["$Fipronil", 0] } },
            "Bacteri": { "$toDouble": { "$ifNull": ["$Bacteri", 0] } },
            "Corrector de ph": { "$toDouble": { "$ifNull": ["$Corrector de ph", 0] } },
            "Formula": {
                "$cond": {
                    "if": {
                        "$eq": ["$Formula", ""]
                    },
                    "then": 0,
                    "else": { "$toDouble": { "$ifNull": ["$Formula", 0] } }
                }
            },
            "Silicio": { "$toDouble": { "$ifNull": ["$Silicio", 0] } },
            "Boro": { "$toDouble": { "$ifNull": ["$Boro", 0] } },
            "Detergente": { "$toDouble": { "$ifNull": ["$Detergente", 0] } },
            "Aceite Vegetal": { "$toDouble": { "$ifNull": ["$Aceite Vegetal", 0] } },
            "Bt": { "$toDouble": { "$ifNull": ["$Bt", 0] } },
            "Bauveria Bassiana": { "$toDouble": { "$ifNull": ["$Bauveria Bassiana", 0] } },
            "Racumin": { "$toDouble": { "$ifNull": ["$Racumin", 0] } },
            "Propico": { "$toDouble": { "$ifNull": ["$Propico", 0] } },
            "Zapicol": { "$toDouble": { "$ifNull": ["$Zapicol", 0] } },





            "Orthene 2": { "$toDouble": { "$ifNull": ["$Orthene 2", 0] } },
            "Zompot 2": { "$toDouble": { "$ifNull": ["$Zompot 2", 0] } },
            "Mirex 2": { "$toDouble": { "$ifNull": ["$Mirex 2", 0] } },
            "Chuspis 2": { "$toDouble": { "$ifNull": ["$Chuspis 2", 0] } },
            "keltfly 2": { "$toDouble": { "$ifNull": ["$keltfly 2", 0] } },
            "Delthroi 2": { "$toDouble": { "$ifNull": ["$Delthroi 2", 0] } },
            "Fipronil 2": { "$toDouble": { "$ifNull": ["$Fipronil 2", 0] } },
            "Bacteri 2": { "$toDouble": { "$ifNull": ["$Bacteri 2", 0] } },
            "Corrector de ph 2": { "$toDouble": { "$ifNull": ["$Corrector de ph 2", 0] } },
            "Formula 2": { "$toDouble": { "$ifNull": ["$Formula 2", 0] } },
            "Silicio 2": { "$toDouble": { "$ifNull": ["$Silicio 2", 0] } },
            "Boro 2": { "$toDouble": { "$ifNull": ["$Boro 2", 0] } },
            "Detergente 2": { "$toDouble": { "$ifNull": ["$Detergente 2", 0] } },
            "Aceite Vegetal 2": { "$toDouble": { "$ifNull": ["$Aceite Vegetal 2", 0] } },
            "Bt 2": { "$toDouble": { "$ifNull": ["$Bt 2", 0] } },
            "Bauveria Bassiana 2": { "$toDouble": { "$ifNull": ["$Bauveria Bassiana 2", 0] } },
            "Racumin 2": { "$toDouble": { "$ifNull": ["$Racumin 2", 0] } },
            "Propico 2": { "$toDouble": { "$ifNull": ["$Propico 2", 0] } },
            "Zapicol 2": { "$toDouble": { "$ifNull": ["$Zapicol 2", 0] } },




            "N de palma": { "$toDouble": { "$ifNull": ["$N de palma", 0] } },
            "N de orificio": { "$toDouble": { "$ifNull": ["$N de orificio", 0] } },
            "m2": { "$toDouble": { "$ifNull": ["$m2", 0] } },
            "Ha": { "$toDouble": { "$ifNull": ["$Ha", 0] } },
            "N de trampa": { "$toDouble": { "$ifNull": ["$N de trampa", 0] } },

            "Nombre del supervisor": { "$toString": { "$ifNull": ["$Nombre del supervisor", ""] } },
            "Nombre del bodeguero": { "$toString": { "$ifNull": ["$Nombre del bodeguero", ""] } },
            "Observacion": { "$toString": { "$ifNull": ["$Observacion", ""] } },
            "supervisor": { "$toString": { "$ifNull": ["$supervisor", ""] } }

            ,"rgDate2":"$rgDate"


        }
    }





]
