var data = db.getCollection("form_monitoreodeenfermedades_data_2023-07-21").aggregate(
    [

        //NOTA: se creo un indice en cartografia de en "properties.name" para optimizar el tiempo
        {
            "$lookup": {
                "from": "cartography",
                "as": "info_cartografia",
                "let": {
                    "arbol": "$codigo_arbol"
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$properties.name", "$$arbol"]
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { "$split": [{ "$trim": { "input": "$info_cartografia.path", "chars": "," } }, ","] }
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { $arrayElemAt: ["$id_str_farm", 0] }
            }
        }



        // //test
        // , {
        //     $limit: 100
        // }




        //datos
        , {
            $addFields: {
                item_insert_form:
                {
                    // "_id": ObjectId("64375a8f68c42eb0b7f32e57"),
                    "Palma": {
                        "type": "selection",
                        "features": [
                            {
                                "_id": { $toString: "$info_cartografia._id" },
                                "type": "Feature",
                                "properties": {
                                    "name": "$info_cartografia.properties.name",
                                    "type": "trees",
                                    "production": "$info_cartografia.properties.production",
                                    "status": "$info_cartografia.properties.status",
                                    "custom": "$info_cartografia.properties.custom",
                                },
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": "$info_cartografia.geometry.coordinates"
                                },
                                "path": "$info_cartografia.path"
                            }
                        ],
                        "path": "$info_cartografia.path"
                    },

                    //     //variables
                    //     // "Formula" : "",
                    // 	"PC Inestable" : [ ],
                    // 	    "Hojas dañadas" : 0,
                    // 	    "Deficiencia de Boro" : [ ],
                    // 	    "Daño nuevo de zompopo" : [ ],
                    // 	    "Observaciones" : "",
                    //     "Huracan" : "",
                    // 	"Palmas en recuperacion menor a 16 hojas sanas" : "",
                    // 	"Palma en recuperacion vegetativa mayor a 16 hojas sanas" : "",
                    // 	"Palmas en recuperacion productiva" : "",
                    // 	"Palmas De Alta" : "",
                    // 	"Erradicar por" : "",
                    // 	"Erradicada por PC" : "",
                    // 	"Espacio vacio" : "",
                    // 	"Palma sana" : "",
                    // 	    "Otras" : "",
                    // 	"Casos nuevos" : "",
                    // 	"Palmas enfermas" : "",

                    //variables
                    // "Formula" : "",
                    "PC Inestable": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "PC Inestable"] },
                            "then": ["$valor"],
                            "else": []
                        }
                    },
                    "Hojas dañadas": 0,
                    "Deficiencia de Boro": [],
                    "Daño nuevo de zompopo": [],
                    "Observaciones": "",
                    "Huracan": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Huracan"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palmas en recuperacion menor a 16 hojas sanas": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palmas en recuperacion menor a 16 hojas sanas"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palma en recuperacion vegetativa mayor a 16 hojas sanas": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palma en recuperacion vegetativa mayor a 16 hojas sanas"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palmas en recuperacion productiva": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palmas en recuperacion productiva"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palmas De Alta": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palmas De Alta"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Erradicar por": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Erradicar por"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Erradicada por PC": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Erradicada por PC"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Espacio vacio": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Espacio vacio"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palma sana": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palma sana"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Otras": "",
                    "Casos nuevos": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Casos Nuevos"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },
                    "Palmas enfermas": {
                        "$cond": {
                            "if": { "$eq": ["$variable", "Palmas enfermas"] },
                            "then": "$valor",
                            "else": ""
                        }
                    },

                    //otros
                    "Point": {
                        "type": "Feature",
                        "geometry": {
                            "coordinates": "$info_cartografia.geometry.coordinates",
                            "type": "point"
                        },
                        "farm": "$id_str_farm"
                    },
                    "uid": ObjectId("5d138cc4430d802507dfcb25"),
                    "supervisor": "Support team",


                    rgDate: "$fecha",
                    uDate: new Date,
                    "capture": "W"
                },

            }

        }



        , {
            "$project": {
                "item_insert_form": 1
            }
        }


    ], { allowDiskUse: true }
)
// .count()


// data

//--BULK INSERT
var bulk = db.form_monitoreodeenfermedades.initializeUnorderedBulkOp();

data.forEach(i => {

    bulk.insert(i.item_insert_form);
})


bulk.execute();



/*

// collection: form_monitoreodeenfermedades
{
	"_id" : ObjectId("64a893cb10abc958429682e7"),
	"Palma" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5f1294be549cc50dd648b4fc",
				"type" : "Feature",
				"path" : ",5d27d22e793a4867b305012c,5da0e7590a3904ab29fe4e4a,5f124acc549cc50dd646ebc1,5f1258d0549cc50dd647851b,",
				"properties" : {
					"type" : "trees",
					"name" : "02-32-201-1",
					"production" : true,
					"status" : true,
					"order" : 1,
					"custom" : {
						"Palmas en recuperacion menor a 16 hojas sanas" : {
							"type" : "bool",
							"value" : true
						},
						"color" : {
							"type" : "color",
							"value" : "#FDFD01"
						}
					}
				},
				"geometry" : {
					"type" : "Point",
					"coordinates" : [
						-84.1924258171726,
						12.2365088626047
					]
				}
			}
		],
		"path" : ",5d27d22e793a4867b305012c,5da0e7590a3904ab29fe4e4a,5f124acc549cc50dd646ebc1,5f1258d0549cc50dd647851b,"
	},
	"Formula" : "",
	"PC Inestable" : [ ],
	"Hojas dañadas" : 0,
	"Deficiencia de Boro" : [ ],
	"Daño nuevo de zompopo" : [ ],
	"Observaciones" : "Pudrición de estipes humeda",
	"Point" : {
		"farm" : "5d27d22e793a4867b305012c",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-84.1924258171726,
				12.2365088626047
			]
		}
	},
	"Huracan" : "",
	"Palmas en recuperacion menor a 16 hojas sanas" : "",
	"Palma en recuperacion vegetativa mayor a 16 hojas sanas" : "",
	"Palmas en recuperacion productiva" : "",
	"Palmas De Alta" : "",
	"Erradicar por" : "",
	"Erradicada por PC" : "",
	"Espacio vacio" : "",
	"Palma sana" : "",
	"Otras" : "",
	"Casos nuevos" : "",
	"Palmas enfermas" : "",
	"uid" : ObjectId("5d138cc4430d802507dfcb25"),
	"supervisor" : "SJ-5829 Maria Fernanda Diaz",
	"rgDate" : ISODate("2023-07-07T09:41:18.000-05:00"),
	"uDate" : ISODate("2023-07-07T09:41:18.000-05:00"),
	"capture" : "M"
}


*/
