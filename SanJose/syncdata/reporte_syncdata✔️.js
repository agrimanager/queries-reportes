[


      { "$limit": 1 },
      {
          "$lookup": {
              "from": "syncdata",
              "as": "data",
              "let": {
                  "filtro_fecha_inicio": "$Busqueda inicio",
                  "filtro_fecha_fin": "$Busqueda fin"
              },

              "pipeline": [

                  {
                      "$project": {
                          "supervisor sincronizacion": "$syncInfo.supervisor",
                          "app version": "$syncInfo.version",
                          "cantidad_censos": { "$size": "$Forms" },
                          "rgDate": "$rgDate"
                      }
                  }

              ]
          }
      }


      , {
          "$project":
          {
              "datos": {
                  "$concatArrays": [
                      "$data"
                      , []
                  ]
              }
          }
      }

      , { "$unwind": "$datos" }
      , { "$replaceRoot": { "newRoot": "$datos" } }


  ]
