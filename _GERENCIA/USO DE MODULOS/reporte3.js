db.users.aggregate(
    [


        //=====BASE0 ---- inyeccion de variables (❌ BORRAR)
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date
            }
        },
        //----------------------------------------------------------------

        //============================================
        //============================================
        //=====BASE1  ---- variables principal_concat
        { "$limit": 1 },
        {
            "$addFields": {
                "data_final": []
            }
        }
        , {
            "$addFields": {
                "user_timezone": "$timezone"
            }
        }

        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "employees",
                "as": "data",
                "let": {},
                "pipeline": [

                ]
            }
        }

        , {
            "$addFields": {
                "size_data": { $size: "$data" }
            }
        }

        , {
            "$project": {
                "num_empleados": "$size_data"

                //----variables bases
                , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
                , "Busqueda fin": "$Busqueda fin"               //--filtro_fecha2
                , "user_timezone": "$user_timezone",         //--filtro_timezone
            }
        }



        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "employees",
                "as": "num_empleados_activos",
                "let": {},
                "pipeline": [

                    {
                        $match: {
                            "status": true
                        }
                    }

                ]
            }
        }

        , {
            "$addFields": {
                "num_empleados_activos": { $size: "$num_empleados_activos" }
            }
        }


        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "employees",
                "as": "num_empleados_activos_llave",
                "let": {},
                "pipeline": [

                    {
                        $match: {
                            "status": true
                            , "onlineAccess": true
                        }
                    }

                ]
            }
        }

        , {
            "$addFields": {
                "num_empleados_activos_llave": { $size: "$num_empleados_activos_llave" }
            }
        }


        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "forms",
                "as": "num_formularios",
                "let": {},
                "pipeline": [
                ]
            }
        }

        , {
            "$addFields": {
                "num_formularios": { $size: "$num_formularios" }
            }
        }


        //logs
        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "logs",
                "as": "data_logs_login",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                    , "user_timezone": "$user_timezone"         //--filtro_timezone
                },
                "pipeline": [

                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                    //----------------------------------------------------------------


                    //---info fechas
                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$rgDate" } },
                            "num_mes": { "$month": { "date": "$rgDate" } }
                        }
                    }


                    , {
                        $group: {
                            _id: {
                                "num_anio": "$num_anio",
                                "num_mes": "$num_mes"

                                , "supervisor": "$log.name"

                            }

                            , cantidad: { $sum: 1 }
                        }
                    }


                    , {
                        $group: {
                            _id: {
                                "num_anio": "$_id.num_anio",
                                "num_mes": "$_id.num_mes"

                                // ,"supervisor":"$log.name"

                            }

                            , cantidad: { $sum: 1 }
                        }
                    }



                ]
            }
        }


        , { $unwind: "$data_logs_login" }


        , {
            "$addFields": {
                "num_anio": "$data_logs_login._id.num_anio"
                , "num_mes": "$data_logs_login._id.num_mes"

                , "num_empleados_login_web": "$data_logs_login.cantidad"
            }
        }


        , {
            "$project": {
                "data_logs_login": 0
            }
        }


        //syncdata
        //============================================
        //=====BASE2_i  ---- data
        , {
            "$lookup": {
                "from": "syncdata",
                "as": "data_syncdata",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                    , "user_timezone": "$user_timezone"         //--filtro_timezone

                    , "num_anio": "$num_anio"
                    , "num_mes": "$num_mes"
                },
                "pipeline": [

                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "$$user_timezone" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                    //----------------------------------------------------------------


                    , {
                        "$project": {
                            "Forms": 0
                            , "Task": 0
                            , "Cartography": 0
                            , "Employees": 0
                            , "PropertiesFeature": 0
                            , "Assistances": 0
                            , "newFeatures": 0
                        }
                    }


                    //---info fechas
                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$rgDate" } },
                            "num_mes": { "$month": { "date": "$rgDate" } }
                        }
                    }


                    //----filtro de fechas2
                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$num_anio", "$$num_anio"] },
                                    { "$eq": ["$num_mes", "$$num_mes"] }
                                ]
                            }
                        }
                    }
                    //----------------------------------------------------------------




                    , {
                        $group: {
                            _id: {
                                "num_anio": "$num_anio",
                                "num_mes": "$num_mes"

                                , "supervisor": "$syncInfo.supervisor"

                            }

                            , cantidad: { $sum: 1 }
                        }
                    }


                    , {
                        $group: {
                            _id: {
                                "num_anio": "$_id.num_anio",
                                "num_mes": "$_id.num_mes"

                                // ,"supervisor":"$log.name"

                            }

                            , cantidad_empleados_sincronizacion: { $sum: 1 }
                            , cantidad_sincronizaciones: { $sum: "$cantidad" }
                        }
                    }



                ]
            }
        }


        , { $unwind: "$data_syncdata" }

        , {
            "$addFields": {
                "cantidad_empleados_sincronizacion": "$data_syncdata.cantidad_empleados_sincronizacion"
                , "cantidad_sincronizaciones": "$data_syncdata.cantidad_sincronizaciones"
            }
        }


        , {
            "$project": {
                "data_syncdata": 0
            }
        }



    ], { allowDiskUse: true }
)
