
//----Ojo usar desde db local

//--obtener bases de datos
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["agrobiz", "valdivia"];

var filtro_fecha_inicio = "2022-01-01"


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]





//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);


    //variables
    var data_aux = {};


    //---EMPLEADOS
    //===================query
    var num_empleados = db.getSiblingDB(db_name_aux)
        .getCollection("employees")
        .aggregate([])
        .count();


    var num_empleados_activos = db.getSiblingDB(db_name_aux)
        .getCollection("employees")
        .aggregate(
            [
                {
                    $match: {
                        "status": true
                    }
                }
            ]
        )
        .count();


    var num_empleados_activos_llave = db.getSiblingDB(db_name_aux)
        .getCollection("employees")
        .aggregate(
            [
                {
                    $match: {
                        "status": true
                        , "onlineAccess": true
                    }
                }
            ]
        )
        .count();


    //---FORMULARIOS
    var num_formularios = db.getSiblingDB(db_name_aux)
        .getCollection("forms")
        .aggregate([])
        .count();



    //---logs -- LOGIN_WEB
    var data_logs_login = db.getSiblingDB(db_name_aux)
        .getCollection("logs")
        .aggregate(
            [

            ]
        )
        .allowDiskUse();







    // //--🔄 data
    // data_query.forEach(item_data => {

    // });
    // data_query.close();








    // //--obtener formularios
    // var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    // //--🔄 ciclar formularios
    // formularios.forEach(item => {

    //     var db_name = db_name_aux;
    //     var coleccion_name = item.anchor;
    //     var result = null;


    //     //---Ejecutar queries

    //     //===================query
    //     var data_query = db.getSiblingDB(db_name)
    //         .getCollection(coleccion_name)
    //         .aggregate(query_pipeline1)
    //         .allowDiskUse();


    //     //--🔄 data
    //     data_query.forEach(item_data => {

    //         result = db.getSiblingDB(db_name)
    //             .getCollection(coleccion_name)
    //             .remove(
    //                 {
    //                     "_id": {
    //                         "$in": item_data.ids
    //                     }
    //                 }
    //             );

    //         result_info.push({
    //             database: db_name,
    //             colleccion: item.anchor,
    //             formulario: coleccion_name,
    //             tquery: "query1 -- repetidos full",
    //             registros_malos: result.nRemoved

    //         })
    //     });
    //     data_query.close();





    // });



});

//--imprimir resultado
result_info
