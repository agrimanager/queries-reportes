//----Ojo usar desde db local

//Fechas de filtro para captura de datos (🚩 EDITAR año y mes)
var año_filtro = 2023;
var mes_filtro = 8;


//--Bases de Datos (DBs)
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = [ "XXXXXXXXXXX"];//(🚩 EDITAR nombre de BASE DE DATOS...si solo es una)

// var bases_de_datos = ["acacias", "agricolaocoa", "avoservice", "elcorozo", "hassexpress", "oleocaribe"];//test

//--Bases de Datos a omitir
var bases_de_datos_lista_negra = [
    //BDs de SISTEMA (⚠️ NO EDITAR)
    "admin",
    "config",
    "local"

    //BDs de PRUEBAS o VIEJAS (🚩 SI SE PUEDE EDITAR)
    //...se puede agregar alguna DB a omitir...que no se necesite para los calculos
    , "capacitacion"
    , "finca"
    , "finca_alejo"
    , "invcamaru_testingNoBorrar"
    , "lukeragricola"
    , "localhost"
    , "localhost:3001"
    , "localhost3"
    , "sanjose_2023-07-12"

    // , "xxxxxxx1"
    // , "xxxxxxx2"
]




//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener usuarios
    var data_users = db.getSiblingDB(db_name_aux).users.aggregate(
        [
            {
                "$addFields": {
                    "_id_str": { $toString: "$_id" }
                }
            }

            , {
                "$group": {
                    "_id": null
                    , "ids": { "$push": "$_id" }
                    , "ids_str": { "$push": "$_id_str" }
                }
            },
            {
                "$project": {
                    "_id": 0
                    , "ids": 1
                    , "ids_str": 1
                }
            }

        ]
    );


    //--🔄 ciclar usuarios
    data_users.forEach(item_data_users => {

        //1) Cantidad de EMPLEADOS con llave de acceso (permiso para entrar al sistema)
        var cantidad_EMPLEADOS_acceso = db.getSiblingDB(db_name_aux)
            .getCollection("employees")
            .aggregate(
                {
                    $match: {
                        // uid: item_data_users._id
                        uid: { $in: item_data_users.ids }
                        , "onlineAccess": true
                    }
                }

            )
            .allowDiskUse()
            .count();


        //2) Cantidad de SINCRONIZACIONES
        var cantidad_SINCRONIZACIONES = db.getSiblingDB(db_name_aux)
            .getCollection("syncdata")
            .aggregate(
                {
                    "$addFields": {
                        "num_anio": { "$year": { "date": "$rgDate" } },
                        "num_mes": { "$month": { "date": "$rgDate" } }
                    }
                }
                , {
                    $match: {
                        // "syncInfo.uid": item_data_users._id_str
                        "syncInfo.uid": { $in: item_data_users.ids_str }
                        , num_anio: año_filtro
                        , num_mes: mes_filtro
                    }
                }

            )
            .allowDiskUse()
            .count();



        //3) Cantidad de LABORES
        var cantidad_LABORES = db.getSiblingDB(db_name_aux)
            .getCollection("tasks")
            .aggregate(
                {
                    "$addFields": {
                        "num_anio": { "$year": { "date": "$rgDate" } },
                        "num_mes": { "$month": { "date": "$rgDate" } }
                    }
                }
                , {
                    $match: {
                        // "uid": item_data_users._id
                        uid: { $in: item_data_users.ids }
                        , num_anio: año_filtro
                        , num_mes: mes_filtro
                    }
                }

            )
            .allowDiskUse()
            .count();


        //4) Cantidad de TRACKS
        var cantidad_TRACKS = db.getSiblingDB(db_name_aux)
            .getCollection("tracks")
            .aggregate(
                {
                    "$addFields": {
                        "num_anio": { "$year": { "date": "$rgDate" } },
                        "num_mes": { "$month": { "date": "$rgDate" } }
                    }
                }
                , {
                    $match: {
                        uid: { $in: item_data_users.ids }
                        , num_anio: año_filtro
                        , num_mes: mes_filtro
                    }
                }

            )
            .allowDiskUse()
            .count();



        //--------FORMULARIOS
        //5) Cantidad de CENSOS (datos de formularios)
        var cantidad_CENSOS_formularios = 0;

        //--obtener formularios
        var formularios = db.getSiblingDB(db_name_aux)
            .getCollection("forms")
            .aggregate(
                {
                    $match: {
                        // "uid": item_data_users._id
                        uid: { $in: item_data_users.ids }
                    }
                }
            )
            .allowDiskUse();

        //--🔄 ciclar formularios
        formularios.forEach(item_formularios => {

            var result = db.getSiblingDB(db_name_aux)
                .getCollection(item_formularios.anchor)
                .aggregate(
                    [
                        {
                            "$addFields": {
                                "num_anio": { "$year": { "date": "$rgDate" } },
                                "num_mes": { "$month": { "date": "$rgDate" } }
                            }
                        }
                        , {
                            $match: {
                                // "uid": item_data_users._id
                                uid: { $in: item_data_users.ids }
                                , num_anio: año_filtro
                                , num_mes: mes_filtro
                            }
                        }
                    ]
                )
                .allowDiskUse()
                .count();


            cantidad_CENSOS_formularios = cantidad_CENSOS_formularios + result


        });



        result_info.push({
            año_filtro: año_filtro,
            mes_filtro: mes_filtro,

            database: db_name_aux
            // user_name: item_data_users.user,
            // user_PIN: item_data_users.PIN

            , "cantidad_EMPLEADOS_acceso": cantidad_EMPLEADOS_acceso
            , "cantidad_SINCRONIZACIONES": cantidad_SINCRONIZACIONES
            , "cantidad_LABORES": cantidad_LABORES
            , "cantidad_TRACKS": cantidad_TRACKS
            , "cantidad_CENSOS_formularios": cantidad_CENSOS_formularios
        })

    })


});

//--imprimir resultado
result_info
