db.form_estadosfenologicos.aggregate(
    [


        // ========= analisis reporte mapa
        // //------OBSERVACIONES QUERIES REPORTES DE MAPA

        // -Variables inyectadas
        // -unwind de campos de seleccion multiple
        // -size de cartografia
        // -NO project
        // -agrupar y desagrupar


        // 1)filtro de fechas
        // 2)unwind de campos de seleccion multiple
        // 3)filtro finca
        // 4)user_timezone
        // 5)cartography y color


        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        // 1)filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",


                "arbol_longitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                "arbol_latitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] },

                //Mapa
                "cartography_id": "$arbol._id",
                "cartography_geometry": "$arbol.geometry",

                "arbol": "$arbol.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },

        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "ARBOL": 0,
                "Point": 0
            }
        }





        , {
            "$project": {


                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",
                "arbol_longitud": "$arbol_longitud",
                "arbol_latitud": "$arbol_latitud",


                "ESTADO FENOLOGICO": "$ESTADO FENOLOGICO",

                "Observaciones": "$OBSERVACIONES",


                "supervisor": "$supervisor",
                "capture": "$capture",
                "Sampling": "$Sampling",
                "Formula": "$Formula",
                "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                //mapa
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"
                , "idform": "$idform"



            }
        }



        // //--leyenda
        // Botones Florales Visibles:#125F2A,
        // Cuajado:#01818F,
        // Cuajado LLenado:#164831,
        // Floracion Cuajado:#0000FF,
        // Floracion Llenado:#072513,
        // Floracion Plena:#AA856B,
        // Flujo Vegetativo:#84BC12,
        // Llenado:#23ADA4,
        // Marchitamiento de Tepalos:#B3FF00,
        // Prefloracion o Coliflor:#258E43,
        // Quieto Floracion:#00FFEE,
        // Quieto Vegetativo:#242AD7,
        // Resiembra:#F91C1C,
        // Yema Hincha o Latente:#63D909

        // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            // {
                            //     "case": { "$eq": ["$ESTADO FENOLOGICO", "xxxxx"] }
                            //     , "then": "#000000"
                            // },
                            //  {"case": { "$eq": ["$ESTADO FENOLOGICO", "
                            //  xxxxx
                            //  "] }, "then": "
                            //  #000000
                            //  "},
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Botones Florales Visibles"] }, "then": "#125F2A" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Cuajado"] }, "then": "#01818F" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Cuajado LLenado"] }, "then": "#164831" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Floracion Cuajado"] }, "then": "#0000FF" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Floracion Llenado"] }, "then": "#072513" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Floracion Plena"] }, "then": "#AA856B" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Flujo Vegetativo"] }, "then": "#84BC12" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Llenado"] }, "then": "#23ADA4" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Marchitamiento de Tepalos"] }, "then": "#B3FF00" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Prefloracion o Coliflor"] }, "then": "#258E43" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Quieto Floracion"] }, "then": "#00FFEE" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Quieto Vegetativo"] }, "then": "#242AD7" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Resiembra"] }, "then": "#F91C1C" },
                            { "case": { "$eq": ["$ESTADO FENOLOGICO", "Yema Hincha o Latente"] }, "then": "#63D909" }


                        ],
                        "default": "#ffffff"
                    }
                }
            }
        }





        //MAPA FINAL
        , {
            "$addFields": {
                "idform": "$idform",
                "cartography_id": "$cartography_id",
                "cartography_geometry": "$cartography_geometry",
                "color": "$color"
            }
        }


        , {
            "$project": {
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },
                "type": "Feature",

                "properties": {
                    "Bloque": "$bloque",
                    "Lote": "$lote",

                    "Estado Fenologico": "$ESTADO FENOLOGICO",

                    "color": "$color"
                }
            }
        }






    ]


)
