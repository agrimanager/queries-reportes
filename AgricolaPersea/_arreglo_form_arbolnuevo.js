var data = db.form_arbolnuevo.aggregate(

    [
        {
            $match: {
                Lote: { $exists: true }
            }
        }
        , {
            $match: {
                Lote: { $ne: 0 }
            }
        }

        , {
            "$addFields": {
                "finca_bloque": {
                    "$cond": {
                        "if": { "$eq": ["$Point.farm", "5dadd2c34f74d529196b2637"] },//la linda
                        "then": "1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$Point.farm", "5dadd1c24f74d529196b2635"] },//alfa
                                "then": "2",
                                "else": "3"//la riviera
                            }
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "nombre_lote": {
                    $concat: [{ $toString: "$finca_bloque" }, "-", { $toString: "$Lote" }]
                }
            }
        }

        , {
            "$lookup": {
                "from": "cartography",
                "as": "info_cartografia",
                "let": {
                    "nombre_lote": "$nombre_lote"
                    , "farm_txt": "$Point.farm"
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": "lot"
                        }
                    }


                    , {
                        "$addFields": {
                            "id_str_farm": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    }

                    , {
                        "$addFields": {
                            "id_str_farm": { $arrayElemAt: ["$id_str_farm", 0] }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$properties.name", "$$nombre_lote"] }
                                    , { "$eq": ["$id_str_farm", "$$farm_txt"] }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "SELECCION_LOTE_UPDATE": {

                    "type": "selection",
                    "features": [
                        {
                            "_id": { $toString: "$info_cartografia._id" },
                            "type": "Feature",
                            "path": "$info_cartografia.path",
                            "properties": {
                                "name": "$info_cartografia.properties.name",
                                "type": "$info_cartografia.properties.type",
                                "production": "$info_cartografia.properties.production",
                                "status": "$info_cartografia.properties.status",
                                "custom": "$info_cartografia.properties.custom",
                            },
                            "geometry": {
                                "type": "$info_cartografia.geometry.type",
                                "coordinates": "$info_cartografia.geometry.coordinates"
                            }
                        }
                    ],
                    "path": "$info_cartografia.path"
                }
            }
        }


        , {
            "$project": {
                "SELECCION_LOTE_UPDATE": 1
            }
        }




    ]
    , { allowDiskUse: true }
)


// data



data.forEach(i => {


    db.form_arbolnuevo.update(
        {
            _id: i._id
        },
        {
            $set: {
                "SELECCION LOTE": i.SELECCION_LOTE_UPDATE
            }
        }
    )



})




/*
// collection: form_arbolnuevo
{
	"_id" : ObjectId("64430b0f739f6f396380b7c6"),
	"SELECCION LOTE" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "60a43ef00b7c610c71773986",
				"type" : "Feature",
				"path" : ",5dadd2fd4f74d529196b2639,60a43eef0b7c610c71773975,",
				"properties" : {
					"type" : "lot",
					"name" : "3-8",
					"production" : true,
					"status" : true,
					"order" : 8,
					"custom" : {
						"color" : {
							"type" : "color",
							"value" : "#D76464"
						},
						"num_arboles" : {
							"type" : "number",
							"value" : 3002
						},
						"num_lineas" : {
							"type" : "number",
							"value" : 188
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						......
					]
				}
			}
		],
		"path" : ",5dadd2fd4f74d529196b2639,60a43eef0b7c610c71773975,"
	},
	"Formula" : "",
	"TIPO ARBOL" : "ARBOLES",
	"TIPO ARBOL_ARBOLES" : "BUENOS",
	"Point" : {
		"farm" : "5dadd2fd4f74d529196b2639",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-76.3206581,
				4.3006436
			]
		}
	},
	"OBSERVACIONES" : "",
	"uid" : ObjectId("5dadc6c24f74d529196b261c"),
	"supervisor" : "800932 SANDRA LORENA RUBIO CARDONA",
	"rgDate" : ISODate("2023-04-21T16:39:14.000-05:00"),
	"uDate" : ISODate("2023-04-21T16:39:14.000-05:00"),
	"capture" : "M"
}
*/
