[

    

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "linea": "$linea.properties.name",

            
            "arbol_longitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
            "arbol_latitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] },

            "arbol": "$arbol.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },

    { "$unwind": "$finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "ARBOL": 0,
            "Point": 0
        }
    }


    , {
        "$project": {
            "rgDate día": 0,
            "rgDate mes": 0,
            "rgDate año": 0,
            "rgDate hora": 0,

            "uDate día": 0,
            "uDate mes": 0,
            "uDate año": 0,
            "uDate hora": 0
        }
    },



    {
        "$addFields": {
            "nombre_maestro_principal1": "FACTOR DE MUERTE_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal1": {
                "$strLenCP": "$nombre_maestro_principal1"
            }
        }
    }




    , {
        "$addFields": {
            "valor_mestro_enlazado1": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal1"]
                                        }, "$nombre_maestro_principal1"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_mestro_enlazado1",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "valor_mestro_enlazado1": { "$ifNull": ["$valor_mestro_enlazado1", ""] }
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal1": 0,
            "num_letras_nombre_maestro_principal1": 0
        }
    },


    {
        "$addFields": {
            "nombre_maestro_principal2": "PRESENCIA DE HONGOS_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal2": {
                "$strLenCP": "$nombre_maestro_principal2"
            }
        }
    }




    , {
        "$addFields": {
            "valor_mestro_enlazado2": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal2"]
                                        }, "$nombre_maestro_principal2"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_mestro_enlazado2",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "valor_mestro_enlazado2": { "$ifNull": ["$valor_mestro_enlazado2", ""] }
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal2": 0,
            "num_letras_nombre_maestro_principal2": 0
        }
    }


    , {
        "$project": {

            "finca": "$finca",
            "bloque": "$bloque",
            "lote": "$lote",
            "linea": "$linea",
            "arbol": "$arbol",
            "arbol_longitud": "$arbol_longitud",
            "arbol_latitud": "$arbol_latitud",

            "Factor de Muerte": { "$ifNull": ["$FACTOR DE MUERTE", ""] },
            "Tipo de Factor": "$valor_mestro_enlazado1",

            "Presencia de hongo": { "$ifNull": ["$PRESENCIA DE HONGOS", ""] },
            "Tipo de hongo": "$valor_mestro_enlazado2",

            "Observaciones": "$OBSERVACIONES",

            "supervisor": "$supervisor",
            "capture": "$capture",
            "Sampling": "$Sampling",
            "Formula": "$Formula",
            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

        }
    }



]