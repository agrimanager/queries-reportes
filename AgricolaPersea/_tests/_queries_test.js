//---transacciones
db.getCollection("suppliesTimeline_new_2020-06-04_1backup").aggregate(
    
    {
        $match:{
            // quantity:{$gt:0},
            deleted:false,
            // lbid : ""
        }
    }

    ,{
        $addFields: {
            "tipo_transaccion": {
                "$cond": {
                    "if": {
                        "$gte": ["$quantity", 0]
                    },
                    "then": "(+)INGRESO",
                    "else": "(-)EGRESO"
                }
            }
        }
    }

)
