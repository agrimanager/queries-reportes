//reporte estandar de INVENTARIO
var cursor = db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---simular fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },

        {
            "$lookup": {
                "from": "supplies",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                "pipeline": [

                    {
                        "$lookup": {
                            "from": "suppliesTimeline",
                            "localField": "_id",
                            "foreignField": "sid",
                            "as": "suppliesTimeline_reference"
                        }
                    }, {
                        "$unwind": {
                            "path": "$suppliesTimeline_reference",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$match": {
                            "suppliesTimeline_reference.deleted": {
                                "$eq": false
                            }
                        }
                    }, {
                        "$lookup": {
                            "from": "warehouses",
                            "localField": "suppliesTimeline_reference.wid",
                            "foreignField": "_id",
                            "as": "warehouse_reference"
                        }
                    }, {
                        "$unwind": {
                            "path": "$warehouse_reference",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$lookup": {
                            "from": "companies",
                            "localField": "warehouse_reference.cid",
                            "foreignField": "_id",
                            "as": "company_reference"
                        }
                    }, {
                        "$unwind": {
                            "path": "$company_reference",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$group": {
                            "_id": {
                                "name_supply": "$name",
                                "name_company": "$company_reference.name",
                                "name_warehouse": "$warehouse_reference.name",
                                "type": "$type",
                                "category": "$category",
                                "description": "$description",
                                "inputMeasureEquivalence": "$inputMeasureEquivalence",
                                "pc": "$pc",
                                "pr": "$pr",
                                "water": "$water",
                                "sync": "$sync",
                                "activeIngredients": "$activeIngredients",
                                "puc": "$puc",
                                "sku": "$sku",
                                "supply_quantity": "$quantity",
                                "supply_disponible": {
                                    "$cond": {
                                        "if": {
                                            "$gt": ["$quantity", 0]
                                        },
                                        "then": "Disponible",
                                        "else": "No disponible"
                                    }
                                }
                            },
                            "transacciones": {
                                "$addToSet": "$suppliesTimeline_reference"
                            }
                        }
                    }, {
                        "$addFields": {
                            "_id.transacciones": "$transacciones"
                        }
                    }, {
                        "$replaceRoot": {
                            "newRoot": "$_id"
                        }
                    }, {
                        "$addFields": {
                            "ingresos": {
                                "$filter": {
                                    "input": "$transacciones",
                                    "as": "transacciones_aux1",
                                    "cond": {
                                        "$gt": ["$$transacciones_aux1.quantity", 0]
                                    }
                                }
                            }
                        }
                    }, {
                        "$addFields": {
                            "egresos": {
                                "$filter": {
                                    "input": "$transacciones",
                                    "as": "transacciones_aux2",
                                    "cond": {
                                        "$lt": ["$$transacciones_aux2.quantity", 0]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$project": {

                            //---reporte sin form
                            "rgDate": "$filtro_fecha_inicio",

                            "Producto": "$name_supply",
                            "Empresa": {
                                "$ifNull": ["$name_company", "---"]
                            },
                            "Bodega": {
                                "$ifNull": ["$name_warehouse", "---"]
                            },
                            "Tipo": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$type", "Consumables"]
                                        },
                                        "then": "Consumibles"
                                    }, {
                                        "case": {
                                            "$eq": ["$type", "Machinery"]
                                        },
                                        "then": "Maquinarias"
                                    }, {
                                        "case": {
                                            "$eq": ["$type", "Tools"]
                                        },
                                        "then": "Herramientas"
                                    }],
                                    "default": "--------"
                                }
                            },
                            "Categoria": "$category",
                            "Descripcion": "$description",
                            "[Unidades]": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mts"]
                                        },
                                        "then": "Metros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "km"]
                                        },
                                        "then": "Kilometros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cm"]
                                        },
                                        "then": "Centimetros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mile"]
                                        },
                                        "then": "Millas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "yard"]
                                        },
                                        "then": "Yardas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "foot"]
                                        },
                                        "then": "Pies"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "inch"]
                                        },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "kg"]
                                        },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "gr"]
                                        },
                                        "then": "Gramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mg"]
                                        },
                                        "then": "Miligramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "US/ton"]
                                        },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "ton"]
                                        },
                                        "then": "Toneladas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "oz"]
                                        },
                                        "then": "Onzas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "lb"]
                                        },
                                        "then": "Libras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "lts"]
                                        },
                                        "then": "Litros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "US/galon"]
                                        },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "galon"]
                                        },
                                        "then": "Galones"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cf"]
                                        },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "ci"]
                                        },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cuc"]
                                        },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cum"]
                                        },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "packages"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "package"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bags"]
                                        },
                                        "then": "Bolsas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "sacks"]
                                        },
                                        "then": "Sacos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "yemas"]
                                        },
                                        "then": "Yemas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bun"]
                                        },
                                        "then": "Factura"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cargo"]
                                        },
                                        "then": "Flete"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "manege"]
                                        },
                                        "then": "Picadero"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "hr"]
                                        },
                                        "then": "Hora"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "qty"]
                                        },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "hectares"]
                                        },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "blocks"]
                                        },
                                        "then": "Cuadras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "dustbin"]
                                        },
                                        "then": "Canecas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bunch"]
                                        },
                                        "then": "Racimos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cubic-meter"]
                                        },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "metro-line"]
                                        },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "square-meter"]
                                        },
                                        "then": "Metro cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            },
                            "pc": "$pc",
                            "pr": "$pr",
                            "Necesita agua": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$water", true]
                                    },
                                    "then": "Si",
                                    "else": "No"
                                }
                            },
                            "Sincronizacion": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$sync", true]
                                    },
                                    "then": "Si",
                                    "else": "No"
                                }
                            },
                            "Ingredientes activos": {
                                "$cond": {
                                    "if": {
                                        "$isArray": ["$activeIngredients"]
                                    },
                                    "then": {
                                        "$reduce": {
                                            "input": "$activeIngredients",
                                            "initialValue": "",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$indexOfArray": ["$activeIngredients", "$$this"]
                                                        }, 0]
                                                    },
                                                    "then": {
                                                        "$concat": ["$$value", "$$this"]
                                                    },
                                                    "else": {
                                                        "$concat": ["$$value", " ; ", "$$this"]
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "else": ""
                                }
                            },
                            "Cuenta contable": "$puc",
                            "SKU": "$sku",
                            "Cantidad disponible": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": [{
                                            "$subtract": [{
                                                "$sum": "$ingresos.productEquivalence"
                                            }, {
                                                "$sum": "$egresos.productEquivalence"
                                            }]
                                        }, 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": [{
                                                "$subtract": [{
                                                    "$sum": "$ingresos.productEquivalence"
                                                }, {
                                                    "$sum": "$egresos.productEquivalence"
                                                }]
                                            }, 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "Disponible": {
                                "$cond": {
                                    "if": {
                                        "$gt": [{
                                            "$subtract": [{
                                                "$sum": "$ingresos.productEquivalence"
                                            }, {
                                                "$sum": "$egresos.productEquivalence"
                                            }]
                                        }, 0]
                                    },
                                    "then": "Si",
                                    "else": "No"
                                }
                            },
                            "Veces de ingresos": {
                                "$size": "$ingresos"
                            },
                            "Costo ingresos ($)": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": [{
                                            "$sum": "$ingresos.total"
                                        }, 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": [{
                                                "$sum": "$ingresos.total"
                                            }, 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "Cantidad ingresos (#)": {
                                "$sum": "$ingresos.productEquivalence"
                            },
                            "veces de egresos": {
                                "$size": "$egresos"
                            },
                            "Costo egresos ($)": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": [{
                                            "$sum": "$egresos.total"
                                        }, 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": [{
                                                "$sum": "$egresos.total"
                                            }, 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "Cantidad egresos (#)": {
                                "$sum": "$egresos.productEquivalence"
                            },
                            "[(Unidades)]": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mts"]
                                        },
                                        "then": "Metros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "km"]
                                        },
                                        "then": "Kilometros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cm"]
                                        },
                                        "then": "Centimetros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mile"]
                                        },
                                        "then": "Millas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "yard"]
                                        },
                                        "then": "Yardas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "foot"]
                                        },
                                        "then": "Pies"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "inch"]
                                        },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "kg"]
                                        },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "gr"]
                                        },
                                        "then": "Gramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "mg"]
                                        },
                                        "then": "Miligramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "US/ton"]
                                        },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "ton"]
                                        },
                                        "then": "Toneladas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "oz"]
                                        },
                                        "then": "Onzas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "lb"]
                                        },
                                        "then": "Libras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "lts"]
                                        },
                                        "then": "Litros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "US/galon"]
                                        },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "galon"]
                                        },
                                        "then": "Galones"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cf"]
                                        },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "ci"]
                                        },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cuc"]
                                        },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cum"]
                                        },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "packages"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "package"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bags"]
                                        },
                                        "then": "Bolsas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "sacks"]
                                        },
                                        "then": "Sacos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "yemas"]
                                        },
                                        "then": "Yemas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bun"]
                                        },
                                        "then": "Factura"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cargo"]
                                        },
                                        "then": "Flete"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "manege"]
                                        },
                                        "then": "Picadero"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "hr"]
                                        },
                                        "then": "Hora"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "qty"]
                                        },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "hectares"]
                                        },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "blocks"]
                                        },
                                        "then": "Cuadras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "dustbin"]
                                        },
                                        "then": "Canecas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "bunch"]
                                        },
                                        "then": "Racimos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "cubic-meter"]
                                        },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "metro-line"]
                                        },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputMeasureEquivalence", "square-meter"]
                                        },
                                        "then": "Metro cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            }
                        }
                    }, {
                        "$sort": {
                            "Producto": 1
                        }
                    }

                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //====(new) AGRUPAR POR PRODUCTO
        , {
            $group: {
                _id: {
                    "Producto": "$Producto"
                }
                , "Cantidad_Total": { $sum: "$Cantidad disponible" }
            }
        }


    ]
)




cursor.forEach(i => {

    // console.log(i._id.Producto)

    db.supplies.update(
        {
            name: i._id.Producto
        },
        {
            $set: {
                quantity: i.Cantidad_Total
            }

        }
    )

})
