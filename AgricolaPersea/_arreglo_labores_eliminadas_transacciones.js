


db.suppliesTimeline.aggregate(

    //---cruzar con labores para hallar transacciones de labores eliminadas

    {
        $match: {
            "lbcod": { $ne: "" }
        }
    }

    , {
        "$lookup": {
            "from": "tasks",
            "localField": "lbcod",
            "foreignField": "cod",
            "as": "data_labores"
        }
    }

    ,{
        $match: {
            "data_labores": { $eq: [] }
        }
    }

)
