db.suppliesTimeline_fix.aggregate(
    [

        //excluir transacciones eliminadas
        {
            "$match": {
                "deleted": false
            }
        },

        //excluir transacciones de labores eliminadas

        {
            "$lookup": {
                "from": "tasks",
                "localField": "lbcod",
                "foreignField": "cod",
                "as": "data_labores"
            }
        }

        , {
            "$addFields": {
                "existe_labor": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$ne": ["$lbcod", ""] }
                                , { "$eq": ["$data_labores", []] }
                            ]
                        },
                        "then": "no_existe",
                        "else": "si_puede_existir"
                    }
                }
            }
        }

        , {
            "$match": {
                "existe_labor": "si_puede_existir"
            }
        }

        , {
            "$project": {
                "data_labores": 0
            }
        }


        //agrupar
        , {
            "$group": {
                "_id": {
                    "sid": "$sid"
                }
                , "cantidad_transacciones": { "$sum": 1 }
                // , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$sort": {
                "cantidad_transacciones": 1
            }
        }


    ]

)
