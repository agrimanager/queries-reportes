[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_registrodeconsumoenergetico",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  
                "filtro_fecha_fin": "$Busqueda fin"         
            },
            "pipeline": [


                { "$addFields": { "type_filtro": { "$type": "$FECHA DE USO" } } },
                { "$match": { "type_filtro": { "$eq": "date" } } },

                { "$addFields": { "anio_filtro": { "$year": "$FECHA DE USO" } } },
                { "$match": { "anio_filtro": { "$gt": 2000 } } },
                { "$match": { "anio_filtro": { "$lt": 3000 } } },



                {
                    "$project": {
                        "FINCA": 0,
                        "Point": 0,
                        "uid": 0,
                        "type_filtro": 0,
                        "anio_filtro": 0
                    }
                },

                { "$addFields": { "rgDate":  "$FECHA DE USO" } }



            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]