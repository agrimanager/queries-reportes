db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registrodeconsumoenergetico",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    // //----condiciones

                    // //--fecha
                    { "$addFields": { "type_filtro": { "$type": "$FECHA DE USO" } } },
                    { "$match": { "type_filtro": { "$eq": "date" } } },

                    { "$addFields": { "anio_filtro": { "$year": "$FECHA DE USO" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //--cartografia
                    //......



                    {
                        "$project": {
                            "FINCA": 0,
                            "Point": 0,
                            "uid": 0,
                            "type_filtro": 0,
                            "anio_filtro": 0
                        }
                    },

                    { "$addFields": { "rgDate":  "$FECHA DE USO" } }



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)