db.suppliesTimeline.aggregate(
    [
        //---producto
        {
            $match: {
                sid: ObjectId("5f31581404d71938f1b6ed84")
            }
        },

        {
            "$lookup": {
                "from": "supplies",
                "localField": "sid",
                "foreignField": "_id",
                "as": "producto"
            }
        }
        ,{ $unwind: "$producto" }
        ,{ "$addFields": { "producto": "$producto.name" } }

        , {
            "$lookup": {
                "from": "warehouses",
                "localField": "wid",
                "foreignField": "_id",
                "as": "bodega"
            }
        },
        { $unwind: "$bodega" }
        ,{ "$addFields": { "bodega": "$bodega.name" } }




        //---transaccion asociada
        , {
            $unwind: {
                "path": "$inAssociativeId",
                "preserveNullAndEmptyArrays": true
            }
        }
        ,{ "$addFields": { "transaccion_asociada": "$inAssociativeId._id" } }


    ]

)