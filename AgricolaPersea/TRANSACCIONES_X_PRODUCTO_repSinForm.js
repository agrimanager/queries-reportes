//query de LABORES X EMPLEADO
db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---simular fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },

        {
            "$lookup": {
                "from": "suppliesTimeline",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                "pipeline": [

                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }, {
                        "$unwind": "$user_timezone"
                    }, {
                        "$addFields": {
                            "user_timezone": "$user_timezone.timezone"
                        }
                    }, {
                        "$match": {
                            "deleted": {
                                "$eq": false
                            }
                        }
                    }, {
                        "$addFields": {
                            "fecha_factura": {
                                "$ifNull": ["$invoiceDate", ""]
                            },
                            "fecha_vencimiento": {
                                "$ifNull": ["$dueDate", ""]
                            }
                        }
                    }, {
                        "$addFields": {
                            "tipo_fecha_factura": {
                                "$type": "$fecha_factura"
                            },
                            "tipo_fecha_vencimiento": {
                                "$type": "$fecha_vencimiento"
                            }
                        }
                    }, {
                        "$addFields": {
                            "fecha_factura_str": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$tipo_tipo_fecha_factura", "string"]
                                    },
                                    "then": "$tipo_fecha_factura",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{
                                                    "$eq": ["$tipo_fecha_factura", "date"]
                                                }, {
                                                    "$gt": ["$fecha_factura", "1970-01-01T05:00:00.000Z"]
                                                }]
                                            },
                                            "then": {
                                                "$dateToString": {
                                                    "format": "%Y-%m-%d",
                                                    "date": "$fecha_factura"
                                                }
                                            },
                                            "else": ""
                                        }
                                    }
                                }
                            },
                            "fecha_vencimiento_str": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$tipo_fecha_vencimiento", "string"]
                                    },
                                    "then": "$fecha_vencimiento",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{
                                                    "$eq": ["$tipo_fecha_vencimiento", "date"]
                                                }, {
                                                    "$gt": ["fecha_vencimiento", "1970-01-01T05:00:00.000Z"]
                                                }]
                                            },
                                            "then": {
                                                "$dateToString": {
                                                    "format": "%Y-%m-%d",
                                                    "date": "$fecha_vencimiento"
                                                }
                                            },
                                            "else": ""
                                        }
                                    }
                                }
                            }
                        }
                    }, {
                        "$lookup": {
                            "from": "supplies",
                            "localField": "sid",
                            "foreignField": "_id",
                            "as": "supplies_reference"
                        }
                    }, {
                        "$unwind": "$supplies_reference"
                    }, {
                        "$lookup": {
                            "from": "warehouses",
                            "localField": "wid",
                            "foreignField": "_id",
                            "as": "warehouse_reference"
                        }
                    }, {
                        "$unwind": "$warehouse_reference"
                    }, {
                        "$lookup": {
                            "from": "companies",
                            "localField": "warehouse_reference.cid",
                            "foreignField": "_id",
                            "as": "company_reference"
                        }
                    }, {
                        "$unwind": {
                            "path": "$company_reference",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_reference"
                        }
                    }, {
                        "$unwind": "$user_reference"
                    }, {
                        "$project": {

                            //---reporte sin form
                            "rgDate": "$filtro_fecha_inicio",

                            "_id": 0,
                            "Producto": "$supplies_reference.name",
                            "Tipo de Producto": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$supplies_reference.type", "Consumables"]
                                        },
                                        "then": "Consumibles"
                                    }, {
                                        "case": {
                                            "$eq": ["$supplies_reference.type", "Machinery"]
                                        },
                                        "then": "Maquinarias"
                                    }, {
                                        "case": {
                                            "$eq": ["$supplies_reference.type", "Tools"]
                                        },
                                        "then": "Herramientas"
                                    }],
                                    "default": "--------"
                                }
                            },
                            "Tipo de transaccion": {
                                "$cond": {
                                    "if": {
                                        "$gt": ["$quantity", 0]
                                    },
                                    "then": "(+) Ingreso",
                                    "else": "(-) Egreso"
                                }
                            },
                            "Cantidad": {
                                "$ifNull": [{
                                    "$cond": {
                                        "if": {
                                            "$gt": ["$quantity", 0]
                                        },
                                        "then": "$productEquivalence",
                                        "else": {
                                            "$multiply": ["$productEquivalence", -1]
                                        }
                                    }
                                }, 0]
                            },
                            "[Unidades]": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$productMeasure", "mts"]
                                        },
                                        "then": "Metros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "km"]
                                        },
                                        "then": "Kilometros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cm"]
                                        },
                                        "then": "Centimetros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "mile"]
                                        },
                                        "then": "Millas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "yard"]
                                        },
                                        "then": "Yardas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "foot"]
                                        },
                                        "then": "Pies"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "inch"]
                                        },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "kg"]
                                        },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "gr"]
                                        },
                                        "then": "Gramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "mg"]
                                        },
                                        "then": "Miligramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "US/ton"]
                                        },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "ton"]
                                        },
                                        "then": "Toneladas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "oz"]
                                        },
                                        "then": "Onzas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "lb"]
                                        },
                                        "then": "Libras"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "lts"]
                                        },
                                        "then": "Litros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "US/galon"]
                                        },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "galon"]
                                        },
                                        "then": "Galones"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cf"]
                                        },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "ci"]
                                        },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cuc"]
                                        },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cum"]
                                        },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "packages"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "package"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bags"]
                                        },
                                        "then": "Bolsas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "sacks"]
                                        },
                                        "then": "Sacos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "yemas"]
                                        },
                                        "then": "Yemas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bun"]
                                        },
                                        "then": "Factura"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cargo"]
                                        },
                                        "then": "Flete"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "manege"]
                                        },
                                        "then": "Picadero"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "hr"]
                                        },
                                        "then": "Hora"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "qty"]
                                        },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "hectares"]
                                        },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "blocks"]
                                        },
                                        "then": "Cuadras"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "dustbin"]
                                        },
                                        "then": "Canecas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bunch"]
                                        },
                                        "then": "Racimos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cubic-meter"]
                                        },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "metro-line"]
                                        },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "square-meter"]
                                        },
                                        "then": "Metro cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            },
                            "Valor total transaccion ($)": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": ["$total", 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": ["$total", 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "Empresa": "$company_reference.name",
                            "Bodega": "$warehouse_reference.name",
                            "Fecha": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            },
                            "Informacion": "$information",
                            "Cantidad Trans": "$quantity",
                            "[Unidad Trans]": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$inputmeasure", "mts"]
                                        },
                                        "then": "Metros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "km"]
                                        },
                                        "then": "Kilometros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cm"]
                                        },
                                        "then": "Centimetros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "mile"]
                                        },
                                        "then": "Millas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "yard"]
                                        },
                                        "then": "Yardas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "foot"]
                                        },
                                        "then": "Pies"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "inch"]
                                        },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "kg"]
                                        },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "gr"]
                                        },
                                        "then": "Gramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "mg"]
                                        },
                                        "then": "Miligramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "US/ton"]
                                        },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "ton"]
                                        },
                                        "then": "Toneladas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "oz"]
                                        },
                                        "then": "Onzas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "lb"]
                                        },
                                        "then": "Libras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "lts"]
                                        },
                                        "then": "Litros"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "US/galon"]
                                        },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "galon"]
                                        },
                                        "then": "Galones"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cf"]
                                        },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "ci"]
                                        },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cuc"]
                                        },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cum"]
                                        },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "packages"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "package"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "bags"]
                                        },
                                        "then": "Bolsas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "sacks"]
                                        },
                                        "then": "Sacos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "yemas"]
                                        },
                                        "then": "Yemas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "bun"]
                                        },
                                        "then": "Factura"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cargo"]
                                        },
                                        "then": "Flete"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "manege"]
                                        },
                                        "then": "Picadero"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "hr"]
                                        },
                                        "then": "Hora"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "qty"]
                                        },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "hectares"]
                                        },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "blocks"]
                                        },
                                        "then": "Cuadras"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "dustbin"]
                                        },
                                        "then": "Canecas"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "bunch"]
                                        },
                                        "then": "Racimos"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "cubic-meter"]
                                        },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "metro-line"]
                                        },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": {
                                            "$eq": ["$inputmeasure", "square-meter"]
                                        },
                                        "then": "Metro cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            },
                            "Factura": "$invoiceNumber",
                            "Subtotal ($)": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": ["$subtotal", 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": ["$subtotal", 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "IVA (%)": "$tax",
                            "Fecha factura": "$fecha_factura_str",
                            "Fecha vencimiento": "$fecha_vencimiento_str",
                            "Cantidad en bodega": {
                                "$divide": [{
                                    "$subtract": [{
                                        "$multiply": ["$outQuantityMovement", 100]
                                    }, {
                                        "$mod": [{
                                            "$multiply": ["$outQuantityMovement", 100]
                                        }, 1]
                                    }]
                                }, 100]
                            },
                            "[[Unidades]]": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$eq": ["$productMeasure", "mts"]
                                        },
                                        "then": "Metros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "km"]
                                        },
                                        "then": "Kilometros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cm"]
                                        },
                                        "then": "Centimetros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "mile"]
                                        },
                                        "then": "Millas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "yard"]
                                        },
                                        "then": "Yardas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "foot"]
                                        },
                                        "then": "Pies"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "inch"]
                                        },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "kg"]
                                        },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "gr"]
                                        },
                                        "then": "Gramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "mg"]
                                        },
                                        "then": "Miligramos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "US/ton"]
                                        },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "ton"]
                                        },
                                        "then": "Toneladas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "oz"]
                                        },
                                        "then": "Onzas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "lb"]
                                        },
                                        "then": "Libras"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "lts"]
                                        },
                                        "then": "Litros"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "US/galon"]
                                        },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "galon"]
                                        },
                                        "then": "Galones"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cf"]
                                        },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "ci"]
                                        },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cuc"]
                                        },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cum"]
                                        },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "packages"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "package"]
                                        },
                                        "then": "Bultos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bags"]
                                        },
                                        "then": "Bolsas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "sacks"]
                                        },
                                        "then": "Sacos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "yemas"]
                                        },
                                        "then": "Yemas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bun"]
                                        },
                                        "then": "Factura"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cargo"]
                                        },
                                        "then": "Flete"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "manege"]
                                        },
                                        "then": "Picadero"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "hr"]
                                        },
                                        "then": "Hora"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "qty"]
                                        },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "hectares"]
                                        },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "blocks"]
                                        },
                                        "then": "Cuadras"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "dustbin"]
                                        },
                                        "then": "Canecas"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "bunch"]
                                        },
                                        "then": "Racimos"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "cubic-meter"]
                                        },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "metro-line"]
                                        },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": {
                                            "$eq": ["$productMeasure", "square-meter"]
                                        },
                                        "then": "Metro cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            }
                        }
                    }

                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




    ]
)
