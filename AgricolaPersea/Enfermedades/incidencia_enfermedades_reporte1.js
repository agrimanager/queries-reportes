db.form_monitoreodeenfermedades.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-06-17T23:45:15.000-05:00"),
                "Busqueda fin": ISODate("2021-09-17T12:45:15.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            $eq: ["$Point.farm", "5dadd2c34f74d529196b2637"]//la linda
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",


                "arbol_longitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                "arbol_latitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] },

                "arbol": "$arbol.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },

        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "ARBOL": 0,
                "Point": 0
            }
        }



        , {
            "$project": {
                "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },



        {
            "$addFields": {
                "nombre_maestro_principal": "ENFERMEDAD_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }

        , {
            "$addFields": {
                "valor_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , {
            "$unwind": {
                "path": "$valor_mestro_enlazado",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0
            }
        }


        , {
            "$project": {


                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",
                "arbol_longitud": "$arbol_longitud",
                "arbol_latitud": "$arbol_latitud",


                "Enfermedad": "$ENFERMEDAD",
                "Tipo de Enfermedad": "$valor_mestro_enlazado",

                "Gravedad": "$GRAVE",
                "Observaciones": "$OBSERVACIONES",


                "supervisor": "$supervisor",
                "capture": "$capture",
                "Sampling": "$Sampling",
                "Formula": "$Formula",
                "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                "Semana": { "$isoWeek": "$rgDate" }

            }
        },




        {
            "$group": {
                "_id": {
                    "Tipo de Enfermedad": "$Tipo de Enfermedad",
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }
        , {
            "$group": {
                "_id": {
                    "Tipo de Enfermedad": "$_id.Tipo de Enfermedad",
                    "Lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_afectados_x_lote_x_enfermedad": { "$sum": 1 }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_afectados_x_lote_x_enfermedad": "$Arboles_afectados_x_lote_x_enfermedad"
                        }
                    ]
                }
            }
        }
        , {
            "$group": {
                "_id": {
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }
        , {
            "$group": {
                "_id": {
                    "lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_evaluados_x_lote": { "$sum": 1 }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_evaluados_x_lote": "$Arboles_evaluados_x_lote"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "PTC_incidencia": {
                    "$multiply": [
                        { "$divide": ["$Arboles_afectados_x_lote_x_enfermedad", "$Arboles_evaluados_x_lote"] }
                        , 100]
                }
            }
        }





    ], { allowDiskUse: true }


)
