[




    {
        "$project": {
            "uDate día": 0,
            "uDate mes": 0,
            "uDate año": 0,
            "uDate hora": 0

            , "Busqueda inicio": 0
            , "Busqueda fin": 0
            , "uid": 0
            , "user": 0
            , "FincaID": 0
            , "Finca nombre": 0

            , "Keys": 0
            , "Lote": 0
            , "today": 0
            , "Formula": 0
        }
    },



    {
        "$addFields": {
            "variable_cartografia": "$SELECCION LOTE"
        }
    },



    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },





    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "cartography_id": { "$ifNull": ["$lote._id", null] }
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "cartography_geometry": { "$ifNull": ["$Point.geometry", {}] }
        }
    },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "OBSERVACIONES": 0,
            "SELECCION LOTE": 0,

            "bloque_id": 0,
            "capture": 0,

            "uid": 0
            , "uDate": 0
            , "Point": 0

        }
    },


    {
        "$addFields": {
            "tipo_arbol_maestro": {
                "$ifNull": [
                    "$TIPO ARBOL_ARBOLES",
                    {
                        "$ifNull": ["$TIPO ARBOL_RESIEMBRAS"
                            , { "$ifNull": ["$TIPO ARBOL_SOCAS", ""] }]
                    }
                ]
            }
        }
    },

    {
        "$project": {
            "TIPO ARBOL_ARBOLES": 0,
            "TIPO ARBOL_RESIEMBRAS": 0,
            "TIPO ARBOL_SOCAS": 0
        }
    },


    {
        "$addFields": {
            "tipo_arbol_maestro_aux": {
                "$switch": {
                    "branches": [


                        {
                            "case": { "$eq": ["$TIPO ARBOL", "ARBOLES"] },
                            "then": {

                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "BUENOS"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "CLOROTICOS"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "MUERTO O FALTANTE"] },
                                            "then": "$tipo_arbol_maestro"
                                        }


                                    ],
                                    "default": "otro_caso"
                                }

                            }
                        },

                        {
                            "case": { "$eq": ["$TIPO ARBOL", "RESIEMBRAS"] },
                            "then": {

                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "BUENA"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "CLOROTICA"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "MUERTA O FALTANTE"] },
                                            "then": "$tipo_arbol_maestro"
                                        }


                                    ],
                                    "default": "otro_caso"
                                }

                            }
                        },


                        {
                            "case": { "$eq": ["$TIPO ARBOL", "SOCAS"] },
                            "then": {

                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "RECUPERADAS"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "NO RECUPERADAS"] },
                                            "then": "$tipo_arbol_maestro"
                                        },

                                        {
                                            "case": { "$eq": ["$tipo_arbol_maestro", "INICIO BROTACION"] },
                                            "then": "$tipo_arbol_maestro"
                                        }


                                    ],
                                    "default": "otro_caso"
                                }

                            }
                        }




                    ],
                    "default": "otro_caso"
                }
            }
        }
    },





    {
        "$addFields": {
            "arr_type": {
                "$concat": [
                    "$TIPO ARBOL",
                    " - ",

                    "$tipo_arbol_maestro_aux"
                ]
            }
        }
    },



    {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$arr_type", "SOCAS - RECUPERADAS"] },
                            "then": "#2ecc71"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "SOCAS - NO RECUPERADAS"] },
                            "then": "#e74c3c"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "SOCAS - INICIO BROTACION"] },
                            "then": "#2ecc71"
                        },

                        {
                            "case": { "$eq": ["$arr_type", "RESIEMBRAS - BUENA"] },
                            "then": "#2ecc71"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "RESIEMBRAS - CLOROTICA"] },
                            "then": "#e74c3c"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "RESIEMBRAS - MUERTA O FALTANTE"] },
                            "then": "#2c3e50"
                        },

                        {
                            "case": { "$eq": ["$arr_type", "ARBOLES - BUENOS"] },
                            "then": "#2ecc71"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "ARBOLES - CLOROTICOS"] },
                            "then": "#e74c3c"
                        },
                        {
                            "case": { "$eq": ["$arr_type", "ARBOLES - MUERTO O FALTANTE"] },
                            "then": "#2c3e50"
                        }
                    ],
                    "default": "#808080"
                }
            }
        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": "$cartography_id"
            , "cartography_geometry": "$cartography_geometry"

            , "color": "$color"

        }
    }




    , {
        "$project": {

            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",


            "properties": {

                "Bloque": "$bloque",
                "Lote": "$lote",
                "Tipo": "$TIPO ARBOL",
                "Tipo DETALLE": "$arr_type",
                "color": "$color"

            }

        }
    }


]
