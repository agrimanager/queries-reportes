//mapa
db.form_arbolnuevo.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-05-07T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5dadd2c34f74d529196b2637"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0

                "idform": "123",
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte


        // {
        //     "$addFields": { "Cartography": "$Point.geometry" }
        // },

        // {
        //     "$addFields": { "elemnq": "$_id" }
        // },


        {
            "$project": {
                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0

                , "Busqueda inicio": 0
                , "Busqueda fin": 0
                , "uid": 0
                , "user": 0
                , "FincaID": 0
                , "Finca nombre": 0

                , "Keys": 0
                , "Lote": 0
                , "today": 0
                , "Formula": 0
            }
        },



        {
            "$addFields": {
                "variable_cartografia": "$SELECCION LOTE"
            }
        },

        // {
        //     "$addFields": { "elemnq": { "$toObjectId": "$_id" } }
        // },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        // {
        //     "$addFields": {
        //         "finca": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$finca",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "finca._id",
        //         "foreignField": "_id",
        //         "as": "finca"
        //     }
        // },
        // { "$unwind": "$finca" },

        // { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        // { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        //------MAPA_VARIABLE_ID_FEATURE
        {
            "$addFields": {
                "cartography_id": { "$ifNull": ["$lote._id", null] }
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        //------MAPA_VARIABLE_GEOMETRY
        {
            "$addFields": {
                "cartography_geometry": { "$ifNull": ["$Point.geometry", {}] }
            }
        },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "OBSERVACIONES": 0,
                "SELECCION LOTE": 0,

                "bloque_id": 0,
                "capture": 0,

                "uid": 0
                , "uDate": 0
                , "Point": 0

            }
        },


        {
            "$addFields": {
                "tipo_arbol_maestro": {
                    "$ifNull": [
                        "$TIPO ARBOL_ARBOLES",
                        {
                            "$ifNull": ["$TIPO ARBOL_RESIEMBRAS"
                                , { "$ifNull": ["$TIPO ARBOL_SOCAS", ""] }]
                        }
                    ]
                }
            }
        },

        {
            "$project": {
                "TIPO ARBOL_ARBOLES": 0,
                "TIPO ARBOL_RESIEMBRAS": 0,
                "TIPO ARBOL_SOCAS": 0
            }
        },


        {
            "$addFields": {
                "tipo_arbol_maestro_aux": {
                    "$switch": {
                        "branches": [


                            //ARBOLES
                            {
                                "case": { "$eq": ["$TIPO ARBOL", "ARBOLES"] },
                                "then": {

                                    "$switch": {
                                        "branches": [
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "BUENOS"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "CLOROTICOS"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "MUERTO O FALTANTE"] },
                                                "then": "$tipo_arbol_maestro"
                                            }


                                        ],
                                        "default": "otro_caso"
                                    }

                                }
                            },

                            //RESIEMBRAS
                            {
                                "case": { "$eq": ["$TIPO ARBOL", "RESIEMBRAS"] },
                                "then": {

                                    "$switch": {
                                        "branches": [
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "BUENA"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "CLOROTICA"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "MUERTA O FALTANTE"] },
                                                "then": "$tipo_arbol_maestro"
                                            }


                                        ],
                                        "default": "otro_caso"
                                    }

                                }
                            },

                            //SOCAS
                            {
                                "case": { "$eq": ["$TIPO ARBOL", "SOCAS"] },
                                "then": {

                                    "$switch": {
                                        "branches": [
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "RECUPERADAS"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "NO RECUPERADAS"] },
                                                "then": "$tipo_arbol_maestro"
                                            },
                                            //aaaaa
                                            {
                                                "case": { "$eq": ["$tipo_arbol_maestro", "INICIO BROTACION"] },
                                                "then": "$tipo_arbol_maestro"
                                            }


                                        ],
                                        "default": "otro_caso"
                                    }

                                }
                            }




                        ],
                        "default": "otro_caso"
                    }
                }
            }
        },





        {
            "$addFields": {
                "arr_type": {
                    "$concat": [
                        "$TIPO ARBOL",
                        " - ",
                        // "$tipo_arbol_maestro"
                        "$tipo_arbol_maestro_aux"
                    ]
                }
            }
        },

        // //test
        // {
        //     $group:{
        //         _id:"$arr_type"
        //         ,cant:{$sum:1}
        //     }
        // }


        /*
        //leyenda

        Tipo arboles:#FFFFFF,
        SOCAS:#ee82ee,
        Socas - Recuperadas:#2ecc71,
        Socas - No recuperadas:#e74c3c,
        Socas - Inicio brotacion:#2ecc71,
        RESIEMBRA:#ee82ee,
        Resiembra - Buena:#2ecc71,
        Resiembra - Clorotica:#e74c3c,
        Resiembra - Muerta o faltante:#2c3e50,
        ARBOLES:#ee82ee,
        Arboles - Buenos:#2ecc71,
        Arboles - Cloroticos:#e74c3c,
        Arboles - Muerto o faltante:#2c3e50,
        OTRO CASO:#808080


        */


        /*


      Socas - Recuperadas:#,
      Socas - No recuperadas:#,
      Socas - Inicio brotacion:#,


      Resiembra - Buena:#,
      Resiembra - Clorotica:#,
      Resiembra - Muerta o faltante:#,

      Arboles - Buenos:#,
      Arboles - Cloroticos:#,
      Arboles - Muerto o faltante:#,

      OTRO CASO:#


      */

        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$arr_type", "SOCAS - RECUPERADAS"] },
                                "then": "#2ecc71"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "SOCAS - NO RECUPERADAS"] },
                                "then": "#e74c3c"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "SOCAS - INICIO BROTACION"] },
                                "then": "#2ecc71"
                            },

                            {
                                "case": { "$eq": ["$arr_type", "RESIEMBRAS - BUENA"] },
                                "then": "#2ecc71"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "RESIEMBRAS - CLOROTICA"] },
                                "then": "#e74c3c"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "RESIEMBRAS - MUERTA O FALTANTE"] },
                                "then": "#2c3e50"
                            },

                            {
                                "case": { "$eq": ["$arr_type", "ARBOLES - BUENOS"] },
                                "then": "#2ecc71"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "ARBOLES - CLOROTICOS"] },
                                "then": "#e74c3c"
                            },
                            {
                                "case": { "$eq": ["$arr_type", "ARBOLES - MUERTO O FALTANTE"] },
                                "then": "#2c3e50"
                            }
                        ],
                        "default": "#808080"
                    }
                }
            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                , "color": "$color"
                // , "rango": "$rango"
            }
        }






        // {
        //     "$project": {
        //         "_id": "$elemnq",
        //         "idform": "$idform",
        //         "type": "Feature",
        //         "properties": {
        //             "Bloque": "$bloque",
        //             "Lote": "$lote",
        //             "Tipo arbol": "$arr_type",

        //             "color": "$color"
        //         },
        //         "geometry": "$Cartography"
        //     }
        // }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {

                    "Bloque": "$bloque",
                    "Lote": "$lote",
                    "Tipo": "$TIPO ARBOL",
                    "Tipo DETALLE": "$arr_type",
                    "color": "$color"

                }

            }
        }


    ]

)
