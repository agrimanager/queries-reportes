[



    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "linea": "$linea.properties.name",


            "arbol_longitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
            "arbol_latitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] },

            "arbol": "$arbol.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },

    { "$unwind": "$finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "ARBOL": 0,
            "Point": 0
        }
    }





    , {
        "$project": {


            "finca": "$finca",
            "bloque": "$bloque",
            "lote": "$lote",
            "linea": "$linea",
            "arbol": "$arbol",
            "arbol_longitud": "$arbol_longitud",
            "arbol_latitud": "$arbol_latitud",


            "FISIOPATIAS": "$FISIOPATIAS",
            "Gravedad": "$GRAVE",
            "Observaciones": "$OBSERVACIONES",


            "supervisor": "$supervisor",
            "capture": "$capture",
            "Sampling": "$Sampling",
            "Formula": "$Formula",
            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

        }
    }



]