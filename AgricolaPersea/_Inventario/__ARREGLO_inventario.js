

//=========PROCESOS BASE 
//1)hacer backup de productos
//2)hacer backup de transacciones
//3)verificar que las transacciones NO SEAN DE LABORES



// //=========PROCESOS DE ARREGLO

// // 1--reset cantidad de productos
// db.supplies.updateMany({},{$set:{quantity:0}});


// // 2--borrar TODAS las transacciones
// db.suppliesTimeline.deleteMany({});


//db.getCollection("suppliesTimeline_new_2020-06-04_1backup")
//--query para ingresos
db.getCollection("suppliesTimeline_new_2020-06-04_1backup").aggregate(
    {
        $match: {
            quantity: { $gt: 0 },
            deleted: false,
            lbid: ""
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    { $unwind: "$productos" }
    , {
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    { $unwind: "$bodegas" }
    , {
        "$project": {
            //--info
            "name": "$productos.name",
            "wid": "$bodegas.name",
            "type": "Ingresos",

            //-----!!!DANGER!!!
            //--CANTIDADES Y UNIDADES
            //"quantity": "$productEquivalence",//--eror
            "quantity": "$quantity",
            "inputmeasure": "$inputmeasure",


            //--costos
            "tax": {
                $cond: {
                    if: {
                        $eq: ["$tax", ""]
                    },
                    then: 0,
                    else: "$tax"
                }
            },
            "subtotal": "$subtotal",

            //--fechas
            "rgDate": { $dateToString: { format: "%Y-%m-%d", date: "$rgDate" } },
            "invoiceDate": {
                $cond: {
                    if: {
                        $eq: ["$invoiceDate", ""]
                    },
                    then: "",
                    else: { $dateToString: { format: "%Y-%m-%d", date: "$invoiceDate" } }
                }
            },
            "dueDate": {
                $cond: {
                    if: {
                        $eq: ["$dueDate", ""]
                    },
                    then: "",
                    else: { $dateToString: { format: "%Y-%m-%d", date: "$dueDate" } }
                }
            },

            //--extra
            "invoiceNumber": "$invoiceNumber",
            "inf": "$information",


            //--labores
            "lbid": "",
            "lbdoce": ""
        }
    }

    // , { "$project": { "_id": 0 } }
);



//----------datos con " (comillas dobles)
//---------------------------------------

--en archivo .xlsx
reemplazar " por xxx
guardar archivo como .csv UTF


--en archivo .csv
abrir archivo en bloc de notas
reemplazar xxx por "
guardar archivo con modificacion




//....importar datos en la web