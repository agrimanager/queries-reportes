
//---unir datos
db.suppliesTimeline.aggregate(

    {
        $group: {
            _id: "data_suppliesTimeline"
            , data: { $push: "$$ROOT" }
        }
    }

    , {
        "$lookup": {
            "from": "suppliesTimeline_fix",
            "as": "data_fix",
            "let": {},
            //query 
            "pipeline": []
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    ,"$data_fix"
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



)



//---buscar datos nuevos (que no estan en fix)
db.suppliesTimeline.aggregate(

    {
        $addFields: {
            "fecha_id": { $toDate: "$_id" }
        }
    }

    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$fecha_id" } },
            "num_mes": { "$month": { "date": "$fecha_id" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$fecha_id" } },
        }
    }
    
    // ,{
    //     $group: {
    //         _id: "$num_dia_mes"
    //         //,cantidad:{$sum:1}
    //         ,data:{$push:"$$ROOT"}
    //     }
    // }
    ,{$sort:{"fecha_id":-1}}
    ,{$limit: 219}
    
    ,{
        $out:"suppliesTimeline_new"
    }
)

