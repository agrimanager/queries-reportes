db.suppliesTimeline.aggregate(

    //---agricolapersea
    //MEZCLA FISICA RIO CLARO -- 60d9c717c149d3687cce46d8
    //SULFATO DE MAGNESIO HEPTAHIDRATADO MANUCHAR 0-0-0-17  -- 601ab555aa14823735de82e6
    //SULFATO DE MAGNESIO HEPTAHIDRATADO TECNIFEED  --  601ac111aa14823735de8336


    {
        $match: {
            lbcod: { $ne: "" }
            //sid: { $eq: ObjectId("60d9c717c149d3687cce46d8") }
        }
    },

    {
        $group: {
            _id: {
                lbcod: "$lbcod",
                sid: "$sid"
            },
            cantidad: { $sum: 1 }
        }
    },

    {
        $match: {
            cantidad: {
                $ne: 1
            }
        }
    }


    // ,{
    //     $group: {
    //         _id: {
    //             // lbcod: "$lbcod",
    //             sid: "$_id.sid"
    //         },
    //         cantidad: { $sum: 1 }
    //     }
    // },

    , {
        "$lookup": {
            "from": "supplies",
            "localField": "_id.sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    { $unwind: "$productos" }






)
    .sort({ cantidad: -1 })
