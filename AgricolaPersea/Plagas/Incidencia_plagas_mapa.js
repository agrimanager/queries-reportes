db.form_monitoreodeplagas.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-06-17T23:45:15.000-05:00"),
                "Busqueda fin": ISODate("2021-09-17T12:45:15.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            $eq: ["$Point.farm", "5dadd2c34f74d529196b2637"]//la linda
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",


                "arbol_longitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                "arbol_latitud": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] },
                "arbol_geometry": "$arbol.geometry",

                "arbol": "$arbol.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },

        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "ARBOL": 0,
                "Point": 0
            }
        }



        , {
            "$project": {
                "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },



        {
            "$addFields": {
                "nombre_maestro_principal": "PLAGA_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }



        , {
            "$addFields": {
                "valor_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , {
            "$unwind": {
                "path": "$valor_mestro_enlazado",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0
            }
        }


        , {
            "$project": {


                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",
                "arbol_geometry": "$arbol_geometry", //mapa
                "arbol_longitud": "$arbol_longitud",
                "arbol_latitud": "$arbol_latitud",


                "Plaga": "$PLAGA",
                "Tipo de Plaga": "$valor_mestro_enlazado",

                "Gravedad": "$GRAVE",
                "Observaciones": "$OBSERVACIONES",


                "supervisor": "$supervisor",
                "capture": "$capture",
                "Sampling": "$Sampling",
                "Formula": "$Formula",
                "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                "Semana": { "$isoWeek": "$rgDate" }

            }
        },


        // [
        //     {
        //         "name": "COLEOPTEROS",
        //         "color": "#82FC08",
        //         "master": "5dcaf5133f5f2656118c7513",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "LEPIDOPTEROS",
        //         "color": "#D50D0D",
        //         "master": "5dcb50013f5f2656118c7524",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "THYSANOPTERA",
        //         "color": "#F2B314",
        //         "master": "5dcb517d3f5f2656118c752e",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "ACAROS",
        //         "color": "#001EFF",
        //         "master": "5dcb51133f5f2656118c7529",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "DIPTERA",
        //         "color": "#CD0EBD",
        //         "master": "5dcb581e3f5f2656118c756a",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "HEMIPTEROS",
        //         "color": "#0FE0D9",
        //         "master": "5dcc53a23f5f2656118c7590",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     },
        //     {
        //         "name": "SUELDA",
        //         "color": "#1E1919",
        //         "master": "5daf20cc1851410cc16d8697",
        //         "consecutives": "days",
        //         "incidenceFrequency": NumberInt(8),
        //         "incidencePercentage": NumberInt(4)
        //     }
        // ]


        // //---color segun plaga

        // ACAROS: #001EFF,
        // COLEOPTEROS: #82FC08,
        // DIPTERA: #CD0EBD,
        // HEMIPTEROS: #0FE0D9,
        // LEPIDOPTEROS: #D50D0D,
        // SUELDA: #1E1919,
        // THYSANOPTERA: #F2B314,

        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$Plaga", "ACAROS"] }, "then": "#001EFF" },
                            { "case": { "$eq": ["$Plaga", "COLEOPTEROS"] }, "then": "#82FC08" },
                            { "case": { "$eq": ["$Plaga", "DIPTERA"] }, "then": "#CD0EBD" },
                            { "case": { "$eq": ["$Plaga", "HEMIPTEROS"] }, "then": "#0FE0D9" },
                            { "case": { "$eq": ["$Plaga", "LEPIDOPTEROS"] }, "then": "#D50D0D" },
                            { "case": { "$eq": ["$Plaga", "SUELDA"] }, "then": "#1E1919" },
                            { "case": { "$eq": ["$Plaga", "THYSANOPTERA"] }, "then": "#F2B314" }
                        ],
                        "default": "#FFFFFF"
                    }
                }
            }
        },




        //-----PROYECCION FINAL
        {
            "$project": {
                "_id": "$elemnq",
                "idform": "$idorm",
                "type": "Feature",
                "properties": {
                    "color": "$color"

                    , "Lote": "$lote"
                    , "Plaga": "$Plaga"
                    , "Tipo de Plaga": "$Tipo de Plaga"
                    , "Gravedad": "$Gravedad"

                    , "Semana": "$Semana"
                    , "supervisor": "$supervisor"

                },
                "geometry": "$arbol_geometry"
            }
        }




    ], { allowDiskUse: true }


)
