[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Arbol": 0
            , "uid": 0
            , "uDate": 0

            , "Formula": 0,
            "Crecimiento de Fruta Rama 1": 0,
            "Crecimiento de Fruta Rama 2": 0,
            "Crecimiento de Fruta Rama 3": 0,
            "Crecimiento de Fruta Rama 4": 0
        }
    }

    , {
        "$addFields": {
            "variables_numeros_lista": [
                "D Ecuatorial A R1F1",
                "D Ecuatorial B R1F1",
                "D Longitudinal R1F1",
                "D Ecuatorial A R1F2",
                "D Ecuatorial B R1F2",
                "D Longitudinal R1F2",
                "D Ecuatorial A R1F3",
                "D Ecuatorial B R1F3",
                "D Longitudinal R1F3",
                "D Ecuatorial A R1F4",
                "D Ecuatorial B R1F4",
                "D Longitudinal R1F4",
                "D Ecuatorial A R2F1",
                "D Ecuatorial B R2F1",
                "D Longitudinal R2F1",
                "D Ecuatorial A R2F2",
                "D Ecuatorial B R2F2",
                "D Longitudinal R2F2",
                "D Ecuatorial A R2F3",
                "D Ecuatorial B R2F3",
                "D Longitudinal R2F3",
                "D Ecuatorial A R2F4",
                "D Ecuatorial B R2F4",
                "D Longitudinal R2F4",
                "D Ecuatorial A R3F1",
                "D Ecuatorial B R3F1",
                "D Longitudinal R3F1",
                "D Ecuatorial A R3F2",
                "D Ecuatorial B R3F2",
                "D Longitudinal R3F2",
                "D Ecuatorial A R3F3",
                "D Ecuatorial B R3F3",
                "D Longitudinal R3F3",
                "D Ecuatorial A R3F4",
                "D Ecuatorial B R3F4",
                "D Longitudinal R3F4",
                "D Ecuatorial A R4F1",
                "D Ecuatorial B R4F1",
                "D Longitudinal R4F1",
                "D Ecuatorial A R4F2",
                "D Ecuatorial B R4F2",
                "D Longitudinal R4F2",
                "D Ecuatorial A R4F3",
                "D Ecuatorial B R4F3",
                "D Longitudinal R4F3",
                "D Ecuatorial A R4F4",
                "D Ecuatorial B R4F4",
                "D Longitudinal R4F4"


            ]
        }
    }

    , {
        "$addFields": {
            "variables_numeros": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "key": "$$dataKV.k"
                                , "type": { "$type": "$$dataKV.v" }
                                , "value": "$$dataKV.v"

                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$in": ["$$item.key", "$variables_numeros_lista"] }
                }
            }
        }
    }

    , {
        "$project": {
            "variables_numeros_lista": 0
        }
    }

    , {
        "$addFields": {
            "variables_numeros": {
                "$map": {
                    "input": "$variables_numeros",
                    "as": "variable",
                    "in": {
                        "key": "$$variable.key"
                        , "value": {
                            "$cond": {
                                "if": { "$eq": ["$$variable.type", "string"] },
                                "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$variable.value" } } } } },
                                "else": "$$variable.value"
                            }
                        }
                    }
                }
            }
        }
    }

    , {
        "$project": {
            "variables_numeros": 0
        }
    }


    , { "$addFields": { "D Ecuatorial A R1F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R1F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R1F1" } } } } }, "else": "$D Ecuatorial A R1F1" } } } }
    , { "$addFields": { "D Ecuatorial B R1F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R1F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R1F1" } } } } }, "else": "$D Ecuatorial B R1F1" } } } }
    , { "$addFields": { "D Longitudinal R1F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R1F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R1F1" } } } } }, "else": "$D Longitudinal R1F1" } } } }
    , { "$addFields": { "D Ecuatorial A R1F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R1F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R1F2" } } } } }, "else": "$D Ecuatorial A R1F2" } } } }
    , { "$addFields": { "D Ecuatorial B R1F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R1F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R1F2" } } } } }, "else": "$D Ecuatorial B R1F2" } } } }
    , { "$addFields": { "D Longitudinal R1F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R1F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R1F2" } } } } }, "else": "$D Longitudinal R1F2" } } } }
    , { "$addFields": { "D Ecuatorial A R1F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R1F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R1F3" } } } } }, "else": "$D Ecuatorial A R1F3" } } } }
    , { "$addFields": { "D Ecuatorial B R1F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R1F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R1F3" } } } } }, "else": "$D Ecuatorial B R1F3" } } } }
    , { "$addFields": { "D Longitudinal R1F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R1F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R1F3" } } } } }, "else": "$D Longitudinal R1F3" } } } }
    , { "$addFields": { "D Ecuatorial A R1F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R1F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R1F4" } } } } }, "else": "$D Ecuatorial A R1F4" } } } }
    , { "$addFields": { "D Ecuatorial B R1F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R1F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R1F4" } } } } }, "else": "$D Ecuatorial B R1F4" } } } }
    , { "$addFields": { "D Longitudinal R1F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R1F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R1F4" } } } } }, "else": "$D Longitudinal R1F4" } } } }
    , { "$addFields": { "D Ecuatorial A R2F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R2F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R2F1" } } } } }, "else": "$D Ecuatorial A R2F1" } } } }
    , { "$addFields": { "D Ecuatorial B R2F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R2F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R2F1" } } } } }, "else": "$D Ecuatorial B R2F1" } } } }
    , { "$addFields": { "D Longitudinal R2F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R2F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R2F1" } } } } }, "else": "$D Longitudinal R2F1" } } } }
    , { "$addFields": { "D Ecuatorial A R2F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R2F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R2F2" } } } } }, "else": "$D Ecuatorial A R2F2" } } } }
    , { "$addFields": { "D Ecuatorial B R2F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R2F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R2F2" } } } } }, "else": "$D Ecuatorial B R2F2" } } } }
    , { "$addFields": { "D Longitudinal R2F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R2F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R2F2" } } } } }, "else": "$D Longitudinal R2F2" } } } }
    , { "$addFields": { "D Ecuatorial A R2F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R2F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R2F3" } } } } }, "else": "$D Ecuatorial A R2F3" } } } }
    , { "$addFields": { "D Ecuatorial B R2F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R2F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R2F3" } } } } }, "else": "$D Ecuatorial B R2F3" } } } }
    , { "$addFields": { "D Longitudinal R2F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R2F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R2F3" } } } } }, "else": "$D Longitudinal R2F3" } } } }
    , { "$addFields": { "D Ecuatorial A R2F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R2F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R2F4" } } } } }, "else": "$D Ecuatorial A R2F4" } } } }
    , { "$addFields": { "D Ecuatorial B R2F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R2F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R2F4" } } } } }, "else": "$D Ecuatorial B R2F4" } } } }
    , { "$addFields": { "D Longitudinal R2F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R2F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R2F4" } } } } }, "else": "$D Longitudinal R2F4" } } } }
    , { "$addFields": { "D Ecuatorial A R3F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R3F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R3F1" } } } } }, "else": "$D Ecuatorial A R3F1" } } } }
    , { "$addFields": { "D Ecuatorial B R3F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R3F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R3F1" } } } } }, "else": "$D Ecuatorial B R3F1" } } } }
    , { "$addFields": { "D Longitudinal R3F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R3F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R3F1" } } } } }, "else": "$D Longitudinal R3F1" } } } }
    , { "$addFields": { "D Ecuatorial A R3F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R3F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R3F2" } } } } }, "else": "$D Ecuatorial A R3F2" } } } }
    , { "$addFields": { "D Ecuatorial B R3F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R3F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R3F2" } } } } }, "else": "$D Ecuatorial B R3F2" } } } }
    , { "$addFields": { "D Longitudinal R3F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R3F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R3F2" } } } } }, "else": "$D Longitudinal R3F2" } } } }
    , { "$addFields": { "D Ecuatorial A R3F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R3F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R3F3" } } } } }, "else": "$D Ecuatorial A R3F3" } } } }
    , { "$addFields": { "D Ecuatorial B R3F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R3F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R3F3" } } } } }, "else": "$D Ecuatorial B R3F3" } } } }
    , { "$addFields": { "D Longitudinal R3F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R3F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R3F3" } } } } }, "else": "$D Longitudinal R3F3" } } } }
    , { "$addFields": { "D Ecuatorial A R3F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R3F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R3F4" } } } } }, "else": "$D Ecuatorial A R3F4" } } } }
    , { "$addFields": { "D Ecuatorial B R3F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R3F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R3F4" } } } } }, "else": "$D Ecuatorial B R3F4" } } } }
    , { "$addFields": { "D Longitudinal R3F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R3F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R3F4" } } } } }, "else": "$D Longitudinal R3F4" } } } }
    , { "$addFields": { "D Ecuatorial A R4F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R4F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R4F1" } } } } }, "else": "$D Ecuatorial A R4F1" } } } }
    , { "$addFields": { "D Ecuatorial B R4F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R4F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R4F1" } } } } }, "else": "$D Ecuatorial B R4F1" } } } }
    , { "$addFields": { "D Longitudinal R4F1": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R4F1" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R4F1" } } } } }, "else": "$D Longitudinal R4F1" } } } }
    , { "$addFields": { "D Ecuatorial A R4F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R4F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R4F2" } } } } }, "else": "$D Ecuatorial A R4F2" } } } }
    , { "$addFields": { "D Ecuatorial B R4F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R4F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R4F2" } } } } }, "else": "$D Ecuatorial B R4F2" } } } }
    , { "$addFields": { "D Longitudinal R4F2": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R4F2" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R4F2" } } } } }, "else": "$D Longitudinal R4F2" } } } }
    , { "$addFields": { "D Ecuatorial A R4F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R4F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R4F3" } } } } }, "else": "$D Ecuatorial A R4F3" } } } }
    , { "$addFields": { "D Ecuatorial B R4F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R4F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R4F3" } } } } }, "else": "$D Ecuatorial B R4F3" } } } }
    , { "$addFields": { "D Longitudinal R4F3": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R4F3" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R4F3" } } } } }, "else": "$D Longitudinal R4F3" } } } }
    , { "$addFields": { "D Ecuatorial A R4F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial A R4F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial A R4F4" } } } } }, "else": "$D Ecuatorial A R4F4" } } } }
    , { "$addFields": { "D Ecuatorial B R4F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Ecuatorial B R4F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Ecuatorial B R4F4" } } } } }, "else": "$D Ecuatorial B R4F4" } } } }
    , { "$addFields": { "D Longitudinal R4F4": { "$cond": { "if": { "$eq": [{ "$type": "$D Longitudinal R4F4" }, "string"] }, "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$D Longitudinal R4F4" } } } } }, "else": "$D Longitudinal R4F4" } } } }




    , {
        "$addFields": {
            "D_prom_R1F1": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R1F1",
                            "$D Ecuatorial B R1F1",
                            "$D Longitudinal R1F1"

                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R1F2": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R1F2",
                            "$D Ecuatorial B R1F2",
                            "$D Longitudinal R1F2"

                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R1F3": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R1F3",
                            "$D Ecuatorial B R1F3",
                            "$D Longitudinal R1F3"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R1F4": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R1F4",
                            "$D Ecuatorial B R1F4",
                            "$D Longitudinal R1F4"
                        ]
                    }
                    , 3]
            }
        }
    }

    , {
        "$addFields": {
            "D_prom_R2F1": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R2F1",
                            "$D Ecuatorial B R2F1",
                            "$D Longitudinal R2F1"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R2F2": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R2F2",
                            "$D Ecuatorial B R2F2",
                            "$D Longitudinal R2F2"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R2F3": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R2F3",
                            "$D Ecuatorial B R2F3",
                            "$D Longitudinal R2F3"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R2F4": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R2F4",
                            "$D Ecuatorial B R2F4",
                            "$D Longitudinal R2F4"
                        ]
                    }
                    , 3]
            }
        }
    }

    , {
        "$addFields": {
            "D_prom_R3F1": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R3F1",
                            "$D Ecuatorial B R3F1",
                            "$D Longitudinal R3F1"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R3F2": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R3F2",
                            "$D Ecuatorial B R3F2",
                            "$D Longitudinal R3F2"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R3F3": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R3F3",
                            "$D Ecuatorial B R3F3",
                            "$D Longitudinal R3F3"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R3F4": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R3F4",
                            "$D Ecuatorial B R3F4",
                            "$D Longitudinal R3F4"

                        ]
                    }
                    , 3]
            }
        }
    }

    , {
        "$addFields": {
            "D_prom_R4F1": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R4F1",
                            "$D Ecuatorial B R4F1",
                            "$D Longitudinal R4F1"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R4F2": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R4F2",
                            "$D Ecuatorial B R4F2",
                            "$D Longitudinal R4F2"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R4F3": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R4F3",
                            "$D Ecuatorial B R4F3",
                            "$D Longitudinal R4F3"
                        ]
                    }
                    , 3]
            }
        }
    }
    , {
        "$addFields": {
            "D_prom_R4F4": {
                "$divide": [
                    {
                        "$sum": [
                            "$D Ecuatorial A R4F4",
                            "$D Ecuatorial B R4F4",
                            "$D Longitudinal R4F4"
                        ]
                    }
                    , 3]
            }
        }
    }



    , { "$addFields": { "D_prom_R1F1": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R1F1", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R1F1", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R1F2": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R1F2", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R1F2", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R1F3": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R1F3", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R1F3", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R1F4": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R1F4", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R1F4", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R2F1": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R2F1", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R2F1", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R2F2": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R2F2", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R2F2", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R2F3": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R2F3", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R2F3", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R2F4": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R2F4", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R2F4", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R3F1": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R3F1", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R3F1", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R3F2": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R3F2", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R3F2", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R3F3": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R3F3", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R3F3", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R3F4": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R3F4", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R3F4", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R4F1": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R4F1", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R4F1", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R4F2": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R4F2", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R4F2", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R4F3": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R4F3", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R4F3", 100] }, 1] }] }, 100] } } }
    , { "$addFields": { "D_prom_R4F4": { "$divide": [{ "$subtract": [{ "$multiply": ["$D_prom_R4F4", 100] }, { "$mod": [{ "$multiply": ["$D_prom_R4F4", 100] }, 1] }] }, 100] } } }



    , {
        "$addFields": {
            "data_D_prom": [
                "$D_prom_R1F1",
                "$D_prom_R1F2",
                "$D_prom_R1F3",
                "$D_prom_R1F4",
                "$D_prom_R2F1",
                "$D_prom_R2F2",
                "$D_prom_R2F3",
                "$D_prom_R2F4",
                "$D_prom_R3F1",
                "$D_prom_R3F2",
                "$D_prom_R3F3",
                "$D_prom_R3F4",
                "$D_prom_R4F1",
                "$D_prom_R4F2",
                "$D_prom_R4F3",
                "$D_prom_R4F4"
            ]
        }
    }

    , {
        "$addFields": {
            "data_D_prom": {
                "$filter": {
                    "input": "$data_D_prom",
                    "as": "item",
                    "cond": { "$ne": ["$$item", 0] }
                }
            }
        }
    }


    , {
        "$addFields": {
            "CANTIDAD FRUTOS EVALUADOS": {
                "$size": "$data_D_prom"
            }
        }
    }


    , {
        "$addFields": {
            "DIAMETRO PROMEDIO FRUTA": {
                "$avg": "$data_D_prom"
            }
        }
    }

    , { "$addFields": { "DIAMETRO PROMEDIO FRUTA": { "$divide": [{ "$subtract": [{ "$multiply": ["$DIAMETRO PROMEDIO FRUTA", 100] }, { "$mod": [{ "$multiply": ["$DIAMETRO PROMEDIO FRUTA", 100] }, 1] }] }, 100] } } }

    , {
        "$project": {
            "data_D_prom": 0
        }
    }




    , {
        "$addFields": {
            "RADIO PROMEDIO FRUTA": {
                "$divide": [
                    "$DIAMETRO PROMEDIO FRUTA",
                    2
                ]
            }
        }
    }

    , { "$addFields": { "RADIO PROMEDIO FRUTA": { "$divide": [{ "$subtract": [{ "$multiply": ["$RADIO PROMEDIO FRUTA", 100] }, { "$mod": [{ "$multiply": ["$RADIO PROMEDIO FRUTA", 100] }, 1] }] }, 100] } } }


    , {
        "$addFields": {
            "r3": {
                "$multiply": [
                    "$RADIO PROMEDIO FRUTA",
                    "$RADIO PROMEDIO FRUTA",
                    "$RADIO PROMEDIO FRUTA"
                ]
            }
        }
    }


    , {
        "$addFields": {
            "4_3_pi": 4.18879
        }
    }

    , {
        "$addFields": {
            "VOLUMEN PROMEDIO FRUTA": {
                "$multiply": [
                    "$4_3_pi",
                    "$r3"
                ]
            }
        }
    }

    , { "$addFields": { "VOLUMEN PROMEDIO FRUTA": { "$divide": [{ "$subtract": [{ "$multiply": ["$VOLUMEN PROMEDIO FRUTA", 100] }, { "$mod": [{ "$multiply": ["$VOLUMEN PROMEDIO FRUTA", 100] }, 1] }] }, 100] } } }

    , {
        "$project": {
            "r3": 0
            , "4_3_pi": 0
        }
    }


    , {
        "$addFields": {
            "Fecha": {
                "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" }
            }
        }
    }



    , {
        "$project": {

            "user": 0,
            "Keys": 0,
            "Finca nombre": 0,
            "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
            "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,

            "Formula": 0,
            "uDate": 0,
            "Busqueda inicio": 0,
            "Busqueda fin": 0,
            "today": 0,
            "FincaID": 0,
            "uid": 0

            , "Point": 0
            , "rgDate": 0
        }
    }


]
