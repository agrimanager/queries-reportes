[




    {
        "$match": {

            "Olig Hojas Afectadas": { "$exists": true }
        }
    },



    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "Trips Total Individuos por Arbol": {
                "$ifNull": ["$Trips Total Individuos por Arbol", {
                    "$ifNull": ["$Total Individuos por Arbol", 0]
                }]
            }
        }
    },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Arbol": 0


            , "Formula": 0
            , "uid": 0
            , "uDate": 0,


            "Oligonychus": 0,
            "Monalonion": 0,
            "Stenoma": 0,
            "Mosca del Ovario": 0,
            "Trips": 0,
            "Compsus": 0,
            "Copturomimus Persea": 0,
            "Amorbia": 0,
            "Xyleborus": 0,
            "Astaena": 0,
            "Heilipus Lauri": 0,
            "Chysomelidae": 0,
            "Beneficos": 0,
            "Antracnosis": 0,
            "Verticillium": 0,
            "Lasiodiplodia": 0,
            "Fenologia": 0

            , "user": 0,
            "Keys": 0,
            "Finca nombre": 0,
            "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
            "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,
            "Busqueda inicio": 0,
            "Busqueda fin": 0,
            "today": 0,
            "FincaID": 0

            , "Total Individuos por Arbol": 0

        }
    }


    , {
        "$addFields": {
            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",

                "etapa": "$Etapa"
            },
            "arboles_x_lote_x_Etapa": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data_array_group": {
                "$map": {
                    "input": "$data"
                    , "as": "item_data"
                    , "in": {
                        "etapa": "$$item_data._id.etapa"
                        , "arboles_x_lote_x_Etapa": "$$item_data.arboles_x_lote_x_Etapa"
                    }
                }
            }
        }
    }



    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "arboles_x_lote_x_Etapa": "$data.arboles_x_lote_x_Etapa"
                        , "data_array_group_etapa": "$data_array_group"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",

                "estado": "$Estado"
            },
            "arboles_x_lote_x_Estado": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data_array_group": {
                "$map": {
                    "input": "$data"
                    , "as": "item_data"
                    , "in": {
                        "estado": "$$item_data._id.estado"
                        , "arboles_x_lote_x_Estado": "$$item_data.arboles_x_lote_x_Estado"
                    }
                }
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "arboles_x_lote_x_Estado": "$data.arboles_x_lote_x_Estado"
                        , "data_array_group_estado": "$data_array_group"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"

                , "arveses": "$Arveses"
                , "categora_arvenses": "$Categora de Arvenses"
            },
            "arboles_x_lote_x_Arveses_y_Categora_arvenses": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data_array_group": {
                "$map": {
                    "input": "$data"
                    , "as": "item_data"
                    , "in": {
                        "arveses": "$$item_data._id.arveses"
                        , "categora_arvenses": "$$item_data._id.categora_arvenses"
                        , "arboles_x_lote_x_Arveses_y_Categora_arvenses": "$$item_data.arboles_x_lote_x_Arveses_y_Categora_arvenses"
                    }
                }
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "arboles_x_lote_x_Arveses_y_Categora_arvenses": "$data.arboles_x_lote_x_Arveses_y_Categora_arvenses"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",

                "complejo_de_raiz": "$Complejo de Raiz"

                , "etapa": "$Etapa"
            },
            "arboles_x_lote_x_complejo_de_raiz": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data_array_group": {
                "$map": {
                    "input": "$data"
                    , "as": "item_data"
                    , "in": {
                        "complejo_de_raiz": "$$item_data._id.complejo_de_raiz"
                        , "arboles_x_lote_x_complejo_de_raiz": "$$item_data.arboles_x_lote_x_complejo_de_raiz"
                    }
                }
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "arboles_x_lote_x_complejo_de_raiz": "$data.arboles_x_lote_x_complejo_de_raiz"

                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "array_data": [


                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": ""
                    , "estado": "Huevo"
                    , "valor": { "$ifNull": ["$Olig Huevo", ""] }
                    , "nombre_variable": "Olig Huevo"
                },
                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": ""
                    , "estado": "Ninfa"
                    , "valor": { "$ifNull": ["$Olig Ninfa", ""] }
                    , "nombre_variable": "Olig Ninfa"
                },
                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": ""
                    , "estado": "Larva"
                    , "valor": { "$ifNull": ["$Olig Larva", ""] }
                    , "nombre_variable": "Olig Larva"
                },
                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": ""
                    , "estado": "Adulto"
                    , "valor": { "$ifNull": ["$OligAdulto", ""] }
                    , "nombre_variable": "OligAdulto"
                },
                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": "Hoja"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Olig Hojas Afectadas", ""] }
                    , "nombre_variable": "Olig Hojas Afectadas"
                },
                {
                    "plaga_o_enfermedad": "Oligonychus"
                    , "organo": ""
                    , "estado": "Severidad"
                    , "valor": { "$ifNull": ["$Olig Severidad", ""] }
                    , "nombre_variable": "Olig Severidad"
                },




                {
                    "plaga_o_enfermedad": "Monalonion"
                    , "organo": "Fruto"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Fruto Afectado por Monalonion", ""] }
                    , "nombre_variable": "Fruto Afectado por Monalonion"
                },
                {
                    "plaga_o_enfermedad": "Monalonion"
                    , "organo": "Terminal"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Terminal Afectado por Monalonion", ""] }
                    , "nombre_variable": "Terminal Afectado por Monalonion"
                },
                {
                    "plaga_o_enfermedad": "Monalonion"
                    , "organo": ""
                    , "estado": "Ninfa"
                    , "valor": { "$ifNull": ["$Mon Ninfa", ""] }
                    , "nombre_variable": "Mon Ninfa"
                },
                {
                    "plaga_o_enfermedad": "Monalonion"
                    , "organo": ""
                    , "estado": "Adulto"
                    , "valor": { "$ifNull": ["$Mon Adulto", ""] }
                    , "nombre_variable": "Mon Adulto"
                },


                {
                    "plaga_o_enfermedad": "Stenoma"
                    , "organo": "Rama"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Ramas Afectadas por Stenoma", ""] }
                    , "nombre_variable": "Ramas Afectadas por Stenoma"
                },

                {
                    "plaga_o_enfermedad": "Stenoma"
                    , "organo": "Fruto"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Frutos Afectados por Stenoma", ""] }
                    , "nombre_variable": "Frutos Afectados por Stenoma"
                },



                {
                    "plaga_o_enfermedad": "Mosca del Ovario"
                    , "organo": "Racimo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Mosca del Ov Racimo", ""] }
                    , "nombre_variable": "Mosca del Ov Racimo"
                },
                {
                    "plaga_o_enfermedad": "Mosca del Ovario"
                    , "organo": "Fruto"
                    , "estado": "Cuaje A"
                    , "valor": { "$ifNull": ["$Mosca del Ov Cuaje A", ""] }
                    , "nombre_variable": "Mosca del Ov Cuaje A"
                },
                {
                    "plaga_o_enfermedad": "Mosca del Ovario"
                    , "organo": "Fruto"
                    , "estado": "Cuaje T"
                    , "valor": { "$ifNull": ["$Mosca del Ov Cuaje T", ""] }
                    , "nombre_variable": "Mosca del Ov Cuaje T"
                },



                {
                    "plaga_o_enfermedad": "Trips"
                    , "organo": "Racimo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Trips Racimo", ""] }
                    , "nombre_variable": "Trips Racimo"
                },
                {
                    "plaga_o_enfermedad": "Trips"
                    , "organo": "Fruto"
                    , "estado": "Cuaje A"
                    , "valor": { "$ifNull": ["$Trips Cuaje A", ""] }
                    , "nombre_variable": "Trips Cuaje A"

                },
                {
                    "plaga_o_enfermedad": "Trips"
                    , "organo": "Fruto"
                    , "estado": "Cuaje T"
                    , "valor": { "$ifNull": ["$Trips Cuaje T", ""] }
                    , "nombre_variable": "Trips Cuaje T"

                },
                {
                    "plaga_o_enfermedad": "Trips"
                    , "organo": ""
                    , "estado": "Adulto"
                    , "valor": { "$ifNull": ["$Trips Total Individuos por Arbol", ""] }
                    , "nombre_variable": "Trips Total Individuos por Arbol"
                },
                {
                    "plaga_o_enfermedad": "Trips"
                    , "organo": ""
                    , "estado": "Severidad"
                    , "valor": { "$ifNull": ["$Trips Severidad", ""] }
                    , "nombre_variable": "Trips Severidad"
                },


                {
                    "plaga_o_enfermedad": "Compsus"
                    , "organo": "Hoja"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Compsus Hojas Afectadas", ""] }
                    , "nombre_variable": "Compsus Hojas Afectadas"
                },
                {
                    "plaga_o_enfermedad": "Compsus"
                    , "organo": ""
                    , "estado": "Huevo"
                    , "valor": { "$ifNull": ["$Compsus Huevo", ""] }
                    , "nombre_variable": "Compsus Huevo"
                },
                {
                    "plaga_o_enfermedad": "Compsus"
                    , "organo": ""
                    , "estado": "Adulto"
                    , "valor": { "$ifNull": ["$Compsus Adulto", ""] }
                    , "nombre_variable": "Compsus Adulto"
                },
                {
                    "plaga_o_enfermedad": "Compsus"
                    , "organo": ""
                    , "estado": "Severidad"
                    , "valor": { "$ifNull": ["$Compsus Severidad", ""] }
                    , "nombre_variable": "Compsus Severidad"
                },



                {
                    "plaga_o_enfermedad": "Copturomimus Persea"
                    , "organo": "Rama"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Copturomimus Ramas Afectadas", ""] }
                    , "nombre_variable": "Copturomimus Ramas Afectadas"
                },

                {
                    "plaga_o_enfermedad": "Copturomimus Persea"
                    , "organo": "Tallo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Copturomimus Tallos Afectados", ""] }
                    , "nombre_variable": "Copturomimus Tallos Afectados"
                },


                {
                    "plaga_o_enfermedad": "Amorbia"
                    , "organo": "Fruto"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Amorbia Frutos Afectados", ""] }
                    , "nombre_variable": "Amorbia Frutos Afectados"
                },


                {
                    "plaga_o_enfermedad": "Xyleborus"
                    , "organo": "Tallo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Xyleborus Tallo Afectado", ""] }
                    , "nombre_variable": "Xyleborus Tallo Afectado"
                },



                {
                    "plaga_o_enfermedad": "Astaena"
                    , "organo": "Hoja"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Astaena Hojas Afectadas", ""] }
                    , "nombre_variable": "Astaena Hojas Afectadas"
                },
                {
                    "plaga_o_enfermedad": "Astaena"
                    , "organo": "Fruto"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Astaena Frutos Afectados", ""] }
                    , "nombre_variable": "Astaena Frutos Afectados"
                },
                {
                    "plaga_o_enfermedad": "Astaena"
                    , "organo": "Terminal"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Astaena Terminales Afectadas", ""] }
                    , "nombre_variable": "Astaena Terminales Afectadas"
                },


                {
                    "plaga_o_enfermedad": "Heilipus Lauri"
                    , "organo": ""
                    , "estado": "Huevo"
                    , "valor": { "$ifNull": ["$Heilipus Huevo", ""] }
                    , "nombre_variable": "Heilipus Huevo"
                },
                {
                    "plaga_o_enfermedad": "Heilipus Lauri"
                    , "organo": ""
                    , "estado": "Larva"
                    , "valor": { "$ifNull": ["$Heilipus Larva", ""] }
                    , "nombre_variable": "Heilipus Larva"
                },
                {
                    "plaga_o_enfermedad": "Heilipus Lauri"
                    , "organo": ""
                    , "estado": "Adulto"
                    , "valor": { "$ifNull": ["$Heilipus Adulo", ""] }
                    , "nombre_variable": "Heilipus Adulo"
                },





                {
                    "plaga_o_enfermedad": "Chysomelidae"
                    , "organo": "Hoja"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Chysomelidae Hojas Afectadas", ""] }
                    , "nombre_variable": "Chysomelidae Hojas Afectadas"
                },
                {
                    "plaga_o_enfermedad": "Chysomelidae"
                    , "organo": "Terminal"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Chysomelidae Terminales Afectadas", ""] }
                    , "nombre_variable": "Chysomelidae Terminales Afectadas"
                },


                {
                    "plaga_o_enfermedad": "Crisopa"
                    , "organo": ""
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Crisopa", ""] }
                    , "nombre_variable": "Crisopa"
                },
                {
                    "plaga_o_enfermedad": "Abeja"
                    , "organo": ""
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Abeja", ""] }
                    , "nombre_variable": "Abeja"
                },




                {
                    "plaga_o_enfermedad": "Antracnosis"
                    , "organo": "Fruto"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Frutos por Antracnosis", ""] }
                    , "nombre_variable": "Frutos por Antracnosis"
                },

                {
                    "plaga_o_enfermedad": "Antracnosis"
                    , "organo": "Racimo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Racimos por Antracnosis", ""] }
                    , "nombre_variable": "Racimos por Antracnosis"
                },


                {
                    "plaga_o_enfermedad": "Verticillium"
                    , "organo": "Rama"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Rama por Verticillium", ""] }
                    , "nombre_variable": "Rama por Verticillium"
                },

                {
                    "plaga_o_enfermedad": "Verticillium"
                    , "organo": "Tallo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Tallo por Verticillium", ""] }
                    , "nombre_variable": "Tallo por Verticillium"
                },


                {
                    "plaga_o_enfermedad": "Lasiodiplodia"
                    , "organo": "Rama"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Rama por Lasiodiplodia", ""] }
                    , "nombre_variable": "Rama por Lasiodiplodia"
                },
                {
                    "plaga_o_enfermedad": "Lasiodiplodia"
                    , "organo": "Tallo"
                    , "estado": ""
                    , "valor": { "$ifNull": ["$Tallo por Lasiodiplodia", ""] }
                    , "nombre_variable": "Tallo por Lasiodiplodia"
                }


                , {
                    "plaga_o_enfermedad": "Fenologia"
                    , "organo": ""
                    , "estado": "Vegetativo"
                    , "valor": { "$ifNull": ["$Vegetativo", ""] }
                    , "nombre_variable": "Vegetativo"
                },
                {
                    "plaga_o_enfermedad": "Fenologia"
                    , "organo": ""
                    , "estado": "Floracion"
                    , "valor": { "$ifNull": ["$Floracion", ""] }
                    , "nombre_variable": "Floracion"
                },
                {
                    "plaga_o_enfermedad": "Fenologia"
                    , "organo": ""
                    , "estado": "Produccion"
                    , "valor": { "$ifNull": ["$Produccion", ""] }
                    , "nombre_variable": "Produccion"
                }








            ]
        }
    }

    , {
        "$project": {
            "OligAdulto": 0,
            "Olig Huevo": 0,
            "Olig Ninfa": 0,
            "Olig Larva": 0,
            "Fruto Afectado por Monalonion": 0,
            "Terminal Afectado por Monalonion": 0,
            "Mon Ninfa": 0,
            "Mon Adulto": 0,
            "Ramas Afectadas por Stenoma": 0,
            "Frutos Afectados por Stenoma": 0,
            "Mosca del Ov Racimo": 0,
            "Mosca del Ov Cuaje A": 0,
            "Mosca del Ov Cuaje T": 0,
            "Trips Racimo": 0,
            "Trips Cuaje A": 0,
            "Trips Cuaje T": 0,
            "Total Individuos por Arbol": 0,
            "Compsus Huevo": 0,
            "Compsus Adulto": 0,
            "Ramas Afectadas": 0,
            "Tallos Afectados": 0,
            "Tallo Afectado": 0,
            "Heilipus Huevo": 0,
            "Heilipus Larva": 0,
            "Heilipus Adulo": 0,
            "Crisopa": 0,
            "Abeja": 0,
            "Frutos por Antracnosis": 0,
            "Racimos por Antracnosis": 0,
            "Rama por Verticillium": 0,
            "Tallo por Verticillium": 0,
            "Rama por Lasiodiplodia": 0,
            "Tallo por Lasiodiplodia": 0,
            "Vegetativo": 0,
            "Floracion": 0,
            "Produccion": 0


            , "Hojas Afectadas": 0,
            "Severidad": 0,
            "Frutos Afectados": 0,
            "Terminales Afectadas": 0

            , "Olig Hojas Afectadas": 0,
            "Olig Severidad": 0,
            "Trips Total Individuos por Arbol": 0,
            "Trips Severidad": 0,
            "Compsus Hojas Afectadas": 0,
            "Compsus Severidad": 0,
            "Copturomimus Ramas Afectadas": 0,
            "Copturomimus Tallos Afectados": 0,
            "Amorbia Frutos Afectados": 0,
            "Xyleborus Tallo Afectado": 0,
            "Astaena Hojas Afectadas": 0,
            "Astaena Frutos Afectados": 0,
            "Astaena Terminales Afectadas": 0,
            "Chysomelidae Hojas Afectadas": 0,
            "Chysomelidae Terminales Afectadas": 0



        }
    }



    , {
        "$addFields": {
            "array_data": {
                "$filter": {
                    "input": "$array_data",
                    "as": "item",
                    "cond": { "$not": { "$in": ["$$item.valor", ["", "0"]] } }
                }
            }
        }
    }

    , {
        "$match": {
            "array_data": { "$ne": [] }
        }
    }




    , { "$unwind": "$array_data" }





    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"

                , "plaga": "$array_data.plaga_o_enfermedad"

                , "etapa": "$Etapa"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

                , "plaga": "$_id.plaga"

                , "etapa": "$_id.etapa"
            },
            "plantas_dif_censadas_x_lote_x_plaga": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"

                , "plaga": "$array_data.plaga_o_enfermedad"
                , "estado": "$array_data.estado"

                , "etapa": "$Etapa"
            },
            "individuos_x_lote_x_plaga_x_estado": { "$sum": { "$toDouble": { "$ifNull": ["$array_data.valor", 0] } } },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "individuos_x_lote_x_plaga_x_estado": "$individuos_x_lote_x_plaga_x_estado"
                    }
                ]
            }
        }
    }




    , {
        "$addFields": {
            "plaga_o_enfermedad": "$array_data.plaga_o_enfermedad",
            "organo": "$array_data.organo",
            "estado_plaga": "$array_data.estado",
            "valor": "$array_data.valor",
            "nombre_variable": "$array_data.nombre_variable"

        }
    }

    , {
        "$project": {
            "array_data": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"

                , "etapa": "$Etapa"



            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Produccion": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": {
                        "$and": [
                            { "$eq": ["$$item.plaga_o_enfermedad", "Fenologia"] }
                            , { "$eq": ["$$item.estado_plaga", "Produccion"] }
                        ]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Produccion": {
                "$reduce": {
                    "input": "$sum_fenologia_x_etapa_Produccion",
                    "initialValue": 0,
                    "in": { "$add": ["$$value", { "$toDouble": "$$this.valor" }] }
                }
            }
        }
    }


    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Vegetativo": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": {
                        "$and": [
                            { "$eq": ["$$item.plaga_o_enfermedad", "Fenologia"] }
                            , { "$eq": ["$$item.estado_plaga", "Vegetativo"] }
                        ]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Vegetativo": {
                "$reduce": {
                    "input": "$sum_fenologia_x_etapa_Vegetativo",
                    "initialValue": 0,
                    "in": { "$add": ["$$value", { "$toDouble": "$$this.valor" }] }
                }
            }
        }
    }



    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Floracion": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": {
                        "$and": [
                            { "$eq": ["$$item.plaga_o_enfermedad", "Fenologia"] }
                            , { "$eq": ["$$item.estado_plaga", "Floracion"] }
                        ]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "sum_fenologia_x_etapa_Floracion": {
                "$reduce": {
                    "input": "$sum_fenologia_x_etapa_Floracion",
                    "initialValue": 0,
                    "in": { "$add": ["$$value", { "$toDouble": "$$this.valor" }] }
                }
            }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "sum_fenologia_x_etapa_Produccion": "$sum_fenologia_x_etapa_Produccion",
                        "sum_fenologia_x_etapa_Vegetativo": "$sum_fenologia_x_etapa_Vegetativo",
                        "sum_fenologia_x_etapa_Floracion": "$sum_fenologia_x_etapa_Floracion"
                    }
                ]
            }
        }
    }


    , {
        "$match": {
            "plaga_o_enfermedad": { "$ne": "Fenologia" }
        }
    }




    , {
        "$addFields": {
            "tiene_severidad": {
                "$cond": {

                    "if": {

                        "$or": [
                            { "$eq": ["$estado_plaga", "Severidad"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje A"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje T"] }

                        ]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    }

    , {
        "$addFields": {
            "tiene_severidad_arbol": {
                "$cond": {

                    "if": {

                        "$or": [
                            { "$eq": ["$estado_plaga", "Severidad"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje A"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje T"] }

                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    }

    , {
        "$addFields": {
            "tiene_severidad_valor": {
                "$cond": {

                    "if": {

                        "$or": [
                            { "$eq": ["$estado_plaga", "Severidad"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje A"] },
                            { "$eq": ["$nombre_variable", "Trips Cuaje T"] }

                        ]
                    },
                    "then": "$valor",
                    "else": 0
                }
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"

                , "etapa": "$Etapa"

            },
            "arboles_x_lote_x_severidad": { "$sum": "$tiene_severidad_arbol" },
            "sum_severidad_x_lote_x_severidad": { "$sum": { "$toDouble": { "$ifNull": ["$tiene_severidad_valor", 0] } } },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }



    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "arboles_x_lote_x_severidad": "$arboles_x_lote_x_severidad",
                        "sum_severidad_x_lote_x_severidad": "$sum_severidad_x_lote_x_severidad"
                    }
                ]
            }
        }
    }


    , {
        "$project": {

            "tiene_severidad_arbol": 0,
            "tiene_severidad_valor": 0
        }
    }




    , {
        "$addFields": {
            "organos_monitoreados": {
                "$switch": {
                    "branches": [

                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Siembra"] },
                                            { "$eq": ["$organo", "Rama"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 4
                        },
                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Siembra"] },
                                            { "$eq": ["$organo", "Hoja"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 20
                        },
                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Siembra"] },
                                            { "$eq": ["$organo", "Racimo"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 4
                        },
                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Siembra"] },
                                            { "$eq": ["$organo", "Fruto"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 10
                        },


                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Resiembra"] },
                                            { "$eq": ["$organo", "Rama"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 1
                        },
                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Resiembra"] },
                                            { "$eq": ["$organo", "Hoja"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 5
                        },


                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Zoca"] },
                                            { "$eq": ["$organo", "Rama"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 1
                        },
                        {
                            "case": {
                                "$and": [
                                    {
                                        "$and": [
                                            { "$eq": ["$Etapa", "Zoca"] },
                                            { "$eq": ["$organo", "Hoja"] }
                                        ]
                                    }
                                ]
                            },
                            "then": 5
                        }

                    ],
                    "default": 0
                }
            }
        }
    },




    {
        "$group": {
            "_id": {
                "bloque": "$bloque",
                "lote": "$lote",
                "etapa": "$Etapa",
                "plaga": "$plaga_o_enfermedad",
                "organo": "$organo"
            },

            "organos_afectados_x_lote_x_etapa_x_plaga": { "$sum": { "$toDouble": { "$ifNull": ["$valor", 0] } } },
            "data": { "$push": "$$ROOT" }

            , "arboles_organos_afectados_x_lote_x_etapa_x_plaga": { "$sum": 1 }
        }
    },
    { "$unwind": "$data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "organos_afectados_x_lote_x_etapa_x_plaga": "$organos_afectados_x_lote_x_etapa_x_plaga"
                        , "arboles_organos_afectados_x_lote_x_etapa_x_plaga": "$arboles_organos_afectados_x_lote_x_etapa_x_plaga"
                    }
                ]
            }
        }
    },





    {
        "$group": {
            "_id": {
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",

                "etapa": "$Etapa",
                "organo": "$organo",

                "valor": "$organos_monitoreados"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",

                "etapa": "$_id.etapa",
                "organo": "$_id.organo"
            },
            "organos_monitoreados_x_lote_x_etapa": { "$sum": "$_id.valor" },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "organos_monitoreados_x_lote_x_etapa": "$organos_monitoreados_x_lote_x_etapa" }
                ]
            }
        }
    },




    {
        "$addFields": {
            "num_organos_monitoreados": {
                "$multiply": [
                    "$arboles_organos_afectados_x_lote_x_etapa_x_plaga",
                    "$organos_monitoreados"
                ]

            }
        }
    },



    {
        "$addFields": {
            "incidencia_organo": {
                "$cond": {
                    "if": { "$gt": ["$num_organos_monitoreados", 0] },
                    "then": {
                        "$multiply": [
                            {
                                "$divide": [
                                    "$organos_afectados_x_lote_x_etapa_x_plaga",
                                    "$num_organos_monitoreados"
                                ]
                            },
                            100
                        ]
                    },
                    "else": 0
                }
            }
        }
    },


    {
        "$addFields": {
            "incidencia_organo": {
                "$trunc": ["$incidencia_organo", 2]
            }
        }
    }

    , {
        "$addFields": {
            "1_PCT_INCIDENCIA_plaga_lote_x_etapa": {
                "$cond": {
                    "if": { "$gt": ["$arboles_x_lote_x_Etapa", 0] },
                    "then": {
                        "$multiply": [
                            {
                                "$divide": [
                                    "$plantas_dif_censadas_x_lote_x_plaga",
                                    "$arboles_x_lote_x_Etapa"
                                ]
                            },
                            100
                        ]
                    },
                    "else": 0
                }
            }
        }
    }
    , {
        "$addFields": {
            "1_PCT_INCIDENCIA_plaga_lote_x_etapa": {
                "$trunc": ["$1_PCT_INCIDENCIA_plaga_lote_x_etapa", 2]
            }
        }
    }


    , {
        "$addFields": {
            "2_PCT_INCIDENCIA_organo_lote": "$incidencia_organo"
        }
    }

    , {
        "$project": {
            "incidencia_organo": 0
        }
    }

    , {
        "$addFields": {
            "3_SEVERIDAD_organo": {
                "$cond": {

                    "if": { "$gt": ["$organos_afectados_x_lote_x_etapa_x_plaga", 0] },
                    "then": {
                        "$divide": [
                            "$sum_severidad_x_lote_x_severidad",

                            "$organos_afectados_x_lote_x_etapa_x_plaga"
                        ]
                    },
                    "else": 0
                }
            }
        }
    }
    , {
        "$addFields": {
            "3_SEVERIDAD_organo": {
                "$trunc": ["$3_SEVERIDAD_organo", 2]
            }
        }
    }

    , {
        "$addFields": {
            "4_PROMEDIO_poblaciones": {
                "$cond": {

                    "if": { "$gt": ["$organos_afectados_x_lote_x_etapa_x_plaga", 0] },
                    "then": {
                        "$divide": [
                            "$individuos_x_lote_x_plaga_x_estado",

                            "$organos_afectados_x_lote_x_etapa_x_plaga"
                        ]
                    },
                    "else": 0
                }
            }
        }
    }
    , {
        "$addFields": {
            "4_PROMEDIO_poblaciones": {
                "$trunc": ["$4_PROMEDIO_poblaciones", 2]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_": { "$ifNull": ["$arboles_etapa_.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_etapa_lote_": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_Siembra": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", "Siembra"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_Siembra",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_Siembra": { "$ifNull": ["$arboles_etapa_Siembra.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_etapa_lote_Siembra": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_Siembra", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_Resiembra": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", "Resiembra"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_Resiembra",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_Resiembra": { "$ifNull": ["$arboles_etapa_Resiembra.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_etapa_lote_Resiembra": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_Resiembra", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_SitioVacio": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", "Sitio Vacío "] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_SitioVacio",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_SitioVacio": { "$ifNull": ["$arboles_etapa_SitioVacio.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_etapa_lote_SitioVacio": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_SitioVacio", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_ArbolMuerto": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", "Arbol Muerto"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_ArbolMuerto",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_ArbolMuerto": { "$ifNull": ["$arboles_etapa_ArbolMuerto.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_etapa_lote_ArbolMuerto": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_ArbolMuerto", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_etapa_Zoca": {
                "$filter": {
                    "input": "$data_array_group_etapa",
                    "as": "item",
                    "cond": { "$eq": ["$$item.etapa", "Zoca"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_etapa_Zoca",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_etapa_Zoca": { "$ifNull": ["$arboles_etapa_Zoca.arboles_x_lote_x_Etapa", 0] }
        }
    }
    , {
        "$addFields": {
            "5_PCT_etapa_lote_Zoca": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_etapa_Zoca", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }



    , {
        "$project": {
            "data_array_group_etapa": 0
        }
    }

    , {
        "$addFields": {
            "arboles_estado_": {
                "$filter": {
                    "input": "$data_array_group_estado",
                    "as": "item",
                    "cond": { "$eq": ["$$item.estado", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_estado_",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_estado_": { "$ifNull": ["$arboles_estado_.arboles_x_lote_x_Estado", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_estado_lote_": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_estado_", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_estado_Clorotico": {
                "$filter": {
                    "input": "$data_array_group_estado",
                    "as": "item",
                    "cond": { "$eq": ["$$item.estado", "Clorotico"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_estado_Clorotico",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_estado_Clorotico": { "$ifNull": ["$arboles_estado_Clorotico.arboles_x_lote_x_Estado", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_estado_lote_Clorotico": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_estado_Clorotico", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }


    , {
        "$addFields": {
            "arboles_estado_InsuficienciaNutrientes": {
                "$filter": {
                    "input": "$data_array_group_estado",
                    "as": "item",
                    "cond": { "$eq": ["$$item.estado", "Insuficiencia de nutrientes"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_estado_InsuficienciaNutrientes",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_estado_InsuficienciaNutrientes": { "$ifNull": ["$arboles_estado_InsuficienciaNutrientes.arboles_x_lote_x_Estado", 0] }
        }
    }
    , {
        "$addFields": {
            "PCT_estado_lote_InsuficienciaNutrientes": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_estado_InsuficienciaNutrientes", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "arboles_estado_Humedad": {
                "$filter": {
                    "input": "$data_array_group_estado",
                    "as": "item",
                    "cond": { "$eq": ["$$item.estado", "Humedad"] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$arboles_estado_Humedad",
            "preserveNullAndEmptyArrays": true
        }
    }
    , {
        "$addFields": {
            "arboles_estado_Humedad": { "$ifNull": ["$arboles_estado_Humedad.arboles_x_lote_x_Estado", 0] }
        }
    }
    , {
        "$addFields": {
            "6_PCT_estado_lote_Humedad": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_estado_Humedad", "$plantas_dif_censadas_x_lote"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$project": {
            "data_array_group_estado": 0
        }
    }

    , {
        "$addFields": {
            "7_PCT_Arveses_y_Categora_arvenses_lote": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_x_lote_x_Arveses_y_Categora_arvenses", "$arboles_x_lote_x_Etapa"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }



    , {
        "$addFields": {
            "PCT_Fenologia_Produccion": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$sum_fenologia_x_etapa_Produccion", "$arboles_x_lote_x_Etapa"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "PCT_Fenologia_Vegetativo": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$sum_fenologia_x_etapa_Vegetativo", "$arboles_x_lote_x_Etapa"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "8_PCT_Fenologia_Floracion": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$sum_fenologia_x_etapa_Floracion", "$arboles_x_lote_x_Etapa"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }

    , {
        "$addFields": {
            "9_PCT_complejo_de_raiz_lote": {
                "$trunc": [
                    {
                        "$multiply": [
                            { "$divide": ["$arboles_x_lote_x_complejo_de_raiz", "$arboles_x_lote_x_Etapa"] },
                            100
                        ]
                    },
                    2
                ]
            }
        }
    }






]
