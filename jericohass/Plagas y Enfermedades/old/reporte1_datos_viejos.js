db.form_monitoreodeplagasyenfermedades.aggregate(

    [


        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-06-28T06:00:00.000-05:00"),
                "Busqueda fin": ISODate("2023-07-28T06:00:00.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        //========CLASIFICACION DE DATOS (VIEJOS o NUEVOS)
        {
            "$match": {
                "Olig Hojas Afectadas": { "$exists": false }//datos VIEJOS
                // "Olig Hojas Afectadas":{"$exists":true}//datos NUEVOS
            }
        },



        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0


                , "Formula": 0
                , "uid": 0
                , "uDate": 0,


                //variables vacias (maestro numerico)
                "Oligonychus": 0,
                "Monalonion": 0,
                "Stenoma": 0,
                "Mosca del Ovario": 0,
                "Trips": 0,
                "Compsus": 0,
                "Copturomimus Persea": 0,
                "Amorbia": 0,
                "Xyleborus": 0,
                "Astaena": 0,
                "Heilipus Lauri": 0,
                "Chysomelidae": 0,
                "Beneficos": 0,
                "Antracnosis": 0,
                "Verticillium": 0,
                "Lasiodiplodia": 0,
                "Fenologia": 0

                //otras
                , "user": 0,
                "Keys": 0,
                "Finca nombre": 0,
                "uDate día": 0, "uDate mes": 0, "uDate año": 0, "uDate hora": 0,
                "rgDate día": 0, "rgDate mes": 0, "rgDate año": 0, "rgDate hora": 0,
                "Busqueda inicio": 0,
                "Busqueda fin": 0,
                "today": 0,
                "FincaID": 0

            }
        }


        // //test
        // ,{
        //     $group: { _id: "$finca",cant:{$sum:1}}
        // }


        //----plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        //----arboles_x_lote_x_ZZZZZZZZZ
        /*
  "Etapa" : "Siembra",
  "Estado" : "",
  "Arveses" : "",
  "Categora de Arvenses" : "",
        */


        //----arboles_x_lote_x_ETAPA
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",

                    "etapa": "$Etapa"
                },
                "arboles_x_lote_x_Etapa": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "arboles_x_lote_x_Etapa": "$arboles_x_lote_x_Etapa"
                        }
                    ]
                }
            }
        }


        //----arboles_x_lote_x_ESTADO
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",

                    "estado": "$Estado"
                },
                "arboles_x_lote_x_Estado": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "arboles_x_lote_x_Estado": "$arboles_x_lote_x_Estado"
                        }
                    ]
                }
            }
        }

        //----arboles_x_lote_x_ARVESES
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",

                    "arveses": "$Arveses"
                },
                "arboles_x_lote_x_Arveses": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "arboles_x_lote_x_Arveses": "$arboles_x_lote_x_Arveses"
                        }
                    ]
                }
            }
        }


        //----arboles_x_lote_x_CATEGORIA_ARVESES
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",

                    "categora_arvenses": "$Categora de Arvenses"
                },
                "arboles_x_lote_x_Categora_arvenses": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "arboles_x_lote_x_Categora_arvenses": "$arboles_x_lote_x_Categora_arvenses"
                        }
                    ]
                }
            }
        }





        /*

        ------CAMPOS MN
        Oligonychus
        Monalonion
        Stenoma
        Mosca del Ovario
        Trips
        Compsus
        Copturomimus Persea
        Amorbia
        Xyleborus
        Astaena
        Heilipus Lauri
        Chysomelidae
        Beneficos
        Antracnosis
        Verticillium
        Lasiodiplodia
        Fenologia

        */



        //----array_data
        , {
            "$addFields": {
                "array_data": [

                    // //=====>>PLAGA_ENFERMEDAD: "XXXXXXXXXXXX"
                    // {
                    //     "plaga_o_enfermedad": "xxxxxx"
                    //     , "organo": "aaaaaaaaaa"
                    //     , "estado": "bbbbbbbbbb"
                    //     , "valor": { "$ifNull": ["$ zzzzzzzz", ""] }
                    //     , "nombre_variable": "zzzzzzzz"
                    // },


                    //=====>>PLAGA_ENFERMEDAD: "Oligonychus"
                    {
                        "plaga_o_enfermedad": "Oligonychus"
                        , "organo": ""
                        , "estado": "Huevo"
                        , "valor": { "$ifNull": ["$Olig Huevo", ""] }
                        , "nombre_variable": "Olig Huevo"
                    },
                    {
                        "plaga_o_enfermedad": "Oligonychus"
                        , "organo": ""
                        , "estado": "Ninfa"
                        , "valor": { "$ifNull": ["$Olig Ninfa", ""] }
                        , "nombre_variable": "Olig Ninfa"
                    },
                    {
                        "plaga_o_enfermedad": "Oligonychus"
                        , "organo": ""
                        , "estado": "Larva"
                        , "valor": { "$ifNull": ["$Olig Larva", ""] }
                        , "nombre_variable": "Olig Larva"
                    },
                    {
                        "plaga_o_enfermedad": "Oligonychus"
                        , "organo": ""
                        , "estado": "Adulto"
                        , "valor": { "$ifNull": ["$OligAdulto", ""] }
                        , "nombre_variable": "OligAdulto"
                    },



                    //---caso ambiguo
                    //  {
                    //     "plaga_o_enfermedad": "Oligonychus"
                    //     , "organo": ""
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Severidad", ""] }
                    //     , "nombre_variable": "Severidad"
                    // },

                    // {
                    //     "plaga_o_enfermedad": "Oligonychus"
                    //     , "organo": "aaaaaaaaaa"
                    //     , "estado": "bbbbbbbbbb"
                    //     , "valor": { "$ifNull": ["$Hojas Afectadas", ""] }
                    //     , "nombre_variable": "Hojas Afectadas"
                    // },


                    //=====>>PLAGA_ENFERMEDAD: "Monalonion"
                    {
                        "plaga_o_enfermedad": "Monalonion"
                        , "organo": "Fruto"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Fruto Afectado por Monalonion", ""] }
                        , "nombre_variable": "Fruto Afectado por Monalonion"
                    },
                    {
                        "plaga_o_enfermedad": "Monalonion"
                        , "organo": "Terminal"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Terminal Afectado por Monalonion", ""] }
                        , "nombre_variable": "Terminal Afectado por Monalonion"
                    },
                    {
                        "plaga_o_enfermedad": "Monalonion"
                        , "organo": ""
                        , "estado": "Ninfa"
                        , "valor": { "$ifNull": ["$Mon Ninfa", ""] }
                        , "nombre_variable": "Mon Ninfa"
                    },
                    {
                        "plaga_o_enfermedad": "Monalonion"
                        , "organo": ""
                        , "estado": "Adulto"
                        , "valor": { "$ifNull": ["$Mon Adulto", ""] }
                        , "nombre_variable": "Mon Adulto"
                    },


                    //=====>>PLAGA_ENFERMEDAD: "Stenoma"
                    {
                        "plaga_o_enfermedad": "Stenoma"
                        , "organo": "Rama"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Ramas Afectadas por Stenoma", ""] }
                        , "nombre_variable": "Ramas Afectadas por Stenoma"
                    },

                    {
                        "plaga_o_enfermedad": "Stenoma"
                        , "organo": "Fruto"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Frutos Afectados por Stenoma", ""] }
                        , "nombre_variable": "Frutos Afectados por Stenoma"
                    },




                    //=====>>PLAGA_ENFERMEDAD: "Mosca del Ovario"
                    {
                        "plaga_o_enfermedad": "Mosca del Ovario"
                        , "organo": "Racimo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Mosca del Ov Racimo", ""] }
                        , "nombre_variable": "Mosca del Ov Racimo"
                    },
                    {
                        "plaga_o_enfermedad": "Mosca del Ovario"
                        , "organo": ""
                        , "estado": "Cuaje A"
                        , "valor": { "$ifNull": ["$Mosca del Ov Cuaje A", ""] }
                        , "nombre_variable": "Mosca del Ov Cuaje A"
                    },
                    {
                        "plaga_o_enfermedad": "Mosca del Ovario"
                        , "organo": ""
                        , "estado": "Cuaje T"
                        , "valor": { "$ifNull": ["$Mosca del Ov Cuaje T", ""] }
                        , "nombre_variable": "Mosca del Ov Cuaje T"
                    },




                    //=====>>PLAGA_ENFERMEDAD: "Trips"
                    {
                        "plaga_o_enfermedad": "Trips"
                        , "organo": "Racimo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Trips Racimo", ""] }
                        , "nombre_variable": "Trips Racimo"
                    },
                    {
                        "plaga_o_enfermedad": "Trips"
                        , "organo": ""
                        , "estado": "Cuaje A"
                        , "valor": { "$ifNull": ["$Trips Cuaje A", ""] }
                        , "nombre_variable": "Trips Cuaje A"
                    },
                    {
                        "plaga_o_enfermedad": "Trips"
                        , "organo": ""
                        , "estado": "Cuaje T"
                        , "valor": { "$ifNull": ["$Trips Cuaje T", ""] }
                        , "nombre_variable": "Trips Cuaje T"
                    },
                    {
                        "plaga_o_enfermedad": "Trips"
                        , "organo": ""
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Total Individuos por Arbol", ""] }
                        , "nombre_variable": "Total Individuos por Arbol"
                    },
                    // {
                    //     "plaga_o_enfermedad": "Trips"
                    //     , "organo": ""
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Severidad", ""] }
                    //     , "nombre_variable": "Severidad"
                    // },




                    //=====>>PLAGA_ENFERMEDAD: "Compsus"
                    // {
                    //     "plaga_o_enfermedad": "Compsus"
                    //     , "organo": "Hoja"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Hojas Afectadas", ""] }
                    //     , "nombre_variable": "Hojas Afectadas"
                    // },
                    {
                        "plaga_o_enfermedad": "Compsus"
                        , "organo": ""
                        , "estado": "Huevo"
                        , "valor": { "$ifNull": ["$Compsus Huevo", ""] }
                        , "nombre_variable": "Compsus Huevo"
                    },
                    {
                        "plaga_o_enfermedad": "Compsus"
                        , "organo": ""
                        , "estado": "Adulto"
                        , "valor": { "$ifNull": ["$Compsus Adulto", ""] }
                        , "nombre_variable": "Compsus Adulto"
                    },
                    // {
                    //     "plaga_o_enfermedad": "Compsus"
                    //     , "organo": ""
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Severidad", ""] }
                    //     , "nombre_variable": "Severidad"
                    // },




                    //=====>>PLAGA_ENFERMEDAD: "Copturomimus Persea"
                    {
                        "plaga_o_enfermedad": "Copturomimus Persea"
                        , "organo": "Rama"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Ramas Afectadas", ""] }
                        , "nombre_variable": "Ramas Afectadas"
                    },

                    {
                        "plaga_o_enfermedad": "Copturomimus Persea"
                        , "organo": "Tallo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Tallos Afectados", ""] }
                        , "nombre_variable": "Tallos Afectados"
                    },



                    // //=====>>PLAGA_ENFERMEDAD: "Amorbia"
                    // {
                    //     "plaga_o_enfermedad": "Amorbia"
                    //     , "organo": "Fruto"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Frutos Afectados", ""] }
                    //     , "nombre_variable": "Frutos Afectados"
                    // },



                    //=====>>PLAGA_ENFERMEDAD: "Xyleborus"
                    {
                        "plaga_o_enfermedad": "Xyleborus"
                        , "organo": "Tallo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Tallo Afectado", ""] }
                        , "nombre_variable": "Tallo Afectado"
                    },



                    //=====>>PLAGA_ENFERMEDAD: "Astaena"
                    // {
                    //     "plaga_o_enfermedad": "Astaena"
                    //     , "organo": "Hoja"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Hojas Afectadas", ""] }
                    //     , "nombre_variable": "Hojas Afectadas"
                    // },
                    // {
                    //     "plaga_o_enfermedad": "Astaena"
                    //     , "organo": "Fruto"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Frutos Afectados", ""] }
                    //     , "nombre_variable": "Frutos Afectados"
                    // // },
                    // {
                    //     "plaga_o_enfermedad": "Astaena"
                    //     , "organo": "Terminal"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Terminales Afectadas", ""] }
                    //     , "nombre_variable": "Terminales Afectadas"
                    // },





                    //=====>>PLAGA_ENFERMEDAD: "Heilipus Lauri"
                    {
                        "plaga_o_enfermedad": "Heilipus Lauri"
                        , "organo": ""
                        , "estado": "Huevo"
                        , "valor": { "$ifNull": ["$Heilipus Huevo", ""] }
                        , "nombre_variable": "Heilipus Huevo"
                    },
                    {
                        "plaga_o_enfermedad": "Heilipus Lauri"
                        , "organo": ""
                        , "estado": "Larva"
                        , "valor": { "$ifNull": ["$Heilipus Larva", ""] }
                        , "nombre_variable": "Heilipus Larva"
                    },
                    {
                        "plaga_o_enfermedad": "Heilipus Lauri"
                        , "organo": ""
                        , "estado": "Adulo"
                        , "valor": { "$ifNull": ["$Heilipus Adulo", ""] }
                        , "nombre_variable": "Heilipus Adulo"
                    },





                    //=====>>PLAGA_ENFERMEDAD: "Chysomelidae"
                    // {
                    //     "plaga_o_enfermedad": "Chysomelidae"
                    //     , "organo": "Hoja"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Hojas Afectadas", ""] }
                    //     , "nombre_variable": "Hojas Afectadas"
                    // },
                    // {
                    //     "plaga_o_enfermedad": "Chysomelidae"
                    //     , "organo": "Terminal"
                    //     , "estado": ""
                    //     , "valor": { "$ifNull": ["$Terminales Afectadas", ""] }
                    //     , "nombre_variable": "Terminales Afectadas"
                    // },



                    //=====>>PLAGA_ENFERMEDAD: "XXXXXXXXXXXX"
                    //CASO GENERAL (Beneficos)
                    {
                        "plaga_o_enfermedad": "Crisopa"
                        , "organo": ""
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Crisopa", ""] }
                        , "nombre_variable": "Crisopa"
                    },
                    {
                        "plaga_o_enfermedad": "Abeja"
                        , "organo": ""
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Abeja", ""] }
                        , "nombre_variable": "Abeja"
                    },





                    //=====>>PLAGA_ENFERMEDAD: "Antracnosis"
                    {
                        "plaga_o_enfermedad": "Antracnosis"
                        , "organo": "Fruto"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Frutos por Antracnosis", ""] }
                        , "nombre_variable": "Frutos por Antracnosis"
                    },

                    {
                        "plaga_o_enfermedad": "Antracnosis"
                        , "organo": "Racimo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Racimos por Antracnosis", ""] }
                        , "nombre_variable": "Racimos por Antracnosis"
                    },



                    //=====>>PLAGA_ENFERMEDAD: "Verticillium"
                    {
                        "plaga_o_enfermedad": "Verticillium"
                        , "organo": "Rama"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Rama por Verticillium", ""] }
                        , "nombre_variable": "Rama por Verticillium"
                    },

                    {
                        "plaga_o_enfermedad": "Verticillium"
                        , "organo": "Tallo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Tallo por Verticillium", ""] }
                        , "nombre_variable": "Tallo por Verticillium"
                    },



                    //=====>>PLAGA_ENFERMEDAD: "Lasiodiplodia"
                    {
                        "plaga_o_enfermedad": "Lasiodiplodia"
                        , "organo": "Rama"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Rama por Lasiodiplodia", ""] }
                        , "nombre_variable": "Rama por Lasiodiplodia"
                    },
                    {
                        "plaga_o_enfermedad": "Lasiodiplodia"
                        , "organo": "Tallo"
                        , "estado": ""
                        , "valor": { "$ifNull": ["$Tallo por Lasiodiplodia", ""] }
                        , "nombre_variable": "Tallo por Lasiodiplodia"
                    },



                    //=====>>PLAGA_ENFERMEDAD: "XXXXXXXXXXXX"
                    //CASO GENERAL (Fenologia)
                    {
                        "plaga_o_enfermedad": "Fenologia"
                        , "organo": ""
                        , "estado": "Vegetativo"
                        , "valor": { "$ifNull": ["$Vegetativo", ""] }
                        , "nombre_variable": "Vegetativo"
                    },
                    {
                        "plaga_o_enfermedad": "Fenologia"
                        , "organo": ""
                        , "estado": "Floracion"
                        , "valor": { "$ifNull": ["$Floracion", ""] }
                        , "nombre_variable": "Floracion"
                    },
                    {
                        "plaga_o_enfermedad": "Fenologia"
                        , "organo": ""
                        , "estado": "Produccion"
                        , "valor": { "$ifNull": ["$Produccion", ""] }
                        , "nombre_variable": "Produccion"
                    }








                ]
            }
        }

        //eliminar variables
        , {
            "$project": {
                "OligAdulto": 0,
                "Olig Huevo": 0,
                "Olig Ninfa": 0,
                "Olig Larva": 0,
                "Fruto Afectado por Monalonion": 0,
                "Terminal Afectado por Monalonion": 0,
                "Mon Ninfa": 0,
                "Mon Adulto": 0,
                "Ramas Afectadas por Stenoma": 0,
                "Frutos Afectados por Stenoma": 0,
                "Mosca del Ov Racimo": 0,
                "Mosca del Ov Cuaje A": 0,
                "Mosca del Ov Cuaje T": 0,
                "Trips Racimo": 0,
                "Trips Cuaje A": 0,
                "Trips Cuaje T": 0,
                "Total Individuos por Arbol": 0,
                "Compsus Huevo": 0,
                "Compsus Adulto": 0,
                "Ramas Afectadas": 0,
                "Tallos Afectados": 0,
                "Tallo Afectado": 0,
                "Heilipus Huevo": 0,
                "Heilipus Larva": 0,
                "Heilipus Adulo": 0,
                "Crisopa": 0,
                "Abeja": 0,
                "Frutos por Antracnosis": 0,
                "Racimos por Antracnosis": 0,
                "Rama por Verticillium": 0,
                "Tallo por Verticillium": 0,
                "Rama por Lasiodiplodia": 0,
                "Tallo por Lasiodiplodia": 0,
                "Vegetativo": 0,
                "Floracion": 0,
                "Produccion": 0


                //ambiguos
                , "Hojas Afectadas": 0,
                "Severidad": 0,
                "Frutos Afectados": 0,
                "Terminales Afectadas": 0



            }
        }



        // //test
        // // , { $limit: 2 }
        // , { $unwind: "$array_data" }
        // // , {
        // //     "$addFields": {
        // //         "variable_nombre": "$array_data.nombre_variable"
        // //     }
        // // }
        // ,{
        //     "$match": {
        //         "array_data.valor": ""
        //     }
        // }


        , {
            "$addFields": {
                "array_data": {
                    "$filter": {
                        "input": "$array_data",
                        "as": "item",
                        "cond": { "$not": { "$in": ["$$item.valor", ["", "0"]] } }
                    }
                }
            }
        }

        , {
            "$match": {
                "array_data": { "$ne": [] }
            }
        }
        , { "$unwind": "$array_data" }




        //----plantas_dif_censadas_x_lote_x_plaga
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"

                    , "plaga": "$array_data.plaga_o_enfermedad"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"

                    , "plaga": "$_id.plaga"
                },
                "plantas_dif_censadas_x_lote_x_plaga": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"
                        }
                    ]
                }
            }
        }


        // //test
        // ,{
        //     $match:{
        //         "array_data.valor":"2,,"
        //     }

        // }


        //----individuos_x_lote_x_plaga_x_estado
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"

                    , "plaga": "$array_data.plaga_o_enfermedad"
                    , "estado": "$array_data.estado"
                },
                "individuos_x_lote_x_plaga_x_estado": { "$sum": { "$toDouble": { "$ifNull": ["$array_data.valor", 0] } } },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "individuos_x_lote_x_plaga_x_estado": "$individuos_x_lote_x_plaga_x_estado"
                        }
                    ]
                }
            }
        }




        , {
            "$addFields": {
                "plaga_o_enfermedad": "$array_data.plaga_o_enfermedad",
                "organo": "$array_data.organo",
                "estado": "$array_data.estado",
                "valor": "$array_data.valor",
                "nombre_variable": "$array_data.nombre_variable"
            }
        }

        , {
            "$project": {
                "array_data": 0
            }
        }





    ]


)




// //=====>>PLAGA_ENFERMEDAD: "XXXXXXXXXXXX"
// {
//     "plaga_o_enfermedad": "xxxxxx"
//     , "organo": "aaaaaaaaaa"
//     , "estado": "bbbbbbbbbb"
//     , "valor": { "$ifNull": ["$ zzzzzzzz", ""] }
//     , "nombre_variable": "zzzzzzzz"
// },
