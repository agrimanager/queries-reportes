db.form_modulodeplagas.aggregate(
    [

        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------


        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Arbol" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        //--linea
        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        //--arbol
        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Arbol": 0
                , "Point": 0
            }
        }


        //=====AGRUPACION
        , {
            "$group": {
                "_id": {
                    "plaga": "$Plaga"

                    , "lote": "$lote"
                    , "arbol": "$arbol"
                }
                , "sum_Muertas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Muertas" } }
                , "sum_Huevos_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Huevos" } }
                , "sum_Pupas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Pupas" } }
                , "sum_Larvas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Larvas" } }
                , "sum_Adultos_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Adultos" } }
                , "sum_Ninfas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Ninfas" } }
                , "sum_Hoja_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Hoja" } }

                // ,"data":{"$push":"$$ROOT"}
            }
        }

        , {
            "$group": {
                "_id": {
                    "plaga": "$_id.plaga"

                    , "lote": "$_id.lote"
                    // ,"arbol":"$_id.arbol"
                }
                , "arboles_dif_censados_x_lote_x_plaga": { "$sum": 1 }

                , "sum_Muertas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Muertas_x_arbol_x_plaga" } }
                , "sum_Huevos_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Huevos_x_arbol_x_plaga" } }
                , "sum_Pupas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Pupas_x_arbol_x_plaga" } }
                , "sum_Larvas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Larvas_x_arbol_x_plaga" } }
                , "sum_Adultos_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Adultos_x_arbol_x_plaga" } }
                , "sum_Ninfas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Ninfas_x_arbol_x_plaga" } }
                , "sum_Hoja_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Hoja_x_arbol_x_plaga" } }

                // ,"data":{"$push":"$$ROOT"}
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "PALMAS POSITIVAS": "$arboles_dif_censados_x_lote_x_plaga"
                            
                            ,"Muertas": "$sum_Muertas_x_lote_x_plaga"
                            ,"Huevos": "$sum_Huevos_x_lote_x_plaga"
                            ,"Pupas": "$sum_Pupas_x_lote_x_plaga"
                            ,"Larvas": "$sum_Larvas_x_lote_x_plaga"
                            ,"Adultos": "$sum_Adultos_x_lote_x_plaga"
                            ,"Ninfas": "$sum_Ninfas_x_lote_x_plaga"
                            ,"Hojas": "$sum_Hoja_x_lote_x_plaga"
                        }
                    ]
                }
            }
        }



        //=====INFO LOTE
        , {
            "$lookup": {
                "from": "form_informaciondelotes",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Peso_promedio_lote": { "$ifNull": ["$info_lote.Peso promedio por lote", 0] }
                , "Siembra_lote": { "$ifNull": ["$info_lote.Siembra", 0] }
                , "Material_lote": { "$ifNull": ["$info_lote.Material", "--sin material---"] }
            }
        }
        , { "$project": { "info_lote": 0 } }




    ]

)
