db.form_modulodeplagas.aggregate(
    [


        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------


        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "Cartography": "$lote"
            }
        },


        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Arbol": 0
                , "Point": 0
            }
        }



        , {
            "$group": {
                "_id": {
                    "plaga": "$Plaga"

                    , "lote": "$lote"
                    , "arbol": "$arbol"
                }
                , "sum_Muertas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Muertas" } }
                , "sum_Huevos_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Huevos" } }
                , "sum_Pupas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Pupas" } }
                , "sum_Larvas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Larvas" } }
                , "sum_Adultos_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Adultos" } }
                , "sum_Ninfas_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Ninfas" } }
                , "sum_Hoja_x_arbol_x_plaga": { "$sum": { "$toDouble": "$Número de Hoja" } }


                , "data": { "$push": "$$ROOT" }


            }
        }

        , { "$addFields": { "Cartography": { "$arrayElemAt": ["$data.Cartography", 0] } } }

        , {
            "$group": {
                "_id": {
                    "plaga": "$_id.plaga"

                    , "lote": "$_id.lote"

                    , "cartography": "$Cartography"

                }
                , "arboles_dif_censados_x_lote_x_plaga": { "$sum": 1 }

                , "sum_Muertas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Muertas_x_arbol_x_plaga" } }
                , "sum_Huevos_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Huevos_x_arbol_x_plaga" } }
                , "sum_Pupas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Pupas_x_arbol_x_plaga" } }
                , "sum_Larvas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Larvas_x_arbol_x_plaga" } }
                , "sum_Adultos_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Adultos_x_arbol_x_plaga" } }
                , "sum_Ninfas_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Ninfas_x_arbol_x_plaga" } }
                , "sum_Hoja_x_lote_x_plaga": { "$sum": { "$toDouble": "$sum_Hoja_x_arbol_x_plaga" } }


            }
        }


        , {
            "$lookup": {
                "from": "form_informaciondelotes",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$_id.lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Peso_promedio_lote": { "$ifNull": ["$info_lote.Peso promedio por lote", 0] }
                , "Siembra_lote": { "$ifNull": ["$info_lote.Siembra", 0] }
                , "Material_lote": { "$ifNull": ["$info_lote.Material", "--sin material---"] }
            }
        }
        , { "$project": { "info_lote": 0 } }
        
        
        
        //-----agrupar x (material o siembra)
         , {
            "$group": {
                "_id": {
                    "plaga": "$_id.plaga"

                    //, "lote": "$_id.lote"
                    , "siembra_lote": "$Siembra_lote"

                    //, "cartography": "$_id.cartography"

                }
                ,"arboles_dif_censados_x_lote_x_plaga": { "$sum": { "$toDouble": "$arboles_dif_censados_x_lote_x_plaga" } }
                , "sum_Larvas_x_agrupacion": { "$sum": { "$toDouble": "$sum_Larvas_x_lote_x_plaga" } }
                //...pupas, huevos, nifas...
                
                //---->>AGRUPAR CARTOGRAFIAS
                , "cartography": {"$push":"$_id.cartography"}

            }
        }
        
        ,{ "$unwind": "$cartography" }
        


        , {
            "$addFields": {
                "indicador": {
                    "$cond": {
                        "if": { "$eq": ["$arboles_dif_censados_x_lote_x_plaga", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$sum_Larvas_x_agrupacion", "$arboles_dif_censados_x_lote_x_plaga"]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
            }
        }


        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }] },
                        "then": "#4bff00",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 3] }]
                                },
                                "then": "#66ff33",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 3] }, { "$lt": ["$indicador", 6] }]
                                        },
                                        "then": "#33cc33",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 6] }, { "$lt": ["$indicador", 9] }]
                                                },
                                                "then": "#ff6600",
                                                "else": "#ff0000"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },

                "rango": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }] },
                        "then": "A-[0- 1)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 3] }]
                                },
                                "then": "B-[1- 3)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 3] }, { "$lt": ["$indicador", 6] }]
                                        },
                                        "then": "C-[3- 6)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 6] }, { "$lt": ["$indicador", 9] }]
                                                },
                                                "then": "D-[6- 9)",
                                                "else": "E-(>=9)"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


            }
        }


        , {
            "$project": {
                "_id": null,
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    //"Lote": "$_id.lote",

                    "Siembra": "$_id.siembra_lote",
                    // "Material": "$Material_lote",
                    // "Peso Promedio": "$Peso_promedio__lote",

                    "Plaga": "$_id.plaga",
                    "Rango": "$rango",
                    "Promedio LARVAS x Hoja": {
                        "$concat": [
                            { "$toString": "$indicador" },
                            " #"
                        ]
                    },

                    "color": "$color"
                },
                "geometry": "$cartography.geometry"
            }
        }


    ]

    , { allowDiskUse: true })
