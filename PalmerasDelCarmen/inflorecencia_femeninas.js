db.form_aconteodeinflorescenciasfemeninas.aggregate(

    [
        {
            "$addFields": {
                "variable_cartografia": "$lotes"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        //lote_num
        { "$match": { "lote": { "$ne": "no existe" } } },
        {
            "$addFields": {
                "lote_num": { "$split": [{ "$trim": { "input": "$lote", "chars": " " } }, " "] }
            }
        },
        {
            "$addFields": {
                "lote_num": { "$arrayElemAt": ["$lote_num", 0] }
            }
        },
        {
            "$addFields": {
                "lote_num": { "$toDouble": "$lote_num" }
            }
        },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        //linea_num
        { "$match": { "linea": { "$ne": "no existe" } } },
        {
            "$addFields": {
                "linea_num": { "$split": [{ "$trim": { "input": "$linea", "chars": "-" } }, "-"] }
            }
        },
        {
            "$addFields": {
                "linea_num": { "$arrayElemAt": ["$linea_num", -1] }
            }
        },
        {
            "$addFields": {
                "linea_num": { "$toDouble": "$linea_num" }
            }
        },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        //arbol_num
        { "$match": { "arbol": { "$ne": "no existe" } } },
        {
            "$addFields": {
                "arbol_num": { "$split": [{ "$trim": { "input": "$arbol", "chars": "-" } }, "-"] }
            }
        },
        {
            "$addFields": {
                "arbol_num": { "$arrayElemAt": ["$arbol_num", -1] }
            }
        },
        {
            "$addFields": {
                "arbol_num": { "$toDouble": "$arbol_num" }
            }
        },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "lotes": 0,
                "Point": 0,
                "bloque_id": 0,

                "Lote": 0,
                "Linea": 0,
                "Palma": 0
            }
        }

        //variable para ordenar arboles
        , {
            "$addFields": {
                "ORDENAR_ARBOL": {
                    "$sum": [
                        {"$multiply": ["$lote_num", 100000]},
                        {"$multiply": ["$linea_num", 100]},
                        {"$multiply": ["$arbol_num", 10]}
                    ]
                }
            }
        }


        , {
            "$sort": {
                // "lote_num": 1,
                // "linea_num": 1,
                // "arbol_num": 1

                "ORDENAR_ARBOL": 1
            }
        }

        // //--test
        // ,{
        //     $group: { _id: "$lote_num"}
        // }

        // , {
        //     $match: {
        //         "lote":"2"
        //     }
        // }

    ]
)

// .count()
