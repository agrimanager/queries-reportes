
// var tablas = [
//     "form_modulodeenfermedades",
//     "form_svcensodeplagas",
//     "form_svmodulodetrampas",
//     "form_svcensodeproduccionrff",
//     "form_svcensodeenfermedades",                   //-----ok
//     "form_aconteodeinflorescenciasfemeninas",
// ]

// A Conteo de Inflorescencias Femeninas
// C cargue
// Modulo de Enfermedades
// Sv Censo de Enfermedades
// Sv censo de kamerunicus
// Sv Censo de Plagas
// Sv Censo de Produccion RFF
// Sv Fruitzet
// Sv Medidas vegetativas
// Sv Modulo de Trampas



var tablas = [
    "form_svcensodeenfermedades",                   //---
    "form_svcensodeplagas",                         //---
    "form_svcensodeproduccionrff",                  //---
    "form_aconteodeinflorescenciasfemeninas",       //---
]



db.form_aconteodeinflorescenciasfemeninas.aggregate(

    //  {
    //     "$match": {
    //         "$and": [
    //             {
    //                 "rgDate": {
    //                     "$gte": ISODate("2022-01-01T11:44:17.117-05:00")
    //                     // "$gte": ISODate("2022-06-01T11:44:17.117-05:00")
    //                 }
    //             }
    //         ]
    //     }
    // },


    {
        "$match": {
            capture: { $ne: "AG1" }
        }
    },

    {
        $group: {
            _id: {
                supervisor: "$supervisor"
            }
            , cantidad: { $sum: 1 }
            // ,data:{$push:"$$ROOT"}
        }
    }

    // , {
    //     $sort: {
    //         "_id.supervisor": 1
    //     }
    // }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    { "cantidad": "$cantidad" }
                ]
            }
        }
    }

    , {
        $sort: {
            "supervisor": 1
        }
    }

)
