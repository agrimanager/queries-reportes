
var tablas = [
    // "form_svcensodeenfermedades",                   //---
    "form_svcensodeplagas",                         //---
    "form_svcensodeproduccionrff",                  //---
    "form_aconteodeinflorescenciasfemeninas",       //---
];



var supervisores = [

    // {
    //     nombre_concat: "xxxxxx",
    //     array_posibles_nombres: [
    //         "x1",
    //         "x2",
    //         "x3",
    //         "x4",
    //     ]
    // },


    // {nombre_concat: "
    // xxxxxx
    // ",array_posibles_nombres: ["
    // x1
    // ","
    // x2
    // ","
    // x3
    // ","
    // x4
    // ",]},


    { nombre_concat: " Anderson Rivas", array_posibles_nombres: ["Anderson", "", "", "",] },
    { nombre_concat: " Luis Alejandro Parada Diaz", array_posibles_nombres: ["Luis Alejandro", "", "", "",] },
    { nombre_concat: " SANDRA MILENA CHAPARRO MARTINEZ", array_posibles_nombres: ["SANDRA MILENA", "SANDRA MILENA CHAPARRO MARTINEZ", "", "",] },
    { nombre_concat: " WILSON RICARDO CASTRO BARRERA", array_posibles_nombres: ["WILSON RICARDO", "Wilson Ricardo", "", "",] },
    { nombre_concat: " YENNI PATRICIA MORENO RIOS", array_posibles_nombres: ["YENNY PATRICIA", "", "", "",] },
    { nombre_concat: "Sv 1 ANDREA DEL PILAR CHAVITA PULIDO", array_posibles_nombres: ["Sv 1 ANDREA DEL PILAR", "", "", "",] },
    { nombre_concat: "Sv 2 HEYNER ALFONSO PEREZ ACUÑA", array_posibles_nombres: ["HEYNER ALFONSO", "Sv 2 HEYNER ALFONSO", "", "",] },
    { nombre_concat: "Sv 2 LEONEL RIVAS BORJA", array_posibles_nombres: ["LEONEL", "Sv 2 LEONEL", "Sv 5 LEONEL", "",] },
    { nombre_concat: "Sv 3 JHON FREDY BUENO FONSECA", array_posibles_nombres: ["Jhon Fredy", "Sv 3 JHON FREDY", "", "",] },
    { nombre_concat: "Sv 4 JOHN FREDY BARRERA GRANADOS", array_posibles_nombres: [" JOHN FREDY BARRERA GRANADOS", "JOHN FREDY", "JOHN FREDY BARRERA GRANADOS", "Sv 4 JOHN FREDY",] },
    { nombre_concat: "Sv 6 JAINER DAVID JIMENEZ CASTAÑEDA", array_posibles_nombres: ["JAINER DAVID", "JAINER DAVID JIMENEZ CASTAÑEDA", "Sv 6 JAINER DAVID", "M 3 JAINER DAVID",] },
    { nombre_concat: "Sv 7 Cristian Stiven Moncada Morales", array_posibles_nombres: ["Sv 7 Cristian Stiven", "", "", "",] },
    { nombre_concat: "Sv 8 Cristian Alexander Medina Lemus", array_posibles_nombres: ["Sv 8 Cristian Alexander", "", "", "",] },



];



tablas.forEach(tabla_i => {


    supervisores.forEach(supervisor_i => {

        db.getCollection(tabla_i).updateMany(
            {
                "supervisor": {
                    $in: supervisor_i.array_posibles_nombres
                }
            },
            {
                $set: {
                    supervisor: supervisor_i.nombre_concat
                }
            }

        )

    })

})
