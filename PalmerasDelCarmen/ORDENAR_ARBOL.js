
//----parte0 -- CARTOGRAFIA

//.....
//.....
//.....


//----parte1 -- num lote,linea y arbol


{
    "$addFields": {
        "lote": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
            }
        }
    }
},

{
    "$unwind": {
        "path": "$lote",
        "preserveNullAndEmptyArrays": true
    }
},

{ "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

{ "$match": { "lote": { "$ne": "no existe" } } },

{
    "$addFields": {
        "lote_num": { "$split": [{ "$trim": { "input": "$lote", "chars": " " } }, " "] }
    }
},
{
    "$addFields": {
        "lote_num": { "$arrayElemAt": ["$lote_num", 0] }
    }
},
{
    "$addFields": {
        "lote_num": { "$toDouble": "$lote_num" }
    }
},

{
    "$addFields": {
        "linea": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
            }
        }
    }
},

{
    "$unwind": {
        "path": "$linea",
        "preserveNullAndEmptyArrays": true
    }
},

{ "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

{ "$match": { "linea": { "$ne": "no existe" } } },

{
    "$addFields": {
        "linea_num": { "$split": [{ "$trim": { "input": "$linea", "chars": "-" } }, "-"] }
    }
},
{
    "$addFields": {
        "linea_num": { "$arrayElemAt": ["$linea_num", -1] }
    }
},
{
    "$addFields": {
        "linea_num": { "$toDouble": "$linea_num" }
    }
},

{
    "$addFields": {
        "arbol": {
            "$filter": {
                "input": "$objetos_del_cultivo",
                "as": "item_cartografia",
                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
            }
        }
    }
},

{
    "$unwind": {
        "path": "$arbol",
        "preserveNullAndEmptyArrays": true
    }
},

{ "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

{ "$match": { "arbol": { "$ne": "no existe" } } },

{
    "$addFields": {
        "arbol_num": { "$split": [{ "$trim": { "input": "$arbol", "chars": "-" } }, "-"] }
    }
},
{
    "$addFields": {
        "arbol_num": { "$arrayElemAt": ["$arbol_num", -1] }
    }
},
{
    "$addFields": {
        "arbol_num": { "$toDouble": "$arbol_num" }
    }
},




//.....
//.....
//.....



//----parte2 -- consecutivo lote,linea y arbol
, {
      "$addFields": {
          "ORDENAR_ARBOL": {
              "$sum": [
                  {"$multiply": ["$lote_num", 100000]},
                  {"$multiply": ["$linea_num", 100]},
                  {"$multiply": ["$arbol_num", 10]}
              ]
          }
      }
  }


, {
    "$sort": {
        "ORDENAR_ARBOL": 1
    }
}
