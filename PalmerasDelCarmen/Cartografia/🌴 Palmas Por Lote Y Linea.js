db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },

        {
            "$lookup": {
                "from": "cartography",
                "as": "data1",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [


                    {
                        "$match": {
                            "properties.type": "lines"
                        }
                    }

                    , {
                        "$addFields": {
                            "cantidad_arboles_x_linea": {
                                "$ifNull": ["$properties.custom.num_arboles.value", 0]
                            },

                            "name": "$properties.name"
                        }
                    }

                    , {
                        "$project": {
                            "name": 1,
                            "cantidad_arboles_x_linea": 1,
                            // "cantidad_erradicada_x_linea": 1,
                            "path": 1
                        }
                    }



                    , {
                        "$addFields": {
                            "split_path_oid": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path_oid", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },


                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties",
                                            "else": "$feature_3.properties"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "cantidad_arboles_x_lote": {
                                // "$ifNull": ["$lote.custom.Número de palmas.value", 0]
                                "$ifNull": ["$lote.custom.num_arboles.value", 0]
                            }
                        }
                    },

                    { "$addFields": { "lote": { "$ifNull": ["$lote.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": "$name"
                        }
                    }

                    , {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0,

                            "path": 0,
                            "name": 0
                        }
                    }

                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }
                ]
            }
        }



        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data1"
                        , []
                    ]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        , {
            "$project": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "cantidad_arboles_x_lote": "$cantidad_arboles_x_lote",
                "linea": "$linea",
                "cantidad_arboles_x_linea": "$cantidad_arboles_x_linea",
                "linea_id": "$_id",
                "rgDate": "$rgDate",
            }
        }

        , {
            "$project": {

                "_id": 0
            }
        }





        , {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "linea_id": "$linea_id"
                },
                "as": "data_erradicadas",
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$or": [
                                    { "$eq": ["$properties.custom.Erradicada por Raleo.value", true] },
                                    { "$eq": ["$properties.custom.Erradicada.value", true] },
                                    { "$eq": ["$properties.custom.Sobre poda Raleo.value", true] }
                                ]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_oid": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "oid_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_oid", 3] } }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$oid_id", "$$linea_id"]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "erradicadas": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.custom.Erradicada.value", true] },
                                    "then": 1,
                                    "else": 0
                                }
                            },
                            "erradicadas_raleo": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.custom.Erradicada por Raleo.value", true] },
                                    "then": 1,
                                    "else": 0
                                }
                            },
                            "erradicadas_total": {
                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$properties.custom.Erradicada por Raleo.value", true] },
                                            { "$eq": ["$properties.custom.Erradicada.value", true] }
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }

                            ,
                            "sobre_poda_raleo": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.custom.Sobre poda Raleo.value", true] },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    },

                    {
                        "$group": {
                            "_id": {
                                "linea": "$oid_id"
                            },
                            "count_err": { "$sum": "$erradicadas" },
                            "count_err_raleo": { "$sum": "$erradicadas_raleo" },
                            "count_err_total": { "$sum": "$erradicadas_total" }

                            , "count_sobre_poda_raleo": { "$sum": "$sobre_poda_raleo" }
                        }
                    }
                ]
            }
        },
        {
            "$addFields": {
                "cantidad_erradicada_x_linea": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_erradicadas.count_err", 0] },
                        0
                    ]
                },
                "cantidad_erradicada_raleo_x_linea": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_erradicadas.count_err_raleo", 0] },
                        0
                    ]
                },
                "cantidad_erradicada_total_x_linea": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_erradicadas.count_err_total", 0] },
                        0
                    ]
                }

                ,
                "cantidad_sobre_poda_raleo_total_x_linea": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_erradicadas.count_sobre_poda_raleo", 0] },
                        0
                    ]
                }
            }
        },


        {
            "$project": {

                "data_erradicadas": 0
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                },
                "count_erradicada_x_lote": { "$sum": "$cantidad_erradicada_x_linea" },
                "count_erradicada_raleo_x_lote": { "$sum": "$cantidad_erradicada_raleo_x_linea" },
                "count_erradicada_total_x_lote": { "$sum": "$cantidad_erradicada_total_x_linea" },
                "count_sobre_poda_raleo_total_x_lote": { "$sum": "$cantidad_sobre_poda_raleo_total_x_linea" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "cantidad_erradicada_x_lote": "$count_erradicada_x_lote",
                            "cantidad_erradicada_raleo_x_lote": "$count_erradicada_raleo_x_lote",
                            "cantidad_erradicada_total_x_lote": "$count_erradicada_total_x_lote",
                            "cantidad_sobre_poda_raleo_total_x_lote": "$count_sobre_poda_raleo_total_x_lote"
                        }

                    ]
                }
            }
        },





        {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "linea_id": "$linea_id"
                },
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$properties.custom.Erradicada.value", true] },
                                    { "$eq": [{ "$type": "$properties.mobileUpdateSync" }, "date"] }
                                ]
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "split_path_oid": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "oid_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_oid", 3] } }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$oid_id", "$$linea_id"]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "properties.mobileUpdateUser",
                            "foreignField": "_id",
                            "as": "data_users"
                        }
                    },
                    { "$unwind": "$data_users" },

                    { "$limit": 1 }
                ],
                "as": "data_arboles"
            }
        },


        {
            "$addFields": {
                "fecha_modificacion": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_arboles.properties.mobileUpdateSync", 0] },
                        "sin datos"
                    ]
                },

                "usuario_modificacion": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_arboles.data_users.fullName", 0] },
                        "sin datos"
                    ]
                }
            }
        },

        {
            "$project": {

                "data_arboles": 0
            }
        },


        {
            "$addFields": {
                "total_arboles_activos_x_linea": {
                    "$subtract": ["$cantidad_arboles_x_linea", "$cantidad_erradicada_total_x_linea"]
                },

                "total_arboles_activos_x_lote": {
                    "$subtract": ["$cantidad_arboles_x_lote", "$cantidad_erradicada_total_x_lote"]
                }
            }
        }


        // {
        //     "$project": {
        //         "finca": 1,
        //         "bloque": 1,
        //         "lote": 1,
        //         "linea": 1,
        //         "rgDate": 1,

        //         "num_arboles_x_linea": "$cantidad_arboles_x_linea",
        //         "num_erradicados_x_linea": "$cantidad_erradicada_x_linea",
        //         "num_erradicados_raleo_x_linea": "$cantidad_erradicada_raleo_x_linea",
        //         "num_erradicados_total_x_linea": "$cantidad_erradicada_total_x_linea",
        //         "total_arboles_activos_x_linea": {
        //             "$subtract": ["$cantidad_arboles_x_linea", "$cantidad_erradicada_total_x_linea"]
        //         },

        //         "num_arboles_x_lote": "$cantidad_arboles_x_lote",
        //         "num_erradicados_x_lote": "$cantidad_erradicada_x_lote",
        //         "num_erradicados_raleo_x_lote": "$cantidad_erradicada_raleo_x_lote",
        //         "num_erradicados_total_x_lote": "$cantidad_erradicada_total_x_lote",
        //         "total_arboles_activos_x_lote": {
        //             "$subtract": ["$cantidad_arboles_x_lote", "$cantidad_erradicada_total_x_lote"]
        //         },

        //         "fecha_modificacion": 1,
        //         "usuario_modificacion": 1
        //     }
        // }




    ]
)
