var data = db.cartography.aggregate(
    {
        $match: {
            "properties.type": "lot"
        }
    }
    , {
        $addFields: {
            "num_arboles": "$properties.custom.num_arboles.value"
            , "num_arboles_erradicados": "$properties.custom.Número de palmas erradicadas.value"
            , "ha": "$properties.custom.Hectáreas.value"
        }
    }
    , {
        $addFields: {
            "num_arboles_activos": {
                $subtract: ["$num_arboles", "$num_arboles_erradicados"]
            }
        }
    }
    , {
        $addFields: {
            "densidad_ha_arboles": {

                "$cond": {
                    "if": { "$eq": ["$ha", 0] },
                    "then": 0,
                    "else": {
                        $divide: ["$num_arboles_activos", "$ha"]
                    }
                }
            }
        }
    }
    , {
        "$addFields": {
            "densidad_ha_arboles": { "$divide": [{ "$subtract": [{ "$multiply": ["$densidad_ha_arboles", 100] }, { "$mod": [{ "$multiply": ["$densidad_ha_arboles", 100] }, 1] }] }, 100] }
        }
    }


)

// data

data.forEach(i => {
    db.cartography.update(
        {
            "properties.name": i.properties.name
        },
        {
            $set: {
                // "properties.custom.Densidad de siembra.value":i.densidad_ha_arboles
                "properties.custom.Densidad de siembra": {
                    "type": "number",
                    "value": i.densidad_ha_arboles
                },
                "properties.custom.Número de palmas activas": {
                    "type": "number",
                    "value": i.num_arboles_activos
                },
            }
        }
    )
})
