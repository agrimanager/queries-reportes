

// //---ver opciones de maestro Plaga
// db.masters.aggregate( 
//     {
//         $match:{
//             "_id" : ObjectId("5d51cc90ec91068edabcde41"),
//         }
//     }
//     ,{$unwind:"$options"}
    
// )


// //----ver plagas agrupadas
// db.form_modulodeplagas.aggregate( 
//     {
//         $group:{
//             _id:"$Plaga"
//             ,cantidad:{$sum:1}
//         }
//     }
    
// );



// //----update ejemplo XXXZZZZ
// db.form_modulodeplagas.updateMany(
//     {
//         "Plaga": "XXXX"
//     },
//     {
//         $set: {
//             "Plaga": "ZZZZZZ"
//         }
//     }
// )


// //----update ejemplo XXXZZZZ (min)
// db.form_modulodeplagas.updateMany({"Plaga": "XXXX"},{$set: {"Plaga": "ZZZZZZ"}})



// //----update ejemplo XXXZZZZ (min-excel)
// db.form_modulodeplagas.updateMany({"Plaga": "
// XXXX_old
// "},{$set: {"Plaga": "
// ZZZZZZ_new
// "}})



//----update ejemplo XXXZZZZ (min-excel)
// db.form_modulodeplagas.updateMany({"Plaga": "
// XXXX_old
// "},{$set: {"Plaga": "
// ZZZZZZ_new
// "}});




//--------------UPDATE FINAL
db.form_modulodeplagas.updateMany({"Plaga": "Acaro rojo"},{$set: {"Plaga": "Acaro rojo"}});
db.form_modulodeplagas.updateMany({"Plaga": "Acaros (Tetranychus Urticae)"},{$set: {"Plaga": "acaros tetranychus urticae"}});
db.form_modulodeplagas.updateMany({"Plaga": "acaros tetranychus urticae"},{$set: {"Plaga": "acaros tetranychus urticae"}});
db.form_modulodeplagas.updateMany({"Plaga": "Arañita Roja del Palto(Oligonychus yothersi)"},{$set: {"Plaga": "Arañita Roja del Palto(Oligonychus yothersi)"}});
db.form_modulodeplagas.updateMany({"Plaga": "arbol sano"},{$set: {"Plaga": "arbol sano"}});
db.form_modulodeplagas.updateMany({"Plaga": "Árbol Sano"},{$set: {"Plaga": "arbol sano"}});
db.form_modulodeplagas.updateMany({"Plaga": "Barrenador"},{$set: {"Plaga": "Barrenador"}});
db.form_modulodeplagas.updateMany({"Plaga": "Barrenador de Pino"},{$set: {"Plaga": "Barrenador de Pino"}});
db.form_modulodeplagas.updateMany({"Plaga": "Barrenador del tallo (Copturomimus Perseae)"},{$set: {"Plaga": "barrenador del tallo copturomimus perseae"}});
db.form_modulodeplagas.updateMany({"Plaga": "Barrenador del Tallo (Xyleborus sp)"},{$set: {"Plaga": "barrenador del tallo xyleborus sp"}});
db.form_modulodeplagas.updateMany({"Plaga": "barrenador del tallo copturomimus perseae"},{$set: {"Plaga": "barrenador del tallo copturomimus perseae"}});
db.form_modulodeplagas.updateMany({"Plaga": "barrenador del tallo xyleborus sp"},{$set: {"Plaga": "barrenador del tallo xyleborus sp"}});
db.form_modulodeplagas.updateMany({"Plaga": "Bombacoccus "},{$set: {"Plaga": "Bombacoccus "}});
db.form_modulodeplagas.updateMany({"Plaga": "Compsus"},{$set: {"Plaga": "Compsus"}});
db.form_modulodeplagas.updateMany({"Plaga": "Conchuela Negra del Olivo"},{$set: {"Plaga": "Conchuela Negra del Olivo"}});
db.form_modulodeplagas.updateMany({"Plaga": "Crisomélido"},{$set: {"Plaga": "crisomelido"}});
db.form_modulodeplagas.updateMany({"Plaga": "Cucarrón Marceño (Phyllophaga obsoleta)"},{$set: {"Plaga": "cucarron marceño phyllophaga obsoleta"}});
db.form_modulodeplagas.updateMany({"Plaga": "Cucarrón Marceño(Frutos)"},{$set: {"Plaga": "cucarron marceñofrutos"}});
db.form_modulodeplagas.updateMany({"Plaga": "Cucarrón Marceño(Hojas)"},{$set: {"Plaga": "cucarron marceñohojas"}});
db.form_modulodeplagas.updateMany({"Plaga": "cucarron marceñofrutos"},{$set: {"Plaga": "cucarron marceñofrutos"}});
db.form_modulodeplagas.updateMany({"Plaga": "cucarron marceñohojas"},{$set: {"Plaga": "cucarron marceñohojas"}});
db.form_modulodeplagas.updateMany({"Plaga": "daño marceño"},{$set: {"Plaga": "daño marceño"}});
db.form_modulodeplagas.updateMany({"Plaga": "Daño Marceño "},{$set: {"Plaga": "daño marceño"}});
db.form_modulodeplagas.updateMany({"Plaga": "Daño Pulgón"},{$set: {"Plaga": "daño pulgon"}});
db.form_modulodeplagas.updateMany({"Plaga": "Escama Blanca"},{$set: {"Plaga": "escama blanca"}});
db.form_modulodeplagas.updateMany({"Plaga": "Escama fruto"},{$set: {"Plaga": "Escama fruto"}});
db.form_modulodeplagas.updateMany({"Plaga": "Escama Hojas"},{$set: {"Plaga": "Escama Hojas"}});
db.form_modulodeplagas.updateMany({"Plaga": "Falso medidor"},{$set: {"Plaga": "falso medidor"}});
db.form_modulodeplagas.updateMany({"Plaga": "falso medidor"},{$set: {"Plaga": "falso medidor"}});
db.form_modulodeplagas.updateMany({"Plaga": "Falso Medidor"},{$set: {"Plaga": "falso medidor"}});
db.form_modulodeplagas.updateMany({"Plaga": "Grillos "},{$set: {"Plaga": "Grillos "}});
db.form_modulodeplagas.updateMany({"Plaga": "gusano de canasta"},{$set: {"Plaga": "gusano de canasta"}});
db.form_modulodeplagas.updateMany({"Plaga": "Gusano de Canasta"},{$set: {"Plaga": "gusano de canasta"}});
db.form_modulodeplagas.updateMany({"Plaga": "gusano defoliador"},{$set: {"Plaga": "gusano defoliador"}});
db.form_modulodeplagas.updateMany({"Plaga": "Gusano Defoliador"},{$set: {"Plaga": "gusano defoliador"}});
db.form_modulodeplagas.updateMany({"Plaga": "Gusano pega hojas (Hedylepta indicata)"},{$set: {"Plaga": "gusano pega hojas hedylepta indicata"}});
db.form_modulodeplagas.updateMany({"Plaga": "gusano pega hojas hedylepta indicata"},{$set: {"Plaga": "gusano pega hojas hedylepta indicata"}});
db.form_modulodeplagas.updateMany({"Plaga": "Hormigas Arrieras (Atta spp)"},{$set: {"Plaga": "hormigas arrieras atta spp"}});
db.form_modulodeplagas.updateMany({"Plaga": "hormigas arrieras atta spp"},{$set: {"Plaga": "hormigas arrieras atta spp"}});
db.form_modulodeplagas.updateMany({"Plaga": "Mariposa (Defoliador)"},{$set: {"Plaga": "Mariposa Gusano Defoliador"}});
db.form_modulodeplagas.updateMany({"Plaga": "mariposas"},{$set: {"Plaga": "mariposas"}});
db.form_modulodeplagas.updateMany({"Plaga": "Mariposas"},{$set: {"Plaga": "mariposas"}});
db.form_modulodeplagas.updateMany({"Plaga": "Monalonium (Monalonion Chupanga)"},{$set: {"Plaga": "monalonium monalonion chupanga"}});
db.form_modulodeplagas.updateMany({"Plaga": "monalonium monalonion chupanga"},{$set: {"Plaga": "monalonium monalonion chupanga"}});
db.form_modulodeplagas.updateMany({"Plaga": "Mosca del Fruto"},{$set: {"Plaga": "mosca del fruto"}});
db.form_modulodeplagas.updateMany({"Plaga": "Mosca del Ovario (Bruggmanniella sp)"},{$set: {"Plaga": "mosca del ovario bruggmanniella sp"}});
db.form_modulodeplagas.updateMany({"Plaga": "mosca del ovario bruggmanniella sp"},{$set: {"Plaga": "mosca del ovario bruggmanniella sp"}});
db.form_modulodeplagas.updateMany({"Plaga": "Mosca Oriental "},{$set: {"Plaga": "Mosca Oriental "}});
db.form_modulodeplagas.updateMany({"Plaga": "Ninguno"},{$set: {"Plaga": "Ninguno"}});
db.form_modulodeplagas.updateMany({"Plaga": "Otros gusanos "},{$set: {"Plaga": "Otros gusanos "}});
db.form_modulodeplagas.updateMany({"Plaga": "Picudo (Heilipus Lauri)"},{$set: {"Plaga": "picudo heilipus lauri"}});
db.form_modulodeplagas.updateMany({"Plaga": "picudo heilipus lauri"},{$set: {"Plaga": "picudo heilipus lauri"}});
db.form_modulodeplagas.updateMany({"Plaga": "Plaga Nueva (Tomar foto y especificar)"},{$set: {"Plaga": "plaga nueva tomar foto y especificar"}});
db.form_modulodeplagas.updateMany({"Plaga": "plaga nueva tomar foto y especificar"},{$set: {"Plaga": "plaga nueva tomar foto y especificar"}});
db.form_modulodeplagas.updateMany({"Plaga": "Postura de Mariposa"},{$set: {"Plaga": "Postura de Mariposa"}});
db.form_modulodeplagas.updateMany({"Plaga": "stenoma fruto"},{$set: {"Plaga": "stenoma fruto"}});
db.form_modulodeplagas.updateMany({"Plaga": "Stenoma Fruto"},{$set: {"Plaga": "stenoma fruto"}});
db.form_modulodeplagas.updateMany({"Plaga": "stenoma tallo"},{$set: {"Plaga": "stenoma tallo"}});
db.form_modulodeplagas.updateMany({"Plaga": "Stenoma Tallo"},{$set: {"Plaga": "stenoma tallo"}});
db.form_modulodeplagas.updateMany({"Plaga": "Thrips (Heliothripshaemorrhoidalis)"},{$set: {"Plaga": "thrips frankliniella occidentalis"}});
db.form_modulodeplagas.updateMany({"Plaga": "thrips frankliniella occidentalis"},{$set: {"Plaga": "thrips frankliniella occidentalis"}});
db.form_modulodeplagas.updateMany({"Plaga": "thrips heliothripshaemorrhoidalis"},{$set: {"Plaga": "thrips frankliniella occidentalis"}});
db.form_modulodeplagas.updateMany({"Plaga": "Trips "},{$set: {"Plaga": "thrips frankliniella occidentalis"}});
db.form_modulodeplagas.updateMany({"Plaga": "Trips del Palto (Heliothrips haemorrhoidalis)"},{$set: {"Plaga": "thrips frankliniella occidentalis"}});
db.form_modulodeplagas.updateMany({"Plaga": "Trozador "},{$set: {"Plaga": "trozador"}});
db.form_modulodeplagas.updateMany({"Plaga": "Vaquita del follaje (Compsus sp)"},{$set: {"Plaga": "vaquita del follaje compsus sp"}});
db.form_modulodeplagas.updateMany({"Plaga": "vaquita del follaje compsus sp"},{$set: {"Plaga": "vaquita del follaje compsus sp"}});
db.form_modulodeplagas.updateMany({"Plaga": ""},{$set: {"Plaga": ""}});
