
//----Ojo usar desde db local

//====== Resultado
var result_info = [];


//--obtener bases de datos
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["agricolaocoa"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name).forms.aggregate().allowDiskUse();;

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var result = db.getSiblingDB(db_name)
            .getCollection(item.anchor)
            .deleteMany({
                //---query delete
                capture: "AG1"
            });
            

        result_info.push({
            database: db_name,
            colleccion: item.anchor,
            formulario: item.name,
            result: result.deletedCount

        })

    });



});

//--imprimir resultado
result_info