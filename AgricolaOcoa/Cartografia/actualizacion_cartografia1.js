
//----Fincas Colombia
//$regex: "^,5d235c7ff0c09e6c437d4108,"//la concha
// $regex: "^,5d235fc8f0c09e6c437d410c,"//la polola
// $regex: "^,5d235ea5f0c09e6c437d410a,"//la leticia



//----propiedades fincas ARBOL (la concha) --- 4 VARIABLES
{
	"name" : "Erradicada",
	"type" : "bool",
	"color" : "#000000"
},
{
	"name" : "Resiembra",
	"type" : "bool",
	"color" : "#F5E904"
},
{
	"name" : "Vegetativos",
	"type" : "bool",
	"color" : "#03A9F4"
},
{
	"name" : "Edranol",
	"type" : "bool",
	"color" : "#E36101"
}



//----propiedades ARBOL (la concha) --- 8 VARIABLES
{
	"Erradicada" : {
		"type" : "bool",
		"value" : false
	},
	"Resiembra" : {
		"type" : "bool",
		"value" : false
	},
	"Fecha de siembra" : {
		"type" : "date",
		"value" : ISODate("1970-01-01T07:00:00.000-05:00")
	},
	"Fecha de erradicacion" : {
		"type" : "date",
		"value" : ISODate("1970-01-01T07:00:00.000-05:00")
	},
	"Fecha de resiembra" : {
		"type" : "date",
		"value" : ISODate("1970-01-01T07:00:00.000-05:00")
	},
	"Informacion" : {
		"type" : "string",
		"value" : ""
	},
	"Soqueada" : {
		"type" : "bool",
		"value" : false
	},
	"Fecha de soqueo" : {
		"type" : "date",
		"value" : ISODate("1970-01-01T07:00:00.000-05:00")
	}
}



//----propiedades ARBOL_ACTUALIZADO_MOVIL (la concha) --- 11 VARIABLES
{
	"Erradicada" : {
		"type" : "bool",
		"value" : true,
		"color" : "#000000",
		"id" : 0
	},
	"Resiembra" : {
		"type" : "bool",
		"value" : false,
		"color" : "#F5E904",
		"id" : 2
	},
	"Vegetativos" : {
		"type" : "bool",
		"value" : [ ],
		"color" : "#03A9F4",
		"id" : 4
	},
	"Edranol" : {
		"type" : "bool",
		"value" : [ ],
		"color" : "#E36101",
		"id" : 6
	},
	"Fecha de erradicacion" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 8,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de resiembra" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 9,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de siembra" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 10,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de soqueo" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 11,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Informacion" : {
		"type" : "string",
		"value" : [ ],
		"id" : 12
	},
	"Soqueada" : {
		"type" : "bool",
		"value" : false,
		"id" : 13
	},
	"color" : {
		"type" : "color",
		"value" : "#39AE37",
		"id" : 14
	}
}


//-----------ANALISIS
//AGREGAR 6 PROPIEDADES A FINCAS
{

  //---no agregar
	"Erradicada" : {
		"type" : "bool",
		"value" : true,
		"color" : "#000000",
		"id" : 0
	},
	"Resiembra" : {
		"type" : "bool",
		"value" : false,
		"color" : "#F5E904",
		"id" : 2
	},
	"Vegetativos" : {
		"type" : "bool",
		"value" : [ ],
		"color" : "#03A9F4",
		"id" : 4
	},
	"Edranol" : {
		"type" : "bool",
		"value" : [ ],
		"color" : "#E36101",
		"id" : 6
	},
  "color" : {
		"type" : "color",
		"value" : "#39AE37",
		"id" : 14
	},


  //---si agregar
	"Fecha de erradicacion" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 8,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de resiembra" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 9,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de siembra" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 10,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Fecha de soqueo" : {
		"type" : "date",
		"value" : "1970-01-01T12:00:00Z",
		"id" : 11,
		"date" : "1970-01-01 07:00 am",
		"Visible" : false
	},
	"Informacion" : {
		"type" : "string",
		"value" : [ ],
		"id" : 12
	},
	"Soqueada" : {
		"type" : "bool",
		"value" : false,
		"id" : 13
	},

}
