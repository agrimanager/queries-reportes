db.cartography.aggregate(

    {
        $match: {
            path: {
                //--colombia
                $regex: "^,5d235c7ff0c09e6c437d4108,"//la concha
                // $regex: "^,5d235fc8f0c09e6c437d410c,"//la polola
                // $regex: "^,5d235ea5f0c09e6c437d410a,"//la leticia


                //--chile
                // $regex: "^,5d1cee6fbaa48a3ea0eaacd3,"//cerro_casas
            }
        }
    }

    ,{
        $match:{
            "properties.type":"trees"
        }
    }

    ,{
        $match:{
            // "properties.custom":{$exists:false}
            "properties.custom.Vegetativos":{$exists:true}
        }
    }

    //Erradicada
)
