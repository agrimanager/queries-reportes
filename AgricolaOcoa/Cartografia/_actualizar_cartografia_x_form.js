var data = db.form_identificaciondearboles.aggregate(

    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2023-07-01T06:00:00.000-05:00"),
            "Busqueda fin": new Date,
            "today": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }

                    ,

                    {
                        "$ne": ["$Identificacion de Arboles", ""]
                    }


                ]
            }
        }
    },
    //----------------------------------------------------------------

    //test
    //{$group: { _id: "$Point.farm",cant:{$sum:1},data:{$push:"$$ROOT"}}}

    {
        $unwind: "$Mapa.features"
    },



    {
        $addFields: {
            oid_cartografia: { $toObjectId: "$Mapa.features._id" }
        }
    }


    ,
    {
        $addFields: {
            "Identificacion de Arboles": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$eq": ["$Identificacion de Arboles", "Erradicado"]
                            },
                            "then": "Erradicada"
                        }
                        , {
                            "case": {
                                "$eq": ["$Identificacion de Arboles", "Resiembra"]
                            },
                            "then": "Resiembra"
                        }
                        , {
                            "case": {
                                "$eq": ["$Identificacion de Arboles", "Vegetativo"]
                            },
                            "then": "Productivo"
                        }
                    ],
                    "default": null
                }
            }
        }
    }


    , {
        $group: {
            _id: {
                "nombre": "$Identificacion de Arboles"
            }
            , arboles: { $push: "$oid_cartografia" }
            , cant: { $sum: 1 }
        }
    }


    , {
        "$addFields": {
            "variable_custom": {
                "$concat": ["properties.custom.", "$_id.nombre"]
            }
        }
    }

)



// data


data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
