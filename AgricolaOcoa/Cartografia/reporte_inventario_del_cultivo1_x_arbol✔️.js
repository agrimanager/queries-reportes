[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "cartography",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                {
                    "$match": {
                        "properties.type": { "$in": ["trees"] }
                    }
                },




                { "$match": { "path": { "$not": { "$regex": "^,5d1cee6fbaa48a3ea0eaacd3," } } } },
                { "$match": { "path": { "$not": { "$regex": "^,5d1cee38baa48a3ea0eaacd1," } } } },
                { "$match": { "path": { "$not": { "$regex": "^,5d1cc1dc71170f1724064a80," } } } },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },


                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0

                        , "path": 0
                        , "geometry": 0
                        , "type": 0
                        , "children": 0

                        , "_id": 0
                    }
                }


                , {
                    "$addFields": {
                        "prop_Erradicada": {
                            "$cond": {
                                "if": { "$eq": [{ "$ifNull": ["$properties.custom.Erradicada.value", false] }, true] },
                                "then": 1,
                                "else": 0
                            }
                        }
                    }
                }
                , {
                    "$addFields": {
                        "prop_Resiembra": {
                            "$cond": {
                                "if": { "$eq": [{ "$ifNull": ["$properties.custom.Resiembra.value", false] }, true] },
                                "then": 1,
                                "else": 0
                            }
                        }
                    }
                }
                , {
                    "$addFields": {
                        "prop_Soqueada": {
                            "$cond": {
                                "if": { "$eq": [{ "$ifNull": ["$properties.custom.Soqueada.value", false] }, true] },
                                "then": 1,
                                "else": 0
                            }
                        }
                    }
                }

                , {
                    "$project": {
                        "properties": 0
                    }
                }


                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
