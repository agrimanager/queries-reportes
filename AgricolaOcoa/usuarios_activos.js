

//====== Resultado
var result_info = [];


//--obtener bases de datos
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["agricolaocoa"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {

    //--obtener formularios
    var formularios = db.getSiblingDB(db_name_aux).forms.aggregate();

    //--🔄 ciclar formularios
    formularios.forEach(item => {

        var db_name = db_name_aux;
        var coleccion_name = item.anchor;
        var result = null;


        //---Ejecutar queries

        //===================query1
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            //.aggregate(query_pipeline1)
            .aggregate(
                [
                    //---condicion
                    {
                        "$match": {
                            "$and": [
                                {
                                    "rgDate": {
                                        "$gte": ISODate("2021-06-20T11:44:17.117-05:00")
                                    }
                                },
                                {
                                    "rgDate": {
                                        "$lte": ISODate("2021-07-22T11:44:17.117-05:00")
                                    }
                                }
                                ,
                                {
                                    "uid": ObjectId("5d1cbc5f71170f1724064a70")
                                }
                            ]
                        }
                    }

                    //---variables
                    , {
                        "$addFields": {
                            "Fecha_Txt": {
                                "$dateToString": {
                                    "format": "%Y-%m-%d",
                                    "date": "$rgDate"
                                }
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "supervisor": "$supervisor",
                                "fecha": "$Fecha_Txt",
                                // "finca": "$farm",
                                // "formulario": "$formulario",
                            },
                            "cantidad_censos": { "$sum": 1 }
                        }
                    }

                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id",
                                    {
                                        "cantidad_censos": "$cantidad_censos"
                                    }
                                ]
                            }
                        }
                    }

                ]
            )
            .allowDiskUse();


        // console.log(data_query)
        // data_query


        //--🔄 data
        data_query.forEach(item_data => {
            result_info.push({
                database: db_name,
                colleccion: item.anchor,
                formulario: coleccion_name,
                // tquery: "query1 -- repetidos full",
                // registros_malos: result.nRemoved

                supervisor: item_data.supervisor,
                fecha: item_data.fecha,
                cantidad_censos: item_data.cantidad_censos,

            })
        });
        data_query.close();

    });



});

//--imprimir resultado
result_info
