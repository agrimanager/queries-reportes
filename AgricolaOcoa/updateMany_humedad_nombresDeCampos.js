db.form_modulodehumedad.updateMany(
    {}
    ,
    {
        $rename: {
            //"campo_viejo":"campo_nuevo"
            "Humedad (20 cm)":"Humedad 10 cm",
            "Humedad (40 cm)":"Humedad 20 cm",
        }
    }
)

//--old
// 	"Rango mínimo" : "1.4",
// 	"Rango maxímo" : "1.6",
// 	"Humedad (20 cm)" : "1.6",
// 	"Humedad (40 cm)" : "1.6",

//--new
// 	"Humedad 10 cm" : 1.6,
// 	"Humedad 20 cm" : 1.7,