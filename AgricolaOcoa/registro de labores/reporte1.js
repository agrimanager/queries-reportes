db.form_registrodelabores.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote_id": { "$ifNull": ["$lote._id", "no existe"] } } },
        { "$addFields": { "lote_path": { "$ifNull": ["$lote.path", "no existe"] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Lote": 0
            }
        }


        , {
            "$addFields": {
                "path_lineas": {
                    "$concat": [
                        "$lote_path"
                        , { "$toString": "$lote_id" }
                        , ","
                    ]
                }
            }
        }


        // , {
        //     "$addFields": {
        //         "Linea de inicio": {"$toString":"$Linea de inicio"},
        //         "Linea final": {"$toString":"$Linea final"}
        //     }
        // }


        //lineas
        , {
            "$lookup": {
                "from": "cartography",
                "as": "data_cartografia_lineas",
                "let": {
                    "path_lineas": "$path_lineas"

                    , "linea_inicio": "$Linea de inicio"
                    , "linea_fin": "$Linea final"
                },
                //query
                "pipeline": [


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$path", "$$path_lineas"]
                            }
                        }
                    }


                    , {
                        "$match": {
                            "properties.type": "lines"
                        }
                    }

                    , {
                        "$addFields": {
                            "split_nombre": { "$split": [{ "$trim": { "input": "$properties.name", "chars": "-" } }, "-"] }
                        }
                    }

                    , {
                        "$addFields": {
                            "num_linea": {
                                "$arrayElemAt": ["$split_nombre", -1]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "num_linea": { "$toDouble": "$num_linea" }
                        }
                    }


                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": ["$num_linea", "$$linea_inicio"]
                                    },
                                    {
                                        "$lte": ["$num_linea", "$$linea_fin"]
                                    }

                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "path_arboles": {
                                "$concat": [
                                    "$path"
                                    , { "$toString": "$_id" }
                                    , ","
                                ]
                            }
                        }
                    }


                    , {
                        "$project": {
                            "num_linea": 1,
                            "path_arboles": 1
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }





                ]
            }
        }

        //arboles
        , {
            "$lookup": {
                "from": "cartography",
                "as": "data_cartografia_arboles",
                "let": {
                    "data_cartografia_lineas": "$data_cartografia_lineas"
                },
                //query
                "pipeline": [


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$in": ["$path", "$$data_cartografia_lineas.path_arboles"]
                            }
                        }
                    }


                    , {
                        "$match": {
                            "properties.type": "trees"
                        }
                    }


                    //Erradicada
                    , {
                        "$match": {
                            "properties.custom.Erradicada.value": { "$ne": true }
                        }
                    }

                    , {
                        "$addFields": {
                            "arbol": "$properties.name"
                        }
                    }

                    , {
                        "$project": {
                            "arbol": 1
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }





                ]
            }
        }


        , {
            "$addFields": {
                "cantidad de lineas": { "$size": "$data_cartografia_lineas" }
                , "cantidad de arboles": { "$size": "$data_cartografia_arboles" }
            }
        }


        , {
            "$project": {
                "lote_id": 0
                , "lote_path": 0
                , "path_lineas": 0
                , "data_cartografia_lineas": 0
                , "data_cartografia_arboles": 0
            }
        }


        //empleado numerico
        , {
            "$unwind": {
                "path": "$Colaboradores",
                "preserveNullAndEmptyArrays": true
            }
        }
        , {
            "$addFields": {
                "nombre_colaborador": { "$ifNull": ["$Colaboradores.name", "sin nombre"] },
                "cantinad_colaborador": { "$ifNull": ["$Colaboradores.value", 0] }
            }
        },
        {
            "$project": {
                "Colaboradores": 0
            }
        }




    ]
)
