db.cartography.aggregate(

    //---- Seleccionar lotes
    {
        "$match": {
            "type": "Feature",
            //"geometry.type": "Polygon"
            "properties.type": "lot"
        }
    }

    //---- Agregar valores nuevos
    , {
        "$addFields": {
            "_id_string": { "$toString": "$_id" }
        }
    }

    // //---- Obtener features internos
    , {
        "$lookup": {
            "from": "cartography",
            "as": "internal_features",
            "let": {
                "feature_id_string": "$_id_string"

            },
            "pipeline": [

                //---solo arboles
                {
                    "$match": {
                        "type": "Feature",
                        "properties.type": "trees"
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$gt": [{
                                "$indexOfCP": ["$path", "$$feature_id_string"]
                            },
                            -1
                            ]
                        }
                    }
                },


                {
                    "$group": {
                        // "_id": "$properties.type",
                        "_id": "$properties.custom.Erradicada.value",
                        "count": { "$sum": 1 }
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "type_features": "$_id",
                        "count_features": "$count"
                    }
                }
            ]
        }
    }




    //---- Seleccionar valores a guardar
    , {
        $project: {
            "_id": 1,
            "type": "$properties.type",
            "name": "$properties.name",
            "internal_features": "$internal_features",
            "path": "$path"
        }
    }

    //---NOTA:
    //true  : si erradicados
    //false : no erradicados


    //---- Insertar en nueva coleccion
    , { $out: "cartography_lotes_arboles" }


)
