db.form_arbolessinfruto.aggregate(
    [

        // //Feature como OBJETO
        { "$unwind": "$Linea.features" },

        {
            "$addFields": {
                "name_feature": "$Linea.features.properties.name"
            }
        },


        {
            "$addFields": {
                "split_name": { "$split": [{ "$trim": { "input": "$name_feature", "chars": "-" } }, "-"] }
            }
        },


        {
            "$addFields": {
                // "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$split_name", 0] },
                "lote": { "$arrayElemAt": ["$split_name", 1] },
                "linea": { "$arrayElemAt": ["$split_name", 2] }

                , "linea_nombre": "$name_feature"
            }
        },




        {
            "$project": {
                "name_feature": 0,
                "split_name": 0
            }
        }

    ]


)