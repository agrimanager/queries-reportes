[


    {
        "$addFields": {
            "variable_cartografia": "$PALMA"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "variable_cartografia_oid": { "$toObjectId": "$variable_cartografia.features._id" }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "variable_cartografia_oid",
            "foreignField": "_id",
            "as": "data_cartografia_actual"
        }
    },

    { "$unwind": "$data_cartografia_actual" },


    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$data_cartografia_actual.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    ["$variable_cartografia_oid"]
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "PALMA": 0
            , "data_cartografia_actual": 0
        }
    }




    , {
        "$addFields": {
            "array_enfermedades": [


                {
                    "enfermedad": "PUDRICION BASAL SECA"
                    , "sintoma": {
                        "$cond": {
                            "if": {
                                "$eq": ["$PUDRICION BASAL SECA", ""]
                            },
                            "then": [""],
                            "else": "$PUDRICION BASAL SECA"
                        }
                    }
                    , "cantidad": 1
                },

                {
                    "enfermedad": "MARCHITEZ SORPRESIVA"
                    , "sintoma": {

                        "$cond": {
                            "if": {
                                "$eq": ["$MARCHITEZ SORPRESIVA", ""]
                            },
                            "then": [""],
                            "else": "$MARCHITEZ SORPRESIVA"
                        }

                    }
                    , "cantidad": 1
                },
                {
                    "enfermedad": "ANILLO ROJO"
                    , "sintoma": {


                        "$cond": {
                            "if": {
                                "$eq": ["$ANILLO ROJO", ""]
                            },
                            "then": [""],
                            "else": "$ANILLO ROJO"
                        }


                    }
                    , "cantidad": 1
                },



                {
                    "enfermedad": "SECAMIENTO DEHOJAS"
                    , "sintoma": "SECAMIENTO DEHOJAS"
                    , "cantidad": {
                        "$cond": {
                            "if": {
                                "$eq": ["$SECAMIENTO DEHOJAS", "SI"]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "enfermedad": "DOBLAMINETO DE COGOLLOY FRACTURA DEL PAQUETE DE FLECHA"
                    , "sintoma": "DOBLAMINETO DE COGOLLOY FRACTURA DEL PAQUETE DE FLECHA"
                    , "cantidad": {
                        "$cond": {
                            "if": {
                                "$eq": ["$DOBLAMINETO DE COGOLLOY FRACTURA DEL PAQUETE DE FLECHA", "SI"]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },

                {
                    "enfermedad": "HOJAS CONPRESENCIA DE PESTALOTIOPSIS"
                    , "sintoma": "HOJAS CONPRESENCIA DE PESTALOTIOPSIS"
                    , "cantidad": {
                        "$cond": {
                            "if": {
                                "$gt": ["$HOJAS CONPRESENCIA DE PESTALOTIOPSIS", 0]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "enfermedad": "NUMEO DEFOLIOLOS AFECTADOSX HOJA"
                    , "sintoma": "NUMEO DEFOLIOLOS AFECTADOSX HOJA"
                    , "cantidad": {
                        "$cond": {
                            "if": {
                                "$gt": ["$NUMEO DEFOLIOLOS AFECTADOSX HOJA", 0]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                },
                {
                    "enfermedad": "NUMERO DE PERFORACIONES"
                    , "sintoma": "NUMERO DE PERFORACIONES"
                    , "cantidad": {
                        "$cond": {
                            "if": {
                                "$gt": ["$NUMERO DE PERFORACIONES", 0]
                            },
                            "then": 1,
                            "else": 0
                        }
                    }
                }

            ]
        }
    }


    , {
        "$project": {
            "SECAMIENTO DEHOJAS": 0,
            "DOBLAMINETO DE COGOLLOY FRACTURA DEL PAQUETE DE FLECHA": 0,
            "HOJAS CONPRESENCIA DE PESTALOTIOPSIS": 0,
            "NUMEO DEFOLIOLOS AFECTADOSX HOJA": 0,
            "PUDRICION BASAL SECA": 0,
            "NUMERO DE PERFORACIONES": 0,
            "MARCHITEZ SORPRESIVA": 0,
            "ANILLO ROJO": 0

        }
    }


    , {
        "$addFields": {
            "array_enfermedades": {
                "$filter": {
                    "input": "$array_enfermedades",
                    "as": "item",
                    "cond": { "$ne": ["$$item.sintoma", [""]] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_enfermedades": {
                "$filter": {
                    "input": "$array_enfermedades",
                    "as": "item",
                    "cond": { "$ne": ["$$item.cantidad", 0] }
                }
            }
        }
    }



    , {
        "$unwind": {
            "path": "$array_enfermedades",
            "preserveNullAndEmptyArrays": false
        }
    }
    , {

        "$unwind": {
            "path": "$array_enfermedades.sintoma",
            "preserveNullAndEmptyArrays": true
        }

    }




    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"

                , "enfermedad": "$array_enfermedades.enfermedad"
                , "sintoma": "$array_enfermedades.sintoma"

            }
            , "cantidad": { "$sum": "$array_enfermedades.cantidad" }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "cantidad_arboles": "$cantidad"
                    }
                ]
            }
        }
    }






]
