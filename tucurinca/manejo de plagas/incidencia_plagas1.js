db.form_manejodeplagas.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$PALMA"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote_num_arboles": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "PALMA": 0
            }
        }



        , {
            "$addFields": {

                "array_data": [

                    //lista
                    {
                        "plaga": "OPSIPHANES"
                        , "valor": {
                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$OPSIPHANES", []] },
                                        { "$eq": ["$OPSIPHANES", ""] }
                                    ]
                                },
                                "then": 0,
                                "else": 1
                            }
                        }
                    },

                    //numeros
                    {
                        "plaga": "DEMOTISPA"
                        , "valor": { "$ifNull": ["$DAÑOS POR DEMOTISPA", 0] }
                    },
                    {
                        "plaga": "LEPTOPHARSA GIBBICARINA"
                        , "valor": { "$ifNull": ["$HOJAS AFECTADAS POR  LEPTOPHARSA GIBBICARINA", 0] }
                    },
                    {
                        "plaga": "ACAROS"
                        , "valor": { "$ifNull": ["$ACAROS", 0] }
                    },
                    {
                        "plaga": "ESTRATEGUS MACHO"
                        , "valor": { "$ifNull": ["$ESTRATEGUS MACHO", 0] }
                    },
                    {
                        "plaga": "ESTRATEGUS HEMBRA"
                        , "valor": { "$ifNull": ["$ESTRATEGUS HEMBRA", 0] }
                    },
                    {
                        "plaga": "RYNCHOPHORUS MACHOS"
                        , "valor": { "$ifNull": ["$RYNCHOPHORUS MACHOS", 0] }
                    },
                    {
                        "plaga": "RYNCHOPHORUS HEMBRA"
                        , "valor": { "$ifNull": ["$RYNCHOPHORUS HEMBRA", 0] }
                    }


                ]

            }
        }


        , {
            "$project": {
                "OPSIPHANES": 0,
                "DAÑOS POR DEMOTISPA": 0,
                "HOJAS AFECTADAS POR  LEPTOPHARSA GIBBICARINA": 0,
                "ACAROS": 0,
                "ESTRATEGUS MACHO": 0,
                "ESTRATEGUS HEMBRA": 0,
                "RYNCHOPHORUS MACHOS": 0,
                "RYNCHOPHORUS HEMBRA": 0,
                "PRODUCTO APLICADO": 0,
                "uid": 0
            }
        }



        , { "$unwind": "$array_data" }




        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"

                    , "plaga": "$array_data.plaga"
                    , "lote_num_arboles": "$lote_num_arboles"

                }
                , "sum_valor_arbol": { "$sum": "$array_data.valor" }
                // , "data": { "$push": "$$ROOT" }
            }
        }


        , {
            "$addFields": {
                "tiene_plaga": {
                    "$cond": {
                        "if": {
                            "$gt": ["$sum_valor_arbol", 0]
                        },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"

                    , "plaga": "$_id.plaga"

                    , "lote_num_arboles": "$_id.lote_num_arboles"

                }
                , "plantas_dif_censadas_x_plaga_x_lote": { "$sum": "$tiene_plaga" }
                // , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_plaga_x_lote": "$plantas_dif_censadas_x_plaga_x_lote"
                        }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": {
                    "$cond": {
                        "if": { "$eq": ["$lote_num_arboles", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$plantas_dif_censadas_x_plaga_x_lote",
                                "$lote_num_arboles"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": {
                    "$multiply": ["$PORCENTAJE_INIDENCIA", 100]
                }
            }
        }

        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, 1] }] }, 100] }
            }
        }


    ]

)



/*
// collection: form_manejodeplagas
{
	"_id" : ObjectId("63f9754879747857b8644607"),
	"MANCHAS NECROTICAS POR DAÑOS" : "SI",
--"OPSIPHANES" : [
		"1ER INSTAR",
		"PUPA",
		"2DO INSTAR ",
		"ADULTO",
		"3ER INSTAR"
	],
--"DAÑOS POR DEMOTISPA" : 55,
	"METODO DE CONTROL" : "BIOLOGICO",
--"HOJAS AFECTADAS POR  LEPTOPHARSA GIBBICARINA" : 66,
	"CANTIDAD DE LARVAS X HOJA" : 77,
	"PRODUCTO APLICADO" : [
		"DETERGENTE",
		"FERTILIZANTE",
		"INSECTICIDA QUIMICO",
		"DRENCH",
		"INSECTICIDA BIOLOGICO"
	],
--"ACAROS" : 88,
"--ESTRATEGUS MACHO" : 99,
--"ESTRATEGUS HEMBRA" : 11,
--"RYNCHOPHORUS MACHOS" : 22,
--"RYNCHOPHORUS HEMBRA" : 33,
	"uid" : ObjectId("63c972b8853d26d60935fa29"),
	"supervisor" : "Support team",
	"rgDate" : ISODate("2023-02-24T21:41:12.131-05:00"),
	"uDate" : ISODate("2023-02-24T21:41:12.131-05:00"),
	"capture" : "W",
	"finca" : "TUCURINCA",
	"bloque" : "1",
	"lote" : "CHINO",
	"lote_num_arboles" : 1573,
	"linea" : "LOTE 7-35",
	"arbol" : "LOTE 7-35-11"
}
*/
