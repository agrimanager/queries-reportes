var data = db.tasks.aggregate(



    //los que eran de labor cosecha
    {
        $match: {
            _id: {
                $in: [
                    // ObjectId("63e397ce4c58faf3aacd68db"),
                    ObjectId("6437f097e5886c30be95d564"), //SI TIENE PRODUCTIVIDAD POR EMPLEADO Y CARTOGRAFIA
                    // ObjectId("6436a071f3dce5b664dfe3f7"),
                    // ObjectId("64354d753081f92cd2d22180"),
                    // ObjectId("6433f9b699fbe6806f1093d2"),
                    // ObjectId("63e398ad280a99431b2b332a"),

                ]
            }
        }
    }

    , {
        "$lookup": {
            "from": "employees",
            "localField": "productivityReport.employee",
            "foreignField": "_id",
            "as": "Empleado"
        }
    }

    , {
        $addFields: {
            "Empleado_form_cosecha": {
                "$map": {
                    "input": "$Empleado",
                    "as": "item_empleado",
                    "in": {
                        "_id": { $toString: "$$item_empleado._id" },
                        // "name": "05 - GUSTAVO ADOLFOCHÁVEZ CARDOSO",
                        "name": { $concat: ["$$item_empleado.code", " - ", "$$item_empleado.firstName", " ", "$$item_empleado.lastName"] },
                        "reference": "COSECHERO",
                        // "value": 66
                    }
                }
            }
        }
    }


    , {
        $addFields: {
            "Empleado_form_cosecha": {
                "$map": {
                    "input": "$Empleado_form_cosecha",
                    "as": "item_empleado_form_cosecha",
                    "in": {
                        "_id": "$$item_empleado_form_cosecha._id",
                        "name": "$$item_empleado_form_cosecha.name",
                        "reference": "$$item_empleado_form_cosecha.reference",
                        // "value": 66
                        "value": {
                            "$reduce": {
                                "input": "$productivityReport",
                                "initialValue": 0,
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ $toObjectId: "$$item_empleado_form_cosecha._id" }, "$$this.employee"] },
                                        "then": "$$this.quantity",
                                        "else": "$$value"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        $addFields: {

            item_insert: {
                // "_id": ObjectId("6448517cf47458dffb235690"),
                "LOTES": "$cartography",
                "Formula": "",
                "RACIMOS": "$productivityAchieved",//!!!!!ojo verificar suma
                "SACOS DE PEPA": 0,
                "MALLAS": 0,
                "RACIMOS PODRIDOS": 0,
                "Peso total racimos kg": 0,
                "Peso totalsacos de pepa": 0,
                "Point": {
                    "farm": { $toString: "$farm" },
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            -75.23012256231402,
                            10.16745434742569
                        ]
                    }
                },
                // "Cantidad de racimos por empleado": [
                //     {
                //         "_id": "63d608f0297c2560c78f249d",
                //         "name": "05 - GUSTAVO ADOLFOCHÁVEZ CARDOSO",
                //         "reference": "COSECHERO",
                //         "value": 66
                //     },
                //     {
                //         "_id": "63dc1c6690245b465b8365be",
                //         "name": "08 - LeonelMartinez Osorio",
                //         "reference": "COSECHERO",
                //         "value": 110
                //     }
                // ],
                "Cantidad de racimos por empleado": "$Empleado_form_cosecha",

                "NUMERO DE ENVIO": "",
                "Observaciones": "",
                "uid": ObjectId("63c972b8853d26d60935fa29"),
                "supervisor": "01 BENJAMIN OSORIO",
                "rgDate": "$rgDate",
                "uDate": "$uDate",
                "capture": "W"
            }

        }
    }


    , {
        $project: {
            item_insert: 1
        }
    }

)

// data



data.forEach(i => {

    db.form_cosecha.insert(
        i.item_insert

    )

})



/*
"cartography" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "63cabee6c4c40c390862e97e",
				"type" : "Feature",
				"path" : ",63caa258fe0795ed5344d3a9,63caa25de535fe9cd816b63f,",
				"properties" : {
					"type" : "lot",
					"name" : "CARLOS FERNANDEZ",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"color" : {
							"type" : "color",
							"value" : "#D76464"
						},
						"num_arboles" : {
							"type" : "number",
							"value" : 1112
						},
						"num_lineas" : {
							"type" : "number",
							"value" : 71
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-75.2334547576477,
								10.1725069359333
							],
							[
								-75.2314961422512,
								10.1712577050674
							],
							[
								-75.2310314620045,
								10.1712269370103
							],
							[
								-75.2304854909289,
								10.1708928043119
							],
							[
								-75.230152069286,
								10.1706159064383
							],
							[
								-75.2301002444899,
								10.1701898170466
							],
							[
								-75.229852841183,
								10.1700296206824
							],
							[
								-75.2298783845923,
								10.1699894176029
							],
							[
								-75.2298875565323,
								10.169977381354
							],
							[
								-75.2300182206278,
								10.1697792898492
							],
							[
								-75.2302103220911,
								10.1694151410301
							],
							[
								-75.2302346485773,
								10.1690595823108
							],
							[
								-75.2302647584567,
								10.1686909959149
							],
							[
								-75.230278346562,
								10.1683304492219
							],
							[
								-75.2302951516622,
								10.1679663207428
							],
							[
								-75.230327080668,
								10.1676063512711
							],
							[
								-75.2303121976801,
								10.1673683711038
							],
							[
								-75.2303137207541,
								10.1673482767195
							],
							[
								-75.2305034740756,
								10.1675920090205
							],
							[
								-75.2307818093027,
								10.1679469903678
							],
							[
								-75.2310468765159,
								10.1683139497913
							],
							[
								-75.2313425922807,
								10.1686913677071
							],
							[
								-75.231606954473,
								10.1690394824189
							],
							[
								-75.2318810041933,
								10.1693995847489
							],
							[
								-75.232151779358,
								10.1697546816037
							],
							[
								-75.2324340958571,
								10.1701171314696
							],
							[
								-75.2327144209871,
								10.1704812783582
							],
							[
								-75.2329722996845,
								10.1708244610449
							],
							[
								-75.2332624097613,
								10.1711944843793
							],
							[
								-75.2338833745426,
								10.1718419249393
							],
							[
								-75.2338969878811,
								10.1718547395359
							],
							[
								-75.2339119595031,
								10.1718951186644
							],
							[
								-75.2339121643815,
								10.1719368425394
							],
							[
								-75.2336224218859,
								10.1722983912205
							],
							[
								-75.2334547576477,
								10.1725069359333
							]
						]
					]
				}
			}
		],
		"path" : ",63caa258fe0795ed5344d3a9,63caa25de535fe9cd816b63f,"
	},


// collection: form_cosecha
{
	"_id" : ObjectId("6448517cf47458dffb235690"),
	"LOTES" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "63cabee7c4c40c390862e983",
				"type" : "Feature",
				"path" : ",63caa258fe0795ed5344d3a9,63caa25de535fe9cd816b63f,",
				"properties" : {
					"type" : "lot",
					"name" : "POZO",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"color" : {
							"type" : "color",
							"value" : "#D76464"
						},
						"num_arboles" : {
							"type" : "number",
							"value" : 1345
						},
						"num_lineas" : {
							"type" : "number",
							"value" : 52
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-75.2317271357426,
								10.1743753019409
							],
							[
								-75.2316265327075,
								10.1744710365389
							],
							[
								-75.2311986705736,
								10.1748311632254
							],
							[
								-75.2307875262423,
								10.175220255199
							],
							[
								-75.2302338093736,
								10.1755738189003
							],
							[
								-75.2297623969516,
								10.1759318918332
							],
							[
								-75.2296325894586,
								10.1760342960616
							],
							[
								-75.227202369596,
								10.1744834404693
							],
							[
								-75.2269220397499,
								10.1745001269606
							],
							[
								-75.2269287075929,
								10.1744898614858
							],
							[
								-75.2271617496768,
								10.1741269802402
							],
							[
								-75.2273974842536,
								10.1737644961811
							],
							[
								-75.2276336257273,
								10.1734011247354
							],
							[
								-75.2278827178024,
								10.1730261917973
							],
							[
								-75.2281128993378,
								10.1726803860202
							],
							[
								-75.2283508499332,
								10.1723130667816
							],
							[
								-75.2283928699333,
								10.1722493824579
							],
							[
								-75.2292779326746,
								10.172809145406
							],
							[
								-75.2295992538963,
								10.1730172766312
							],
							[
								-75.2298560164502,
								10.1731709265867
							],
							[
								-75.2307536064198,
								10.1737465899449
							],
							[
								-75.2317271357426,
								10.1743753019409
							]
						]
					]
				}
			}
		],
		"path" : ",63caa258fe0795ed5344d3a9,63caa25de535fe9cd816b63f,"
	},
	"Formula" : "",
	"RACIMOS" : 176,
	"SACOS DE PEPA" : 0,
	"MALLAS" : 0,
	"RACIMOS PODRIDOS" : 0,
	"Peso total racimos kg" : 0,
	"Peso totalsacos de pepa" : 0,
	"Point" : {
		"farm" : "63caa258fe0795ed5344d3a9",
		"type" : "Feature",
		"geometry" : {
        	"type" : "Point",
        	"coordinates" : [
        		-75.23012256231402,
        		10.16745434742569
        	]
        }
	},
	"Cantidad de racimos por empleado" : [
		{
			"_id" : "63d608f0297c2560c78f249d",
			"name" : "05 - GUSTAVO ADOLFOCHÁVEZ CARDOSO",
			"reference" : "CORTADOR",
			"value" : 66
		},
		{
			"_id" : "63dc1c6690245b465b8365be",
			"name" : "08 - LeonelMartinez Osorio",
			"reference" : "CORTADOR",
			"value" : 110
		}
	],
	"NUMERO DE ENVIO" : "",
	"Observaciones" : "",
	"uid" : ObjectId("63c972b8853d26d60935fa29"),
	"supervisor" : "01 BENJAMIN OSORIO",
	"rgDate" : ISODate("2023-04-25T17:15:18.000-05:00"),
	"uDate" : ISODate("2023-04-25T17:15:18.000-05:00"),
	"capture" : "M"
}



*/
