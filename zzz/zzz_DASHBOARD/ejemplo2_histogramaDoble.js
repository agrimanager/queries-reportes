db.form_enfermedadesriopaila.aggregate(
    [

        { "$unwind": "$Palma.features" },
        {
            "$group": {
                "_id": {
                    "Enfermedad": "$Enfermedad"
                    , "palma": "$Palma.features.properties.name"
                }
                , "cantidad1": { "$sum": 1 }
            }
        },


        //--valor cualitativo(texto) y cuantitativo(numero)
        {
            "$group": {
                "_id": "$_id.Enfermedad"
                , "cantidad1": { "$sum": "$cantidad1" }
                , "cantidad2": { "$sum": 1 }
            }
        }

        //--armar array de valores
        , {
            "$group": {
                "_id": null
                , "data_nombres": { "$push": "$_id" }
                , "data_numeros1": { "$push": "$cantidad1" }
                , "data_numeros2": { "$push": "$cantidad2" }
                //--colores
                // , "data_colores": { "$push": "red" }
            }
        }

        , { "$project": { "_id": 0 } }

        //==== PROYECCION FINAL
        , {
            "$project": {
                "data": {
                    //"labels": ["txt1", "txt2", "txt3", "txt4"],
                    "labels": "$data_nombres",//🚩
                    "datasets": [
                        //--variable1
                        {
                            "label": "data1_arboles",
                            "backgroundColor": "green",
                            "borderColor": "green",
                            "borderWidth": 1,
                            "data": "$data_numeros1"//🚩
                        },
                        //--variable2
                        {
                            "label": "data2_censos",
                            "backgroundColor": "blue",
                            "borderColor": "blue",
                            "borderWidth": 1,
                            "data": "$data_numeros2"//🚩
                        }
                    ]
                },
                "options": {
                    "title": {
                        "display": true,
                        "text": "Chart.js Bar Chart"
                    }
                }
            }
        }



    ]
)

/*

//-histograma horizontal
{
  "data": {
    "labels": [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July"
    ],
    "datasets": [
      //--variable1
      {
        "label": "Dataset 1",
        "backgroundColor": "green",
        "borderColor": "green",
        "borderWidth": 1,
        "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
      },
      //--variable2
      {
        "label": "Dataset 2",
        "backgroundColor": "blue",
        "borderColor": "blue",
        "borderWidth": 1,
        "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
      }
    ]
  },
  "options": {
    "title": {
      "display": true,
      "text": "Chart.js Bar Chart"
    }
  }
}

*/
