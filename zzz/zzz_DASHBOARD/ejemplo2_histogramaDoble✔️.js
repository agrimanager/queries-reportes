[

    { "$unwind": "$Palma.features" },
    {
        "$group": {
            "_id": {
                "Enfermedad": "$Enfermedad"
                , "palma": "$Palma.features.properties.name"
            }
            , "cantidad1": { "$sum": 1 }
        }
    },



    {
        "$group": {
            "_id": "$_id.Enfermedad"
            , "cantidad1": { "$sum": "$cantidad1" }
            , "cantidad2": { "$sum": 1 }
        }
    }


    , {
        "$group": {
            "_id": null
            , "data_nombres": { "$push": "$_id" }
            , "data_numeros1": { "$push": "$cantidad1" }
            , "data_numeros2": { "$push": "$cantidad2" }

        }
    }

    , { "$project": { "_id": 0 } }


    , {
        "$project": {
            "data": {
                "labels": "$data_nombres",
                "datasets": [

                    {
                        "label": "data1_arboles",
                        "backgroundColor": "green",
                        "borderColor": "green",
                        "borderWidth": 1,
                        "data": "$data_numeros1"
                    },

                    {
                        "label": "data2_censos",
                        "backgroundColor": "blue",
                        "borderColor": "blue",
                        "borderWidth": 1,
                        "data": "$data_numeros2"
                    }
                ]
            },
            "options": {
                "title": {
                    "display": true,
                    "text": "Chart.js Bar Chart"
                }
            }
        }
    }



]
