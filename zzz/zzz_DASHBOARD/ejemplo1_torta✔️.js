
[

    {
        "$group": {
            "_id": "$Enfermedad"
            , "cantidad": { "$sum": 1 }
        }
    }


    , {
        "$group": {
            "_id": null
            , "data_nombres": { "$push": "$_id" }
            , "data_numeros": { "$push": "$cantidad" }

            , "data_colores": { "$push": "red" }
        }
    }

    , { "$project": { "_id": 0 } }


    , {
        "$project": {
            "data": {
                "labels": "$data_nombres",
                "datasets": [
                    {
                        "label": "Titulo_123",
                        "data": "$data_numeros",
                        "backgroundColor": "$data_colores"
                    }
                ]
            }
        }
    }
]
