
//--ejemplo de torta
{
  "data": {
    "labels": ["green", "red", "orange", "yellow"],
    "datasets": [
      //--variable1
      {
        "label": "Dataset 1",
        "data": [57, 36, 800, 40],
        "backgroundColor": ["green", "red", "orange", "yellow"]
      }
    ]
  },
  "options": {}
},


//-histograma horizontal
{
  "data": {
    "labels": [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July"
    ],
    "datasets": [
      //--variable1
      {
        "label": "Dataset 1",
        "backgroundColor": "green",
        "borderColor": "green",
        "borderWidth": 1,
        "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
      },
      //--variable2
      {
        "label": "Dataset 2",
        "backgroundColor": "blue",
        "borderColor": "blue",
        "borderWidth": 1,
        "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
      }
    ]
  },
  "options": {
    "title": {
      "display": true,
      "text": "Chart.js Bar Chart"
    }
  }
}
