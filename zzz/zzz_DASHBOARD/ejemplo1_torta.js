db.form_enfermedadesriopaila.aggregate(
    [
        //--valor cualitativo(texto) y cuantitativo(numero)
        {
            "$group": {
                "_id": "$Enfermedad"
                , "cantidad": { "$sum": 1 }
            }
        }


        //--armar array de valores
        , {
            "$group": {
                "_id": null
                , "data_nombres": { "$push": "$_id" }
                , "data_numeros": { "$push": "$cantidad" }
                //--colores
                , "data_colores": { "$push": "red" }
            }
        }

        , { "$project": { "_id": 0 } }

        //==== PROYECCION FINAL
        , {
            "$project": {
                "data": {
                    //"labels": ["txt1", "txt2", "txt3", "txt4"],
                    "labels": "$data_nombres",//🚩
                    "datasets": [
                        {
                            "label": "Titulo_123",

                            //"data": [57, 36, 800, 40],
                            "data": "$data_numeros",//🚩

                            //"backgroundColor": ["green", "red", "orange", "yellow"]
                            "backgroundColor": "$data_colores"//🚩
                        }
                    ]
                },
                // "options": {}
            }
        }



    ]
)


/*
//--ejemplo de torta
{
  "data": {
    "labels": ["green", "red", "orange", "yellow"],
    "datasets": [
      {
        "label": "Dataset 1",
        "data": [57, 36, 800, 40],
        "backgroundColor": ["green", "red", "orange", "yellow"]
      }
    ]
  },
  "options": {}
}

*/
