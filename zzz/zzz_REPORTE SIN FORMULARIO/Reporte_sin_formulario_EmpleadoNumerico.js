//cluster
db.users.aggregate(
    [

        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_nominaagrojar",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //=============filtro fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                    { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                                ]
                            }
                        }
                    },

                    //=============finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca_point"
                        }
                    },
                    { "$unwind": "$finca_point" },
                    { "$addFields": { "finca_point": { "$ifNull": ["$finca_point.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                        }
                    }


                    //=============empleados
                    , {
                        "$match": {
                            "Colaborador": { "$ne": "" }
                        }
                    },

                    //---empleado numerico


                    //========EMPLEADOS
                    //----NOTA: tener en cuenta que eligen mas de [20] empleados  y tambien registros sin empleados = "" !!!!
                    {
                        "$addFields": {
                            "Empleados": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$Colaborador" }, "array"] },
                                    "then": "$Colaborador",
                                    "else": { "$map": { "input": { "$objectToArray": "$Colaborador" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                                }
                            }
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$Empleados",
                            // "includeArrayIndex": "arrayIndex",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    //--info empleado
                    , {
                        "$addFields": {
                            "empleado_oid": { "$toObjectId": "$Empleados._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_oid",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , {
                        "$addFields": {
                            "empleado seleccion": "$Empleados.name"

                            , "empleado nombre y apellidos": {
                                "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                            }

                            , "empleado codigo": "$info_empleado.code"
                            , "empleado cedula": "$info_empleado.numberID"
                        }
                    }

                    , {
                        "$project": {
                            "info_empleado": 0
                            , "Colaborador": 0
                            , "Empleados": 0

                            , "Nombre Empleado": 0
                            , "Point": 0
                            , "Formula": 0
                            , "uid": 0

                            , "empleado_oid": 0
                        }
                    }



                    //fecha
                    , {
                        "$addFields": {
                            "Fecha": {
                                "$dateToString": {
                                    "date": "$Fecha",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
