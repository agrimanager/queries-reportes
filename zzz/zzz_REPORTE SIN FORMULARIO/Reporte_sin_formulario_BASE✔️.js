
[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "XXXXXXXXXX___FORM",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [


              {
                  "$addFields": {
                      "point_farm_oid": { "$toObjectId": "$Point.farm" }
                  }
              },
              {
                  "$lookup": {
                      "from": "farms",
                      "localField": "point_farm_oid",
                      "foreignField": "_id",
                      "as": "finca"
                  }
              },
              { "$unwind": "$finca" },
              { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

              {
                  "$project": {
                      "Point": 0
                      , "point_farm_oid": 0
                      , "uid": 0
                  }
              }



            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
