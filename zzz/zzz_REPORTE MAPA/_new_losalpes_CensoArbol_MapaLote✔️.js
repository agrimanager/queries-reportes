[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": {
                "$split": [
                    {
                        "$trim": {
                            "input": "$variable_cartografia.path",
                            "chars": ","
                        }
                    },
                    ","
                ]
            }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": {
                "$map": {
                    "input": "$split_path_padres",
                    "as": "strid",
                    "in": { "$toObjectId": "$$strid" }
                }
            }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [
                { "$toObjectId": "$variable_cartografia.features._id" }
            ]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": "$split_path_oid" },
                            { "$size": "$objetos_del_cultivo" }
                        ]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "blocks"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] }
        }
    },
    {
        "$addFields": {
            "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "lot"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "lote": { "$ifNull": ["$lote.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "lines"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "linea": { "$ifNull": ["$linea.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "trees"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "cartography_id": { "$ifNull": ["$arbol._id", null] }
        }
    },

    {
        "$addFields": {
            "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] }
        }
    },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Arbol": 0,
            "Point": 0,
            "bloque_id": 0,
            "Formula": 0
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "enfermedad": "$Enfermedad"
            },
            "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "enfermedad": "$_id.enfermedad"
            },
            "count": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },
    { "$unwind": "$data.data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "numero_de_arboles_x_enfermedad": "$count" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "semana": { "$week": "$rgDate" },
            "año": { "$year": "$rgDate" }
        }
    },
    {
        "$addFields": {
            "semana_y_año": {
                "$concat": [
                    { "$toString": "$semana" },
                    "-",
                    { "$toString": "$año" }
                ]
            }
        }
    },
    {
        "$group": {
            "_id": "$semana_y_año",
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": null,
            "semanas_arr": { "$push": "$_id" },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$addFields": {
            "num_semanas": { "$size": "$semanas_arr" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "num_semanas": "$num_semanas" }

                ]
            }
        }
    },
    {
        "$addFields": {
            "const_num_arboles_muestra": { "$multiply": [34, "$num_semanas"] }
        }
    },



    {
        "$addFields": {
            "porc_incidencia_x_enfermedad": {
                "$cond": {
                    "if": { "$eq": ["$const_num_arboles_muestra", 0] },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$floor": {
                                    "$multiply": [


                                        {
                                            "$multiply": [
                                                {
                                                    "$divide": [
                                                        "$numero_de_arboles_x_enfermedad",
                                                        "$const_num_arboles_muestra"
                                                    ]
                                                },
                                                100
                                            ]
                                        },


                                        100
                                    ]
                                }
                            },
                            100
                        ]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "semana del año": "$semana",
            "numero de semanas": "$num_semanas",
            "numero arboles de muestra": "$const_num_arboles_muestra",
            "numero arboles por enfermedad": "$numero_de_arboles_x_enfermedad",
            "% incidencia por enfermedad": "$porc_incidencia_x_enfermedad"
        }
    },
    {
        "$project": {
            "semana_y_año": 0,
            "semana": 0,
            "num_semanas": 0,
            "numero_de_arboles_x_enfermedad": 0,
            "const_num_arboles_muestra": 0,
            "porc_incidencia_x_enfermedad": 0
        }
    }

    , {
        "$addFields": {
            "indicador": "$% incidencia por enfermedad"
        }
    }
    , {
        "$addFields": {
            "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
        }
    }


    , {
        "$addFields": {
            "elemento_agrupacion": "$Enfermedad"
        }
    }
    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
                , "arbol": "$arbol"

                , "idform": "$idform"
                , "elemento_agrupacion": "$elemento_agrupacion"
            }
            , "indicador": { "$min": "$indicador" }
            , "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

                , "idform": "$_id.idform"
                , "elemento_agrupacion": "$_id.elemento_agrupacion"
            }
            , "indicador": { "$min": "$indicador" }
            , "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "indicador": "$indicador"

                        , "cartography_id": {
                            "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.cartography_id", 0] }, 0]
                        }
                    }
                ]
            }
        }
    }


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "lote",
            "foreignField": "properties.name",
            "as": "info_lote"
        }
    }
    , { "$unwind": "$info_lote" }
    , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
    , {
        "$project": {
            "info_lote": 0
        }
    }

    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 0] }
                                    , { "$lt": ["$indicador", 20] }
                                ]
                            }
                            , "then": "#0000ff"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 20] }
                                    , { "$lt": ["$indicador", 40] }
                                ]
                            }
                            , "then": "#008000"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 40] }
                                    , { "$lt": ["$indicador", 60] }
                                ]
                            }
                            , "then": "#ffff00"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 60] }
                                    , { "$lt": ["$indicador", 80] }
                                ]
                            }
                            , "then": "#ffa500"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 80] }
                                ]
                            }
                            , "then": "#ff0000"
                        }

                    ],
                    "default": "#000000"
                }
            }

            , "rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 0] }
                                    , { "$lt": ["$indicador", 20] }
                                ]
                            }
                            , "then": "A-[ 0% - 20% )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 20] }
                                    , { "$lt": ["$indicador", 40] }
                                ]
                            }
                            , "then": "B-[ 20% - 40% )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 40] }
                                    , { "$lt": ["$indicador", 60] }
                                ]
                            }
                            , "then": "C-[ 40% - 60% )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 60] }
                                    , { "$lt": ["$indicador", 80] }
                                ]
                            }
                            , "then": "D-[ 60% - 80% )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 80] }
                                ]
                            }
                            , "then": "E-[ 80% - >100% )"
                        }

                    ],
                    "default": "F-otro"
                }
            }

        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": "$cartography_id"
            , "cartography_geometry": "$cartography_geometry"

            , "color": "$color"
            , "rango": "$rango"
        }
    }


    , {
        "$project": {

            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",


            "properties": {
                "Finca": "$finca",
                "Bloque": "$bloque",
                "Lote": "$lote",
                "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                "color": "$color"

                , "Enfermedad": "$elemento_agrupacion"
                , "% Incidencia": { "$concat": [{ "$toString": "$indicador" }, " %"] }
            }
        }
    }


]
