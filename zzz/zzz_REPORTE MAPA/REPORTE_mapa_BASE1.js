//---mapa
db.form_trampasderpalmarum.aggregate(
    [

        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------


        //------CARTOGRAFIA
        {
            "$addFields": { "Cartography": "$Trampa" }//---EDITAR
        },
        {"$unwind": "$Cartography.features"},
        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },


        //------COLOR
        {
            "$addFields": {
                "color":"#ff0000",
                "rango": "123"
            }
        },



        //-----PROYECCION FINAL
        {
            "$project": {
                "_id": "$elemnq",
                "idform": "$idorm",
                "type": "Feature",
                "properties": {
                    "color":"$color"

                    , "Semana": "$Semana"
                    , "supervisor": "$supervisor"
                    
                    ,"rango":"$rango"
                    ,"anio": {"$toString":{"$year":"$Busqueda inicio"}}
                },
                "geometry": "$Cartography.features.geometry"
            }
        }

        /*

                ,{
                    "$project": {
                        // "_id": {
                        //     "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                        // },
                        "idform": "$_id.idform",
                        "type": "Feature",
                        "properties": {
                            "Lote": "$_id.lote",
                            "Enfermedad": "$_id.plaga_enfermedad",
                            "Rango": "$rango_tratamiento_en_30_dias",

                            "color": "$color"
                        },
                        "geometry":
                        // {
                            //"$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
                            // "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                            "$_id.Cartography.geometry"
                        // }
                    }
                }
        */

    ]

)
