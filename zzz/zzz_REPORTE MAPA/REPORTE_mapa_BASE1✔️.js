[
 
    {
        "$addFields": { "Cartography": "$Trampa" }
    },
    {"$unwind": "$Cartography.features"},
    {
        "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
    },
    
    
    {
        "$addFields": {
            "color":"#ff0000",
            "rango": "123"
        }
    },

    
    
    {
        "$project": {
            "_id": "$elemnq",
            "idform": "$idorm",
            "type": "Feature",
            "properties": {
                "color":"$color"
                ,"rango":"$rango"
                ,"anio": {"$toString":{"$year":"$Busqueda inicio"}}
            },
            "geometry": "$Cartography.features.geometry"
        }
    }
]