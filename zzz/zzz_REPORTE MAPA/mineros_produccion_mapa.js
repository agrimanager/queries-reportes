[


    { "$addFields": { "variable_fecha": "$Fecha y hora Inicio" } },
    { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
    { "$addFields": { "mes": { "$month": "$variable_fecha" } } },

    { "$match": { "anio": { "$gt": 2000 } } },
    { "$match": { "anio": { "$lt": 3000 } } }

    , {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },





    {
        "$lookup": {
            "from": "form_ciclosmesesproductividad",
            "let": {
                "variable_fecha": "$variable_fecha"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$variable_fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio Ciclo" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$variable_fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Finalizacion Ciclo" } } }
                                    ]
                                },

                                {
                                    "$gte": [
                                        "$Finalizacion Ciclo",
                                        "$Inicio Ciclo"
                                    ]
                                }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                },
                {
                    "$project": {
                        "_id": 0,
                        "Mes": 1,
                        "Finalizacion Ciclo": 1
                    }
                }
            ],
            "as": "ciclo"
        }
    },

    {
        "$unwind": {
            "path": "$ciclo",
            "preserveNullAndEmptyArrays": false
        }
    },

    { "$addFields": { "anio": { "$year": "$ciclo.Finalizacion Ciclo" } } },

    {
        "$addFields": {
            "ciclo": "$ciclo.Mes"
        }
    },

    {
        "$addFields": {
            "ciclo": {
                "$ifNull": [
                    {
                        "$concat": [
                            { "$toUpper": { "$substrCP": ["$ciclo", 0, 1] } },
                            {
                                "$toLower": {
                                    "$substrCP": [
                                        "$ciclo",
                                        1,
                                        { "$subtract": [{ "$strLenCP": "$ciclo" }, 1] }
                                    ]
                                }
                            }
                        ]
                    },
                    {
                        "$concat": [
                            "Sin ciclo - ",
                            "$Mes_Txt"
                        ]
                    }
                ]
            }
        }
    },





    {
        "$addFields": {
            "variable_cartografia": "$Tarea latex"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },




    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "tarea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "samplingPolygons"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$tarea",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": { "Cartography": "$tarea" }
    },

    {

        "$addFields": { "elemnq": { "$toObjectId": "$Cartography._id" } }
    }

    , { "$addFields": { "tarea": { "$ifNull": ["$tarea.properties.name", "no existe"] } } },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Tarea latex": 0
            , "Point": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
        }
    }





    , {
        "$group": {
            "_id": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",

                "tarea_cartografia": "$tarea"

                , "anio": "$anio"
                , "mes": "$ciclo"
            }
            , "data": { "$push": "$$ROOT" }

            , "sum_kg_latex": { "$sum": { "$toDouble": "$Latex en Kg" } }
            , "num_rayadas": { "$sum": 1 }
        }
    }



    , {
        "$lookup": {
            "from": "form_inventariodecultivo",
            "as": "info_inventario_cultivo",
            "let": {
                "tarea_cartografia": "$_id.tarea_cartografia"
                , "anio": "$_id.anio"
                , "mes": "$_id.mes"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [

                                { "$ne": [{ "$type": "$Tarea.features.properties.name" }, "missing"] },
                                { "$in": ["$$tarea_cartografia", "$Tarea.features.properties.name"] },

                                { "$eq": ["$Mes", "$$mes"] }


                                , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                            ]
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_inventario_cultivo",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "num_arboles": { "$ifNull": ["$info_inventario_cultivo.Numero de Arboles", 0] }
        }
    }

    , {
        "$project": {
            "info_inventario_cultivo": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_informacionproyecciondeproduccion",
            "as": "info_ppto",
            "let": {
                "tarea_cartografia": "$_id.tarea_cartografia"
                , "anio": "$_id.anio"
                , "mes": "$_id.mes"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                , { "$eq": ["$Mes", "$$mes"] }


                                , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                            ]
                        }
                    }
                }

            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_ppto",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "ppto": { "$ifNull": ["$info_ppto.Produccion programada", 0] }
        }
    }

    , {
        "$project": {
            "info_ppto": 0
        }
    }


    , {
        "$addFields": {
            "produccion_rayadas_x_kg": {
                "$cond": {
                    "if": { "$eq": ["$num_rayadas", 0] },
                    "then": 0,
                    "else": { "$divide": ["$sum_kg_latex", "$num_rayadas"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "produccion_rayadas_x_kg": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, 1] }] }, 100] }
        }
    }

    , {
        "$addFields": {
            "produccion_rayadas_x_kg_x_arbol": {
                "$cond": {
                    "if": { "$eq": ["$num_arboles", 0] },
                    "then": 0,
                    "else": { "$divide": ["$produccion_rayadas_x_kg", "$num_arboles"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "produccion_rayadas_x_kg_x_arbol": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, 1] }] }, 100] }
        }
    }

    , {
        "$addFields": {
            "pct_cumplimiento": {
                "$cond": {
                    "if": { "$eq": ["$ppto", 0] },
                    "then": 0,
                    "else": { "$multiply": [{ "$divide": ["$sum_kg_latex", "$ppto"] }, 100] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "pct_cumplimiento": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_cumplimiento", 100] }, { "$mod": [{ "$multiply": ["$pct_cumplimiento", 100] }, 1] }] }, 100] }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$lt": ["$pct_cumplimiento", 74] },
                    "then": "#ff0000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_cumplimiento", 74] }, {
                                    "$lt": ["$pct_cumplimiento", 90]
                                }]
                            },
                            "then": "#ffff00",
                            "else": "#008000"
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$lt": ["$pct_cumplimiento", 74] },
                    "then": "A-[0% - 74%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_cumplimiento", 74] }, {
                                    "$lt": ["$pct_cumplimiento", 90]
                                }]
                            },
                            "then": "B-[74% - 90%)",
                            "else": "C-[>= 90%)"
                        }
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": {
                "$arrayElemAt": ["$data.idform", { "$subtract": [{ "$size": "$data.idform" }, 1] }]
            },
            "type": "Feature",
            "properties": {
                "Año": { "$toString": "$_id.anio" },
                "Mes": "$_id.mes",

                "Bloque": "$_id.bloque",
                "Lote": "$_id.lote",
                "Tarea": "$_id.tarea_cartografia",

                "Rango": "$rango",
                "PCT Cumplimiento": {
                    "$concat": [
                        { "$toString": "$pct_cumplimiento" },
                        " %"
                    ]
                },
                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
            }
        }
    }


]
