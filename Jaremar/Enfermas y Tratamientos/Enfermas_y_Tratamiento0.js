db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },


        {
            "$lookup": {
                "from": "form_Censodepc",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                //query
                "pipeline": [

                    {
                        "$addFields": {
                            "tiene_enfermedad": {
                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                                            { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                                            { "$eq": ["$Mordisco en hojas", "SI"] },
                                            { "$ne": ["$Severidad de la enfermedad", ""] }
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    },

                    {
                        "$match": {
                            "tiene_enfermedad": 1
                        }
                    },


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




    ]
)
