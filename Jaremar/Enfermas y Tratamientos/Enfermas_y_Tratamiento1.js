//reporte
db.form_Censodepc.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                // "Busqueda inicio": ISODate("2021-07-16T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-23T00:00:00.000-05:00"),
                // "Busqueda fin": new Date,

                //=================SAN ALEJO (fechas ciclos)

                //Finca: San Alejo
                //desde: ciclo 2
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-06-19T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-28T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 3
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-08-03T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 2
                //hasta: ciclo 4
                "Busqueda inicio": ISODate("2021-06-19T00:00:00.000-05:00"),
                "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-28T00:00:00.000-05:00"),

                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),



                //=================CAICESA (fechas ciclos)
                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 1
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-04-12T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-21T00:00:00.000-05:00"),

                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 3
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-07-16T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-22T00:00:00.000-05:00"),

                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 1
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-04-12T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-22T00:00:00.000-05:00"),

                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",
            }
        },

        //FILTRO FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            // "$eq": ["$Point.farm", "5fbe5f972033e75d57a736e6"]//Caicesa-Ponce-Pizzati
                            "$eq": ["$Point.farm", "5fac01ce246347247f068528"]//San Alejo

                        }
                    ]
                }
            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------



        //===== Enfermas
        {
            "$addFields": {
                "tiene_enfermedad": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                                { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                                { "$eq": ["$Mordisco en hojas", "SI"] },
                                { "$ne": ["$Severidad de la enfermedad", ""] }
                            ]
                        },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },

        {
            "$match": {
                "tiene_enfermedad": 1
            }
        },



        //===== Cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },


        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol_id": { "$ifNull": ["$arbol._id", null] } } },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "info_lote": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,
                "Formula": 0


                , "capture": 0
                , "tiene_enfermedad": 0

                //OTROS
                , "ESTADO DE LA RECUPERACION": 0
                , "OBSERVACIONES EN PALMA CON PC": 0
                , "Es PC Nuevo": 0

                , "Secamiento de foliolo rudimentario": 0
                , "Amarillamiento de hojas jovenes": 0
                , "Mordisco en hojas": 0
                , "Severidad de la enfermedad": 0
                , "Ha sido atacada por R PALMARUM": 0


            }
        },



        //===== Agrupacion Arbol (enfermo)
        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "lote": "$lote",
                    "arbol": "$arbol"

                }
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", -1] }
            }
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {}
                    ]
                }
            }
        },

        {
            "$addFields": {
                "arbol enfermo": "SI"
            }
        },




        //===== Fecha Siembra Lote_Finca
        {
            "$lookup": {
                "from": "form_puentefincalotefechasiembra",
                "as": "data_lote",
                "let": {
                    "finca": "$finca",
                    "lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$Finca"] },
                                    { "$eq": ["$$lote", "$Lote"] }
                                ]
                            }
                        }
                    }

                    , { "$limit": 1 }
                ]
            }
        },

        {
            "$unwind": {
                "path": "$data_lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "lote_anio_siembra": { "$ifNull": ["$data_lote.Anio Siembra", 0] }
            }
        },
        {
            "$addFields": {
                "lote_anio_siembra": { "$toDouble": "$lote_anio_siembra" }
            }
        },

        {
            "$project": {
                "data_lote": 0
            }
        },



        //===== Edad Lote
        {
            "$addFields": {
                "num_anio_actual": { "$year": { "date": "$today" } }
            }
        },

        {
            "$addFields": {
                "lote_edad": {
                    "$subtract": ["$num_anio_actual", "$lote_anio_siembra"]
                }
            }
        },



        //===== CONDICION Edad Lote
        {
            "$match": {
                "lote_edad": { "$lte": 17 }
            }
        },



        //===== Tratadas
        {
            "$lookup": {
                "from": "form_tratamientodelapc",
                "let": {
                    "arbol_id": "$arbol_id"
                    // , "fecha_censo": "$rgDate"
                },
                "pipeline": [

                    { "$unwind": "$Palmas.features" },

                    {
                        "$addFields": {
                            "palma_oid": { "$toObjectId": "$Palmas.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$palma_oid", "$$arbol_id"] }
                                ]
                            }
                        }
                    }


                    // ,{
                    //     "$match": {
                    //         "$expr": {
                    //             "$gte": ["$rgDate", "$$fecha_censo"]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$sort": {
                    //         "rgDate": 1
                    //     }
                    // }
                    , {
                        "$limit": 1
                    }

                    , {
                        "$addFields": {
                            "arbol tratado": "SI"
                        }
                    }

                    , {
                        "$project": {
                            "arbol tratado": 1
                        }
                    }
                ],
                "as": "data_tratamiento"
            }
        },


        {
            "$unwind": {
                "path": "$data_tratamiento",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "arbol tratado": { "$ifNull": ["$data_tratamiento.arbol tratado", "NO"] }
            }
        },
        {
            "$project": {
                "data_tratamiento": 0
                , "arbol_id": 0
            }
        }





    ]
    , { allowDiskUse: true }
)
