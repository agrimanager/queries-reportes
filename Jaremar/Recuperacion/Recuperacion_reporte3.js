[

    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },

    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },

    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Palma": 0,
            "Point": 0,
            "uid": 0,
            "uDate": 0,
            "Formula": 0
        }
    },


    {
        "$addFields": {
            "tiene_enfermedad": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                            { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                            { "$eq": ["$Mordisco en hojas", "SI"] },
                            { "$ne": ["$Severidad de la enfermedad", ""] }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },



    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol"

                ,"tiene_enfermedad":"$tiene_enfermedad"
            }

            , "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            }
            , "numero_palmas_enfermas": { "$sum": "$_id.tiene_enfermedad" }
            , "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },
    { "$unwind": "$data.data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "numero_palmas_enfermas": "$numero_palmas_enfermas" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "esta_recuperada": {
                "$cond": {
                    "if": {
                        "$in": [
                            "$ESTADO DE LA RECUPERACION",
                            [
                                "Emision sana",
                                "Recuperacion vegetativa",
                                "Recuperacion productiva"
                            ]
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },



    { "$match": { "esta_recuperada": 1 } },



    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "estado_recuperacion": "$ESTADO DE LA RECUPERACION"
            },
            "count": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    { "palmas_segun_estado_recupercacion": "$count" }
                ]
            }
        }
    },



    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            },
            "count": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    { "palmas_recuperadas": "$count" }
                ]
            }
        }
    },


    {
        "$addFields": {
            "ESTADO DE LA RECUPERACION": {
                "$cond": {
                    "if": { "$eq": ["$ESTADO DE LA RECUPERACION", ""] },
                    "then": "sin estado",
                    "else": "$ESTADO DE LA RECUPERACION"
                }
            },
            "# plantas enfermas por lote": "$numero_palmas_enfermas",
            "# plantas segun estado de recuperacion del lote": "$palmas_segun_estado_recupercacion",
            "# plantas recuperadas del lote (Total)": "$palmas_recuperadas",

            "% segun estado de recuperacion del lote": {
                "$cond": {
                    "if": { "$gt": ["$numero_palmas_enfermas", 0] },
                    "then": {
                        "$divide": [
                            {
                                "$floor": {
                                    "$multiply": [
                                        { "$divide": [{ "$multiply": ["$palmas_segun_estado_recupercacion", 100] }, "$numero_palmas_enfermas"] }
                                    , 100]
                                }
                            }
                        , 100]
                    },
                    "else": 0
                }
            },

            "% recuperadas del lote (Total)": {
                "$cond": {
                    "if": { "$gt": ["$numero_palmas_enfermas", 0] },
                    "then": {
                        "$divide": [
                            {
                                "$floor": {
                                    "$multiply": [
                                        { "$divide": [{ "$multiply": ["$palmas_recuperadas", 100] }, "$numero_palmas_enfermas"] }
                                    , 100]
                                }
                            }
                        , 100]
                    },
                    "else": 0
                }
            }
        }
    },



    {
        "$addFields": {
            "% recuperadas segun estado por arbol": {
                "$cond": {
                    "if": { "$gt": ["$# plantas segun estado de recuperacion del lote", 0] },
                    "then": {
                        "$divide": ["$% segun estado de recuperacion del lote", "$# plantas segun estado de recuperacion del lote"]
                    },
                    "else": 0
                }
            }
        }
    },



    {
        "$project": {
            "tiene_enfermedad": 0,
            "numero_palmas_enfermas": 0,
            "esta_recuperada": 0,
            "palmas_segun_estado_recupercacion": 0,
            "palmas_recuperadas": 0
        }
    }
]
