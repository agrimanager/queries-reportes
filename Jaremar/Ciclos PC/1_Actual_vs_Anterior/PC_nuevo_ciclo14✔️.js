[


    {
        "$addFields": {
            "tiene_enfermedad": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                            { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                            { "$eq": ["$Mordisco en hojas", "SI"] },
                            { "$ne": ["$Severidad de la enfermedad", ""] }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },

    {
        "$match": {
            "tiene_enfermedad": 1
        }
    },


    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },

    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },


    { "$addFields": { "finca_id": { "$arrayElemAt": ["$split_path_padres_oid", 0] } } },


    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },

    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },



    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },



    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "poligono adicional": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$poligono adicional",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "poligono adicional": { "$ifNull": ["$poligono adicional.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "info_lote": 0,
            "Palma": 0,
            "Point": 0,
            "uid": 0,
            "uDate": 0,
            "Formula": 0

            , "capture": 0
            , "tiene_enfermedad": 0

            , "ESTADO DE LA RECUPERACION": 0
            , "OBSERVACIONES EN PALMA CON PC": 0
            , "Es PC Nuevo": 0
        }
    },

    {
        "$lookup": {
            "from": "form_ciclos",
            "as": "data_ciclos",
            "let": {
                "fecha_censo": "$rgDate"
                , "today": "$today"
                , "finca_id": "$finca_id"
            },
            "pipeline": [

                { "$addFields": { "finca": { "$toObjectId": "$Point.farm" } } },
                { "$match": { "$expr": { "$eq": ["$finca", "$$finca_id"] } } },

                { "$addFields": { "anio_fecha": { "$year": "$Fin de ciclo" } } },

                {
                    "$addFields": {
                        "Fin de ciclo": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "$Fin de ciclo",
                                "else": "$$today"
                            }
                        },
                        "ciclo_tiene_fin": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "SI",
                                "else": "NO"
                            }
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                        ,
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio de ciclo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                    ]
                                },

                                {
                                    "$lte": [
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                        ,
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fin de ciclo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$data_ciclos",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "ciclo num": { "$ifNull": [{ "$toDouble": "$data_ciclos.Ciclo" }, -1] }
            , "ciclo fecha inicio": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Inicio de ciclo", "timezone": "America/Managua" } }
            , "ciclo fecha fin": {
                "$cond": {
                    "if": { "$eq": ["$data_ciclos.ciclo_tiene_fin", "SI"] },
                    "then": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Fin de ciclo", "timezone": "America/Managua" } },
                    "else": "ciclo sin fecha fin"
                }

            }
        }
    },

    {
        "$project": {
            "data_ciclos": 0
        }
    },



    {
        "$lookup": {
            "from": "form_ciclos",
            "as": "data_ciclos_filtros",
            "let": {
                "today": "$today"
                , "busqueda_inicio": "$Busqueda inicio"
                , "busqueda_fin": "$Busqueda fin"
                , "finca_id": "$finca_id"
            },
            "pipeline": [

                { "$addFields": { "finca": { "$toObjectId": "$Point.farm" } } },
                { "$match": { "$expr": { "$eq": ["$finca", "$$finca_id"] } } },

                { "$addFields": { "anio_fecha": { "$year": "$Fin de ciclo" } } },

                {
                    "$addFields": {
                        "Fin de ciclo": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "$Fin de ciclo",
                                "else": "$$today"
                            }
                        },
                        "ciclo_tiene_fin": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "SI",
                                "else": "NO"
                            }
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [

                                {
                                    "$gte": [
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio de ciclo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                        ,
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$busqueda_inicio", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                }


                , { "$sort": { "Ciclo": 1 } }
                , { "$limit": 1 }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$data_ciclos_filtros",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "ciclo_filtro_inicio": { "$ifNull": [{ "$toDouble": "$data_ciclos_filtros.Ciclo" }, -1] }
        }
    },

    {
        "$project": {
            "data_ciclos_filtros": 0
        }
    },

    {
        "$lookup": {
            "from": "form_ciclos",
            "as": "data_ciclos_filtros",
            "let": {
                "today": "$today"
                , "busqueda_inicio": "$Busqueda inicio"
                , "busqueda_fin": "$Busqueda fin"
                , "finca_id": "$finca_id"
            },
            "pipeline": [

                { "$addFields": { "finca": { "$toObjectId": "$Point.farm" } } },
                { "$match": { "$expr": { "$eq": ["$finca", "$$finca_id"] } } },

                { "$addFields": { "anio_fecha": { "$year": "$Fin de ciclo" } } },

                {
                    "$addFields": {
                        "Fin de ciclo": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "$Fin de ciclo",
                                "else": "$$today"
                            }
                        },
                        "ciclo_tiene_fin": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "SI",
                                "else": "NO"
                            }
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$lte": [
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$busqueda_fin", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                        ,
                                        {
                                            "$dateFromString": {
                                                "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fin de ciclo", "timezone": "America/Managua" } }
                                                , "timezone": "America/Managua"
                                            }
                                        }
                                    ]
                                }

                            ]
                        }
                    }
                }


                , { "$sort": { "Ciclo": 1 } }
                , { "$limit": 1 }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$data_ciclos_filtros",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "ciclo_filtro_fin": { "$ifNull": [{ "$toDouble": "$data_ciclos_filtros.Ciclo" }, -1] }
        }
    },

    {
        "$project": {
            "data_ciclos_filtros": 0
        }
    },




    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "poligono adicional": "$poligono adicional",

                "arbol": "$arbol"

                , "ciclo": "$ciclo num"

                , "ciclo_filtro_inicio": "$ciclo_filtro_inicio"
                , "ciclo_filtro_fin": "$ciclo_filtro_fin"

            }

        }
    },



    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
                , "poligono adicional": "$_id.poligono adicional"

                , "arbol": "$_id.arbol"

                , "ciclo_filtro_inicio": "$_id.ciclo_filtro_inicio"
                , "ciclo_filtro_fin": "$_id.ciclo_filtro_fin"


            }

            , "data": { "$push": "$$ROOT" }
        }
    },


    {
        "$addFields": {
            "array_entre_num_ciclos": {
                "$range": [
                    "$_id.ciclo_filtro_fin",
                    { "$subtract": ["$_id.ciclo_filtro_inicio", 1] },
                    -1
                ]
            }
        }
    },

    {
        "$addFields": {
            "ciclo_actual_vs_anterior": {
                "$map": {
                    "input": "$array_entre_num_ciclos"
                    , "as": "ciclo_i"
                    , "in": {
                        "$reduce": {
                            "input": ["$$ciclo_i"],

                            "initialValue": {
                                "ciclo_actual": 0
                                , "ciclo_anterior": 0
                                , "ciclo_actual_data": []
                                , "ciclo_anterior_data": []
                            },

                            "in": {
                                "$cond": {
                                    "if": { "$ne": ["$$this", "$_id.ciclo_filtro_inicio"] },
                                    "then": {
                                        "ciclo_actual": "$$this"
                                        , "ciclo_anterior": { "$subtract": ["$$this", 1] }

                                        , "ciclo_actual_data": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [
                                                        {
                                                            "$filter": {
                                                                "input": "$data",
                                                                "as": "item_data",
                                                                "cond": { "$eq": ["$$item_data._id.ciclo", "$$this"] }
                                                            }
                                                        }
                                                        ,
                                                        []
                                                    ]
                                                }
                                                , "then": 0
                                                , "else": 1
                                            }
                                        }
                                        , "ciclo_anterior_data": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [
                                                        {
                                                            "$filter": {
                                                                "input": "$data",
                                                                "as": "item_data",
                                                                "cond": { "$eq": ["$$item_data._id.ciclo", { "$subtract": ["$$this", 1] }] }
                                                            }
                                                        }
                                                        ,
                                                        []
                                                    ]
                                                }
                                                , "then": 0
                                                , "else": 1
                                            }
                                        }

                                    },
                                    "else": {
                                        "ciclo_actual": null
                                        , "ciclo_anterior": null
                                        , "ciclo_actual_data": []
                                        , "ciclo_anterior_data": []
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "ciclo_actual_vs_anterior": {
                "$filter": {
                    "input": "$ciclo_actual_vs_anterior",
                    "as": "item",
                    "cond": { "$ne": ["$$item.ciclo_actual", null] }
                }
            }
        }
    },


    {
        "$project": {
            "data": 0
            , "array_entre_num_ciclos": 0
        }
    },

    { "$unwind": "$ciclo_actual_vs_anterior" },

    {
        "$addFields": {
            "Comparacion Ciclos": {
                "$concat": [
                    "Ciclo "
                    , { "$toString": "$ciclo_actual_vs_anterior.ciclo_actual" }

                    , " VS "

                    , "Ciclo "
                    , { "$toString": "$ciclo_actual_vs_anterior.ciclo_anterior" }
                ]
            }
        }
    },

    {
        "$addFields": {
            "condicion_arbol_ciclo": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$eq": ["$ciclo_actual_vs_anterior.ciclo_actual_data", 0] }
                            , { "$eq": ["$ciclo_actual_vs_anterior.ciclo_anterior_data", 1] }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }

            }
        }
    },


    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",

                    {
                        "ciclo_actual": "$ciclo_actual_vs_anterior.ciclo_actual",
                        "ciclo_anterior": "$ciclo_actual_vs_anterior.ciclo_anterior",
                        "ciclo_actual_data": "$ciclo_actual_vs_anterior.ciclo_actual_data",
                        "ciclo_anterior_data": "$ciclo_actual_vs_anterior.ciclo_anterior_data"
                    }

                    , { "Comparacion Ciclos": "$Comparacion Ciclos" }
                    , { "condicion_arbol_ciclo": "$condicion_arbol_ciclo" }
                ]
            }
        }
    }

]
