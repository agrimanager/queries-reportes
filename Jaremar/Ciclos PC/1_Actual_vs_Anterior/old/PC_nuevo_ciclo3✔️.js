[

    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },

    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },

    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },



    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },



    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "poligono adicional": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$poligono adicional",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "poligono adicional": { "$ifNull": ["$poligono adicional.properties.name", "no existe"] } } },



    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "info_lote": 0,
            "Palma": 0,
            "Point": 0,
            "uid": 0,
            "uDate": 0,
            "Formula": 0
        }
    },


    {
        "$addFields": {
            "tiene_enfermedad": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                            { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                            { "$eq": ["$Mordisco en hojas", "SI"] },
                            { "$ne": ["$Severidad de la enfermedad", ""] }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },

    {
        "$match": {
            "tiene_enfermedad": 1
        }
    },


    {
        "$lookup": {
            "from": "form_ciclos",
            "as": "data_ciclos",
            "let": {
                "fecha_censo": "$rgDate"
                , "today": "$today"
            },
            "pipeline": [

                { "$addFields": { "anio_fecha": { "$year": "$Fin de ciclo" } } },

                {
                    "$addFields": {
                        "Fin de ciclo": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "$Fin de ciclo",
                                "else": "$$today"
                            }
                        },
                        "ciclo_tiene_fin": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gt": ["$anio_fecha", 2000] },
                                        { "$lt": ["$anio_fecha", 3000] }
                                    ]
                                },
                                "then": "SI",
                                "else": "NO"
                            }
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio de ciclo", "timezone": "America/Managua" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fin de ciclo", "timezone": "America/Managua" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$data_ciclos",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "ciclo num": { "$ifNull": [{ "$toString": "$data_ciclos.Ciclo" }, "sin ciclo"] }
            , "ciclo fecha inicio": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Inicio de ciclo", "timezone": "America/Managua" } }
            , "ciclo fecha fin": {
                "$cond": {
                    "if": { "$eq": ["$data_ciclos.ciclo_tiene_fin", "SI"] },
                    "then": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Fin de ciclo", "timezone": "America/Managua" } },
                    "else": "ciclo sin fecha fin"
                }

            }
        }
    },

    {
        "$project": {
            "data_ciclos": 0
        }
    },



    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol"

                , "ciclo": "$ciclo num"

            }
            , "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
                , "linea": "$_id.linea"
                , "arbol": "$_id.arbol"


            }
            , "max_ciclo": { "$max": { "$toDouble": "$_id.ciclo" } }
            , "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca"
            }
            , "max_ciclo_total": { "$max": { "$toDouble": "$max_ciclo" } }
            , "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    { "max_ciclo_total": "$max_ciclo_total" }
                ]
            }
        }
    },


    {
        "$addFields": {
            "condicion_ciclo_anterior": {
                "$filter": {
                    "input": "$data",
                    "as": "item_data",
                    "cond": {
                        "$eq": [
                            { "$toDouble": "$$item_data._id.ciclo" }
                            , { "$subtract": ["$max_ciclo", 1] }
                        ]
                    }
                }
            }
        }
    },
    {
        "$match": {
            "condicion_ciclo_anterior": []
        }
    },

    {
        "$addFields": {
            "condicion_ciclo_anterior2": {
                "$filter": {
                    "input": "$data",
                    "as": "item_data",
                    "cond": {
                        "$eq": [
                            { "$toDouble": "$$item_data._id.ciclo" }
                            , { "$subtract": ["$max_ciclo_total", 0] }
                        ]
                    }
                }
            }
        }
    },
    {
        "$match": {
            "condicion_ciclo_anterior2": []
        }
    },



    { "$unwind": "$data" },


    {
        "$addFields": {
            "data_max": {
                "$arrayElemAt": ["$data.data", -1]
            }
        }
    },



    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    { "max_ciclo": "$max_ciclo" }
                    , { "max_ciclo_total": "$max_ciclo_total" }
                    , { "ultima_fecha_censo": "$data_max.rgDate" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "Comparacion Ciclos": {
                "$concat": [
                    "Ciclo "
                    , { "$toString": "$max_ciclo_total" }
                    , " VS "
                    , "Ciclo "
                    , { "$toString": { "$subtract": ["$max_ciclo_total", 1] } }
                ]
            },
            "ultima_fecha_censo": { "$dateToString": { "format": "%Y-%m-%d", "date": "$ultima_fecha_censo", "timezone": "America/Managua" } }
        }
    },

    {
        "$addFields": {
            "ultimo_ciclo_filtro": "$max_ciclo_total",
            "penultimo_ciclo_filtro": { "$subtract": ["$max_ciclo_total", 1] },
            "ciclo_ultimo_censo": "$max_ciclo"
        }
    },



    {
        "$project": {
            "max_ciclo": 0,
            "max_ciclo_total": 0
        }
    }
]
