[

    { "$limit": 1 },

    {
        "$project": {

            "datos": [

                {
                    "FincaID": "$FincaID"
                }
            ]
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }

    , {
        "$lookup": {
            "from": "form_Censodepc",
            "as": "data_cartography",
            "let": {

                "finca_id": "$FincaID"
            },

            "pipeline": [
                {
                    "$addFields": {
                        "variable_cartografia": "$Palma"
                    }
                },

                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },




                {
                    "$addFields": {
                        "finca_id_strng": {
                            "$arrayElemAt": ["$split_path_padres", 0]
                        }
                    }
                },
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": [
                                        { "$toString": "$$finca_id" }
                                        , { "$toString": "$finca_id_strng" }
                                    ]
                                }
                            ]
                        }
                    }
                },




                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },

                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },

                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



                {
                    "$addFields": {
                        "poligono adicional": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$poligono adicional",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "poligono adicional": { "$ifNull": ["$poligono adicional.properties.name", "no existe"] } } },



                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },

                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },

                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0,
                        "Palma": 0,
                        "Point": 0,
                        "bloque_id": 0
                    }
                },


                {
                    "$addFields": {
                        "fecha_inicio_ciclo": { "$toDate": "2021-06-19T00:00:00.000-04:00" },
                        "fecha_fin_ciclo": { "$toDate": "2021-08-03T00:00:01.000-04:00" },
                        "secciones_adicionales": ["3-13D-82", "3-13D-86", "3-10D-53"]
                    }
                },


                {
                    "$addFields": {
                        "pc_new": {
                            "$cond": {
                                "if": {
                                    "$or": [
                                        {
                                            "$and": [
                                                { "$gte": ["$rgDate", "$fecha_inicio_ciclo"] },
                                                { "$lt": ["$rgDate", "$fecha_fin_ciclo"] }
                                            ]
                                        },
                                        {
                                            "$and": [
                                                { "$lt": ["$rgDate", { "$toDate": "2021-08-07T00:00:00.000-04:00" }] },
                                                { "$gte": ["$rgDate", "$fecha_inicio_ciclo"] },
                                                { "$in": ["$poligono adicional", "$secciones_adicionales"] }
                                            ]
                                        }
                                    ]

                                },
                                "then": true,
                                "else": false
                            }
                        }
                    }
                },

                {
                    "$match": {
                        "pc_new": true
                    }
                },

                {
                    "$addFields": {
                        "fecha": {
                            "$dateToString": {
                                "date": "$rgDate",
                                "format": "%d-%m-%Y"
                            }
                        }
                    }
                },

                {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "poligono adicional": "$poligono adicional",
                            "linea": "$linea",
                            "arbol": "$arbol",
                            "fecha": "$fecha"
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_cartography",
            "preserveNullAndEmptyArrays": true
        }
    },



    {
        "$project": {
            "finca": "$data_cartography._id.finca",
            "bloque": "$data_cartography._id.bloque",
            "lote": "$data_cartography._id.lote",
            "poligono adicional": "$data_cartography._id.poligono adicional",
            "linea": "$data_cartography._id.linea",
            "arbol": "$data_cartography._id.arbol",
            "fecha": "$data_cartography._id.fecha"
        }
    }
]
