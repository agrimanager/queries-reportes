
[
    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$lookup": {
            "from": "cartography_polygon",
            "localField": "lote._id",
            "foreignField": "_id",
            "as": "info_lote"
        }
    },
    {
        "$addFields": {
            "info_lote": "$info_lote.internal_features"
        }
    },
    { "$unwind": "$info_lote" },

    {
        "$addFields": {
            "total_plantas_x_lote": {
                "$filter": {
                    "input": "$info_lote",
                    "as": "item",
                    "cond": { "$eq": ["$$item.type_features", "trees"] }
                }
            }
        }
    },
    { "$unwind": "$total_plantas_x_lote" },
    {
        "$addFields": {
            "total_plantas_x_lote": "$total_plantas_x_lote.count_features"
        }
    },

    {
        "$addFields": { "Cartography": "$lote" }
    },

    {
        "$addFields": { "elemnq": { "$toObjectId": "$Cartography._id" } }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },






    {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartography",
            "let": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            },
            "pipeline": [

                {
                    "$addFields": {
                        "variable_custom": "$properties.custom.Eliminada"
                    }
                },

                {
                    "$match": {
                        "variable_custom": { "$exists": true },
                        "variable_custom.value": { "$ne": false }
                    }
                },



                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },

                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },

                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },



                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0
                    }
                },



                {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote"
                        }
                        , "custom_x_lote_cantidad": { "$sum": 1 }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$finca", "$_id.finca"] },
                                { "$eq": ["$$bloque", "$_id.bloque"] },
                                { "$eq": ["$$lote", "$_id.lote"] }
                            ]
                        }
                    }
                }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$data_cartography",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "plantas_eliminadas_x_lote": { "$ifNull": ["$data_cartography.custom_x_lote_cantidad", 0] }
        }
    },

    {
        "$project": {
            "data_cartography": 0
        }
    },



    {
        "$addFields": {
            "plantas_x_lote": {
                "$subtract": ["$total_plantas_x_lote", "$plantas_eliminadas_x_lote"]
            }
        }
    },







    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "info_lote": 0,
            "Palma": 0,
            "Point": 0,
            "uid": 0,
            "uDate": 0,
            "Formula": 0
        }
    },





    {
        "$addFields": {
            "tiene_enfermedad": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                            { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                            { "$eq": ["$Mordisco en hojas", "SI"] },
                            { "$ne": ["$Severidad de la enfermedad", ""] }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },

    {
        "$match": {
            "tiene_enfermedad": 1
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol"

            }
            , "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

            }
            , "numero_palmas_enfermas": { "$sum": 1 }
            , "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },
    { "$unwind": "$data.data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "numero_palmas_enfermas": "$numero_palmas_enfermas" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "# plantas por lote": "$plantas_x_lote",
            "# plantas enfermas por lote": "$numero_palmas_enfermas",

            "% incidencia de plantas enfermas": {
                "$cond": {
                    "if": { "$gt": ["$plantas_x_lote", 0] },
                    "then": {
                        "$divide": [
                            {
                                "$floor": {
                                    "$multiply": [
                                        { "$divide": [{ "$multiply": ["$numero_palmas_enfermas", 100] }, "$plantas_x_lote"] }
                                        , 100]
                                }
                            }
                            , 100]
                    },
                    "else": 0
                }
            }
        }
    },



    {
        "$addFields": {
            "rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$% incidencia de plantas enfermas", 0] },
                                    { "$lte": ["$% incidencia de plantas enfermas", 5] }
                                ]
                            },
                            "then": "LEVE"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gt": ["$% incidencia de plantas enfermas", 5] },
                                    { "$lte": ["$% incidencia de plantas enfermas", 25] }
                                ]
                            },
                            "then": "MEDIO"
                        },
                        {
                            "case": {
                                "$gt": ["$% incidencia de plantas enfermas", 25]
                            },
                            "then": "SEVERO"
                        }
                    ],
                    "default": null
                }
            }
        }
    },

    {
        "$match": {
            "rango": { "$ne": null }
        }
    },

    {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$rango", "LEVE"] },
                            "then": "#0833a2"
                        },
                        {
                            "case": { "$eq": ["$rango", "MEDIO"] },
                            "then": "#ffff00"
                        },
                        {
                            "case": { "$eq": ["$rango", "SEVERO"] },
                            "then": "#ff0000"
                        }
                    ],
                    "default": null
                }
            }
        }
    },

    {
        "$group": {
            "_id": {
                "elemnq": "$elemnq",
                "idform": "$idform",
                "bloque": "$bloque",
                "lote": "$lote",
                "% incidencia de plantas enfermas": "$% incidencia de plantas enfermas",
                "rango": "$rango",
                "color": "$color",
                "Cartography": "$Cartography"
            }
        }
    },



    {
        "$project": {
            "_id": "$_id.elemnq",
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Bloque": "$_id.bloque",
                "Lote": "$_id.lote",
                "% incidencia de plantas enfermas": {
                    "$concat": [{ "$toString": "$_id.% incidencia de plantas enfermas" }, "%"]
                },
                "rango": "$_id.rango",
                "color": "$_id.color"
            },
            "geometry": "$_id.Cartography.geometry"
        }
    }

]
