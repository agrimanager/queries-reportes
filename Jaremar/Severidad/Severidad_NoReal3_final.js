db.form_Censodepc.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-05-24T06:00:00.000-05:00"),
                "Busqueda fin": ISODate("2021-06-17T23:45:15.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        {
            "$match": {
                "$expr": {
                    "$and": [
                        { "$gte": ["$rgDate", "$Busqueda inicio"] },
                        { "$lte": ["$rgDate", "$Busqueda fin"] },
                        {
                            $eq: ["$Point.farm", "5fac01ce246347247f068528"]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        //====cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,
                "Formula": 0
            }
        },



        //====logica de severidad
        {
            "$addFields": {
                "tiene_enfermedad": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                                { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                                { "$eq": ["$Mordisco en hojas", "SI"] },
                                { "$ne": ["$Severidad de la enfermedad", ""] }
                            ]
                        },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },


        {
            "$match": {
                "tiene_enfermedad": 1
            }
        },


        //---test
        {
            $match: {
                //"lote": "3-3A"
                "lote": "3-15A"
            }
        },


        //=======plantas(censos) enfermas x lote
        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                }
                , "numero_palmas_enfermas": { "$sum": 1 }
                , "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "numero_palmas_enfermas": "$numero_palmas_enfermas" }
                    ]
                }
            }
        },


        //=======plantas(censos) enfermas x lote x severidad
        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    // "arbol": "$arbol",
                    "severidad_enfermedad": "$Severidad de la enfermedad"
                },
                "data": { "$push": "$$ROOT" }

                ,"palmas_segun_severidad": { "$sum": 1 }
            }
        },



        { "$unwind": "$data" },


        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [

                        "$data",
                        { "numero_palmas_segun_severidad": "$palmas_segun_severidad" }
                    ]
                }
            }
        },





        {
            "$addFields": {
                "# plantas enfermas por lote": "$numero_palmas_enfermas",
                "# plantas segun severidad por lote": "$numero_palmas_segun_severidad",

                "% incidencia segun severidad por lote": {
                    "$cond": {
                        "if": { "$gt": ["$numero_palmas_enfermas", 0] },
                        "then": {
                            "$divide": [
                                {
                                    "$multiply": [
                                        { "$divide": [{ "$multiply": ["$numero_palmas_segun_severidad", 100] }, "$numero_palmas_enfermas"] }
                                        , 100]
                                }
                                , 100]
                        },
                        "else": 0
                    }
                }

            }
        },


        {
            "$addFields": {
                "% incidencia segun severidad por palma": {
                    "$cond": {
                        "if": { "$gt": ["$numero_palmas_segun_severidad", 0] },
                        "then": {
                            "$divide": ["$% incidencia segun severidad por lote","$numero_palmas_segun_severidad"]
                        },
                        "else": 0
                    }
                }

            }
        },




        {
            "$project": {
                "tiene_enfermedad": 0,
                "numero_palmas_enfermas": 0,
                "numero_palmas_segun_severidad": 0
            }
        }
    ]
)
