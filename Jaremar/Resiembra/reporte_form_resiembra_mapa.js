//----ojo se debe pegar de algun formulario
db.form_Censodepc.aggregate(
    [


        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",

                // "Finca nombre": "Caicesa-Ponce-Pizzati",
                // "FincaID": "5fbe5f972033e75d57a736e6",
                // "FincaID": ObjectId("5fbe5f972033e75d57a736e6"),


                // "Finca nombre": "San Alejo",
                // "FincaID": "5fac01ce246347247f068528",
                "FincaID": ObjectId("5fac01ce246347247f068528"),

            }
        },
        //---------------------





        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                // "datos": []
                "datos": [
                    // { "data": "data_limit1" }
                    {
                        "FincaID": "$FincaID"
                        // ,"Finca nombre":"$Finca nombre"

                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        //....query de mapa
        //---new
        //3)---pc_nuevo_x_lote  --- (cartography) --- {custom_x_lote_cantidad}
        , {
            "$lookup": {
                "from": "form_resiembra",
                "as": "data",
                "let": {
                    // //🔓 NO EDITAR
                    "finca_id": "$FincaID"
                },
                //query
                "pipeline": [


                    //  //----CONDICION INICIAL
                    {
                        "$match": {
                            "se hara resiembra": "SI"
                        }
                    },

                    //=====CARTOGRAFIA

                    //--paso1 (cartografia-nombre variable y ids)
                    {
                        "$addFields": {
                            "variable_cartografia": "$MAPA" //🚩editar
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    //--paso2 (cartografia-cruzar informacion)
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    // //--paso3 (cartografia-obtener informacion)

                    //--finca
                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    //  //----🔓CONDICION DE CRUCE lookup
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": [
                                            { "$toString": "$$finca_id" }
                                            , { "$toString": "$finca._id" }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    //--bloque
                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    //--lote
                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "geometry": { "$ifNull": ["$lote.geometry", {}] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "MAPA": 0
                            , "Point": 0
                        }
                    }


                ]
            }
        }

        //obtener cartography
        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }



        //---color
        , {
            "$addFields": {
                "color": "#FF00BF" //azul
            }
        }



        // ----proyeccion final de mapa
        , {
            "$project": {
                "_id": "$_id.elemnq",
                "idform": "$_id.idform",

                "type": "Feature",
                "properties": {
                    "Finca": "$data.finca",
                    "Bloque": "$data.bloque",
                    "Lote": "$data.lote",

                    "color": "$color"
                },
                "geometry": { "$ifNull": ["$data.geometry", {}] }
            }
        }



    ], { allowDiskUse: true }
)
