[




    { "$limit": 1 },


    {
        "$lookup": {
            "from": "cartography_polygon",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [
                {
                    "$match": {
                        "type": "lot"
                    }
                }

                , {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },

                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },

                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },



                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0
                    }
                },


                {
                    "$addFields": {
                        "total_plantas_x_lote": {
                            "$filter": {
                                "input": "$internal_features",
                                "as": "item",
                                "cond": { "$eq": ["$$item.type_features", "trees"] }
                            }
                        }
                    }
                },
                { "$unwind": "$total_plantas_x_lote" },

                {
                    "$addFields": {
                        "total_plantas_x_lote": "$total_plantas_x_lote.count_features"
                    }
                }


                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }

                , {
                    "$project": {
                        "internal_features": 0,
                        "path": 0,
                        "type": 0
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartography",
            "let": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            },

            "pipeline": [


                {
                    "$addFields": {
                        "variable_custom": "$properties.custom.Eliminada"
                    }
                },


                {
                    "$match": {
                        "variable_custom": { "$exists": true }
                        , "variable_custom.value": { "$ne": false }
                    }
                }




                , {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },

                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },

                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },



                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0
                    }
                },


                {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote"
                        }
                        , "custom_x_lote_cantidad": { "$sum": 1 }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$finca", "$_id.finca"] },
                                { "$eq": ["$$bloque", "$_id.bloque"] },
                                { "$eq": ["$$lote", "$_id.lote"] }
                            ]
                        }
                    }
                }



            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_cartography",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "plantas_eliminadas_x_lote": { "$ifNull": ["$data_cartography.custom_x_lote_cantidad", 0] }
        }
    }

    , {
        "$project": {
            "data_cartography": 0
        }
    }


    , {
        "$addFields": {
            "plantas_x_lote": {
                "$subtract": ["$total_plantas_x_lote", "$plantas_eliminadas_x_lote"]
            }
        }
    }



]
