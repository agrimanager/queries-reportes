var cursor = db.form_eliminaciondepalmas.aggregate(

  //----filtro de fechas
      {
          "$match": {
              "$expr": {
                  "$and": [
                      {
                          // $eq: ["$Point.farm", "5fac01ce246347247f068528"] //San Alejo
                          $eq: ["$Point.farm", "5fbe5f972033e75d57a736e6"] //Caicesa

                      }
                  ]
              }
          }
      },
      //----------------------------------------------------------------

    //=====CARTOGRAFIA

    //--paso1 (cartografia-nombre variable y ids)
    {
        "$addFields": {
            "variable_cartografia": "$Palma" //🚩editar
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "variable_cartografia_oid": { "$toObjectId": "$variable_cartografia.features._id" }
        }
    },

    {
        "$project": {
            "variable_cartografia_oid": 1
        }
    }




);

// cursor


cursor.forEach(item => {

    db.cartography.update(
        {
            _id: item.variable_cartografia_oid
        },
        {
            $set: {
                "properties.custom.PC Nuevo": {
                    "type": "bool",
                    "value": false,//FALSE
                    //"color": "#FFFB00",//san alejo
                    "color": "#FBFF00",//caicesa
                    "id": NumberInt(0)
                },
                "properties.custom.Recuperación vegetativa": {
                    "type": "bool",
                    "value": false,//FALSE
                    "color": "#00FFE5",
                    "id": NumberInt(0)
                },
                "properties.custom.Recuperación productiva": {
                    "type": "bool",
                    "value": false,//FALSE
                    "color": "#88FF00",
                    "id": NumberInt(0)
                },
                "properties.custom.Eliminada": {
                    "type": "bool",
                    "value": true,//TRUE <<=============================
                    "color": "#000000",
                    "id": NumberInt(0)
                }
            }
        }
    )

})
