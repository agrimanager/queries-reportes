db.cartography.aggregate(


    //---condicion "PC Nuevo"
    {
        "$match": {
            "properties.custom.PC Nuevo": { $exists: true }
            , "properties.custom.PC Nuevo.value": { "$ne": false } // todos lo que no es falso se interpreta como verdadero

            // ,"properties.custom.PC Nuevo.value": { $ne: [] }
            // ,"properties.custom.PC Nuevo.value": { $eq: true }
            // ,"properties.custom.PC Nuevo.value": { $eq: [] }
        }
    }



    , {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },

    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
        }
    },

    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },



    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            // "info_lote": 0,
            // "Palma": 0,
            // "Point": 0,
            // "uid": 0,
            // "uDate": 0,
            // "Formula": 0
        }
    },



    //----agrupar
    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            }
            , "pc_nuevo_x_lote": { "$sum": 1 }
            // , "data": { "$push": "$$ROOT" }
        }
    },

    // { "$unwind": "$data" },

    // {
    //     "$replaceRoot": {
    //         "newRoot": {
    //             "$mergeObjects": [
    //                 "$data",
    //                 { "numero_palmas_segun_severidad": "$palmas_segun_severidad" }
    //             ]
    //         }
    //     }
    // },






)
