/*
----------jerarquia de propiedades----------
1)Recuperacion productiva
2)Recuperacion vegetativa
3)PC Nuevo
*/



var cursor = db.cartography.aggregate(

    {
        $match: {
            "properties.type": "trees",
            "properties.custom": { $ne: {} }
        }
    },
    {
        $project: {
            geometry: 0
            , type: 0
        }
    },

    // //========================= 1_CONDICION 3,2 y 1

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.PC Nuevo": { $exists: true },
    //         "properties.custom.PC Nuevo.value": true
    //     }
    // },

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.Recuperación vegetativa": { $exists: true }
    //         , "properties.custom.Recuperación vegetativa.value": true
    //     }
    // },

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.Recuperación productiva": { $exists: true }
    //         , "properties.custom.Recuperación productiva.value": true
    //     }
    // },


    // //========================= 2_CONDICION 3 y 1

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.PC Nuevo": { $exists: true },
    //         "properties.custom.PC Nuevo.value": true
    //     }
    // },

    // // //condicion propiedad
    // // {
    // //     $match: {
    // //         "properties.custom.Recuperación vegetativa": { $exists: true }
    // //         , "properties.custom.Recuperación vegetativa.value": true
    // //     }
    // // },

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.Recuperación productiva": { $exists: true }
    //         , "properties.custom.Recuperación productiva.value": true
    //     }
    // },


     //========================= 3_CONDICION 3 y 2

    //condicion propiedad
    {
        $match: {
            "properties.custom.PC Nuevo": { $exists: true },
            "properties.custom.PC Nuevo.value": true
        }
    },

    //condicion propiedad
    {
        $match: {
            "properties.custom.Recuperación vegetativa": { $exists: true }
            , "properties.custom.Recuperación vegetativa.value": true
        }
    },

    // //condicion propiedad
    // {
    //     $match: {
    //         "properties.custom.Recuperación productiva": { $exists: true }
    //         , "properties.custom.Recuperación productiva.value": true
    //     }
    // },




    //-----------
    //---ids
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },


);


// cursor


cursor.forEach(i => {

    db.cartography.updateMany(
        {
            "_id": {
                "$in": i.ids
            }
        },
        // //========================= 1_CONDICION 3,2 y 1
        // {
        //     $set: {
        //         "properties.custom.PC Nuevo": {
        //             "type": "bool",
        //             "value": false,
        //             "color": "#FFFB00",
        //             "id": NumberInt(0)
        //         },
        //         "properties.custom.Recuperación vegetativa": {
        //             "type": "bool",
        //             "value": false,
        //             "color": "#00FFE5",
        //             "id": NumberInt(0)
        //         },
        //         "properties.custom.Recuperación productiva": {
        //             "type": "bool",
        //             "value": true,
        //             "color": "#88FF00",
        //             "id": NumberInt(0)
        //         }
        //     }
        // }

        // //========================= 2_CONDICION 3 y 1
        // {
        //     $set: {
        //         "properties.custom.PC Nuevo": {
        //             "type": "bool",
        //             "value": false,
        //             "color": "#FFFB00",
        //             "id": NumberInt(0)
        //         },
        //         // "properties.custom.Recuperación vegetativa": {
        //         //     "type": "bool",
        //         //     "value": false,
        //         //     "color": "#00FFE5",
        //         //     "id": NumberInt(0)
        //         // },
        //         "properties.custom.Recuperación productiva": {
        //             "type": "bool",
        //             "value": true,
        //             "color": "#88FF00",
        //             "id": NumberInt(0)
        //         }
        //     }
        // }

         //========================= 3_CONDICION 3 y 2
        {
            $set: {
                "properties.custom.PC Nuevo": {
                    "type": "bool",
                    "value": false,
                    "color": "#FFFB00",
                    "id": NumberInt(0)
                },
                "properties.custom.Recuperación vegetativa": {
                    "type": "bool",
                    "value": true,
                    "color": "#00FFE5",
                    "id": NumberInt(0)
                },
                // "properties.custom.Recuperación productiva": {
                //     "type": "bool",
                //     "value": true,
                //     "color": "#88FF00",
                //     "id": NumberInt(0)
                // }
            }
        }
    )


});
