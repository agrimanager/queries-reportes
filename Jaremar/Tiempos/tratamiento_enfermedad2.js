db.form_Censodepc.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-05-24T06:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-06-17T23:45:15.000-05:00"),
                "Busqueda fin": ISODate("2021-06-17T12:45:15.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", timezone: "America/Managua" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", timezone: "America/Managua" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            $eq: ["$Point.farm", "5fac01ce246347247f068528"]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        {
            "$match": {
                "$expr": {
                    "$or": [
                        { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                        { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                        { "$eq": ["$Mordisco en hojas", "SI"] },
                        { "$ne": ["$Severidad de la enfermedad", ""] }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "info_lote": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,
                "Formula": 0
            }
        },





        {
            "$lookup": {
                "from": "form_tratamientodelapc",
                "let": {
                    "id": "$variable_cartografia_oid"
                    ,"fecha_censo":"$rgDate"
                },
                "pipeline": [

                    { "$unwind": "$Palmas.features" },

                    {
                        "$addFields": {
                            "palma_oid": { "$toObjectId": "$Palmas.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$in": ["$palma_oid", "$$id"]
                            }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$gte": ["$rgDate", "$$fecha_censo"]
                            }
                        }
                    }

                    ,{
                        "$sort":{
                            "rgDate":1
                        }
                    }
                    ,{
                        "$limit":1
                    }
                ],
                "as": "fecha_tratamiento"
            }
        },
        //---

        {
            "$unwind": {
                "path": "$fecha_tratamiento",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            "$addFields": {
                "fecha_tratamiento": "$fecha_tratamiento.rgDate",
                "fecha_censo": "$rgDate"
            }
        },

        {
            "$match": {
                "fecha_tratamiento": { "$exists": true },
                "fecha_censo": { "$exists": true }
            }
        },

        // {
        //     "$match": {
        //         "$expr": {
        //             "$lte": ["$fecha_censo", "$fecha_tratamiento"]
        //         }
        //     }
        // },

        {
            "$addFields": {
                "tiempo_para_tratamiento": { "$subtract": ["$fecha_tratamiento", "$fecha_censo"] }
            }
        },

        {
            "$addFields": {
                "tiempo_para_tratamiento_dias": {
                    "$divide": [
                        {
                            "$floor": {
                                "$multiply": [{ "$divide": ["$tiempo_para_tratamiento", 86400000] }, 100]
                            }
                        }
                        , 100]
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol",
                    "tiempo_tratamiento": "$tiempo_para_tratamiento_dias",
                    "fecha_tratamiento": { "$toLong": "$fecha_tratamiento" },
                    "fecha_censo": { "$toLong": "$fecha_censo" }
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "tiempo_tratamiento_avg_lote": { "$avg": "$_id.tiempo_tratamiento" },
                "avg_fecha_tratamiento": { "$avg": "$_id.fecha_tratamiento" },
                "avg_fecha_censo": { "$avg": "$_id.fecha_censo" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "tiempo_tratamiento_avg_lote": "$tiempo_tratamiento_avg_lote",
                            "avg_fecha_tratamiento": { "$toDate": "$avg_fecha_tratamiento" },
                            "avg_fecha_censo": { "$toDate": "$avg_fecha_censo" }
                        }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "avg_fecha_tratamiento": {
                    "$dateToString": {
                        "date": "$avg_fecha_tratamiento",
                        "format": "%d-%m-%Y"
                    }
                },
                "avg_fecha_censo": {
                    "$dateToString": {
                        "date": "$avg_fecha_censo",
                        "format": "%d-%m-%Y"
                    }
                }
            }
        },
        {
            "$addFields": {
                "fechas_promedio_del_lote": {
                    "$concat": [
                        "$avg_fecha_censo",
                        " - ",
                        "$avg_fecha_tratamiento"
                    ]
                }
            }
        },

        {
            "$addFields": {
                "fecha de tratamiento": { "$ifNull": ["$fecha_tratamiento", "sin datos"] },
                "fecha de recuperacion productiva": "$fecha_recup_vege",

                "tiempo para tratamiento (dias)": { "$ifNull": ["$tiempo_para_tratamiento_dias", "sin datos"] },
                "tiempo para tratamiento por lote (dias)": {
                    "$ifNull": [
                        { "$divide": [{ "$floor": { "$multiply": ["$tiempo_tratamiento_avg_lote", 100] } }, 100] },
                        "sin datos"
                    ]
                }
            }
        },

        {
            "$project": {
                "fecha_tratamiento": 0,
                "fecha_recup_censo": 0,
                "tiempo_para_tratamiento": 0,
                "tiempo_para_tratamiento_dias": 0,
                "variable_cartografia_oid": 0,
                "tiempo_tratamiento_avg_lote": 0
            }
        }
    ]
)
