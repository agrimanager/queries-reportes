
/*
//-----------jaremar
(1) renombrar la cartografia(lineas y arboles)
3-14B-174_1 (izq)

(2)trasladar los arboles (ejemplo linea = 3-14B-174_1-17 tiene los arboles de esa linea y de la 3-14B-174-1-17)
3-14B-174 (der)
*/

//===============================IDs
//***finca: San Alejo
//id: 5fac01ce246347247f068528

//***lote: 3-14B
//id: 605e393e8f91bc452c873f76
//path: ,5fac01ce246347247f068528,604631877f100a7c9417eb45,


//***poligonoMuestreo : 3-14B-174_1 (izq)
//id: 60994b863dea14ded4db08e8
//path: ,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,
//regex: ,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,


//***poligonoMuestreo : 3-14B-174 (der)
//id: 60994b883dea14ded4db0960
//path: ,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,
//regex: ,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,

//=================================ANALISIS
//-detectar arboles desubicados
db.cartography.aggregate(


    {
        $match: {
            path: {
                $regex: `^,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,`
            }
        }
    },


    {
        $project: {
            "id_fecha": {$toDate:"$_id"},

            "nombre": "$properties.name",
            "tipo": "$properties.type",
            "path": "$path"

        }
    }

)
//===>>CONCLUSION:
//los arboles desubicados se montaron el 08/09/2021 y los arboles que estan bien y hay que renombrar son los del 03/09/2021



//======================EJECUCION
//----(1) renombrar la cartografia(lineas y arboles)-- 3-14B-174_1 (izq)

//================================================================
var cursor = db.cartography.aggregate(


    {
        $match: {
            path: {
                $regex: `^,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,`
            }
        }
    },


    {
        $project: {
            "id_fecha": { $toDate: "$_id" },

            "nombre": "$properties.name",
            "tipo": "$properties.type",
            "path": "$path"

        }
    }


    //----datos a renombrar
    , {
        "$match": {
            "id_fecha": {
                "$lte": ISODate("2021-09-07T11:44:17.117-05:00")
            }
        }
    }
)


// cursor

cursor.forEach(i => {

    var item_actual = i.nombre
    var item_nuevo = item_actual.replace('-174-', '-174_1-')

    //UPDATE
    db.cartography.update(
        {_id: i._id},
        {
            $set: {
                "properties.name": item_nuevo
            }
        }
    )

});
//================================================================



//----(2)trasladar los arboles (ejemplo linea = 3-14B-174_1-17 tiene los arboles de esa linea y de la 3-14B-174-1-17)---3-14B-174 (der)
//3-14B-174-5-1
//path_old :
//,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,60b70b9e7273be4cf6719313,

//path_new :
//,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,XXXXXXXXXX,


//---DATOS PARA CRUZAR EN EXCEL
//================================================================

//obtener lineas
var cursor_lineas = db.cartography.aggregate(


    {
        $match: {
            path: {
                // $regex: `^,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,`
                $regex: `^,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,`
            }
        }
    },


    {
        $project: {
            "id_fecha": { $toDate: "$_id" },

            "nombre": "$properties.name",
            "tipo": "$properties.type",
            "path": "$path"
        }
    }


    //----datos a trasladar
    , {
        "$match": {
            "tipo": "lines"
        }
    }

    //---split linea
    ,{
        $addFields: {
            "split_nombre_linea":{$split:["$nombre","-"]}
        }
    }
    ,{
        $addFields: {
            "split_nombre_linea":{ "$arrayElemAt": ["$split_nombre_linea", 3] }
        }
    }
)

cursor_lineas

// //obtener arboles a trasladar
// var cursor = db.cartography.aggregate(


//     {
//         $match: {
//             path: {
//                 $regex: `^,5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b863dea14ded4db08e8,`
//             }
//         }
//     },


//     {
//         $project: {
//             "id_fecha": { $toDate: "$_id" },

//             "nombre": "$properties.name",
//             "tipo": "$properties.type",
//             "path": "$path"
//         }
//     }


//     //----datos a trasladar
//     , {
//         "$match": {
//             "id_fecha": {
//                 "$gte": ISODate("2021-09-07T11:44:17.117-05:00")
//             }
//         }
//     }

//     //---split linea
//     ,{
//         $addFields: {
//             "split_nombre_linea":{$split:["$nombre","-"]}
//         }
//     }
//     ,{
//         $addFields: {
//             "split_nombre_linea":{ "$arrayElemAt": ["$split_nombre_linea", 3] }
//         }
//     }
// )


// cursor

// // ================================================================




//---DATOS PARA ACTAULIZAR PATH
//================================================================



var data_update = [

    // {
    //     "id_arbol": ObjectId("6138594248bf1ff1d4c1cd08"),
    //     "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9,"
    // },
    // {"id_arbol": ObjectId("6138594248bf1ff1d4c1cd08"),"path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9,"},

    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd08"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd09"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd0a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd0b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd0c"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd0d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdde"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cddf"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cde0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce49"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce7e"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("613859bd48bf1ff1d4c1ceb0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db9," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc3"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc4"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc6"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc8"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc9"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccca"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdbc"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdbd"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdbe"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce38"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce39"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce75"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("613859be48bf1ff1d4c1ceb1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7dae," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd21"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd22"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd23"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd24"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd25"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd26"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd27"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd28"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdea"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdeb"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdec"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cded"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdee"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce4f"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce50"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce81"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("613859bc48bf1ff1d4c1cead"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cced"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccee"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccef"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf3"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf4"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd3"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce42"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce43"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce7b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce97"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7db1," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4e"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4f"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc50"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc51"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc52"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc53"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc54"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd7d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd81"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd82"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd83"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd84"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce1b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce67"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce8d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130563a48bf1ff1d4bf7dcd," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce4"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce6"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce8"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cce9"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccea"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cceb"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccec"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdcc"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdcd"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdce"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdcf"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce40"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce41"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce7a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("613859bc48bf1ff1d4c1ceac"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7daf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc61"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc62"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc63"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc64"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc65"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc66"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc67"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd8a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd8b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd8c"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd8d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce1f"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce20"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce69"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("613859bc48bf1ff1d4c1ceaa"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9e," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccaa"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccab"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccac"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccad"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccae"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccaf"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccb0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccb1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccb2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdaf"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb3"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce32"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce33"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce72"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce93"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7d9a," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccfa"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccfb"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccfc"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccfd"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccfe"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccff"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd00"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd01"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd02"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd8"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd9"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdda"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cddb"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce46"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce47"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce7c"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce98"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dba," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd30"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd31"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd32"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd33"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd34"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd35"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd36"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd37"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf3"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf4"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce53"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce54"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce83"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce9b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7dbf," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccbc"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccbd"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccbe"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccbf"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc0"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc1"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccc2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb8"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdb9"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdba"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdbb"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce36"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce37"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce74"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce94"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd48"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd49"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4c"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdfe"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdff"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1ce00"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce59"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce86"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("613859bf48bf1ff1d4c1ceb2"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc7," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4c"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc49"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cc4d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd7f"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cd80"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce19"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce1a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce66"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613055ce48bf1ff1d4bf7dcb," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4e"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd4f"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd50"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd51"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd52"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd53"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1ce01"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1ce02"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1ce03"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce5a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce5b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("613859b548bf1ff1d4c1ce9d"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc6," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf6"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf8"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1ccf9"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd4"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdd6"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce44"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce45"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("613859b948bf1ff1d4c1cea5"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130541848bf1ff1d4bf7da5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd38"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd39"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd3a"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd3b"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf6"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdf7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce55"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("613859b948bf1ff1d4c1cea7"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,6130548948bf1ff1d4bf7db5," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd44"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd45"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd46"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138594248bf1ff1d4c1cd47"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdfc"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138597e48bf1ff1d4c1cdfd"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("6138599d48bf1ff1d4c1ce58"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },
    { "id_arbol": ObjectId("613859ad48bf1ff1d4c1ce85"), "path_nuevo": ",5fac01ce246347247f068528,604631877f100a7c9417eb45,605e393e8f91bc452c873f76,60994b883dea14ded4db0960,613054f748bf1ff1d4bf7dc4," },

];


data_update.forEach(i=>{

    //UPDATE
    db.cartography.update(
        {_id: i.id_arbol},
        {
            $set: {
                "path": i.path_nuevo
            }
        }
    )

})
