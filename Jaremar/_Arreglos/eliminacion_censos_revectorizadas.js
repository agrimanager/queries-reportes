

var data_delete = [

    // {
    //     "cartografia": "3-15A-185",
    //     "fecha": "2021-07-08"
    // },
    // {
    //     "cartografia": "3-9C-146",
    //     "fecha": "2021-07-08"
    // }

    // {"cartografia": "
    // 3-15A-185
    // ","fecha": "
    // 2021-07-08
    // "},

    { "cartografia": "3-15A-185", "fecha": "2021-07-08" },
    { "cartografia": "3-9C-146", "fecha": "2021-07-08" },
    { "cartografia": "3-14D-116", "fecha": "2021-07-08" },
    { "cartografia": "3-9D-62", "fecha": "2021-07-14" },
    { "cartografia": "3-9D-34", "fecha": "2021-07-14" },
    { "cartografia": "19D-4", "fecha": "2021-07-14" },
    { "cartografia": "19D-7", "fecha": "2021-07-14" },
    { "cartografia": "3-13D-82", "fecha": "2021-08-03" },
    { "cartografia": "3-13D-86", "fecha": "2021-08-03" },
    { "cartografia": "3-11A-161", "fecha": "2021-08-17" },
    { "cartografia": "19D-6", "fecha": "2021-08-17" },
    { "cartografia": "3-4D A-170 ", "fecha": "2021-08-24" },
    { "cartografia": "3-16D-332", "fecha": "2021-08-27" },
    { "cartografia": "3-16D-333", "fecha": "2021-08-27" },
    { "cartografia": "3-16D-328", "fecha": "2021-08-27" },
    { "cartografia": "3-16D-327", "fecha": "2021-08-27" },
    { "cartografia": "19D-3", "fecha": "2021-08-27" },
    { "cartografia": "3-14B-174_1", "fecha": "2021-09-08" },
    { "cartografia": "3-14B-174_2", "fecha": "2021-09-08" },
    { "cartografia": "3-9D-34 B", "fecha": "2021-09-21" },
    { "cartografia": "3-15A-186", "fecha": "2021-09-25" },
    { "cartografia": "3-1B-293", "fecha": "2021-09-25" },
    { "cartografia": "3-15A-181_2", "fecha": "2021-09-25" },
    { "cartografia": "3-13B-44", "fecha": "2021-09-25" },
    { "cartografia": "3-13B-107", "fecha": "2021-09-25" },
    { "cartografia": "3-20C-151 B", "fecha": "2021-09-25" },
    { "cartografia": "3-7C-17", "fecha": "2021-09-25" },
    { "cartografia": "3-15A-277", "fecha": "2021-09-25" },
    { "cartografia": "3-15A-277", "fecha": "2021-09-25" },
    { "cartografia": "3-15A-278", "fecha": "2021-09-25" },

    // --ojo
    { "cartografia": "3-4D B-28", "fecha": "2022-01-01" },
    { "cartografia": "3-4D B-27", "fecha": "2022-01-01" },


];



var array_result = [];

data_delete.forEach(item => {


    var cursor = db.form_Censodepc.aggregate(

        {
            $match: {
                $expr: { $lte: ["$rgDate", { "$toDate": item.fecha }] }
            }
        },



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // {
        //     "$addFields": {
        //         "finca": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$finca",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },

        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "finca._id",
        //         "foreignField": "_id",
        //         "as": "finca"
        //     }
        // },
        // { "$unwind": "$finca" },

        // { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        // {
        //     "$addFields": {
        //         "bloque": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$bloque",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },

        // { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "poligono adicional": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$poligono adicional",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "poligono adicional": { "$ifNull": ["$poligono adicional.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "info_lote": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,
                "Formula": 0
            }
        },


        {
            $match: {
                $expr: {
                    $or: [
                        { $eq: ["$lote", item.cartografia] },
                        { $eq: ["$poligono adicional", item.cartografia] },
                    ]
                }
            }
        }


        // , {
        //     "$project": {
        //         "bloque": 1,
        //         "lote": 1,
        //         "poligono adicional": 1,
        //         "linea": 1
        //     }
        // }


    )

    cursor.forEach(item_cursor => {
        array_result.push(item_cursor);
    })

    // cursor.close();

})


//array_result

//eliminar
array_result.forEach(item_a_borrar => {
    // console.log(item_a_borrar._id)
    db.form_Censodepc.remove({ _id: item_a_borrar._id })
})
