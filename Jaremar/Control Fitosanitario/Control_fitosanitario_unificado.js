db.form_controlfitosanitariogjh.aggregate(
    [
        //----------------------------------
        //-----VARIBLES IYECTADAS

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),

                // "Busqueda fin": ISODate("2021-06-17T23:45:15.000-05:00"),
                //"Busqueda fin": ISODate("2021-06-17T12:45:15.000-05:00"),
                "Busqueda fin": new Date,

                "FincaID": ObjectId("5fac01ce246347247f068528"),//San Alejo
                // "FincaID": ObjectId("5fbe5f972033e75d57a736e6"),//Caicesa
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Managua" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Managua" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            // $eq: ["$Point.farm", "5fac01ce246347247f068528"] //San Alejo
                            // $eq: ["$Point.farm", "5fbe5f972033e75d57a736e6"] //Caicesa

                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]

                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------

        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },




        {
            "$addFields": {
                "poligono adicional": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$poligono adicional",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "poligono adicional": { "$ifNull": ["$poligono adicional.properties.name", "no existe"] } } },




        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "coordenada x": {
                    "$ifNull": [
                        {
                            "$arrayElemAt": ["$variable_cartografia.features.geometry.coordinates", 0]
                        },
                        "sin datos"
                    ]
                },
                "coordenada y": {
                    "$ifNull": [
                        {
                            "$arrayElemAt": [
                                "$variable_cartografia.features.geometry.coordinates",
                                1
                            ]
                        },
                        "sin datos"
                    ]

                }
            }
        },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0
            }
        }



        //data_agrupacion
        , {
            "$group": {
                "_id": {
                    // "formulario": "control fitosanitario gjh",
                    "finca_id": "$FincaID"
                }
                , "data": { "$push": "$$ROOT" }
                , "Busqueda inicio": { "$min": "$Busqueda inicio" }
                , "Busqueda fin": { "$min": "$Busqueda fin" }
            }
        }

        //cruzar con otro form
        , {
            "$lookup": {
                "from": "form_controlfitosanitario",
                "as": "data_form",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2

                    , "finca_id": "$_id.finca_id",              //--FincaID

                    // , "user_timezone": "$user_timezone",         //--filtro_timezone
                },

                "pipeline": [


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Managua" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Managua" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                    , {
                                        "$eq": ["$Point.farm", { "$toString": "$$finca_id" }]
                                    }
                                ]
                            }
                        }
                    },
                    //----------------------------------------------------------------

                    {
                        "$addFields": {
                            "variable_cartografia": "$Palma"
                        }
                    },

                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },



                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    {
                        "$lookup": {
                            "from": "cartography_polygon",
                            "localField": "lote._id",
                            "foreignField": "_id",
                            "as": "info_lote"
                        }
                    },

                    {
                        "$addFields": {
                            "info_lote": "$info_lote.internal_features"
                        }
                    },
                    { "$unwind": "$info_lote" },

                    {
                        "$addFields": {
                            "total_plantas_x_lote": {
                                "$filter": {
                                    "input": "$info_lote",
                                    "as": "item",
                                    "cond": { "$eq": ["$$item.type_features", "trees"] }
                                }
                            }
                        }
                    },
                    { "$unwind": "$total_plantas_x_lote" },

                    {
                        "$addFields": {
                            "total_plantas_x_lote": "$total_plantas_x_lote.count_features"
                        }
                    },


                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "as": "data_cartography",
                            "let": {

                                "finca": "$finca",
                                "bloque": "$bloque",
                                "lote": "$lote"
                            },

                            "pipeline": [


                                {
                                    "$addFields": {
                                        "variable_custom": "$properties.custom.Eliminada"
                                    }
                                },


                                {
                                    "$match": {
                                        "variable_custom": { "$exists": true }
                                        , "variable_custom.value": { "$ne": false }
                                    }
                                }




                                , {
                                    "$addFields": {
                                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                                    }
                                },

                                {
                                    "$addFields": {
                                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                                    }
                                },

                                {
                                    "$addFields": {
                                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                                    }
                                },

                                {
                                    "$addFields": {
                                        "split_path_oid": {
                                            "$concatArrays": [
                                                "$split_path_padres_oid",
                                                "$variable_cartografia_oid"
                                            ]
                                        }
                                    }
                                },



                                {
                                    "$lookup": {
                                        "from": "cartography",
                                        "localField": "split_path_oid",
                                        "foreignField": "_id",
                                        "as": "objetos_del_cultivo"
                                    }
                                },

                                {
                                    "$addFields": {
                                        "tiene_variable_cartografia": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                                "then": "si",
                                                "else": "no"
                                            }
                                        }
                                    }
                                },

                                {
                                    "$addFields": {
                                        "objetos_del_cultivo": {
                                            "$cond": {
                                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                                "then": "$objetos_del_cultivo",
                                                "else": {
                                                    "$concatArrays": [
                                                        "$objetos_del_cultivo",
                                                        ["$variable_cartografia.features"]
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                },



                                {
                                    "$addFields": {
                                        "finca": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$finca",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },

                                {
                                    "$lookup": {
                                        "from": "farms",
                                        "localField": "finca._id",
                                        "foreignField": "_id",
                                        "as": "finca"
                                    }
                                },
                                { "$unwind": "$finca" },

                                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                                {
                                    "$addFields": {
                                        "bloque": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$bloque",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },

                                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                                {
                                    "$addFields": {
                                        "lote": {
                                            "$filter": {
                                                "input": "$objetos_del_cultivo",
                                                "as": "item_cartografia",
                                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                            }
                                        }
                                    }
                                },
                                {
                                    "$unwind": {
                                        "path": "$lote",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },

                                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                                {
                                    "$project": {
                                        "variable_cartografia": 0,
                                        "split_path_padres": 0,
                                        "split_path_padres_oid": 0,
                                        "variable_cartografia_oid": 0,
                                        "split_path_oid": 0,
                                        "objetos_del_cultivo": 0,
                                        "tiene_variable_cartografia": 0
                                    }
                                },


                                {
                                    "$group": {
                                        "_id": {
                                            "finca": "$finca",
                                            "bloque": "$bloque",
                                            "lote": "$lote"
                                        }
                                        , "custom_x_lote_cantidad": { "$sum": 1 }
                                    }
                                },


                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$finca", "$_id.finca"] },
                                                { "$eq": ["$$bloque", "$_id.bloque"] },
                                                { "$eq": ["$$lote", "$_id.lote"] }
                                            ]
                                        }
                                    }
                                }



                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$data_cartography",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , {
                        "$addFields": {
                            "plantas_eliminadas_x_lote": { "$ifNull": ["$data_cartography.custom_x_lote_cantidad", 0] }
                        }
                    }

                    , {
                        "$project": {
                            "data_cartography": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "plantas_x_lote": {
                                "$subtract": ["$total_plantas_x_lote", "$plantas_eliminadas_x_lote"]
                            }
                        }
                    },








                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

                    { "$addFields": { "plantas por lote": "$plantas_x_lote" } },



                    {
                        "$addFields": {
                            "coordenada x": {
                                "$ifNull": [
                                    {
                                        "$arrayElemAt": [
                                            "$variable_cartografia.features.geometry.coordinates",
                                            0
                                        ]
                                    },
                                    "sin datos"
                                ]
                            },
                            "coordenada y": {
                                "$ifNull": [
                                    {
                                        "$arrayElemAt": [
                                            "$variable_cartografia.features.geometry.coordinates",
                                            1
                                        ]
                                    },
                                    "sin datos"
                                ]
                            }
                        }
                    },



                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0,
                            "info_lote": 0,
                            "Palma": 0,
                            "Point": 0,
                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "plantas_x_lote": 0
                        }
                    }





                ]
            }
        }



          //=====UNIR DATOS
        , {
            "$project": {

                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data"
                        , "$data_form"
                    ]
                }

                // //----variables bases
                // , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
                // , "Busqueda fin": "$Busqueda fin"               //--filtro_fecha2
                // , "user_timezone": "$user_timezone",         //--filtro_timezone
            }
        }

        //desagregar datos
        , { "$unwind": "$data_final" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final",
                        {
                            // "rgDate": "$Busqueda inicio"
                        }
                    ]
                }
            }
        }




    ]
)
