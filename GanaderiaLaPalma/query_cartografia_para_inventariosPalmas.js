db.cartography.aggregate(

    {
        $match: {
            "properties.type": "lot"
        }
    }

    //----
    , {
        $project: {

            "Lotes": "$properties.name",
            "Palmas Iniciales": "$properties.custom.Numero de plantas.value",
            "Palmas erradicadas": "$properties.custom.Numero de plantas erradicadas.value",
            "Hectareas Iniciales": "$properties.custom.Numero de hectareas.value",
            "Año de siembra": "$properties.custom.Año de siembra.value",
            "Material": "MATERIAL 123",
            "Observaciones": "data cartografia",
        }
    }

)