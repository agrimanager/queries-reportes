

var cursor = db.form_recoleccioncosecha.aggregate(

    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2021-03-01T06:00:00.000-05:00"),
            "Busqueda fin": ISODate("2021-04-01T06:00:00.000-05:00"),
            "today": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lt": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------


    //--estructura nueva de form
    {
        "$match": {
            "Numero Racimos": { "$exists": true }
        }
    },
    {
        "$match": {
            "Numero Racimos": { "$ne": "" }
        }
    },




    //---proyectar ids como array
    {
        $project: {
            _id: 1
        }
    }

    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": 1
        }
    }


)



cursor.forEach(item => {

    db.form_recoleccioncosecha.updateMany(
        {
            "_id": {
                "$in": item.ids
            }
        }
        ,
        {
            $set: {
                "Numero cajon": 1
            }
        }
    )

})


....hacer varias veces para cajon 2 y 3 segun fechas.
