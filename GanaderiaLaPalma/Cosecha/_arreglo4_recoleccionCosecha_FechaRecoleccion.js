

var cursor = db.form_recoleccioncosecha.aggregate(

    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2021-05-11T12:00:00.000-05:00"),
            "Busqueda fin": ISODate("2021-05-31T12:00:00.000-05:00"),
            "today": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------


    //---proyectar
    {
        $project: {
            _id: 1,
            rgDate: 1,
        }
    }

)


// //--ver
// cursor


cursor.forEach(item => {

    db.form_recoleccioncosecha.update(
        {
            "_id": item._id
        }
        ,
        {
            $set: {
                "Fecha de recoleccion": item.rgDate
            }
        }
    )

})
