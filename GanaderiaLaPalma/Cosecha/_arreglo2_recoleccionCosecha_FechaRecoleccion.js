

var cursor = db.form_recoleccioncosecha.aggregate(


    //--estructura nueva de form
    {
        "$match": {
            // "Fecha de recoleccion": { "$exists": false } //paso1
            "Fecha de recoleccion": { "$exists": true } //paso2
        }
    },

    {
        "$match": {
            "Fecha de recoleccion": { "$eq": "" }//paso2
        }
    },


     //---proyectar
    {
        $project: {
            _id: 1,
            rgDate: 1,
        }
    }

)


//--ver
// cursor


cursor.forEach(item => {

    db.form_recoleccioncosecha.update(
        {
            "_id": item._id
        }
        ,
        {
            $set: {
                "Fecha de recoleccion": item.rgDate
            }
        }
    )

})
