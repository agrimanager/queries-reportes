db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [

                    //=====CARTOGRAFIA

                    //--paso1 (cartografia-nombre variable y ids)
                    {
                        "$addFields": {
                            "variable_cartografia": "$Lotes" //🚩editar
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$match": {
                            "variable_cartografia.path": { "$ne": "" }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    //--paso2 (cartografia-cruzar informacion)
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    // //--paso3 (cartografia-obtener informacion)

                    //--finca
                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    //--bloque
                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    //--lote
                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "Lotes": 0
                            , "Point": 0
                            , "Formula": 0
                            , "uid": 0
                        }
                    }



                    //-----RG_DATE
                    ,{ "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
