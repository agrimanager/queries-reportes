//---reportes
/*
--🧩Pesos Promedios 🚜Cosecha x Lotes🌴
--🧩Produccion 🚜Cosecha x Lotes🌴
--💎Productividad Ha 🚜Cosecha x Lotes🌴
*/

db.form_recoleccioncosecha.aggregate(
    [


        //================CONDICIONES BASES

        {
            "$match": {
                "Numero Racimos": { "$exists": true }
            }
        },
        {
            "$match": {
                "Numero Racimos": { "$ne": "" }
            }
        },


        //{ "$addFields": { "fecha": "$Fecha de recoleccion" } },
        { "$addFields": { "fecha": "$rgDate" } },//new

        { "$match": { "fecha": { "$ne": "" } } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },

        {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" } },

                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                          { "case": { "$eq": ["$mes", 1] }, "then": "01-Enero" },
                          { "case": { "$eq": ["$mes", 2] }, "then": "02-Febrero" },
                          { "case": { "$eq": ["$mes", 3] }, "then": "03-Marzo" },
                          { "case": { "$eq": ["$mes", 4] }, "then": "04-Abril" },
                          { "case": { "$eq": ["$mes", 5] }, "then": "05-Mayo" },
                          { "case": { "$eq": ["$mes", 6] }, "then": "06-Junio" },
                          { "case": { "$eq": ["$mes", 7] }, "then": "07-Julio" },
                          { "case": { "$eq": ["$mes", 8] }, "then": "08-" },
                          { "case": { "$eq": ["$mes", 9] }, "then": "09-Septiembre" },
                          { "case": { "$eq": ["$mes", 10] }, "then": "10-Octubre" },
                          { "case": { "$eq": ["$mes", 11] }, "then": "11-Noviembre" },
                          { "case": { "$eq": ["$mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        },


        //================CARTOGRAFIA


        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$match": {
                "variable_cartografia.path": { "$ne": "" }
            }
        },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Lote": 0
                , "Point": 0
                , "Formula": 0
                , "uid": 0
            }
        }



        //================LOTE INFO


        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "localField": "lote",
                "foreignField": "Lotes.features.properties.name",
                "as": "info_lote1"
            }
        }
        , {
            "$unwind": {
                "path": "$info_lote1",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Palmas Iniciales": { "$ifNull": ["$info_lote1.Palmas Iniciales", 0] }
                , "Palmas erradicadas": { "$ifNull": ["$info_lote1.Palmas erradicadas", 0] }

                , "Hectareas Iniciales": { "$ifNull": ["$info_lote1.Hectareas Iniciales", 0] }

                , "Año de siembra": { "$ifNull": ["$info_lote1.Año de siembra", "0"] }
            }
        }

        , {
            "$project": {
                "info_lote1": 0
            }
        }



        , {
            "$lookup": {
                "from": "form_formulariopuenteparacosecha",
                "as": "info_lote2",
                "let": {
                    "nombre_lote": "$lote"
                    , "anio": "$anio"
                    , "mes": "$mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Anio" }, "$$anio"] }
                                    , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote2",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lote2": 0
            }
        }



        //================EMPLEADOS


        , {
            "$addFields": {


                "Racimero_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Racimero" } } }, ""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [{ "$trim": { "input": { "$toString": "$Racimero" } } }, "000"]
                        }
                    }
                }

                , "Cosechero_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Cosechero" } } }, ""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [{ "$trim": { "input": { "$toString": "$Cosechero" } } }, "000"]
                        }
                    }
                }


                , "Tractorista_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Tractorista" } } }, ""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [{ "$trim": { "input": { "$toString": "$Tractorista" } } }, "000"]
                        }
                    }
                }


            }
        }



        , {
            "$lookup": {
                "from": "employees",
                "as": "Racimero_info",
                "let": {
                    "empleado_info": "$Racimero_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{ "$toString": "$$empleado_info" }, { "$toString": "$code" }] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }





        , {
            "$unwind": {
                "path": "$Racimero_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Racimero_info": {
                    "$ifNull": [
                        { "$concat": ["$Racimero_info.firstName", " ", "$Racimero_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }


        , {
            "$lookup": {
                "from": "employees",
                "as": "Cosechero_info",
                "let": {
                    "empleado_info": "$Cosechero_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{ "$toString": "$$empleado_info" }, { "$toString": "$code" }] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$Cosechero_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Cosechero_info": {
                    "$ifNull": [
                        { "$concat": ["$Cosechero_info.firstName", " ", "$Cosechero_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }


        , {
            "$lookup": {
                "from": "employees",
                "as": "Tractorista_info",
                "let": {
                    "empleado_info": "$Tractorista_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{ "$toString": "$$empleado_info" }, { "$toString": "$code" }] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$Tractorista_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Tractorista_info": {
                    "$ifNull": [
                        { "$concat": ["$Tractorista_info.firstName", " ", "$Tractorista_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }



        //================PESO APROXIMADO


        , {
            "$addFields": {
                "num_racimos_enviados": {
                    "$ifNull": ["$Numero Racimos", 0]
                }
            }
        }

        , {
            "$addFields": {
                "peso_promedio_racimos_enviados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$lote_peso_promedio" }, 0] }
                    ]
                }
            }
        }



        // //==================DESPACHO


        , {
            "$lookup": {
                "from": "form_despachoviajesaplantaextractora",
                "as": "info_despacho",
                "let": {
                    "recoleccion_fecha": "$fecha"
                    , "recoleccion_vagon": "$Numero cajon"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    {
                                        "$gte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$recoleccion_fecha"
                                                    }
                                                }
                                            }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Inicio Releccion" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$recoleccion_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Fin de Recoleccion" } } }
                                        ]
                                    }



                                    , { "$eq": [{ "$trim": { "input": { "$toString": "$Vagon" } } }, { "$trim": { "input": { "$toString": "$$recoleccion_vagon" } } }] }
                                ]
                            }
                        }
                    }
                ]
            }
        },
        {
            "$unwind": {

                "path": "$info_despacho",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                //--bases
                "despacho_peso_kg": { "$ifNull": ["$info_despacho.Peso Vagon Kg", 0] },
                "despacho_numTicket": { "$ifNull": ["$info_despacho.Tiquete", 0] },
                //--otras
                "despacho_numRemision": { "$ifNull": ["$info_despacho.Remision", 0] },
                "despacho_observaciones": { "$ifNull": ["$info_despacho.Observaciones", ""] },
                "despacho_vehiculo": { "$ifNull": ["$info_despacho.Vehiculo Transportador", ""] }

                //--fechas
                //"Salida del Vehiculo" : ISODate("1969-12-31T19:00:00.000-05:00"), //OJO opcional
                //"Fecha de Inicio Releccion" : ISODate("2021-03-20T00:00:00.000-05:00"),
                //"Fecha de Fin de Recoleccion" : ISODate("2021-03-31T00:00:00.000-05:00"),

            }
        }

        , {
            "$project": {
                "info_despacho": 0
            }
        }


        //===============AGRUPACIONES

        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "recoleccion_vagon": "$Numero cajon"
                    , "ticket": "$despacho_numTicket"//------------UNICIDAD DE DESPACHO
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_lote_viaje": {
                    "$sum": { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] }
                }
                ,
                "total_peso_racimos_alzados_lote_viaje": {
                    "$sum": "$peso_promedio_racimos_enviados"
                }
            }
        },

        {
            "$group": {
                "_id": {

                    "recoleccion_vagon": "$_id.recoleccion_vagon"
                    , "ticket": "$_id.ticket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_peso_racimos_alzados_viaje": {
                    "$sum": "$total_peso_racimos_alzados_lote_viaje"
                }

            }
        },
        {
            "$unwind": "$data"
        },

        {
            "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_racimos_alzados_lote_viaje"
            }
        },

        {
            "$unwind": "$data.data"
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                            "total_alzados_lote": "$total_alzados_lote"
                            , "ticket": "$_id.ticket"
                            , "pct_Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }

        , {
            "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$despacho_peso_kg" }, 0] }, "$pct_Alzados x lote"]
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL lote": {
                    "$cond": {
                        "if": { "$eq": ["$total_alzados_lote", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL cosecha x registro": {
                    "$multiply": ["$num_racimos_enviados", "$Peso REAL lote"]
                }
            }
        }


        // //---test para indicadores (Ha)
        // ,{
        //     $match:{
        //         "Hectareas Iniciales": 0
        //     }
        // }


        //-----------------------agrupaciones para productividad
        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$lote"
        //             , "lote_material": "$Material"
        //             , "lote_siembra": "$Año de siembra"
        //             , "lote_ha": "$Hectareas Iniciales"

        //             , "anio": "$anio"
        //             , "mes": "$mes"
        //             , "mes_txt": "$Mes_Txt"
        //         },

        //         "total_racimos": {
        //             "$sum": { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] }
        //         },
        //         "total_peso_kg": {
        //             "$sum": { "$ifNull": [{ "$toDouble": "$Peso REAL cosecha x registro" }, 0] }
        //         }
        //     }
        // },


        // {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {
        //                     "total_racimos": "$total_racimos",
        //                     "total_peso_kg": "$total_peso_kg"
        //                 }
        //             ]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "total_peso_TON": { "$divide": ["$total_peso_kg", 1000] }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "productividad": { "$divide": ["$total_peso_TON", "$lote_ha"] }
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$lote"
        //             , "lote_siembra": "$lote_siembra"
        //             , "lote_ha": "$lote_ha"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "lote_siembra": "$_id.lote_siembra"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //         , "total_ha_x_siembra": {
        //             "$sum": { "$ifNull": [{ "$toDouble": "$_id.lote_ha" }, 0] }
        //         }
        //     }
        // }

        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data.data",
        //                 {
        //                     "total_ha_x_siembra": "$total_ha_x_siembra"
        //                 }
        //             ]
        //         }
        //     }
        // }



        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$lote"
        //             , "lote_material": "$lote_material"
        //             , "lote_ha": "$lote_ha"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "lote_material": "$_id.lote_material"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //         , "total_ha_x_material": {
        //             "$sum": { "$ifNull": [{ "$toDouble": "$_id.lote_ha" }, 0] }
        //         }
        //     }
        // }

        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data.data",
        //                 {
        //                     "total_ha_x_material": "$total_ha_x_material"
        //                 }
        //             ]
        //         }
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$lote"
        //             , "lote_ha": "$lote_ha"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //         , "total_ha_x_finca": {
        //             "$sum": { "$ifNull": [{ "$toDouble": "$_id.lote_ha" }, 0] }
        //         }
        //     }
        // }

        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data.data",
        //                 {
        //                     "total_ha_x_finca": "$total_ha_x_finca"
        //                 }
        //             ]
        //         }
        //     }
        // }




    ]


)
// .sort({"rgDate": -1})
// .count()
