

var cursor = db.form_recoleccioncosecha.aggregate(

    //--estructura nueva de form
    {
        "$match": {
            "Tractorista": { "$exists": false }
        }
    },


    // //---proyectar
    {
        $project: {
            _id: 1,
            // rgDate: 1,
        }
    }

    , {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    },
    {
        "$project": {
            "_id": 0,
            "ids": 1
        }
    }


)


//--ver
// cursor


cursor.forEach(item => {

    db.form_recoleccioncosecha.updateMany(
        {
            "_id": {
                "$in": item.ids
            }
        }
        ,
        {
            $set: {
                "Tractorista":""
            }
        }
    )

})
