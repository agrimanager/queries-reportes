[

    { "$addFields": { "fecha": "$Fecha de recoleccion" } },


    { "$addFields": { "variable_cartografia": "$Lote" } },

    { "$addFields": { "elemnq": "$_id" } },


    { "$match": { "fecha": { "$ne": "" } } },
    { "$addFields": { "anio_filtro": { "$year": "$fecha" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },
    


    { "$match": { "variable_cartografia.path": { "$ne": "" } } },



    { "$unwind": "$variable_cartografia.features" }



    , {
        "$group": {
            "_id": {
                "nombre_cartografia": "$variable_cartografia.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$addFields": { "data": { "$arrayElemAt": ["$data", 0] } } },

    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$data.fecha"] }, 86400000]
                }
            }
        }
    },



    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$lt": ["$dias de ciclo", 0] },
                    "then": "#3f3b69",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "#008000",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                            , { "$lte": ["$dias de ciclo", 12] }]
                                    },
                                    "then": "#FFFF00",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                    , { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "#ff8000",
                                            "else": "#FF0000"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$lt": ["$dias de ciclo", 0] },
                    "then": "error dias",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "A-[0 a 9] dias",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                            , { "$lte": ["$dias de ciclo", 12] }]
                                    },
                                    "then": "B-[10 a 12] dias",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                    , { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "C-[13 a 15] dias",
                                            "else": "D-[>15] dias"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    },


    {
        "$project": {
            "_id": "$data.elemnq",
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.nombre_cartografia",
                "Rango": "$rango",
                "Dias Ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$dias de ciclo", -1] },
                        "then": "-1",
                        "else": {
                            "$concat": [
                                { "$toString": "$dias de ciclo" },
                                " dias"
                            ]
                        }
                    }
                },
                "color": "$color"
            },
            "geometry": "$data.variable_cartografia.features.geometry"
        }
    }
]
