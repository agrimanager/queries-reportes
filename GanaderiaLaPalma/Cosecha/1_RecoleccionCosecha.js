db.form_recoleccioncosecha.aggregate(
    [

        //=====CONDICIONALES BASE

        //---arreglo
        // {
        //     "$addFields": {
        //         "Vagon": { "$trim": { "input": { "$toString": "$Vagon" } } }
        //     }
        // },

        //--estructura nueva de form
        {
            "$match": {
                "Numero Racimos": { "$exists": true }
            }
        },
        {
            "$match": {
                "Numero Racimos": { "$ne": "" }
            }
        },


        //=====FECHAS

        // { "$addFields": { "fecha": "$rgDate" } },//old
        // { "$addFields": { "fecha": "$Fecha de recoleccion" } },//new
        { "$addFields": { "fecha": "$rgDate" } },//new 

        { "$match": { "fecha": { "$ne": "" } } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },

        {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" } },

                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        },




        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Lote" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$match": {
                "variable_cartografia.path": { "$ne": "" }
            }
        },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Lote": 0
                , "Point": 0
                , "Formula": 0
                , "uid": 0
            }
        }


        // //=====CRUCE1.1
        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "localField": "lote",
                "foreignField": "Lotes.features.properties.name",
                "as": "info_lote1"
            }
        }
        , {
            "$unwind": {
                "path": "$info_lote1",
                "preserveNullAndEmptyArrays": true
            }
        }

        //--sacar variables
        , {
            "$addFields": {
                "Palmas Iniciales": { "$ifNull": ["$info_lote1.Palmas Iniciales", 0] }
                , "Palmas erradicadas": { "$ifNull": ["$info_lote1.Palmas erradicadas", 0] }
                // , "Palmas Productivas": { "$ifNull": ["$info_lote1.Palmas Productivas", 0] }
                , "Hectareas Iniciales": { "$ifNull": ["$info_lote1.Hectareas Iniciales", 0] }
                // , "Hectareas Productivas": { "$ifNull": ["$info_lote1.Hectareas Productivas", 0] }
                // , "Material": { "$ifNull": ["$info_lote1.Material", ""] }
                , "Año de siembra": { "$ifNull": ["$info_lote1.Año de siembra", "0"] }
            }
        }

        , {
            "$project": {
                "info_lote1": 0
            }
        }



        //=====CRUCE1.2
        //-----info_lote
        , {
            "$lookup": {
                "from": "form_formulariopuenteparacosecha",
                "as": "info_lote2",
                "let": {
                    "nombre_lote": "$lote"
                    , "anio": "$anio"
                    , "mes": "$mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Anio" }, "$$anio"] }
                                    , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote2",
                "preserveNullAndEmptyArrays": true
            }
        },

        //---sacar variables
        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lote2": 0
            }
        }


        // // //===========================================EMPLEADOS

        , {
            "$addFields": {

                //new  //--- "" == "0"
                "Racimero_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Racimero" } } },""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [ { "$trim": { "input": { "$toString": "$Racimero" } } }, "000" ]
                        }
                    }
                }

                ,"Cosechero_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Cosechero" } } },""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [ { "$trim": { "input": { "$toString": "$Cosechero" } } }, "000" ]
                        }
                    }
                }


                ,"Tractorista_info": {
                    "$cond": {
                        "if": { "$eq": [{ "$trim": { "input": { "$toString": "$Tractorista" } } },""] },
                        "then": "000",
                        "else": {
                            "$ifNull": [ { "$trim": { "input": { "$toString": "$Tractorista" } } }, "000" ]
                        }
                    }
                }


            }
        }



        // //======CRUCE EMPLEADOS --cruzar con empleados el codigo

        //---Racimero_info
        , {
            "$lookup": {
                "from": "employees",
                "as": "Racimero_info",
                "let": {
                    "empleado_info": "$Racimero_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{"$toString":"$$empleado_info"},{"$toString":"$code"}] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }





        , {
            "$unwind": {
                "path": "$Racimero_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Racimero_info": {
                    "$ifNull": [
                        { "$concat": ["$Racimero_info.firstName", " ", "$Racimero_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }



        // //---Cosechero_info
        , {
            "$lookup": {
                "from": "employees",
                "as": "Cosechero_info",
                "let": {
                    "empleado_info": "$Cosechero_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{"$toString":"$$empleado_info"},{"$toString":"$code"}] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$Cosechero_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Cosechero_info": {
                    "$ifNull": [
                        { "$concat": ["$Cosechero_info.firstName", " ", "$Cosechero_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }


        // //---Tractorista_info
        , {
            "$lookup": {
                "from": "employees",
                "as": "Tractorista_info",
                "let": {
                    "empleado_info": "$Tractorista_info"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": [{"$toString":"$$empleado_info"},{"$toString":"$code"}] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$Tractorista_info",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Tractorista_info": {
                    "$ifNull": [
                        { "$concat": ["$Tractorista_info.firstName", " ", "$Tractorista_info.lastName"] }
                        , "---sin empleado---"
                    ]
                }
            }
        }



        //====peso aproximado recoleccion
        , {
            "$addFields": {
                "num_racimos_enviados": {
                    "$ifNull": ["$Numero Racimos", 0]  //---OJO
                }
            }
        }

        , {
            "$addFields": {
                "peso_promedio_racimos_enviados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$lote_peso_promedio" }, 0] }
                    ]
                }
            }
        }


    ]

)
// .sort({"rgDate": -1})
// .count()
