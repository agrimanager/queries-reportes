﻿SELECT
	getGroup(DetalleCensoPlaga."FechaCrea", 'Plagas', DetalleCensoPlaga."IdFinca") "CensoPlaga",
	Finca."Nombre" NombreFinca,
	getCodigo(Bloque."Codigo") || CASE WHEN (Bloque."Nombre" IS NULL OR TRIM(Bloque."Nombre") = '' OR TRIM(Bloque."Nombre") = Bloque."Codigo") THEN '' ELSE ' (' || Bloque."Nombre" || ')' END Bloque,
	getCodigo(Lote."Codigo") || CASE WHEN (Lote."Nombre" IS NULL OR TRIM(Lote."Nombre") = '' OR TRIM(Lote."Nombre") = Lote."Codigo") THEN '' ELSE ' (' || Lote."Nombre" || ')' END Lote,
	getCodigo(Linea."Codigo") Linea,
	getCodigo(Palma."Codigo") Palma,
	Plaga."Nombre" Plaga,
	(
		SELECT
			CASE
				WHEN (COUNT(I2."Nombre") > 0) THEN
					STRING_AGG(DISTINCT I2."Nombre", ', ')
				ELSE
					'N/A'
			END
		FROM (
			(
				SELECT I."Nombre"
				FROM "InstarPlaga" IP
				INNER JOIN "Instar" I on I."IdInstar" = IP."IdInstar"
				WHERE IP."IdDetalleCensoPlaga" = DetalleCensoPlaga."IdDetalleCensoPlaga"
			) UNION (
				SELECT I."Nombre"
				FROM "Instar" I
				WHERE I."IdInstar" = DetalleCensoPlaga."IdInstar"
			)
		) I2
	) Instar,
	DetalleCensoPlaga."Lugar" Lugar,
	DetalleCensoPlaga."Parasitaria" Parasitaria,
	COALESCE(DetalleCensoPlaga."NumHuevos", 0) NumHuevos,
	COALESCE(DetalleCensoPlaga."NumLarva", 0) NumLarva,
	COALESCE(DetalleCensoPlaga."NumPupa", 0) NumPupa,
	COALESCE(DetalleCensoPlaga."NumNinfa", 0) NumNinfa,
	COALESCE(DetalleCensoPlaga."NumMuerta", 0) NumMuerta,
	COALESCE(DetalleCensoPlaga."NumHoja", 0) NumHoja,
	COALESCE(DetalleCensoPlaga."NumAdulto", 0) NumAdulto,
	COALESCE(DetalleCensoPlaga."Distancia", 0) Distancia,
	DetalleCensoPlaga."UsuarioCrea" Usuario,
	DetalleCensoPlaga."FechaCrea" Fecha,
	DetalleCensoPlaga."Observacion" Observacion,
	REPLACE(REPLACE(REPLACE(ST_AsText(Palma.geom), 'MULTI', ''), '((', '('), '))', ')') geom
FROM "DetalleCensoPlaga" DetalleCensoPlaga
	INNER JOIN "Finca" Finca ON Finca."IdFinca" = DetalleCensoPlaga."IdFinca"
	INNER JOIN "Plaga" Plaga ON DetalleCensoPlaga."IdPlaga" = Plaga."IdPlaga"
	INNER JOIN "Palma" Palma ON Palma."Codigo" = DetalleCensoPlaga."CodPalma" AND Palma."IdFinca" = DetalleCensoPlaga."IdFinca"
	INNER JOIN "Linea" Linea ON Linea."Codigo" = Palma."CodLinea" AND Linea."IdFinca" = DetalleCensoPlaga."IdFinca"
	INNER JOIN "Lote" Lote ON Lote."Codigo" = Linea."CodLote" AND Lote."IdFinca" = DetalleCensoPlaga."IdFinca"
	INNER JOIN "Bloque" Bloque ON Bloque."Codigo" = Lote."CodBloque" AND Bloque."IdFinca" = DetalleCensoPlaga."IdFinca"

WHERE 1 = 1
	--@IdFinca AND DetalleCensoPlaga."IdFinca" = @IdFinca
	--@Bloque  AND Bloque."Codigo" = '@Bloque'
	--@Lote    AND Lote."Codigo" = '@Lote'
	--@Linea   AND Linea."Codigo" = '@Linea'
	--@CensoPlaga AND @CensoPlaga
	--@IdPlaga AND DetalleCensoPlaga."IdPlaga" = @IdPlaga
	--@Instar AND Instar."IdInstar" = '@Instar'
	--@Lugar AND DetalleCensoPlaga."Lugar" = '@Lugar'
	--@FechaInicio AND DetalleCensoPlaga."FechaCrea" >= '@FechaInicio'
	--@FechaFin AND DetalleCensoPlaga."FechaCrea" <= '@FechaFin'
	--@UsuarioCrea AND DetalleCensoPlaga."UsuarioCrea" = '@UsuarioCrea'
	--@InFincas @InFincas
ORDER BY DetalleCensoPlaga."FechaCrea"