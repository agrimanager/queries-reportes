﻿SELECT
	getGroup(DetalleCensoEnfermedad."FechaCrea", 'Enfermedades', DetalleCensoEnfermedad."IdFinca") CensoEnfermedad,
	Finca."Nombre" NombreFinca,
	getCodigo(Bloque."Codigo") || CASE WHEN (Bloque."Nombre" IS NULL OR TRIM(Bloque."Nombre") = '' OR TRIM(Bloque."Nombre") = Bloque."Codigo") THEN '' ELSE ' (' || Bloque."Nombre" || ')' END NombreBloque,						
	getCodigo(Lote."Codigo") || CASE WHEN (Lote."Nombre" IS NULL OR TRIM(Lote."Nombre") = '' OR TRIM(Lote."Nombre") = Lote."Codigo") THEN '' ELSE ' (' || Lote."Nombre" || ')' END Lote,
	getCodigo(Palma."CodLinea") Linea,
	getCodigo(Palma."Codigo") Palma,
	Enfermedad."Nombre" Enfermedad,
	Fisiopatia."Nombre" NombreFisiopatia,
	CASE Grado."EsGradoDeRecuperacion" WHEN false THEN Grado."Nombre" ELSE 'N/A' END Grado,
	CASE Grado."EsGradoDeRecuperacion" WHEN true THEN Grado."Nombre" ELSE 'N/A' END GradoRecuperacion,
	CASE DetalleCensoEnfermedad."DadaDeAlta" WHEN true THEN 'Si' ELSE 'No' END DadaDeAlta,
	CASE DetalleCensoEnfermedad."Erradicar" WHEN true THEN 'Si' ELSE 'No' END Erradicar,
	DetalleCensoEnfermedad."FechaCrea" Fecha,
	DetalleCensoEnfermedad."UsuarioCrea" Usuario,
	DetalleCensoEnfermedad."Distancia",
	DetalleCensoEnfermedad."Observacion",
	REPLACE(REPLACE(REPLACE(ST_AsText(Palma.geom), 'MULTI', ''), '((', '('), '))', ')') geom
FROM "DetalleCensoEnfermedad" DetalleCensoEnfermedad
	INNER JOIN "Finca" Finca ON Finca."IdFinca" = DetalleCensoEnfermedad."IdFinca"                                    
	LEFT JOIN "Enfermedad" Enfermedad ON Enfermedad."IdEnfermedad" = DetalleCensoEnfermedad."IdEnfermedad"
	LEFT JOIN "Grado" Grado ON Grado."IdGrado" = DetalleCensoEnfermedad."IdGrado"
	LEFT JOIN "Fisiopatia" Fisiopatia ON Fisiopatia."IdFisiopatia" = DetalleCensoEnfermedad."IdFisiopatia"
	INNER JOIN "Palma" Palma ON Palma."Codigo" = DetalleCensoEnfermedad."CodPalma" AND Palma."IdFinca" = DetalleCensoEnfermedad."IdFinca"
	INNER JOIN "Linea" Linea ON Linea."Codigo" = Palma."CodLinea" AND Linea."IdFinca" = DetalleCensoEnfermedad."IdFinca"
	INNER JOIN "Lote" Lote ON Lote."Codigo" = Linea."CodLote" AND Lote."IdFinca" = DetalleCensoEnfermedad."IdFinca"
	INNER JOIN "Bloque" Bloque ON Bloque."Codigo" = Lote."CodBloque" AND Bloque."IdFinca" = DetalleCensoEnfermedad."IdFinca"	
WHERE 1 = 1
	--@IdFinca AND DetalleCensoEnfermedad."IdFinca" = @IdFinca
	--@Bloque  AND Bloque."Codigo" = '@Bloque'
	--@Lote    AND Lote."Codigo" = '@Lote'
	--@Linea   AND Linea."Codigo" = '@Linea'
	--@CensoEnfermedad AND @CensoEnfermedad
	--@IdEnfermedad AND DetalleCensoEnfermedad."IdEnfermedad" = @IdEnfermedad
	--@IdGrado AND DetalleCensoEnfermedad."IdGrado" = @IdGrado
	--@FechaInicio AND DetalleCensoEnfermedad."FechaCrea" >= '@FechaInicio'
	--@FechaFin AND DetalleCensoEnfermedad."FechaCrea" <= '@FechaFin'
	--@UsuarioCrea AND DetalleCensoEnfermedad."UsuarioCrea" = '@UsuarioCrea'
	--@IdFisiopatia AND Fisiopatia."IdFisiopatia" = @IdFisiopatia
	--@InFincas @InFincas
ORDER BY DetalleCensoEnfermedad."FechaCrea"