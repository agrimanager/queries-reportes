db.form_modulodeproduccionpalmas.aggregate(
    [



        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"


                , "Plantas_inactivas": {
                    "$cond": {
                        "if": {
                            "$and": [

                                { "$eq": [{ "$toDouble": "$Racimos verdes" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos pintones" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos maduros" }, 0] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }

                }
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }




        , { "$addFields": { "fecha": "$rgDate" } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } }



        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"

                    , "anio": "$anio"
                    , "mes": "$mes"
                }



                , "sum_Racimos_verdes": { "$sum": { "$toDouble": "$Racimos verdes" } }
                , "sum_Racimos_pintones": { "$sum": { "$toDouble": "$Racimos pintones" } }
                , "sum_Racimos_maduros": { "$sum": { "$toDouble": "$Racimos maduros" } }


                , "total_Plantas_inactivas": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Plantas_inactivas", true] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "sum_Racimos_verdes_y_pintones": { "$sum": ["$sum_Racimos_verdes", "$sum_Racimos_pintones"] }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"

                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                }

                , "plantas_dif_censadas_x_lote": { "$sum": 1 }

                , "sum_Racimos_verdes_y_pintones": { "$sum": "$sum_Racimos_verdes_y_pintones" }

                , "total_Plantas_inactivas": {
                    "$sum": "$total_Plantas_inactivas"
                }
            }
        }



        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "as": "info_lote",
                "let": {

                    "nombre_lote": "$_id.lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lotes.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lotes.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        }



        , {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales"

            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }

        , {
            "$lookup": {
                "from": "form_formulariopuenteparacosecha",
                "as": "info_lote2",
                "let": {
                    "nombre_lote": "$_id.lote"
                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Anio" }, "$$anio"] }
                                    , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }

                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote2",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lote2": 0
            }
        }



        , {
            "$addFields": {
                "prom_racimos_x_palma": {
                    "$divide": ["$sum_Racimos_verdes_y_pintones", "$plantas_dif_censadas_x_lote"]
                }
            }
        }




        , {
            "$addFields": {
                "pct_palmas_muestradas": {
                    "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote", "$num_palmas"] }, 100]
                }
            }
        }


        , {
            "$addFields": {
                "pct_plantas_inactivas": {
                    "$multiply": [{ "$divide": ["$total_Plantas_inactivas", "$plantas_dif_censadas_x_lote"] }, 100]
                }
            }
        }


        , {
            "$addFields": {
                "num_plantas_inactivas_esperado": {
                    "$divide": [{ "$multiply": ["$pct_plantas_inactivas", "$num_palmas"] }, 100]
                }
            }
        }



        , {
            "$addFields": {
                "num_plantas_activas_esperado": {
                    "$divide": [{ "$multiply": [{ "$subtract": [100, "$pct_plantas_inactivas"] }, "$num_palmas"] }, 100]
                }
            }
        }


        , {
            "$addFields": {
                "produccion_estimada_x_mes": {
                    "$divide": [{ "$multiply": ["$num_plantas_activas_esperado", "$prom_racimos_x_palma", "$lote_peso_promedio"] }, 100]
                }
            }
        }



        , {
            "$addFields": {
                "array_meses": [

                    {
                        "num_mes": 1,
                        "pct_mes": 0.11,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 2,
                        "pct_mes": 0.21,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 3,
                        "pct_mes": 0.35,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 4,
                        "pct_mes": 0.33,
                        "cuatrimestre": 1
                    },

                    {
                        "num_mes": 5,
                        "pct_mes": 0.27,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 6,
                        "pct_mes": 0.26,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 7,
                        "pct_mes": 0.27,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 8,
                        "pct_mes": 0.21,
                        "cuatrimestre": 2
                    },

                    {
                        "num_mes": 9,
                        "pct_mes": 0.31,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 10,
                        "pct_mes": 0.34,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 11,
                        "pct_mes": 0.22,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 12,
                        "pct_mes": 0.13,
                        "cuatrimestre": 3
                    }

                ]
            }
        }


        , {
            "$addFields": {

                "pct_mes": {
                    "$filter": {
                        "input": "$array_meses",
                        "as": "item_array_meses",
                        "cond": { "$eq": ["$$item_array_meses.num_mes", "$_id.mes"] }
                    }
                }
            }
        }

        , { "$unwind": "$pct_mes" }
        , {
            "$addFields": {
                "cuatrimestre": "$pct_mes.cuatrimestre"
                , "pct_mes": "$pct_mes.pct_mes"
            }
        }


        , {
            "$addFields": {
                "distribucion_de_produccion": {
                    "$multiply": ["$produccion_estimada_x_mes", "$pct_mes"]
                }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
                            "sum_Racimos_verdes_y_pintones": "$sum_Racimos_verdes_y_pintones",
                            "total_Plantas_inactivas": "$total_Plantas_inactivas",
                            "num_palmas": "$num_palmas",
                            "lote_peso_promedio": "$lote_peso_promedio",
                            "prom_racimos_x_palma": "$prom_racimos_x_palma",
                            "pct_palmas_muestradas": "$pct_palmas_muestradas",
                            "pct_plantas_inactivas": "$pct_plantas_inactivas",
                            "num_plantas_inactivas_esperado": "$num_plantas_inactivas_esperado",
                            "num_plantas_activas_esperado": "$num_plantas_activas_esperado",
                            "produccion_estimada_x_mes": "$produccion_estimada_x_mes",
                            "pct_mes": "$pct_mes",
                            "cuatrimestre": "$cuatrimestre",
                            "distribucion_de_produccion": "$distribucion_de_produccion"

                        }
                    ]
                }
            }
        }




    ]
)
