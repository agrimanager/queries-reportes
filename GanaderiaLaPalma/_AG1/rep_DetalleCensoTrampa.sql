﻿SELECT
	getGroup(DetalleCensoTrampa."FechaCrea", 'Trampas', DetalleCensoTrampa."IdFinca") CensoTrampa,
	Finca."Nombre" NombreFinca,
	getCodigo(Bloque."Codigo") || CASE WHEN (Bloque."Nombre" IS NULL OR TRIM(Bloque."Nombre") = '' OR TRIM(Bloque."Nombre") = Bloque."Codigo") THEN '' ELSE ' (' || Bloque."Nombre" || ')' END NombreBloque,
	getCodigo(Lote."Codigo") || CASE WHEN (Lote."Nombre" IS NULL OR TRIM(Lote."Nombre") = '' OR TRIM(Lote."Nombre") = Lote."Codigo") THEN '' ELSE ' (' || Lote."Nombre" || ')' END Lote,
	getCodigo(Trampa."Codigo") Trampa,
	COALESCE(TipoTrampa."Nombre", 'N/A') TipoTrampa,
	Plaga."Nombre" NombrePlaga,	
	COALESCE(DetalleCensoTrampa."NumeroInsectos", 0) NumInsectos,
	COALESCE(DetalleCensoTrampa."NumeroMachos", 0) NumMachos,
	COALESCE(DetalleCensoTrampa."NumeroHembras", 0) NumHembras,
	(CASE DetalleCensoTrampa."CambiaFeromona" WHEN true THEN 'Si' ELSE 'No' END) CambiaFeromona,
	(CASE DetalleCensoTrampa."CambiaCebo" WHEN true THEN 'Si' ELSE 'No' END) CambiaCebo,
	(CASE DetalleCensoTrampa."CambiaLamina" WHEN true THEN 'Si' ELSE 'No' END) CambiaLamina,
	DetalleCensoTrampa."Distancia",
	DetalleCensoTrampa."UsuarioCrea" Usuario,
	DetalleCensoTrampa."FechaCrea" Fecha,
	DetalleCensoTrampa."Observacion",
	REPLACE(REPLACE(REPLACE(ST_AsText(Trampa.geom), 'MULTI', ''), '((', '('), '))', ')') geom
	,ST_YMax(Trampa.geom) Latitud
	,ST_XMax(Trampa.geom) Longitud
 FROM "DetalleCensoTrampa" DetalleCensoTrampa
	INNER JOIN "Finca" Finca ON Finca."IdFinca" = DetalleCensoTrampa."IdFinca"
	INNER JOIN "Plaga" Plaga ON DetalleCensoTrampa."IdPlaga" = Plaga."IdPlaga"
	INNER JOIN "Trampa" Trampa ON Trampa."Codigo" = DetalleCensoTrampa."CodTrampa" AND Trampa."IdFinca" = DetalleCensoTrampa."IdFinca"
	LEFT JOIN "TipoTrampa" TipoTrampa ON TipoTrampa."IdTipoTrampa" = DetalleCensoTrampa."IdTipoTrampa"
	INNER JOIN "Lote" Lote ON Trampa."CodLote" = Lote."Codigo" AND Lote."IdFinca" = DetalleCensoTrampa."IdFinca"
	INNER JOIN "Bloque" Bloque ON Bloque."Codigo" = Lote."CodBloque" AND Bloque."IdFinca" = DetalleCensoTrampa."IdFinca"
 WHERE 1 = 1
	--@IdFinca AND DetalleCensoTrampa."IdFinca" = @IdFinca
	--@Bloque  AND Bloque."Codigo" = '@Bloque'
	--@Lote    AND Lote."Codigo" = '@Lote'
	--@CensoTrampa AND @CensoTrampa
	--@IdTrampa AND DetalleCensoTrampa."IdTrampa" = @IdTrampa
	--@TipoTrampa AND DetalleCensoTrampa."IdTipoTrampa" = @TipoTrampa
	--@FechaInicio AND DetalleCensoTrampa."FechaCrea" >= '@FechaInicio'
	--@FechaFin AND DetalleCensoTrampa."FechaCrea" <= '@FechaFin'
	--@UsuarioCrea AND DetalleCensoTrampa."UsuarioCrea" = '@UsuarioCrea'
	--@InFincas @InFincas
ORDER BY DetalleCensoTrampa."FechaCrea"