db.reports.aggregate(

    {
        $lookup: {
            from: "forms",
            localField: "form",
            foreignField: "_id",
            as: "data_form"
        }
    }
    ,{
        $unwind: "$data_form"

    }

    ,{
        $project: {
            "reporte":"$name",
            "formulario":"$data_form.name"
        }
    }
)
