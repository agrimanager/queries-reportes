db.tasks.aggregate(
    [

        //---actividad
        {
            "$lookup": {
                "from": "activities",
                "localField": "activity",
                "foreignField": "_id",
                "as": "activity"
            }
        }
        , { "$unwind": "$activity" }


        //---centro de costos
        , {
            "$lookup": {
                "from": "costsCenters",
                "localField": "ccid",
                "foreignField": "_id",
                "as": "costsCenter"
            }
        }
        , { "$unwind": "$costsCenter" }



        //---finca
        , {
            "$lookup": {
                "from": "farms",
                "localField": "farm",
                "foreignField": "_id",
                "as": "farm"
            }
        }
        , { "$unwind": "$farm" }

        , {
            "$addFields": {
                "finca": "$farm.name"
            }
        }


        //---fechas
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": { "$max": "$when.finish" } } },
                "num_mes": { "$month": { "date": { "$max": "$when.finish" } } },
                "num_dia_mes": { "$dayOfMonth": { "date": { "$max": "$when.finish" } } }
            }
        }


        , {
            "$addFields": {
                "periodo_mes": {
                    "$switch": {
                        "branches": [
                            { "case": { "$lte": ["$num_dia_mes", 15] }, "then": 1 },
                            { "case": { "$gt": ["$num_dia_mes", 15] }, "then": 2 },
                        ],
                        "default": 0
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "ENERO" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "FEBRERO" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "MARZO" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "ABRIL" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "MAYO" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "JUNIO" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "JULIO" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "AGOSTO" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "SEPTIEMBRE" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "OCTUBRE" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "NOVIEMBRE" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "DICIEMBRE" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }



        //----presupuesto
        //----finca = finca
        //----ceco = conjunto labor
        //----mes
        //----periodo
        , {
            "$lookup": {
                "from": "form_ppto",
                "as": "presupuesto",
                "let": {
                    "finca": "$farm.name",
                    "ceco": "$costsCenter.name",
                    "mes": "$Mes_Txt",
                    "periodo": "$periodo_mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Finca", "$$finca"] },
                                    { "$eq": ["$Conjunto labor", "$$ceco"] },
                                    { "$eq": ["$Mes", "$$mes"] },
                                    { "$eq": ["$Periodo", "$$periodo"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$presupuesto",
                "preserveNullAndEmptyArrays": true
            }
        }



        //---costos

        //mano de obra
        //.......
        //productivityPrice.price * (sum("productivityReport.quantity"))
        //o
        //productivityPrice.price * productivityAchieved
        , {
            "$addFields": {
                "costo_mano_obra": {
                    "$multiply": ["$productivityPrice.price", "$productivityAchieved"]
                }
            }
        }

        //--inventario
        //.......
        //totalSupplies
        //o
        //cruzar con transacciones
        , {
            "$addFields": {
                "costo_inventario": "$totalSupplies"
            }
        }


        //----VALORES A MOSTAR
        , {
            "$project": {
                // "_id": 0,
                "Actividad": "$activity.name",
                "Codigo Labor": "$cod",
                "Estado Labor": {
                    "$switch": {
                        "branches": [{
                            "case": {
                                "$eq": ["$status", "To do"]
                            },
                            "then": "⏰ Por hacer"
                        }, {
                            "case": {
                                "$eq": ["$status", "Doing"]
                            },
                            "then": "💪 En progreso"
                        }, {
                            "case": {
                                "$eq": ["$status", "Done"]
                            },
                            "then": "✔ Listo"
                        }]
                    }
                },
                "Centro de costos": "$costsCenter.name",
                "Finca": "$farm.name",
                "Semana": { "$week": { "$min": "$when.start" } },
                "Fecha inicio": { "$max": "$when.start" },
                "Fecha fin": { "$max": "$when.finish" },

                "num_anio": "$num_anio",
                "num_mes": "$num_mes",
                "num_dia_mes": "$num_dia_mes",
                "periodo_mes": "$periodo_mes",
                "Mes_Txt": "$Mes_Txt",
                
                
                
                "costo_mano_obra_real": "$costo_mano_obra",
                "costo_inventario_real": "$costo_inventario",
                
                "provision_real": {"$multiply":["$costo_mano_obra",0.4287]},
                
                //"Valor" : 3060803,
                "Presupuesto" : {"$ifNull":["$presupuesto.Valor",-1]},
                
                //"Provision Mano de Obra" : 641889,
                "Presupuesto Provision" : {"$ifNull":["$presupuesto.Provision Mano de Obra",-1]},
                

            }
        }

    ]


)