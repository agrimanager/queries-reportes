db.tasks.aggregate(
    [

        //---actividad
        {
            "$lookup": {
                "from": "activities",
                "localField": "activity",
                "foreignField": "_id",
                "as": "activity"
            }
        }
        , { "$unwind": "$activity" }


        //---centro de costos
        , {
            "$lookup": {
                "from": "costsCenters",
                "localField": "ccid",
                "foreignField": "_id",
                "as": "costsCenter"
            }
        }
        , { "$unwind": "$costsCenter" }



        //---finca
        , {
            "$lookup": {
                "from": "farms",
                "localField": "farm",
                "foreignField": "_id",
                "as": "farm"
            }
        }
        , { "$unwind": "$farm" }
        
        , {
            "$addFields": {
                "finca": "$farm.name"
            }
        }


        //---fechas
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": { "$max": "$when.finish" } } },
                "num_mes": { "$month": { "date": { "$max": "$when.finish" } } },
                "num_dia_mes": { "$dayOfMonth": { "date": { "$max": "$when.finish" } } }
            }
        }


        , {
            "$addFields": {
                "periodo_mes": {
                    "$switch": {
                        "branches": [
                            { "case": { "$lte": ["$num_dia_mes", 15] }, "then": 1 },
                            { "case": { "$gt": ["$num_dia_mes", 15] }, "then": 2 },
                        ],
                        "default": 0
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "ENERO" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "FEBRERO" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "MARZO" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "ABRIL" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "MAYO" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "JUNIO" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "JULIO" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "AGOSTO" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "SEPTIEMBRE" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "OCTUBRE" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "NOVIEMBRE" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "DICIEMBRE" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }

        // , {
        //     "$addFields": {
        //         "Mes_Txt": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
        //                     { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
        //                     { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
        //                     { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
        //                     { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
        //                     { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
        //                     { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
        //                     { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
        //                     { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
        //                     { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
        //                     { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
        //                     { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
        //                 ],
        //                 "default": "Mes desconocido"
        //             }
        //         }
        //     }
        // }



        //----presupuesto
        //----finca = finca
        //----ceco = conjunto labor
        //----mes
        //----periodo
        , {
            "$lookup": {
                "from": "form_ppto",
                "as": "presupuesto",
                "let": {
                    "finca": "$farm.name",
                    "ceco": "$costsCenter.name",
                    "mes": "$Mes_Txt",
                    "periodo": "$periodo_mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Finca", "$$finca"] },
                                    { "$eq": ["$Conjunto labor", "$$ceco"] },
                                    { "$eq": ["$Mes", "$$mes"] },
                                    { "$eq": ["$Periodo", "$$periodo"] }
                                ]
                            }
                        }
                    },
                    // {
                    //     "$sort": {
                    //         "rgDate": 1
                    //     }
                    // },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        // {
        //     "$unwind": {
        //         "path": "$presupuesto",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },



        //----VALORES A MOSTAR
        // , {
        //     "$project": {
        //         // "_id": 0,
        //         "Actividad": "$activity.name",
        //         "Codigo Labor": "$cod",
        //         "Estado Labor": {
        //             "$switch": {
        //                 "branches": [{
        //                     "case": {
        //                         "$eq": ["$status", "To do"]
        //                     },
        //                     "then": "⏰ Por hacer"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$status", "Doing"]
        //                     },
        //                     "then": "💪 En progreso"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$status", "Done"]
        //                     },
        //                     "then": "✔ Listo"
        //                 }]
        //             }
        //         },
        //         "Centro de costos": "$costsCenter.name",
        //         "Finca": "$farm.name",
        //         // "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
        //         // "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
        //         // "Tipo cultivo labor": {
        //         //     "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"]
        //         // },
        //         "Semana": {
        //             "$week": {
        //                 "$min": "$when.start"
        //             }
        //         },
        //         "Fecha inicio": {
        //             "$max": "$when.start"
        //         },
        //         "Fecha fin": { "$max": "$when.finish" },
        //         // "Supervisor": {
        //         //     "$ifNull": [{
        //         //         "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"]
        //         //     }, "--sin supervisor--"]
        //         // },
        //         "num Productividad esperada de Labor": "$productivityPrice.expectedValue",
        //         "($) Precio x unidad de Actividad": "$productivityPrice.price",
        //         "[Unidad] Medida de Actividad": {
        //             "$switch": {
        //                 "branches": [{
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "blocks"]
        //                     },
        //                     "then": "Bloque"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "lot"]
        //                     },
        //                     "then": "Lotes"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "lines"]
        //                     },
        //                     "then": "Linea"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "trees"]
        //                     },
        //                     "then": "Árboles"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "fruitCenters"]
        //                     },
        //                     "then": "Centro frutero"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "samplingPolygons"]
        //                     },
        //                     "then": "Poligono de muestreo"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "valves"]
        //                     },
        //                     "then": "Valvula"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "drainages"]
        //                     },
        //                     "then": "Drenaje"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "sprinklers"]
        //                     },
        //                     "then": "Aspersors"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"]
        //                     },
        //                     "then": "Red de riego uno"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"]
        //                     },
        //                     "then": "Red de riego dos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"]
        //                     },
        //                     "then": "Red de riego tres"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "traps"]
        //                     },
        //                     "then": "Trampa"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "lanes"]
        //                     },
        //                     "then": "Vías"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "woods"]
        //                     },
        //                     "then": "Bosque"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "sensors"]
        //                     },
        //                     "then": "Sensor"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cableways"]
        //                     },
        //                     "then": "Cable vía"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "buildings"]
        //                     },
        //                     "then": "Edificio"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "waterBodies"]
        //                     },
        //                     "then": "Cuerpo de agua"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "additionalPolygons"]
        //                     },
        //                     "then": "Poligonos adicionales"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "farming units"]
        //                     },
        //                     "then": "Unidades de cultivo"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "wages"]
        //                     },
        //                     "then": "Jornales"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "quantity"]
        //                     },
        //                     "then": "Cantidades"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "mts"]
        //                     },
        //                     "then": "Metros"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "km"]
        //                     },
        //                     "then": "Kilometros"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cm"]
        //                     },
        //                     "then": "Centimetros"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "mile"]
        //                     },
        //                     "then": "Millas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "yard"]
        //                     },
        //                     "then": "Yardas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "foot"]
        //                     },
        //                     "then": "Pies"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "inch"]
        //                     },
        //                     "then": "Pulgadas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "kg"]
        //                     },
        //                     "then": "Kilogramos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "gr"]
        //                     },
        //                     "then": "Gramos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "mg"]
        //                     },
        //                     "then": "Miligramos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "US/ton"]
        //                     },
        //                     "then": "Toneladas estadounidenses"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "ton"]
        //                     },
        //                     "then": "Toneladas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "oz"]
        //                     },
        //                     "then": "Onzas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "lb"]
        //                     },
        //                     "then": "Libras"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "lts"]
        //                     },
        //                     "then": "Litros"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "US/galon"]
        //                     },
        //                     "then": "Galones estadounidenses"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "galon"]
        //                     },
        //                     "then": "Galones"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cf"]
        //                     },
        //                     "then": "Pies cúbicos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "ci"]
        //                     },
        //                     "then": "Pulgadas cúbicas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cuc"]
        //                     },
        //                     "then": "Centimetros cúbicos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cum"]
        //                     },
        //                     "then": "Metros cúbicos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "packages"]
        //                     },
        //                     "then": "Bultos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "bags"]
        //                     },
        //                     "then": "Bolsas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "sacks"]
        //                     },
        //                     "then": "sacks"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "yemas"]
        //                     },
        //                     "then": "Yemas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "bun"]
        //                     },
        //                     "then": "Factura"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cargo"]
        //                     },
        //                     "then": "Flete"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "manege"]
        //                     },
        //                     "then": "Picadero"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "hr"]
        //                     },
        //                     "then": "Hora"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "qty"]
        //                     },
        //                     "then": "Por cantidad"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "hectares"]
        //                     },
        //                     "then": "Hectáreas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "squares"]
        //                     },
        //                     "then": "Cuadras"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "dustbin"]
        //                     },
        //                     "then": "Canecas"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "bunch"]
        //                     },
        //                     "then": "Racimos"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "cubic-meter"]
        //                     },
        //                     "then": "Metro cúbico"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "metro-line"]
        //                     },
        //                     "then": "Metro Lineal"
        //                 }, {
        //                     "case": {
        //                         "$eq": ["$productivityPrice.measure", "square-meter"]
        //                     },
        //                     "then": "Metro cuadrado"
        //                 }],
        //                 "default": "--------"
        //             }
        //         },
        //         // "(#)Empleados en labor": {
        //         //     "$size": "$employees"
        //         // },
        //         "(#)Total productividad Empleados": {
        //             "$ifNull": [{
        //                 "$toDouble": "$productivityAchieved"
        //             }, 0]
        //         },
        //         "($)Total Pago Empleados": {
        //             "$divide": [{
        //                 "$subtract": [{
        //                     "$multiply": ["$TotalPagoEmpleado", 100]
        //                 }, {
        //                     "$mod": [{
        //                         "$multiply": ["$TotalPagoEmpleado", 100]
        //                     }, 1]
        //                 }]
        //             }, 100]
        //         },
        //         "Empleados asignados": {
        //             "$cond": {
        //                 "if": {
        //                     "$eq": ["$Empleados", ""]
        //                 },
        //                 "then": "--sin empleados--",
        //                 "else": "$Empleados"
        //             }
        //         },
        //         // "(#)Productos en labor": {
        //         //     "$size": "$supplies"
        //         // },
        //         "($)Total Productos": {
        //             "$divide": [{
        //                 "$subtract": [{
        //                     "$multiply": ["$totalSupplies", 100]
        //                 }, {
        //                     "$mod": [{
        //                         "$multiply": ["$totalSupplies", 100]
        //                     }, 1]
        //                 }]
        //             }, 100]
        //         },
        //         "Productos asignados": {
        //             "$cond": {
        //                 "if": {
        //                     "$eq": ["$productos", ""]
        //                 },
        //                 "then": "--sin productos--",
        //                 "else": "$productos"
        //             }
        //         },
        //         "TOTAL LABOR": {
        //             "$divide": [{
        //                 "$subtract": [{
        //                     "$multiply": ["$TOTAL LABOR", 100]
        //                 }, {
        //                     "$mod": [{
        //                         "$multiply": ["$TOTAL LABOR", 100]
        //                     }, 1]
        //                 }]
        //             }, 100]
        //         },
        //         // "Observaciones": "$observation",
        //         // "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
        //         // "Lista lotes": "$lista_lotes",
        //         // "Lista estados": "$lista_estados",
        //         // "Blancos biologicos de labor": {
        //         //     "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"]
        //         // },
        //         // "Etiquetas": {
        //         //     "$cond": {
        //         //         "if": {
        //         //             "$eq": ["$Etiquetas", ""]
        //         //         },
        //         //         "then": "--sin etiquetas--",
        //         //         "else": "$Etiquetas"
        //         //     }
        //         // }
        //     }
        // }

    ]


)