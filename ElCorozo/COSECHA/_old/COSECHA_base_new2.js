db.form_corteycarguedefruto.aggregate(

    //--condiciones iniciales
    //--lotes
    { "$match": { "LOTE.path": { "$ne": "" } } },

    //--fechas
    { "$addFields": { "anio_filtro": { "$year": "$FECHA DE PUESTA EN CAJA" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    //--cartografia
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    //Lote como OBJETO
    { "$unwind": "$LOTE.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$LOTE.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    }

    , {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" },

    //---NECESARIO PARA CRUZAR CON PESOS PROMEDIOS DE FINCA Y LOTE
    {
        "$addFields": {
            "_id_str_farm": { "$toString": "$Finca._id" }
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    // ---cruzar con peso promedio
    // ---DANGER---CRUZAR FINCA Y LOTE
    , {
        "$lookup": {
            "from": "form_pesopromedioporlote",
            "as": "Peso promedio lote",
            "let": {
                "nombre_lote": "$lote",
                "idfarm": "$_id_str_farm"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Point.farm", "$$idfarm"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                { "$sort": { "rgDate": -1 } },
                { "$limit": 1 }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso promedio lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso promedio lote": {
                "$ifNull": ["$Peso promedio lote.PESO PROMEDIO RACIMO", 0]
            }
        }
    }



    //---cruzar con despacho
    , {
        "$lookup": {
            "from": "form_despachodefruta",
            "as": "despacho_aux",
            "let": {
                "caja": "$NUMERO DE CAJA",
                "fecha": "$FECHA DE PUESTA EN CAJA"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$NUMERO DE LA CAJA", "$$caja"] },
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA DE INICIO DE LLENADO" } } }
                                    ]
                                },
                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA DE FIN DE LLENADO" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }
                , { "$sort": { "rgDate": 1 } }
                , { "$limit": 1 }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$despacho_aux",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$despacho_aux.PESO NETO EXTRACTORA" }, -1] },
            "Numero de ticket": { "$ifNull": [{ "$toDouble": "$despacho_aux.NUMERO DE TICKET DE EXTRACTORA" }, -1] }
        }
    }


    //---grupar datos

    // , {
    //     "$group": {
    //         "_id": {
    //             "lote": "$lote",
    //             "vagon": "$Codigo Vagon  Asociado a Despacho",
    //             "num_viaje_vagon": "$Numero de Viaje de Vagon",
    //             "ticket": "$Numero de ticket"
    //         },
    //         "data": {
    //             "$push": "$$ROOT"
    //         },
    //         "total_peso_racimos_alzados_lote_viaje": {
    //             "$sum": "$Peso aproximado Alzados"
    //         },
    //         "total_alzados_lote": {
    //             "$sum": "$Total Alzados"
    //         }
    //     }
    // },
    // {
    //     "$group": {
    //         "_id": {
    //             "vagon": "$_id.vagon",
    //             "num_viaje_vagon": "$_id.num_viaje_vagon",
    //             "ticket": "$_id.ticket"
    //         },
    //         "data": {
    //             "$push": "$$ROOT"
    //         },
    //         "total_peso_racimos_alzados_viaje": {
    //             "$sum": "$total_peso_racimos_alzados_lote_viaje"
    //         }

    //     }
    // },
    // {
    //     "$unwind": "$data"
    // },

    // {
    //     "$addFields":
    //         {
    //             "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
    //             "total_alzados_lote": "$data.total_alzados_lote"
    //         }
    // },

    // {
    //     "$unwind": "$data.data"
    // },
    // {
    //     "$replaceRoot": {
    //         "newRoot": {
    //             "$mergeObjects": [
    //                 "$data.data",
    //                 {
    //                     "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
    //                     "total_alzados_lote": "$total_alzados_lote"
    //                     , "ticket": "$_id.ticket"
    //                     , "(%) Alzados x lote": {
    //                         "$cond": {
    //                             "if": {
    //                                 "$eq": ["$total_peso_racimos_alzados_viaje", 0]
    //                             },
    //                             "then": 0,
    //                             "else": {
    //                                 "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
    //                             }
    //                         }
    //                     }
    //                 }
    //             ]
    //         }
    //     }
    // }

    // , {
    //     "$addFields":
    //         {
    //             "Peso REAL Alzados": {
    //                 "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

    //             }
    //         }
    // }

    // , {
    //     "$addFields": {
    //         "Peso REAL lote": {
    //             "$cond": {
    //                 "if": { "$eq": ["$Total Alzados", 0] },
    //                 "then": 0,
    //                 "else": {
    //                     "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
    //                 }
    //             }
    //         }
    //     }
    // }
    // , {
    //     "$project": {
    //         "form_despacho_cosecha": 0,
    //         "empleados_cosecha_adicionales": 0,
    //         "info_empleado_adicional": 0,
    //         "anio_filtro": 0
    //     }
    // }

    // , {
    //     "$lookup": {
    //         "from": "form_cargueinformaciondeplantas",
    //         "as": "referencia_siembras",
    //         "let": {
    //             "nombre_lote": "$lote",
    //             "cultivo": "$cultivo"
    //         },
    //         "pipeline": [
    //             {
    //                 "$match": {
    //                     "$expr": {
    //                         "$and": [
    //                             { "$eq": ["$CULTIVO", "$$cultivo"] },
    //                             { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
    //                             { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
    //                         ]
    //                     }
    //                 }
    //             },
    //             {
    //                 "$sort": {
    //                     "rgDate": -1
    //                 }
    //             },
    //             {
    //                 "$limit": 1
    //             }

    //         ]
    //     }
    // },

    // {
    //     "$unwind": {
    //         "path": "$referencia_siembras",
    //         "preserveNullAndEmptyArrays": false
    //     }
    // },



    // {
    //     "$addFields": {
    //         "Hectareas": "$referencia_siembras.HECTAREAS",
    //         "Siembra lote": { "$toString": "$referencia_siembras.SIEMBRA" },
    //         "num_palmas": "$referencia_siembras.PALMAS",
    //         "Material": "$referencia_siembras.MATERIAL",
    //         "Cod_Lote": "$referencia_siembras.CODIGO LOTE"
    //     }
    // },

    // {
    //     "$project": {
    //         "referencia_siembras": 0,
    //         "form_despacho_cosecha": 0,
    //         "empleados_cosecha_adicionales": 0,
    //         "info_empleado_adicional": 0,
    //         "Empleados de Cosecha": 0,

    //         "Empleado adicional 1": 0,
    //         "Empleado adicional 1 opcion": 0,
    //         "Empleado adicional 1 cantidad": 0,
    //         "Empleado adicional 2": 0,
    //         "Empleado adicional 2 opcion": 0,
    //         "Empleado adicional 2 cantidad": 0,
    //         "Empleado adicional 3 cantidad": 0,
    //         "Empleado adicional 3": 0,
    //         "empleado Adicional opcion3": 0,

    //         "anio_filtro": 0,
    //         "Formula": 0,
    //         "Lote": 0,
    //         "Point": 0,
    //         "_id_str_farm": 0,
    //         "uid": 0,
    //         "uDate": 0,
    //         "_id": 0
    //     }
    // }




)