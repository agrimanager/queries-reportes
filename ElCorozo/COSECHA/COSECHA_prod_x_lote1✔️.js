[
    { "$match": { "LOTE.path": { "$ne": "" } } },
    { "$match": { "LOTE.path": { "$ne": "," } } },


    { "$addFields": { "anio_filtro": { "$year": "$FECHA DE PUESTA EN CAJA" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },



    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$LOTE.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$LOTE.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    }

    , {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" },


    {
        "$addFields": {
            "_id_str_farm": { "$toString": "$Finca._id" }
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "Formula": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_pesopromedioporlote",
            "as": "Peso promedio lote",
            "let": {
                "nombre_lote": "$lote",
                "idfarm": "$_id_str_farm"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Point.farm", "$$idfarm"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                { "$sort": { "rgDate": -1 } },
                { "$limit": 1 }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso promedio lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso promedio lote": {
                "$ifNull": ["$Peso promedio lote.PESO PROMEDIO RACIMO", 0]
            }
        }
    }



    , {
        "$lookup": {
            "from": "form_despachodefruta",
            "as": "despacho_aux",
            "let": {
                "caja": "$NUMERO DE CAJA",
                "fecha": "$FECHA DE PUESTA EN CAJA"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$NUMERO DE LA CAJA", "$$caja"] },
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA DE INICIO DE LLENADO" } } }
                                    ]
                                },
                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA DE FIN DE LLENADO" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }
                , { "$sort": { "rgDate": 1 } }
                , { "$limit": 1 }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$despacho_aux",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$despacho_aux.PESO NETO EXTRACTORA" }, -1] },
            "Numero de ticket": { "$ifNull": [{ "$toDouble": "$despacho_aux.NUMERO DE TICKET DE EXTRACTORA" }, -1] }
        }
    }

    , {
        "$project": {
            "despacho_aux": 0
        }
    }

    , {
        "$addFields":
            {
                "COSECHERO": {
                    "$filter": {
                        "input": "$COSECHERO",
                        "as": "one_data_cosechero",
                        "cond": {
                            "$and": [
                                { "$ne": ["$$one_data_cosechero.name", ""] },
                                { "$ne": ["$$one_data_cosechero", 0] }
                            ]
                        }
                    }
                }
            }
    }


    , {
        "$addFields": {
            "Total Alzados": {
                "$reduce": {
                    "input": "$COSECHERO.value",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Peso aproximado Alzados": {
                "$multiply": [
                    { "$ifNull": [{ "$toDouble": "$Total Alzados" }, 0] },
                    { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                ]
            }
        }
    }




    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "caja": "$NUMERO DE CAJA",
                "ticket": "$Numero de ticket"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_peso_racimos_alzados_lote_viaje": {
                "$sum": "$Peso aproximado Alzados"
            },
            "total_alzados_lote": {
                "$sum": "$Total Alzados"
            }
        }
    },
    {
        "$group": {
            "_id": {
                "caja": "$_id.caja",
                "ticket": "$_id.ticket"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_peso_racimos_alzados_viaje": {
                "$sum": "$total_peso_racimos_alzados_lote_viaje"
            }

        }
    },
    {
        "$unwind": "$data"
    },

    {
        "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_alzados_lote"
            }
    },

    {
        "$unwind": "$data.data"
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                        "total_alzados_lote": "$total_alzados_lote"
                        , "ticket": "$_id.ticket"
                        , "(%) Alzados x lote": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                }
                            }
                        }
                    }
                ]
            }
        }
    }

    , {
        "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

                }
            }
    }

    , {
        "$addFields": {
            "Peso REAL lote": {
                "$cond": {
                    "if": { "$eq": ["$Total Alzados", 0] },
                    "then": 0,
                    "else": {
                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                    }
                }
            }
        }
    }


    , {
        "$project": {

            "_id": 0,

            "uid": 0,
            "uDate": 0,
            "supervisor": 0,
            "capture": 0,
            "anio_filtro": 0,
            "_id_str_farm": 0,

            "COSECHERO": 0,
            "LOTE": 0,
            "Point": 0
        }
    }

    , {
        "$addFields": {
            "FECHA DE PUESTA EN CAJA": { "$dateToString": { "format": "%Y-%m-%d", "date": "$FECHA DE PUESTA EN CAJA" } }
        }
    }


]