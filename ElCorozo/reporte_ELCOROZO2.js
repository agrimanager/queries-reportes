db.form_corteycarguedefruto.aggregate(
    [

        // // //--- Desagregar features de LOTE (en un censo escogen varios lotes)
        // { "$unwind": "$LOTE.features" },


        // //--- Cartografia Info
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                // "features_oid": { "$toObjectId": "$LOTE.features._id" }
                "features_oid": { "$map": { "input": "$LOTE.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        // ["$features_oid"]
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        //--- Pesos promedios x Lote
        {
            "$lookup": {
                "from": "form_pesopromedioporlote",
                "as": "Peso promedio lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$Peso promedio lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Peso promedio lote": {
                    "$ifNull": ["$Peso promedio lote.PESO PROMEDIO RACIMO", 0]
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        //----Calcular pesos de racimos
        , {
            "$addFields":
                {
                    "Peso aproximado CORTADOS": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$RACIMOS CORTADOR" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                        ]
                    },
                    "Peso aproximado CARGADOS": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$RACIMOS CARGADOS" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                        ]
                    },
                    "Peso aproximado TRANSPORTADOS": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$RACIMOS TRANSPORTADOS" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                        ]
                    }
                }
        }



        //--- Calcular Total peso aproximado de lote

        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "vagon": "$NUMERO DE CAJA"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_lote_viaje": {
                    "$sum": "$Peso aproximado TRANSPORTADOS"

                }
            }
        },
        {
            "$group": {
                "_id": "$_id.vagon",
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_viaje": {
                    "$sum": "$total_racimos_alzados_lote_viaje"
                }
            }
        },
        {
            "$unwind": "$data"
        },
        {
            "$unwind": "$data.data"
        },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Total Peso aproximado TRANSPORTADOS (lote)": "$total_racimos_alzados_viaje",
                            "(%) Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$data.data.Peso aproximado TRANSPORTADOS", "$total_racimos_alzados_viaje"]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }


        //--- Cruzar con formulario de despacho
        , {
            "$lookup": {
                "from": "form_despachodefruta",
                "as": "form_despacho_cosecha",
                "let": {
                    "vagon": "$NUMERO DE CAJA",
                    "fecha": "$rgDate"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$NUMERO DE LA CAJA", "$$vagon"] },
                                    { "$gt": ["$rgDate", "$$fecha"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": 1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$form_despacho_cosecha",
                "preserveNullAndEmptyArrays": true
            }
        },



        //--Calcular peso real
        {
            "$addFields":
                {
                    "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.PESO NETO EXTRACTORA" }, -1] }
                }
        }

        , {
            "$addFields":
                {
                    "Total Peso REAL TRANSPORTADOS (lote)": {
                        "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]
                    }
                }
        }

        , {
            "$addFields":
                {
                    "Peso REAL lote": {
                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Total Peso REAL TRANSPORTADOS (lote)" }, 0] }, "$RACIMOS TRANSPORTADOS"]
                    }
                }
        },
        {
            "$project": {
                "form_despacho_cosecha": 0
            }
        }



        //-----multiplicar
        //Peso REAL lote * Racimos * Precio

        //--- Calcular liquidacion de cada empleado
        //--- segun el registro, lote, fecha, vagon y valores de actividades 

        , {
            "$addFields": {
                "datos_empleados_cosecha": {
                    "$filter": {
                        "input": [
                            // --1 Cortador
                            {
                                "cajon": "$NUMERO DE CAJA",
                                "finca": "$Finca",
                                "bloque": "$Bloque",
                                "lote": "$lote",
                                "fecha": "$FECHA DE CARGUE",
                                "cargo": "Cortador",
                                "nombre": "$CORTADOR",
                                "racimos": { "$ifNull": [{ "$toDouble": "$RACIMOS CORTADOR" }, 0] },
                                "peso": {
                                    "$ifNull": [
                                        {
                                            "$multiply": [
                                                { "$ifNull": [{ "$toDouble": "$RACIMOS CORTADOR" }, 0] }, "$Peso REAL lote"]
                                        }
                                        , 0]
                                },
                                "costo": {
                                    "$multiply": [{
                                        "$ifNull": [
                                            {
                                                "$multiply": [
                                                    { "$ifNull": [{ "$toDouble": "$RACIMOS CORTADOR" }, 0] }, "$Peso REAL lote"]
                                            }
                                            , 0]
                                    }, { "$toDouble": "$PRECIO FRUTA CORTADA" }]

                                }

                            },

                            // --1 Cargador
                            {
                                "cajon": "$NUMERO DE CAJA",
                                "finca": "$Finca",
                                "bloque": "$Bloque",
                                "lote": "$lote",
                                "fecha": "$FECHA DE CARGUE",
                                "cargo": "Cargador",
                                "nombre": "$CARGADOR",
                                "racimos": { "$ifNull": [{ "$toDouble": "$RACIMOS CARGADOS" }, 0] },
                                "peso": {
                                    "$ifNull": [
                                        {
                                            "$multiply": [
                                                { "$ifNull": [{ "$toDouble": "$RACIMOS CARGADOS" }, 0] }, "$Peso REAL lote"]
                                        }
                                        , 0]
                                },
                                "costo": {
                                    "$multiply": [{
                                        "$ifNull": [
                                            {
                                                "$multiply": [
                                                    { "$ifNull": [{ "$toDouble": "$RACIMOS CARGADOS" }, 0] }, "$Peso REAL lote"]
                                            }
                                            , 0]
                                    }, { "$toDouble": "$PRECIO FRUTA CARGADA" }]

                                }

                            },

                            // --1 Transportador
                            {
                                "cajon": "$NUMERO DE CAJA",
                                "finca": "$Finca",
                                "bloque": "$Bloque",
                                "lote": "$lote",
                                "fecha": "$FECHA DE CARGUE",
                                "cargo": "Transportador",
                                "nombre": "$TRANSPORTADOR INTERNO",
                                "racimos": { "$ifNull": [{ "$toDouble": "$RACIMOS TRANSPORTADOS" }, 0] },
                                "peso": {
                                    "$ifNull": [
                                        {
                                            "$multiply": [
                                                { "$ifNull": [{ "$toDouble": "$RACIMOS TRANSPORTADOS" }, 0] }, "$Peso REAL lote"]
                                        }
                                        , 0]
                                },
                                "costo": {
                                    "$multiply": [{
                                        "$ifNull": [
                                            {
                                                "$multiply": [
                                                    { "$ifNull": [{ "$toDouble": "$RACIMOS TRANSPORTADOS" }, 0] }, "$Peso REAL lote"]
                                            }
                                            , 0]
                                    }, { "$toDouble": "$PRECIO FRUTA TRANSPORTADA" }]

                                }

                            }



                        ],
                        "as": "datos_empleados_cosecha",
                        "cond": {
                            "$ne": ["$$datos_empleados_cosecha.nombre", ""]
                        }
                    }
                }


            }

        },

        { "$unwind": "$datos_empleados_cosecha" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$datos_empleados_cosecha",
                        {
                            "Point": "$Point",
                            "rgDate": "$rgDate"
                        }
                    ]
                }
            }
        }


    ]
)