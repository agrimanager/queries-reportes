db.form_cenirrometro.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cenirrometro.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Cenirrometro.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "cenirrometro": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "cenirrometro": "$cenirrometro.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },


        //----join con misma tabla
        {
            "$lookup": {
                "from": "form_cenirrometro",
                "as": "registro_anterior",
                "let": {
                    "nombre_cenirrometro": "$cenirrometro",
                    "fecha": "$rgDate"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Cenirrometro.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_cenirrometro", "$Cenirrometro.features.properties.name"] },
                                    { "$lte": ["$rgDate", "$$fecha"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        ,{ "$unwind": "$registro_anterior" }
        
        //---lectura_mas_precipitacion de ayer
        ,{
            "$addFields":
                {
                    "lectura_mas_precipitacion": {
                        "$sum": [
                            { "$ifNull": [{ "$toDouble": "$registro_anterior.Lectura Nivel Cenirrometro" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$registro_anterior.Precipitacion" }, 0] }
                        ]
                    }
                }
        }
        
        //---Evaporacion = (lectura_ayer + precipitacion_ayer) - lectura_hoy
        ,{
            "$addFields":
                {
                    "evaporacion": {
                        "$subtract": [
                            { "$ifNull": [{ "$toDouble": "$lectura_mas_precipitacion" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$Precipitacion" }, 0] }
                        ]
                    }
                }
        }




    ]

)