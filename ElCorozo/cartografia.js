db.form_corteycarguedefruto.aggregate(

    //--condiciones iniciales
    //--lotes
    { "$match": { "LOTE.path": { "$ne": "" } } },

    //--fechas
    { "$addFields": { "anio_filtro_corte": { "$year": "$FECHA DE CORTE" } } },
    { "$match": { "anio_filtro_corte": { "$gt": 2000 } } },
    { "$match": { "anio_filtro_corte": { "$lt": 3000 } } },

    { "$addFields": { "anio_filtro_cargue": { "$year": "$FECHA DE CARGUE" } } },
    { "$match": { "anio_filtro_cargue": { "$gt": 2000 } } },
    { "$match": { "anio_filtro_cargue": { "$lt": 3000 } } },


    //--cartografia
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    //Lote como OBJETO
    { "$unwind": "$LOTE.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$LOTE.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    
    

    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }



)