//agrobiz
db.tasks.aggregate(
    {
        $match: {
            "cod": "ED44HD"
        }
    }

    , { $unwind: "$water" }
    
    //---convertir string de water en objeto
    ,{
        $addFields: {
            obj_water: JSON.parse("{\"name\":\"Provecampo - Difeconazole 250 EC - 1 litro\",\"_id\":\"5f10c5e29d4e8f6838f42468\",\"qty\":1}")
        }
    }
    
    ,{
        $addFields: {
            obj_water_cantidad: "$obj_water.qty"
        }
    }


)


//--------------PROCEDIMIENTO
var data_labores = db.tasks.aggregate( {$unwind:"$water"});


data_labores.forEach(i=>{
    var obj_water_aux = JSON.parse(i.water)
    
    console.log(obj_water_aux);
    console.log(obj_water_aux.name);
})
   


//--------------PROCEDIMIENTO2
db.tasks.aggregate(

    {
        $match: {
            "water": { $ne: [] }
        }
    },

    //----arreglo array con mas de 20 elemntos
    {
        "$addFields": {
            "water": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$water" }, "array"] },
                    "then": "$water",
                    "else": { "$map": { "input": { "$objectToArray": "$water" }, "as": "dataKV", "in": "$$dataKV.v" } }
                }
            }
        }
    },


    {
        "$addFields": {
            "water_tipo_variable": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$water",
                        "initialValue": "",
                        "in": { "$type": "$$this" }
                    }
                }, "sin_tipo"]
            }
        }
    },
    
    
    //----test
    {
        $group: {
            _id: "$water_tipo_variable"
            ,cantidad :{$sum:1}
        }
    }

)
