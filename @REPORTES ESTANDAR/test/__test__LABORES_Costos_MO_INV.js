//query de LABORES
db.tasks.aggregate(
    [


        //=========-MO
        {
            "$addFields": {
                "ProductividadEmpleados": { "$sum": "$productivityReport.quantity" }
            }
        },
        { "$addFields": { "TotalPagoEmpleado": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$ProductividadEmpleados" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] } } }

        //========= INV
        //--(si la labor esta cerrada)
        , {
            "$lookup": {
                "from": "suppliesTimeline",
                "localField": "_id",
                "foreignField": "lbid",
                "as": "suppliesTimeline_reference"
            }
        }

        , {
            "$addFields": {
                "TotalCostoProducto": {
                    "$reduce": {
                        "input": "$suppliesTimeline_reference",
                        "initialValue": 0,
                        "in": {
                            "$sum": [
                                "$$value",
                                {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$$this.productEquivalence" }, 0] }
                                        , { "$ifNull": [{ "$toDouble": "$$this.valueByUnity" }, 0] }
                                    ]
                                }
                            ]
                        }
                    }
                }
            }
        }

        //========= TOTAL (MO + INV)


        , {
            "$addFields": {
                "TOTAL LABOR": {
                    "$sum": [
                        { "$ifNull": [{ "$toDouble": "$TotalPagoEmpleado" }, 0] }
                        , { "$ifNull": [{ "$toDouble": "$TotalCostoProducto" }, 0] }
                    ]

                }
            }
        }

    ]
)