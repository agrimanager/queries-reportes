//query de LABORES X PRODUCTO
db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---simular fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },

        {
            "$lookup": {
                "from": "tasks",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                "pipeline": [

                    // //------------------------arreglo crop y lots
                    {
                        "$addFields": {
                            "crop": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$crop" }, "object"] },
                                    "then": [],
                                    "else": "$crop"
                                }
                            },
                            "lots": {
                                "$cond": {
                                    "if": { "$eq": ["$lots", {}] },
                                    "then": [],
                                    "else": "$lots"
                                }
                            }
                        }
                    },


                    {
                        "$match": {
                            "active": true
                        }
                    },


                    //-------------------------------------
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }, { "$unwind": "$user_timezone" }, { "$addFields": { "user_timezone": "$user_timezone.timezone" } }, {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    }, { "$unwind": "$activity" }, {
                        "$lookup": {
                            "from": "costsCenters",
                            "localField": "ccid",
                            "foreignField": "_id",
                            "as": "costsCenter"
                        }
                    }, { "$unwind": "$costsCenter" }, {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    }, { "$unwind": "$farm" }, {
                        "$lookup": {
                            "from": "employees",
                            "localField": "supervisor",
                            "foreignField": "_id",
                            "as": "supervisor"
                        }
                    }, {
                        "$unwind": {
                            "path": "$supervisor",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, { "$addFields": { "total_productos_seleccionados_labor": { "$size": "$supplies" } } }, {
                        "$unwind": {
                            "path": "$supplies",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$addFields": {
                            "supplies._id": { "$toObjectId": "$supplies._id" },
                            "supplies.wid": { "$toObjectId": "$supplies.wid" }
                        }
                    }


                    //---nuevo 2021-11-09
                    , {
                        "$addFields": {
                            "userRg": { "$ifNull": ["$userRg", "--sin Usuario Logueado--"] },
                            "userUpd": { "$ifNull": ["$userUpd", "--sin Usuario Logueado--"] }
                        }
                    }
                    , {
                        "$addFields": {
                            "mixingVolume": { "$ifNull": ["$mixingVolume", 0] }
                        }
                    }




                    // //test
                    // , { $match: {userUpd:{$ne:"--sin Usuario Logueado--"}} }
                    // , { $limit: 1000 }



                    , {
                        "$group": {
                            "_id": {
                                "_id": "$_id",
                                "cod": "$cod",
                                "activity": "$activity",
                                "costsCenter": "$costsCenter",
                                "farm": "$farm",
                                "crops": "$crop",
                                "tags": "$tags",
                                "features": "$cartography.features",
                                "lots": "$lots",
                                "supervisor": "$supervisor",
                                "when": "$when",
                                "user_timezone": "$user_timezone",
                                "employees": "$employees",
                                "planningEmployees": "$planningEmployees",
                                "planningSupplies": "$planningSupplies",
                                "total_empleados_seleccionados_labor": "$total_empleados_seleccionados_labor",
                                "total_productos_seleccionados_labor": "$total_productos_seleccionados_labor",
                                "totalSupplies": "$totalSupplies",
                                "productivityPrice": "$productivityPrice",
                                "productivityReport": "$productivityReport",
                                "observation": "$observation",
                                "status": "$status",
                                "rgDate": "$rgDate",
                                "uDate": "$uDate",
                                "biologicalTarget": "$biologicalTarget"

                                // //---nuevo 2021-11-09
                                , "userRg": "$userRg"
                                , "userUpd": "$userUpd"

                                , "mixingVolume": "$mixingVolume"

                            }, "supplies": { "$push": "$supplies" }
                        }
                    }, { "$addFields": { "_id.supplies": "$supplies" } }, { "$replaceRoot": { "newRoot": "$_id" } }, {
                        "$addFields": {
                            "supplies": {
                                "$setDifference": ["$supplies", [{
                                    "_id": null,
                                    "wid": null
                                }]]
                            }
                        }
                    }, {
                        "$lookup": {
                            "from": "suppliesTimeline",
                            "localField": "_id",
                            "foreignField": "lbid",
                            "as": "suppliesTimeline_reference"
                        }
                    }, { "$unwind": { "path": "$suppliesTimeline_reference" } }, {
                        "$lookup": {
                            "from": "supplies",
                            "localField": "suppliesTimeline_reference.sid",
                            "foreignField": "_id",
                            "as": "supplies_reference"
                        }
                    }, { "$unwind": { "path": "$supplies_reference", "preserveNullAndEmptyArrays": true } }, {
                        "$lookup": {
                            "from": "warehouses",
                            //"localField": "supplies.wid",
                            "localField": "suppliesTimeline_reference.wid",
                            "foreignField": "_id",
                            "as": "warehouse_reference"
                        }
                    }, { "$unwind": { "path": "$warehouse_reference", "preserveNullAndEmptyArrays": true } }, {
                        "$lookup": {
                            "from": "companies",
                            "localField": "warehouse_reference.cid",
                            "foreignField": "_id",
                            "as": "company_reference"
                        }
                    }, {
                        "$unwind": {
                            "path": "$company_reference",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    //---costo inv -- con error
                    //, { "$addFields": { "TotalCostoProducto": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.quantity" }, 0] }, { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.valueByUnity" }, 0] }] } } }

                    // //---costo inv -- con error
                    // //NOTA:valueByUnity esta mal ingresado a la DB entonces no usuar
                    // , {
                    //     "$addFields": {
                    //         "TotalCostoProducto": {
                    //             "$multiply": [
                    //                 //{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.quantity" }, 0] }
                    //                 { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.productEquivalence" }, 0] }
                    //                 , { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.valueByUnity" }, 0] }
                    //             ]
                    //         }
                    //     }
                    // }

                    //---costo inv -- sin error
                    //NOTA:valueByUnity esta mal ingresado a la DB entonces no usuar
                    , {
                        "$addFields": {
                            "TotalCostoProducto": {
                                "$multiply": [
                                    //{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.quantity" }, 0] }
                                    // { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.productEquivalence" }, 0] }
                                    // , { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.valueByUnity" }, 0] }
                                    { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.total" }, 0] }
                                    , -1
                                ]
                            }
                        }
                    }
                    , {
                        "$addFields": {
                            "TotalCostoUnitario": {
                                "$cond": {
                                    "if": { "$eq": [{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.productEquivalence" }, 0] }, 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": [
                                            { "$ifNull": [{ "$toDouble": "$TotalCostoProducto" }, 0] }
                                            , { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.productEquivalence" }, 0] }
                                        ]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "employees": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$employees" }, "array"] },
                                    "then": "$employees",
                                    "else": { "$map": { "input": { "$objectToArray": "$employees" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                                }
                            },
                            "productivityReport": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$productivityReport" }, "array"] },
                                    "then": "$productivityReport",
                                    "else": {
                                        "$map": {
                                            "input": { "$objectToArray": "$productivityReport" },
                                            "as": "productivityReportKV",
                                            "in": "$$productivityReportKV.v"
                                        }
                                    }
                                }
                            },
                            "lots": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$lots" }, "array"] },
                                    "then": "$lots",
                                    "else": { "$map": { "input": { "$objectToArray": "$lots" }, "as": "lotKV", "in": "$$lotKV.v" } }
                                }
                            }
                        }
                    }, { "$addFields": { "total_empleados_seleccionados_labor": { "$size": "$employees" } } }, {
                        "$addFields": {
                            "employees": {
                                "$map": {
                                    "input": "$employees",
                                    "as": "strid",
                                    "in": { "$toObjectId": "$$strid" }
                                }
                            }
                        }
                    }, {
                        "$lookup": {
                            "from": "employees",
                            "localField": "employees",
                            "foreignField": "_id",
                            "as": "Empleados"
                        }
                    }, {
                        "$addFields": {
                            "Empleados_fullNames": {
                                "$map": {
                                    "input": "$Empleados",
                                    "as": "empleado",
                                    "in": { "$concat": ["$$empleado.firstName", " ", "$$empleado.lastName"] }
                                }
                            }
                        }
                    }, {
                        "$addFields": {
                            "Empleados": {
                                "$reduce": {
                                    "input": "$Empleados_fullNames",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Etiquetas": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$tags",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin etiquetas--"]
                            },
                            "Tipo_cultivo": {
                                "$reduce": {
                                    "input": "$crops", "initialValue": "", "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] }, "then": {
                                                "$switch": {
                                                    "branches": [{
                                                        "case": { "$eq": ["$$this", "coffee"] },
                                                        "then": "Café"
                                                    }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                        "case": { "$eq": ["$$this", "avocado"] },
                                                        "then": "Aguacate"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "orange"] },
                                                        "then": "Naranja"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tangerine"] },
                                                        "then": "Mandarina"
                                                    }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                        "case": { "$eq": ["$$this", "cacao"] },
                                                        "then": "Cacao"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                        "then": "Frutos de alta densidad(uvas, fresas, cerezas, otros)"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "vegetable"] },
                                                        "then": "Hortalizas"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                        "then": "Frutos tropicales"
                                                    }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                        "case": { "$eq": ["$$this", "flower"] },
                                                        "then": "Flores"
                                                    }, { "case": { "$eq": ["$$this", "cereal"] }, "then": "Cereales" }, {
                                                        "case": { "$eq": ["$$this", "cattle"] },
                                                        "then": "Ganado"
                                                    }, { "case": { "$eq": ["$$this", "pork"] }, "then": "Cerdos" }, {
                                                        "case": { "$eq": ["$$this", "apiculture"] },
                                                        "then": "Apicultura"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-birds"] },
                                                        "then": "Otras aves"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "chicken"] },
                                                        "then": "Pollos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                        "then": "Animales acuáticos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-animals"] },
                                                        "then": "Otros animales"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "woods"] },
                                                        "then": "Bosques"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "greenhouses"] },
                                                        "then": "Invernaderos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                        "then": "Otros (no limitativo)"
                                                    }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                        "case": { "$eq": ["$$this", "yucca"] },
                                                        "then": "Yuca"
                                                    }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                                }
                                            }, "else": {
                                                "$concat": ["$$value", "; ", {
                                                    "$switch": {
                                                        "branches": [{
                                                            "case": { "$eq": ["$$this", "coffee"] },
                                                            "then": "Café"
                                                        }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                            "case": { "$eq": ["$$this", "avocado"] },
                                                            "then": "Aguacate"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "orange"] },
                                                            "then": "Naranja"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "tangerine"] },
                                                            "then": "Mandarina"
                                                        }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                            "case": { "$eq": ["$$this", "cacao"] },
                                                            "then": "Cacao"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                            "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "vegetable"] },
                                                            "then": "Hortalizas"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                            "then": "Frutos tropicales"
                                                        }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                            "case": { "$eq": ["$$this", "flower"] },
                                                            "then": "Flores"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "cereal"] },
                                                            "then": "Cereales"
                                                        }, { "case": { "$eq": ["$$this", "cattle"] }, "then": "Ganado" }, {
                                                            "case": { "$eq": ["$$this", "pork"] },
                                                            "then": "Cerdos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "apiculture"] },
                                                            "then": "Apicultura"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-birds"] },
                                                            "then": "Otras aves"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "chicken"] },
                                                            "then": "Pollos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                            "then": "Animales acuáticos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-animals"] },
                                                            "then": "Otros animales"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "woods"] },
                                                            "then": "Bosques"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "greenhouses"] },
                                                            "then": "Invernaderos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                            "then": "Otros (no limitativo)"
                                                        }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                            "case": { "$eq": ["$$this", "yucca"] },
                                                            "then": "Yuca"
                                                        }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                                    }
                                                }]
                                            }
                                        }
                                    }
                                }
                            },
                            "Blancos_biologicos": {
                                "$reduce": {
                                    "input": {
                                        "$cond": {
                                            "if": { "$eq": ["$biologicalTarget", ""] },
                                            "then": null,
                                            "else": "$biologicalTarget"
                                        }
                                    },
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_mapa_tipos": {
                                "$reduce": {
                                    "input": {
                                        "$reduce": {
                                            "input": "$features.properties.type",
                                            "initialValue": [],
                                            "in": {
                                                "$cond": {
                                                    "if": { "$lt": [{ "$indexOfArray": ["$$value", "$$this"] }, 0] },
                                                    "then": { "$concatArrays": ["$$value", ["$$this"]] },
                                                    "else": "$$value"
                                                }
                                            }
                                        }
                                    }, "initialValue": "", "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] }, "then": {
                                                "$switch": {
                                                    "branches": [{
                                                        "case": { "$eq": ["$$this", "blocks"] },
                                                        "then": "Bloque"
                                                    }, { "case": { "$eq": ["$$this", "lot"] }, "then": "Lotes" }, {
                                                        "case": { "$eq": ["$$this", "lines"] },
                                                        "then": "Linea"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "trees"] },
                                                        "then": "Árboles"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "fruitCenters"] },
                                                        "then": "Centro frutero"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "samplingPolygons"] },
                                                        "then": "Poligono de muestreo"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "valves"] },
                                                        "then": "Valvula"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "drainages"] },
                                                        "then": "Drenaje"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "sprinklers"] },
                                                        "then": "Aspersors"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkOne"] },
                                                        "then": "Red de riego uno"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkTwo"] },
                                                        "then": "Red de riego dos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkThree"] },
                                                        "then": "Red de riego tres"
                                                    }, { "case": { "$eq": ["$$this", "traps"] }, "then": "Trampa" }, {
                                                        "case": { "$eq": ["$$this", "lanes"] },
                                                        "then": "Vías"
                                                    }, { "case": { "$eq": ["$$this", "woods"] }, "then": "Bosque" }, {
                                                        "case": { "$eq": ["$$this", "sensors"] },
                                                        "then": "Sensor"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "cableways"] },
                                                        "then": "Cable vía"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "buildings"] },
                                                        "then": "Edificio"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "waterBodies"] },
                                                        "then": "Cuerpo de agua"
                                                    }, { "case": { "$eq": ["$$this", "additionalPolygons"] }, "then": "Poligonos adicionales" }],
                                                    "default": "--------"
                                                }
                                            }, "else": {
                                                "$concat": ["$$value", "; ", {
                                                    "$switch": {
                                                        "branches": [{
                                                            "case": { "$eq": ["$$this", "blocks"] },
                                                            "then": "Bloque"
                                                        }, { "case": { "$eq": ["$$this", "lot"] }, "then": "Lotes" }, {
                                                            "case": { "$eq": ["$$this", "lines"] },
                                                            "then": "Linea"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "trees"] },
                                                            "then": "Árboles"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "fruitCenters"] },
                                                            "then": "Centro frutero"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "samplingPolygons"] },
                                                            "then": "Poligono de muestreo"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "valves"] },
                                                            "then": "Valvula"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "drainages"] },
                                                            "then": "Drenaje"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "sprinklers"] },
                                                            "then": "Aspersors"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkOne"] },
                                                            "then": "Red de riego uno"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkTwo"] },
                                                            "then": "Redde riego dos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkThree"] },
                                                            "then": "Red de riego tres"
                                                        }, { "case": { "$eq": ["$$this", "traps"] }, "then": "Trampa" }, {
                                                            "case": { "$eq": ["$$this", "lanes"] },
                                                            "then": "Vías"
                                                        }, { "case": { "$eq": ["$$this", "woods"] }, "then": "Bosque" }, {
                                                            "case": { "$eq": ["$$this", "sensors"] },
                                                            "then": "Sensor"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "cableways"] },
                                                            "then": "Cable vía"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "buildings"] },
                                                            "then": "Edificio"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "waterBodies"] },
                                                            "then": "Cuerpo de agua"
                                                        }, { "case": { "$eq": ["$$this", "additionalPolygons"] }, "then": "Poligonos adicionales" }],
                                                        "default": "--------"
                                                    }
                                                }]
                                            }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_mapa_elementos": {
                                "$reduce": {
                                    "input": "$features.properties.name",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_lista_lotes_y_estados": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lots",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin lotes seleccionados--"]
                            }
                        }
                    }

                    // //---nuevo 2021-11-09
                    // //---nuevo 2021-12-07
                    ,{
                        "$addFields": {
                            "supplies_dosis": {
                                "$filter": {
                                    "input": "$supplies",
                                    "as": "item_producto_labor",
                                    //"cond": { "$eq": ["$$item_producto_labor._id", "$suppliesTimeline_reference.sid"] }
                                    "cond": {
                                        "$and":[
                                                {"$eq": ["$$item_producto_labor._id", "$suppliesTimeline_reference.sid"] }
                                                //,{"$eq": ["$$item_producto_labor.quantity", "$suppliesTimeline_reference.quantity"] }
                                                , { "$eq": [ {"$abs":"$$item_producto_labor.quantity"}, {"$abs":"$suppliesTimeline_reference.quantity"}] }
                                            ]
                                    }
                                }
                            }
                        }
                    }
                    , { "$unwind": "$supplies_dosis" }


                    , {
                        "$project": {
                            "_id": 0,
                            "Actividad": "$activity.name",
                            "Codigo Labor": "$cod",
                            "Estado Labor": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$status", "To do"] },
                                        "then": "⏰ Por hacer"
                                    }, { "case": { "$eq": ["$status", "Doing"] }, "then": "� En progreso" }, {
                                        "case": { "$eq": ["$status", "Done"] },
                                        "then": "✔ Listo"
                                    }]
                                }
                            },
                            "Finca": "$farm.name",
                            "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                            "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                            "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                            "Tipo cultivo labor": { "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"] },
                            "Etiquetas": { "$cond": { "if": { "$eq": ["$Etiquetas", ""] }, "then": "--sin etiquetas--", "else": "$Etiquetas" } },
                            "Centro de costos": "$costsCenter.name",
                            "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                            "[Unidad] Medida de Actividad": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                                        "then": "Bloque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                                        "then": "Lotes"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                                        "then": "Linea"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                                        "then": "Árboles"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                                        "then": "Centro frutero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                                        "then": "Poligono de muestreo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                                        "then": "Valvula"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                                        "then": "Drenaje"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                                        "then": "Aspersors"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                                        "then": "Red de riego uno"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                                        "then": "Red de riego dos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                                        "then": "Red de riego tres"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                                        "then": "Trampa"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                                        "then": "Vías"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                                        "then": "Bosque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                                        "then": "Sensor"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                                        "then": "Cable vía"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                                        "then": "Edificio"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                                        "then": "Cuerpo de agua"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                                        "then": "Poligonos adicionales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                                        "then": "Unidades de cultivo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                                        "then": "Jornales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                                        "then": "Cantidades"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                                        "then": "Metros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "km"] },
                                        "then": "Kilometros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                                        "then": "Centimetros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                                        "then": "Millas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                                        "then": "Yardas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                                        "then": "Pies"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                                        "then": "Gramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                                        "then": "Miligramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "us/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                                        "then": "Toneladas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                                        "then": "Onzas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                                        "then": "Libras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                                        "then": "Litros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                                        "then": "Galones"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                                        "then": "Bultos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                                        "then": "Bolsas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                                        "then": "sacks"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                                        "then": "Yemas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                                        "then": "Factura"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                                        "then": "Flete"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                                        "then": "Picadero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                                        "then": "Hora"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                                        "then": "Cuadras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                                        "then": "Canecas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                                        "then": "Racimos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "metro-line"] },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "square-meter"] },
                                        "then": "Metro cuadrado"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "estaquilla"] },
                                        "then": "estaquilla"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "holes"] },
                                        "then": "Hoyos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "rastas"] },
                                        "then": "Rastas"
                                    }, { "case": { "$eq": ["$productivityPrice.measure", "postes"] }, "then": "Postes" }], "default": "--------"
                                }
                            },
                            "($) Precio x unidad de Actividad": "$productivityPrice.price",
                            "Semana": { "$sum": [{ "$week": { "$min": "$when.start" } }, 1] },
                            "Fecha inicio": {
                                "$dateToString": {
                                    "date": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] },
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            },
                            "Fecha fin": {
                                "$dateToString": {
                                    "date": { "$arrayElemAt": ["$when.finish", { "$subtract": [{ "$size": "$when.finish" }, 1] }] },
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            },
                            "Blancos biologicos de labor": { "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"] },
                            "Observaciones": "$observation",


                            "Tipo producto": {
                                "$ifNull": [{
                                    "$switch": {
                                        "branches": [{
                                            "case": { "$eq": ["$supplies_reference.type", "Consumables"] },
                                            "then": "Consumibles"
                                        }, {
                                            "case": { "$eq": ["$supplies_reference.type", "Machinery"] },
                                            "then": "Maquinarias"
                                        }, { "case": { "$eq": ["$supplies_reference.type", "Tools"] }, "then": "Herramientas" }], "default": "--------"
                                    }
                                }, "---"]
                            },
                            //---DATOS DE TRANSACCION DE RODUCTO
                            "Producto": { "$ifNull": ["$supplies_reference.name", "---"] },

                            //---la unidad del producto es la de la transaccion segun su equivalencia
                            //...es decir
                            //NO $suppliesTimeline_reference.inputmeasure
                            //SI $suppliesTimeline_reference.productMeasure
                            "[Unidad] Producto": {
                                "$ifNull": [{
                                    "$switch": {
                                        "branches": [{
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "mts"] },
                                            "then": "Metros"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "km"] },
                                            "then": "Kilometros"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cm"] },
                                            "then": "Centimetros"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "mile"] },
                                            "then": "Millas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "yard"] },
                                            "then": "Yardas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "foot"] },
                                            "then": "Pies"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "inch"] },
                                            "then": "Pulgadas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "kg"] },
                                            "then": "Kilogramos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "gr"] },
                                            "then": "Gramos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "mg"] },
                                            "then": "Miligramos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "US/ton"] },
                                            "then": "Toneladas estadounidenses"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "ton"] },
                                            "then": "Toneladas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "oz"] },
                                            "then": "Onzas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "lb"] },
                                            "then": "Libras"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "lts"] },
                                            "then": "Litros"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "US/galon"] },
                                            "then": "Galones estadounidenses"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "galon"] },
                                            "then": "Galones"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cf"] },
                                            "then": "Pies cúbicos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "ci"] },
                                            "then": "Pulgadas cúbicas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cuc"] },
                                            "then": "Centimetros cúbicos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cum"] },
                                            "then": "Metros cúbicos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "packages"] },
                                            "then": "Bultos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "package"] },
                                            "then": "Bultos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "bags"] },
                                            "then": "Bolsas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "sacks"] },
                                            "then": "Sacos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "yemas"] },
                                            "then": "Yemas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "bun"] },
                                            "then": "Factura"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cargo"] },
                                            "then": "Flete"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "manege"] },
                                            "then": "Picadero"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "hr"] },
                                            "then": "Hora"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "qty"] },
                                            "then": "Por cantidad"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "hectares"] },
                                            "then": "Hectáreas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "blocks"] },
                                            "then": "Cuadras"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "dustbin"] },
                                            "then": "Canecas"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "bunch"] },
                                            "then": "Racimos"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "cubic-meter"] },
                                            "then": "Metro cúbico"
                                        }, {
                                            "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "metro-line"] },
                                            "then": "Metro Lineal"
                                        }, { "case": { "$eq": ["$suppliesTimeline_reference.productMeasure", "square-meter"] }, "then": "Metro cuadrado" }],
                                        "default": "--------"
                                    }
                                }, "---"]
                            },
                            //"(#) Cantidad Producto": { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.quantity" }, 0] },
                            "(#) Cantidad Producto": { "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.productEquivalence" }, 0] },

                            //---nuevo 2021-11-09 (volumen y mezcla)
                            "Volumen de mezcla": { "$ifNull": ["$mixingVolume", 0] },
                            "Dosis": {"$toDouble":{ "$ifNull": ["$supplies_dosis.dose", 0] }},


                            //"($) Costo unitario": { "$divide": [{ "$subtract": [{ "$multiply": [{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.valueByUnity" }, 0] }, 100] }, { "$mod": [{ "$multiply": [{ "$ifNull": [{ "$toDouble": "$suppliesTimeline_reference.valueByUnity" }, 0] }, 100] }, 1] }] }, 100] },
                            "($) Costo unitario": { "$divide": [{ "$subtract": [{ "$multiply": [{ "$ifNull": [{ "$toDouble": "$TotalCostoUnitario" }, 0] }, 100] }, { "$mod": [{ "$multiply": [{ "$ifNull": [{ "$toDouble": "$TotalCostoUnitario" }, 0] }, 100] }, 1] }] }, 100] },
                            "($) TOTAL costo producto": { "$divide": [{ "$subtract": [{ "$multiply": ["$TotalCostoProducto", 100] }, { "$mod": [{ "$multiply": ["$TotalCostoProducto", 100] }, 1] }] }, 100] },
                            "SKU Producto": { "$ifNull": ["$supplies_reference.sku", "---"] },
                            "PC Producto": { "$ifNull": ["$supplies_reference.pc", "---"] },
                            "PR Producto": { "$ifNull": ["$supplies_reference.pr", "---"] },
                            "Bodega": { "$ifNull": ["$warehouse_reference.name", "---"] },
                            "Empresa": { "$ifNull": ["$company_reference.name", "---"] },
                            "Supervisor": { "$ifNull": [{ "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"] }, "--sin supervisor--"] },
                            "(#) Empleados asignados": "$total_empleados_seleccionados_labor",
                            "Empleados asignados": {
                                "$cond": {
                                    "if": { "$eq": ["$Empleados", ""] },
                                    "then": "--sin empleados--",
                                    "else": "$Empleados"
                                }
                            },
                            "(#) Productos asignados": "$total_productos_seleccionados_labor"

                            //---nuevo 2021-11-09
                            // ,"Usuario Logueado": { "$ifNull": ["$userRg", "--sin Usuario Logueado--"] }
                            , "Usuario Logueado": { "$ifNull": ["$userUpd", "--sin Usuario Logueado--"] }


                            //---rgDate filtro fecha
                            , "rgDate": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    }

                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




    ]
)
