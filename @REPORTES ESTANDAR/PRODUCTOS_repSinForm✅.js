[


    { "$limit": 1 },

    {
        "$lookup": {
            "from": "supplies",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [



                {
                    "$project": {

                        "rgDate": "$$filtro_fecha_inicio",

                        "Producto": "$name",
                        "Tipo": {
                            "$switch": {
                                "branches": [{
                                    "case": {
                                        "$eq": ["$type", "Consumables"]
                                    },
                                    "then": "Consumibles"
                                }, {
                                    "case": {
                                        "$eq": ["$type", "Machinery"]
                                    },
                                    "then": "Maquinarias"
                                }, {
                                    "case": {
                                        "$eq": ["$type", "Tools"]
                                    },
                                    "then": "Herramientas"
                                }],
                                "default": "--------"
                            }
                        },
                        "Categoria": "$category",
                        "Descripcion": "$description",
                        "[Unidades]": {
                            "$switch": {
                                "branches": [{
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "mts"]
                                    },
                                    "then": "Metros"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "km"]
                                    },
                                    "then": "Kilometros"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cm"]
                                    },
                                    "then": "Centimetros"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "mile"]
                                    },
                                    "then": "Millas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "yard"]
                                    },
                                    "then": "Yardas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "foot"]
                                    },
                                    "then": "Pies"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "inch"]
                                    },
                                    "then": "Pulgadas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "kg"]
                                    },
                                    "then": "Kilogramos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "gr"]
                                    },
                                    "then": "Gramos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "mg"]
                                    },
                                    "then": "Miligramos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "US/ton"]
                                    },
                                    "then": "Toneladas estadounidenses"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "ton"]
                                    },
                                    "then": "Toneladas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "oz"]
                                    },
                                    "then": "Onzas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "lb"]
                                    },
                                    "then": "Libras"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "lts"]
                                    },
                                    "then": "Litros"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "US/galon"]
                                    },
                                    "then": "Galones estadounidenses"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "galon"]
                                    },
                                    "then": "Galones"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cf"]
                                    },
                                    "then": "Pies cúbicos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "ci"]
                                    },
                                    "then": "Pulgadas cúbicas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cuc"]
                                    },
                                    "then": "Centimetros cúbicos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cum"]
                                    },
                                    "then": "Metros cúbicos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "packages"]
                                    },
                                    "then": "Bultos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "package"]
                                    },
                                    "then": "Bultos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "bags"]
                                    },
                                    "then": "Bolsas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "sacks"]
                                    },
                                    "then": "Sacos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "yemas"]
                                    },
                                    "then": "Yemas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "bun"]
                                    },
                                    "then": "Factura"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cargo"]
                                    },
                                    "then": "Flete"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "manege"]
                                    },
                                    "then": "Picadero"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "hr"]
                                    },
                                    "then": "Hora"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "qty"]
                                    },
                                    "then": "Por cantidad"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "hectares"]
                                    },
                                    "then": "Hectáreas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "blocks"]
                                    },
                                    "then": "Cuadras"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "dustbin"]
                                    },
                                    "then": "Canecas"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "bunch"]
                                    },
                                    "then": "Racimos"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "cubic-meter"]
                                    },
                                    "then": "Metro cúbico"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "metro-line"]
                                    },
                                    "then": "Metro Lineal"
                                }, {
                                    "case": {
                                        "$eq": ["$inputMeasureEquivalence", "square-meter"]
                                    },
                                    "then": "Metro cuadrado"
                                }],
                                "default": "--------"
                            }
                        },
                        "pc": "$pc",
                        "pr": "$pr",
                        "Necesita agua": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$water", true]
                                },
                                "then": "Si",
                                "else": "No"
                            }
                        },

                        "Ingredientes activos": {
                            "$cond": {
                                "if": {
                                    "$isArray": ["$activeIngredients"]
                                },
                                "then": {
                                    "$reduce": {
                                        "input": "$activeIngredients",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [{
                                                        "$indexOfArray": ["$activeIngredients", "$$this"]
                                                    }, 0]
                                                },
                                                "then": {
                                                    "$concat": ["$$value", "$$this"]
                                                },
                                                "else": {
                                                    "$concat": ["$$value", " ; ", "$$this"]
                                                }
                                            }
                                        }
                                    }
                                },
                                "else": ""
                            }
                        },
                        "Cuenta contable": "$puc",
                        "SKU": "$sku"
                    }
                }
                , {
                    "$sort": {
                        "Producto": 1
                    }
                }

            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }




]
