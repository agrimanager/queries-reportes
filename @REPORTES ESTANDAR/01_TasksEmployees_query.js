const luxon = require( 'luxon' );
var filter_dates_reports = require('../querys/filterDates');
var {switch_translate_measure_type_units_es, switch_translate_supply_type_es, switch_translate_measure_type_es, switch_translate_crop_type_es, switch_translate_feature_type_es} = require('../controllers/helpers/mongoTranslate');
var ObjectID = require('mongodb').ObjectID;


// filter_dates : fecha_inicio , fecha_fin, farm
function getQuery(filter_dates) {
    let start = new Date( filter_dates.start );
    let end = new Date( filter_dates.finish );
    start = luxon.DateTime.fromJSDate( start ).startOf('day');
    end = luxon.DateTime.fromJSDate( end ).endOf('day');
    var query = [

        //-- Filtrar finca
        // filter_dates_reports("", filter_dates),
        {$match: { $and:[ {"farm": new ObjectID(filter_dates.farm)} ,  { "when.start":{ $gte: start.toUTC() } }, { "when.finish":{ $lte: end.toUTC() } }  ] } },
        // {$match: {farm: {$ne: ""}}},

        // //------------------------arreglo crop y lots
        {
            "$addFields": {
                "crop": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$crop" }, "object"] },
                        "then": [],
                        "else": "$crop"
                    }
                },
                "lots": {
                    "$cond": {
                        "if": { "$eq": ["$lots" , {}] },
                        "then": [],
                        "else": "$lots"
                    }
                }
            }
        },

        //---arreglo no mostrar labores borradas
        {
            $match: {
                "active": true //"deleteUser":{$exists:true}
            }
        },

        //-- JOIN user (timezone)
        //------------------------------
        {
            $lookup: {
                from: "users",
                localField: "uid",
                foreignField: "_id",
                as: "user_timezone"
            }
        },
        {"$unwind": "$user_timezone"},
        {
            "$addFields": {
                "user_timezone": "$user_timezone.timezone"
            }
        },
        //------------------------------

        {
            $lookup: {
                from: "activities",
                localField: "activity",
                foreignField: "_id",
                as: "activity"
            }
        },
        {"$unwind": "$activity"},
        //-- JOIN centersCosts
        {
            $lookup: {
                from: "costsCenters",
                localField: "ccid",
                foreignField: "_id",
                as: "costsCenter"
            }
        },
        {"$unwind": "$costsCenter"},
        //-- JOIN farm
        {
            $lookup: {
                from: "farms",
                localField: "farm",
                foreignField: "_id",
                as: "farm"
            }
        },
        {"$unwind": "$farm"},


        //-- JOIN employees ( supervisor)
        {
            $lookup: {
                from: "employees",
                localField: "supervisor",
                foreignField: "_id",
                as: "supervisor"
            }
        },
        //{"$unwind": "$supervisor"},
        {
            "$unwind": {
                path: "$supervisor",
                preserveNullAndEmptyArrays: true
            }
        },


        //-------- Empleado y #productividad
        // Array con mas de 20 se vuelve OBJETO
        //..comprobar si es array u objeto
        {
            $addFields: {

                "employees": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$employees"}, "array"]
                        },
                        then: "$employees",
                        else: {
                            $map: {
                                input: {$objectToArray: "$employees"},
                                as: "employeeKV",
                                in: "$$employeeKV.v"
                            }
                        }
                    }
                },
                "productivityReport": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$productivityReport"}, "array"]
                        },
                        then: "$productivityReport",
                        else: {
                            $map: {
                                input: {$objectToArray: "$productivityReport"},
                                as: "productivityReportKV",
                                in: "$$productivityReportKV.v"
                            }
                        }
                    }
                },
                "lots": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$lots"}, "array"]
                        },
                        then: "$lots",
                        else: {
                            $map: {
                                input: {$objectToArray: "$lots"},
                                as: "lotKV",
                                in: "$$lotKV.v"
                            }
                        }
                    }
                }
            }
        },

        //-- total de productos y empelados SELECCIONADOS a la labor
        {
            "$addFields": {
                // "total_empleados_seleccionados_labor": {$size: "$employees"},
                "total_productos_seleccionados_labor": {$size: "$supplies"}
            }
        },

        //--- Converitir de ($supplies) _id strings en objectId
        {
            '$unwind': {
                'path': '$productivityReport',
                'includeArrayIndex': 'arrayIndex',
                'preserveNullAndEmptyArrays': false
            }
        },
        {
            "$addFields": {
                "employees": {
                    "$toObjectId": "$productivityReport.employee"
                }
            }
        },

      /*
        {
            "$addFields": {
                "productivityReport": {
                    "$map": {
                        "input": "$productivityReport",
                        "as": "item",
                        "in": {
                            "$mergeObjects": [
                                "$$item",
                                {
                                    "employee": {"$toObjectId": "$$item.employee"}
                                }
                            ]
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "productivityReport": {
                    "$filter": {
                        "input": "$productivityReport",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.employee", "$employees"]
                        }
                    }
                }
            }
        },
        */

        {
            "$unwind": {
                path: "$productivityReport",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            "$addFields": {
                "ProductividadEmpleado": {$toDouble: "$productivityReport.quantity"}
            }
        },

        { "$addFields": { "Horas Empleado": { "$ifNull": ["$productivityReport.hours", 0] } } },


        //-- JOIN employees ( empleados de labor)
        {
            "$lookup": {
                "from": "employees",
                "localField": "employees",
                "foreignField": "_id",
                "as": "Empleado"
            }
        },
        {
            "$unwind": {
                path: "$Empleado",
                preserveNullAndEmptyArrays: true
            }
        },

        //-- calcular Total Salario
        {
            "$addFields":
                {
                    "TotalPagoEmpleado": {
                        "$multiply": [
                            {$ifNull: [{$toDouble: "$ProductividadEmpleado"}, 0]},
                            {$ifNull: [{$toDouble: "$productivityPrice.price"}, 0]},
                        ],
                    },
                }
        },


        //-- productos
        // //----------------------------------------------------------------
        {
            $addFields: {
                "productos": {
                    $cond: {
                        if: {
                            $eq: [{$type: "$supplies"}, "array"]
                        },
                        then: "$supplies",
                        else: {
                            $map: {
                                input: {$objectToArray: "$employees"},
                                as: "suppliesKV",
                                in: "$$suppliesKV.v"
                            }
                        }
                    }
                }
            }
        },


        //-- convertir id en ObjectId
        {
            "$addFields": {
                "productos": {"$map": {input: "$productos._id", as: "strid", in: {"$toObjectId": "$$strid"}}},
            }
        },

        //-- JOIN supplies ( productos de labor)
        {
            "$lookup": {
                "from": "supplies",
                "localField": "productos",
                "foreignField": "_id",
                "as": "productos"
            }
        },

        // // //----------------------------------------------------------------


        //-- arrays de strings (separados por ;)
        {
            "$addFields": {
                user: { $arrayElemAt: [ "$user.timezone", 0 ] },
                "productos": {
                    "$reduce": {
                        "input": "$productos.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },

                "Etiquetas": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$tags",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                    "then": "$$this",
                                    "else": {"$concat": ["$$value", "; ", "$$this"]}
                                }
                            }
                        }
                    }, "--sin etiquetas--"]
                },


                "Tipo_cultivo": {
                    "$reduce": {
                        "input": "$crop",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                // "then": "$$this",
                                "then": switch_translate_crop_type_es("$$this"),
                                // "else": { "$concat": ["$$value", "; ", "$$this"] }
                                "else": {"$concat": ["$$value", "; ", switch_translate_crop_type_es("$$this")]}
                            }
                        }
                    }
                },

                "Blancos_biologicos": {
                    "$reduce": {
                        "input": {
                            "$cond": {
                                "if": {"$eq": ["$biologicalTarget", ""]},
                                "then": null,
                                "else": "$biologicalTarget"
                            }
                        },
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },

                //---quitar duplicados
                "Cartografia_seleccionada_mapa_tipos": {
                    "$reduce": {
                        "input": {
                            "$reduce": {
                                "input": "$cartography.features.properties.type",
                                "initialValue": [],
                                "in": {
                                    "$cond": {
                                        "if": {"$lt": [{"$indexOfArray": ["$$value", "$$this"]}, 0]},
                                        "then": {"$concatArrays": ["$$value", ["$$this"]]},
                                        "else": "$$value"
                                    }
                                }
                            }
                        },
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                // "then": "$$this",
                                "then": switch_translate_feature_type_es("$$this"),
                                // "else": { "$concat": ["$$value", "; ", "$$this"] }
                                "else": {"$concat": ["$$value", "; ", switch_translate_feature_type_es("$$this")]}
                            }
                        }
                    }
                },

                "Cartografia_seleccionada_mapa_elementos": {
                    "$reduce": {
                        "input": "$cartography.features.properties.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                "then": "$$this",
                                "else": {"$concat": ["$$value", "; ", "$$this"]}
                            }
                        }
                    }
                },


                "Cartografia_seleccionada_lista_lotes_y_estados": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lots",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {"$eq": [{"$strLenCP": "$$value"}, 0]},
                                    "then": "$$this",
                                    "else": {"$concat": ["$$value", "; ", "$$this"]}
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },

                "lista_lots": {
                    "$map": {
                        "input": "$lots",
                        "as": "lot",
                        "in": {
                            "$split": ["$$lot", ", "]
                        }
                    }
                }


            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 0]
                        }
                    }
                },
                "lista_estados": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 1]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lista_lotes",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    //"if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "if": { "$eq": [{ "$strLenCP": {$ifNull:["$$value",""] }}, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },
                "lista_estados": {
                    $ifNull: [{
                        "$reduce": {
                            "input": "$lista_estados",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    //"if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "if": { "$eq": [{ "$strLenCP": {$ifNull:["$$value",""] }}, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin estados seleccionados--"]
                },
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "uid",
                foreignField: "_id",
                as: "user"
            }
        },
        // {
        //     $addFields:{
        //         "Fecha inicio": { $dateToString: { date: {$max: '$when.start'}, format: "%d-%m-%Y %H:%M:%S", timezone: "$user" } },
        //         "Fecha fin": { $dateToString: { date: {$max: '$when.finish'}, format: "%d-%m-%Y %H:%M:%S", timezone: "$user" } },
        //     }
        // },
        //-- SELECT valores
        {
            "$addFields": {
                // "user": "$user",
                user: { $arrayElemAt: [ "$user.timezone", 0 ] },
                "Actividad": "$activity.name",
                "Codigo Labor": "$cod",
                "Estado Labor": {
                    $switch: {
                        branches: [
                            {case: {$eq: ["$status", "To do"]}, then: "⏰ Por hacer"},
                            {case: {$eq: ["$status", "Doing"]}, then: "💪 En progreso"},
                            {case: {$eq: ["$status", "Done"]}, then: "✔ Listo"}
                        ]
                    }
                },
                "Finca": "$farm.name",

                //---CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",

                //---SIN CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                "Lista lotes": "$lista_lotes",
                "Lista estados": "$lista_estados",

                "Tipo cultivo labor": {$ifNull: ["$Tipo_cultivo", "--sin tipo cultivo--"]},//--array_string
                "Etiquetas": {
                    $cond: {
                        if: {$eq: ["$Etiquetas", ""]},
                        then: "--sin etiquetas--",
                        else: "$Etiquetas"
                    }
                },//--array_string


                "Centro de costos": "$costsCenter.name",
                // productividad
                "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                // "[Unidad] Medida de Actividad": "$productivityPrice.measure",//TRADUCIR
                "[Unidad] Medida de Actividad": switch_translate_measure_type_units_es("$productivityPrice.measure"),
                "($) Precio x unidad de Actividad": "$productivityPrice.price",

                "Semana de inicio labor": { $sum: [ { $week: { $arrayElemAt: [ "$when.start", 0 ] } }, 1 ] },
                "Semana de registro labor": { $sum: [ { $week: "$rgDate" }, 1 ] },
                "Fecha inicio": {$max: '$when.start'}, //{$max: '$when.start'},
                "Fecha fin": {$max: '$when.finish'}, //{$max: '$when.finish'},

                // strings
                "Blancos biologicos de labor": {$ifNull: ["$Blancos_biologicos", "--sin blancos biologicos--"]},//--array_string
                "Observaciones": "$observation",


                // empleados
                // "(#) Empleados asignados": "$planningEmployees",
                "Nombre de Empleado": {$ifNull: [{$concat: ["$Empleado.firstName", " ", "$Empleado.lastName"]}, "--sin empleados--"]},
                "Identificacion": {$ifNull: ["$Empleado.numberID", "--"]},
                "(#) Productividad de Empleado": {$ifNull: ["$ProductividadEmpleado", 0]},
                "($) Total Pago Empleado": "$TotalPagoEmpleado",
                "Firma de empleado": "* ",
                "Aviso legal": "* ",

                "Supervisor": {$ifNull: [{$concat: ["$supervisor.firstName", " ", "$supervisor.lastName"]}, "--sin supervisor--"]},

                "(#) Productos asignados": "$total_productos_seleccionados_labor",
                "Productos asignados": {
                    $cond: {
                        if: {$eq: ["$productos", ""]},
                        then: "--sin productos--",
                        else: "$productos"
                    }
                },//--array_string


                "(#) Empleados asignados": "$total_empleados_seleccionados_labor"
            }
        },
        //{$sort: {"Codigo Labor": 1}},
        // //--Filtro de fechas
        // filter_dates_reports("Fecha inicio", filter_dates),
        //-- JOIN activities
        {
            "$project": {
                // "user": "$user",
                "Actividad": 1,
                "Codigo Labor": 1,
                "Estado Labor": 1,
                "Finca":1,

                //---CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "tipos Cartografia": 1,
                "elementos Cartografia": 1,

                //---SIN CARTOGRAFIA (SELECCIONADA)//features seleccionados
                "Lista lotes-estado":1,
                "Lista lotes": 1,
                "Lista estados": 1,

                "Tipo cultivo labor": 1,//--array_string


                "Centro de costos": 1,
                // productividad
                "(#) Productividad esperada de Labor": 1,
                // "[Unidad] Medida de Actividad": "$productivityPrice.measure",//TRADUCIR
                "[Unidad] Medida de Actividad": 1,
                "($) Precio x unidad de Actividad": 1,

                "Semana de inicio labor": 1,
                "Semana de registro labor": 1,
                //"Fecha inicio": { $dateToString: { date: "$Fecha inicio", format: "%d-%m-%Y %H:%M:%S", timezone: "$user" } }, //{$max: '$when.start'},
                //"Fecha fin": { $dateToString: { date: "$Fecha fin", format: "%d-%m-%Y %H:%M:%S", timezone: "$user" } }, //{$max: '$when.finish'},
                // "Fecha inicio": {$max: '$when.start'},
                // "Fecha fin": {$max: '$when.finish'},
                //-----fechas
                //--------------------------------------
                "Fecha inicio": {
                    "$dateToString": {
                        "date": {
                            "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }]
                        },
                        "format": "%Y-%m-%d %H:%M",
                        //"timezone": "-0500"
                        //"timezone":"America/Bogota"
                        "timezone": "$user_timezone"
                    }
                },
                "Fecha fin": {
                    "$dateToString": {
                        "date": {
                            "$arrayElemAt": ["$when.finish", { "$subtract": [{ "$size": "$when.finish" }, 1] }]
                        },
                        "format": "%Y-%m-%d %H:%M",
                        //"timezone": "-0500"
                        // "timezone":"America/Bogota"
                        "timezone": "$user_timezone"
                    }
                },
                //--------------------------------------

                // strings
                "Blancos biologicos de labor": 1,//--array_string
                "Observaciones": 1,


                // empleados
                // "(#) Empleados asignados": "$planningEmployees",
                "Nombre de Empleado": 1,
                "Identificacion": 1,
                "(#) Productividad de Empleado": 1,
                "Horas Empleado": 1,//new
                "($) Total Pago Empleado": 1,
                "Firma de empleado": 1,
                "Aviso legal": 1,

                "Supervisor": 1,

                "(#) Productos asignados": 1,
                "Productos asignados": 1,//--array_string


                "(#) Empleados asignados": 1


                //---nuevo 2021-11-09
                // ,"Usuario Logueado": { "$ifNull": ["$userRg", "--sin Usuario Logueado--"] }
                ,"Usuario Logueado": { "$ifNull": ["$userUpd", "--sin Usuario Logueado--"] }
            }
        },


        //------------------------new arreglo HORAS
        {
            "$addFields": {
                "Horas Empleado": {
                    "$cond": {
                        // "if": { "$eq": [{ "$type": "$Horas Empleado" }, 10] },
                        "if": { "$eq": ["$Horas Empleado", NaN] },
                        "then": 0,
                        "else": "$Horas Empleado"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "INDICADOR_HORAS": {
                    "$cond": {
                        "if": { "$eq": ["$Horas Empleado", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$(#) Productividad de Empleado",
                                "$Horas Empleado"]
                        }
                    }
                }
            }
        }

    ];

    console.log("----QUERY LABORES x EMPLEADOS-----");
    console.log(JSON.stringify(query));
    console.log("----------------------");

    return query;
}

module.exports = getQuery;
