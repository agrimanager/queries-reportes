//--Labores por empleado
db.tasks.aggregate(
    [

        {
            "$lookup": {
                "from": "activities",
                "localField": "activity",
                "foreignField": "_id",
                "as": "activity"
            }
        }, {
            "$unwind": "$activity"
        }, {
            "$lookup": {
                "from": "costsCenters",
                "localField": "ccid",
                "foreignField": "_id",
                "as": "costsCenter"
            }
        }, {
            "$unwind": "$costsCenter"
        }, {
            "$lookup": {
                "from": "farms",
                "localField": "farm",
                "foreignField": "_id",
                "as": "farm"
            }
        }, {
            "$unwind": "$farm"
        }, {
            "$lookup": {
                "from": "employees",
                "localField": "supervisor",
                "foreignField": "_id",
                "as": "supervisor"
            }
        }, {
            "$unwind": {
                "path": "$supervisor",
                "preserveNullAndEmptyArrays": true
            }
        }, {
            "$addFields": {
                "employees": {
                    "$cond": {
                        "if": {
                            "$eq": [{
                                "$type": "$employees"
                            }, "array"]
                        },
                        "then": "$employees",
                        "else": {
                            "$map": {
                                "input": {
                                    "$objectToArray": "$employees"
                                },
                                "as": "employeeKV",
                                "in": "$$employeeKV.v"
                            }
                        }
                    }
                },
                "productivityReport": {
                    "$cond": {
                        "if": {
                            "$eq": [{
                                "$type": "$productivityReport"
                            }, "array"]
                        },
                        "then": "$productivityReport",
                        "else": {
                            "$map": {
                                "input": {
                                    "$objectToArray": "$productivityReport"
                                },
                                "as": "productivityReportKV",
                                "in": "$$productivityReportKV.v"
                            }
                        }
                    }
                },
                "lots": {
                    "$cond": {
                        "if": {
                            "$eq": [{
                                "$type": "$lots"
                            }, "array"]
                        },
                        "then": "$lots",
                        "else": {
                            "$map": {
                                "input": {
                                    "$objectToArray": "$lots"
                                },
                                "as": "lotKV",
                                "in": "$$lotKV.v"
                            }
                        }
                    }
                }
            }
        }, {
            "$addFields": {
                "total_productos_seleccionados_labor": {
                    "$size": "$supplies"
                }
            }
        }, {
            "$unwind": {
                "path": "$employees",
                "includeArrayIndex": "arrayIndex",
                "preserveNullAndEmptyArrays": false
            }
        }, {
            "$addFields": {
                "employees": {
                    "$toObjectId": "$employees"
                }
            }
        }, {
            "$addFields": {
                "productivityReport": {
                    "$map": {
                        "input": "$productivityReport",
                        "as": "item",
                        "in": {
                            "$mergeObjects": ["$$item", {
                                "employee": {
                                    "$toObjectId": "$$item.employee"
                                }
                            }]
                        }
                    }
                }
            }
        }, {
            "$addFields": {
                "productivityReport": {
                    "$filter": {
                        "input": "$productivityReport",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.employee", "$employees"]
                        }
                    }
                }
            }
        }, {
            "$unwind": {
                "path": "$productivityReport",
                "preserveNullAndEmptyArrays": true
            }
        }, {
            "$addFields": {
                "ProductividadEmpleado": {
                    "$toDouble": "$productivityReport.quantity"
                }
            }
        }, {
            "$lookup": {
                "from": "employees",
                "localField": "employees",
                "foreignField": "_id",
                "as": "Empleado"
            }
        }, {
            "$unwind": {
                "path": "$Empleado",
                "preserveNullAndEmptyArrays": true
            }
        }, {
            "$addFields": {
                "TotalPagoEmpleado": {
                    "$multiply": [{
                        "$ifNull": [{
                            "$toDouble": "$ProductividadEmpleado"
                        }, 0]
                    }, {
                        "$ifNull": [{
                            "$toDouble": "$productivityPrice.price"
                        }, 0]
                    }]
                }
            }
        }, {
            "$addFields": {
                "productos": {
                    "$cond": {
                        "if": {
                            "$eq": [{
                                "$type": "$supplies"
                            }, "array"]
                        },
                        "then": "$supplies",
                        "else": {
                            "$map": {
                                "input": {
                                    "$objectToArray": "$employees"
                                },
                                "as": "suppliesKV",
                                "in": "$$suppliesKV.v"
                            }
                        }
                    }
                }
            }
        }, {
            "$addFields": {
                "productos": {
                    "$map": {
                        "input": "$productos._id",
                        "as": "strid",
                        "in": {
                            "$toObjectId": "$$strid"
                        }
                    }
                }
            }
        }, {
            "$lookup": {
                "from": "supplies",
                "localField": "productos",
                "foreignField": "_id",
                "as": "productos"
            }
        }, {
            "$addFields": {
                "user": {
                    "$arrayElemAt": ["$user.timezone", 0]
                },
                "productos": {
                    "$reduce": {
                        "input": "$productos.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$strLenCP": "$$value"
                                    }, 0]
                                },
                                "then": "$$this",
                                "else": {
                                    "$concat": ["$$value", "; ", "$$this"]
                                }
                            }
                        }
                    }
                },
                "Etiquetas": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$tags",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$strLenCP": "$$value"
                                        }, 0]
                                    },
                                    "then": "$$this",
                                    "else": {
                                        "$concat": ["$$value", "; ", "$$this"]
                                    }
                                }
                            }
                        }
                    }, "--sin etiquetas--"]
                },
                "Tipo_cultivo": {
                    "$reduce": {
                        "input": "$crop",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$strLenCP": "$$value"
                                    }, 0]
                                },
                                "then": {
                                    "$switch": {
                                        "branches": [{
                                            "case": {
                                                "$eq": ["$$this", "coffee"]
                                            },
                                            "then": "Café"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "banana"]
                                            },
                                            "then": "Banana"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "avocado"]
                                            },
                                            "then": "Aguacate"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "orange"]
                                            },
                                            "then": "Naranja"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "tangerine"]
                                            },
                                            "then": "Mandarina"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "lemon"]
                                            },
                                            "then": "Limón"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "cacao"]
                                            },
                                            "then": "Cacao"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "hight-density-fruit"]
                                            },
                                            "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "vegetable"]
                                            },
                                            "then": "Hortalizas"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "tropical-fruit"]
                                            },
                                            "then": "Frutos tropicales"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "palm"]
                                            },
                                            "then": "Palma"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "flower"]
                                            },
                                            "then": "Flores"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "cereal"]
                                            },
                                            "then": "Cereales"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "cattle"]
                                            },
                                            "then": "Ganado"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "pork"]
                                            },
                                            "then": "Cerdos"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "apiculture"]
                                            },
                                            "then": "Apicultura"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "other-birds"]
                                            },
                                            "then": "Otras aves"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "chicken"]
                                            },
                                            "then": "Pollos"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "aquatic-animals"]
                                            },
                                            "then": "Animales acuáticos"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "other-animals"]
                                            },
                                            "then": "Otros animales"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "woods"]
                                            },
                                            "then": "Bosques"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "greenhouses"]
                                            },
                                            "then": "Invernaderos"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "other-(non-limiting)"]
                                            },
                                            "then": "Otros (no limitativo)"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "platano"]
                                            },
                                            "then": "Platano"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "yucca"]
                                            },
                                            "then": "Yuca"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "pasilla"]
                                            },
                                            "then": "Pasilla"
                                        }],
                                        "default": null
                                    }
                                },
                                "else": {
                                    "$concat": ["$$value", "; ", {
                                        "$switch": {
                                            "branches": [{
                                                "case": {
                                                    "$eq": ["$$this", "coffee"]
                                                },
                                                "then": "Café"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "banana"]
                                                },
                                                "then": "Banana"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "avocado"]
                                                },
                                                "then": "Aguacate"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "orange"]
                                                },
                                                "then": "Naranja"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "tangerine"]
                                                },
                                                "then": "Mandarina"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "lemon"]
                                                },
                                                "then": "Limón"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "cacao"]
                                                },
                                                "then": "Cacao"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "hight-density-fruit"]
                                                },
                                                "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "vegetable"]
                                                },
                                                "then": "Hortalizas"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "tropical-fruit"]
                                                },
                                                "then": "Frutos tropicales"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "palm"]
                                                },
                                                "then": "Palma"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "flower"]
                                                },
                                                "then": "Flores"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "cereal"]
                                                },
                                                "then": "Cereales"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "cattle"]
                                                },
                                                "then": "Ganado"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "pork"]
                                                },
                                                "then": "Cerdos"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "apiculture"]
                                                },
                                                "then": "Apicultura"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "other-birds"]
                                                },
                                                "then": "Otras aves"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "chicken"]
                                                },
                                                "then": "Pollos"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "aquatic-animals"]
                                                },
                                                "then": "Animales acuáticos"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "other-animals"]
                                                },
                                                "then": "Otros animales"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "woods"]
                                                },
                                                "then": "Bosques"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "greenhouses"]
                                                },
                                                "then": "Invernaderos"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "other-(non-limiting)"]
                                                },
                                                "then": "Otros (no limitativo)"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "platano"]
                                                },
                                                "then": "Platano"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "yucca"]
                                                },
                                                "then": "Yuca"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "pasilla"]
                                                },
                                                "then": "Pasilla"
                                            }],
                                            "default": null
                                        }
                                    }]
                                }
                            }
                        }
                    }
                },
                "Blancos_biologicos": {
                    "$reduce": {
                        "input": "$biologicalTarget",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$strLenCP": "$$value"
                                    }, 0]
                                },
                                "then": "$$this",
                                "else": {
                                    "$concat": ["$$value", "; ", "$$this"]
                                }
                            }
                        }
                    }
                },
                "Cartografia_seleccionada_mapa_tipos": {
                    "$reduce": {
                        "input": {
                            "$reduce": {
                                "input": "$cartography.features.properties.type",
                                "initialValue": [],
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$lt": [{
                                                "$indexOfArray": ["$$value", "$$this"]
                                            }, 0]
                                        },
                                        "then": {
                                            "$concatArrays": ["$$value", ["$$this"]]
                                        },
                                        "else": "$$value"
                                    }
                                }
                            }
                        },
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$strLenCP": "$$value"
                                    }, 0]
                                },
                                "then": {
                                    "$switch": {
                                        "branches": [{
                                            "case": {
                                                "$eq": ["$$this", "blocks"]
                                            },
                                            "then": "Bloque"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "lot"]
                                            },
                                            "then": "Lotes"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "lines"]
                                            },
                                            "then": "Linea"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "trees"]
                                            },
                                            "then": "Árboles"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "fruitCenters"]
                                            },
                                            "then": "Centro frutero"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "samplingPolygons"]
                                            },
                                            "then": "Poligono de muestreo"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "valves"]
                                            },
                                            "then": "Valvula"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "drainages"]
                                            },
                                            "then": "Drenaje"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "sprinklers"]
                                            },
                                            "then": "Aspersors"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "irrigationNetworkOne"]
                                            },
                                            "then": "Red de riego uno"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "irrigationNetworkTwo"]
                                            },
                                            "then": "Red de riego dos"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "irrigationNetworkThree"]
                                            },
                                            "then": "Red de riego tres"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "traps"]
                                            },
                                            "then": "Trampa"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "lanes"]
                                            },
                                            "then": "Vías"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "woods"]
                                            },
                                            "then": "Bosque"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "sensors"]
                                            },
                                            "then": "Sensor"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "cableways"]
                                            },
                                            "then": "Cable vía"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "buildings"]
                                            },
                                            "then": "Edificio"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "waterBodies"]
                                            },
                                            "then": "Cuerpo de agua"
                                        }, {
                                            "case": {
                                                "$eq": ["$$this", "additionalPolygons"]
                                            },
                                            "then": "Poligonos adicionales"
                                        }],
                                        "default": "--------"
                                    }
                                },
                                "else": {
                                    "$concat": ["$$value", "; ", {
                                        "$switch": {
                                            "branches": [{
                                                "case": {
                                                    "$eq": ["$$this", "blocks"]
                                                },
                                                "then": "Bloque"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "lot"]
                                                },
                                                "then": "Lotes"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "lines"]
                                                },
                                                "then": "Linea"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "trees"]
                                                },
                                                "then": "Árboles"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "fruitCenters"]
                                                },
                                                "then": "Centro frutero"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "samplingPolygons"]
                                                },
                                                "then": "Poligono de muestreo"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "valves"]
                                                },
                                                "then": "Valvula"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "drainages"]
                                                },
                                                "then": "Drenaje"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "sprinklers"]
                                                },
                                                "then": "Aspersors"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "irrigationNetworkOne"]
                                                },
                                                "then": "Red de riego uno"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "irrigationNetworkTwo"]
                                                },
                                                "then": "Red de riego dos"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "irrigationNetworkThree"]
                                                },
                                                "then": "Red de riego tres"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "traps"]
                                                },
                                                "then": "Trampa"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "lanes"]
                                                },
                                                "then": "Vías"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "woods"]
                                                },
                                                "then": "Bosque"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "sensors"]
                                                },
                                                "then": "Sensor"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "cableways"]
                                                },
                                                "then": "Cable vía"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "buildings"]
                                                },
                                                "then": "Edificio"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "waterBodies"]
                                                },
                                                "then": "Cuerpo de agua"
                                            }, {
                                                "case": {
                                                    "$eq": ["$$this", "additionalPolygons"]
                                                },
                                                "then": "Poligonos adicionales"
                                            }],
                                            "default": "--------"
                                        }
                                    }]
                                }
                            }
                        }
                    }
                },
                "Cartografia_seleccionada_mapa_elementos": {
                    "$reduce": {
                        "input": "$cartography.features.properties.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [{
                                        "$strLenCP": "$$value"
                                    }, 0]
                                },
                                "then": "$$this",
                                "else": {
                                    "$concat": ["$$value", "; ", "$$this"]
                                }
                            }
                        }
                    }
                },
                "Cartografia_seleccionada_lista_lotes_y_estados": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lots",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$strLenCP": "$$value"
                                        }, 0]
                                    },
                                    "then": "$$this",
                                    "else": {
                                        "$concat": ["$$value", "; ", "$$this"]
                                    }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },
                "lista_lots": {
                    "$map": {
                        "input": "$lots",
                        "as": "lot",
                        "in": {
                            "$split": ["$$lot", ", "]
                        }
                    }
                }
            }
        }, {
            "$addFields": {
                "lista_lotes": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 0]
                        }
                    }
                },
                "lista_estados": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 1]
                        }
                    }
                }
            }
        }, {
            "$addFields": {
                "lista_lotes": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lista_lotes",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$strLenCP": "$$value"
                                        }, 0]
                                    },
                                    "then": "$$this",
                                    "else": {
                                        "$concat": ["$$value", "; ", "$$this"]
                                    }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },
                "lista_estados": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lista_estados",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$strLenCP": "$$value"
                                        }, 0]
                                    },
                                    "then": "$$this",
                                    "else": {
                                        "$concat": ["$$value", "; ", "$$this"]
                                    }
                                }
                            }
                        }
                    }, "--sin estados seleccionados--"]
                }
            }
        }, {
            "$lookup": {
                "from": "users",
                "localField": "uid",
                "foreignField": "_id",
                "as": "user"
            }
        }, {
            "$addFields": {
                "user": {
                    "$arrayElemAt": ["$user.timezone", 0]
                },
                "Actividad": "$activity.name",
                "Codigo Labor": "$cod",
                "Estado Labor": {
                    "$switch": {
                        "branches": [{
                            "case": {
                                "$eq": ["$status", "To do"]
                            },
                            "then": "⏰ Por hacer"
                        }, {
                            "case": {
                                "$eq": ["$status", "Doing"]
                            },
                            "then": "💪 En progreso"
                        }, {
                            "case": {
                                "$eq": ["$status", "Done"]
                            },
                            "then": "✔ Listo"
                        }]
                    }
                },
                "Finca": "$farm.name",
                "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                "Lista lotes": "$lista_lotes",
                "Lista estados": "$lista_estados",
                "Tipo cultivo labor": {
                    "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"]
                },
                "Etiquetas": {
                    "$cond": {
                        "if": {
                            "$eq": ["$Etiquetas", ""]
                        },
                        "then": "--sin etiquetas--",
                        "else": "$Etiquetas"
                    }
                },
                "Centro de costos": "$costsCenter.name",
                "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                "[Unidad] Medida de Actividad": {
                    "$switch": {
                        "branches": [{
                            "case": {
                                "$eq": ["$productivityPrice.measure", "blocks"]
                            },
                            "then": "Bloque"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "lot"]
                            },
                            "then": "Lotes"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "lines"]
                            },
                            "then": "Linea"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "trees"]
                            },
                            "then": "Árboles"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "fruitCenters"]
                            },
                            "then": "Centro frutero"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "samplingPolygons"]
                            },
                            "then": "Poligono de muestreo"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "valves"]
                            },
                            "then": "Valvula"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "drainages"]
                            },
                            "then": "Drenaje"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "sprinklers"]
                            },
                            "then": "Aspersors"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"]
                            },
                            "then": "Red de riego uno"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"]
                            },
                            "then": "Red de riego dos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"]
                            },
                            "then": "Red de riego tres"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "traps"]
                            },
                            "then": "Trampa"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "lanes"]
                            },
                            "then": "Vías"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "woods"]
                            },
                            "then": "Bosque"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "sensors"]
                            },
                            "then": "Sensor"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cableways"]
                            },
                            "then": "Cable vía"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "buildings"]
                            },
                            "then": "Edificio"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "waterBodies"]
                            },
                            "then": "Cuerpo de agua"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "additionalPolygons"]
                            },
                            "then": "Poligonos adicionales"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "farming units"]
                            },
                            "then": "Unidades de cultivo (Ejemplo: Árboles)"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "wages"]
                            },
                            "then": "Jornales"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "quantity"]
                            },
                            "then": "Cantidades"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "mts"]
                            },
                            "then": "Metros"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "km"]
                            },
                            "then": "Kilometros"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cm"]
                            },
                            "then": "Centimetros"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "mile"]
                            },
                            "then": "Millas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "yard"]
                            },
                            "then": "Yardas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "foot"]
                            },
                            "then": "Pies"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "inch"]
                            },
                            "then": "Pulgadas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "kg"]
                            },
                            "then": "Kilogramos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "gr"]
                            },
                            "then": "Gramos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "mg"]
                            },
                            "then": "Miligramos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "US/ton"]
                            },
                            "then": "Toneladas estadounidenses"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "ton"]
                            },
                            "then": "Toneladas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "oz"]
                            },
                            "then": "Onzas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "lb"]
                            },
                            "then": "Libras"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "lts"]
                            },
                            "then": "Litros"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "US/galon"]
                            },
                            "then": "Galones estadounidenses"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "galon"]
                            },
                            "then": "Galones"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cf"]
                            },
                            "then": "Pies cúbicos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "ci"]
                            },
                            "then": "Pulgadas cúbicas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cuc"]
                            },
                            "then": "Centimetros cúbicos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cum"]
                            },
                            "then": "Metros cúbicos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "packages"]
                            },
                            "then": "Bultos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "bags"]
                            },
                            "then": "Bolsas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "sacks"]
                            },
                            "then": "sacks"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "yemas"]
                            },
                            "then": "Yemas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "bun"]
                            },
                            "then": "Factura"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cargo"]
                            },
                            "then": "Flete"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "manege"]
                            },
                            "then": "Picadero"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "hr"]
                            },
                            "then": "Hora"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "qty"]
                            },
                            "then": "Por cantidad"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "hectares"]
                            },
                            "then": "Hectáreas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "squares"]
                            },
                            "then": "Cuadras"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "dustbin"]
                            },
                            "then": "Canecas"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "bunch"]
                            },
                            "then": "Racimos"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "cubic-meter"]
                            },
                            "then": "Metro cúbico"
                        }, {
                            "case": {
                                "$eq": ["$productivityPrice.measure", "metro-line"]
                            },
                            "then": "Metro Lineal"
                        }],
                        "default": "--------"
                    }
                },
                "($) Precio x unidad de Actividad": "$productivityPrice.price",
                "Semana de inicio labor": {
                    "$sum": [{
                        "$week": {
                            "$arrayElemAt": ["$when.start", 0]
                        }
                    }, 1]
                },
                "Semana de registro labor": {
                    "$sum": [{
                        "$week": "$rgDate"
                    }, 1]
                },
                "Fecha inicio": {
                    "$max": "$when.start"
                },
                "Fecha fin": {
                    "$max": "$when.finish"
                },
                "Blancos biologicos de labor": {
                    "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"]
                },
                "Observaciones": "$observation",
                "Nombre de Empleado": {
                    "$ifNull": [{
                        "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"]
                    }, "--sin empleados--"]
                },
                "Identificacion": {
                    "$ifNull": ["$Empleado.numberID", "--"]
                },
                "(#) Productividad de Empleado": {
                    "$ifNull": ["$ProductividadEmpleado", 0]
                },
                "($) Total Pago Empleado": "$TotalPagoEmpleado",
                "Firma de empleado": "* ",
                "Aviso legal": "* ",
                "Supervisor": {
                    "$ifNull": [{
                        "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"]
                    }, "--sin supervisor--"]
                },
                "(#) Productos asignados": "$total_productos_seleccionados_labor",
                "Productos asignados": {
                    "$cond": {
                        "if": {
                            "$eq": ["$productos", ""]
                        },
                        "then": "--sin productos--",
                        "else": "$productos"
                    }
                },
                "(#) Empleados asignados": "$total_empleados_seleccionados_labor"
            }
        }, {
            "$sort": {
                "Codigo Labor": 1
            }
        }
        , {
            "$project": {
                "Actividad": 1,
                "Codigo Labor": 1,
                "Estado Labor": 1,
                "Finca": 1,
                "tipos Cartografia": 1,
                "elementos Cartografia": 1,
                "Lista lotes-estado": 1,
                "Lista lotes": 1,
                "Lista estados": 1,
                "Tipo cultivo labor": 1,
                "Centro de costos": 1,
                "(#) Productividad esperada de Labor": 1,
                "[Unidad] Medida de Actividad": 1,
                "($) Precio x unidad de Actividad": 1,
                "Semana de inicio labor": 1,
                "Semana de registro labor": 1,
                "Fecha inicio": {
                    "$dateToString": {
                        "date": "$Fecha inicio",
                        "format": "%d-%m-%Y %H:%M:%S",
                        "timezone": "$user"
                    }
                },
                "Fecha fin": {
                    "$dateToString": {
                        "date": "$Fecha fin",
                        "format": "%d-%m-%Y %H:%M:%S",
                        "timezone": "$user"
                    }
                },
                "Blancos biologicos de labor": 1,
                "Observaciones": 1,
                "Nombre de Empleado": 1,
                "Identificacion": 1,
                "(#) Productividad de Empleado": 1,
                "($) Total Pago Empleado": 1,
                "Firma de empleado": 1,
                "Aviso legal": 1,
                "Supervisor": 1,
                "(#) Productos asignados": 1,
                "Productos asignados": 1,
                "(#) Empleados asignados": 1
            }
        }
        
        //---actualizacion 2020-08-27 (valdivia)
        ,{
            "$match":{
                "(#) Productividad de Empleado":{
                    "$gt":0
                }
            }
        }
        
        ]
    , { "allowDiskUse": true })