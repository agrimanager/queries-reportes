db.form_rotacionpotreros.aggregate(
    [




        { "$addFields": { "anio_filtro_entrada": { "$year": "$Fecha entrada" } } },
        { "$match": { "anio_filtro_entrada": { "$gt": 2000 } } },
        { "$match": { "anio_filtro_entrada": { "$lt": 3000 } } },

        { "$addFields": { "anio_filtro_salida": { "$year": "$Fecha salida" } } },
        { "$match": { "anio_filtro_salida": { "$gt": 2000 } } },
        { "$match": { "anio_filtro_salida": { "$lt": 3000 } } },



        {
            "$addFields": {
                "variable_cartografia": "$Rotacion"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "Circuito": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Circuito",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Circuito": { "$ifNull": ["$Circuito.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "potrero": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$potrero",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "potrero_oid": { "$ifNull": ["$potrero._id", null] } } },
        { "$addFields": { "potrero_area": { "$ifNull": ["$potrero.properties.custom.Area.value", 0] } } },
        { "$addFields": { "potrero_geometry": { "$ifNull": ["$potrero.geometry", {}] } } },
        { "$addFields": { "potrero": { "$ifNull": ["$potrero.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "uid": 0
                , "Point": 0
                , "Formula": 0
                , "Rotacion": 0


                , "Seleccionar potrero": 0,
                "Numero de ordeños": 0,
                "Dias descanso": 0,
                "Presencia de plagas": 0,
                "supervisor_u": 0

                , "anio_filtro_entrada": 0
                , "anio_filtro_salida": 0
            }
        }


        , {
            "$lookup": {
                "from": "form_rotacionpotreros",
                "let": {
                    "potrero_oid": "$potrero_oid"

                    , "fecha_entrada": "$Fecha entrada"
                    , "fecha_salida": "$Fecha salida"

                },
                "pipeline": [

                    { "$addFields": { "anio_filtro_entrada": { "$year": "$Fecha entrada" } } },
                    { "$match": { "anio_filtro_entrada": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro_entrada": { "$lt": 3000 } } },

                    { "$addFields": { "anio_filtro_salida": { "$year": "$Fecha salida" } } },
                    { "$match": { "anio_filtro_salida": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro_salida": { "$lt": 3000 } } },


                    { "$unwind": "$Rotacion.features" },

                    {
                        "$addFields": {
                            "feature_oid": { "$toObjectId": "$Rotacion.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$feature_oid", "$$potrero_oid"]
                            }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$lte": ["$Fecha salida", "$$fecha_entrada"]
                            }
                        }
                    }

                    , {
                        "$sort": {
                            "Fecha salida": -1
                        }
                    }
                    , {
                        "$limit": 1
                    }


                    , {
                        "$addFields": {
                            "tiene_cruce": "si"
                        }
                    }
                ],
                "as": "data_rotacion"
            }
        },

        {
            "$unwind": {
                "path": "$data_rotacion",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "tiene_salida_anterior": { "$ifNull": ["$data_rotacion.tiene_cruce", "no"] }
            }
        },


        {
            "$addFields": {
                "periodo_descanso_dias": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_salida_anterior", "no"] },
                        "then": -1,
                        "else": {
                            "$divide": [
                                { "$subtract": ["$Fecha entrada", "$data_rotacion.Fecha salida"] }
                                , 86400000]
                        }
                    }
                }
            }
        }

        , {
            "$project": {
                "data_rotacion": 0
            }
        }



        , {
            "$addFields": {
                "fecha_entrada_hora": {
                    "$hour": {
                        "date": "$Fecha entrada"
                        , "timezone": "America/Bogota"
                    }
                }
                , "fecha_salida_hora": {
                    "$hour": {
                        "date": "$Fecha salida"
                        , "timezone": "America/Bogota"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "fecha_entrada_auxiliar": {
                    "$dateFromString": {
                        "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha entrada", "timezone": "America/Bogota" } }
                        , "timezone": "America/Bogota"
                    }
                }
                , "fecha_salida_auxiliar": {
                    "$dateFromString": {
                        "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha salida", "timezone": "America/Bogota" } }
                        , "timezone": "America/Bogota"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "fecha_entrada_auxiliar": {
                    "$cond": {
                        "if": { "$lte": ["$fecha_entrada_hora", 12] },
                        "then": "$fecha_entrada_auxiliar",
                        "else": {
                            "$add": [
                                "$fecha_entrada_auxiliar",
                                43200000
                            ]
                        }
                    }
                }

                , "fecha_salida_auxiliar": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$fecha_salida_hora", 23] }
                                , { "$eq": ["$fecha_salida_hora", 0] }
                            ]
                        },
                        "then": {
                            "$add": [
                                "$fecha_salida_auxiliar",
                                86399999
                            ]
                        },
                        "else": {
                            "$add": [
                                "$fecha_salida_auxiliar",
                                43200000
                            ]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "periodo_ocupacion_dias": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$fecha_salida_hora", 23] }
                                , { "$eq": ["$fecha_salida_hora", 0] }
                            ]
                        },
                        "then": {
                            "$subtract": [{ "$add": ["$fecha_salida_auxiliar", 1] }, "$fecha_entrada_auxiliar"]
                        },
                        "else": {
                            "$subtract": ["$fecha_salida_auxiliar", "$fecha_entrada_auxiliar"]
                        }
                    }
                }
            }
        }
        , {
            "$addFields": {
                "periodo_ocupacion_dias": {
                    "$divide": ["$periodo_ocupacion_dias", 86400000]
                }
            }
        }


        , {
            "$addFields": {
                "periodo_ocupacion_ordeños": {
                    "$multiply": ["$periodo_ocupacion_dias", 2]
                }
            }
        }



        , {
            "$addFields": {
                "area_pastoreada_x_ordeño": {
                    "$cond": {
                        "if": { "$eq": ["$periodo_ocupacion_ordeños", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$potrero_area", "$periodo_ocupacion_ordeños"]
                        }
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_aforo",
                "let": {
                    "potrero_oid": "$potrero_oid"
                },
                "pipeline": [
                    { "$unwind": "$Aforo lote.features" },

                    {
                        "$addFields": {
                            "feature_oid": { "$toObjectId": "$Aforo lote.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$feature_oid", "$$potrero_oid"]
                            }
                        }
                    }

                    , {
                        "$limit": 1
                    }

                ],
                "as": "data_aforo"
            }
        },

        {
            "$unwind": {
                "path": "$data_aforo",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "aforo_peso_muestra1": { "$ifNull": [{ "$toDouble": "$data_aforo.Muestra1" }, 0] }
            }
        }

        , {
            "$project": {
                "data_aforo": 0
            }
        }

        , {
            "$addFields": {
                "oferta_forrajera_x_ordeño": {
                    "$multiply": ["$area_pastoreada_x_ordeño", "$aforo_peso_muestra1"]
                }
            }
        }



        , {
            "$addFields": {
                "vacas_en_pastoreo": 14
            }
        }

        , {
            "$addFields": {
                "oferta_forrajera_x_animal": {
                    "$cond": {
                        "if": { "$eq": ["$vacas_en_pastoreo", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$oferta_forrajera_x_ordeño", "$vacas_en_pastoreo"]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "consumo_forraje_verde_x_animal_x_ordeño": {
                    "$multiply": ["$oferta_forrajera_x_animal",
                        { "$divide": [{ "$subtract": [100, { "$toDouble": "$Porcentaje desperdicio" }] }, 100] }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "consumo_forraje_verde_x_animal_x_dia": {
                    "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 2]
                }
            }
        }


        , {
            "$project": {
                // "potrero_oid": 0, //---reporte mapa
                 "uDate": 0
                , "fecha_entrada_hora": 0
                , "fecha_salida_hora": 0
            }
        }

        , {
            "$addFields": {
                "fecha_entrada_auxiliar_txt": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$fecha_entrada_auxiliar", "timezone": "America/Bogota" } }
                , "fecha_salida_auxiliar_txt": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$fecha_salida_auxiliar", "timezone": "America/Bogota" } }
            }
        }


        , {
            "$addFields": {
                "area_pastoreada_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$area_pastoreada_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$area_pastoreada_x_ordeño", 100] }, 1] }] }, 100] }
                , "oferta_forrajera_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$oferta_forrajera_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$oferta_forrajera_x_ordeño", 100] }, 1] }] }, 100] }
                , "oferta_forrajera_x_animal": { "$divide": [{ "$subtract": [{ "$multiply": ["$oferta_forrajera_x_animal", 100] }, { "$mod": [{ "$multiply": ["$oferta_forrajera_x_animal", 100] }, 1] }] }, 100] }
                , "consumo_forraje_verde_x_animal_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 100] }, 1] }] }, 100] }
                , "consumo_forraje_verde_x_animal_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_dia", 100] }, { "$mod": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_dia", 100] }, 1] }] }, 100] }
            }
        }




        //agrupacion con PROMEDIO
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "circuito": "$Circuito",
                    "potrero": "$potrero",
                    "potrero_geometry": "$potrero_geometry"

                    ,"elemnq": "$potrero_oid"
                    ,"idform": "$idform"

                }
                , "prom_oferta_forrajera_x_animal": { "$avg": "$oferta_forrajera_x_animal" }
                , "prom_consumo_forraje_verde_x_animal_x_dia": { "$avg": "$consumo_forraje_verde_x_animal_x_dia" }
                , "data": { "$push": "$$ROOT" }
            }
        }


        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$prom_oferta_forrajera_x_animal", 100] }, { "$mod": [{ "$multiply": ["$prom_oferta_forrajera_x_animal", 100] }, 1] }] }, 100] }
            }
        }



        //---rangos
        //Rango de Kg ofertados-----------oferta_forrajera_x_animal---50-55 Kg 56-60Kg 61-90Kg >91Kg


        //Rango de consumo de FV----------consumo_forraje_verde_x_animal_x_dia--------50-55 Kg 56-60Kg 61-90Kg >91Kg
        // Rojo: Consumo bajo
        // Amarillo: Consumo aceptable
        // Verde: Consumo esperado



        //PROMEDIO Kg ofertados: #ffffff,[ 0 Kg - 50 Kg ): #c65911,[ 50 Kg - 55 Kg ): #ff0000,[ 55 Kg - 60 Kg ): #ffff00,[ 60 Kg - 95 Kg ): #008000,[ >= 95 Kg ): #0000ff


        //---mapa
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lt": ["$indicador", 50] }
                                    ]
                                }
                                , "then": "#c65911"//CAFE
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 50] }
                                        , { "$lt": ["$indicador", 55] }
                                    ]
                                }
                                , "then": "#ff0000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 55] }
                                        , { "$lt": ["$indicador", 60] }
                                    ]
                                }
                                , "then": "#ffff00"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 60] }
                                        , { "$lt": ["$indicador", 90] }
                                    ]
                                }
                                , "then": "#008000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 90] }
                                    ]
                                }
                                , "then": "#0000ff"
                            },

                        ],
                        "default": "#000000"//negro
                    }
                }

                , "rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lt": ["$indicador", 50] }
                                    ]
                                }
                                , "then": "A-[ 0 Kg - 50 Kg )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 50] }
                                        , { "$lt": ["$indicador", 55] }
                                    ]
                                }
                                , "then": "B-[ 50 Kg - 55 Kg )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 55] }
                                        , { "$lt": ["$indicador", 60] }
                                    ]
                                }
                                , "then": "C-[ 55 Kg - 60 Kg )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 60] }
                                        , { "$lt": ["$indicador", 90] }
                                    ]
                                }
                                , "then": "D-[ 60 Kg - 90 Kg )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 90] }
                                    ]
                                }
                                , "then": "E-[ >= 90 Kg )"
                            },

                        ],
                        "default": "F-Otro"
                    }
                }
            }
        }

        // ----proyeccion final de mapa
        , {
            "$project": {
                "_id": "$_id.elemnq",
                "idform": "$_id.idform",

                "type": "Feature",
                "properties": {
                    "Finca": "$_id.finca",
                    "Circuito": "$_id.circuito",
                    "Potrero": "$_id.potrero",

                    "Prom Kg ofertados": { "$concat": [{ "$toString": "$indicador" }, " Kg"] },

                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color"
                },
                "geometry": { "$ifNull": ["$_id.potrero_geometry", {}] }
            }
        }




    ]

)
