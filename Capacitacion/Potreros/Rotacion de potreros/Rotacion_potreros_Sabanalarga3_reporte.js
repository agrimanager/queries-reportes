db.form_rotacionpotreros.aggregate(
    [

        // //------------------------------------------------------------------
        // //---filtros de fechas
        // {
        //     $addFields: {
        //         "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
        //         "Busqueda fin": new Date,
        //         "today": new Date
        //     }
        // },
        // //----------------------------------------------------------------

        // //----filtro de fechas
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //                 {
        //                     "$gte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
        //                         ,
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
        //                     ]
        //                 },

        //                 {
        //                     "$lte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
        //                         ,
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
        //                     ]
        //                 }
        //             ]
        //         }
        //     }
        // },
        // //----------------------------------------------------------------


        //---------condicion fechas malas
        { "$addFields": { "anio_filtro_entrada": { "$year": "$Fecha entrada" } } },
        { "$match": { "anio_filtro_entrada": { "$gt": 2000 } } },
        { "$match": { "anio_filtro_entrada": { "$lt": 3000 } } },

        { "$addFields": { "anio_filtro_salida": { "$year": "$Fecha salida" } } },
        { "$match": { "anio_filtro_salida": { "$gt": 2000 } } },
        { "$match": { "anio_filtro_salida": { "$lt": 3000 } } },



        //---------cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Rotacion" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "Circuito": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Circuito",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Circuito": { "$ifNull": ["$Circuito.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "potrero": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$potrero",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "potrero_oid": { "$ifNull": ["$potrero._id", null] } } },
        { "$addFields": { "potrero_area": { "$ifNull": ["$potrero.properties.custom.Area.value", 0] } } },
        { "$addFields": { "potrero": { "$ifNull": ["$potrero.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "uid": 0
                , "Point": 0
                , "Formula": 0
                , "Rotacion": 0 //🚩editar : 0


                , "Seleccionar potrero": 0,
                "Numero de ordeños": 0,
                "Dias descanso": 0,
                "Presencia de plagas": 0,
                "supervisor_u": 0

                , "anio_filtro_entrada": 0
                , "anio_filtro_salida": 0
            }
        }


        //---------fechas
        //--timezone_user: America/Bogota


        //***periodo_descanso = PD= FECHA DE ENTRADA- FECHA DE SALIDA ANTERIOR
        //cruzar con misma tabla
        , {
            "$lookup": {
                "from": "form_rotacionpotreros",
                "let": {

                    "potrero_oid": "$potrero_oid"

                    , "fecha_entrada": "$Fecha entrada"
                    , "fecha_salida": "$Fecha salida"

                },
                "pipeline": [

                    //---------condicion fechas malas
                    { "$addFields": { "anio_filtro_entrada": { "$year": "$Fecha entrada" } } },
                    { "$match": { "anio_filtro_entrada": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro_entrada": { "$lt": 3000 } } },

                    { "$addFields": { "anio_filtro_salida": { "$year": "$Fecha salida" } } },
                    { "$match": { "anio_filtro_salida": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro_salida": { "$lt": 3000 } } },


                    //cruzar lote
                    { "$unwind": "$Rotacion.features" },

                    {
                        "$addFields": {
                            "feature_oid": { "$toObjectId": "$Rotacion.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$feature_oid", "$$potrero_oid"]
                            }
                        }
                    }

                    //cruzar fechas
                    , {
                        "$match": {
                            "$expr": {
                                //"$gte": ["$Fecha entrada", "$$fecha_salida"]
                                "$lte": ["$Fecha salida", "$$fecha_entrada"]
                            }
                        }
                    }

                    , {
                        "$sort": {
                            // "Fecha entrada": 1
                            "Fecha salida": -1
                        }
                    }
                    , {
                        "$limit": 1
                    }


                    //---aux
                    , {
                        "$addFields": {
                            "tiene_cruce": "si"
                        }
                    }
                ],
                "as": "data_rotacion"
            }
        },

        {
            "$unwind": {
                "path": "$data_rotacion",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "tiene_salida_anterior": { "$ifNull": ["$data_rotacion.tiene_cruce", "no"] }
            }
        },


        {
            "$addFields": {
                "periodo_descanso_dias": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_salida_anterior", "no"] },
                        "then": -1,
                        "else": {
                            "$divide": [
                                { "$subtract": ["$Fecha entrada", "$data_rotacion.Fecha salida"] }
                                , 86400000]
                        }
                    }
                }
            }
        }

        , {
            "$project": {
                "data_rotacion": 0
            }
        }





        //==========>> LOGICA DE FECHAS (para contar dias)
        // //⚠️DUDA!--casos que cumplen varios casos ???

        // //---condiciones sobre Fecha salida
        // // · Cuando las fechas de salida reporta rango de horas entre 13:00 y 6:25 significa que solo permanecieron medio día (agregar 12:00).
        // // · Cuando las fechas de salida reportan horas 00:00 significa que permanecieron todo ese día (agregar 23:59).
        // //====>>SI HORA == 23 (11:59 pm) OR SI HORA == 0 (12:00 am), ENTONCES 1 DIAs, SINO 0.5 DIAs

        // //---condiciones sobre Fecha entrada
        // // · Cuando la fecha de entrada reporta rango de horas entre 6:30 y 7:00 significa que permanecieron todo ese día (agregar 23:59).
        // // · Cuando la fecha de entrada reporta rango de horas entre 15:15 y 15:46 significa que solo permanecieron medio día (agregar 12:00).
        // //====>>SI HORA < 12, ENTONCES 1 DIAs, SINO 0.5 DIAs


        //==================================================================
        //---obtener horas
        , {
            "$addFields": {
                "fecha_entrada_hora": {
                    "$hour": {
                        "date": "$Fecha entrada"
                        , "timezone": "America/Bogota"
                    }
                }
                , "fecha_salida_hora": {
                    "$hour": {
                        "date": "$Fecha salida"
                        , "timezone": "America/Bogota"
                    }
                }
            }
        }

        //---calcular fechas crudas
        , {
            "$addFields": {
                "fecha_entrada_auxiliar": {
                    "$dateFromString": {
                        "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha entrada", "timezone": "America/Bogota" } }
                        , "timezone": "America/Bogota"
                    }
                }
                , "fecha_salida_auxiliar": {
                    "$dateFromString": {
                        "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha salida", "timezone": "America/Bogota" } }
                        , "timezone": "America/Bogota"
                    }
                }
            }
        }


        //---calcular fechas_horas segun condiciones
        , {
            "$addFields": {
                "fecha_entrada_auxiliar": {
                    "$cond": {
                        "if": { "$lte": ["$fecha_entrada_hora", 12] },
                        "then": "$fecha_entrada_auxiliar", //1 dia
                        "else": {// 0.5 dia
                            "$add": [
                                "$fecha_entrada_auxiliar",
                                43200000 // 12 horas
                            ]
                        }
                    }
                }

                , "fecha_salida_auxiliar": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$fecha_salida_hora", 23] }
                                , { "$eq": ["$fecha_salida_hora", 0] }
                            ]
                        },
                        "then": {// 1 dia
                            "$add": [
                                "$fecha_salida_auxiliar",
                                86399999 // 24 horas - 1 milisegundo
                            ]
                        },
                        "else": {// 0.5 dia
                            "$add": [
                                "$fecha_salida_auxiliar",
                                43200000 // 12 horas
                            ]
                        }
                    }
                }
            }
        }
        //==================================================================



        //***periodo_ocupacion = PO (días) = FECHA DE SALIDA – FECHA DE ENTRADA
        // , {
        //     "$addFields": {
        //         "periodo_ocupacion_dias": {
        //             // "$subtract": ["$Fecha salida", "$Fecha entrada"]
        //             "$subtract": ["$fecha_salida_auxiliar", "$fecha_entrada_auxiliar"]
        //         }
        //     }
        // }
        , {
            "$addFields": {
                "periodo_ocupacion_dias": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$fecha_salida_hora", 23] }
                                , { "$eq": ["$fecha_salida_hora", 0] }
                            ]
                        },
                        "then": {
                            "$subtract": [{ "$add": ["$fecha_salida_auxiliar", 1] }, "$fecha_entrada_auxiliar"]
                        },
                        "else": {
                            "$subtract": ["$fecha_salida_auxiliar", "$fecha_entrada_auxiliar"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "periodo_ocupacion_dias": {
                    "$divide": ["$periodo_ocupacion_dias", 86400000]
                }
            }
        }

        //--test
        // , {
        //     "$addFields": {
        //         //"periodo_ocupacion_dias": { $round: "$periodo_ocupacion_dias"}
        //         "periodo_ocupacion_dias": {
        //             //$trunc: "$periodo_ocupacion_dias"
        //             $floor: "$periodo_ocupacion_dias"
        //         }
        //     }
        // }





        //***periodo_ocupacion_ordeños = PO (ordeños) = PERIODO OCUPACION (días) * 2 (ordeños)
        , {
            "$addFields": {
                "periodo_ocupacion_ordeños": {
                    "$multiply": ["$periodo_ocupacion_dias", 2]
                }
            }
        }



        //---------otros indicadores

        // -Area pastoreada por ordeño:AP (ordeños) = AREA EN M2 / PERIODO DE OCUPACION (ordeños)
        , {
            "$addFields": {
                "area_pastoreada_x_ordeño": {
                    "$cond": {
                        "if": { "$eq": ["$periodo_ocupacion_ordeños", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$potrero_area", "$periodo_ocupacion_ordeños"]
                        }
                    }
                }
            }
        }



        // -Oferta forrajera por ordeño:OF (ordeño) = AREA PASTOREADA (ordeño) * AFORO (Kg/m2)
        //---cruzar con aforo
        , {
            "$lookup": {
                "from": "form_aforo",
                "let": {
                    "potrero_oid": "$potrero_oid"
                },
                "pipeline": [
                    //cruzar lote
                    { "$unwind": "$Aforo lote.features" },

                    {
                        "$addFields": {
                            "feature_oid": { "$toObjectId": "$Aforo lote.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$feature_oid", "$$potrero_oid"]
                            }
                        }
                    }

                    // , {
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // }

                    //----OJO ACLARAR BIEN LOS DATOS A SACAR

                    , {
                        "$limit": 1
                    }

                ],
                "as": "data_aforo"
            }
        },

        //----OJO ACLARAR BIEN LOS DATOS A SACAR
        {
            "$unwind": {
                "path": "$data_aforo",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "aforo_peso_muestra1": { "$ifNull": [{ "$toDouble": "$data_aforo.Muestra1" }, 0] }
            }
        }

        , {
            "$project": {
                "data_aforo": 0
            }
        }

        , {
            "$addFields": {
                "oferta_forrajera_x_ordeño": {
                    "$multiply": ["$area_pastoreada_x_ordeño", "$aforo_peso_muestra1"]
                }
            }
        }




        // -Oferta forrajera por animal:OF (animal) = OFERTA FORRAJERA (ordeño) / VACAS EN PASTOREO
        //.....en el maestro de vacas existen 14 opciones
        // , {
        //     "$addFields": {
        //         "vacas_en_pastoreo": 14
        //     }
        // }

        //---cruzar con form_vacas
        , {
            "$lookup": {
                "from": "form_vacas",
                "let": {
                    "fecha_entrada_auxiliar": "$fecha_entrada_auxiliar"
                },
                "pipeline": [
                    {
                        "$match": {
                            "vacas": { "$ne": "" }
                        }
                    },

                    {
                        "$addFields": {
                            "cantidad_vacas": { "$size": "$vacas" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$gt": ["$$fecha_entrada_auxiliar", "$rgDate"]
                            }
                        }
                    }

                    , {
                        "$sort": {
                            "rgDate": -1
                        }
                    }
                    //----OJO ACLARAR BIEN LOS DATOS A SACAR
                    , {
                        "$limit": 1
                    }

                ],
                "as": "data_vacas"
            }
        },

        //----OJO ACLARAR BIEN LOS DATOS A SACAR
        {
            "$unwind": {
                "path": "$data_vacas",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "vacas_en_pastoreo": { "$ifNull": [{ "$toDouble": "$data_vacas.cantidad_vacas" }, 0] }
            }
        }

        , {
            "$project": {
                "data_vacas": 0
            }
        }



        , {
            "$addFields": {
                "oferta_forrajera_x_animal": {
                    "$cond": {
                        "if": { "$eq": ["$vacas_en_pastoreo", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$oferta_forrajera_x_ordeño", "$vacas_en_pastoreo"]
                        }
                    }
                }
            }
        }


        //nueva variable
        , {
            "$addFields": {
                "oferta_forrajera_x_animal_x_dia": { "$multiply": ["$oferta_forrajera_x_animal", 2] }
            }
        }


        //- Consumo de forraje verde por animal por ordeño:
        , {
            "$addFields": {
                "consumo_forraje_verde_x_animal_x_ordeño": {
                    "$multiply": ["$oferta_forrajera_x_animal",
                        { "$divide": [{ "$subtract": [100, { "$toDouble": "$Porcentaje desperdicio" }] }, 100] }
                    ]
                }
            }
        }


        //- Consumo de forraje verde por animal por dia
        , {
            "$addFields": {
                "consumo_forraje_verde_x_animal_x_dia": {
                    "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 2]
                }
            }
        }


        //agrupacion con PROMEDIO



        //---rangos
        //Rango de Kg ofertados-----------oferta_forrajera_x_animal---50-55 Kg 56-60Kg 61-90Kg >91Kg


        //Rango de consumo de FV----------consumo_forraje_verde_x_animal_x_dia--------50-55 Kg 56-60Kg 61-90Kg >91Kg
        // Rojo: Consumo bajo
        // Amarillo: Consumo aceptable
        // Verde: Consumo esperado



        , {
            "$project": {
                "potrero_oid": 0
                , "uDate": 0
                , "fecha_entrada_hora": 0
                , "fecha_salida_hora": 0
            }
        }



        //-----ULTIMOS DETALLES
        //cartografia nombres------ok
        //redondear dias-----------ok
        //fechas txt---------------ok
        , {
            "$addFields": {
                "fecha_entrada_auxiliar_txt": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$fecha_entrada_auxiliar", "timezone": "America/Bogota" } }
                , "fecha_salida_auxiliar_txt": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$fecha_salida_auxiliar", "timezone": "America/Bogota" } }
            }
        }


        //2 decimales--------------ok
        , {
            "$addFields": {
                "area_pastoreada_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$area_pastoreada_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$area_pastoreada_x_ordeño", 100] }, 1] }] }, 100] }
                , "oferta_forrajera_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$oferta_forrajera_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$oferta_forrajera_x_ordeño", 100] }, 1] }] }, 100] }
                , "oferta_forrajera_x_animal": { "$divide": [{ "$subtract": [{ "$multiply": ["$oferta_forrajera_x_animal", 100] }, { "$mod": [{ "$multiply": ["$oferta_forrajera_x_animal", 100] }, 1] }] }, 100] }
                , "oferta_forrajera_x_animal_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$oferta_forrajera_x_animal_x_dia", 100] }, { "$mod": [{ "$multiply": ["$oferta_forrajera_x_animal_x_dia", 100] }, 1] }] }, 100] }
                , "consumo_forraje_verde_x_animal_x_ordeño": { "$divide": [{ "$subtract": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 100] }, { "$mod": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_ordeño", 100] }, 1] }] }, 100] }
                , "consumo_forraje_verde_x_animal_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_dia", 100] }, { "$mod": [{ "$multiply": ["$consumo_forraje_verde_x_animal_x_dia", 100] }, 1] }] }, 100] }
            }
        }


    ]
)
