db.form_movimientodepotrero.aggregate(
    [


        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Lote" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Lote": 0
                , "Point": 0
                , "Formula": 0
                , "uid": 0
            }
        }
        
        
        //----condiciones
        
        //--fecha
        ,{ "$addFields": { "anio_filtro": { "$year": "$Fecha" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },
        
        
        
        //---agrupar
        {
            "$group":{
                "_id":{
                    "finca":"$finca",
                    "bloque":"$bloque",
                    "lote":"$lote"
                    
                    ,"tipo_movimiento":"$Tipo de movimiento de rotacion"
                    
                }
                
                ,"max_fecha": {"$max":"$Fecha"}
                ,"min_fecha": {"$min":"$Fecha"}
                
                ,"data":{"$push":"$$ROOT"}
                // ,cantidad:{$sum:1}
            }
        }
        
        
        
        
        
        //---agrupar
        ,{
            "$group":{
                "_id":{
                    "finca":"$_id.finca",
                    "bloque":"$_id.bloque",
                    "lote":"$_id.lote"
                    
                    //,"tipo_movimiento":"$Tipo de movimiento de rotacion"
                    
                }
                
                // ,"max_fecha": {"$max":"$Fecha"}
                
                ,"data":{"$push":"$$ROOT"}
                
                
                // ,cantidad:{$sum:1}
                // , "sum_cantidad_racimos": {
                //     "$sum": {
                //         "$switch": {
                //             "branches": [
                //                 {
                //                     "case": { "$eq": ["$tipo_criterio", "Racimos"] },
                //                     "then": "$cantidad"
                //                 }
                //             ],
                //             "default": 0
                //         }
                //     }
                // }
                
            }
        }
        
        
        //--fecha1
        ,{
            "$addFields": {
                "fecha_entrada_max": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Entrada"] }
                    }
                }
            }
        }
        ,{
            "$unwind": {
                "path": "$fecha_entrada_max",
                "preserveNullAndEmptyArrays": true
            }
        }
        
        ,{
            "$addFields": {
                "fecha_entrada_max": "$fecha_entrada_max.max_fecha"
            }
        }
        
        // //--fecha2
        // ,{
        //     "$addFields": {
        //         "fecha_salida_max": {
        //             "$filter": {
        //                 "input": "$data",
        //                 "as": "item_data",
        //                 "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Salida"] }
        //             }
        //         }
        //     }
        // }
        // ,{
        //     "$unwind": {
        //         "path": "$fecha_salida_max",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }
        
        // ,{
        //     "$addFields": {
        //         "fecha_salida_max": "$fecha_salida_max.max_fecha"
        //     }
        // }
        
        //--fecha2
        ,{
            "$addFields": {
                "fecha_salida_min": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Salida"] }
                    }
                }
            }
        }
        ,{
            "$unwind": {
                "path": "$fecha_salida_min",
                "preserveNullAndEmptyArrays": true
            }
        }
        
        ,{
            "$addFields": {
                "fecha_salida_min": "$fecha_salida_min.min_fecha"
            }
        }
        
        
        
        //----condicion de f1 y f2 existan
        ,{
            "$match":{
                "fecha_entrada_max":{"$exists":true},
                //"fecha_salida_max":{"$exists":true}
                "fecha_salida_min":{"$exists":true}
            }
        }
        
        //----condicion de f1 > f2
        ,{
            "$match":{
                "$expr":{
                    "$gte":["$fecha_entrada_max","$fecha_salida_min"]
                }
            }
        }
        
        
        //---calculos
        
        //---restar fechas
        ,{
            "$addFields": {
                "dias":{
                    "$subtract":["$fecha_entrada_max","$fecha_salida_min"]
                }
            }
        }
        ,{
            "$addFields": {
                "dias":{
                    "$divide":["$dias",86400000]
                }
            }
        }




    ]


)