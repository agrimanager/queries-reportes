//---mapa
db.form_movimientodepotrero.aggregate(
    [
        //-----
        {
            "$addFields": { "idform": "xxxxx form xxxx" }
        },
        //-----
        

        {
            "$addFields": { "Cartography": "$Lote" }
        },

        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },



        {
            "$addFields": {
                "Fecha": { "$ifNull": ["$Fecha", "$Fecha del movimiento"] }
            }
        },


        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "potrero": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$potrero",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "potrero": { "$ifNull": ["$potrero.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Lote": 0,
                "Point": 0,
                "Formula": 0,
                "uid": 0
            }
        },

        { "$addFields": { "anio_filtro": { "$year": "$Fecha" } } },

        { "$match": { "anio_filtro": { "$gt": 2000 } } },

        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "potrero": "$potrero",
                    "tipo_movimiento": "$Tipo de movimiento de rotacion"
                },
                "max_fecha": { "$max": "$Fecha" },
                "data": { "$push": "$$ROOT" }
            }
        },



        //---sacar variable de dias
        {
            "$addFields": {
                "dias": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data.Fecha", "$max_fecha"] }
                    }
                }
            }
        },


        {
            "$unwind": {
                "path": "$dias",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "dias": {
                    "$cond": {
                        "if": { "$eq": ["$_id.tipo_movimiento", "Entrada"] },
                        "then": "$dias.Dias de ocupacion esperados",
                        "else": "$dias.Dias de ocupacion reales"
                    }
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "potrero": "$_id.potrero"
                },
                "data": { "$push": "$$ROOT" }
            }
        },



        //---ultima fecha entrada
        {
            "$addFields": {
                "fecha_entrada_max": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Entrada"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$fecha_entrada_max",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "fecha_entrada_max": "$fecha_entrada_max.max_fecha"
            }
        },




        //---DIAS de ultima fecha entrada
        {
            "$addFields": {
                "dias_esperados": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Entrada"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$dias_esperados",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "dias_esperados": "$dias_esperados.dias"
            }
        },

        //---ultima fecha salida
        {
            "$addFields": {
                "fecha_salida_max": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Salida"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$fecha_salida_max",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "fecha_salida_max": "$fecha_salida_max.max_fecha"
            }
        },


        //---DIAS de ultima fecha salida
        {
            "$addFields": {
                "dias_reales": {
                    "$filter": {
                        "input": "$data",
                        "as": "item_data",
                        "cond": { "$eq": ["$$item_data._id.tipo_movimiento", "Salida"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$dias_reales",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "dias_reales": "$dias_reales.dias"
            }
        },




        {
            "$match": {
                "fecha_entrada_max": { "$exists": true },
                "fecha_salida_max": { "$exists": true }
            }
        },

        {
            "$match": {
                "$expr": {
                    "$lte": ["$fecha_entrada_max", "$fecha_salida_max"]
                }
            }
        },

    

        //---new
        //---mostrar datos

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            //"fecha_entrada_max": "$fecha_entrada_max",
                            //"fecha_salida_max": "$fecha_salida_max",
                            "dias_esperados": "$dias_esperados",
                            "dias_reales": "$dias_reales"

                            , "data": "$data"
                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": { "$eq": ["$dias_esperados", "$dias_reales"] },
                        "then": "DE = DR",
                        "else": {
                            "$cond": {
                                "if": { "$lt": ["$dias_esperados", "$dias_reales"] },
                                "then": "DE < DR",
                                "else": "DE > DR"
                            }
                        }
                    }
                }
            }
        },




        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$rango", "DE = DR"] },
                                "then": "#00ff00"
                            },
                            {
                                "case": { "$eq": ["$rango", "DE < DR"] },
                                "then": "#ffff00"
                            },
                            {
                                "case": { "$eq": ["$rango", "DE > DR"] },
                                "then": "#ff0000"
                            }
                        ],
                        "default": null
                    }
                }
            }
        },



        {
            "$project": {
                "_id": {
                    "$arrayElemAt": [
                        { "$arrayElemAt": ["$data.data.elemnq", { "$subtract": [{ "$size": "$data.data.elemnq" }, 1] }] }
                        , 0]
                },
                "idform": {
                    "$arrayElemAt": [
                        { "$arrayElemAt": ["$data.data.idform", { "$subtract": [{ "$size": "$data.data.idform" }, 1] }] }
                        , 0]
                },
                "type": "Feature",
                "properties": {
                    "Bloque": "$bloque",
                    "Potrero": "$potrero",
                    "rango": "$rango",
                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": [
                        { "$arrayElemAt": ["$data.data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.features.geometry" }, 1] }] }
                        , 0]
                }
            }
        }



    ]


)