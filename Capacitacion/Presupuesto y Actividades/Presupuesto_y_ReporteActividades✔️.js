[

    {
        "$lookup": {
            "from": "activities",
            "as": "data_activities",
            "let": {
                "actividad": "$Actividad a reporta"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$actividad", "$name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$data_activities",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$unwind": {
            "path": "$data_activities.ccid",
            "preserveNullAndEmptyArrays": false
        }
    },
    {
        "$addFields": {
            "ceco_oid": "$data_activities.ccid"
        }
    },


    {
        "$project": {
            "data_activities": 0
        }
    }



    , {
        "$lookup": {
            "from": "costsCenters",
            "as": "data_costsCenters",
            "let": {
                "ceco": "$ceco_oid"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$ceco", "$_id"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$data_costsCenters",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "ceco_cod": "$data_costsCenters.code"
        }
    },


    {
        "$project": {
            "data_costsCenters": 0
            , "ceco_oid": 0
        }
    }



    , {
        "$addFields": {
            "costo_mo": { "$toDouble": { "$ifNull": ["$Valor en Mano de Obra", 0] } },
            "costo_inv": { "$toDouble": { "$ifNull": ["$Valor en insumos", 0] } },
            "costo_mo_inv": {
                "$sum": [
                    { "$toDouble": { "$ifNull": ["$Valor en Mano de Obra", 0] } }
                    , { "$toDouble": { "$ifNull": ["$Valor en insumos", 0] } }
                ]
            }
        }

    }


    , {
        "$group": {
            "_id": {
                "actividad": "$Actividad a reporta"
                , "ceco_cod": "$ceco_cod"
            },
            "costo_mo": { "$sum": { "$toDouble": { "$ifNull": ["$costo_mo", 0] } } },
            "costo_inv": { "$sum": { "$toDouble": { "$ifNull": ["$costo_inv", 0] } } },
            "costo_mo_inv": { "$sum": { "$toDouble": { "$ifNull": ["$costo_mo_inv", 0] } } }

            , "cantidad de registros": { "$sum": 1 }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "costo_mo": "$costo_mo",
                        "costo_inv": "$costo_inv",
                        "costo_mo_inv": "$costo_mo_inv",
                        "cantidad de registros": "$cantidad de registros"
                    }
                ]
            }
        }
    }





    , {
        "$lookup": {
            "from": "form_presupuesto",
            "as": "data_presupuesto",
            "let": {
                "actividad": "$actividad"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$actividad", "$Actividad"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$data_presupuesto",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "ppto_mo_inv": { "$toDouble": { "$ifNull": ["$data_presupuesto.Valor total", 0] } }

            , "ppto_cliente": "$data_presupuesto.Cliente"

        }
    },


    {
        "$project": {
            "data_presupuesto": 0
        }
    }



    , {
        "$addFields": {
            "PCT_CUMPLIMIENTO": {
                "$cond": {
                    "if": { "$eq": ["$ppto_mo_inv", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{
                            "$divide": ["$costo_mo_inv",
                                "$ppto_mo_inv"]
                        }, 100]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "PCT_CUMPLIMIENTO": {
                "$divide": [
                    {
                        "$subtract": [
                            { "$multiply": ["$PCT_CUMPLIMIENTO", 100] },
                            { "$mod": [{ "$multiply": ["$PCT_CUMPLIMIENTO", 100] }, 1] }
                        ]
                    }
                    , 100
                ]
            }
        }
    }


    , {
        "$project": {
            "num credito": "$ceco_cod"
            , "cliente": "$ppto_cliente"
            , "actividad": "$actividad"
            , "presupuesto": "$ppto_mo_inv"
            , "mano de obra": "$costo_mo"
            , "insumos": "$costo_inv"
            , "total costo": "$costo_mo_inv"
            , "PCT_CUMPLIMIENTO": "$PCT_CUMPLIMIENTO"

            , "cantidad de registros": "$cantidad de registros"
        }
    }



]
