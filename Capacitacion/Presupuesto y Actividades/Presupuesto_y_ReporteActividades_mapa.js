db.form_reportedeactividad.aggregate(
    [

        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------

        //===MAPA

        {
            "$addFields": {
                "variable_cartografia": "$Cliente"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": { "elemnq": { "$toObjectId": "$variable_cartografia.features._id" } }
        },


        {
            "$project": {
                "Cliente": 0
                , "Point": 0
            }
        },



        //--------------




        {
            "$lookup": {
                "from": "activities",
                "as": "data_activities",
                "let": {
                    "actividad": "$Actividad a reporta"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$actividad", "$name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$data_activities",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$data_activities.ccid",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            "$addFields": {
                "ceco_oid": "$data_activities.ccid"
            }
        },


        {
            "$project": {
                "data_activities": 0
            }
        }



        , {
            "$lookup": {
                "from": "costsCenters",
                "as": "data_costsCenters",
                "let": {
                    "ceco": "$ceco_oid"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$ceco", "$_id"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$data_costsCenters",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "ceco_cod": "$data_costsCenters.code"
            }
        },


        {
            "$project": {
                "data_costsCenters": 0
                , "ceco_oid": 0
            }
        }



        , {
            "$addFields": {
                "costo_mo": { "$toDouble": { "$ifNull": ["$Valor en Mano de Obra", 0] } },
                "costo_inv": { "$toDouble": { "$ifNull": ["$Valor en insumos", 0] } },
                "costo_mo_inv": {
                    "$sum": [
                        { "$toDouble": { "$ifNull": ["$Valor en Mano de Obra", 0] } }
                        , { "$toDouble": { "$ifNull": ["$Valor en insumos", 0] } }
                    ]
                }
            }

        }


        , {
            "$group": {
                "_id": {
                    "actividad": "$Actividad a reporta"
                    , "ceco_cod": "$ceco_cod"
                },
                "costo_mo": { "$sum": { "$toDouble": { "$ifNull": ["$costo_mo", 0] } } },
                "costo_inv": { "$sum": { "$toDouble": { "$ifNull": ["$costo_inv", 0] } } },
                "costo_mo_inv": { "$sum": { "$toDouble": { "$ifNull": ["$costo_mo_inv", 0] } } }

                , "cantidad de registros": { "$sum": 1 }

                , "data": { "$push": "$$ROOT" } //mapa
            }
        }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {
        //                     "costo_mo": "$costo_mo",
        //                     "costo_inv": "$costo_inv",
        //                     "costo_mo_inv": "$costo_mo_inv",
        //                     "cantidad de registros": "$cantidad de registros"
        //                 }
        //             ]
        //         }
        //     }
        // }





        , {
            "$lookup": {
                "from": "form_presupuesto",
                "as": "data_presupuesto",
                "let": {
                    //"actividad": "$actividad"
                    "actividad": "$_id.actividad"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$actividad", "$Actividad"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$data_presupuesto",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "ppto_mo_inv": { "$toDouble": { "$ifNull": ["$data_presupuesto.Valor total", 0] } }

                , "ppto_cliente": "$data_presupuesto.Cliente"

            }
        },


        {
            "$project": {
                "data_presupuesto": 0
            }
        }



        , {
            "$addFields": {
                "PCT_CUMPLIMIENTO": {
                    "$cond": {
                        "if": { "$eq": ["$ppto_mo_inv", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [{
                                "$divide": ["$costo_mo_inv",
                                    "$ppto_mo_inv"]
                            }, 100]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "PCT_CUMPLIMIENTO": {
                    "$divide": [
                        {
                            "$subtract": [
                                { "$multiply": ["$PCT_CUMPLIMIENTO", 100] },
                                { "$mod": [{ "$multiply": ["$PCT_CUMPLIMIENTO", 100] }, 1] }
                            ]
                        }
                        , 100
                    ]
                }
            }
        }


        // , {
        //     "$project": {
        //         "num credito": "$ceco_cod"
        //         , "cliente": "$ppto_cliente"
        //         , "actividad": "$actividad"
        //         , "presupuesto": "$ppto_mo_inv"
        //         , "mano de obra": "$costo_mo"
        //         , "insumos": "$costo_inv"
        //         , "total costo": "$costo_mo_inv"
        //         , "PCT_CUMPLIMIENTO": "$PCT_CUMPLIMIENTO"

        //         , "cantidad de registros": "$cantidad de registros"
        //     }
        // }




        //=============================MAPA


        //---color

        //leyenda
        //%Cumplimiento: #ffffff,[0% - 25%) : #ff0000,[25% - 50%) : #ffff00,[50% - 75%) : #ffa500,[75% - 100%) : #008000,>= 100%:#0000ff
        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 0] }, { "$lt": ["$PCT_CUMPLIMIENTO", 25] }]
                        },
                        "then": "#ff0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 25] }, { "$lt": ["$PCT_CUMPLIMIENTO", 50] }]
                                },
                                "then": "#ffff00",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 50] }, { "$lt": ["$PCT_CUMPLIMIENTO", 75] }]
                                        },
                                        "then": "#ffa500",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 75] }, { "$lt": ["$PCT_CUMPLIMIENTO", 100] }]
                                                },
                                                "then": "#008000",
                                                "else": "#0000ff"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 0] }, { "$lt": ["$PCT_CUMPLIMIENTO", 25] }]
                        },
                        "then": "A-[0% - 25%)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 25] }, { "$lt": ["$PCT_CUMPLIMIENTO", 50] }]
                                },
                                "then": "B-[25% - 50%)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 50] }, { "$lt": ["$PCT_CUMPLIMIENTO", 75] }]
                                        },
                                        "then": "C-[50% - 75%)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$PCT_CUMPLIMIENTO", 75] }, { "$lt": ["$PCT_CUMPLIMIENTO", 100] }]
                                                },
                                                "then": "D-[75% - 100%)",
                                                "else": "E-[>= 100%)"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
            }
        },



        //---proyeccion
        {
            "$project": {
                "_id": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                },
                "idform": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.idform" }, 1] }]
                },
                "type": "Feature",
                "properties": {
                    "Cliente": "$ppto_cliente",
                    "Actividad": "$_id.actividad",
                    "%Cumplimiento": {
                        "$concat": ["% ", { "$toString": "$PCT_CUMPLIMIENTO" }]
                    },
                    "rango": "$rango",
                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": ["$data.variable_cartografia.features.geometry", { "$subtract": [{ "$size": "$data.variable_cartografia.features.geometry" }, 1] }]
                    // "$arrayElemAt": [
                    //     { "$arrayElemAt": ["$data.data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.features.geometry" }, 1] }] }
                    //     , 0]
                }
            }
        }






    ]

)
