[


    { "$match": { "Arbol.path": { "$exists": true } } },
    { "$match": { "Arbol.path": { "$ne": "" } } },
    { "$match": { "Arbol.path": { "$ne": "," } } },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "linea": "$linea.properties.name",
            "planta": "$planta.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0

            , "Arbol": 0
            , "Point": 0

            , "Formula": 0
            , "Numero mazorcas por rbol": 0
        }
    }








    , {
        "$addFields": {
            "array_info_mazorcas": [
                {
                    "tipo": "Mazorca sana",
                    "valor": { "$toDouble": "$Mazorca sana" },
                    "grado": 0
                },
                {
                    "tipo": "Puntos aceitosos",
                    "valor": { "$toDouble": "$Puntos aceitosos" },
                    "grado": 1
                },
                {
                    "tipo": "Tumefacccion",
                    "valor": { "$toDouble": "$Tumefacccion" },
                    "grado": 2
                },
                {
                    "tipo": "Mancha chocolate",
                    "valor": { "$toDouble": "$Mancha chocolate" },
                    "grado": 3
                },
                {
                    "tipo": "Micelio25",
                    "valor": { "$toDouble": "$Micelio25" },
                    "grado": 4
                },
                {
                    "tipo": "Micelio mayor",
                    "valor": { "$toDouble": "$Micelio mayor" },
                    "grado": 5
                }



            ]
        }
    }


    , {
        "$addFields": {
            "grado_maximo_monilia": {
                "$map": {
                    "input": "$array_info_mazorcas",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": { "$gt": ["$$item.valor", 0] },
                            "then": "$$item.grado",
                            "else": 0
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "grado_maximo_monilia": {
                "$max": "$grado_maximo_monilia"
            }
        }
    }


    , {
        "$addFields": {
            "total_mazorcas_sanas": {
                "$map": {
                    "input": "$array_info_mazorcas",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": { "$eq": ["$$item.grado", 0] },
                            "then": { "$sum": "$$item.valor" },
                            "else": 0
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "total_mazorcas_sanas": {
                "$reduce": {
                    "input": "$total_mazorcas_sanas",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "total_mazorcas_con_monilia": {
                "$map": {
                    "input": "$array_info_mazorcas",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": { "$ne": ["$$item.grado", 0] },
                            "then": { "$sum": "$$item.valor" },
                            "else": 0
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "total_mazorcas_con_monilia": {
                "$reduce": {
                    "input": "$total_mazorcas_con_monilia",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "total_mazorcas": {
                "$reduce": {
                    "input": "$array_info_mazorcas.valor",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "%incidencia_x_arbol": {
                "$cond": {
                    "if": { "$eq": ["$total_mazorcas", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            { "$divide": ["$total_mazorcas_sanas", "$total_mazorcas"] }, 100]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "%incidencia_x_arbol": { "$divide": [{ "$subtract": [{ "$multiply": ["$%incidencia_x_arbol", 100] }, { "$mod": [{ "$multiply": ["$%incidencia_x_arbol", 100] }, 1] }] }, 100] }
        }
    }




    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            }


            , "total_mazorcas_con_monilia_x_lote": { "$sum": "$total_mazorcas_con_monilia" }
            , "total_mazorcas_sanas_x_lote": { "$sum": "$total_mazorcas_sanas" }
            , "total_mazorcas_x_lote": { "$sum": "$total_mazorcas" }

            , "data": { "$push": "$$ROOT" }
        }
    }


    , {
        "$addFields": {
            "numerador": "$total_mazorcas_con_monilia_x_lote"
        }
    }

    , {
        "$addFields": {
            "denominador": "$total_mazorcas_x_lote"
        }
    }


    , {
        "$addFields": {
            "%incidencia_x_lote": {
                "$cond": {
                    "if": { "$eq": ["$denominador", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            { "$divide": ["$numerador", "$denominador"] }, 100]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "%incidencia_x_lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$%incidencia_x_lote", 100] }, { "$mod": [{ "$multiply": ["$%incidencia_x_lote", 100] }, 1] }] }, 100] }
        }
    }


    , { "$unwind": "$data" }





    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "%incidencia_x_lote": "$%incidencia_x_lote",
                        "total_mazorcas_x_lote": "$total_mazorcas_x_lote",
                        "total_mazorcas_sanas_x_lote": "$total_mazorcas_sanas_x_lote",
                        "total_mazorcas_con_monilia_x_lote": "$total_mazorcas_con_monilia_x_lote"
                    }
                ]
            }
        }
    }
    
    
    
     ,{
        "$project": {
            "array_info_mazorcas": 0
            , "Numero de plantas por hectrea": 0
            , "Indice de mazorca": 0

        }
    }

]