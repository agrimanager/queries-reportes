db.form_monilia.aggregate(

    [

        //=====CONDICIONALES BASE
        { "$match": { "Arbol.path": { "$exists": true } } },
        { "$match": { "Arbol.path": { "$ne": "" } } },
        { "$match": { "Arbol.path": { "$ne": "," } } },

        //=====CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Arbol": 0
                , "Point": 0

                , "Formula": 0
                , "Numero mazorcas por rbol": 0
            }
        }




        //=====OPERACIONES



        , {
            "$addFields": {
                "array_info_mazorcas": [
                    {
                        "tipo": "Mazorca sana",
                        "valor": { "$toDouble": "$Mazorca sana" },
                        "grado": 0
                    },
                    {
                        "tipo": "Puntos aceitosos",
                        "valor": { "$toDouble": "$Puntos aceitosos" },
                        "grado": 1
                    },
                    {
                        "tipo": "Tumefacccion",
                        "valor": { "$toDouble": "$Tumefacccion" },
                        "grado": 2
                    },
                    {
                        "tipo": "Mancha chocolate",
                        "valor": { "$toDouble": "$Mancha chocolate" },
                        "grado": 3
                    },
                    {
                        "tipo": "Micelio25",
                        "valor": { "$toDouble": "$Micelio25" },
                        "grado": 4
                    },
                    {
                        "tipo": "Micelio mayor",
                        "valor": { "$toDouble": "$Micelio mayor" },
                        "grado": 5
                    }

                    // //---otro
                    // ,{
                    //     "tipo": "mazorcas otros daños",
                    //     "valor": { "$toDouble": "$mazorcas otros daños" },
                    //     "grado": 0
                    // }

                ]
            }
        }

        //----NUMERADOR
        , {
            "$addFields": {
                "suma_grados_severidad": {
                    "$map": {
                        "input": "$array_info_mazorcas",
                        "as": "item",
                        "in": {
                            "$multiply": ["$$item.valor", "$$item.grado"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "suma_grados_severidad": {
                    "$reduce": {
                        "input": "$suma_grados_severidad",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }




        //----DENOMINADOR

        , {
            "$addFields": {
                "total_mazorcas": {
                    "$reduce": {
                        "input": "$array_info_mazorcas.valor",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "grado_maximo": {
                    "$map": {
                        "input": "$array_info_mazorcas",
                        "as": "item",
                        "in": {
                            "$cond": {
                                "if": { "$gt": ["$$item.valor", 0] },
                                "then": "$$item.grado",
                                "else": 0
                            }
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "grado_maximo": {
                    "$max": "$grado_maximo"
                }
            }
        }


        //----AGRUPACION
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                }
                , "suma_grados_severidad_lote": { "$sum": "$suma_grados_severidad" }
                , "total_mazorcas_lote": { "$sum": "$total_mazorcas" }
                , "grado_maximo_lote": { "$max": "$grado_maximo" }
            }
        }


        , {
            "$addFields": {
                "numerador": "$suma_grados_severidad_lote"
            }
        }

        , {
            "$addFields": {
                "denominador": {
                    "$multiply": ["$total_mazorcas_lote", "$grado_maximo_lote"]
                }
            }
        }


        , {
            "$addFields": {
                "%severidad_x_lote": {
                    "$cond": {
                        "if": { "$eq": ["$denominador", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                { "$divide": ["$numerador", "$denominador"] }, 100]
                        }
                    }
                }
            }
        }

        //--2 decimales
        , {
            "$addFields": {
                "%severidad_x_lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$%severidad_x_lote", 100] }, { "$mod": [{ "$multiply": ["$%severidad_x_lote", 100] }, 1] }] }, 100] }
            }
        }




        //=====VALORES A MOSTRAR
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "%severidad_x_lote": "$%severidad_x_lote",
                            "suma_grados_severidad_lote": "$suma_grados_severidad_lote",
                            "total_mazorcaote": "$total_mazorcas_lote",
                            "grado_maximo_lote": "$grado_maximo_lote",
                            "numerador": "$numerador",
                            "denominador": "$denominador"

                        }
                    ]
                }
            }
        }





    ]
)