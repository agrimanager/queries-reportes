var data = db.forms.aggregate(

    {
        $match:{
            name:"REGISTRO PREDIO DE AGUACATE"
        }
    }

    ,{
        $addFields: {
            var_array: { "$objectToArray": "$fstructure" }

        }
    }

)

// data

data.forEach(item_data => {

    // console.log(item_data)

    var variable_array = []
    item_data.var_array.forEach(item => {
        variable_array.push(item.v)
    })

    //console.log(variable_array)

    db.forms.update(
        {
            _id: item_data._id
        },
        {
            $set: {
                "fstructure": variable_array
            }
        }
    )


})
