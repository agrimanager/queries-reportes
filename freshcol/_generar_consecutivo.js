
/*
//---freshcol
ticket#1690 (Modificación en la generación del código de productor Freshcol)

--DUDAS: Como generar el NUMERO DE VISITA automaticamente ???

1) Condiciones para generar el numero?
por Finca_AG? Finca_nombre? Departamento? Supervisor? fecha?

2) Para datos nuevos y viejos ? (existen muchos datos con numeros iguales por ejemplo el 0,1)
3) proceso de crear cartografia desde registro...e insertar a seguimiento con mismo numero ?
4) desde que numero comenzar ? (5267 es el ultimo)
*/

//
// db.form_registroprediodeaguacate.aggregate(
// // db.form_seguimientoregistroprediodeaguacate.aggregate(
//     [
//         {
//             $group: {
//                 _id: "$Numero de visita"
//                 , data: { $push: "$$ROOT" }
//                 , cant: { $sum: 1 }
//                 , fecha_min: { $min: "$rgDate" }
//                 , fecha_max: { $max: "$rgDate" }
//
//             }
//         }
//         , {
//             $sort: {
//                 cant: -1
//             }
//         }
//
//     ]
//     , { allowDiskUse: true }
// )
//
//

db.form_registroprediodeaguacate.aggregate(

    {
        "$lookup": {
            "from": "form_registroprediodeaguacate",
            "let": {
                "date": "$rgDate",
                "id": "$_id"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$lte": ["$rgDate", "$$date"] },
                                // { "$eq": ["$_id", "$$id"] }
                            ]
                        }
                    }
                },
                { "$count": "count" },
                // {
                //     "$addFields": {
                //         "count": {
                //             "$switch": {
                //                 "branches": [
                //                     {
                //                         "case": { "$gt": ["$count", 130] },
                //                         "then": { "$subtract": ["$count", 130] }
                //                     }
                //                 ],
                //                 "default": "$count"
                //             }
                //         }
                //     }
                // },
                // { "$project": { "count": { "$toString": "$count" } } }
            ],
            "as": "data_consecutivo"
        }
    },

)
