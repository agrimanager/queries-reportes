


//ids de que no cruzan con cartografia  y desde movil

var query_ids = db._data_new.aggregate(
    {
        "$group": {
            "_id": null,
            "ids": {
                "$push": "$_id"
            }
        }
    }
)

var array_ids = []

query_ids.forEach(aux => {
    array_ids = aux.ids
})



var data_form = db.form_registroprediodeaguacate.aggregate(

    {
        $match: {
            _id: {
                $in: array_ids
            }
        }
    }

    //finca
    , {
        "$addFields": {
            "id_finca": "$Point.farm"
        }
    }

    , {
        "$addFields": {
            "id_finca": { "$toObjectId": "$id_finca" }
        }
    }

    , {
        "$lookup": {
            "from": "farms",
            "localField": "id_finca",
            "foreignField": "_id",
            "as": "FINCA AGRIMANAGER"
        }
    },
    { "$unwind": "$FINCA AGRIMANAGER" },

    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }


    , {
        "$addFields": {
            "ITEM_CARTOGRAFIA_INSERT": {
                //"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                "type": "Feature",
                //"geometry": "$Point.geometry"
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 0] },
                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 1] }
                    ]
                }
                , "properties": {
                    "type": "trees",
                    "name": "$nombre cartografia",
                    "production": true,
                    "status": true,
                    "custom": {
                        "Codigo del productor": {
                            "value": "$codigo productor",
                            "type": "string"
                        },
                        "Nombre de la finca": {
                            "value": "$Nombre de finca",
                            "type": "string"
                        },
                        "Vereda": {
                            "value": "$Vereda",
                            "type": "string"
                        },
                        "Propietario": {
                            "value": "$Nombre del Propietario",
                            "type": "string"
                        },
                        "Cel del propietario": {
                            "value": { $toString: "$Celular del propietario" },
                            "type": "string"
                        },
                        "Latitud": {
                            "value": { $toString: { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 1] } },
                            "type": "string"
                        },
                        "Longitud": {
                            "value": { $toString: { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 0] } },
                            "type": "string"
                        },
                    }
                },
                "path": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Sonson"] },
                                "then": ",62cd92723cf9784d38eb0743,633352c3c7fa8e99d502bad7,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Urrao"] },
                                "then": ",632873173cf9784d38eb1357,63333235c7fa8e99d502b757,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Antioquia"] },
                                "then": ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Suroeste"] },
                                "then": ",6328768f3cf9784d38eb1359,63335942c7fa8e99d502c094,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Caldas"] },
                                "then": ",632872833cf9784d38eb1355,63330f78bf48fd5f2ae52a4e,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Eje Cafetero"] },
                                "then": ",632871e03cf9784d38eb1351,6334d1f5c7fa8e99d502c388,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Tolima"] },
                                "then": ",62dfdf753cf9784d38eb0797,632c794abf48fd5f2ae52a4d,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Valle del Cauca"] },
                                "then": ",633adbb82951a04f62d56c86,6346e6d8c7fa8e99d503963a,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cauca"] },
                                "then": ",633adbe82951a04f62d56c88,633e1658c7fa8e99d5036089,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Huila"] },
                                "then": ",632872113cf9784d38eb1353,63337653c7fa8e99d502c2d4,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Fresno"] },
                                "then": ",632871483cf9784d38eb134f,6333672ac7fa8e99d502c0f9,"
                            }
                            , {
                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cundinamarca"] },
                                "then": ",633adc4b2951a04f62d56c8c,6346fba2c7fa8e99d5039972,"
                            }
                        ], "default": "--------"
                    }
                }
            }
        }
    }




)


// data_form



data_form.forEach(item_data_form => {


    //====2)insertar en cartografia
    db.cartography.insert(
        item_data_form.ITEM_CARTOGRAFIA_INSERT
    )

})
