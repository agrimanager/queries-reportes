//PROCESO_4 (GENERAR CARTOGRAFIA)
//lanzar query para obtener URIs "servicio_generar_cartografia"
//bajar datos a excel
//abrir powershell y disparar metodos POST

//====================================
//---ejemplo sanjose
//method: POST
//finca: Semana Santa
//finca_id: 5d27d22e793a4867b305012c
//Request URL: https://map.agrimanager.app/sanjose/api/mobile/5d27d22e793a4867b305012c.sqlite

/*
https://map.agrimanager.app/
DB_NAME
/api/mobile/
FINCA_ID
.sqlite
*/

//-----ejemplo2
//https://map.agrimanager.app/freshcol/api/mobile/62dfdefb3cf9784d38eb0795.sqlite


//----Ojo usar desde db local

//====== Resultado
var result_info = [];


//--obtener bases de datos
// var bases_de_datos = db.getMongo().getDBNames();
var bases_de_datos = ["freshcol"];

//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name => {


    //--obtener formularios
    var fincas = db.getSiblingDB(db_name).farms.aggregate(
        {
            $addFields: {
                finca_id: { $toString: "$_id" }
            }
        }
    ).allowDiskUse();

    //--🔄 ciclar formularios
    fincas.forEach(item => {

        result_info.push({
            database: db_name,
            finca: item.name,
            finca_oid: item._id,
            finca_id: item.finca_id

            //,url_generar_cartografia : "https://map.agrimanager.app/" + db_name + "/api/mobile/" + item.finca_id + ".sqlite"
            ,servicio_generar_cartografia : "Invoke-WebRequest -Uri https://map.agrimanager.app/" + db_name + "/api/mobile/" + item.finca_id + ".sqlite  -Method POST"

        })

    });

});

//--imprimir resultado
result_info



/*

Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632871e03cf9784d38eb1351.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62cd92723cf9784d38eb0743.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62dfdefb3cf9784d38eb0795.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62dfdf753cf9784d38eb0797.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632871483cf9784d38eb134f.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632872113cf9784d38eb1353.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632872833cf9784d38eb1355.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632873173cf9784d38eb1357.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/6328768f3cf9784d38eb1359.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adbb82951a04f62d56c86.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adbe82951a04f62d56c88.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adc1b2951a04f62d56c8a.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adc4b2951a04f62d56c8c.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/6479080dc23f316ad9d3eb78.sqlite  -Method POST



*/
