//PROCESO_0 (obtener datos sin cartografia)
//lanzar reporte, filtros y guardar en TABLA TEMPORAL "_data_new"
/*
filtrar formulario "registroprediodeaguacate"
filtrar en campo "nombre cartografia" los campos VACIOS,Pendiente,NA
filtrar Nombre de finca VACIOS,NA
filtrar los que tengan buenos las coordendas (latitud > 0)
*/

db.users.aggregate(
    [




        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 }
        , {
            "$lookup": {
                "from": "form_seguimientoregistroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }




                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }



                    , {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }



                    , {
                        "$addFields": {
                            "variable_cartografia": "$Finca"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },



                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "codigo productor": { "$ifNull": ["$arbol.properties.custom.Codigo del productor.value", ""] } } },


                    {
                        "$addFields": {
                            "Longitud": { "$arrayElemAt": [{ "$ifNull": ["$arbol.geometry.coordinates", []] }, 0] },
                            "Latitud": { "$arrayElemAt": [{ "$ifNull": ["$arbol.geometry.coordinates", []] }, 1] }
                        }
                    }

                    , {
                        "$addFields": {
                            "Longitud": { "$ifNull": ["$Longitud", 0] }
                            , "Latitud": { "$ifNull": ["$Latitud", 0] }
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "Formula": 0
                            , "Point": 0
                            , "Finca": 0
                        }
                    }



                    , {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Departamento y Municipio_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },

                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] }
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }





                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "Municipio": {
                                "$cond": {
                                    "if": { "$eq": ["$Municipio", ""] },
                                    "then": "$valor_maestro_enlazado",
                                    "else": "$Municipio"
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre cartografia": "$arbol"
                        }
                    }



                    , {
                        "$project": {
                            "finca": 0,
                            "bloque": 0,
                            "arbol": 0,
                            "nombre_maestro_enlazado": 0,
                            "valor_maestro_enlazado": 0
                            , "supervisor_u": 0
                            , "id_finca": 0
                            , "uid": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "formulario": "seguimientoregistroprediodeaguacate"

                        }
                    }

                    , {
                        "$addFields": {
                            "filtro_fecha_fin": "$$filtro_fecha_fin"

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }

                , "filtro_fecha_fin": "$Busqueda fin"
            }
        }


        , {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_fin": "$filtro_fecha_fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }



                    , {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }


                    // //error porque aveces trae bloques
                    // , {
                    //     "$lookup": {
                    //         "from": "cartography",
                    //         "localField": "nombre cartografia",
                    //         "foreignField": "properties.name",
                    //         "as": "data_cartografia"
                    //     }
                    // }


                    //new -- obtener solo arboles
                    , {
                        "$lookup": {
                            "from": "cartography",
                            "as": "data_cartografia",
                            "let": {
                                "nombre_cartografia": "$nombre cartografia"
                            },
                            "pipeline": [


                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$nombre_cartografia", "$properties.name"] },
                                                { "$eq": ["$properties.type", "trees"] }
                                            ]
                                        }
                                    }

                                }

                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$data_cartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "Longitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 0] },
                            "Latitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 1] }
                        }
                    }

                    , {
                        "$addFields": {
                            "Longitud": {
                                "$ifNull": [
                                    "$Longitud",
                                    { "$arrayElemAt": ["$Point.geometry.coordinates", 0] }
                                ]
                            }
                            , "Latitud": {
                                "$ifNull": [
                                    "$Latitud",
                                    { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
                                ]
                            }
                        }
                    }

                    , {
                        "$project": {
                            "data_cartografia": 0
                            , "Formula": 0
                            , "Point": 0

                        }
                    }



                    , {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Departamento y Municipio_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },

                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] }
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }





                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "Municipio": {
                                "$cond": {
                                    "if": { "$eq": ["$Municipio", ""] },
                                    "then": "$valor_maestro_enlazado",
                                    "else": "$Municipio"
                                }
                            }
                        }
                    }

                    , {
                        "$project": {
                            "valor_maestro_enlazado": 0
                            , "supervisor_u": 0
                            , "id_finca": 0
                            , "uid": 0
                        }
                    }

                    , {
                        "$addFields": {
                            "formulario": "registroprediodeaguacate"

                        }
                    }


                    , {
                        "$addFields": {
                            "filtro_fecha_fin": "$$filtro_fecha_fin"

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$datos"
                        , "$data"
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        , {
            "$sort": {
                "rgDate": -1
            }
        }

        , {
            "$group": {
                "_id": {
                    "nombre_cartografia": "$nombre cartografia"
                }

                , "data": { "$push": "$$ROOT" }
            }

        }



        , {
            "$addFields": {
                "data": {
                    "$cond": {
                        "if": {
                            "$in": ["$_id.nombre_cartografia",
                                ["", "Pendiente", "Pendiente "]
                            ]
                        },
                        "then": "$data",
                        "else": [{ "$arrayElemAt": ["$data", 0] }]
                    }

                }
            }
        }


        , { "$unwind": "$data" }
        , { "$replaceRoot": { "newRoot": "$data" } }



        , {
            "$project": {
                "Departamento y Municipio_Antioquia": 0,
                "Departamento y Municipio_Caldas": 0,
                "Departamento y Municipio_Cauca": 0,
                "Departamento y Municipio_Cundinamarca": 0,
                "Departamento y Municipio_Huila": 0,
                "Departamento y Municipio_Quindio": 0,
                "Departamento y Municipio_Risaralda": 0,
                "Departamento y Municipio_Santander": 0,
                "Departamento y Municipio_Tolima": 0,
                "Departamento y Municipio_Valle Del Cauca": 0

            }
        }


        , {
            "$sort": {
                "rgDate": -1
            }
        }

        , {
            "$addFields": {
                "Departamento y Municipio": { "$trim": { "input": "$Departamento y Municipio" } }
            }
        }

        , {
            "$addFields": {
                "Municipio": { "$trim": { "input": "$Municipio" } }
            }
        }

        , {
            "$addFields": {
                "Municipio": { "$toUpper": "$Municipio" }
            }
        }



        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },

                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }

            }
        }



        , {
            "$addFields": {
                "rgDate": "$filtro_fecha_fin"
            }
        }
        , {
            "$project": {
                "filtro_fecha_fin": 0
            }
        }


        //new
        //arreglar coordenadas
        , {
            "$addFields": {
                "aux_Longitud": "$Longitud",
                "aux_Latitud": "$Latitud"
            }
        }

        , {
            "$addFields": {
                "Longitud": {
                    "$cond": {
                        "if": {
                            $lt: ["$Latitud", 0]
                        },
                        "then": "$aux_Latitud",
                        "else": "$Longitud"
                    }
                }
                , "Latitud": {
                    "$cond": {
                        "if": {
                            $lt: ["$Latitud", 0]
                        },
                        "then": "$aux_Longitud",
                        "else": "$Latitud"
                    }
                }
            }
        }

        , {
            "$project": {
                "aux_Longitud": 0,
                "aux_Latitud": 0
            }
        }





        , {
            $match: {
                "formulario": "registroprediodeaguacate",

                "nombre cartografia": {
                    $in: ["", "na", "NA", "Na", "Na ", "Pendiente", "pendiente", "Pendiente ", "pendiente "]
                }
                , "Nombre de finca": {
                    $nin: ["", "na", "NA", "Na", "Na "]
                }
                , "Latitud": { $gt: 0 }
            }

        }


        , {
            "$lookup": {
                "from": "form_puentecodigoregion",
                "localField": "FINCA AGRIMANAGER",
                "foreignField": "Finca Agrimanager",
                "as": "codigo_region"
            }
        }

        , { "$unwind": "$codigo_region" }

        , { "$addFields": { "codigo_region": { "$ifNull": ["$codigo_region.Codigo Region", ""] } } }


        //new arreglo repetidos
        , {
            "$group": {
                "_id": {
                    "id": "$_id"
                }

                , "data": { "$push": "$$ROOT" }
            }

        }



        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        , { "$unwind": "$data" }
        , { "$replaceRoot": { "newRoot": "$data" } }


        //GUARDAR TEMPORAL
        , {
            $out: "_data_new"
        }





    ]


)
