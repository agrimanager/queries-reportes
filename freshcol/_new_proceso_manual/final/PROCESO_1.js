//PROCESO_1 (generar codigo_productor y nombre_cartografia)
//y actualizar registros del formulario "form_registroprediodeaguacate"
/*
--ejemplo: (La Maria A3:23IA)

La Maria= Finca
A3= REGION (En este caso Antioquia)
: Dos puntos
23= Campo NUMERO DE VISITA
IA= Ultimas dos letras del campo NOMBRE DE FINCA
*/


//OBTENER TEMPORAL
var data = db._data_new.aggregate()
// data

var array_data_update = [];

data.forEach(item_data => {


    var nombre_finca = item_data["Nombre de finca"]
    nombre_finca = nombre_finca.trim()
    nombre_finca = nombre_finca.trim()
    // console.log(nombre_finca)//test

    //---EJEMPLO:La Maria A3:23IA

    //CODIGO_PRODUCTOR (codigo_region + ":" + num_visita + ultimas_2_letras_finca)
    var CODIGO_PRODUCTOR = "";
    CODIGO_PRODUCTOR = CODIGO_PRODUCTOR.concat(item_data["codigo_region"])
    CODIGO_PRODUCTOR = CODIGO_PRODUCTOR.concat(":")
    CODIGO_PRODUCTOR = CODIGO_PRODUCTOR.concat(item_data["Numero de visita"])

    var nombre_finca_array = nombre_finca.split('')
    var ultimas_2_letras_finca = ""
    ultimas_2_letras_finca = ultimas_2_letras_finca.concat(nombre_finca_array[nombre_finca_array.length - 2].toUpperCase())
    ultimas_2_letras_finca = ultimas_2_letras_finca.concat(nombre_finca_array[nombre_finca_array.length - 1].toUpperCase())

    CODIGO_PRODUCTOR = CODIGO_PRODUCTOR.concat(ultimas_2_letras_finca)
    // console.log(CODIGO_PRODUCTOR)


    //NOMBRE_CARTOGRAFIA (Nombre de finca + CODIGO_PRODUCTOR)
    var NOMBRE_CARTOGRAFIA = "";
    NOMBRE_CARTOGRAFIA = NOMBRE_CARTOGRAFIA.concat(nombre_finca)
    NOMBRE_CARTOGRAFIA = NOMBRE_CARTOGRAFIA.concat(" ")
    NOMBRE_CARTOGRAFIA = NOMBRE_CARTOGRAFIA.concat(CODIGO_PRODUCTOR)
    console.log(NOMBRE_CARTOGRAFIA)


    array_data_update.push({
        _id: item_data._id,
        codigo_productor: CODIGO_PRODUCTOR,
        nombre_cartografia: NOMBRE_CARTOGRAFIA,
    })




})

// array_data_update


array_data_update.forEach(i => {
    db.form_registroprediodeaguacate.update(
        {
            _id: i._id
        },
        {
            $set: {
                "nombre cartografia": i.nombre_cartografia
                , "codigo productor": i.codigo_productor

            }
        }
    )
})
