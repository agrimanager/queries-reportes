


//1)actualizar valor de "Departamento y Municipio_Antioquia" por "Municipios de Antioquia"

var cursor = db.form_registroprediodeaguacate.aggregate(
    {
        $match:{
            "Municipio":{$exists: true}
        }
    }

    ,{
        $match:{
            "Municipio":{$ne: ""}
        }
    }

    ,{
        $addFields: {
            municipio:"$Municipio"
        }
    }
)


// cursor


cursor.forEach(i => {

    db.form_registroprediodeaguacate.update(
        {
            _id: i._id
        },
        {
            $set: {
                //"Departamento y Municipio_Municipios de Tolima": i.municipio
                "Departamento y Municipio_Municipios de Antioquia": i.municipio
                // "Departamento y Municipio_Municipios de Caldas": i.municipio
            }
        }
    )


});


// //2)borrar la variable "Municipios de Antioquia"

// db.form_registroprediodeaguacate.update(
//     {
//         "Municipio":{$exists: true}
//     }
//     ,
//     {
//         $unset: {
//             "Municipio": 1
//         }

//     }, false, true
// );
