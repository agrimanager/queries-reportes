/* 1 createdAt:26/7/2022, 4:07:44 p. m.*/
{
	"_id" : ObjectId("62e057a03cf9784d38eb0843"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Risaralda",
	"options" : [
		{
			"name" : "APÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BALBOA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BELÉN DE UMBRÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "DOSQUEBRADAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUÁTICA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA CELIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA VIRGINIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARSELLA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MISTRATÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PEREIRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUEBLO RICO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "QUINCHiA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA ROSA DE CABAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTUARIO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:07:44.148-05:00"),
	"uDate" : ISODate("2022-07-26T16:07:44.148-05:00")
},

/* 2 createdAt:26/7/2022, 4:07:34 p. m.*/
{
	"_id" : ObjectId("62e057963cf9784d38eb083e"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Valle Del Cauca",
	"options" : [
		{
			"name" : "ALCALa",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANDALUCÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANSERMANUEVO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BUGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BUGALAGRANDE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CALI",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CALIMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CANDELARIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "DAGUA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL CERRITO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA UNIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA VICTORIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "OBANDO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PRADERA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RESTREPO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RIOFRIO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ROLDANILLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN PEDRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TORO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ULLOA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VERSALLES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VIJES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YOTOCO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YUMBO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:07:34.385-05:00"),
	"uDate" : ISODate("2022-07-26T16:07:34.385-05:00")
},

/* 3 createdAt:26/7/2022, 4:07:22 p. m.*/
{
	"_id" : ObjectId("62e0578a3cf9784d38eb0839"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Quindio",
	"options" : [
		{
			"name" : "ARMENIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BUENAVISTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CALARCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CIRCASIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CoRDOBA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FILANDIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GeNOVA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA TEBAIDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Montengro",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PIJAO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "QUIMBAYA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SALENTO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:07:22.872-05:00"),
	"uDate" : ISODate("2022-07-26T16:07:22.872-05:00")
},

/* 4 createdAt:26/7/2022, 4:07:14 p. m.*/
{
	"_id" : ObjectId("62e057823cf9784d38eb0834"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Caldas",
	"options" : [
		{
			"name" : "AGUADAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANSERMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARANZAZU",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BELALCÁZAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHINCHINa",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FILADELFIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA DORADA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA MERCED",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MANIZALES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MANZANARES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARMATO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARQUETALIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARULANDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NEIRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NORCASIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PÁCORA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PALESTINA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PENSILVANIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RIOSUCIO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RISARALDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SALAMINA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAMANÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JOSÉ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SUPÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VICTORIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VILLAMARiA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VITERBO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:07:14.360-05:00"),
	"uDate" : ISODate("2022-07-26T16:07:14.360-05:00")
},

/* 5 createdAt:26/7/2022, 4:07:04 p. m.*/
{
	"_id" : ObjectId("62e057783cf9784d38eb082f"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Antioquia",
	"options" : [
		{
			"name" : "ABEJORRAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ABRIAQUÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ALEJANDRÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "AMAGa",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "AMALFI",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANDES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANGELOPOLIS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANGOSTURA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANORÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANZA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "APARTADÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARBOLETES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARGELIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARMENIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BARBOSA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BELLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BELMIRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BETANIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BETULIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BRICEÑO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BURITICÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CÁCERES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAICEDO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CALDAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAMPAMENTO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAÑASGORDAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CARACOLÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CARAMANTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAREPA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CARMEN DE VIBORAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAROLINA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAUCASIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHIGORODÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CISNEROS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CIUDAD BOLÍVAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "COCORNÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CONCEPCIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CONCORDIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "COPACABANA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "DABEIBA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "DON MATiAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EBÉJICO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL BAGRE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ENTRERRIOS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ENVIGADO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FREDONIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FRONTINO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GIRALDO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GIRARDOTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GÓMEZ PLATA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GRANADA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUADALUPE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUARNE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUATAPE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "HELICONIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "HISPANIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ITAGUI",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ITUANGO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "JARDÍN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "JERICÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA CEJA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA ESTRELLA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA PINTADA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA UNIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LIBORINA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MACEO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARINILLA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MEDELLÍN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MONTEBELLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MURINDÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MUTATA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NARIÑO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NECHÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NECOCLÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "OLAYA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PEÑOL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PEQUE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUEBLORRICO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUERTO BERRiO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUERTO NARE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUERTO TRIUNFO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "REMEDIOS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RETIRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RIONEGRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SABANALARGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SABANETA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SALGAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN ANDRÉS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN CARLOS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN FRANCISCO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JERÓNIMO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JOSÉ DE LA MONTAÑA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JUAN DE URABA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN LUIS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN PEDRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN PEDRO DE URABA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN RAFAEL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN ROQUE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN VICENTE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA BaRBARA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA ROSA de osos",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTAFÉ DE ANTIOQUIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTO DOMINGO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTUARIO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SEGOVIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SONSON",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SOPETRaN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TÁMESIS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TARAZÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TARSO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TITIRIBÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TOLEDO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TURBO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "URAMITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "URRAO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VALDIVIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VALPARAISO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VEGACHÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VENECIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VIGÍA DEL FUERTE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YALÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YARUMAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YOLOMBÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "YONDÓ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ZARAGOZA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:07:04.441-05:00"),
	"uDate" : ISODate("2022-07-26T16:07:04.441-05:00")
},

/* 6 createdAt:26/7/2022, 4:06:56 p. m.*/
{
	"_id" : ObjectId("62e057703cf9784d38eb082a"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Santander",
	"options" : [
		{
			"name" : "AGUADA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ALBANIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARATOCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BARBOSA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BARICHARA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BARRANCABERMEJA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BETULIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BOLÍVAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "BUCARAMANGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CABRERA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CALIFORNIA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAPITANEJO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CARCASÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CEPITÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CERRITO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHARALÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHARTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHIMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHIPATÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CIMITARRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CONCEPCIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CONFINES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CONTRATACIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "COROMORO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CURITÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL CARMEN DE CHUCURÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL GUACAMAYO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL PEÑÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "EL PLAYÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ENCINO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ENCISO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FLORIÁN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FLORIDABLANCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GALÁN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GAMBITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GIRÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUACA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUADALUPE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUAPOTÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUAVATÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GuEPSA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "HATO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "JESÚS MARÍA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "JORDÁN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA BELLEZA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LA PAZ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LANDÁZURI",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LEBRÍJA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LOS SANTOS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MACARAVITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MÁLAGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MATANZA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MOGOTES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MOLAGAVITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "OCAMONTE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "OIBA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ONZAGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PALMAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PALMAS DEL SOCORRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PÁRAMO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PIEDECUESTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PINCHOTE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUENTE NACIONAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUERTO PARRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PUERTO WILCHES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RIONEGRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SABANA DE TORRES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN ANDRÉS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN BENITO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN GIL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JOAQUÍN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN JOSÉ DE MIRANDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN MIGUEL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN VICENTE DE CHUCURÍ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA BÁRBARA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA HELENA DEL OPÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SIMACOTA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SOCORRO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SUAITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SUCRE",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SURATA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "TONA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VALLE DE SAN JOSÉ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VÉLEZ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VETAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VILLANUEVA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ZAPATOCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:06:56.558-05:00"),
	"uDate" : ISODate("2022-07-26T16:06:56.558-05:00")
},

/* 7 createdAt:26/7/2022, 4:06:46 p. m.*/
{
	"_id" : ObjectId("62e057663cf9784d38eb0825"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "Municipios de Tolima",
	"options" : [
		{
			"name" : "ALPUJARRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ALVARADO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "AMBALEMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ANZOÁTEGUI",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ARMERO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ATACO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CAJAMARCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CARMEN DE APICALÁ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CASABIANCA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CHAPARRAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "COELLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "COYAIMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "CUNDAY",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "DOLORES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ESPINAL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FALAN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FLANDES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "FRESNO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "GUAMO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "HERVEO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "HONDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "IBAGUe",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ICONONZO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LeRIDA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "LiBANO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MARIQUITA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MELGAR",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "MURILLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "NATAGAIMA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ORTEGA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PALOCABILDO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PIEDRAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PLANADAS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PRADO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "PURIFICACIÓN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RIOBLANCO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "RONCESVALLES",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "ROVIRA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SALDAÑA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN ANTONIO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SAN LUIS",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SANTA ISABEL",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "SUÁREZ",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VALLE DE SAN JUAN",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VENADILLO",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VILLAHERMOSA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "VILLARRICA",
			"master" : "",
			"color" : "#ccc",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T16:06:46.536-05:00"),
	"uDate" : ISODate("2022-07-26T16:06:46.536-05:00")
},

/* 8 createdAt:26/7/2022, 3:55:07 p. m.*/
{
	"_id" : ObjectId("62e054ab3cf9784d38eb0820"),
	"uid" : ObjectId("62cc2d7e3cf9784d38eb0732"),
	"name" : "DEPARTAMENTOS",
	"options" : [
		{
			"name" : "Antioquia",
			"color" : "",
			"master" : "62e057783cf9784d38eb082f",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Caldas",
			"color" : "",
			"master" : "62e057823cf9784d38eb0834",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Quindio",
			"color" : "",
			"master" : "62e0578a3cf9784d38eb0839",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Risaralda",
			"color" : "",
			"master" : "62e057a03cf9784d38eb0843",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Santander",
			"color" : "",
			"master" : "62e057703cf9784d38eb082a",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Tolima",
			"color" : "",
			"master" : "62e057663cf9784d38eb0825",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		},
		{
			"name" : "Valle Del Cauca",
			"color" : "",
			"master" : "62e057963cf9784d38eb083e",
			"consecutives" : "",
			"incidenceFrequency" : 0,
			"incidencePercentage" : 0
		}
	],
	"images" : false,
	"rgDate" : ISODate("2022-07-26T15:55:07.720-05:00"),
	"uDate" : ISODate("2022-07-26T16:10:58.973-05:00")
}
