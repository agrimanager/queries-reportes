[




    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_registroprediodeaguacate",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                {
                    "$addFields": {
                        "id_finca": "$Point.farm"
                    }
                }

                , {
                    "$addFields": {
                        "id_finca": { "$toObjectId": "$id_finca" }
                    }
                }

                , {
                    "$lookup": {
                        "from": "farms",
                        "localField": "id_finca",
                        "foreignField": "_id",
                        "as": "FINCA AGRIMANAGER"
                    }
                },
                { "$unwind": "$FINCA AGRIMANAGER" },

                { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }






                , {
                    "$addFields": {
                        "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                        , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                        , "Municipio": { "$ifNull": ["$Municipio", ""] }

                        , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                    }
                }

                , {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "nombre cartografia",
                        "foreignField": "properties.name",
                        "as": "data_cartografia"
                    }
                }
                , {
                    "$unwind": {
                        "path": "$data_cartografia",
                        "preserveNullAndEmptyArrays": true
                    }
                }


                , {
                    "$addFields": {
                        "Longitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 0] },
                        "Latitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 1] }
                    }
                }

                , {
                    "$addFields": {
                        "Longitud": {
                            "$ifNull": [
                                "$Longitud",
                                { "$arrayElemAt": ["$Point.geometry.coordinates", 0] }
                            ]
                        }
                        , "Latitud": {
                            "$ifNull": [
                                "$Latitud",
                                { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
                            ]
                        }
                    }
                }

                , {
                    "$project": {
                        "data_cartografia": 0
                        , "id_finca": 0

                        , "uid": 0
                        , "uDate": 0
                        , "Point": 0
                    }
                }


                , {
                    "$addFields": {
                        "Fecha": "$rgDate"
                    }
                }

                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
