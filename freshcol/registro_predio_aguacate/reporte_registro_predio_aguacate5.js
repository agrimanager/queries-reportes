db.form_registroprediodeaguacate.aggregate(
    [

        {
            "$addFields": {
                "id_finca": "$Point.farm"
            }
        }

        , {
            "$addFields": {
                "id_finca": { "$toObjectId": "$id_finca" }
            }
        }

        , {
            "$lookup": {
                "from": "farms",
                "localField": "id_finca",
                "foreignField": "_id",
                "as": "FINCA AGRIMANAGER"
            }
        },
        { "$unwind": "$FINCA AGRIMANAGER" },

        { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }






        , {
            "$addFields": {
                "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                , "Municipio": { "$ifNull": ["$Municipio", ""] }

                , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

            }
        }

        , {
            "$lookup": {
                "from": "cartography",
                "localField": "nombre cartografia",
                "foreignField": "properties.name",
                "as": "data_cartografia"
            }
        }
        , {
            "$unwind": {
                "path": "$data_cartografia",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "Longitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 0] },
                "Latitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 1] }
            }
        }

        , {
            "$addFields": {
                "Longitud": { "$ifNull": ["$Longitud", 0] }
                , "Latitud": { "$ifNull": ["$Latitud", 0] }
            }
        }

        , {
            "$project": {
                "data_cartografia": 0
                , "id_finca": 0

                , "uid": 0
                , "uDate": 0
                , "Point": 0
                , "Formula": 0
                , "capture": 0
                , "supervisor_u": 0

            }
        }


        // , {
        //     "$addFields": {
        //         "Fecha": "$rgDate"
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "rgDate": "$$filtro_fecha_inicio"
        //     }
        // }


        //---new

        //---info fechas
        , { "$addFields": { "variable_fecha": "$rgDate" } }
        , {
            "$addFields": {
                "Fecha Visita": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }
                , "Hora Visita": { "$dateToString": { "format": "%H:%M", "date": "$variable_fecha", "timezone": "America/Bogota" } }
            }
        }


        //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        , { "$project": { "variable_fecha": 0 } },





    ]
)
