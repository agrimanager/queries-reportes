db.form_registroprediodeaguacate.aggregate(
    [

        {
            "$addFields": {
                "codigo_region": "A3"
            }
        }

        , {
            "$addFields": {
                "codigo_visita": { "$toString": "$Numero de visita" }
            }
        }

        , {
            "$addFields": {
                "finca_num_letras": { "$strLenCP": "$Nombre de finca" }
            }
        }

        , {
            "$addFields": {
                "finca_num_letras2": {
                    "$subtract": ["$finca_num_letras", 2]
                }
            }
        }

        , {
            "$addFields": {
                "codigo_finca": {
                    "$substr": ["$Nombre de finca", "$finca_num_letras2", -1]
                }
            }
        }



        , {
            "$addFields": {
                "CODIGO DEL PRODUCTOR": {
                    "$concat": ["$codigo_region", ":", "$codigo_visita", "$codigo_finca"]
                }
            }
        }


        , {
            "$project": {
                "finca_num_letras": 0,
                "finca_num_letras2": 0
            }
        }




    ]

)
