db.form_registroprediodeaguacate.aggregate(
    [

        // {
        //     $match: {
        //         "Nombre de finca": {
        //             $regex: 'ñ'
        //         }
        //     }
        // },

        {
            "$match": {
                "Nombre de finca": {
                    "$not": {
                        "$regex": 'ñ'
                    }
                }
            }
        },
        {
            "$match": {
                "Nombre de finca": {
                    "$not": {
                        "$regex": 'Ñ'
                    }
                }
            }
        },


        {
            "$addFields": {
                "codigo_region": "A3"
            }
        }

        , {
            "$addFields": {
                "codigo_visita": { "$toString": "$Numero de visita" }
            }
        }

        , {
            "$addFields": {
                "finca_num_letras": { "$strLenCP": "$Nombre de finca" }
            }
        }

        , {
            "$addFields": {
                "finca_num_letras2": {
                    "$subtract": ["$finca_num_letras", 2]
                }
            }
        }


        //ERROR CON ñ
        //"$substrBytes:  Invalid range, starting index is a UTF-8 continuation byte."
        // , {
        //     "$addFields": {
        //         "codigo_finca": {
        //             "$substr": ["$Nombre de finca", "$finca_num_letras2", -1]
        //         }
        //     }
        // }

        , {
            "$addFields": {
                "codigo_finca": {
                    "$cond": {
                        "if": { "$eq": ["$finca_num_letras", 0] }
                        , "then": ""
                        , "else": {
                            "$substr": ["$Nombre de finca", "$finca_num_letras2", -1]
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "CODIGO DEL PRODUCTOR": {
                    "$concat": ["$codigo_region", ":", "$codigo_visita", "$codigo_finca"]
                }
            }
        }


        , {
            "$project": {
                "finca_num_letras": 0,
                "finca_num_letras2": 0
            }
        }


        , {
            "$addFields": {
                "CARTOGRAFIA": {
                    "$concat": ["$Nombre de finca", " ", "$CODIGO DEL PRODUCTOR"]
                }
            }
        }




    ]

)
