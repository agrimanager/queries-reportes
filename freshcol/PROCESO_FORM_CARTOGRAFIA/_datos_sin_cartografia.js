var data_form = db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //data_cartografia
                    {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }

                    , {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "nombre cartografia",
                            "foreignField": "properties.name",
                            "as": "data_cartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$data_cartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            _id_cartografia: { "$ifNull": ["$data_cartografia._id", null] }
                        }
                    }

                    , {
                        $match: {
                            _id_cartografia: { $eq: null },
                        }
                    }

                    // , {
                    //     "$addFields": {
                    //         "_id_feature_string": { $toString: "$data_cartografia._id" }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "data_cartografia._id": "$_id_feature_string"
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         //"Finca": "$data_cartografia"
                    //         "Finca": {
                    //             "type": "selection",
                    //             "features": [
                    //                 "$data_cartografia"
                    //             ],
                    //             "path": "$data_cartografia.path"
                    //         }
                    //     }
                    // }



                    // , {
                    //     "$project": {
                    //         "_id": 0
                    //         , "data_cartografia": 0
                    //         , "_id_cartografia": 0
                    //         , "_id_feature_string": 0
                    //     }
                    // }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //test
        // , { $limit: 10 }



    ]
)



data_form
