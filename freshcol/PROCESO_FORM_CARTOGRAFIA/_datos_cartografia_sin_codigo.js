db.cartography.aggregate(

    {
        $match:{
            type:"Feature"
            ,"properties.type":"trees"
        }
    },
    {
        $match:{
            "properties.name":{
                $not:{
                    $regex: ':'
                }
            }
        }
    }

)
