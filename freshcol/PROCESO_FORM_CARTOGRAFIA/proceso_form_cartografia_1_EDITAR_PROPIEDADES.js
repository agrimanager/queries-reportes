var data_form = db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    //finca_AG
                    {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }


                    // //test
                    // , {
                    //     $match: {
                    //         "FINCA AGRIMANAGER": "Antioquia",
                    //     }
                    // }



                    //data_cartografia
                    , {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }

                    , {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "nombre cartografia",
                            "foreignField": "properties.name",
                            "as": "data_cartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$data_cartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            _id_cartografia: { "$ifNull": ["$data_cartografia._id", null] }
                        }
                    }

                    , {
                        $match: {
                            _id_cartografia: { $ne: null },
                        }
                    }


                    , {
                        $match: {
                            "data_cartografia.properties.custom.Codigo del productor": { $exists: false },
                            "data_cartografia.properties.custom.Nombre de la finca": { $exists: false },
                            "data_cartografia.properties.custom.Vereda": { $exists: false },
                            "data_cartografia.properties.custom.Propietario": { $exists: false },
                            "data_cartografia.properties.custom.Cel del propietario": { $exists: false },
                            "data_cartografia.properties.custom.Latitud": { $exists: false },
                            "data_cartografia.properties.custom.Longitud": { $exists: false },
                        }
                    }


                    , {
                        "$addFields": {
                            "Longitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 0] },
                            "Latitud": { "$arrayElemAt": [{ "$ifNull": ["$data_cartografia.geometry.coordinates", []] }, 1] }
                        }
                    }

                    , {
                        "$addFields": {
                            "Longitud": { "$ifNull": ["$Longitud", 0] }
                            , "Latitud": { "$ifNull": ["$Latitud", 0] }
                        }
                    }

                    , {
                        "$project": {
                            "data_cartografia": 0
                            , "id_finca": 0

                            , "uid": 0
                            , "uDate": 0
                            , "Point": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "Fecha": "$rgDate"
                        }
                    }

                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //variables para editar cartografia
        , {
            "$addFields": {
                data_codigo_productor: { $toString: "$codigo productor" },
                data_nombre_finca: { $toString: "$Nombre de finca" },
                data_vereda: { $toString: "$Vereda" },
                data_propietario: { $toString: "$Nombre del Propietario" },
                data_cel_propietario: { $toString: "$Celular del propietario" },
                data_latitud: { $toString: "$Latitud" },
                data_longitud: { $toString: "$Longitud" },
            }
        }





    ]
)



// data_form


data_form.forEach(item_data_form => {

    db.cartography.update(
        {
            _id: item_data_form._id_cartografia
        },
        {
            $set: {
                "properties.custom.Codigo del productor": {
                    "value": item_data_form.data_codigo_productor,
                    "type": "string"
                },

                "properties.custom.Nombre de la finca": {
                    "value": item_data_form.data_nombre_finca,
                    "type": "string"
                },

                "properties.custom.Vereda": {
                    "value": item_data_form.data_vereda,
                    "type": "string"
                },

                "properties.custom.Propietario": {
                    "value": item_data_form.data_propietario,
                    "type": "string"
                },

                "properties.custom.Cel del propietario": {
                    "value": item_data_form.data_cel_propietario,
                    "type": "string"
                },

                "properties.custom.Latitud": {
                    "value": item_data_form.data_latitud,
                    "type": "string"
                },

                "properties.custom.Longitud": {
                    "value": item_data_form.data_longitud,
                    "type": "string"
                },

            }
        }
    )

})
