var data_form = db.users.aggregate(
    [

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }
                        }
                    }


                    //condiciones_1
                    , {
                        $match: {
                            supervisor: { $ne: "Support team" }
                        }
                    }

                    , {
                        $match: {
                            "codigo productor": {
                                $in: ["", "sin_codigo"]
                            }
                        }
                    }


                    //finca
                    , {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }


                    //codigo productor

                    /*
                    ----codigos de regiones (segun cartografia)
                    A1 = Sonson
                    A2 = Urrao
                    A3 = Antioquia
                    A4 = Suroeste
                    A5 = Caldas
                    A6 = Eje Cafetero
                    A7 = Tolima
                    A8 = Valle del Cauca
                    A9 = Cauca
                    A10 = Huila
                    A11 =
                    A12 = Cundinamarca
                    A13 =
                    A14 = Fresno

                    ??Cundinamarca
                    ??Putumayo
                    */

                    , {
                        "$addFields": {
                            "codigo_region": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Sonson"] },
                                            "then": "A1"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Urrao"] },
                                            "then": "A2"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Antioquia"] },
                                            "then": "A3"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Suroeste"] },
                                            "then": "A4"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Caldas"] },
                                            "then": "A5"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Eje Cafetero"] },
                                            "then": "A6"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Tolima"] },
                                            "then": "A7"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Valle del Cauca"] },
                                            "then": "A8"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Cauca"] },
                                            "then": "A9"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Huila"] },
                                            "then": "A10"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Cundinamarca"] },
                                            "then": "A12"
                                        }
                                        , {
                                            "case": { "$eq": ["$FINCA AGRIMANAGER", "Fresno"] },
                                            "then": "A14"
                                        }
                                    ], "default": "--------"
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "codigo_visita": { "$toString": "$Numero de visita" }
                        }
                    }

                    , {
                        "$addFields": {
                            "finca_num_letras": { "$strLenCP": "$Nombre de finca" }
                        }
                    }

                    , {
                        "$addFields": {
                            "finca_num_letras2": {
                                "$subtract": ["$finca_num_letras", 2]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "codigo_finca": {
                                "$cond": {
                                    "if": { "$eq": ["$finca_num_letras", 0] }
                                    , "then": ""
                                    , "else": {
                                        "$substr": ["$Nombre de finca", "$finca_num_letras2", -1]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "codigo_finca": {
                                "$toUpper": "$codigo_finca"
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "CODIGO_DEL_PRODUCTOR": {
                                "$concat": ["$codigo_region", ":", "$codigo_visita", "$codigo_finca"]
                            }
                        }
                    }


                    , {
                        "$project": {
                            "finca_num_letras": 0,
                            "finca_num_letras2": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "CARTOGRAFIA": {
                                "$concat": ["$Nombre de finca", " ", "$CODIGO_DEL_PRODUCTOR"]
                            }
                        }
                    }



                    //municipio
                    //--Maestro principal
                    , {
                        "$addFields": {
                            "nombre_maestro_principal": "Departamento y Municipio_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }


                    //--maestro enlazado
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    //"then": "$$dataKV.k",
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    //----FALLA EN LA WEB
                    /*
                    , {
                        "$unwind": {
                            "path": "$nombre_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    */

                    //---//obtener el primero (aveces falla)
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] }
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }




                    //--Valor maestro enlazado
                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "MUNICIPIO_MAESTRO_ENLAZADO": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }




                    //----CARTOGRAFIA
                    , {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "CARTOGRAFIA",
                            "foreignField": "properties.name",
                            "as": "data_cartografia"
                        }
                    }

                    , {
                        $match: {
                            data_cartografia: { $eq: [] }
                        }
                    }



                    //--item_cartografia para insert
                    /*
                    {
                    	"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                    	"type" : "Feature",
                    	"geometry" : {
                    		"type" : "Point",
                    		"coordinates" : [
                    			-75.21604919433594,
                    			6.20022439956665
                    		]
                    	},
                    	"properties" : {
                    		"type" : "trees",
                    		"name" : "La piedra",
                    		"production" : true,
                    		"status" : true,
                    		"custom" : {
                    			"Codigo del productor" : {
                            		"value" : "A8:75TA",
                            		"type" : "string"
                            	},
                    		}
                    	},
                    	"path" : ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
                    }
                    */

                    //     "geometry" : {
                    // 		"type" : "Point",
                    // 		"coordinates" : [
                    // 			6.228152295570652,
                    // 			-75.57598963764835
                    // 		]
                    // 	}

                    // , {
                    //     "$addFields": {
                    //         "ITEM_CARTOGRAFIA_INSERT": {
                    //             //"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                    //             "type": "Feature",
                    //             "geometry": {
                    //                 "type": "Point",
                    //                 "coordinates": [
                    //                     -75.21604919433594,
                    //                     6.20022439956665
                    //                 ]
                    //             },
                    //             "properties": {
                    //                 "type": "trees",
                    //                 "name": "La piedra",
                    //                 "production": true,
                    //                 "status": true,
                    //                 "custom": {
                    //                     "Codigo del productor": {
                    //                         "value": "A8:75TA",
                    //                         "type": "string"
                    //                     },
                    //                 }
                    //             },
                    //             "path": ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
                    //         }
                    //     }
                    // }

                    , {
                        "$addFields": {
                            "ITEM_CARTOGRAFIA_INSERT": {
                                //"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                                "type": "Feature",
                                //"geometry": "$Point.geometry"
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [
                                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 1] },
                                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 0] }
                                    ]
                                }
                                , "properties": {
                                    "type": "trees",
                                    "name": "$CARTOGRAFIA",
                                    "production": true,
                                    "status": true,
                                    "custom": {
                                        "Codigo del productor": {
                                            "value": "$CODIGO_DEL_PRODUCTOR",
                                            "type": "string"
                                        },
                                    }
                                },
                                "path": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Sonson"] },
                                                "then": ",62cd92723cf9784d38eb0743,633352c3c7fa8e99d502bad7,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Urrao"] },
                                                "then": ",632873173cf9784d38eb1357,63333235c7fa8e99d502b757,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Antioquia"] },
                                                "then": ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Suroeste"] },
                                                "then": ",6328768f3cf9784d38eb1359,63335942c7fa8e99d502c094,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Caldas"] },
                                                "then": ",632872833cf9784d38eb1355,63330f78bf48fd5f2ae52a4e,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Eje Cafetero"] },
                                                "then": ",632871e03cf9784d38eb1351,6334d1f5c7fa8e99d502c388,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Tolima"] },
                                                "then": ",62dfdf753cf9784d38eb0797,632c794abf48fd5f2ae52a4d,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Valle del Cauca"] },
                                                "then": ",633adbb82951a04f62d56c86,6346e6d8c7fa8e99d503963a,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cauca"] },
                                                "then": ",633adbe82951a04f62d56c88,633e1658c7fa8e99d5036089,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Huila"] },
                                                "then": ",632872113cf9784d38eb1353,63337653c7fa8e99d502c2d4,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Fresno"] },
                                                "then": ",632871483cf9784d38eb134f,6333672ac7fa8e99d502c0f9,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cundinamarca"] },
                                                "then": ",633adc4b2951a04f62d56c8c,6346fba2c7fa8e99d5039972,"
                                            }
                                        ], "default": "--------"
                                    }
                                }
                            }
                        }
                    }


                    //--path bloques
                    //                     ﻿"_id"	"bloque"	"path_bloque"
                    // "ObjectId(""63333235c7fa8e99d502b757"")"	"Urrao"	""
                    // "ObjectId(""63163da7c7fa8e99d501af8c"")"	"Antioquia"	""
                    // "ObjectId(""632c794abf48fd5f2ae52a4d"")"	"Tolima"	""
                    // "ObjectId(""63330f78bf48fd5f2ae52a4e"")"	"Caldas"	""
                    // "ObjectId(""633352c3c7fa8e99d502bad7"")"	"Sonson"	""
                    // "ObjectId(""63335942c7fa8e99d502c094"")"	"Suroeste"	""
                    // "ObjectId(""6333672ac7fa8e99d502c0f9"")"	"Fresno"	""
                    // "ObjectId(""63337653c7fa8e99d502c2d4"")"	"Huila"	""
                    // "ObjectId(""6334d1f5c7fa8e99d502c388"")"	"Eje Cafetero"	""
                    // "ObjectId(""633e1658c7fa8e99d5036089"")"	"Cauca"	""
                    // "ObjectId(""6346e6d8c7fa8e99d503963a"")"	"Valle del Cauca"	""
                    // "ObjectId(""6346fba2c7fa8e99d5039972"")"	"Cundinamarca"	""





                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //test
        , { $limit: 1 }



    ]
)


// data_form



// data_form.forEach(item_data_form => {

//     //====1)actualizar dato de formulario
//     db.form_registroprediodeaguacate.update(
//         {
//             _id: item_data_form._id
//         },
//         {
//             $set: {
//                 "nombre cartografia": item_data_form.CARTOGRAFIA
//                 , "codigo productor": item_data_form.CODIGO_DEL_PRODUCTOR
//                 , "Municipio": item_data_form.MUNICIPIO_MAESTRO_ENLAZADO
//             }
//         }

//     )

//     //====2)insertar en cartografia
//     db.cartography.insert(
//         item_data_form.ITEM_CARTOGRAFIA_INSERT
//     )

// })
