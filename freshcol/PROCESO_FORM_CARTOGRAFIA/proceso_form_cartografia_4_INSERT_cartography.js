var data_form = db.users.aggregate(
    [

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }
                        }
                    }


                    //condiciones_1
                    , {
                        $match: {
                            "Departamento y Municipio": "Cundinamarca",
                        }
                    }

                    , {
                        $match: {
                            "nombre cartografia": {
                                $not: {
                                    $in: ["", "sin_cartografia"]
                                }
                            }
                        }
                    }

                    , {
                        $match: {
                            "Nombre de finca": {
                                $not: {
                                    $in: [""]
                                }
                            }
                        }
                    }


                    //finca
                    , {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }



                    , {
                        "$addFields": {
                            "ITEM_CARTOGRAFIA_INSERT": {
                                //"_id" : ObjectId("63475a1d3d493844d79dfff4"),
                                "type": "Feature",
                                //"geometry": "$Point.geometry"
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [
                                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 0] },
                                        { "$arrayElemAt": [{ "$ifNull": ["$Point.geometry.coordinates", []] }, 1] }
                                    ]
                                }
                                , "properties": {
                                    "type": "trees",
                                    "name": "$nombre cartografia",
                                    "production": true,
                                    "status": true,
                                    "custom": {
                                        "Codigo del productor": {
                                            "value": "$codigo productor",
                                            "type": "string"
                                        },
                                    }
                                },
                                "path": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Sonson"] },
                                                "then": ",62cd92723cf9784d38eb0743,633352c3c7fa8e99d502bad7,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Urrao"] },
                                                "then": ",632873173cf9784d38eb1357,63333235c7fa8e99d502b757,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Antioquia"] },
                                                "then": ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Suroeste"] },
                                                "then": ",6328768f3cf9784d38eb1359,63335942c7fa8e99d502c094,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Caldas"] },
                                                "then": ",632872833cf9784d38eb1355,63330f78bf48fd5f2ae52a4e,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Eje Cafetero"] },
                                                "then": ",632871e03cf9784d38eb1351,6334d1f5c7fa8e99d502c388,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Tolima"] },
                                                "then": ",62dfdf753cf9784d38eb0797,632c794abf48fd5f2ae52a4d,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Valle del Cauca"] },
                                                "then": ",633adbb82951a04f62d56c86,6346e6d8c7fa8e99d503963a,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cauca"] },
                                                "then": ",633adbe82951a04f62d56c88,633e1658c7fa8e99d5036089,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Huila"] },
                                                "then": ",632872113cf9784d38eb1353,63337653c7fa8e99d502c2d4,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Fresno"] },
                                                "then": ",632871483cf9784d38eb134f,6333672ac7fa8e99d502c0f9,"
                                            }
                                            , {
                                                "case": { "$eq": ["$FINCA AGRIMANAGER", "Cundinamarca"] },
                                                "then": ",633adc4b2951a04f62d56c8c,6346fba2c7fa8e99d5039972,"
                                            }
                                        ], "default": "--------"
                                    }
                                }
                            }
                        }
                    }




                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //test
        // , { $limit: 1 }



    ]
)


// data_form



data_form.forEach(item_data_form => {

    // //====1)actualizar dato de formulario
    // db.form_registroprediodeaguacate.update(
    //     {
    //         _id: item_data_form._id
    //     },
    //     {
    //         $set: {
    //             "nombre cartografia": item_data_form.CARTOGRAFIA
    //             , "codigo productor": item_data_form.CODIGO_DEL_PRODUCTOR
    //             , "Municipio": item_data_form.MUNICIPIO_MAESTRO_ENLAZADO
    //         }
    //     }

    // )

    //====2)insertar en cartografia
    db.cartography.insert(
        item_data_form.ITEM_CARTOGRAFIA_INSERT
    )

})
