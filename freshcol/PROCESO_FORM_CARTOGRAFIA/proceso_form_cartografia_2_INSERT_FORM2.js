var data_form = db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //data_cartografia
                    {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }

                    , {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "nombre cartografia",
                            "foreignField": "properties.name",
                            "as": "data_cartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$data_cartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            _id_cartografia: { "$ifNull": ["$data_cartografia._id", null] }
                        }
                    }

                    , {
                        $match: {
                            _id_cartografia: { $ne: null },
                        }
                    }

                    , {
                        "$addFields": {
                            "_id_feature_string": { $toString: "$data_cartografia._id" }
                        }
                    }

                    , {
                        "$addFields": {
                            "data_cartografia._id": "$_id_feature_string"
                        }
                    }


                    , {
                        "$addFields": {
                            //"Finca": "$data_cartografia"
                            "Finca": {
                                "type": "selection",
                                "features": [
                                    "$data_cartografia"
                                ],
                                "path": "$data_cartografia.path"
                            }
                        }
                    }



                    , {
                        "$project": {
                            "_id": 0
                            , "data_cartografia": 0
                            , "_id_cartografia": 0
                            , "_id_feature_string": 0
                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //test
        // , { $limit: 10 }



        //--actualizacion 2022-10-12
        //datos que no esten en form2

        , {
            "$lookup": {
                "from": "form_seguimientoregistroprediodeaguacate",
                "localField": "nombre cartografia",
                "foreignField": "nombre cartografia",
                "as": "data_form2"
            }
        }
        , {
            "$unwind": {
                "path": "$data_form2",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                _id_data_form2: { "$ifNull": ["$data_form2._id", null] }
            }
        }

        , {
            $match: {
                _id_data_form2: { $eq: null },
            }
        }

        , {
            "$project": {
                "data_form2": 0
                , "_id_data_form2": 0
            }
        }




    ]
)


// data_form



////---cartografia_form
// {
// 	"type" : "selection",
// 	"features" : [
// 		{
// 			"_id" : "631bcd07c7fa8e99d501fcf6",
// 			"type" : "Feature",
// 			"path" : ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,",
// 			"properties" : {
// 				"type" : "trees",
// 				"name" : "El Guadal A3:311AL",
// 				"production" : true,
// 				"status" : true,
// 				"order" : NumberInt(0),
// 				"custom" : {
// 					"color" : {
// 						"type" : "color",
// 						"value" : "#39AE37"
// 					}
// 				}
// 			},
// 			"geometry" : {
// 				"type" : "Point",
// 				"coordinates" : [
// 					-75.2584979,
// 					6.2141644
// 				]
// 			}
// 		}
// 	],
// 	"path" : ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
// }


////cartografia
// collection: cartography
// {
// 	"_id" : ObjectId("631bcd07c7fa8e99d501fcf6"),
// 	"type" : "Feature",
// 	"properties" : {
// 		"name" : "El Guadal A3:311AL",
// 		"type" : "trees",
// 		"production" : true,
// 		"status" : true,
// 		"custom" : {
// 			"Cel del propietario" : {
// 				"value" : "3218564397",
// 				"type" : "string"
// 			},
// 			"Codigo del productor" : {
// 				"value" : "A3:311AL",
// 				"type" : "string"
// 			},
// 			"Latitud" : {
// 				"value" : "6.21416",
// 				"type" : "string"
// 			},
// 			"Longitud" : {
// 				"value" : "-75.2585",
// 				"type" : "string"
// 			},
// 			"Nombre de la finca" : {
// 				"value" : "El Guadal",
// 				"type" : "string"
// 			},
// 			"Propietario" : {
// 				"value" : "Diego Osorio",
// 				"type" : "string"
// 			},
// 			"Vereda" : {
// 				"value" : "El Pozo",
// 				"type" : "string"
// 			}
// 		}
// 	},
// 	"geometry" : {
// 		"type" : "Point",
// 		"coordinates" : [
// 			-75.2584979,
// 			6.2141644
// 		]
// 	},
// 	"path" : ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"
// }


//---vs
//cartografia_form
//_id -->>string
//"type" : "selection",
//"path" : ",62dfdefb3cf9784d38eb0795,63163da7c7fa8e99d501af8c,"



// data_form



data_form.forEach(item_data_form => {

    db.form_seguimientoregistroprediodeaguacate.insert(
        item_data_form
    )

})
