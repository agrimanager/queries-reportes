

var data_cartografia_ids = [

    ObjectId("633352c3c7fa8e99d502bd43"),
    ObjectId("633352c4c7fa8e99d502be34"),
    ObjectId("63506afc578f64347809a0ca"),
    ObjectId("641bb06b4130373d309d484a"),
    ObjectId("641bb0a44130373d309d49ef"),
    ObjectId("641bb0c44130373d309d4ad4"),
    ObjectId("641bb0a14130373d309d49de"),
    ObjectId("641bb0ca4130373d309d4afb"),
    ObjectId("64475b42ae299d4874ac1a78"),

]



var data_cartografia = db.cartography.aggregate(

    {
        $match: {
            _id: { $in: data_cartografia_ids }
        }
    }


    , {
        "$group": {
            "_id": null
            , "ids": { "$push": "$_id" }
            , "names": { "$push": "$properties.name" }
        }
    },
    {
        "$project": {
            "_id": 0
            , "ids": 1
            , "names": 1
        }
    }

)



// data_cartografia

var data_cartografia_clone = data_cartografia.clone()
var data_registro = null
var data_seguimiento = null

data_cartografia.forEach(item_data_cartografia => {

    data_registro = db.form_registroprediodeaguacate.aggregate(

        {
            $match: {
                "nombre cartografia": { $in: item_data_cartografia.names }
            }
        }


        , {
            "$group": {
                "_id": null
                , "ids": { "$push": "$_id" }
            }
        },
        {
            "$project": {
                "_id": 0
                , "ids": 1
            }
        }

    )


    data_seguimiento = db.form_seguimientoregistroprediodeaguacate.aggregate(

        {
            $match: {
                // "nombre cartografia": { $in: item_data_cartografia.names }
                "Finca.features.properties.name": { $in: item_data_cartografia.names }
            }
        }


        , {
            "$group": {
                "_id": null
                , "ids": { "$push": "$_id" }
            }
        },
        {
            "$project": {
                "_id": 0
                , "ids": 1
            }
        }

    )


})



////---->>OJO REVISAR ANTES DE BORRAR

// data_cartografia_clone
// data_registro
// data_seguimiento


/*
--->>respuesta ticket
-archivo: AAAAAAAAAAAA.xlsx
datos BORRADOS:
cartografia:  XXXXX datos BORRADOS.
registro: YYYYY datos BORRADOS.
seguimiento: ZZZZZZ datos BORRADOS.

*/



//======BORRAR DATOS

//data_cartografia_clone
data_cartografia_clone.forEach(item_data_cartografia_clone => {
    db.cartography.remove(
        {
            "_id": {
                "$in": item_data_cartografia_clone.ids
            }
        }
    );
})


//data_registro
data_cartografia.forEach(item_data_registro => {
    db.form_registroprediodeaguacate.remove(
        {
            "_id": {
                "$in": item_data_registro.ids
            }
        }
    );
})


//data_seguimiento
data_cartografia.forEach(item_data_seguimiento => {
    db.form_seguimientoregistroprediodeaguacate.remove(
        {
            "_id": {
                "$in": item_data_seguimiento.ids
            }
        }
    );
})


console.log("FIN EXITOSO")
/*
{
	"message" : "Cursor is closed",
	"driver" : true,
	"name" : "MongoError"
}
*/



//!!!!!....LUEGO GENERAR CARTOGRAFIA DE FINCAS

/*

Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632871e03cf9784d38eb1351.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62cd92723cf9784d38eb0743.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62dfdefb3cf9784d38eb0795.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/62dfdf753cf9784d38eb0797.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632871483cf9784d38eb134f.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632872113cf9784d38eb1353.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632872833cf9784d38eb1355.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/632873173cf9784d38eb1357.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/6328768f3cf9784d38eb1359.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adbb82951a04f62d56c86.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adbe82951a04f62d56c88.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adc1b2951a04f62d56c8a.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/633adc4b2951a04f62d56c8c.sqlite  -Method POST
Invoke-WebRequest -Uri https://map.agrimanager.app/freshcol/api/mobile/6479080dc23f316ad9d3eb78.sqlite  -Method POST



*/
