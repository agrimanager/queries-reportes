var data = db.cartography.aggregate(
    {
        $match: {
            "properties.type": "trees",

            path: {
                //$regex: `^,6479080dc23f316ad9d3eb78,` ////----Narino // A13-->>A11
                $regex: `^,633adc1b2951a04f62d56c8a,` ////----Putumayo // A11-->>A13
            }
        }
    }

)


data.forEach(i => {


    //cartografia
    var nombre_cartografia_old = i.properties.name;

    // var nombre_cartografia_new = nombre_cartografia_old.replace("A13:", "A11:");//Narino
    var nombre_cartografia_new = nombre_cartografia_old.replace("A11:", "A13:");//Putumayo

    console.log("-----")
    console.log(nombre_cartografia_old)
    console.log(nombre_cartografia_new)

    //codigo_productor
    var codigo_productor_old = i.properties.custom["Codigo del productor"].value;

    // var codigo_productor_new = codigo_productor_old.replace("A13:", "A11:");//Narino
    var codigo_productor_new = codigo_productor_old.replace("A11:", "A13:");//Putumayo

    console.log(codigo_productor_old)
    console.log(codigo_productor_new)


    db.cartography.update(
        {
            _id: i._id
        },
        {
            $set: {
                "properties.name": nombre_cartografia_new
                , "properties.custom.Codigo del productor.value": codigo_productor_new
            }
        }
    )


})
