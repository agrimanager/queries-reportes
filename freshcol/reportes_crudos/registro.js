db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registroprediodeaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "id_finca": "$Point.farm"
                        }
                    }

                    , {
                        "$addFields": {
                            "id_finca": { "$toObjectId": "$id_finca" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "id_finca",
                            "foreignField": "_id",
                            "as": "FINCA AGRIMANAGER"
                        }
                    },
                    { "$unwind": "$FINCA AGRIMANAGER" },

                    { "$addFields": { "FINCA AGRIMANAGER": { "$ifNull": ["$FINCA AGRIMANAGER.name", "no existe"] } } }






                    , {
                        "$addFields": {
                            "nombre cartografia": { "$ifNull": ["$nombre cartografia", "sin_cartografia"] }
                            , "codigo productor": { "$ifNull": ["$codigo productor", "sin_codigo"] }
                            , "Municipio": { "$ifNull": ["$Municipio", ""] }

                            , "Cumple con Buenas Practicas Agricolas BPA": { "$ifNull": ["$Cumple con Buenas Practicas Agricolas BPA", ""] }

                        }
                    }


                    , {
                        "$addFields": {
                            "Longitud": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
                            "Latitud": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
                        }
                    }


                    , {
                        "$project": {
                            "Formula": 0,
                            "Point": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
