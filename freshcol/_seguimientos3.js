db.form_seguimientoregistroprediodeaguacate.aggregate(
    [
        {
            "$match": {
                "$and": [{
                    "Point.farm": "62dfdefb3cf9784d38eb0795"
                }, {
                    "rgDate": {
                        // "$gte": "2022-08-21T00:00:00.000Z"
                        "$gte":ISODate("2022-08-21T00:00:00.000Z")
                    }
                }, {
                    "Finca.path": {
                        "$regex": ",6352bac3bf48fd5f2ae52a4f,"
                    }
                }]
            }
        }
        , {
            "$addFields": {
                "elementsTaken": "$Finca.features._id",
                "cartography": "$Finca",
                "formName": "test1"
            }
        }
        // , {
        //     "$sort": {
        //         "rgDate": -1
        //     }
        // }
        // , {
        //     "$group": {
        //         "_id": "$Finca.features.properties.name",
        //         "r": {
        //             "$first": "$$ROOT"
        //         }
        //     }
        // }
        // , {
        //     "$replaceRoot": {
        //         "newRoot": "$r"
        //     }
        // }
        // , {
        //     "$unwind": "$elementsTaken"
        // }
        // , {
        //     "$project": {
        //         "uid": 0,
        //         "Finca": 0
        //     }
        // }
        // , {
        //     "$project": {
        //         "elementsTaken": "$elementsTaken",
        //         "formId": "636eada3f9b8b221a674b7e8",
        //         "formName": "test1",
        //         "rgDate": "$rgDate",
        //         "data": "$$ROOT"
        //     }
        // }
    ]


)
