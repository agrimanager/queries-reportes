


db.cartography.aggregate(

    {
        $match: {
            "properties.type": "trees"
        }
    }


    , {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": "si"
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } }



    , {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } }




    // , {
    //     $project: {
    //         nombre: "$properties.name",
    //         finca: "$finca",
    //         // path: "$path",
    //         // propiedades: "$properties.custom"

    //         "Codigo del productor": { $ifNull: ["$properties.custom.Codigo del productor.value", ""] },
    //         "Nombre de la finca": { $ifNull: ["$properties.custom.Nombre de la finca.value", ""] },
    //         "Vereda": { $ifNull: ["$properties.custom.Vereda.value", ""] },
    //         "Propietario": { $ifNull: ["$properties.custom.Propietario.value", ""] },
    //         "Cel del propietario": { $ifNull: ["$properties.custom.Cel del propietario.value", ""] },
    //         "Latitud": { $ifNull: ["$properties.custom.Latitud.value", ""] },
    //         "Longitud": { $ifNull: ["$properties.custom.Longitud.value", ""] },

    //         lon: {$arrayElemAt: ["$geometry.coordinates", 0]},
    //         lat: {$arrayElemAt: ["$geometry.coordinates", 1]},



    //     }
    // }
    ////-----------------------------



    , {
        $project: {
            nombre_cartografia: "$properties.name",
            finca_AG: "$finca",
            bloque_AG: "$bloque",
            status_AG: "$properties.status",
            production_AG: "$properties.production",

            Longitud: { $arrayElemAt: ["$geometry.coordinates", 0] },
            Latitud: { $arrayElemAt: ["$geometry.coordinates", 1] },

            "tiene_propiedades": {
                "$cond": {
                    "if": { "$eq": ["$properties.custom", {}] },
                    "then": "no",
                    "else": "si"
                }
            },


            "prop_Codigo del productor": { $ifNull: ["$properties.custom.Codigo del productor.value", ""] },
            "prop_Nombre de la finca": { $ifNull: ["$properties.custom.Nombre de la finca.value", ""] },
            "prop_Vereda": { $ifNull: ["$properties.custom.Vereda.value", ""] },
            "prop_Propietario": { $ifNull: ["$properties.custom.Propietario.value", ""] },
            "prop_Cel del propietario": { $ifNull: ["$properties.custom.Cel del propietario.value", ""] },
            "prop_Longitud": { $ifNull: ["$properties.custom.Longitud.value", ""] },
            "prop_Latitud": { $ifNull: ["$properties.custom.Latitud.value", ""] },



        }
    }


)
