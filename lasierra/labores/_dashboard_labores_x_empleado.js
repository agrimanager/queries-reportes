db.form_puentereportes.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-03-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "tasks",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [


                    {
                        "$addFields": {
                            "crop": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$crop" }, "object"] },
                                    "then": [],
                                    "else": "$crop"
                                }
                            },
                            "lots": {
                                "$cond": {
                                    "if": { "$eq": ["$lots", {}] },
                                    "then": [],
                                    "else": "$lots"
                                }
                            }
                        }
                    },


                    {
                        "$match": {
                            "active": true
                        }
                    },



                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }, { "$unwind": "$user_timezone" }, { "$addFields": { "user_timezone": "$user_timezone.timezone" } }, {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    }, { "$unwind": "$activity" }, {
                        "$lookup": {
                            "from": "costsCenters",
                            "localField": "ccid",
                            "foreignField": "_id",
                            "as": "costsCenter"
                        }
                    }, { "$unwind": "$costsCenter" }, {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    }, { "$unwind": "$farm" }, {
                        "$lookup": {
                            "from": "employees",
                            "localField": "supervisor",
                            "foreignField": "_id",
                            "as": "supervisor"
                        }
                    }, {
                        "$unwind": {
                            "path": "$supervisor",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, {
                        "$addFields": {
                            "employees": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$employees" }, "array"] },
                                    "then": "$employees",
                                    "else": { "$map": { "input": { "$objectToArray": "$employees" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                                }
                            },
                            "productivityReport": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$productivityReport" }, "array"] },
                                    "then": "$productivityReport",
                                    "else": {
                                        "$map": {
                                            "input": { "$objectToArray": "$productivityReport" },
                                            "as": "productivityReportKV",
                                            "in": "$$productivityReportKV.v"
                                        }
                                    }
                                }
                            },
                            "lots": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$lots" }, "array"] },
                                    "then": "$lots",
                                    "else": { "$map": { "input": { "$objectToArray": "$lots" }, "as": "lotKV", "in": "$$lotKV.v" } }
                                }
                            }
                        }
                    }, { "$addFields": { "total_productos_seleccionados_labor": { "$size": "$supplies" } } }, {
                        "$unwind": {
                            "path": "$productivityReport",
                            "includeArrayIndex": "arrayIndex",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , { "$addFields": { "employees": { "$toObjectId": "$productivityReport.employee" } } }
                    , {
                        "$unwind": {
                            "path": "$productivityReport",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, { "$addFields": { "ProductividadEmpleado": { "$toDouble": "$productivityReport.quantity" } } }, {
                        "$lookup": {
                            "from": "employees",
                            "localField": "employees",
                            "foreignField": "_id",
                            "as": "Empleado"
                        }
                    }, {
                        "$unwind": {
                            "path": "$Empleado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, { "$addFields": { "TotalPagoEmpleado": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$ProductividadEmpleado" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] } } }, {
                        "$addFields": {
                            "productos": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$supplies" }, "array"] },
                                    "then": "$supplies",
                                    "else": { "$map": { "input": { "$objectToArray": "$employees" }, "as": "suppliesKV", "in": "$$suppliesKV.v" } }
                                }
                            }
                        }
                    }, {
                        "$addFields": {
                            "productos": {
                                "$map": {
                                    "input": "$productos._id",
                                    "as": "strid",
                                    "in": { "$toObjectId": "$$strid" }
                                }
                            }
                        }
                    }, { "$lookup": { "from": "supplies", "localField": "productos", "foreignField": "_id", "as": "productos" } }, {
                        "$addFields": {
                            "user": { "$arrayElemAt": ["$user.timezone", 0] },
                            "productos": {
                                "$reduce": {
                                    "input": "$productos.name",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Etiquetas": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$tags",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin etiquetas--"]
                            },
                            "Tipo_cultivo": {
                                "$reduce": {
                                    "input": "$crop", "initialValue": "", "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] }, "then": {
                                                "$switch": {
                                                    "branches": [{
                                                        "case": { "$eq": ["$$this", "coffee"] },
                                                        "then": "Café"
                                                    }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                        "case": { "$eq": ["$$this", "avocado"] },
                                                        "then": "Aguacate"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "orange"] },
                                                        "then": "Naranja"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tangerine"] },
                                                        "then": "Mandarina"
                                                    }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                        "case": { "$eq": ["$$this", "cacao"] },
                                                        "then": "Cacao"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                        "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "vegetable"] },
                                                        "then": "Hortalizas"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                        "then": "Frutos tropicales"
                                                    }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                        "case": { "$eq": ["$$this", "flower"] },
                                                        "then": "Flores"
                                                    }, { "case": { "$eq": ["$$this", "cereal"] }, "then": "Cereales" }, {
                                                        "case": { "$eq": ["$$this", "cattle"] },
                                                        "then": "Ganado"
                                                    }, { "case": { "$eq": ["$$this", "pork"] }, "then": "Cerdos" }, {
                                                        "case": { "$eq": ["$$this", "apiculture"] },
                                                        "then": "Apicultura"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-birds"] },
                                                        "then": "Otras aves"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "chicken"] },
                                                        "then": "Pollos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                        "then": "Animales acuáticos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-animals"] },
                                                        "then": "Otros animales"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "woods"] },
                                                        "then": "Bosques"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "greenhouses"] },
                                                        "then": "Invernaderos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                        "then": "Otros (no limitativo)"
                                                    }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                        "case": { "$eq": ["$$this", "yucca"] },
                                                        "then": "Yuca"
                                                    }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                                }
                                            }, "else": {
                                                "$concat": ["$$value", "; ", {
                                                    "$switch": {
                                                        "branches": [{
                                                            "case": { "$eq": ["$$this", "coffee"] },
                                                            "then": "Café"
                                                        }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                            "case": { "$eq": ["$$this", "avocado"] },
                                                            "then": "Aguacate"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "orange"] },
                                                            "then": "Naranja"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "tangerine"] },
                                                            "then": "Mandarina"
                                                        }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                            "case": { "$eq": ["$$this", "cacao"] },
                                                            "then": "Cacao"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                            "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "vegetable"] },
                                                            "then": "Hortalizas"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                            "then": "Frutos tropicales"
                                                        }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                            "case": { "$eq": ["$$this", "flower"] },
                                                            "then": "Flores"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "cereal"] },
                                                            "then": "Cereales"
                                                        }, { "case": { "$eq": ["$$this", "cattle"] }, "then": "Ganado" }, {
                                                            "case": { "$eq": ["$$this", "pork"] },
                                                            "then": "Cerdos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "apiculture"] },
                                                            "then": "Apicultura"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-birds"] },
                                                            "then": "Otras aves"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "chicken"] },
                                                            "then": "Pollos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                            "then": "Animales acuáticos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-animals"] },
                                                            "then": "Otros animales"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "woods"] },
                                                            "then": "Bosques"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "greenhouses"] },
                                                            "then": "Invernaderos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                            "then": "Otros (no limitativo)"
                                                        }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                            "case": { "$eq": ["$$this", "yucca"] },
                                                            "then": "Yuca"
                                                        }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                                    }
                                                }]
                                            }
                                        }
                                    }
                                }
                            },
                            "Blancos_biologicos": {
                                "$reduce": {
                                    "input": {
                                        "$cond": {
                                            "if": { "$eq": ["$biologicalTarget", ""] },
                                            "then": null,
                                            "else": "$biologicalTarget"
                                        }
                                    },
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_mapa_tipos": {
                                "$reduce": {
                                    "input": {
                                        "$reduce": {
                                            "input": "$cartography.features.properties.type",
                                            "initialValue": [],
                                            "in": {
                                                "$cond": {
                                                    "if": { "$lt": [{ "$indexOfArray": ["$$value", "$$this"] }, 0] },
                                                    "then": { "$concatArrays": ["$$value", ["$$this"]] },
                                                    "else": "$$value"
                                                }
                                            }
                                        }
                                    }, "initialValue": "", "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] }, "then": {
                                                "$switch": {
                                                    "branches": [{
                                                        "case": { "$eq": ["$$this", "blocks"] },
                                                        "then": "Bloque"
                                                    }, { "case": { "$eq": ["$$this", "lot"] }, "then": "Lotes" }, {
                                                        "case": { "$eq": ["$$this", "lines"] },
                                                        "then": "Linea"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "trees"] },
                                                        "then": "Árboles"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "fruitCenters"] },
                                                        "then": "Centro frutero"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "samplingPolygons"] },
                                                        "then": "Poligono de muestreo"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "valves"] },
                                                        "then": "Valvula"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "drainages"] },
                                                        "then": "Drenaje"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "sprinklers"] },
                                                        "then": "Aspersors"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkOne"] },
                                                        "then": "Red de riego uno"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkTwo"] },
                                                        "then": "Red de riego dos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "irrigationNetworkThree"] },
                                                        "then": "Red de riego tres"
                                                    }, { "case": { "$eq": ["$$this", "traps"] }, "then": "Trampa" }, {
                                                        "case": { "$eq": ["$$this", "lanes"] },
                                                        "then": "Vías"
                                                    }, { "case": { "$eq": ["$$this", "woods"] }, "then": "Bosque" }, {
                                                        "case": { "$eq": ["$$this", "sensors"] },
                                                        "then": "Sensor"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "cableways"] },
                                                        "then": "Cable vía"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "buildings"] },
                                                        "then": "Edificio"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "waterBodies"] },
                                                        "then": "Cuerpo de agua"
                                                    }, { "case": { "$eq": ["$$this", "additionalPolygons"] }, "then": "Poligonos adicionales" }],
                                                    "default": "--------"
                                                }
                                            }, "else": {
                                                "$concat": ["$$value", "; ", {
                                                    "$switch": {
                                                        "branches": [{
                                                            "case": { "$eq": ["$$this", "blocks"] },
                                                            "then": "Bloque"
                                                        }, { "case": { "$eq": ["$$this", "lot"] }, "then": "Lotes" }, {
                                                            "case": { "$eq": ["$$this", "lines"] },
                                                            "then": "Linea"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "trees"] },
                                                            "then": "Árboles"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "fruitCenters"] },
                                                            "then": "Centro frutero"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "samplingPolygons"] },
                                                            "then": "Poligono de muestreo"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "valves"] },
                                                            "then": "Valvula"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "drainages"] },
                                                            "then": "Drenaje"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "sprinklers"] },
                                                            "then": "Aspersors"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkOne"] },
                                                            "then": "Red de riego uno"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkTwo"] },
                                                            "then": "Red de riego dos"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "irrigationNetworkThree"] },
                                                            "then": "Red de riego tres"
                                                        }, { "case": { "$eq": ["$$this", "traps"] }, "then": "Trampa" }, {
                                                            "case": { "$eq": ["$$this", "lanes"] },
                                                            "then": "Vías"
                                                        }, { "case": { "$eq": ["$$this", "woods"] }, "then": "Bosque" }, {
                                                            "case": { "$eq": ["$$this", "sensors"] },
                                                            "then": "Sensor"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "cableways"] },
                                                            "then": "Cable vía"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "buildings"] },
                                                            "then": "Edificio"
                                                        }, {
                                                            "case": { "$eq": ["$$this", "waterBodies"] },
                                                            "then": "Cuerpo de agua"
                                                        }, { "case": { "$eq": ["$$this", "additionalPolygons"] }, "then": "Poligonos adicionales" }],
                                                        "default": "--------"
                                                    }
                                                }]
                                            }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_mapa_elementos": {
                                "$reduce": {
                                    "input": "$cartography.features.properties.name",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },
                            "Cartografia_seleccionada_lista_lotes_y_estados": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lots",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin lotes seleccionados--"]
                            },
                            "lista_lots": { "$map": { "input": "$lots", "as": "lot", "in": { "$split": ["$$lot", ", "] } } }
                        }
                    }, {
                        "$addFields": {
                            "lista_lotes": { "$map": { "input": "$lista_lots", "as": "lot", "in": { "$arrayElemAt": ["$$lot", 0] } } },
                            "lista_estados": { "$map": { "input": "$lista_lots", "as": "lot", "in": { "$arrayElemAt": ["$$lot", 1] } } }
                        }
                    }


                    , {
                        "$addFields": {
                            "lista_lotes": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lista_lotes",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {

                                                "if": { "$eq": [{ "$strLenCP": { "$ifNull": ["$$value", ""] } }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin lotes seleccionados--"]
                            },
                            "lista_estados": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lista_estados",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {

                                                "if": { "$eq": [{ "$strLenCP": { "$ifNull": ["$$value", ""] } }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin estados seleccionados--"]
                            }
                        }
                    }, { "$lookup": { "from": "users", "localField": "uid", "foreignField": "_id", "as": "user" } }, {
                        "$addFields": {
                            "user": { "$arrayElemAt": ["$user.timezone", 0] },
                            "Actividad": "$activity.name",
                            "Codigo Labor": "$cod",
                            "Estado Labor": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$status", "To do"] },
                                        "then": "⏰Por hacer"
                                    }, { "case": { "$eq": ["$status", "Doing"] }, "then": "💪 En progreso" }, {
                                        "case": { "$eq": ["$status", "Done"] },
                                        "then": "✔ Listo"
                                    }]
                                }
                            },
                            "Finca": "$farm.name",
                            "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                            "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                            "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                            "Lista lotes": "$lista_lotes",
                            "Lista estados": "$lista_estados",
                            "Tipo cultivo labor": { "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"] },
                            "Etiquetas": { "$cond": { "if": { "$eq": ["$Etiquetas", ""] }, "then": "--sin etiquetas--", "else": "$Etiquetas" } },
                            "Centro de costos": "$costsCenter.name",
                            "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                            "[Unidad] Medida de Actividad": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                                        "then": "Bloque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                                        "then": "Lotes"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                                        "then": "Linea"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                                        "then": "Árboles"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                                        "then": "Centro frutero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                                        "then": "Poligono de muestreo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                                        "then": "Valvula"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                                        "then": "Drenaje"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                                        "then": "Aspersors"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                                        "then": "Red de riego uno"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                                        "then": "Red de riego dos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                                        "then": "Red de riego tres"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                                        "then": "Trampa"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                                        "then": "Vías"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                                        "then": "Bosque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                                        "then": "Sensor"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                                        "then": "Cable vía"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                                        "then": "Edificio"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                                        "then": "Cuerpo de agua"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                                        "then": "Poligonos adicionales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                                        "then": "Unidades de cultivo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                                        "then": "Jornales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                                        "then": "Cantidades"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                                        "then": "Metros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "km"] },
                                        "then": "Kilometros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                                        "then": "Centimetros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                                        "then": "Millas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                                        "then": "Yardas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                                        "then": "Pies"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                                        "then": "Gramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                                        "then": "Miligramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "us/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                                        "then": "Toneladas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                                        "then": "Onzas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                                        "then": "Libras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                                        "then": "Litros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                                        "then": "Galones"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                                        "then": "Bultos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                                        "then": "Bolsas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                                        "then": "sacks"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                                        "then": "Yemas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                                        "then": "Factura"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                                        "then": "Flete"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                                        "then": "Picadero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                                        "then": "Hora"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                                        "then": "Cuadras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                                        "then": "Canecas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                                        "then": "Racimos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "metro-line"] },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "square-meter"] },
                                        "then": "Metro cuadrado"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "estaquilla"] },
                                        "then": "estaquilla"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "holes"] },
                                        "then": "Hoyos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "rastas"] },
                                        "then": "Rastas"
                                    }, { "case": { "$eq": ["$productivityPrice.measure", "postes"] }, "then": "Postes" }], "default": "--------"
                                }
                            },
                            "($) Precio x unidad de Actividad": "$productivityPrice.price",
                            "Semana de inicio labor": { "$sum": [{ "$week": { "$arrayElemAt": ["$when.start", 0] } }, 1] },
                            "Semana de registro labor": { "$sum": [{ "$week": "$rgDate" }, 1] },
                            "Fecha inicio": { "$max": "$when.start" },
                            "Fecha fin": { "$max": "$when.finish" },
                            "Blancos biologicos de labor": { "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"] },
                            "Observaciones": "$observation",
                            "Nombre de Empleado": { "$ifNull": [{ "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"] }, "--sin empleados--"] },
                            "Identificacion": { "$ifNull": ["$Empleado.numberID", "--"] },
                            "(#) Productividad de Empleado": { "$ifNull": ["$ProductividadEmpleado", 0] },
                            "($) Total Pago Empleado": "$TotalPagoEmpleado",
                            "Firma de empleado": "* ",
                            "Aviso legal": "* ",
                            "Supervisor": { "$ifNull": [{ "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"] }, "--sin supervisor--"] },
                            "(#) Productos asignados": "$total_productos_seleccionados_labor",
                            "Productos asignados": {
                                "$cond": {
                                    "if": { "$eq": ["$productos", ""] },
                                    "then": "--sin productos--",
                                    "else": "$productos"
                                }
                            },
                            "(#) Empleados asignados": "$total_empleados_seleccionados_labor"
                        }
                    }


                    , {
                        "$addFields": {
                            "HORAS EMPLEADO": {
                                "$cond": {
                                    "if": { "$eq": ["$[Unidad] Medida de Actividad", "Hora"] },
                                    "then": "$(#) Productividad de Empleado",
                                    "else": 0
                                }
                            },
                            "VALOR HORA EMPLEADO": { "$ifNull": ["$Empleado.salary", 0] }
                        }
                    }
                    , {
                        "$addFields": {
                            "PRECIO TOTAL EMPLEADO": {
                                "$multiply": [
                                    { "$toDouble": "$HORAS EMPLEADO" },
                                    { "$toDouble": "$VALOR HORA EMPLEADO" }
                                ]
                            }
                        }
                    }
                    , {
                        "$addFields": {
                            "PRECIO TOTAL EMPLEADO": { "$divide": [{ "$subtract": [{ "$multiply": ["$PRECIO TOTAL EMPLEADO", 100] }, { "$mod": [{ "$multiply": ["$PRECIO TOTAL EMPLEADO", 100] }, 1] }] }, 100] }
                        }
                    }



                    , {
                        "$project": {
                            "Actividad": 1,
                            "Codigo Labor": 1,
                            "Estado Labor": 1,
                            "Finca": 1,
                            "tipos Cartografia": 1,
                            "elementos Cartografia": 1,
                            "Lista lotes-estado": 1,
                            "Lista lotes": 1,
                            "Lista estados": 1,
                            "Tipo cultivo labor": 1,
                            "Centro de costos": 1,
                            "(#) Productividad esperada de Labor": 1,
                            "[Unidad] Medida de Actividad": 1,
                            "($) Precio x unidad de Actividad": 1,
                            "Semana de inicio labor": 1,
                            "Semana de registro labor": 1,
                            "Fecha inicio": {
                                "$dateToString": {
                                    "date": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] },
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            },
                            "Fecha fin": {
                                "$dateToString": {

                                    "date": { "$arrayElemAt": ["$when.finish", { "$subtract": [{ "$size": "$when.finish" }, 1] }] },
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            },
                            "Blancos biologicos de labor": 1,
                            "Observaciones": 1,

                            "Firma de empleado": 1,
                            "Aviso legal": 1,
                            "Supervisor": 1,
                            "(#) Productos asignados": 1,
                            "Productos asignados": 1,
                            "(#) Empleados asignados": 1,

                            "Nombre de Empleado": 1,
                            "Identificacion": 1,
                            "HORAS EMPLEADO": 1,
                            "VALOR HORA EMPLEADO": 1,
                            "PRECIO TOTAL EMPLEADO": 1,

                            "Usuario Logueado": { "$ifNull": ["$userUpd", "--sin Usuario Logueado--"] }



                            , "rgDate": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    }

                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        // //---DASHBOARD

        // , {
        //     "$addFields": {
        //         "variable_label": "$Finca"

        //         , "variable_dataset": "$Actividad"
        //     }
        // }



        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$variable_label"
        //             , "dashboard_dataset": "$variable_dataset"
        //         }
        //         , "dashboard_cantidad": { "$sum": 1 }

        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_dataset": "$_id.dashboard_dataset"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }

        //     }
        // }

        // , {
        //     "$sort": {
        //         "_id.dashboard_dataset": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "data_group": { "$push": "$$ROOT" }
        //         , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
        //     }
        // }

        // , { "$unwind": "$data_group" }

        // , { "$unwind": "$data_group.data_group" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data_group.data_group",
        //                 {
        //                     "array_dashboard_dataset": "$array_dashboard_dataset"
        //                 }
        //             ]
        //         }
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$_id.dashboard_label"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }
        //     }
        // }
        // , {
        //     "$sort": {
        //         "_id.dashboard_label": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "dashboard_data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$map": {
        //                 "input": "$dashboard_data",
        //                 "as": "item_dashboard_data",
        //                 "in": {
        //                     "$reduce": {
        //                         "input": "$$item_dashboard_data.data_group",
        //                         "initialValue": [],
        //                         "in": {
        //                             "$concatArrays": [
        //                                 "$$value",
        //                                 [
        //                                     {
        //                                         "dashboard_label": "$$this._id.dashboard_label",
        //                                         "dashboard_dataset": "$$this._id.dashboard_dataset",
        //                                         "dashboard_cantidad": "$$this.dashboard_cantidad"
        //                                     }
        //                                 ]
        //                             ]
        //                         }
        //                     }
        //                 }

        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$reduce": {
        //                 "input": "$datos_dashboard",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$concatArrays": [
        //                         "$$value",
        //                         "$$this"
        //                     ]
        //                 }
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "info_datasets": {
        //             "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "array_colores": [

        //             "#773e91",
        //             "#61c3ac",
        //             "#fed330",
        //             "#fc5c65",
        //             "#45aaf2",
        //             "#d1d8e0",
        //             "#635f6d",

        //             "#474787",
        //             "#218c74",
        //             "#26de81",
        //             "#b33939",
        //             "#218c74",
        //             "#227093",
        //             "#cd6133",
        //             "#84817a",
        //             "#40407a",
        //             "#33d9b2",
        //             "#ffda79",
        //             "#2c2c54",



        //             "#a55eea",
        //             "#2bcbba",
        //             "#fd9644",
        //             "#4b7bec",
        //             "#20bf6b",
        //             "#4b6584",
        //             "#ffb142",


        //             "#fc5c65",
        //             "#20bf6b",
        //             "#3867d6",
        //             "#fed330",
        //             "#a55eea",
        //             "#45aaf2",
        //             "#fa8231",
        //             "#eb3b5a",
        //             "#0fb9b1",
        //             "#d1d8e0",

        //             "#8854d0",


        //             "#63b598",
        //             "#ce7d78",
        //             "#ea9e70",
        //             "#a48a9e",
        //             "#c6e1e8",
        //             "#648177",
        //             "#0d5ac1",
        //             "#f205e6",
        //             "#1c0365",
        //             "#14a9ad",
        //             "#4ca2f9",
        //             "#a4e43f",
        //             "#d298e2",
        //             "#6119d0",
        //             "#d2737d",
        //             "#c0a43c",
        //             "#f2510e",
        //             "#651be6",
        //             "#79806e",
        //             "#61da5e",
        //             "#cd2f00",
        //             "#9348af",
        //             "#01ac53",
        //             "#c5a4fb",
        //             "#996635",
        //             "#b11573",
        //             "#4bb473",
        //             "#75d89e",
        //             "#2f3f94",
        //             "#2f7b99",
        //             "#da967d",
        //             "#34891f",
        //             "#b0d87b",
        //             "#ca4751",
        //             "#7e50a8",
        //             "#c4d647",
        //             "#e0eeb8",
        //             "#11dec1",
        //             "#289812",
        //             "#566ca0",
        //             "#ffdbe1",
        //             "#2f1179",
        //             "#935b6d",
        //             "#916988",
        //             "#513d98",
        //             "#aead3a",
        //             "#9e6d71",
        //             "#4b5bdc",
        //             "#0cd36d",
        //             "#250662",
        //             "#cb5bea",
        //             "#228916",
        //             "#ac3e1b",
        //             "#df514a",
        //             "#539397",
        //             "#880977",
        //             "#f697c1",
        //             "#ba96ce",
        //             "#679c9d",
        //             "#c6c42c",
        //             "#5d2c52",
        //             "#48b41b",
        //             "#e1cf3b",
        //             "#5be4f0",
        //             "#57c4d8",
        //             "#a4d17a",
        //             "#be608b",
        //             "#96b00c",
        //             "#088baf",
        //             "#f158bf",
        //             "#e145ba",
        //             "#ee91e3",
        //             "#05d371",
        //             "#5426e0",
        //             "#4834d0",
        //             "#802234",
        //             "#6749e8",
        //             "#0971f0",
        //             "#8fb413",
        //             "#b2b4f0",
        //             "#c3c89d",
        //             "#c9a941",
        //             "#41d158",
        //             "#fb21a3",
        //             "#51aed9",
        //             "#5bb32d",
        //             "#21538e",
        //             "#89d534",
        //             "#d36647",
        //             "#7fb411",
        //             "#0023b8",
        //             "#3b8c2a",
        //             "#986b53",
        //             "#f50422",
        //             "#983f7a",
        //             "#ea24a3",
        //             "#79352c",
        //             "#521250",
        //             "#c79ed2",
        //             "#d6dd92",
        //             "#e33e52",
        //             "#b2be57",
        //             "#fa06ec",
        //             "#1bb699",
        //             "#6b2e5f",
        //             "#64820f",
        //             "#21538e",
        //             "#89d534",
        //             "#d36647",
        //             "#7fb411",
        //             "#0023b8",
        //             "#3b8c2a",
        //             "#986b53",
        //             "#f50422",
        //             "#983f7a",
        //             "#ea24a3",
        //             "#79352c",
        //             "#521250",
        //             "#c79ed2",
        //             "#d6dd92",
        //             "#e33e52",
        //             "#b2be57",
        //             "#fa06ec",
        //             "#1bb699",
        //             "#6b2e5f",
        //             "#64820f",
        //             "#9cb64a",
        //             "#996c48",
        //             "#9ab9b7",
        //             "#06e052",
        //             "#e3a481",
        //             "#0eb621",
        //             "#fc458e",
        //             "#b2db15",
        //             "#aa226d",
        //             "#792ed8",
        //             "#73872a",
        //             "#520d3a",
        //             "#cefcb8",
        //             "#a5b3d9",
        //             "#7d1d85",
        //             "#c4fd57",
        //             "#f1ae16",
        //             "#8fe22a",
        //             "#ef6e3c",
        //             "#243eeb",
        //             "#dd93fd",
        //             "#3f8473",
        //             "#e7dbce",
        //             "#421f79",
        //             "#7a3d93",

        //             "#93f2d7",
        //             "#9b5c2a",
        //             "#15b9ee",
        //             "#0f5997",
        //             "#409188",
        //             "#911e20",
        //             "#1350ce",
        //             "#10e5b1",
        //             "#fff4d7",
        //             "#cb2582",
        //             "#ce00be",
        //             "#32d5d6",
        //             "#608572",
        //             "#c79bc2",
        //             "#00f87c",
        //             "#77772a",
        //             "#6995ba",
        //             "#fc6b57",
        //             "#f07815",
        //             "#8fd883",
        //             "#060e27",
        //             "#96e591",
        //             "#21d52e",
        //             "#d00043",
        //             "#b47162",
        //             "#1ec227",
        //             "#4f0f6f",
        //             "#1d1d58",
        //             "#947002",
        //             "#bde052",
        //             "#e08c56",
        //             "#28fcfd",
        //             "#36486a",
        //             "#d02e29",
        //             "#1ae6db",
        //             "#3e464c",
        //             "#a84a8f",
        //             "#911e7e",
        //             "#3f16d9",
        //             "#0f525f",
        //             "#ac7c0a",
        //             "#b4c086",
        //             "#c9d730",
        //             "#30cc49",
        //             "#3d6751",
        //             "#fb4c03",
        //             "#640fc1",
        //             "#62c03e",
        //             "#d3493a",
        //             "#88aa0b",
        //             "#406df9",
        //             "#615af0",
        //             "#2a3434",
        //             "#4a543f",
        //             "#79bca0",
        //             "#a8b8d4",
        //             "#00efd4",
        //             "#7ad236",
        //             "#7260d8",
        //             "#1deaa7",
        //             "#06f43a",
        //             "#823c59",
        //             "#e3d94c",
        //             "#dc1c06",
        //             "#f53b2a",
        //             "#b46238",
        //             "#2dfff6",
        //             "#a82b89",
        //             "#1a8011",
        //             "#436a9f",
        //             "#1a806a",
        //             "#4cf09d",
        //             "#c188a2",
        //             "#67eb4b",
        //             "#b308d3",
        //             "#fc7e41",
        //             "#af3101",
        //             "#71b1f4",
        //             "#a2f8a5",
        //             "#e23dd0",
        //             "#d3486d",
        //             "#00f7f9",
        //             "#474893",
        //             "#3cec35",
        //             "#1c65cb",
        //             "#5d1d0c",
        //             "#2d7d2a",
        //             "#ff3420",
        //             "#5cdd87",
        //             "#a259a4",
        //             "#e4ac44",
        //             "#1bede6",
        //             "#8798a4",
        //             "#d7790f",
        //             "#b2c24f",
        //             "#de73c2",
        //             "#d70a9c",
        //             "#88e9b8",
        //             "#c2b0e2",
        //             "#86e98f",
        //             "#ae90e2",
        //             "#1a806b",
        //             "#436a9e",
        //             "#0ec0ff"

        //         ]
        //     }
        // }




        // , {
        //     "$addFields": {
        //         "DATA_ARRAY_DATASETS": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_info_datasets",
        //                 "in": {
        //                     "label": "$$item_info_datasets",
        //                     "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
        //                     "borderColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
        //                     "borderWidth": 0,
        //                     "data": {
        //                         "$map": {
        //                             "input": "$DATA_LABELS",
        //                             "as": "item_data_labels",
        //                             "in": {
        //                                 "$reduce": {
        //                                     "input": "$datos_dashboard",
        //                                     "initialValue": 0,
        //                                     "in": {
        //                                         "$cond": {
        //                                             "if": {
        //                                                 "$and": [
        //                                                     { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
        //                                                     , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
        //                                                 ]

        //                                             },
        //                                             "then": "$$this.dashboard_cantidad",
        //                                             "else": "$$value"
        //                                         }
        //                                     }
        //                                 }
        //                             }

        //                         }

        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }




        // , { "$project": { "_id": 0 } }


        // , {
        //     "$project": {

        //         "data": {
        //             "labels": "$DATA_LABELS",
        //             "datasets": "$DATA_ARRAY_DATASETS"
        //         },
        //         "options": {
        //             "title": {
        //                 "display": true,
        //                 "text": "Chart.js Bar Chart"
        //             }
        //         }

        //     }
        // }



    ]


)
