[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_formulariopuentecartografiaagrowin",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [


                { "$unwind": "$CARTOGRAFIA AGRIMANAGER.features" },


                {
                    "$addFields": {
                        "TIPO CARTOGRAFIA": "$CARTOGRAFIA AGRIMANAGER.features.properties.type"
                    }
                },

                {
                    "$addFields": {
                        "CARTOGRAFIA AGRIMANAGER": "$CARTOGRAFIA AGRIMANAGER.features.properties.name"
                    }
                },


                {
                    "$project": {
                        "Point": 0
                        , "uid": 0
                        , "uDate": 0
                    }
                },


                {
                    "$addFields": {
                        "TIPO CARTOGRAFIA": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": ["$TIPO CARTOGRAFIA", "drainages"] },
                                        "then": "Drenaje"
                                    },
                                    {
                                        "case": { "$eq": ["$TIPO CARTOGRAFIA", "lanes"] },
                                        "then": "Vías"
                                    },
                                    {
                                        "case": { "$eq": ["$TIPO CARTOGRAFIA", "sensors"] },
                                        "then": "Sensor"
                                    }

                                ],
                                "default": "--------"
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
