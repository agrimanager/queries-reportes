db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_formulariopuentecartografiaagrowin",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    { "$unwind": "$CARTOGRAFIA AGRIMANAGER.features" },


                    {
                        "$addFields": {
                            "TIPO CARTOGRAFIA": "$CARTOGRAFIA AGRIMANAGER.features.properties.type"
                        }
                    },

                    {
                        "$addFields": {
                            "CARTOGRAFIA AGRIMANAGER": "$CARTOGRAFIA AGRIMANAGER.features.properties.name"
                        }
                    },


                    {
                        "$project": {
                            "Point": 0
                            , "uid": 0
                            , "uDate": 0
                        }
                    },


                    {
                        "$addFields": {
                            "TIPO CARTOGRAFIA": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$TIPO CARTOGRAFIA", "drainages"] },
                                            "then": "Drenaje"
                                        },
                                        {
                                            "case": { "$eq": ["$TIPO CARTOGRAFIA", "lanes"] },
                                            "then": "Vías"
                                        },
                                        {
                                            "case": { "$eq": ["$TIPO CARTOGRAFIA", "sensors"] },
                                            "then": "Sensor"
                                        }

                                    ],
                                    "default": "--------"
                                }
                            },
                        }
                    },



                    {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
