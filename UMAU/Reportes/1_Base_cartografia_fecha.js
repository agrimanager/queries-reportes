db.form_moniliasis.aggregate(
    [


        //test
        {
            $sort: {
                Fecha: -1
            }
        },

        //===========CARTOGRAFIA
        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "Poligono_adicional": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$Poligono_adicional",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "Poligono_adicional": { "$ifNull": ["$Poligono_adicional.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,

                "Lote": 0
            }
        }


        //===========FECHAS
        //---filtro fechas malas
        , { "$addFields": { "variable_fecha": "$Fecha" } }
        , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
        , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


        { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
        { "$match": { "anio_variable_fecha": { "$lt": 3000 } } },

        {
            "$project": {
                "variable_fecha": 0
                , "type_variable_fecha": 0
                , "anio_variable_fecha": 0
            }
        }


        //---info fechas
        , { "$addFields": { "variable_fecha": "$Fecha" } }
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$variable_fecha" } },
                "num_mes": { "$month": { "date": "$variable_fecha" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
                "num_semana": { "$week": { "date": "$variable_fecha" } },
                "num_dia_semana": { "$dayOfWeek": { "date": "$variable_fecha" } }
            }
        }

        , {
            "$addFields": {
                //"Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


                , "Dia_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        },

        //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        {
            "$project": {
                "num_dia_semana": 0
                , "variable_fecha": 0
            }
        }




    ]
)
