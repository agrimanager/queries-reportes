db.form_formulariodecosecha.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Lote": 0,
                "Point": 0,
                "bloque_id": 0,
                "uid": 0
            }
        }

        //--variable A
        , {
            "$addFields": {
                "total_kg": {
                    "$sum": [
                        { "$toDouble": "$Kg cacao premium" }
                        , { "$toDouble": "$Kg cacao germinado" }
                        , { "$toDouble": "$Kg cacao segunda" }
                    ]
                }
            }
        }


        //--variables B

        //B1
        , {
            "$addFields": {
                "total_jornales_Descacote": {
                    "$filter": {
                        "input": "$Jornales de cosecha y descacote",
                        "as": "item_empleado",
                        "cond": { "$eq": ["$$item_empleado.reference", "Descacote"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "total_jornales_Descacote": {
                    "$reduce": {
                        "input": "$total_jornales_Descacote.value",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }

        //B2
        , {
            "$addFields": {
                "total_jornales_Cosecha": {
                    "$filter": {
                        "input": "$Jornales de cosecha y descacote",
                        "as": "item_empleado",
                        "cond": { "$eq": ["$$item_empleado.reference", "Cosecha"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "total_jornales_Cosecha": {
                    "$reduce": {
                        "input": "$total_jornales_Cosecha.value",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }

        //B3 -- aux
        , {
            "$addFields": {
                "total_jornales_Sin Rol": {
                    "$filter": {
                        "input": "$Jornales de cosecha y descacote",
                        "as": "item_empleado",
                        "cond": { "$eq": ["$$item_empleado.reference", "sin selección"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "total_jornales_Sin Rol": {
                    "$reduce": {
                        "input": "$total_jornales_Sin Rol.value",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }



        //--variables C = A/B

        //C1
        , {
            "$addFields": {
                "RENDIMIENTO KG_JORNAL Descacote": {
                    "$cond": {
                        "if": { "$eq": ["$total_jornales_Descacote", 0] },
                        "then": 0,
                        "else": { "$divide": ["$total_kg", "$total_jornales_Descacote"] }
                    }
                }
            }
        }

        //..2 decimales
        , {
            "$addFields": {
                "RENDIMIENTO KG_JORNAL Descacote": { "$divide": [{ "$subtract": [{ "$multiply": ["$RENDIMIENTO KG_JORNAL Descacote", 100] }, { "$mod": [{ "$multiply": ["$RENDIMIENTO KG_JORNAL Descacote", 100] }, 1] }] }, 100] }
            }
        }


        //C2
        , {
            "$addFields": {
                "RENDIMIENTO KG_JORNAL Cosecha": {
                    "$cond": {
                        "if": { "$eq": ["$total_jornales_Cosecha", 0] },
                        "then": 0,
                        "else": { "$divide": ["$total_kg", "$total_jornales_Cosecha"] }
                    }
                }
            }
        }

        //..2 decimales
        , {
            "$addFields": {
                "RENDIMIENTO KG_JORNAL Cosecha": { "$divide": [{ "$subtract": [{ "$multiply": ["$RENDIMIENTO KG_JORNAL Cosecha", 100] }, { "$mod": [{ "$multiply": ["$RENDIMIENTO KG_JORNAL Cosecha", 100] }, 1] }] }, 100] }
            }
        }



        //---proyeccion final
        , {
            "$project": {
                "Jornales de cosecha y descacote": 0
            }
        }


    ]
)
