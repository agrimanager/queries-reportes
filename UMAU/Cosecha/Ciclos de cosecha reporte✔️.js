[

    { "$addFields": { "variable_fecha": "$Fecha de cosecha" } },
    { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    {
        "$sort": {
            "variable_fecha": -1
        }
    },
    {
        "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    }
    , {
        "$group": {
            "_id": {
                "nombre_lote": "$Cartography.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data": { "$arrayElemAt": ["$data", 0] }
        }
    },

    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$data.variable_fecha"] }, 86400000]
                }
            }
        }
    },

    {
        "$addFields": {
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 7] }]
                    },
                    "then": "A- [0 - 7] Dias",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 8] }, { "$lte": ["$dias de ciclo", 10] }]
                            },
                            "then": "B- [8 - 10] Dias",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 11] }, { "$lte": ["$dias de ciclo", 12] }]
                                    },
                                    "then": "C- [11 - 12] Dias",
                                    "else": "D- ( > 12) Dias"
                                }
                            }
                        }
                    }
                }
            }

        }
    },


    {
        "$addFields": {
            "Fecha_Ultima_Cosecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data.variable_fecha" } }
            , "Fecha_Actual_Hoy": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data.today" } }
        }
    },


    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "dias de ciclo": "$dias de ciclo",
                        "rango": "$rango"

                        , "Fecha_Ultima_Cosecha": "$Fecha_Ultima_Cosecha"
                        , "Fecha_Actual_Hoy": "$Fecha_Actual_Hoy"
                    }
                ]
            }
        }
    }




    , {
        "$project": {
            "lote": "$nombre_lote",
            "Rango": "$rango",
            "Dias Ciclo": "$dias de ciclo",
            "Dias Ciclo txt": {
                "$cond": {
                    "if": { "$eq": ["$dias de ciclo", -1] },
                    "then": "-1",
                    "else": {
                        "$concat": [
                            { "$toString": "$dias de ciclo" },
                            " dias"
                        ]
                    }
                }
            }
            , "Fecha_Ultima_Cosecha": "$Fecha_Ultima_Cosecha"
            , "Fecha_Actual_Hoy": "$Fecha_Actual_Hoy"
        }
    }
]
