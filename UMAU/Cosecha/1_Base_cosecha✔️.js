[

    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Lote": 0,
            "Point": 0,
            "bloque_id": 0,
            "uid": 0
        }
    }

    , {
        "$addFields": {
            "total_kg": {
                "$sum": [
                    {"$toDouble":"$Kg cacao premium"}
                    ,{"$toDouble":"$Kg cacao germinado"}
                    ,{"$toDouble":"$Kg cacao segunda"}
                ]
            }
        }
    }


    , {
        "$addFields": {
            "Jornales de cosecha y descacote": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$Jornales de cosecha y descacote" }, "array"] },
                    "then": "$Jornales de cosecha y descacote",
                    "else": { "$map": { "input": { "$objectToArray": "$Jornales de cosecha y descacote" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                }
            }
        }
    }

    , {
        "$unwind": {
            "path": "$Jornales de cosecha y descacote",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "empleado_oid": { "$toObjectId": "$Jornales de cosecha y descacote._id" }
        }
    }

    , {
        "$lookup": {
            "from": "employees",
            "localField": "empleado_oid",
            "foreignField": "_id",
            "as": "info_empleado"
        }
    }
    , {
        "$unwind": {
            "path": "$info_empleado",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "empleado nombre y apellidos": {
                "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
            }
            , "empleado identificacion": "$info_empleado.numberID"

            , "empleado rol": "$Jornales de cosecha y descacote.reference"
            , "empleado jornales": "$Jornales de cosecha y descacote.value"
        }
    }

    , {
        "$project": {
            "info_empleado": 0
            , "empleado_oid": 0

            , "Jornales de cosecha y descacote": 0
        }
    }


]
