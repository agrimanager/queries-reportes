db.form_formulariodecosecha.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$addFields": { "variable_fecha": "$Fecha de cosecha" } },
        { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        {
            "$sort": {
                "variable_fecha": -1
            }
        },
        {
            "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }
        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        },

        {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        "$divide": [{ "$subtract": ["$_id.today", "$data.variable_fecha"] }, 86400000]
                    }
                }
            }
        },

        //---colores
        /*
        //============== LEYENDA DE REPORTE
        //--parte1
        #Dias Cilo Cosecha: #blanco,
        [0 - 7] Dias: #verde,
        [8 - 10] Dias: #amarillo,
        [11 - 12] Dias: #naranja,
        ( > 12) Dias: #rojo

        //--parte2
        #Dias Cilo Cosecha: #ffffff,
        [0 - 7] Dias: #008000,
        [8 - 10] Dias: #ffff00,
        [11 - 12] Dias: #ffa500,
        ( > 12) Dias: #ff0000

        //--parte Final
        #Dias Cilo Cosecha: #ffffff,[0 - 7] Dias: #008000,[8 - 10] Dias: #ffff00,[11 - 12] Dias: #ffa500,( > 12) Dias: #ff0000

        : #,: #,( > 12) Dias: #
        */


        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 7] }]
                        },
                        "then": "#008000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 8] }, { "$lte": ["$dias de ciclo", 10] }]
                                },
                                "then": "#ffff00",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 11] }, { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "#ffa500",
                                        "else": "#ff0000"
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 7] }]
                        },
                        "then": "A- [0 - 7] Dias",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 8] }, { "$lte": ["$dias de ciclo", 10] }]
                                },
                                "then": "B- [8 - 10] Dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 11] }, { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "C- [11 - 12] Dias",
                                        "else": "D- ( > 12) Dias"
                                    }
                                }
                            }
                        }
                    }
                }

            }
        },




        {
            "$project": {
                "_id": "$data.elemnq",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_lote",
                    "Rango": "$rango",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": "$data.Cartography.features.geometry"
            }
        }
    ]

)
