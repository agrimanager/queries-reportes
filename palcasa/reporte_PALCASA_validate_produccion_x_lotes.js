[

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },

    {
        "$lookup": {
            "from": "form_pesopromedioracimosporlote",
            "as": "Peso promedio lote",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso promedio lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso promedio lote": {
                "$ifNull": ["$Peso promedio lote.Peso Promedio", 0]
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    , {
        "$addFields":
            {
                "Total Cortados": {
                    "$sum": [
                        { "$ifNull": [{ "$toDouble": "$Racimos Cortador 1" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Racimos Cortador 2" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Racimos Cortador 3" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Racimos Cortador 4" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Racimos Cortador 5" }, 0] }
                    ]
                },
                "Total Recolectados (sacos)": {
                    "$sum": [
                        { "$ifNull": [{ "$toDouble": "$Sacos Recolector 1" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Sacos Recolector 2" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Sacos Recolector 3" }, 0] }
                    ]
                },
                "Total Cargados": {
                    "$sum": [
                        { "$ifNull": [{ "$toDouble": "$Racimos Cargador 1" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Racimos Cargador 2" }, 0] }
                    ]
                },

                "Total Empleados Cortadores": {
                    "$sum": [
                        { "$cond": { "if": { "$eq": ["$CORTADOR 1", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$CORTADOR 2", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$CORTADOR 3", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$CORTADOR 4", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$CORTADOR 5", ""] }, "then": 0, "else": 1 } }
                    ]
                },

                "Total Empleados Recolectores": {
                    "$sum": [
                        { "$cond": { "if": { "$eq": ["$RECOLECTOR FRUTA 1", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$RECOLECTOR FRUTA 2", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$RECOLECTOR FRUTA 3", ""] }, "then": 0, "else": 1 } }
                    ]
                },

                "Total Empleados Cargadores": {
                    "$sum": [
                        { "$cond": { "if": { "$eq": ["$CARGADOR FRUTA 1", ""] }, "then": 0, "else": 1 } },
                        { "$cond": { "if": { "$eq": ["$CARGADOR FRUTA 2", ""] }, "then": 0, "else": 1 } }
                    ]
                }


            }

    }

    , {
        "$addFields":
            {
                "Peso aproximado Cargados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$Total Cargados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                    ]
                },

                "Total Empleados (labor)": {
                    "$sum": [
                        { "$ifNull": [{ "$toDouble": "$Total Empleados Cortadores" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Total Empleados Recolectores" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Total Empleados Cargadores" }, 0] }
                    ]
                }
            }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "vagon": "$Identificacion Cajon u Otro Medio de Transporte de Fruta"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_racimos_alzados_lote_viaje": {
                "$sum": "$Peso aproximado Cargados"

            }
        }
    },
    {
        "$group": {
            "_id": "$_id.vagon",
            "data": {
                "$push": "$$ROOT"
            },
            "total_racimos_alzados_viaje": {
                "$sum": "$total_racimos_alzados_lote_viaje"
            }
        }
    },
    {
        "$unwind": "$data"
    },
    {
        "$unwind": "$data.data"
    },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "Total Peso aproximado Cargados (lote)": "$total_racimos_alzados_viaje",
                        "(%) Alzados x lote": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$total_racimos_alzados_viaje", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": ["$data.data.Peso aproximado Cargados", "$total_racimos_alzados_viaje"]
                                }
                            }
                        }
                    }
                ]
            }
        }
    }

    , {
        "$lookup": {
            "from": "form_despachodefruta",
            "as": "form_despacho_cosecha",
            "let": {
                "vagon": "$Identificacion Cajon u Otro Medio de Transporte de Fruta",
                "fecha": "$rgDate"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Codigo Cajones u Otro medio Asociado Al Corte de Fruta", "$$vagon"] },
                                { "$gt": ["$rgDate", "$$fecha"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": 1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$form_despacho_cosecha",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields":
            {
                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso Neto Fruta Fabrica" }, -1] },
                "Numero de ticket" :{ "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Numero Ticket Fabrica" }, -1] }
            }
    }

    , {
        "$addFields":
            {
                "Total Peso REAL Cargados (lote)": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]
                }
            }
    }

    , {
        "$addFields":
            {
                "Peso REAL lote": {
                    "$divide": [{ "$ifNull": [{ "$toDouble": "$Total Peso REAL Cargados (lote)" }, 0] }, "$Total Cargados"]
                }
            }
    },
    {
        "$project": {
            "form_despacho_cosecha": 0
        }
    }
    , {
        "$lookup": {
            "from": "form_precioalgunaslabores",
            "as": "actividades",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$in": ["$$nombre_lote", "$Lote.features.properties.name"]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$actividades",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "($)Valor Tonelada Fruta Ordinaria": { "$toDouble": { "$ifNull": ["$actividades.Valor Tonelada Fruta Ordinaria", 0] } },
            "($)Valor Tonelada Fruta al Destajo": { "$toDouble": { "$ifNull": ["$actividades.Valor Tonelada Fruta al Destajo", 0] } }
        }
    },
    {
        "$addFields": {
            "Total TON (lote)": {
                "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso REAL Cargados (lote)" }, 0] }, 0.001]
            }
        }
    },
    {
        "$addFields": {
            "TON al Destajo (lote)": {
                "$subtract": [{ "$ifNull": [{ "$toDouble": "$Total TON (lote)" }, 0] }, "$Total Empleados (labor)"]
            }
        }
    },
    {
        "$addFields": {
            "($)Valor Total Destajo (lote)": {
                "$multiply": [{ "$ifNull": [{ "$toDouble": "$TON al Destajo (lote)" }, 0] }, "$($)Valor Tonelada Fruta al Destajo"]
            }
        }
    },
    {
        "$addFields": {
            "($)Valor Unitario Destajo (lote)": {
                "$divide": [{ "$ifNull": [{ "$toDouble": "$($)Valor Total Destajo (lote)" }, 0] }, "$Total Empleados (labor)"]
            }
        }
    },

    {
        "$addFields": {
            "($)Valor a pagar": {
                "$sum": [{ "$ifNull": [{ "$toDouble": "$($)Valor Unitario Destajo (lote)" }, 0] }, "$($)Valor Tonelada Fruta Ordinaria"]
            }
        }
    }


]