db.form_formulariofecha.aggregate(

    {
        "$addFields": {
            "dia1": { "$dayOfMonth": "$Fecha1" },
            "dia2": { "$dayOfMonth": "$Fecha2" }
        }
    },

    {
        "$addFields": {
            "fecha1_unix": { "$toDouble": "$Fecha1" },
            "fecha2_unix": { "$toDouble": "$Fecha2" }
        }
    },

    {
        "$addFields": {
            "resta_dias": {
                "$subtract": [
                    "$dia2",
                    "$dia1"
                ]
            }
        }
    }


    ,{
        "$addFields": {
            "fecha1_unix_dias":  {"$divide": [{"$divide": [{"$divide": [{"$divide": ["$fecha1_unix", 1000]},60]},60]},24]},
            "fecha2_unix_dias":  {"$divide": [{"$divide": [{"$divide": [{"$divide": ["$fecha2_unix", 1000]},60]},60]},24]}
        }
    }


    ,{
        "$addFields": {
            "resta_dias_unix": {
                "$subtract": [
                    "$fecha2_unix_dias",
                    "$fecha1_unix_dias"
                ]
            }
        }
    }
);

