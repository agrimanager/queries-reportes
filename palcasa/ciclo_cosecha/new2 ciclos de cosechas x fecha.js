db.form_cicloscosechapalcasa.aggregate(

    [
        //---agrupar por lote
        {
            "$group": {
                "_id": {
                    "lote": "$Lote.features._id",
                    "geometry": "$Lote.features.geometry"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        },

        //---filtrar estado y fechas
        {
            "$addFields": {
                "estado_ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo cosecha",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha_ciclo_inicio": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },

            }
        },



        //---obtener dias
        {
            "$addFields": {
                "dias": {
                    "$cond": {
                        "if": { "$eq": ["$estado_ciclo", "inicia"] },
                        "then": -999,
                        "else": {
                            "$floor": {
                                //"$divide": [{ "$subtract": ["$today", "$Fecha ciclo"] }, 86400000]
                                "$divide": [{ "$subtract": [new Date(), "$fecha_ciclo_inicio"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },

        //---obtener color segun dias
        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$dias", 0] }, { "$lt": ["$dias", 9] }]
                        },
                        "then": "#2570A9",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias", 9] }, { "$lt": ["$dias", 12] }]

                                },
                                "then": "#9ACD32",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias", 12] }, { "$lt": ["$dias", 15] }]
                                        },
                                        "then": "#FFFD1F",
                                        "else": "#E84C3F"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        
        //---mostrar valores
        {
            "$project": {
                //"_id": "$Cartography._id",
                "_id": "$_id.lote",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "color": "$color"
                },
                //"geometry": "$Cartography.geometry"
                "geometry": "$_id.geometry"

                , "dias": "$dias"
            }
        }

        // //---mostrar valores
        // {
        //     "$project": {
        //         //"_id": "$Cartography._id",
        //         "_id": "$Lote.features._id",
        //         "idform": "$idform",
        //         "type": "Feature",
        //         "properties": {
        //             "color": "$color"
        //         },
        //         //"geometry": "$Cartography.geometry"
        //         "geometry": "$Lote.features.geometry"

        //         , "dias": "$dias"
        //     }
        // }
    ]


)