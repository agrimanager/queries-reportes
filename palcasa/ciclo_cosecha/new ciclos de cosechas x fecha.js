

//LEYENDAS
Ciclo cosecha de 0 a 9: #2570A9, Ciclo cosecha de 9 a 12: #9ACD32, Ciclo cosecha de 12 a 15: #FFFD1F, No definido:  #E84C3F


//PROYECCION DE COLOR
{
    "$cond": {
        "if": { "$and": [{ "$gte": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 0] }
        , { "$lt": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 9] }] },
        "then": "#2570A9",
        "else": {
            "$cond": {
                "if": { "$and": [{ "$gte": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 9] },
                { "$lt": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 12] }] },
                "then": "#9ACD32",
                "else": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 12] },
                        { "$lt": [{"$subtract": [{ "$dayOfYear": "$today" },{ "$dayOfYear": "$Fecha de ciclo" }]}, 15] }] },
                        "then": "#FFFD1F",
                        "else": "#E84C3F"
                    }
                }
            }
        }
    }
}