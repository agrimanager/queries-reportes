[
        { "$addFields": { "año_registro": { "$year": "$Fecha Cosecha" } } }
        , { "$match": { "año_registro": { "$lt": 3000 } } }

        , {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "año_registro": 0
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "year": { "$year": "$Fecha Cosecha" },
                    "dia": { "$dayOfYear": "$Fecha Cosecha" }
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$sort": {
                "_id.lote": 1,
                "_id.year": 1,
                "_id.dia": 1
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote",
                    "year": "$_id.year"
                },
                "data": {
                    "$push": {
                        "dia": "$_id.dia",
                        "data": "$data"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "data": {
                    "$reduce": {
                        "input": "$data",
                        "initialValue": {
                            "ultimo_ciclo": null,
                            "ciclos": []
                        },
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$$value.ultimo_ciclo", null]
                                },
                                "then": {
                                    "ultimo_ciclo": {
                                        "dia_inicial": "$$this.dia",
                                        "dia_final": "$$this.dia",
                                        "dias": 0,
                                        "data": "$$this.data"
                                    },
                                    "ciclos": "$$value.ciclos"
                                },
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$lt": [
                                                {
                                                    "$subtract": [
                                                        "$$this.dia",
                                                        "$$value.ultimo_ciclo.dia_final"
                                                    ]
                                                },
                                                1
                                            ]
                                        },
                                        "then": {
                                            "ultimo_ciclo": {
                                                "dia_inicial": "$$value.ultimo_ciclo.dia_inicial",
                                                "dia_final": "$$this.dia",
                                                "dias": "$$value.ultimo_ciclo.dias",
                                                "data": {
                                                    "$concatArrays": [
                                                        "$$value.ultimo_ciclo.data",
                                                        "$$this.data"
                                                    ]
                                                }
                                            },
                                            "ciclos": "$$value.ciclos"
                                        },
                                        "else": {
                                            "ultimo_ciclo": {
                                                "dia_inicial": "$$this.dia",
                                                "dia_final": "$$this.dia",
                                                "dias": {
                                                    "$subtract": [
                                                        "$$this.dia",
                                                        "$$value.ultimo_ciclo.dia_final"
                                                    ]
                                                },
                                                "data": "$$this.data"
                                            },
                                            "ciclos": {
                                                "$concatArrays": [
                                                    "$$value.ciclos",
                                                    ["$$value.ultimo_ciclo"]
                                                ]
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        , {
            "$addFields": {
                "data": {
                    "$concatArrays": [
                        "$data.ciclos",
                        ["$data.ultimo_ciclo"]
                    ]
                }
            }
        }


        , {
            "$unwind": "$data"
        },
        {
            "$unwind": "$data.data"
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data.data.Fecha Cosecha" } },
                            "año": { "$year": "$data.data.Fecha Cosecha" },
                            "mes": { "$month": "$data.data.Fecha Cosecha" },
                            "dia": { "$dayOfMonth": "$data.data.Fecha Cosecha" },

                            "dia_del_año": { "$dayOfYear": "$data.data.Fecha Cosecha" },
                            "semana_del_año": { "$week": "$data.data.Fecha Cosecha" },

                            "dias_anterior_ciclo": "$data.dias",
                            "ciclo_dia_inicial": "$data.dia_inicial",
                            "ciclo_dia_final": "$data.dia_final"
                        }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "ciclo_duracion_dias": {
                    "$subtract": [
                        "$ciclo_dia_final",
                        "$ciclo_dia_inicial"
                    ]
                }
            }
        }
    ]