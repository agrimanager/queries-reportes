db.form_evaluaciondeinsectosplagas.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$ARBOL.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$ARBOL.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },




        {
            "$lookup": {
                "from": "cartography_polygon",
                "localField": "lote._id",
                "foreignField": "_id",
                "as": "info_lote"
            }
        },
        {
            "$addFields": {
                "info_lote": "$info_lote.internal_features"
            }
        },
        { "$unwind": "$info_lote" },

        {
            "$addFields": {
                "plantas_x_lote": {
                    "$filter": {
                        "input": "$info_lote",
                        "as": "item",
                        "cond": { "$eq": ["$$item.type_features", "trees"] }
                    }
                }
            }
        },
        { "$unwind": "$plantas_x_lote" },
        {
            "$addFields": {
                "plantas_x_lote": "$plantas_x_lote.count_features"
            }
        },




        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_lote": 0
            }
        }

        , {
            "$match": {
                "INSECTOS Y ACAROS PLAGA": { "$ne": "" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "num_planta": { "$split": ["$planta", "-"] }
            }
        }

        , {
            "$addFields": {
                "num_planta": {
                    "$sum": [
                        {
                            "$multiply": [
                                { "$toInt": { "$arrayElemAt": ["$num_planta", 2] } },
                                10000
                            ]
                        },
                        { "$toInt": { "$arrayElemAt": ["$num_planta", 3] } }
                    ]
                }
            }
        }


    ]

)