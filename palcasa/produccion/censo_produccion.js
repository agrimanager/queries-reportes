[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote_id": "$Lote._id",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
       
    },
     {
        "$addFields": {
            "Total Racimos": { "$sum": [ { "$toDouble": "$Flores" }, { "$toDouble": "$Verdes"},{ "$toDouble": "$Maduros"} ] }
        }
    }
    
    ,{
        "$lookup": {
            "from": "form_pesopromedioracimosporlote",
            "as": "Peso_promedio_x_lote",
            "let": {
                "nombre_lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso_promedio_x_lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso_promedio_x_lote": {
                "$ifNull": ["$Peso_promedio_x_lote.Peso Promedio", 0]
            }
        }
    },
    
     {
          "$lookup": {
            "from": "cartography",
            "localField": "Lote_id",
            "foreignField": "_id",
            "as": "Numero_plantas"
        }
    }
    ,{
        "$unwind": {
            "path": "$Numero_plantas",
            "preserveNullAndEmptyArrays": true
        }
    }
    ,{
        "$addFields":{
            "Numero de plantas": "$Numero_plantas.properties.custom.Numero de plantas.value",
            "Hectareas del lote": "$Numero_plantas.properties.custom.Numero de hectareas.value",
            "Numero de plantas erradicadas":"$Numero_plantas.properties.custom.Numero de plantas erradicadas.value"
        }
    }
    
    , {
            "$group": {
                "_id": {
                    "lote": "$Lote",
                    "planta": "$Planta"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }
        
    
]  