[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Arbol": 0
        }
    }





    , {
        "$group": {
            "_id": null,
            "data": {
                "$push": "$$ROOT"
            }
            , "cantidad_censos": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_censos": "$cantidad_censos"
                    }
                ]
            }
        }
    }



    , {
        "$group": {
            "_id": {
                "plaga": "$Plaga"
            },
            "data": {
                "$push": "$$ROOT"
            }
            , "cantidad_censos_x_plaga": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_censos_x_plaga": "$cantidad_censos_x_plaga"
                    }
                ]
            }
        }
    }




    , {
        "$addFields": {
            "data_array_estados": [

                {
                    "estado": "Huevo",
                    "cantidad": { "$toDouble": "$Huevo" }
                },
                {
                    "estado": "Larva",
                    "cantidad": { "$toDouble": "$Larva" }
                },
                {
                    "estado": "Pupa",
                    "cantidad": { "$toDouble": "$Pupa" }
                },
                {
                    "estado": "Ninfa",
                    "cantidad": { "$toDouble": "$Ninfa" }
                },
                {
                    "estado": "Adulto",
                    "cantidad": { "$toDouble": "$Adulto" }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "data_array_estados": {
                "$filter": {
                    "input": "$data_array_estados",
                    "as": "item",
                    "cond": { "$ne": ["$$item.cantidad", 0] }
                }
            }
        }
    }

    , {
        "$unwind": {
            "path": "$data_array_estados",
            "preserveNullAndEmptyArrays": true
        }
    }

    , { "$addFields": { "estado_plaga": { "$ifNull": ["$data_array_estados.estado", ""] } } }

    , {
        "$project": {
            "data_array_estados": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "plaga": "$Plaga"
                , "estado_plaga": "$estado_plaga"
            },
            "data": {
                "$push": "$$ROOT"
            }
            , "cantidad_censos_x_plaga_x_estado": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_censos_x_plaga_x_estado": "$cantidad_censos_x_plaga_x_estado"
                    }
                ]
            }
        }
    }



     , {
        "$group": {
            "_id": {
                "plaga": "$Plaga"
                , "estado_plaga": "$estado_plaga"
                , "nivel_afectacion": "$Nivel de afectacion"
            },
            "data": {
                "$push": "$$ROOT"
            }
            , "cantidad_censos_x_plaga_x_estado_x_nivel": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_censos_x_plaga_x_estado_x_nivel": "$cantidad_censos_x_plaga_x_estado_x_nivel"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "incidencia_x_plaga": {
                "$cond": {
                    "if": { "$eq": ["$cantidad_censos", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$cantidad_censos_x_plaga", "$cantidad_censos"]
                            }
                            , 100]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "incidencia_x_plaga": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia_x_plaga", 100] }, { "$mod": [{ "$multiply": ["$incidencia_x_plaga", 100] }, 1] }] }, 100] }
        }
    }

    , {
        "$addFields": {
            "incidencia_x_plaga_x_estado": {
                "$cond": {
                    "if": { "$eq": ["$cantidad_censos", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$cantidad_censos_x_plaga_x_estado", "$cantidad_censos"]
                            }
                            , 100]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "incidencia_x_plaga_x_estado": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia_x_plaga_x_estado", 100] }, { "$mod": [{ "$multiply": ["$incidencia_x_plaga_x_estado", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {
            "severidad_x_plaga_x_estado_x_nivel": {
                "$cond": {
                    "if": { "$eq": ["$cantidad_censos_x_plaga", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$cantidad_censos_x_plaga_x_estado_x_nivel", "$cantidad_censos_x_plaga"]
                            }
                            , 100]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "severidad_x_plaga_x_estado_x_nivel": { "$divide": [{ "$subtract": [{ "$multiply": ["$severidad_x_plaga_x_estado_x_nivel", 100] }, { "$mod": [{ "$multiply": ["$severidad_x_plaga_x_estado_x_nivel", 100] }, 1] }] }, 100] }
        }
    }




]
