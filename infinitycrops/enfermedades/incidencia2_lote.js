db.form_monitoreodeenfermedades.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0
            }
        }



        //# de censos realizados

        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "cantidad_censos": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "cantidad_censos": "$cantidad_censos"
                        }
                    ]
                }
            }
        }


        //# de árboles afectados por la enfermedad
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "enfermedad": "$Enfermedad"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "cantidad_censos_x_enfermedad": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "cantidad_censos_x_enfermedad": "$cantidad_censos_x_enfermedad"
                        }
                    ]
                }
            }
        }



        //# de árboles afectados por la enfermedad y NIVEL
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "enfermedad": "$Enfermedad"
                    , "nivel": "$Nivel de avance"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "cantidad_censos_x_enfermedad_x_nivel": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "cantidad_censos_x_enfermedad_x_nivel": "$cantidad_censos_x_enfermedad_x_nivel"
                        }
                    ]
                }
            }
        }


        //=============INCIDENCIA

        //incidencia_x_enfermedad
        , {
            "$addFields": {
                "incidencia_x_enfermedad": {
                    "$cond": {
                        "if": { "$eq": ["$cantidad_censos", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                {
                                    "$divide": ["$cantidad_censos_x_enfermedad", "$cantidad_censos"]
                                }
                                , 100]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "incidencia_x_enfermedad": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia_x_enfermedad", 100] }, { "$mod": [{ "$multiply": ["$incidencia_x_enfermedad", 100] }, 1] }] }, 100] }
            }
        }

        //incidencia_x_enfermedad_x_nivel
        , {
            "$addFields": {
                "incidencia_x_enfermedad_x_nivel": {
                    "$cond": {
                        "if": { "$eq": ["$cantidad_censos", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                {
                                    "$divide": ["$cantidad_censos_x_enfermedad_x_nivel", "$cantidad_censos"]
                                }
                                , 100]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "incidencia_x_enfermedad_x_nivel": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia_x_enfermedad_x_nivel", 100] }, { "$mod": [{ "$multiply": ["$incidencia_x_enfermedad_x_nivel", 100] }, 1] }] }, 100] }
            }
        }








    ]

)
