[

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Seguimiento Aguacate Hass.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$Seguimiento Aguacate Hass.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Seguimiento Aguacate Hass.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "Seguimiento Aguacate Hass":0,
            "Point":0
            
        }
    }
]