var data =db.suppliesTimeline.aggregate(

    {
        $match: {
            total: 0
            , quantity: { $gt: 0 }
        }
    }

)

data.forEach(i=>{

    db.suppliesTimeline.update(
        {
            _id:i._id
        },
        {
            $set:{
                "subtotal":1
                ,"total":1
                ,"valueByUnity":1
            }
        }

    )


})

/*
// collection: suppliesTimeline
{
	"_id" : ObjectId("635c50b81c96c821208a3f8b"),
	"sid" : ObjectId("62c33bc9a91473797653482f"),
	"uid" : ObjectId("6239f6ce62017207a7b8532f"),
	"wid" : ObjectId("62c31fbda9147379765347e6"),
	"lbid" : "",
	"lbcod" : "",
	"typeSupply" : "undefined",
	"inputmeasure" : "kg",
	"quantity" : 2.25,
	"productMeasure" : "kg",
	"productEquivalence" : 2.25,
	"outQuantityMovement" : 2.25,
--"subtotal" : 0,
	"tax" : 0,
--"total" : 0,
	"information" : "AJUSTE DEL INVENTARIO",
--"valueByUnity" : 0,
	"invoiceNumber" : "AJUSTE INVT.",
	"invoiceDate" : ISODate("2022-10-28T00:00:00.000-05:00"),
	"dueDate" : ISODate("2022-10-28T00:00:00.000-05:00"),
	"rgDate" : ISODate("2022-10-28T16:59:20.196-05:00"),
	"deleted" : false,
	"transfer" : false,
	"userRg" : "Altamira Agricultura"
}
*/
