
var data = db.tasks.aggregate(

    {
        $match: {
            status: "Done"
            , supplies: { $ne: [] }
        }
    }

    , {
        $addFields: {
            productos: { $size: "$supplies" }
        }
    }

    , {
        "$unwind": {
            "path": "$supplies",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$lookup": {
            "from": "suppliesTimeline",
            "as": "data",
            "let": {
                "codigo_labor": "$cod"
                , "id_producto": "$supplies._id"
            },
            //query
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$lbcod", "$$codigo_labor"]
                        }
                    }
                }

                , {
                    "$match": {
                        "$expr": {
                            "$eq": ["$sid", "$$id_producto"]
                        }
                    }
                }

            ]
        }
    }


    , {
        $addFields: {
            "size_data": { $size: "$data" }
        }
    }

    , {
        "$match": {
            "$expr": {
                "$ne": ["$size_data", 1]
            }
        }
    }

    , {
        $group: {
            _id: "$cod"

            , cantidad_errores: { $sum: 1 }
        }
    }




)



// data


data.forEach(i => {

    //1)abrir labores
    db.tasks.update(
        {
            cod: i._id
        }
        , {
            $set: {
                status: "Doing"
            }
        }

    )

    //2)borrar transacciones
    db.suppliesTimeline.remove(
        {
            lbcod: i._id
        }
    )


})
