
db.tasks.aggregate(

    {
        $match: {
            status: "Done"
            , supplies: { $ne: [] }
        }
    }

    , {
        $addFields: {
            productos: { $size: "$supplies" }
        }
    }

    , {
        "$lookup": {
            "from": "suppliesTimeline",
            "as": "data",
            "let": {
                "codigo_labor": "$cod"
            },
            //query
            "pipeline": [

                {
                    $group: {
                        _id: "$lbcod"

                        , transacciones_productos: { $sum: 1 }
                    }
                }

                , {
                    "$match": {
                        "$expr": {
                            "$eq": ["$_id", "$$codigo_labor"]
                        }
                    }
                }

            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        $addFields: {
            transacciones_productos: { $ifNull: ["$data.transacciones_productos", 0] }
        }
    }


    , {
        "$match": {
            "$expr": {
                "$ne": ["$productos", "$transacciones_productos"]
            }
        }
    }






)
