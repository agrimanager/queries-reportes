

var data = db.form_actualizacioncartografica.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-08-23T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------


        {
            $unwind: "$Arbol.features"
        }

        , {
            $addFields: {
                cartografia_oid: { $toObjectId: "$Arbol.features._id" }
            }
        }



        , {
            $group: {
                _id: {
                    "nombre": "$Estado del Arbol"
                }
                , arboles: { $push: "$cartografia_oid" }
                , cant: { $sum: 1 }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.nombre"]
                }
            }
        }


    ]

)


// data


data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})
