db.users.aggregate(
    [


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_pluviometria",
                "as": "data",
                "let": {},
                "pipeline": [

                    {
                        "$addFields": {
                            "farm_id": { "$toObjectId": "$Point.farm" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm_id",
                            "foreignField": "_id",
                            "as": "farm_data"
                        }
                    },
                    { "$unwind": "$farm_data" },
                    {
                        "$addFields": {
                            "Finca": "$farm_data.name"
                        }
                    },

                    {
                        "$project": {
                            "Finca": 1,
                            "FECHA": 1,
                            "MILIMETROS": 1,
                            "OBSERVACIONES": 1
                        }
                    },

                    {
                        "$project": {
                            "_id": 0
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        , { "$addFields": { "variable_fecha": "$FECHA" } }
        , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
        , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


        { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
        { "$match": { "anio_variable_fecha": { "$lt": 3000 } } },

        {
            "$project": {
                "variable_fecha": 0
                , "type_variable_fecha": 0
                , "anio_variable_fecha": 0
            }
        }




        , { "$addFields": { "rgDate": "$FECHA" } }
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
                "num_semana": { "$week": { "date": "$rgDate" } }
            }
        }


        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "weather",
                "as": "data_clima",
                "let": {
                    "finca": "$Finca",
                    "fecha_txt": "$Fecha_Txt"
                },
                "pipeline": [


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "fid",
                            "foreignField": "_id",
                            "as": "feature"
                        }
                    },
                    { "$unwind": "$feature" },


                    {
                        "$addFields": {
                            "fecha_unix_x_1000": {
                                "$multiply": ["$dt", 1000]
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "farm": { "$split": [{ "$trim": { "input": "$feature.path", "chars": "," } }, ","] }
                        }
                    },
                    //error
                    // {
                    //     "$addFields": {
                    //         "farm": { "$map": { "input": "$farm", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    //     }
                    // },
                    //
                    // 
                    // { "$unwind": "$farm" },
                    { "$unwind": "$farm" },
                    {
                        "$addFields": {
                            "farm": { "$toObjectId": "$farm" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    },

                    {
                        "$addFields": {
                            "farm": "$farm.name"
                        }
                    },
                    { "$unwind": "$farm" },


                    {
                        "$project": {
                            _id: 0,
                            "Sector": "$name",
                            "Finca": "$farm",
                            "Tipo elemento": "$feature.properties.type",
                            "Nombre elemento": "$feature.properties.name",

                            "Temperatura (K)": "$main.temp",
                            "Temp minima  (K)": "$main.temp_min",
                            "Temp maxima  (K)": "$main.temp_max",

                            "Viento (m/s)": "$wind.speed",
                            "Presion (hPa)": "$main.pressure",

                            "Nubes (%)": "$clouds.all",
                            "Humedad (%)": "$main.humidity",


                            "Fecha": { "$toDate": "$fecha_unix_x_1000" }

                        }
                    }

                    , {
                        "$addFields": {
                            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
                        }
                    }


                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$Finca"] },
                                    { "$eq": ["$$fecha_txt", "$Fecha_Txt"] }
                                ]
                            }
                        }
                    }
                ]
            }
        }


        , {
            "$addFields": {

                "clima sector": { "$ifNull": [{ "$arrayElemAt": ["$data_clima.Sector", 0] }, ""] }

                , "clima humedad promedio (%)": {
                    "$ifNull": [
                        { "$avg": "$data_clima.Humedad (%)" }
                        , 0
                    ]
                }


                , "clima temperatura promedio (C)": {
                    "$ifNull": [
                        {
                            "$subtract": [{ "$avg": "$data_clima.Temperatura (K)" }, 273.15]
                        }

                        , 0
                    ]
                }

                , "clima viento promedio (m/s)": {
                    "$ifNull": [
                        { "$avg": "$data_clima.Viento (m/s)" }
                        , 0
                    ]
                }

                , "clima presion promedio (hPa)": {
                    "$ifNull": [
                        { "$avg": "$data_clima.Presion (hPa)" }
                        , 0
                    ]
                }

                , "clima nubes promedio (%)": {
                    "$ifNull": [
                        { "$avg": "$data_clima.Nubes (%)" }
                        , 0
                    ]
                }

            }
        }

        , {
            "$project": {
                "data_clima": 0
            }
        }




    ]
)
