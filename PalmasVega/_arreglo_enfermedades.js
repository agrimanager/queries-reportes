

//--variable_old
// var name_old = ["pudricion de cogollo pc", "Pudrición de cogollo PC"];
// var name_old = ["Pudrición basal"];
var name_old = ["Otra"];


//--variable_new
// var name_new = "Pudricion de cogollo";
// var name_new = "pudricion basal";
var name_new = "otra";



//--UPDATE_MANY
db.form_modulodeenfermedades.updateMany(
    {
        "Enfermedad": {
            $in: name_old
        }
    },
    {
        $set: {
            "Enfermedad": name_new
        }
    }
)
