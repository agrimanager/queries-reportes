[

    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "uid": 0
            , "Lote": 0
        }
    }

    , {
        "$addFields": {
            "variable_fecha": "$Fecha de Recoleccion"
        }
    },
    { "$addFields": { "variable_fecha_type": { "$type": "$variable_fecha" } } },
    { "$match": { "variable_fecha_type": { "$eq": "date" } } },

    { "$addFields": { "variable_fecha_anio": { "$year": "$variable_fecha" } } },
    { "$match": { "variable_fecha_anio": { "$gt": 2000 } } },
    { "$match": { "variable_fecha_anio": { "$lt": 3000 } } },

    { "$addFields": { "variable_fecha_mes": { "$month": "$variable_fecha" } } },

    {
        "$project": {
            "Fecha de Recoleccion": 0
            , "variable_fecha_type": 0
        }
    },



    {
        "$lookup": {
            "from": "form_formulariopuente",
            "as": "Peso promedio lote",
            "let": {
                "nombre_lote": "$lote",
                "anio_registro": "$variable_fecha_anio",
                "mes_registro": "$variable_fecha_mes"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] },
                                { "$eq": [{ "$toString": "$$anio_registro" }, { "$toString": "$Ano" }] },
                                { "$eq": [{ "$toString": "$$mes_registro" }, { "$toString": "$Mes" }] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso promedio lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso promedio lote": {
                "$ifNull": ["$Peso promedio lote.peso promedio", 0]
            }
        }
    }




    , {
        "$addFields": {
            "variable_empleado_numerico": "$Numero de racimos"
        }
    },
    { "$match": { "variable_empleado_numerico": { "$exists": true } } },
    { "$match": { "variable_empleado_numerico": { "$ne": "" } } },
    { "$addFields": { "variable_empleado_numerico_filtro": { "$size": "$variable_empleado_numerico" } } },
    { "$match": { "variable_empleado_numerico_filtro": { "$gt": 0 } } },


    { "$unwind": "$variable_empleado_numerico" },

    {
        "$addFields": {
            "empleado_nombre": { "$ifNull": [{ "$toString": "$variable_empleado_numerico.name" }, ""] },
            "empleado_referencia": { "$ifNull": [{ "$toString": "$variable_empleado_numerico.reference" }, ""] },
            "empleado_racimos": { "$ifNull": [{ "$toDouble": "$variable_empleado_numerico.value" }, 0] },
            "empleado_id": { "$ifNull": [{ "$toObjectId": "$variable_empleado_numerico._id" }, null] }
        }
    },


    {
        "$lookup": {
            "from": "employees",
            "localField": "empleado_id",
            "foreignField": "_id",
            "as": "info_empleado"
        }
    },
    { "$unwind": "$info_empleado" },

    {
        "$addFields": {
            "empleado_nombre": { "$concat": [{ "$toString": "$info_empleado.firstName" }, " ", { "$toString": "$info_empleado.lastName" }] },
            "empleado_identificacion": { "$ifNull": [{ "$toString": "$info_empleado.numberID" }, ""] }
        }
    },

    {
        "$project": {
            "Numero de racimos": 0
            , "variable_empleado_numerico_filtro": 0
            , "variable_empleado_numerico": 0
            , "info_empleado": 0
            , "empleado_id": 0
        }
    },

    {
        "$addFields": {
            "empleado_racimos_peso_promedio": {
                "$multiply": ["$empleado_racimos", "$Peso promedio lote"]
            }
        }
    },
    {
        "$addFields": {
            "empleado_racimos_peso_promedio_ton": {
                "$divide": ["$empleado_racimos_peso_promedio", 1000]
            }
        }
    },


    {
        "$group": {
            "_id": {
                "_id": "$_id",
                "fecha": "$variable_fecha",
                "lote": "$lote"
            }

            , "total_racimos_recoleccion": { "$sum": "$empleado_racimos" }
            , "total_racimos_peso_promedio": { "$sum": "$empleado_racimos_peso_promedio" }
            , "total_racimos_peso_promedio_ton": { "$sum": "$empleado_racimos_peso_promedio_ton" }
            , "data": { "$push": "$$ROOT" }

        }
    },


    {
        "$lookup": {
            "from": "form_despachodefruta",
            "as": "form_despacho_cosecha",
            "let": {
                "fecha": "$_id.fecha"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de inicio de recoleccion" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de fin de recoleccion" } } }
                                    ]
                                }


                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": 1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$form_despacho_cosecha",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields":
        {
            "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso  ton" }, -1] },
            "Numero de ticket": { "$ifNull": [{ "$toString": "$form_despacho_cosecha.Numero de ticket" }, -1] }
        }
    },

    {
        "$project": {
            "form_despacho_cosecha": 0
        }
    },



    { "$unwind": "$data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "total_racimos_recoleccion": "$total_racimos_recoleccion"
                        , "total_racimos_peso_promedio": "$total_racimos_peso_promedio"
                        , "total_racimos_peso_promedio_ton": "$total_racimos_peso_promedio_ton"
                        , "peso_ticket_ton": "$Total Peso despachados (ticket)"
                        , "ticket": "$Numero de ticket"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "ticket": "$ticket"
                , "lote": "$lote"
            }

            , "despacho_peso_ton": { "$min": "$peso_ticket_ton" }
            , "total_racimos_peso_promedio_ton": { "$sum": "$empleado_racimos_peso_promedio_ton" }
            , "total_racimos_recoleccion": { "$min": "$total_racimos_recoleccion" }
            , "data": { "$push": "$$ROOT" }

        }
    }

    , {
        "$group": {
            "_id": {
                "ticket": "$_id.ticket"
            }
            , "despacho_total_racimos_peso_promedio_ton": { "$sum": "$total_racimos_peso_promedio_ton" }
            , "despacho_total_racimos_recoleccion": { "$sum": "$total_racimos_recoleccion" }
            , "data": { "$push": "$$ROOT" }

        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }
    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "despacho_total_racimos_recoleccion": "$despacho_total_racimos_recoleccion",
                        "despacho_total_racimos_peso_promedio_ton": "$despacho_total_racimos_peso_promedio_ton"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "pct_peso_recoleccion_despacho": {
                "$cond": {
                    "if": { "$eq": ["$despacho_total_racimos_peso_promedio_ton", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$total_racimos_peso_promedio_ton", "$despacho_total_racimos_peso_promedio_ton"]
                    }
                }
            }

            , "pct_peso_recoleccion_despacho_empleado": {
                "$cond": {
                    "if": { "$eq": ["$despacho_total_racimos_peso_promedio_ton", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$empleado_racimos_peso_promedio_ton", "$despacho_total_racimos_peso_promedio_ton"]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "total_racimos_peso_REAL_ton": {
                "$multiply": [{ "$ifNull": [{ "$toDouble": "$peso_ticket_ton" }, 0] }, "$pct_peso_recoleccion_despacho"]
            }
            , "empleado_racimos_peso_REAL_ton": {
                "$multiply": [{ "$ifNull": [{ "$toDouble": "$peso_ticket_ton" }, 0] }, "$pct_peso_recoleccion_despacho_empleado"]
            }
        }
    }

    , {
        "$addFields": {
            "Peso REAL lote": {
                "$cond": {
                    "if": { "$eq": ["$total_racimos_recoleccion", 0] },
                    "then": 0,
                    "else": {
                        "$divide": [{ "$ifNull": [{ "$toDouble": "$total_racimos_peso_REAL_ton" }, 0] }, "$total_racimos_recoleccion"]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "Peso REAL lote": {
                "$multiply": [{ "$ifNull": [{ "$toDouble": "$Peso REAL lote" }, 0] }, 1000]
            }
        }
    }


    , {
        "$addFields": {
            "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }
            , "anio": "$variable_fecha_anio"
            , "mes_num": "$variable_fecha_mes"
        }
    }
    , {
        "$project": {
            "variable_fecha": 0
            , "variable_fecha_anio": 0
            , "variable_fecha_mes": 0
        }
    }

    , {
        "$addFields": {
            "mes_txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$mes_num", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$mes_num", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$mes_num", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$mes_num", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$mes_num", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$mes_num", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$mes_num", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$mes_num", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$mes_num", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$mes_num", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$mes_num", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$mes_num", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }








]
