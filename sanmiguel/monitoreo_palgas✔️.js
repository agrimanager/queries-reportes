[


    {
        "$project": {
            "rgDate día": 0,
            "rgDate mes": 0,
            "rgDate año": 0,
            "rgDate hora": 0,

            "uDate día": 0,
            "uDate mes": 0,
            "uDate año": 0,
            "uDate hora": 0

        }
    },



    {
        "$addFields": {
            "nombre_maestro_principal1": "Orden_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal1": {
                "$strLenCP": "$nombre_maestro_principal1"
            }
        }
    }


    , {
        "$addFields": {
            "nombre_maestro_enlazado1": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal1"]
                                        }, "$nombre_maestro_principal1"]
                                    },

                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal1",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }


    , {
        "$addFields": {
            "nombre_maestro_enlazado1": { "$arrayElemAt": ["$nombre_maestro_enlazado1", 0] }
        }
    }
    , {
        "$addFields": {
            "nombre_maestro_enlazado1": { "$ifNull": ["$nombre_maestro_enlazado1", ""] }
        }
    }


    , {
        "$addFields": {
            "valor_maestro_enlazado1": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal1"]
                                        }, "$nombre_maestro_principal1"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_maestro_enlazado1",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "valor_maestro_enlazado1": { "$ifNull": ["$valor_maestro_enlazado1", ""] }
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal1": 0,
            "num_letras_nombre_maestro_principal1": 0
        }
    }


    , {
        "$addFields": {
            "nombre_maestro_principal2": "Orden Insectos_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal2": {
                "$strLenCP": "$nombre_maestro_principal2"
            }
        }
    }


    , {
        "$addFields": {
            "nombre_maestro_enlazado2": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal2"]
                                        }, "$nombre_maestro_principal2"]
                                    },
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal2",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "nombre_maestro_enlazado2": { "$arrayElemAt": ["$nombre_maestro_enlazado2", 0] }
        }
    }
    , {
        "$addFields": {
            "nombre_maestro_enlazado2": { "$ifNull": ["$nombre_maestro_enlazado2", ""] }
        }
    }



    , {
        "$addFields": {
            "valor_maestro_enlazado2": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal2"]
                                        }, "$nombre_maestro_principal2"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_maestro_enlazado2",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "valor_maestro_enlazado2": { "$ifNull": ["$valor_maestro_enlazado2", ""] }
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal2": 0,
            "num_letras_nombre_maestro_principal2": 0
        }
    }


    , {
        "$addFields": {
            "Orden Insectos": { "$ifNull": ["$Orden", "$Orden Insectos"] }
        }
    }
    , {
        "$addFields": {
            "nombre_maestro_enlazado": {
                "$cond": {
                    "if": {
                        "$eq": ["$nombre_maestro_enlazado1", ""]
                    },
                    "then": "$nombre_maestro_enlazado2",
                    "else": "$nombre_maestro_enlazado1"
                }
            }
        }
    }

    , {
        "$addFields": {
            "valor_maestro_enlazado": {
                "$cond": {
                    "if": {
                        "$eq": ["$valor_maestro_enlazado1", ""]
                    },
                    "then": "$valor_maestro_enlazado2",
                    "else": "$valor_maestro_enlazado1"
                }
            }
        }
    }

    , {
        "$project": {
            "nombre_maestro_enlazado1": 0,
            "nombre_maestro_enlazado2": 0,
            "valor_maestro_enlazado1": 0,
            "valor_maestro_enlazado2": 0
        }
    }


    , {
        "$addFields": {
            "variable_cartografia": "$Unidad"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Unidad": 0

            , "uid": 0
            , "uDate": 0
            , "Orden": 0
        }
    }
    , { "$addFields": { "rgDate_fecha": "$rgDate" } }


    , {
        "$project": {

            "finca": 1,
            "bloque": 1,
            "lote": 1,
            "linea": 1,
            "arbol": 1,
            "rgDate_fecha": 1,

            "Sampling": 1,
            "Formula": 1,
            "Arbol sano": 1,
            "Fisiopatias": 1,
            "Cantidad individuos": 1,
            "Estado fenologico": 1,
            "supervisor": 1,
            "rgDate": 1,
            "capture": 1,
            "Orden Insectos": 1,
            "nombre_maestro_enlazado": 1,
            "valor_maestro_enlazado": 1



        }
    }








]
