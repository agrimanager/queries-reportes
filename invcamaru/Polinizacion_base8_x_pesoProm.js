db.form_censodepolinizacion.aggregate(

    [
        //====CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$toObjectId": "$Point.farm" },

                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        //====FECHAS
        // , {
        //     "$addFields": {
        //         "Fecha": {
        //             "$dateToString": {
        //                 "date": "$rgDate",
        //                 "format": "%Y-%m-%d",
        //                 "timezone": "America/Bogota"
        //             }
        //         }
        //     }
        // }

        //---info fechas
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate", "timezone": "America/Bogota" } },
                "num_mes": { "$month": { "date": "$rgDate", "timezone": "America/Bogota" } }
                // "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate", "timezone": "America/Bogota" } },
                // "num_semana": { "$week": { "date": "$rgDate", "timezone": "America/Bogota" } },
                // "num_dia_semana": { "$dayOfWeek": { "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }




        , {
            "$group": {
                "_id": {
                    //"fecha_censo": "$Fecha",

                    "num_anio": "$num_anio",
                    "num_mes": "$num_mes",
                    "lote": "$lote"
                }


                , "plantas_censadas": { "$sum": 1 }
                , "aplicacion1": { "$sum": { "$toDouble": "$Aplicacion 1" } }
                , "aplicacion2": { "$sum": { "$toDouble": "$Aplicacion 2" } }
                , "kit": { "$sum": { "$toDouble": "$Kit utilizados" } }
            }
        }

        , {
            "$sort": {
                "_id.num_mes": 1,
                "_id.lote": 1
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_censadas": "$plantas_censadas"
                            , "aplicacion1": "$aplicacion1"
                            , "aplicacion2": "$aplicacion2"
                            , "kit": "$kit"
                        }
                    ]
                }
            }
        }


        //=====CRUZAR CON PESOS PROMEDIOS (X TRIMESTRE)
        , {
            "$lookup": {
                "from": "form_pesospromedioracimo",
                "as": "Peso_promedio_lote",
                "let": {
                    "nombre_lote": "$lote",
                    "num_anio": "$num_anio",
                    "num_mes": "$num_mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    //lote
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] },

                                    //anio
                                    { "$eq": ["$$num_anio", { "$year": "$rgDate" }] },
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    },


                    //mes (x trimestre)

                    //          "Enero a Marzo" : 5,
                    //          "Abril a junio" : 6,
                    //          "Julio a septiembre" : 7,
                    //          "Ocubre a diciembre" : 8,

                    //  "SIEMBRA" : "2014",
                    //  "TRIMESTRE 1" : 0,
                    //  "TRIMESTRE 2" : 0,
                    //  "TRIMESTRE 3" : 0,
                    //  "TRIMESTRE 4" : 20.2,

                    {
                        "$addFields": {
                            "peso_prom_mes": {
                                "$switch": {
                                    "branches": [{
                                        "case": {
                                            "$in": ["$$num_mes", [1, 2, 3]]
                                        },
                                        "then": "$TRIMESTRE 1"
                                    }, {
                                        "case": {
                                            "$in": ["$$num_mes", [4, 5, 6]]
                                        },
                                        "then": "$TRIMESTRE 2"
                                    }, {
                                        "case": {
                                            "$in": ["$$num_mes", [7, 8, 9]]
                                        },
                                        "then": "$TRIMESTRE 3"
                                    }, {
                                        "case": {

                                            "$in": ["$$num_mes", [10, 11, 12]]
                                        },
                                        "then": "$TRIMESTRE 4"
                                    }],
                                    "default": 0
                                }
                            }
                        }
                    },

                    {
                        "$project": {
                            "_id": 0,
                            "peso_prom_mes": 1
                            , "SIEMBRA": 1
                        }
                    },


                ]
            }
        },
        {
            "$unwind": {
                "path": "$Peso_promedio_lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Peso_promedio_lote": {
                    "$ifNull": ["$Peso_promedio_lote.peso_prom_mes", 0]
                }
                , "Siembra": {
                    "$ifNull": ["$Peso_promedio_lote.SIEMBRA", 0]
                }
            }
        }



        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }






    ], { allowDiskUse: true }

)