//----Ojo usar desde db local

// DBs
var db_desde = "lukeragricola";//db1

var db_hacia = "invcamaru"; //db2
var uid_hacia = ObjectId("5dde7df9d4db191919dadcbc");
var supervisores_hacia = [
            ObjectId("5f088ba3176ca56f022873d4"),
            ObjectId("5f088ba3176ca56f022873da"),
            ObjectId("5fbe5d619a9424132de40275"),
            ObjectId("5fbe65339a9424132de402ae"),
            ObjectId("6005acdc658c4d6d61c2c57e"),
            ObjectId("5f088ba4176ca56f022873e6"),
            ObjectId("6005b0d6658c4d6d61c2c586"),
            ObjectId("6005b159658c4d6d61c2c58e"),
            ObjectId("60252a6827ed8e5f95fa83b5"),
            ObjectId("60252cf527ed8e5f95fa83e6"),
            ObjectId("6086b4576505261dccf75ee1"),
            ObjectId("6086b5866505261dccf75ef0"),
            ObjectId("60ae53449f93705bf7dfaedd")
    ];

//============= Maestros

//Evaluaciones de Calidad -----ppal
//5eb0cee9817cca0e65733c6e

var mestros_desde = db.getSiblingDB(db_desde)
    .getCollection("masters")
    .aggregate(
        [
            {
                $match:{
                    //name:"Evaluaciones de Calidad"
                    _id:{
                        $in:[

                        //---maestros enlazados
                        ObjectId("5f04dd7e198c6b56bd25894b"),
                        ObjectId("5eb0e5913a3a1b28cc3e568e"),
                        ObjectId("5eb0e5963a3a1b28cc3e56c0"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b8"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b4"),
                        ObjectId("5eb0e5943a3a1b28cc3e56b1"),
                        ObjectId("5eb0e5963a3a1b28cc3e56bd"),
                        ObjectId("5eb0e5943a3a1b28cc3e56b0"),
                        ObjectId("5eb0e5943a3a1b28cc3e56af"),
                        ObjectId("5eb0e5943a3a1b28cc3e56ae"),
                        ObjectId("5eb0e5943a3a1b28cc3e56ad"),
                        ObjectId("5eb0e5943a3a1b28cc3e56aa"),
                        ObjectId("5eb0e5943a3a1b28cc3e56a9"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a5"),
                        ObjectId("5eb0e5963a3a1b28cc3e56be"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a4"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a2"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a0"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a3"),
                        ObjectId("5eb0e5933a3a1b28cc3e569f"),
                        ObjectId("5eb0e5923a3a1b28cc3e569e"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b9"),
                        ObjectId("5eb0e5923a3a1b28cc3e569c"),
                        ObjectId("5ecedce1e9891e1fa017a86f"),
                        ObjectId("5eb0e5923a3a1b28cc3e569b"),
                        ObjectId("5eb0e5923a3a1b28cc3e5699"),
                        ObjectId("5eb0e58f3a3a1b28cc3e567d"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b5"),
                        ObjectId("5eb0e58c3a3a1b28cc3e5669"),
                        ObjectId("5eb0e5963a3a1b28cc3e56bf"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5677"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5675"),
                        ObjectId("5eb0e5923a3a1b28cc3e5697"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5680"),
                        ObjectId("5eb0e58d3a3a1b28cc3e5672"),
                        ObjectId("5eb0e58c3a3a1b28cc3e566a"),
                        ObjectId("5eced1d1e9891e1fa017a5e6"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b7"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5674"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b6"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5685"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5679"),
                        ObjectId("5eb0e5903a3a1b28cc3e568b"),
                        ObjectId("5eb0e58d3a3a1b28cc3e5671"),
                        ObjectId("5eb0e58e3a3a1b28cc3e567b"),
                        ObjectId("5eb0e5943a3a1b28cc3e56ab"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a6"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a1"),
                        ObjectId("5eb0e5913a3a1b28cc3e5693"),
                        ObjectId("5eb0e5923a3a1b28cc3e5698"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5678"),
                        ObjectId("5eb0e58e3a3a1b28cc3e567c"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a7"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5681"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5683"),
                        ObjectId("5eb0e58c3a3a1b28cc3e5667"),
                        ObjectId("5eb0e5913a3a1b28cc3e5690"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b2"),
                        ObjectId("5eb0e58d3a3a1b28cc3e566f"),
                        ObjectId("5eb0e5913a3a1b28cc3e5694"),
                        ObjectId("5eced399e9891e1fa017a679"),
                        ObjectId("5eb0e58e3a3a1b28cc3e5676"),
                        ObjectId("5eb0e5963a3a1b28cc3e56c1"),
                        ObjectId("5eb0e58d3a3a1b28cc3e566d"),
                        ObjectId("5eb0e58d3a3a1b28cc3e566c"),
                        ObjectId("5eb0e5963a3a1b28cc3e56bc"),
                        ObjectId("5eb0e58d3a3a1b28cc3e5673"),
                        ObjectId("5eb0e5903a3a1b28cc3e5689"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5682"),
                        ObjectId("5eb0e58f3a3a1b28cc3e567e"),
                        ObjectId("5eb0e5943a3a1b28cc3e56ac"),
                        ObjectId("5eb0e58f3a3a1b28cc3e567f"),
                        ObjectId("5eb0e58f3a3a1b28cc3e5684"),
                        ObjectId("5eb0e5953a3a1b28cc3e56b3"),
                        ObjectId("5eb0e58d3a3a1b28cc3e566b"),
                        ObjectId("5eb0e58d3a3a1b28cc3e566e"),
                        ObjectId("5eb0e5903a3a1b28cc3e5686"),
                        ObjectId("5eb0e5933a3a1b28cc3e56a8"),
                        ObjectId("5eb0e5923a3a1b28cc3e569a"),
                        ObjectId("5eb0e58c3a3a1b28cc3e5668"),
                        ObjectId("5eb0e5903a3a1b28cc3e5687"),
                        ObjectId("5eb0e5953a3a1b28cc3e56bb"),
                        ObjectId("5eb0e5923a3a1b28cc3e5696"),
                        ObjectId("5eb0e5903a3a1b28cc3e5688"),
                        ObjectId("5eb0e5903a3a1b28cc3e568a"),
                        ObjectId("5eb0e58e3a3a1b28cc3e567a"),
                        ObjectId("5eb0e5903a3a1b28cc3e568c"),
                        ObjectId("5eb0e5953a3a1b28cc3e56ba"),
                        ObjectId("5eb0e58d3a3a1b28cc3e5670"),
                        ObjectId("5eb0e5903a3a1b28cc3e568d"),
                        ObjectId("5eb0e5913a3a1b28cc3e568f"),
                        ObjectId("5eb0e5923a3a1b28cc3e569d"),
                        ObjectId("5eb0e5913a3a1b28cc3e5691"),
                        ObjectId("5eb0e5913a3a1b28cc3e5692"),
                        ObjectId("5f4d0bfd16d7173264f839ba"),
                        ObjectId("5eb0e5913a3a1b28cc3e5695"),

                        //maestro ppal
                        ObjectId("5eb0cee9817cca0e65733c6e"),


                        ]
                    }
                }
            }
        ]
        )
    .allowDiskUse();


//--🔄 data
mestros_desde.forEach(item_maestro => {

    //modificar uid
    item_maestro.uid = uid_hacia;

    //insertar maestro
    db.getSiblingDB(db_hacia)
        .getCollection("masters")
        .insert(item_maestro);
});
// mestros_desde.close();


//============= Formularios

//Evaluacion de Calidad
//5e5ece026712a43d860b936f

var formularios_desde = db.getSiblingDB(db_desde)
    .getCollection("forms")
    .aggregate(
        [
            {
                $match:{
                    name:"Evaluacion de Calidad"
                }
            }
        ]
        )
    .allowDiskUse();


//--🔄 data
formularios_desde.forEach(item_formulario => {

    //modificar uid
    item_formulario.uid = uid_hacia;
    //modificar supervisors
    item_formulario.supervisors = supervisores_hacia;

    //crear coleccion
    db.getSiblingDB(db_hacia).createCollection(item_formulario.anchor).ok;

    //insertar formulario
    db.getSiblingDB(db_hacia)
        .getCollection("forms")
        .insert(item_formulario);
});
// formularios_desde.close();





//-------NO SE VAN A MIGRAR REPORTES POR EL MOMENTO POR QUE TIENEN CRUCE CON (CONFIGURACION DE CALIDAD)
/*
//============= Reportes

var reportes_desde = db.getSiblingDB(db_desde)
    .getCollection("reports")
    .aggregate()
    .allowDiskUse();


//--🔄 data
reportes_desde.forEach(item_reporte => {

    //modificar uid
    item_reporte.uid = uid_hacia;


    //insertar reporte
    db.getSiblingDB(db_hacia)
        .getCollection("reports")
        .insert(item_reporte);
});
// reportes_desde.close();
*/