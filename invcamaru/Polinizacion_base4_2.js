db.form_censodepolinizacion.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$toObjectId": "$Point.farm" },

                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        , {
            "$addFields": {
                "Fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%Y-%m-%d",
                        "timezone": "America/Bogota"
                    }
                }
            }
        }



        , {
            "$addFields": {
                "Hora": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%H",
                        "timezone": "America/Bogota"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "rango_hora": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$gte": [{ "$toDouble": "$Hora" }, 6] }
                                , { "$lte": [{ "$toDouble": "$Hora" }, 12] }
                            ]
                        },
                        "then": "A-Dia-(6-12)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gte": [{ "$toDouble": "$Hora" }, 13] }
                                        , { "$lte": [{ "$toDouble": "$Hora" }, 16] }
                                    ]
                                },
                                "then": "B-Tarde-(1-4)",
                                "else": "C-(otro)"
                            }
                        }
                    }
                }
            }
        }





        // , {
        //     "$group": {
        //         "_id": {
        //             "fecha_censo": "$Fecha",
        //             "rango_hora": "$rango_hora",
        //             "empleado": "$supervisor"

        //         }
        //         , "aplicacion1": { "$sum": { "$toDouble": "$Aplicacion 1" } }
        //         , "aplicacion2": { "$sum": { "$toDouble": "$Aplicacion 2" } }

        //     }
        // }

        // , {
        //     "$sort": {
        //         "_id.fecha_censo": 1,
        //         "_id.empleado": 1
        //     }
        // }



        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {
        //                     "aplicacion_total": { "$sum": ["$aplicacion1", "$aplicacion2"] }
        //                 }
        //             ]
        //         }
        //     }
        // }




    ], { allowDiskUse: true }

)