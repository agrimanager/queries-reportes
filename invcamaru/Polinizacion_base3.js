db.form_censodepolinizacion.aggregate(

    [
        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                //"finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "finca": { "$toObjectId": "$Point.farm" },

                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                //"localField": "finca._id",
                "localField": "finca",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        //===== FECHAS
        , {
            "$addFields": {
                "Fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%Y-%m-%d",
                        //"timezone": "-0500"
                        "timezone": "America/Bogota"
                    }
                }
            }
        }




        //===== CALCULOS
        , {
            "$group": {
                "_id": {
                    "fecha_censo": "$Fecha",
                    "empleado": "$supervisor" // x empleado
                    //"lote": "$lote" // x lote

                    // ,"planta":"$arbol"
                }

                // ,"data":{"$push":"$$ROOT"}
                , "plantas_censadas": { "$sum": 1 }
                , "aplicacion1": { "$sum": { "$toDouble": "$Aplicacion 1" } }
                , "aplicacion2": { "$sum": { "$toDouble": "$Aplicacion 2" } }
                , "kit": { "$sum": { "$toDouble": "$Kit utilizados" } }
            }
        }

        , {
            "$sort": {
                "_id.fecha_censo": 1,
                "_id.empleado": 1
                //"_id.lote": 1
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_censadas": "$plantas_censadas"
                            , "aplicacion1": "$aplicacion1"
                            , "aplicacion2": "$aplicacion2"
                            , "kit": "$kit"
                        }
                    ]
                }
            }
        }




    ], { allowDiskUse: true }

)