db.form_censodepolinizacion.aggregate(

    [
        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                //"finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "finca": { "$toObjectId": "$Point.farm" },

                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                //"localField": "finca._id",
                "localField": "finca",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        //===== FECHAS
        , {
            "$addFields": {
                "Fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%Y-%m-%d",
                        //"timezone": "-0500"
                        "timezone": "America/Bogota"
                    }
                }
            }
        }


        //---HORAS
        , {
            "$addFields": {
                "Hora": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%H",
                        //"timezone": "-0500"
                        "timezone": "America/Bogota"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "rango_hora": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$gte": [{ "$toDouble": "$Hora" }, 7] }
                                , { "$lte": [{ "$toDouble": "$Hora" }, 12] }
                            ]
                        },
                        "then": "(7-12)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$gte": [{ "$toDouble": "$Hora" }, 13] }
                                        , { "$lte": [{ "$toDouble": "$Hora" }, 16] }
                                    ]
                                },
                                "then": "(1-4)",
                                "else": "(otro)"
                            }
                        }
                    }
                }
            }
        }




        //===== CALCULOS
        , {
            "$group": {
                "_id": {
                    "fecha_censo": "$Fecha",
                    "rango_hora": "$rango_hora",
                    "empleado": "$supervisor" // x empleado

                }
                , "aplicacion1": { "$sum": { "$toDouble": "$Aplicacion 1" } }
                , "aplicacion2": { "$sum": { "$toDouble": "$Aplicacion 2" } }

            }
        }

        , {
            "$sort": {
                "_id.fecha_censo": 1,
                "_id.empleado": 1
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "aplicacion_total": { "$sum": ["$aplicacion1", "$aplicacion2"] }
                        }
                    ]
                }
            }
        }




    ], { allowDiskUse: true }

)