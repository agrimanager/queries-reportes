db.form_censodepolinizacion.aggregate(
    [


        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // ,{
                        //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        // }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        // {
        //     "$addFields": {
        //         "variable_cartografia": "$Arbol" //🚩editar
        //     }
        // },
        // { "$unwind": "$variable_cartografia.features" },

        // {
        //     "$addFields": {
        //         "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": {
        //             "$concatArrays": [
        //                 "$split_path_padres_oid",
        //                 "$variable_cartografia_oid"
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$lookup": {
        //         "from": "cartography",
        //         "localField": "split_path_oid",
        //         "foreignField": "_id",
        //         "as": "objetos_del_cultivo"
        //     }
        // },

        // {
        //     "$addFields": {
        //         "tiene_variable_cartografia": {
        //             "$cond": {
        //                 "if": {
        //                     "$eq": [
        //                         { "$size": { "$ifNull": ["$split_path_oid", []] } }
        //                         , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
        //                 },
        //                 "then": "si",
        //                 "else": "no"
        //             }
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "objetos_del_cultivo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
        //                 "then": "$objetos_del_cultivo",
        //                 "else": {
        //                     "$concatArrays": [
        //                         "$objetos_del_cultivo",
        //                         ["$variable_cartografia.features"]
        //                     ]
        //                 }
        //             }
        //         }
        //     }
        // },



        // {
        //     "$addFields": {
        //         "finca": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$finca",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "finca._id",
        //         "foreignField": "_id",
        //         "as": "finca"
        //     }
        // },
        // { "$unwind": "$finca" },

        // { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        // {
        //     "$addFields": {
        //         "bloque": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$bloque",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        // {
        //     "$addFields": {
        //         "lote": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$lote",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        // {
        //     "$addFields": {
        //         "linea": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$linea",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        // {
        //     "$addFields": {
        //         "arbol": {
        //             "$filter": {
        //                 "input": "$objetos_del_cultivo",
        //                 "as": "item_cartografia",
        //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
        //             }
        //         }
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$arbol",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },
        // { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        // {
        //     "$project": {
        //         "variable_cartografia": 0,
        //         "split_path_padres": 0,
        //         "split_path_padres_oid": 0,
        //         "variable_cartografia_oid": 0,
        //         "split_path_oid": 0,
        //         "objetos_del_cultivo": 0,
        //         "tiene_variable_cartografia": 0

        //         , "Point": 0
        //         , "Arbol": 0 //🚩editar : 0

        //         , "Formula": 0
        //         , "uid": 0
        //         , "uDate": 0
        //         , "capture": 0
        //     }
        // }

        // // ,{ "$sort": { "rgDate": -1 } }


        // , {
        //     "$group": {
        //         "_id": {
        //             "finca": "$finca",
        //             "bloque": "$bloque",
        //             "lote": "$lote",
        //             "arbol": "$arbol"
        //         }
        //         , "max_fecha": { "$max": "$rgDate" }
        //         // , "data": { "$push": "$$ROOT" }
        //     }
        // }

        // //   , { "$unwind": "$data" }



        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$data",
        // //                 {
        // //                     "max_fecha": "$max_fecha"
        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }


        // //------------



        { "$addFields": { "variable_cartografia": "$Arbol" } },

        { "$addFields": { "elemnq": "$_id" } },

        { "$sort": { "rgDate": -1 } },


        { "$match": { "variable_cartografia.path": { "$ne": "" } } },



        { "$unwind": "$variable_cartografia.features" }


        , {
            "$project": {

                "Point": 0
                , "Arbol": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
            }
        }



        , {
            "$group": {
                "_id": {
                    "nombre_cartografia": "$variable_cartografia.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$addFields": { "data": { "$arrayElemAt": ["$data", 0] } } },

        {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        "$divide": [{ "$subtract": ["$_id.today", "$data.rgDate"] }, 86400000]
                    }
                }
            }
        },



        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "#3f3b69",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "#008000",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                                , { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "#FFFF00",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                        , { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "#ff8000",
                                                "else": "#FF0000"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "error dias",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "A-[0 a 9] dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                                , { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "B-[10 a 12] dias",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                        , { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "C-[13 a 15] dias",
                                                "else": "D-[>15] dias"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        },


        {
            "$addFields": {
                "split_cartografia": { "$split": [{ "$trim": { "input": "$_id.nombre_cartografia", "chars": "-" } }, "-"] }
            }
        }

        , {
            "$addFields": {
                "bloque": { "$arrayElemAt": ["$split_cartografia", 0] }

                , "lote": {
                    "$concat": [
                        { "$arrayElemAt": ["$split_cartografia", 0] },
                        "-",
                        { "$arrayElemAt": ["$split_cartografia", 1] }
                    ]
                }

                , "linea": {
                    "$concat": [
                        { "$arrayElemAt": ["$split_cartografia", 0] },
                        "-",
                        { "$arrayElemAt": ["$split_cartografia", 1] },
                        "-",
                        { "$arrayElemAt": ["$split_cartografia", 2] }
                    ]
                }
            }
        }


        , {
            "$project": {
                "_id": "$data.elemnq",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$_id.nombre_cartografia",
                "Rango": "$rango",
                "Dias": "$dias de ciclo",
                "Dias Ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$dias de ciclo", -1] },
                        "then": "-1",
                        "else": {
                            "$concat": [
                                { "$toString": "$dias de ciclo" },
                                " dias"
                            ]
                        }
                    }
                }
            }
        }


    ]
    , { allowDiskUse: true }
)
