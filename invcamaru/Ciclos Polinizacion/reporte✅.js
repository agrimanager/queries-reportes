[



    { "$addFields": { "variable_cartografia": "$Arbol" } },

    { "$addFields": { "elemnq": "$_id" } },

    { "$sort": { "rgDate": -1 } },


    { "$match": { "variable_cartografia.path": { "$ne": "" } } },



    { "$unwind": "$variable_cartografia.features" }


    , {
        "$project": {

            "Point": 0
            , "Arbol": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "nombre_cartografia": "$variable_cartografia.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$addFields": { "data": { "$arrayElemAt": ["$data", 0] } } },

    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$data.rgDate"] }, 86400000]
                }
            }
        }
    },



    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$lt": ["$dias de ciclo", 0] },
                    "then": "#3f3b69",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "#008000",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                            , { "$lte": ["$dias de ciclo", 12] }]
                                    },
                                    "then": "#FFFF00",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                    , { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "#ff8000",
                                            "else": "#FF0000"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$lt": ["$dias de ciclo", 0] },
                    "then": "error dias",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "A-[0 a 9] dias",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                            , { "$lte": ["$dias de ciclo", 12] }]
                                    },
                                    "then": "B-[10 a 12] dias",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                    , { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "C-[13 a 15] dias",
                                            "else": "D-[>15] dias"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    },


    {
        "$addFields": {
            "split_cartografia": { "$split": [{ "$trim": { "input": "$_id.nombre_cartografia", "chars": "-" } }, "-"] }
        }
    }

    , {
        "$addFields": {
            "bloque": { "$arrayElemAt": ["$split_cartografia", 0] }

            , "lote": {
                "$concat": [
                    { "$arrayElemAt": ["$split_cartografia", 0] },
                    "-",
                    { "$arrayElemAt": ["$split_cartografia", 1] }
                ]
            }

            , "linea": {
                "$concat": [
                    { "$arrayElemAt": ["$split_cartografia", 0] },
                    "-",
                    { "$arrayElemAt": ["$split_cartografia", 1] },
                    "-",
                    { "$arrayElemAt": ["$split_cartografia", 2] }
                ]
            }
        }
    }


    , {
        "$project": {
            "_id": "$data.elemnq",
            "bloque": "$bloque",
            "lote": "$lote",
            "linea": "$linea",
            "arbol": "$_id.nombre_cartografia",
            "Rango": "$rango",
            "Dias": "$dias de ciclo",
            "Dias Ciclo": {
                "$cond": {
                    "if": { "$eq": ["$dias de ciclo", -1] },
                    "then": "-1",
                    "else": {
                        "$concat": [
                            { "$toString": "$dias de ciclo" },
                            " dias"
                        ]
                    }
                }
            }
        }
    }


]
