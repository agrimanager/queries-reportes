
//ticket#1514
//Se deben contar los días a partir del registro generado en la línea 1 de cada lote
//, hasta volver a esta misma línea de cada lote.


db.form_censodepolinizacion.aggregate(
    [


        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": 123
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // ,{
                        //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        // }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        //------MAPA_VARIABLE_ID_FEATURE
        {
            "$addFields": {
                "cartography_id": { "$ifNull": ["$arbol._id", null] }
            }
        },



        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0
                , "Formula": 0
            }
        }


        //primera linea del lote
        , {
            "$addFields": {
                "split_linea": { "$split": [{ "$trim": { "input": "$linea", "chars": "-" } }, "-"] }
            }
        }
        , {
            "$addFields": {
                "num_linea": { "$arrayElemAt": ["$split_linea", -1] }
            }
        }
        , {
            "$match": {
                "num_linea": "1"
            }
        }

        // , { "$sort": { "rgDate": -1 } }


        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }


        , {
            "$group": {
                "_id": {
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "linea": "$linea"

                    , "fecha": "$Fecha_Txt"


                    , "today": "$today"
                    , "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$addFields": { "data": { "$arrayElemAt": ["$data", 0] } } }

        , { "$sort": { "_id.fecha": -1 } }

        //mapa
        , { "$addFields": { "cartography_id": "$data.cartography_id" } }



        , {
            "$group": {
                "_id": {
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote",
                    "linea": "$_id.linea"

                    // ,"fecha":"$Fecha_Txt"


                    // ,"today": "$today"
                    , "idform": "$_id.idform"
                    // ,"cartography_id": "$cartography_id"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }


        , { "$addFields": { "size_data": { "$size": "$data" } } }

        , {
            "$addFields": {
                "data1": { "$arrayElemAt": ["$data", 0] }
            }
        }

        , {
            "$addFields": {
                "data2": {
                    "$cond": {
                        "if": { "$eq": ["$size_data", 1] },
                        "then": {},
                        "else": { "$arrayElemAt": ["$data", 1] }
                    }
                }
            }
        }



        //---variables importantes
        , {
            "$addFields": {
                "estado del ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$size_data", 1] },
                        "then": "NO finalizado",
                        "else": "SI finalizado"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "fecha inicio cliclo": {
                    "$cond": {
                        "if": { "$eq": ["$size_data", 1] },
                        "then": "$data1._id.fecha",//"NO finalizado",
                        "else": "$data2._id.fecha",//"SI finalizado"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "fecha fin cliclo": {
                    "$cond": {
                        "if": { "$eq": ["$size_data", 1] },
                        "then": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data1._id.today", "timezone": "America/Bogota" } },//"NO finalizado", hoy
                        "else": "$data1._id.fecha",//"SI finalizado"
                    }
                }
            }
        }



        //dias de ciclo
        , {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        "$divide": [
                            {
                                "$subtract": [
                                    { "$toDate": "$fecha fin cliclo" }
                                    , { "$toDate": "$fecha inicio cliclo" }
                                ]
                            }
                            , 86400000]
                    }
                }
            }
        }


        //------MAPA_VARIABLE_ID_FEATURE
        , {
            "$addFields": {
                "cartography_id": "$data1.cartography_id"
            }
        }



        //================================
        //-----MAPA


        //NOTA!!!!!!!!!
        //la geometria del lote rompia la agrupacion del lote
        //....===>>>entonces hacer lookup para sacar geometria
        //------MAPA_VARIABLE_GEOMETRY
        , {
            "$lookup": {
                "from": "cartography",
                "localField": "_id.lote",
                "foreignField": "properties.name",
                "as": "info_lote"
            }
        }
        , { "$unwind": "$info_lote" }
        , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
        , {
            "$project": {
                "info_lote": 0
            }
        }
        //----------------


        //--leyenda
        // #Dias de Ciclo Polinizacion: #ffffff,Ciclo polinizacion de 0 a 4 dias: #ffff00,Ciclo polinizacion de 5 a 7 dias: #ff0080,Ciclo polinizacion de 8 a 10 dias: #008000,Ciclo polinizacion de 11 a 13 dias: #ffa500,Ciclo polinizacion > 13 dias: #ff0000


        // --color
        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "#000000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 4] }]
                                },
                                "then": "#ffff00",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 5] }
                                                , { "$lte": ["$dias de ciclo", 7] }]
                                        },
                                        "then": "#ff0080",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 8] }
                                                        , { "$lte": ["$dias de ciclo", 10] }]
                                                },
                                                "then": "#008000",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$dias de ciclo", 11] }
                                                                , { "$lte": ["$dias de ciclo", 13] }]
                                                        },
                                                        "then": "#ffa500",
                                                        "else": "#ff0000"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },

                "rango": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "error dias",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 4] }]
                                },
                                "then": "A-[0 a 4] dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 5] }
                                                , { "$lte": ["$dias de ciclo", 7] }]
                                        },
                                        "then": "B-[5 a 7] dias",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 8] }
                                                        , { "$lte": ["$dias de ciclo", 10] }]
                                                },
                                                "then": "C-[8 a 10] dias",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$dias de ciclo", 11] }
                                                                , { "$lte": ["$dias de ciclo", 13] }]
                                                        },
                                                        "then": "D-[11 a 13] dias",
                                                        "else": "E-[>13] dias"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$_id.idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {
                    "Bloque": "$_id.bloque",
                    "Lote": "$_id.lote",
                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color",

                    "Estado del ciclo": "$estado del ciclo",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },

                    "fecha inicio cliclo": "$fecha inicio cliclo",
                    "fecha fin cliclo": "$fecha fin cliclo"

                }

            }
        }


    ]
    , { allowDiskUse: true }
)
