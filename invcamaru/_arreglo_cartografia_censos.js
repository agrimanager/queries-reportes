



//======= Fincas (modificar ids de fincas a remplazar en formularios)
var id_finca_vieja = "5dde9e56d4db191919dadcc8";
var id_finca_nueva = "5dde82e1d4db191919dadcc4";


//--obtener formularios
var formularios = db.forms.find({})

//--ciclar formularios
formularios.forEach(item => {


    db.getCollection(item.anchor).updateMany(
        {
            "Point.farm": id_finca_vieja
        },
        {
            $set: {
                "Point.farm": id_finca_nueva
            }
        }
    )
});



//============EMPLEADOS 8borrar finca)

//--actualizar empleados con finca vieja
db.employees.aggregate(
    {
        "$addFields": {
            "fids": {
                "$filter": {
                    "input": "$fids",
                    "as": "finca",
                    "cond": { "$ne": ["$$finca", ObjectId("5dde9e56d4db191919dadcc8")] }
                }
            }
        }
    }


    , {
        $out: "employees"
    }

)