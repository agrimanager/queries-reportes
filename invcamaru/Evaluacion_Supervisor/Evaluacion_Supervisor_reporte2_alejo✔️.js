[

      
      {
          "$addFields": {
              "split_path": { "$split": [{ "$trim": { "input": "$evaluacion labores.path", "chars": "," } }, ","] }
          }
      },
      {
          "$addFields": {
              "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
          }
      },
      {
          "$addFields": {
              "features_oid": { "$map": { "input": "$evaluacion labores.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
          }
      },
      {
          "$addFields": {
              "split_path_oid": {
                  "$concatArrays": [
                      "$split_path_oid",
                      "$features_oid"
                  ]
              }
          }
      },

      {
          "$lookup": {
              "from": "cartography",
              "localField": "split_path_oid",
              "foreignField": "_id",
              "as": "objetos_del_cultivo"
          }
      },

      {
          "$addFields": {
              "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
              "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
              "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
              "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
              "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }


          }
      },

      {
          "$addFields": {
              "bloque": "$bloque.properties.name",
              "lote": "$lote.properties.name",
              "linea": "$linea.properties.name",
              "arbol": "$arbol.properties.name"



          }
      },

      {
          "$lookup": {
              "from": "farms",
              "localField": "finca._id",
              "foreignField": "_id",
              "as": "finca"
          }
      },

      {
          "$addFields": {
              "finca": "$finca.name"
          }
      },
      { "$unwind": "$finca" },



      {
          "$project": {
              "split_path": 0,
              "split_path_oid": 0,
              "objetos_del_cultivo": 0,
              "features_oid": 0
          }
      }

      , {
          "$addFields": {
              "array_data": [
                  {
                      "calificacion_nombre": "Abertura espata",
                      "calificacion_medida": "$Abertura espata"
                  },
                  {
                      "calificacion_nombre": "Marcacion",
                      "calificacion_medida": "$Marcacion"
                  },
                  {
                      "calificacion_nombre": "aplicacion ana",
                      "calificacion_medida": "$aplicacion ana"
                  }
              ]
          }
      }
      
      ,{ "$unwind": "$array_data" }
      
      
      , {
          "$addFields": {
              "calificacion_nombre": "$array_data.calificacion_nombre",
              "calificacion_medida": "$array_data.calificacion_medida"
          }
      }
      
      ,{
          "$project": {
              "array_data": 0
          }
      }

      , {
          "$addFields": {
              "arbol_aux": 1
          }
      }



  ]