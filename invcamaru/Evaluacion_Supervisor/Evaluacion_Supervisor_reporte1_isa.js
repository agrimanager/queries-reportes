db.form_evaluacionsupervisor.aggregate(
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$evaluacion labores.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$evaluacion labores.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }


            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"



            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },


        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "espata": "$Abertura espata"

                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_espata": { "$sum": 1 }
            }
        }



        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote": { "$sum": "$plantas_dif_censadas_x_lote_x_espata" }
            }
        },





        { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote_x_espata": "$data.plantas_dif_censadas_x_lote_x_espata",
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        },






        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "marcacion": "$Marcacion"

                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_marcacion": { "$sum": 1 }
            }
        }



        , { "$unwind": "$data" }




        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "plantas_dif_censadas_x_lote_x_marcacion": "$plantas_dif_censadas_x_lote_x_marcacion"

                        }
                    ]
                }
            }
        },









        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Aplicacion ana": "$aplicacion ana"

                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_aplicacion": { "$sum": 1 }
            }
        }



        , { "$unwind": "$data" }




        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "plantas_dif_censadas_x_lote_x_aplicacion": "$plantas_dif_censadas_x_lote_x_aplicacion"

                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "PTC_espata": {
                    "$multiply": [
                        { "$divide": ["$plantas_dif_censadas_x_lote_x_espata", "$plantas_dif_censadas_x_lote"] }
                        , 100]
                }
            }
        },



        {
            "$addFields": {
                "PTC_marcacion": {
                    "$multiply": [
                        { "$divide": ["$plantas_dif_censadas_x_lote_x_marcacion", "$plantas_dif_censadas_x_lote"] }
                        , 100]
                }
            }
        },




        {
            "$addFields": {
                "PTC_aplicacion": {
                    "$multiply": [
                        { "$divide": ["$plantas_dif_censadas_x_lote_x_aplicacion", "$plantas_dif_censadas_x_lote"] }
                        , 100]
                }
            }
        },


        {
            "$project": {
                "finca": 0,
                "capture": 0,
                "bloque": 0,
                "linea": 0,
                "arbol": 0

            }
        }



    ]

)