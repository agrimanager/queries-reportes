db.form_censodepolinizacion.aggregate(

    [
        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                //"finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "finca": { "$toObjectId": "$Point.farm" },

                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                //"localField": "finca._id",
                "localField": "finca",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        //===== FECHAS
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate", "timezone": "America/Bogota" } },
                "num_dia_anio": { "$dayOfYear": { "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }




        //===== CALCULOS
        , {
            "$group": {
                "_id": {
                    "num_anio": "$num_anio",
                    "num_dia_anio": "$num_dia_anio",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"

                }
                // , "data": { "$push": "$$ROOT" }
                , "aplicacion1": { "$sum": { "$toDouble": "$Aplicacion 1" } }
                , "aplicacion2": { "$sum": { "$toDouble": "$Aplicacion 2" } }

            }
        }



        //===== REGLAS DE NEGOCIO
        , {
            "$match": {
                "$or": [
                    { "aplicacion1": { "$ne": 0 } },
                    { "aplicacion2": { "$ne": 0 } }
                ]
            }
        }

        , {
            "$sort": {
                "_id.arbol": 1,
                "_id.num_anio": 1,
                "_id.num_dia_anio": 1
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "aplicacion1": "$aplicacion1"
                            , "aplicacion2": "$aplicacion2"
                        }
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$sort": {
                "_id.arbol": 1
            }
        }

        //---filtrar datos con p1
        , {
            "$addFields": {
                "data_p1": {
                    "$filter": {
                        "input": "$data",
                        "as": "censo",
                        "cond": {
                            "$ne": ["$$censo.aplicacion1", 0]
                        }
                    }
                }
            }
        }


        //===== REGLAS DE NEGOCIO
        , {
            "$match": {
                "data_p1": { "$ne": [] }
            }
        }


        //-------------------------------------condicion de datos
        , {
            "$addFields": {
                "data_map": {
                    "$map": {
                        "input": "$data_p1",
                        "as": "item_i",
                        "in": {
                            "$map": {
                                "input": "$data",
                                "as": "item_j",
                                "in": {
                                    "$cond": {
                                        //"if": { "$gt": ["$$item_j.num_dia_anio", "$$item_i.num_dia_anio"] },
                                        "if": {
                                            "$and": [
                                                { "$gt": ["$$item_j.num_dia_anio", "$$item_i.num_dia_anio"] },
                                                { "$lte": [{ "$subtract": ["$$item_j.num_dia_anio", "$$item_i.num_dia_anio"] }, 12] },
                                                { "$gte": ["$$item_j.aplicacion2", 1] },
                                            ]
                                        },
                                        "then": "si",
                                        "else": "no"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        //----concatenar arrays
        , {
            "$addFields": {
                "data_map2": {
                    "$reduce": {
                        "input": "$data_map",
                        "initialValue": [],
                        "in": {
                            "$concatArrays": ["$$value", "$$this"]
                        }
                    }
                }
            }
        }



        //-------------------------------------sacar datos
        , {
            "$addFields": {
                "data_map3": {
                    "$filter": {
                        "input": "$data_map2",
                        "as": "item",
                        "cond": {
                            "$not": {
                                "$in": [
                                    "$$item",
                                    ["no"]
                                ]
                            }
                        }
                    }
                }
            }
        }


        //===== REGLAS DE NEGOCIO
        , {
            "$match": {
                "data_map3": { "$ne": [] }
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": "$_id"
            }
        }


    ], { allowDiskUse: true }

)