

//---update1 (Censo)
//------------------------------------------------------------
// db.form_campo.aggregate(
//     {
//         $group: { _id: "$Censo", cant: { $sum: 1 } }
//     }
// )


db.form_campo.updateMany(
    {
        Censo: "Re_Siembra"
    },
    {
        $set: {
            Censo: "Re_Siembra Con Cinta"
        }
    }

)


//---update2 (Pronostico Cosecha)
//------------------------------------------------------------

// db.form_campo.aggregate(
//     {
//         $group: { _id: "$Pronostico Cosecha", cant: { $sum: 1 } }
//     }
// )


db.form_campo.updateMany(
    {
        // "Pronostico Cosecha": "xxxxxx"
        // "Pronostico Cosecha": "Mas de 150 Frutos" //igual
        // "Pronostico Cosecha": "Menos de 5 kg"
        // "Pronostico Cosecha": "Mas de 50 Frutos"
        "Pronostico Cosecha": "Mas de 100 Frutos"

    },
    {
        $set: {
            // "Pronostico Cosecha": "xxxxxx"
            // "Pronostico Cosecha": "Mas de 150 Frutos" //igual
            // "Pronostico Cosecha": "1-5 Frutos"
            // "Pronostico Cosecha": "51-100 Frutos"
            "Pronostico Cosecha": "101-150 Frutos"

        }
    }

)
