[

        { "$addFields": { "variable_fecha": "$Ingreso Labor1" } }
        , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
        , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


        { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
        { "$match": { "anio_variable_fecha": { "$lt": 3000 } } }



        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$variable_fecha" } },
                "num_mes": { "$month": { "date": "$variable_fecha" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
                "num_semana": { "$week": { "date": "$variable_fecha" } }
            }
        }

        , {
            "$addFields": {

                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


            }
        }

        , {
            "$project": {
                "variable_fecha": 0
                , "type_variable_fecha": 0
                , "anio_variable_fecha": 0
            }
        }

        , {
            "$addFields": {
                "HORAS Labor1": {
                    "$cond": {
                        "if": { "$eq": ["$Labor1", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor1" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor1" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                },
                "HORAS Labor2": {
                    "$cond": {
                        "if": { "$eq": ["$Labor2", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor2" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor2" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                },
                "HORAS Labor3": {
                    "$cond": {
                        "if": { "$eq": ["$Labor3", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor3" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor3" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "TOTAL HORAS": {
                    "$sum": [
                        "$HORAS Labor1",
                        "$HORAS Labor2",
                        "$HORAS Labor3"
                    ]
                }
            }
        }




        , {
            "$addFields": {
                "Ingreso Labor1": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor1", "timezone": "America/Bogota" } },
                "Salida Labor1": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor1", "timezone": "America/Bogota" } },

                "Ingreso Labor2": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor2", "timezone": "America/Bogota" } },
                "Salida Labor2": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor2", "timezone": "America/Bogota" } },

                "Ingreso Labor3": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor3", "timezone": "America/Bogota" } },
                "Salida Labor3": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor3", "timezone": "America/Bogota" } }

            }
        }


        , {
            "$project": {
                "Formula": 0
                , "Lote": 0
                , "Point": 0
                , "uid": 0
                , "uDate": 0
            }
        }


        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados",
                    "unidad_tiempo": "$Fecha_Txt"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS DIA": "$sum_horas"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "HORAS EXTRAS DIA": {
                    "$subtract": ["$TOTAL HORAS DIA", 8]
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados"
                    , "unidad_tiempo1": "$num_anio"
                    , "unidad_tiempo2": "$num_semana"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS SEMANA": "$sum_horas"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "HORAS EXTRAS SEMANA": {
                    "$subtract": ["$TOTAL HORAS SEMANA", 56]
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados"
                    , "unidad_tiempo1": "$num_anio"
                    , "unidad_tiempo2": "$num_mes"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS MES": "$sum_horas"
                        }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "HORAS EXTRAS MES": {
                    "$subtract": ["$TOTAL HORAS MES", 240]
                }
            }
        }






    ]
