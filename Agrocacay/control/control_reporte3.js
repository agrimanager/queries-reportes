db.form_control.aggregate(
    [

        { "$addFields": { "variable_fecha": "$Ingreso Labor1" } }
        , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
        , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


        { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
        { "$match": { "anio_variable_fecha": { "$lt": 3000 } } }



        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$variable_fecha" } },
                "num_mes": { "$month": { "date": "$variable_fecha" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
                "num_semana": { "$week": { "date": "$variable_fecha" } }
            }
        }

        , {
            "$addFields": {

                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha", "timezone": "America/Bogota" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


            }
        }

        , {
            "$project": {
                "variable_fecha": 0
                , "type_variable_fecha": 0
                , "anio_variable_fecha": 0
            }
        }

        , {
            "$addFields": {
                "HORAS Labor1": {
                    "$cond": {
                        "if": { "$eq": ["$Labor1", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor1" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor1" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                },
                "HORAS Labor2": {
                    "$cond": {
                        "if": { "$eq": ["$Labor2", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor2" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor2" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                },
                "HORAS Labor3": {
                    "$cond": {
                        "if": { "$eq": ["$Labor3", ""] },
                        "then": 0,
                        "else": {
                            "$subtract": [
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Salida Labor3" }, 1000] }, 60] }, 60] },
                                { "$divide": [{ "$divide": [{ "$divide": [{ "$toDouble": "$Ingreso Labor3" }, 1000] }, 60] }, 60] }
                            ]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "TOTAL HORAS": {
                    "$sum": [
                        "$HORAS Labor1",
                        "$HORAS Labor2",
                        "$HORAS Labor3"
                    ]
                }
            }
        }




        , {
            "$addFields": {
                "Ingreso Labor1": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor1", "timezone": "America/Bogota" } },
                "Salida Labor1": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor1", "timezone": "America/Bogota" } },

                "Ingreso Labor2": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor2", "timezone": "America/Bogota" } },
                "Salida Labor2": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor2", "timezone": "America/Bogota" } },

                "Ingreso Labor3": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Ingreso Labor3", "timezone": "America/Bogota" } },
                "Salida Labor3": { "$dateToString": { "format": "%Y-%m-%d %H:%M", "date": "$Salida Labor3", "timezone": "America/Bogota" } }

            }
        }


        , {
            "$project": {
                "Formula": 0
                , "Lote": 0
                , "Point": 0
                , "uid": 0
                , "uDate": 0
            }
        }


        //----agrupaciones(dia,semana,mes)
        //---horas extras
        //La base será 8 horas/día, 56 horas/semana, 240 horas/mes.

        //dia
        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados",
                    "unidad_tiempo": "$Fecha_Txt"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS DIA": "$sum_horas"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "HORAS EXTRAS DIA": {
                    "$subtract": ["$TOTAL HORAS DIA", 8]
                }
            }
        }

        //semana
        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados"
                    , "unidad_tiempo1": "$num_anio"
                    , "unidad_tiempo2": "$num_semana"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS SEMANA": "$sum_horas"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "HORAS EXTRAS SEMANA": {
                    "$subtract": ["$TOTAL HORAS SEMANA", 56]
                }
            }
        }

        //mes
        , {
            "$group": {
                "_id": {
                    "empleado": "$Empleados"
                    , "unidad_tiempo1": "$num_anio"
                    , "unidad_tiempo2": "$num_mes"
                }
                , "sum_horas": { "$sum": "$TOTAL HORAS" }
                , "data": { "$push": "$$ROOT" }
            }

        }
        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "TOTAL HORAS MES": "$sum_horas"
                        }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "HORAS EXTRAS MES": {
                    "$subtract": ["$TOTAL HORAS MES", 240]
                }
            }
        }






    ]

)
    .sort({ _id: -1 })

/*
// collection: form_control
{
	"_id" : ObjectId("63cf2eda2eaa6f9edd9a58f2"),
	"Empleados" : "Robinson Stivens Rincon Espinel",
	"Formula" : "",
	"Lote" : {
		"type" : "selection",
		"features" : [ ],
		"path" : ""
	},
	"Labor1" : "AC_Fertilización Liquida_Alistamiento",
	"Lote Labor1" : "Matalarga General",
	"Ingreso Labor1" : ISODate("2023-01-21T05:30:00.000-05:00"),
	"Salida Labor1" : ISODate("2023-01-21T06:30:00.000-05:00"),
	"Tarea" : 0,
	"Consumo1" : 0,
	"Consumo Tractor" : 0,
	"ConsumoEquipo" : 0,
	"Consumo Vehiculo" : 0,
	"KM1" : 0,
	"Alimentacin" : 3,
	"Labor2" : "AC_Fertilización Liquida",
	"Lote Labor2" : "ML3",
	"Ingreso Labor2" : ISODate("2023-01-21T06:30:00.000-05:00"),
	"Salida Labor2" : ISODate("2023-01-21T13:00:00.000-05:00"),
	"Tarea2" : 0,
	"Consumo2" : 0,
	"Consumo Tractor2" : 0,
	"ConsumoEquipo2" : 0,
	"Consumo Vehiculo2" : 0,
	"KM2" : 0,
	"Labor3" : "AC_Fertilización Liquida_Alistamiento",
	"Lote Labor3" : "Matalarga General",
	"Ingreso Labor3" : ISODate("2023-01-21T13:00:00.000-05:00"),
	"Salida Labor3" : ISODate("2023-01-21T14:00:00.000-05:00"),
	"Tarea3" : 0,
	"Consumo3" : 0,
	"Consumo Tractor3" : 0,
	"Consumo Equipo3" : 0,
	"Consumo Vehiculo3" : 0,
	"KM3" : 0,
	"Point" : {
		"farm" : "6398f126ab09ea157b5f8328",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-71.875755,
				4.22461
			]
		}
	},
	"TRACTOR1" : "",
	"EQUIPO1" : "",
	"VEHICULO1" : "",
	"TRACTOR2" : "",
	"EQUIPOS2" : "",
	"VEHICULO2" : "",
	"TRACTOR3" : "",
	"EQUIPOS3" : "",
	"VEHICULO3" : "",
	"uid" : ObjectId("62e9463ddf914a4b01e13532"),
	"supervisor" : "Julian David Pareja Suarez",
	"rgDate" : ISODate("2023-01-23T20:01:21.000-05:00"),
	"uDate" : ISODate("2023-01-23T20:01:21.000-05:00"),
	"capture" : "M"
}
*/
