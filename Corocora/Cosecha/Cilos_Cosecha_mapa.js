//---MAPA
//---NOTA: reporte que sale del formulario de recoleccion de cosecha

//---ejemploDB: palmasdelsinu
db.form_modulodecosechapalmas.aggregate(
    [

        //=====TEST //❌borrar
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //-------------------


        //=====1)VARIABLES BASE //🚩editar
        //fecha
        { "$addFields": { "fecha": "$Fecha de recoleccion" } },

        //cartografia
        { "$addFields": { "variable_cartografia": "$Lotes" } },

        //otras
        { "$addFields": { "elemnq": "$_id" } },


        //=====2)CONDICIONALES BASE  //📐reglas
        //fecha
        { "$addFields": { "anio_filtro": { "$year": "$fecha" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },
        { "$sort": { "fecha": -1 } },

        //cartografia
        { "$match": { "variable_cartografia.path": { "$ne": "" } } },


        //=====3)OPERACIONES 
        //fecha

        //cartografia
        { "$unwind": "$variable_cartografia.features" }


        //=====4)AGRUPACION
        , {
            "$group": {
                "_id": {
                    "nombre_cartografia": "$variable_cartografia.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$addFields": { "data": { "$arrayElemAt": ["$data", 0] } } },

        {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        "$divide": [{ "$subtract": ["$_id.today", "$data.fecha"] }, 86400000]
                    }
                }
            }
        },

        //#Dias a Hoy: #FFFFFF,Ciclo cosecha de 0 a 9 dias: #008000,Ciclo cosecha de 10 a 12 dias: #FFFF00,Ciclo cosecha de 13 a 15 dias: #ff8000,Ciclo cosecha > 15 dias: #FF0000
        //#Dias a Hoy: #FFFFFF,Ciclo cosecha de 0 a 9 dias: #,Ciclo cosecha de 10 a 12 dias: #,Ciclo cosecha de 13 a 15 dias: #,Ciclo cosecha > 15 dias: #


        //=====COLOR
        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "#3f3b69",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "#008000",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                                , { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "#FFFF00",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                        , { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "#ff8000",
                                                "else": "#FF0000"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": { "$lt": ["$dias de ciclo", 0] },
                        "then": "error dias",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "A-[0 a 9] dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }
                                                , { "$lte": ["$dias de ciclo", 12] }]
                                        },
                                        "then": "B-[10 a 12] dias",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                                        , { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "C-[13 a 15] dias",
                                                "else": "D-[>15] dias"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        },

        //=====PROYECCION FINAL
        {
            "$project": {
                "_id": "$data.elemnq",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_cartografia",
                    "Rango": "$rango",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": "$data.variable_cartografia.features.geometry"
            }
        }
    ], { "allowDiskUse": true })