db.form_modulodecosechapalmas.aggregate(
    [


        //=====CONDICIONALES BASE

        //----porque la estructura de AG1 es diferente
        {
            "$match": {
                "capture": { "$ne": "AG1" }
            }
        },

        {
            "$match": {
                "Lotes.path": { "$ne": "" }
            }
        },


        { "$addFields": { "fecha": "$Fecha de recoleccion" } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },

        {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" } },

                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        },




        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Lotes" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Lotes": 0
                , "Point": 0
                , "Formula": 0
            }
        }


        // //=====CRUCE1.1
        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "localField": "lote",
                "foreignField": "Lotes.features.properties.name",
                "as": "info_lote1"
            }
        }
        , {
            "$unwind": {
                "path": "$info_lote1",
                "preserveNullAndEmptyArrays": true
            }
        }

        //--sacar variables
        , {
            "$addFields": {
                "Palmas Iniciales": { "$ifNull": ["$info_lote1.Palmas Iniciales", 0] }
                , "Palmas erradicadas": { "$ifNull": ["$info_lote1.Palmas erradicadas", 0] }
                , "Palmas Productivas": { "$ifNull": ["$info_lote1.Palmas Productivas", 0] }
                , "Hectareas Iniciales": { "$ifNull": ["$info_lote1.Hectareas Iniciales", 0] }
                , "Hectareas Productivas": { "$ifNull": ["$info_lote1.Hectareas Productivas", 0] }
                , "Material": { "$ifNull": ["$info_lote1.Material", ""] }
                , "Año de siembra": { "$ifNull": ["$info_lote1.Año de siembra", "0"] }
            }
        }

        , {
            "$project": {
                "info_lote1": 0
            }
        }



        //=====CRUCE1.2
        //-----info_lote
        , {
            "$lookup": {
                "from": "form_formulariopuenteparacosecha",
                "as": "info_lote2",
                "let": {
                    "nombre_lote": "$lote"
                    , "anio": "$anio"
                    , "mes": "$mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Año" }, "$$anio"] }
                                    , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote2",
                "preserveNullAndEmptyArrays": true
            }
        },

        //---sacar variables
        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }
                , "lote_tarifa_cosecha": { "$ifNull": ["$info_lote2.Tarifa de Cosecha", 0] }
            }
        }

        , {
            "$project": {
                "info_lote2": 0
            }
        }


        // //----racimos enviados
        , {
            "$addFields": {
                "num_racimos_enviados": {
                    "$ifNull": ["$Racimos (#)", 0]
                }
            }
        }

        , {
            "$addFields": {
                "peso_promedio_racimos_enviados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$lote_peso_promedio" }, 0] }
                    ]
                }
            }
        }




        //=====CRUCE2 (form_despachodefruta)
        //-----despacho
        , {
            "$lookup": {
                "from": "form_despachoviajesaplantaextractora",
                "as": "info_despacho",
                "let": {
                    "recoleccion_fecha": "$fecha"
                    , "recoleccion_vagon": "$Numero de Vagon"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //--fechas
                                    {
                                        "$gte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$recoleccion_fecha"
                                                    }
                                                }
                                            }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Inicio Releccion" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$recoleccion_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Fin de Recoleccion" } } }
                                        ]
                                    }


                                    //---vagon
                                    , { "$eq": [{ "$trim": { "input": { "$toString": "$Vagon" } } }, { "$trim": { "input": { "$toString": "$$recoleccion_vagon" } } }] }
                                ]
                            }
                        }
                    },


                    //--------------------DUDA1
                    // {
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // },
                    // {
                    //     "$limit": 1
                    // }
                ]
            }
        },
        {
            "$unwind": {

                "path": "$info_despacho",
                //--------------------DUDA
                //"preserveNullAndEmptyArrays": true
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {

                //----dato principal
                "despacho_peso_kg": { "$ifNull": ["$info_despacho.Peso Vagon Kg", 0] },
                "despacho_numTicket": { "$ifNull": ["$info_despacho.Tiquete", 0] },


                // //--info
                "despacho_numRemision": { "$ifNull": ["$info_despacho.Remision", 0] },
                "despacho_observaciones": { "$ifNull": ["$info_despacho.Observaciones", ""] },
                "despacho_despachador": { "$ifNull": ["$info_despacho.Despachador", ""] },
                "despacho_vehiculo": { "$ifNull": ["$info_despacho.Vehiculo Transportador", ""] },

            }
        }

        , {
            "$project": {
                "info_despacho": 0
            }
        }



        //----TEST
        , {
            $match: {
                "despacho_numTicket": 153680
            }
        }


        // //==========AGRUPACION DE RECOLECCION Y DESPACHO
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "recoleccion_vagon": "$Numero de Vagon"
                    , "ticket": "$despacho_numTicket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_lote_viaje": {
                    "$sum": { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] }
                }
                ,
                "total_peso_racimos_alzados_lote_viaje": {
                    "$sum": "$peso_promedio_racimos_enviados"
                }
            }
        },

        {
            "$group": {
                "_id": {
                    // "lote": "$lote",
                    "recoleccion_vagon": "$_id.recoleccion_vagon"
                    , "ticket": "$_id.ticket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_peso_racimos_alzados_viaje": {
                    "$sum": "$total_peso_racimos_alzados_lote_viaje"
                }

            }
        },
        {
            "$unwind": "$data"
        },

        {
            "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_racimos_alzados_lote_viaje"
            }
        },

        {
            "$unwind": "$data.data"
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                            "total_alzados_lote": "$total_alzados_lote"
                            , "ticket": "$_id.ticket"
                            , "pct_Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }

        // //--2decimales //----OJO NO SE PUEDE ACA POR DAÑA CALCULOS
        // , {
        //     "$addFields": {
        //         "pct_Alzados x lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_Alzados x lote", 100] }, { "$mod": [{ "$multiply": ["$pct_Alzados x lote", 100] }, 1] }] }, 100] }
        //     }
        // }

        , {
            "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$despacho_peso_kg" }, 0] }, "$pct_Alzados x lote"]
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL lote": {
                    "$cond": {
                        "if": { "$eq": ["$Total Alzados", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                        }
                    }
                }
            }
        }



        //---TEST
        , {
            "$addFields": {
                "Peso REAL lote2": {
                    "$multiply": ["$num_racimos_enviados", "$Peso REAL lote"]
                }
            }
        }
        
        
        
        // // //=====EMPLEADOS
        // , {
        //     "$addFields": {
        //         "array_info_empleados": "$Colaborador"
        //     }
        // }




        // // //-------------REGLAS DE NEGOCIO

        // // , {
        // //     "$addFields": {
        // //         "cargo": {
        // //             "$cond": {
        // //                 "if": { "$eq": ["$empleado_cargo", "Tractorista"] },
        // //                 "then": "Tractorista",
        // //                 "else": "Cosechero y/o Recolector"
        // //             }
        // //         }
        // //     }
        // // }



        // // , {
        // //     "$addFields": {
        // //         "peso_repartido": {
        // //             "$cond": {
        // //                 "if": { "$eq": ["$empleado_cargo", "Tractorista"] },
        // //                 "then": "$Peso REAL Alzados",
        // //                 "else": {
        // //                     "$cond": {
        // //                         "if": {
        // //                             "$eq": ["$cantidad_recolectores_y_cosecheros", 0]
        // //                         },
        // //                         "then": 0,
        // //                         "else": {
        // //                             "$divide": ["$Peso REAL Alzados", "$cantidad_recolectores_y_cosecheros"]
        // //                         }
        // //                     }
        // //                 }
        // //             }
        // //         }
        // //     }
        // // }



        // // //----DUDA
        // // //---CONDICION DE NO DESPACHO ?



        // // //---total dias trabajados

        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "anio": "$anio"
        // //             , "mes": "$mes"
        // //             , "mes_txt": "$Mes_Txt"
        // //             , "fecha_txt": "$Fecha_Txt"

        // //             , "empleado_nombre": "$empleado_nombre"
        // //             // , "cargo": "$cargo"

        // //         },
        // //         "data": {
        // //             "$push": "$$ROOT"
        // //         }
        // //         //, "dias_trabajados": {"$sum": 1}
        // //     }
        // // }

        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "anio": "$_id.anio"
        // //             , "mes": "$_id.mes"
        // //             , "mes_txt": "$_id.mes_mxt"
        // //             // , "fecha_txt": "$Fecha_Txt"

        // //             , "empleado_nombre": "$_id.empleado_nombre"
        // //             // , "cargo": "$cargo"

        // //         },
        // //         "data": {
        // //             "$push": "$$ROOT"
        // //         }
        // //         , "dias_trabajados": { "$sum": 1 }
        // //     }
        // // }

        // // , { "$unwind": "$data" }
        // // , { "$unwind": "$data.data" }


        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$data.data",
        // //                 {
        // //                     "dias_trabajados": "$dias_trabajados"
        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }




        // // //==========AGRUPACION
        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "anio": "$anio"
        // //             , "mes": "$mes"
        // //             , "mes_txt": "$Mes_Txt"

        // //             , "empleado_nombre": "$empleado_nombre"
        // //             , "cargo": "$cargo"

        // //         },
        // //         "data": {
        // //             "$push": "$$ROOT"
        // //         }
        // //         , "total_peso_x_empleado": {
        // //             "$sum": { "$ifNull": [{ "$toDouble": "$peso_repartido" }, 0] }
        // //         }
        // //         //---DUDA
        // //         , "total_racimos_x_empleado": {
        // //             "$sum": { "$ifNull": [{ "$toDouble": "$total_alzados_lote" }, 0] }
        // //         }
        // //         //---DUDA
        // //         , "total_dias_x_empleado": { "$min": "$dias_trabajados" }
        // //     }
        // // }

        // // //==========AGRUPACION
        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "anio": "$_id.anio"
        // //             , "mes": "$_id.mes"
        // //             , "mes_txt": "$_id.mes_txt"

        // //             // , "empleado_nombre": "$_id.empleado_nombre"
        // //             // , "cargo": "$_id.cargo"

        // //         },
        // //         "data": {
        // //             "$push": "$$ROOT"
        // //         }
        // //         //---maximo no tractorista
        // //         , "maximo_peso": {
        // //             "$max": {
        // //                 "$switch": {
        // //                     "branches": [
        // //                         {
        // //                             "case": { "$eq": ["$_id.cargo", "Cosechero y/o Recolector"] },
        // //                             "then": "$total_peso_x_empleado"
        // //                         }
        // //                     ],
        // //                     "default": 0
        // //                 }
        // //             }
        // //         }
        // //     }
        // // }



        // // , { "$unwind": "$data" }
        // // // , { "$unwind": "$data.data" }


        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$data",
        // //                 {
        // //                     "maximo_peso": "$maximo_peso"
        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }


        // // //diferencia (peso_maximo - peso_repartido)
        // // , {
        // //     "$addFields": {
        // //         "diferencia_peso_maximo": {
        // //             "$cond": {
        // //                 "if": { "$eq": ["$_id.cargo", "Tractorista"] },
        // //                 "then": -1,
        // //                 "else": { "$subtract": ["$maximo_peso", "$total_peso_x_empleado"] }
        // //             }
        // //         }
        // //     }
        // // }




        // // //=====CRUCE
        // // //-----tarifas
        // // , {
        // //     "$lookup": {
        // //         "from": "form_configuraciondetarifascosecha",
        // //         "as": "info_tarifas",
        // //         "let": {
        // //             "diferencia_peso_maximo": "$diferencia_peso_maximo"

        // //             , "anio": "$_id.anio"
        // //             , "mes_txt": "$_id.mes_txt" //texto
        // //         },
        // //         "pipeline": [
        // //             {
        // //                 "$match": {
        // //                     "$expr": {
        // //                         "$and": [
        // //                             { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] },
        // //                             { "$eq": ["$Mes", "$$mes_txt"] }//texto

        // //                             , { "$gte": ["$$diferencia_peso_maximo", { "$toDouble": "$Kilogramos Desde" }] }
        // //                             , { "$lte": ["$$diferencia_peso_maximo", { "$toDouble": "$Kilogramos Hasta" }] }
        // //                         ]
        // //                     }
        // //                 }
        // //             },
        // //             // {
        // //             //     "$sort": {
        // //             //         "rgDate": -1
        // //             //     }
        // //             // },
        // //             {
        // //                 "$limit": 1
        // //             }
        // //         ]
        // //     }
        // // },
        // // {
        // //     "$unwind": {
        // //         "path": "$info_tarifas",
        // //         "preserveNullAndEmptyArrays": true
        // //     }
        // // },
        // // {
        // //     "$addFields": {
        // //         "tarifa_ton": { "$ifNull": ["$info_tarifas.Tarifa Tonelada", 0] }
        // //     }
        // // },
        // // {
        // //     "$addFields": {
        // //         "tarifa_kg": { "$divide": ["$tarifa_ton", 1000] }
        // //     }
        // // }

        // // , {
        // //     "$project": {
        // //         "info_tarifas": 0
        // //     }
        // // }



        // // //---pago total
        // // , {
        // //     "$addFields": {
        // //         "valor_total_pago_empleado": { "$multiply": ["$total_peso_x_empleado", "$tarifa_kg"] }
        // //     }
        // // }



        // // //----proyeccion final

        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$_id",
        // //                 {
        // //                     "total_peso_x_empleado": "$total_peso_x_empleado",
        // //                     "total_racimos_x_empleado": "$total_racimos_x_empleado",
        // //                     "total_dias_x_empleado": "$total_dias_x_empleado",
        // //                     "maximo_peso": "$maximo_peso",
        // //                     "diferencia_peso_maximo": "$diferencia_peso_maximo",
        // //                     "tarifa_ton": "$tarifa_ton",
        // //                     "tarifa_kg": "$tarifa_kg",
        // //                     "valor_total_pago_empleado": "$valor_total_pago_empleado"

        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }

    ]

)