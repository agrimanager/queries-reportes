// //----PROCESO1
// var data = db.form_modulodecosechapalmas.aggregate(
//     [


//         //-----VARIBLES IYECTADAS
//         {
//             $addFields: {
//                 "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
//                 "Busqueda fin": new Date,
//                 "today": new Date,
//                 "idform": "123",

//                 // "Finca nombre": "Caicesa-Ponce-Pizzati",
//                 // "FincaID": "5fbe5f972033e75d57a736e6",
//                 // "FincaID": ObjectId("5fbe5f972033e75d57a736e6"),


//                 // "Finca nombre": "San Alejo",
//                 // "FincaID": "5fac01ce246347247f068528",
//                 "FincaID": ObjectId("5efcfed215ae9c757d998473"),

//             }
//         },
//         //---------------------





//         //----new
//         //no mostrar datos de form
//         { "$limit": 1 },

//         {
//             "$project": {
//                 "datos": [
//                     {
//                         "FincaID": "$FincaID"
//                     }
//                 ]
//             }
//         }

//         , { "$unwind": "$datos" }
//         , { "$replaceRoot": { "newRoot": "$datos" } }

//         , {
//             "$lookup": {
//                 "from": "cartography",
//                 "as": "data_cartography",
//                 "let": {
//                     // //🔓 NO EDITAR
//                     "finca_id": "$FincaID"
//                 },
//                 //query
//                 "pipeline": [

//                     //----varibale custom
//                     {
//                         "$addFields": {
//                             "variable_custom": "$properties.custom.Erradicada" //🚩editar
//                         }
//                     },

//                     //condicion custom
//                     {
//                         "$match": {
//                             "variable_custom": { "$exists": true }
//                             , "variable_custom.value": { "$ne": false } // todos lo que no es falso se interpreta como verdadero
//                         }
//                     }



//                     //----cartografia
//                     , {
//                         "$addFields": {
//                             "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
//                         }
//                     },

//                     {
//                         "$addFields": {
//                             "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
//                         }
//                     },

//                     {
//                         "$addFields": {
//                             "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
//                         }
//                     },

//                     {
//                         "$addFields": {
//                             "split_path_oid": {
//                                 "$concatArrays": [
//                                     "$split_path_padres_oid",
//                                     "$variable_cartografia_oid"
//                                 ]
//                             }
//                         }
//                     },






//                     //----🔓CONDICION DE CRUCE lookup
//                     //---finca_id_strng
//                     {
//                         "$addFields": {
//                             "finca_id_strng": {
//                                 //---sacar el primero del array split_path_padres
//                                 "$arrayElemAt": ["$split_path_padres", 0]
//                             }
//                         }
//                     },
//                     {
//                         "$match": {
//                             "$expr": {
//                                 "$and": [
//                                     {
//                                         "$eq": [
//                                             { "$toString": "$$finca_id" }
//                                             , { "$toString": "$finca_id_strng" }
//                                         ]
//                                     }
//                                 ]
//                             }
//                         }
//                     },



//                     {
//                         "$lookup": {
//                             "from": "cartography",
//                             "localField": "split_path_oid",
//                             "foreignField": "_id",
//                             "as": "objetos_del_cultivo"
//                         }
//                     },

//                     {
//                         "$addFields": {
//                             "tiene_variable_cartografia": {
//                                 "$cond": {
//                                     "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
//                                     "then": "si",
//                                     "else": "no"
//                                 }
//                             }
//                         }
//                     },

//                     {
//                         "$addFields": {
//                             "objetos_del_cultivo": {
//                                 "$cond": {
//                                     "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
//                                     "then": "$objetos_del_cultivo",
//                                     "else": {
//                                         "$concatArrays": [
//                                             "$objetos_del_cultivo",
//                                             ["$variable_cartografia.features"]
//                                         ]
//                                     }
//                                 }
//                             }
//                         }
//                     },



//                     {
//                         "$addFields": {
//                             "finca": {
//                                 "$filter": {
//                                     "input": "$objetos_del_cultivo",
//                                     "as": "item_cartografia",
//                                     "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
//                                 }
//                             }
//                         }
//                     },
//                     {
//                         "$unwind": {
//                             "path": "$finca",
//                             "preserveNullAndEmptyArrays": true
//                         }
//                     },

//                     {
//                         "$lookup": {
//                             "from": "farms",
//                             "localField": "finca._id",
//                             "foreignField": "_id",
//                             "as": "finca"
//                         }
//                     },
//                     { "$unwind": "$finca" },

//                     { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

//                     {
//                         "$addFields": {
//                             "bloque": {
//                                 "$filter": {
//                                     "input": "$objetos_del_cultivo",
//                                     "as": "item_cartografia",
//                                     "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
//                                 }
//                             }
//                         }
//                     },
//                     {
//                         "$unwind": {
//                             "path": "$bloque",
//                             "preserveNullAndEmptyArrays": true
//                         }
//                     },

//                     { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

//                     {
//                         "$addFields": {
//                             "lote": {
//                                 "$filter": {
//                                     "input": "$objetos_del_cultivo",
//                                     "as": "item_cartografia",
//                                     "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
//                                 }
//                             }
//                         }
//                     },
//                     {
//                         "$unwind": {
//                             "path": "$lote",
//                             "preserveNullAndEmptyArrays": true
//                         }
//                     },

//                     { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

//                     {
//                         "$project": {
//                             "variable_cartografia": 0,
//                             "split_path_padres": 0,
//                             "split_path_padres_oid": 0,
//                             "variable_cartografia_oid": 0,
//                             "split_path_oid": 0,
//                             "objetos_del_cultivo": 0,
//                             "tiene_variable_cartografia": 0
//                         }
//                     }


//                 ]
//             }
//         }

//         //obtener cartography
//         , {
//             "$unwind": {
//                 "path": "$data_cartography",
//                 "preserveNullAndEmptyArrays": true
//             }
//         }



//         // //---color
//         // , {
//         //     "$addFields": {
//         //         "color": "#FF00BF" //azul
//         //     }
//         // }



//         // // ----proyeccion final de mapa
//         // , {
//         //     "$project": {
//         //         "_id": "$_id.elemnq",
//         //         "idform": "$_id.idform",

//         //         "type": "Feature",
//         //         "properties": {
//         //             "Finca": "$data_cartography.finca",
//         //             "Bloque": "$data_cartography.bloque",
//         //             "Lote": "$data_cartography.lote",
//         //             // "Palmas": "$data_cartography.properties.name",
//         //             "Palmas": {"$ifNull": [ "$data_cartography.name", "SIN DATOS" ]},
//         //             "color": "$color"
//         //         },
//         //         // "geometry": "$data_cartography.geometry"
//         //         "geometry": {"$ifNull": [ "$data_cartography.geometry", {} ]}
//         //     }
//         // }



//         // ----proyeccion final de reporte
//         , {
//             "$project": {
//                 "Finca": "$data_cartography.finca",
//                 "Bloque": "$data_cartography.bloque",
//                 "Lote": "$data_cartography.lote",
//                 "Palmas": { "$ifNull": ["$data_cartography.properties.name", "SIN DATOS"] }
//             }
//         }


//         ,{
//             $group:{
//                 _id:{
//                     "finca":"$Finca",
//                     "bloque":"$Bloque",
//                     "lote":"$Lote"
//                 }
//                 ,palmas_erradicadas:{$sum:1}
//             }
//         }



//     ], { allowDiskUse: true }
// )


// // data


// data.forEach(i=>{

//     db.form_inventariodepalmasiniciales.update(
//     {
//         "Lotes.features.properties.name":{$in:[i._id.lote]}
//     },
//     {
//         $set:{
//             "Palmas erradicadas":i.palmas_erradicadas
//         }
//     }
//     )

// })






//----PROCESO2

// 	"Palmas Iniciales" : 964,
// 	"Palmas erradicadas" : 2,
// 	--"Palmas Productivas" : 964,
// 	"Hectareas Iniciales" : 6.56,
// 	--"Hectareas Productivas" : 6.56,
// 	"Material" : "CIRAD 7001",
// 	"Año de siembra" : "2015",

var data2 = db.form_inventariodepalmasiniciales.aggregate(

    {
        $addFields: {
            //palmas_iniciales_menos_erradicadas
            palmas_productivas: {
                $subtract: ["$Palmas Iniciales", "$Palmas erradicadas"]
            }
        }
    }

)

// data2

data2.forEach(item => {
    db.form_inventariodepalmasiniciales.update(
        {
            "_id": item._id
        },
        {
            $set: {
                "Palmas Productivas": item.palmas_productivas
            }
        }
    )
})
