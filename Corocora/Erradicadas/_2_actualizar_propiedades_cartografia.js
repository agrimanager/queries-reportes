var data = db.form_inventariodepalmasiniciales.aggregate(

    { $unwind: "$Lotes.features" }

    , {
        $addFields: {
            lote_oid: { $toObjectId: "$Lotes.features._id" }

            , palmas_erradicadas: "$Palmas erradicadas"
            , palmas_productivas: "$Palmas Productivas"
        }
    }

)


// data


data.forEach(i => {
    db.cartography.update(
        {
            _id: i.lote_oid
        },
        {
            $set: {
                "properties.custom.Numero de plantas erradicadas.value":i.palmas_erradicadas,
                "properties.custom.Numero de plantas.value":i.palmas_productivas

            }
        }
    )
})

// 	"Numero de plantas" : {
// 		"type" : "number",
// 		"value" : NumberInt(3332)
// 	},
// 	"Numero de plantas erradicadas" : {
// 		"type" : "number",
// 		"value" : NumberInt(23)
// 	}
