db.form_modulodeproduccionpalmas.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"


                , "Plantas_inactivas": {
                    "$cond": {
                        "if": {
                            "$and": [

                                { "$eq": [{ "$toDouble": "$Racimos verdes" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos pintones" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos maduros" }, 0] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }

                }
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lotes.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lotes.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        }



        , {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales"

            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }




        //---plantas dif x lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"

                    , "num_palmas": "$num_palmas"


                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"


                    , "num_palmas": "$_id.num_palmas"

                }
                , "data": { "$push": "$$ROOT" }

                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
            }
        }




        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }





        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                },
                "data": {
                    "$push": "$$ROOT"
                }


                , "sum_Racimos_verdes": { "$sum": { "$toDouble": "$Racimos verdes" } }
                , "sum_Racimos_pintones": { "$sum": { "$toDouble": "$Racimos pintones" } }


                , "sum_Racimos_maduros": { "$sum": { "$toDouble": "$Racimos maduros" } }


                , "total_Plantas_inactivas": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Plantas_inactivas", true] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }


            }
        }






        , {
            "$addFields": {
                "sum_total_racimos": {
                    "$sum": ["$sum_Racimos verdes", "$sum_Racimos pintones", "$sum_Racimos maduros"]
                }
            }
        }



        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {

                            "total_Plantas_inactivas": "$total_Plantas_inactivas"



                            , "sum_Racimos_verdes": "$sum_racimos_verdes"
                            , "sum_Racimos_pintones": "$sum_Racimos_pintones"
                            , "sum_racimos_inmaduros": "$sum_racimos_inmaduros"
                            , "sum_total_racimos": "$sum_Racimos_maduros"


                        }
                    ]
                }
            }
        }

        //---inidicador1
        , {
            "$addFields": {
                "pct_palmas_muestradas": {
                    "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote", "$num_palmas"] }, 100]
                }
            }
        }

        //---inidicador2
        , {
            "$addFields": {
                "pct_plantas_inactivas": {
                    "$multiply": [{ "$divide": ["$total_Plantas_inactivas", "$plantas_dif_censadas_x_lote"] }, 100]
                }
            }
        }


        //---inidicador3
        , {
            "$addFields": {
                "num_plantas_inactivas_esperado": {
                    "$divide": [{ "$multiply": ["$pct_plantas_inactivas", "$num_palmas"] }, 100]
                }
            }
        }


        //---inidicador4
        , {
            "$addFields": {
                "num_plantas_activas_esperado": {
                    "$divide": [{ "$multiply": [{ "$subtract": [100, "$pct_plantas_inactivas"] }, "$num_palmas"] }, 100]
                }
            }
        }


        //---inidicador5
        //----MESES
        //---fechas

        , { "$addFields": { "fecha": "$rgDate" } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },

        {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" } },

                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        //--array meses
        , {
            "$addFields": {
                "array_meses": [
                    //---cuatrimestre1
                    {
                        "num_mes": 1,
                        "pct_mes": 11,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 2,
                        "pct_mes": 21,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 3,
                        "pct_mes": 35,
                        "cuatrimestre": 1
                    },
                    {
                        "num_mes": 4,
                        "pct_mes": 33,
                        "cuatrimestre": 1
                    },

                    //---cuatrimestre2
                    {
                        "num_mes": 5,
                        "pct_mes": 27,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 6,
                        "pct_mes": 26,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 7,
                        "pct_mes": 27,
                        "cuatrimestre": 2
                    },
                    {
                        "num_mes": 8,
                        "pct_mes": 21,
                        "cuatrimestre": 2
                    },

                    //---cuatrimestre3
                    {
                        "num_mes": 9,
                        "pct_mes": 31,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 10,
                        "pct_mes": 34,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 11,
                        "pct_mes": 22,
                        "cuatrimestre": 3
                    },
                    {
                        "num_mes": 12,
                        "pct_mes": 13,
                        "cuatrimestre": 3
                    },

                ]
            }
        }





        //---test
        // ,{
        //     $match:{
        //         Mes_Txt:"Noviembre"
        //     }
        // }


        // //---dato1

        // //=====CRUCE PP
        // , {
        //     "$lookup": {
        //         "from": "form_formulariopuenteparacosecha",
        //         "as": "info_lote2",
        //         "let": {
        //             "nombre_lote": "$lote"
        //             , "anio": "$anio"
        //             , "mes": "$mes"
        //         },
        //         "pipeline": [
        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
        //                             { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

        //                             , { "$eq": [{ "$toDouble": "$Año" }, "$$anio"] }
        //                             // , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }
        //                         ]
        //                     }
        //                 }
        //             },
        //             {
        //                 "$sort": {
        //                     "rgDate": -1
        //                 }
        //             },
        //             {
        //                 "$limit": 1
        //             }
        //         ]
        //     }
        // },
        // {
        //     "$unwind": {
        //         "path": "$info_lote2",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },

        // //---sacar variables
        // {
        //     "$addFields": {
        //         "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }
        //     }
        // }

        // , {
        //     "$project": {
        //         "info_lote2": 0
        //     }
        // }





        // //--dato2
        // , {
        //     "$addFields": {
        //         "num_plantas_inactivas_esperado": {
        //             "$sum": ["$"]
        //         }
        //     }
        // }



    ]

)