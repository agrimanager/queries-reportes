db.form_modulodeproduccionpalmas.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"


                , "Plantas_inactivas": {
                    "$cond": {
                        "if": {
                            "$and": [

                                { "$eq": [{ "$toDouble": "$Racimos verdes" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos pintones" }, 0] },
                                { "$eq": [{ "$toDouble": "$Racimos maduros" }, 0] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }

                }
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }





        , {
            "$lookup": {
                "from": "form_inventariodepalmasiniciales",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lotes.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lotes.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        }



        , {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales"

            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }




        //---plantas dif x lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"

                    , "num_palmas": "$num_palmas"


                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"


                    , "num_palmas": "$_id.num_palmas"

                }
                , "data": { "$push": "$$ROOT" }

                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
            }
        }




        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }





        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                },
                "data": {
                    "$push": "$$ROOT"
                }


                , "sum_Racimos_verdes": { "$sum": { "$toDouble": "$Racimos verdes" } }
                , "sum_Racimos_pintones": { "$sum": { "$toDouble": "$Racimos pintones" } }


                , "sum_Racimos_maduros": { "$sum": { "$toDouble": "$Racimos maduros" } }


                , "total_Plantas_inactivas": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Plantas_inactivas", true] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }


            }
        }






        , {
            "$addFields": {
                "sum_total_racimos": {
                    "$sum": ["$sum_Racimos verdes", "$sum_Racimos pintones", "$sum_Racimos maduros"]
                }
            }
        }



        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {

                            "total_Plantas_inactivas": "$total_Plantas_inactivas"



                            , "sum_Racimos_verdes": "$sum_racimos_verdes"
                            , "sum_Racimos_pintones": "$sum_Racimos_pintones"
                            , "sum_racimos_inmaduros": "$sum_racimos_inmaduros"
                            , "sum_total_racimos": "$sum_Racimos_maduros"


                        }
                    ]
                }
            }
        }

        //---inidicador1
        , {
            "$addFields": {
                "pct_palmas_muestradas": {
                    "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote", "$num_palmas"] }, 100]
                }
            }
        }

        //---inidicador2
        , {
            "$addFields": {
                "pct_plantas_inactivas": {
                    "$multiply": [{ "$divide": ["$total_Plantas_inactivas", "$plantas_dif_censadas_x_lote"] }, 100]
                }
            }
        }


        //---inidicador3
        , {
            "$addFields": {
                "num_plantas_inactivas_esperado": {
                    "$divide": [{ "$multiply": ["$pct_plantas_inactivas", "$num_palmas"] }, 100]
                }
            }
        }


        //---inidicador4
        , {
            "$addFields": {
                "num_plantas_activas_esperado": {
                    "$divide": [{ "$multiply": [{ "$subtract": [100, "$pct_plantas_inactivas"] }, "$num_palmas"] }, 100]
                }
            }
        }



    ]

)