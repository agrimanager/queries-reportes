db.form_recolecciondecosecha.aggregate(
    [
        //=====CONDICIONALES BASE

        {
            "$match": {
                "Lote.path": { "$ne": "" }
            }
        },

        { "$match": { "Colaborador": { "$exists": true } } },
        { "$match": { "Colaborador": { "$ne": "" } } },
        { "$addFields": { "empleados_filtro": { "$size": "$Colaborador" } } },
        { "$match": { "empleados_filtro": { "$gt": 0 } } },

        { "$addFields": { "fecha": "$Fecha Salida Plantacion" } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },




        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Lote" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Lote": 0
                , "Point": 0
            }
        }


        //=====EMPLEADOS
        , { "$unwind": "$Colaborador" }

        , {
            "$addFields": {
                "empleado_num_racimos": { "$toDouble": "$Colaborador.value" }

                , "empleado_cargo": "$Colaborador.reference"

                , "empleado_oid": { "$toObjectId": "$Colaborador._id" }
            }
        }

        //---info_empleado
        , {
            "$lookup": {
                "from": "employees",
                "localField": "empleado_oid",
                "foreignField": "_id",
                "as": "info_empleado"
            }
        }
        , { "$unwind": "$info_empleado" }

        , {
            "$addFields": {
                "empleado_nombre": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                "empleado_identificacion": "$info_empleado.numberID"
            }
        }




        //---info_empleado_empresa
        , {
            "$lookup": {
                "from": "companies",
                "localField": "info_empleado.cid",
                "foreignField": "_id",
                "as": "info_empleado_empresa"
            }
        }
        , { "$unwind": "$info_empleado_empresa" }

        , {
            "$addFields": {
                "empleado_empresa": "$info_empleado_empresa.name"
            }
        }


        , {
            "$project": {
                "info_empleado": 0,
                "info_empleado_empresa": 0

                , "empleado_oid": 0
                , "Colaborador": 0
            }
        }


        //=====CRUCE1
        //-----info_lote
        , {
            "$lookup": {
                "from": "form_configuraciondepesospromedio",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"

                    , "anio": "$anio"
                    //, "mes": "$mes" //texto
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }

                                    //, { "$eq": [{ "$toDouble": "$Mes de registro" }, "$$mes"] }

                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }


        //=====CRUCE2 (form_despachodefruta)
        //-----despacho
        , {
            "$lookup": {
                "from": "form_despacho",
                "as": "info_despacho",
                "let": {
                    "recoleccion_fecha": "$fecha"

                    , "recoleccion_placa": "$Placa Vehiculo"
                    , "recoleccion_viaje": "$Numero viaje"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //--fechas
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$recoleccion_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Salida Plantacion" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$recoleccion_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Entrega Extractora" } } }
                                        ]
                                    }


                                    //---vagon y viaje
                                    , { "$eq": ["$Placa Vehiculo", "$$recoleccion_placa"] }
                                    , { "$eq": ["$Numero de viaje", "$$recoleccion_viaje"] }
                                ]
                            }
                        }
                    },


                    //--------------------DUDA1
                    // {
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // },
                    // {
                    //     "$limit": 1
                    // }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_despacho",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {

                //----dato principal
                "despacho_peso_kg": { "$ifNull": ["$info_despacho.Peso Extractora", 0] },
                "despacho_numTicket": { "$ifNull": ["$info_despacho.Numero del ticket", 0] },

                // //--info
                // "despacho_extractora": { "$ifNull": ["$info_despacho.Extractora", "--sin despacho--"] },
                // "despacho_conductor": { "$ifNull": ["$info_despacho.Conductor", "--sin despacho--"] },
                // "despacho_numRemision": { "$ifNull": ["$info_despacho.Numero de remision", 0] },

                // //--fechas
                // "despacho_fecha_inicio_llenado": "$info_despacho.Fecha inicio llenado",
                // "despacho_fecha_fin_llenado": "$info_despacho.Fecha fin llenado",
                // "despacho_fecha_de_despacho": "$info_despacho.Fecha de despacho"
            }
        }

        , {
            "$project": {
                "info_despacho": 0
            }
        }


        , {
            "$addFields":
            {
                "Peso aproximado Alzados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$empleado_num_racimos" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$lote_peso_promedio" }, 0] }
                    ]
                }
            }
        }


        //==========AGRUPACION DE RECOLECCION Y DESPACHO
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "recoleccion_placa": "$Placa Vehiculo"
                    , "recoleccion_viaje": "$Numero viaje"
                    , "ticket": "$despacho_numTicket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_lote_viaje": {
                    "$sum": { "$ifNull": [{ "$toDouble": "$empleado_num_racimos" }, 0] }
                }
                ,
                "total_peso_racimos_alzados_lote_viaje": {
                    "$sum": "$Peso aproximado Alzados"
                }
            }
        },

        {
            "$group": {
                "_id": {
                    // "lote": "$lote",
                    "recoleccion_placa": "$_id.recoleccion_placa"
                    , "recoleccion_viaje": "$_id.recoleccion_viaje"
                    , "ticket": "$_id.ticket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_peso_racimos_alzados_viaje": {
                    "$sum": "$total_peso_racimos_alzados_lote_viaje"
                }

            }
        },
        {
            "$unwind": "$data"
        },

        {
            "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_racimos_alzados_lote_viaje"
            }
        },

        {
            "$unwind": "$data.data"
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                            "total_alzados_lote": "$total_alzados_lote"
                            , "ticket": "$_id.ticket"
                            , "pct_Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }

        //--2decimales
        , {
            "$addFields": {
                "pct_Alzados x lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_Alzados x lote", 100] }, { "$mod": [{ "$multiply": ["$pct_Alzados x lote", 100] }, 1] }] }, 100] }
            }
        }

        , {
            "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$despacho_peso_kg" }, 0] }, "$pct_Alzados x lote"]
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL lote": {
                    "$cond": {
                        "if": { "$eq": ["$Total Alzados", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                        }
                    }
                }
            }
        }


        //----Otros

        //---peso real empleado
        , {
            "$addFields":
            {
                "EMPLEADO_PESO_REAL_COSECHA": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$empleado_num_racimos" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Peso REAL lote" }, 0] }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "EMPLEADO_PESO_REAL_COSECHA": { "$divide": [{ "$subtract": [{ "$multiply": ["$EMPLEADO_PESO_REAL_COSECHA", 100] }, { "$mod": [{ "$multiply": ["$EMPLEADO_PESO_REAL_COSECHA", 100] }, 1] }] }, 100] }
            }
        }

        // //---$pago empleado



    ]

)