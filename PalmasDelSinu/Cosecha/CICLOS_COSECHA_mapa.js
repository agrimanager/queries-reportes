db.form_recolecciondecosecha.aggregate(
    [
        
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        { "$addFields": { "anio_filtro": { "$year": "$Fecha de corte" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        {
            "$sort": {
                "Fecha de corte": -1
            }
        },
        {
            "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }
        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        },

        {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        "$divide": [{ "$subtract": ["$_id.today", "$data.Fecha de corte"] }, 86400000]
                    }
                }
            }
        },
        
        //---colores
        // 1-12 días de color Verde
        // 13-20 días color Amarillo
        // 21- en adelante Color Rojo
        
        //#Dias a Hoy: #FFFFFF,Ciclo cosecha de 0 a 12 dias: #008000,Ciclo cosecha de 13 a 20 dias: #FFFF00,Ciclo cosecha >= 21 dias: #FF0000


        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {"$lt": ["$dias de ciclo", 0]},
                        "then": "#3f3b69",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 12] }]
                                },
                                "then": "#008000",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                            , { "$lte": ["$dias de ciclo", 20] }]
                                        },
                                        "then": "#FFFF00",
                                        "else": "#FF0000"
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {"$lt": ["$dias de ciclo", 0]},
                        "then": "error dias",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 12] }]
                                },
                                "then": "A-[0 a 12] dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 13] }
                                            , { "$lte": ["$dias de ciclo", 20] }]
                                        },
                                        "then": "B-[13 a 20] dias",
                                        "else": "C-[>=21] dias"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            "$project": {
                "_id": "$data.elemnq",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_lote",
                    "Rango": "$rango",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": "$data.Cartography.features.geometry"
            }
        }
    ]

)