[
    { "$limit": 1 },


    {
        "$lookup": {
            "from": "cartography",
            "as": "data1",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [


                {
                    "$match": {
                        "properties.type": "lines"
                    }
                }

                , {
                    "$addFields": {
                        "cantidad_arboles": {
                            "$ifNull": ["$properties.custom.Numero de plantas.value", 0]
                        },
                        "cantidad_erradicada": {
                            "$ifNull": ["$properties.custom.Numero de plantas erradicadas.value", 0]
                        },

                        "name": "$properties.name"
                    }
                }

                , {
                    "$project": {
                        "name": 1,
                        "cantidad_arboles": 1,
                        "cantidad_erradicada": 1,
                        "path": 1
                    }
                }



                , {
                    "$addFields": {
                        "split_path_oid": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": { "$map": { "input": "$split_path_oid", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                        "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                        "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                    }
                },


                {
                    "$addFields": {
                        "finca": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.type", "Farm"] },
                                "then": "$feature_1",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.type", "Farm"] },
                                        "then": "$feature_2",
                                        "else": "$feature_3"
                                    }
                                }
                            }
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },

                {
                    "$addFields": {
                        "finca": "$finca.name"
                    }
                },
                { "$unwind": "$finca" },


                {
                    "$addFields": {
                        "bloque": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                "then": "$feature_1.properties.name",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                        "then": "$feature_2.properties.name",
                                        "else": "$feature_3.properties.name"
                                    }
                                }
                            }
                        }
                    }
                },


                {
                    "$addFields": {
                        "lote": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                "then": "$feature_1.properties.name",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                        "then": "$feature_2.properties.name",
                                        "else": "$feature_3.properties.name"
                                    }
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "linea": "$name"
                    }
                }

                , {
                    "$project": {
                        "split_path": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "features_oid": 0,

                        "feature_1": 0,
                        "feature_2": 0,
                        "feature_3": 0,

                        "path": 0,
                        "name": 0,
                        "_id": 0
                    }
                }


                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }



            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data1"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } },

    {
        "$addFields": {
            "total_arboles_activos": {
                "$subtract": ["$cantidad_arboles", "$cantidad_erradicada"]
            }
        }
    },


    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
            },
            "cantidad_arboles_por_lote": { "$sum": "$cantidad_arboles" },
            "cantidad_erradicada_por_lote": { "$sum": "$cantidad_erradicada" },
            "total_arboles_activos_por_lote": { "$sum": "$total_arboles_activos" },
            "data": { "$push": "$$ROOT" }
        }
    },

    { "$unwind": "$data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_arboles_por_lote": "$cantidad_arboles_por_lote",
                        "cantidad_erradicada_por_lote": "$cantidad_erradicada_por_lote",
                        "total_arboles_activos_por_lote": "$total_arboles_activos_por_lote"
                    }
                ]
            }
        }
    }
]
