
//error con empleados que no llegan como objectId sino con string de cedula
//...por que el reporte de labores x empleado EXPLOTA
db.tasks.aggregate(


    ////----caso de prueba del error
    // {
    //     "$match": {
    //         // "cod": "PD8ZC"//empleados no es objectId, es string de cedula //====ERROR
    //         // "cod": "ID5ND"//empleados ""//====ERROR

    //         "cod": "RJ17KK"//empleados []
    //     }
    // },


    //---identificar labores con empleados malos
    { $unwind: "$employees" }

    , {
        $addFields: {
            cantidad_caracteres_empleado: { $strLenCP: "$employees" }
        }
    }

    , {
        $match: {
            cantidad_caracteres_empleado: { $ne: 24 }
        }
    }

    ,{
        $group: { _id: "$cod"}
    }

)
