//----🚩 (REPORTE SIN FORMULARIO) ---- SIN FILTRO DE FECHAS
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-10-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "form_registrodeproducciondiaria",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //---TIPO DE VARIABLE CARTOGRAFIA (object,string)
                    {
                        "$addFields": {
                            "tipo_variable_cartografia": { "$type": "$Tarea latex" }
                        }
                    }

                    , {
                        "$addFields": {
                            "valor_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": ["$tipo_variable_cartografia", "object"] },
                                    "then": "$Tarea latex.features.properties.name",
                                    "else": "$Tarea latex"
                                }
                            }
                        }
                    }
                    , { "$unwind": "$valor_variable_cartografia" }

                    , {
                        "$match": {
                            "valor_variable_cartografia": { "$ne": "" }
                        }
                    }



                    //---FINCA
                    , { "$addFields": { "finca_oid": { "$toObjectId": "$Point.farm" } } }
                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca_oid",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    }

                    , { "$unwind": "$Finca" }

                    , { "$addFields": { "Finca": "$Finca.name" } }

                    //----------CRUCE1
                    //----Cruzar con Puente (form_puentetareas)
                    , {
                        "$lookup": {
                            "from": "form_puentetareas",
                            "as": "info_cartografia",
                            "let": {
                                "finca": "$Finca",
                                "valor_variable_cartografia": "$valor_variable_cartografia"
                            },
                            "pipeline": [

                                //----filtro de finca
                                {
                                    "$match": {
                                        "$expr": {
                                            "$eq": ["$Tarea Finca", "$$finca"]
                                        }
                                    }
                                },


                                //---filtro cartografia
                                {
                                    "$match": {
                                        "$expr": {
                                            "$or": [
                                                { "$eq": ["$Tarea Cartografia", "$$valor_variable_cartografia"] }
                                                , { "$eq": ["$Tarea Codigo", "$$valor_variable_cartografia"] }
                                            ]
                                        }
                                    }
                                }

                                , {
                                    "$project": {
                                        "Tarea Cartografia": 1,
                                        "Tarea Codigo": 1,
                                        "Tarea Finca": 1
                                    }
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$info_cartografia",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , {
                        "$addFields": {
                            "Tarea Cartografia": "$info_cartografia.Tarea Cartografia",
                            "Tarea Codigo": "$info_cartografia.Tarea Codigo",
                            "Tarea Finca": "$info_cartografia.Tarea Finca"
                        }
                    }




                    , {
                        "$project": {
                            "tipo_variable_cartografia": 0,
                            "valor_variable_cartografia": 0,
                            "finca_oid": 0,
                            "info_cartografia": 0
                            , "Finca": 0

                        }
                    },



                    //----variables de cruces (tarea, año, mes)

                    //---variables de fechas
                    { "$addFields": { "anio": { "$year": "$Fecha y hora Inicio" } } },
                    { "$addFields": { "mes": { "$month": "$Fecha y hora Inicio" } } },
                    //---condiciones deechas
                    { "$match": { "anio": { "$gt": 2000 } } },
                    { "$match": { "anio": { "$lt": 3000 } } }

                    , {
                        "$addFields": {
                            "Mes_Txt": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                                        { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                                        { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                                        { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                                        { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                                        { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                                        { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                                        { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                                        { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                                        { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                                        { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                                        { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                                    ],
                                    "default": "Mes desconocido"
                                }
                            }
                        }
                    }



                    // //Agrupar
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Tarea Finca"

                                , "tarea_cartografia": "$Tarea Cartografia"
                                , "tarea_codigo": "$Tarea Codigo"

                                , "anio": "$anio"
                                , "num_mes": "$mes"
                                , "mes": "$Mes_Txt"
                            }
                            // , "data": { "$push": "$$ROOT" }

                            , "sum_kg_latex": { "$sum": { "$toDouble": "$Latex en Kg" } }
                            , "num_rayadas": { "$sum": 1 }
                        }
                    }






                    //----------CRUCE2
                    //----Cruzar con Puente (form_inventariodecultivo)
                    , {
                        "$lookup": {
                            "from": "form_inventariodecultivo",
                            "as": "info_inventario_cultivo",
                            "let": {
                                "tarea_cartografia": "$_id.tarea_cartografia"
                                , "anio": "$_id.anio"
                                , "mes": "$_id.mes"
                            },
                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                // { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                                { "$ne": [{ "$type": "$Tarea.features.properties.name" }, "missing"] },
                                                { "$in": ["$$tarea_cartografia", "$Tarea.features.properties.name"] },

                                                { "$eq": ["$Mes", "$$mes"] }

                                                //--año (OJO---tipos de variables)
                                                , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$info_inventario_cultivo",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "num_arboles": { "$ifNull": ["$info_inventario_cultivo.Numero de Arboles", 0] }
                        }
                    }

                    , {
                        "$project": {
                            "info_inventario_cultivo": 0
                        }
                    }




                    //----------CRUCE3
                    //----Cruzar con PPTO (form_informacionproyecciondeproduccion)
                    , {
                        "$lookup": {
                            "from": "form_informacionproyecciondeproduccion",
                            "as": "info_ppto",
                            "let": {
                                "tarea_cartografia": "$_id.tarea_cartografia"
                                , "anio": "$_id.anio"
                                , "mes": "$_id.mes"
                            },
                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                                , { "$eq": ["$Mes", "$$mes"] }

                                                //--año (OJO---tipos de variables)
                                                , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                                            ]
                                        }
                                    }
                                }

                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$info_ppto",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "ppto": { "$ifNull": ["$info_ppto.Produccion programada", 0] }
                        }
                    }

                    , {
                        "$project": {
                            "info_ppto": 0
                        }
                    }


                    //---mostrar valores
                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id",
                                    {
                                        "sum_kg_latex": "$sum_kg_latex",
                                        "num_rayadas": "$num_rayadas",
                                        "num_arboles": "$num_arboles",
                                        "ppto": "$ppto"
                                    }
                                ]
                            }
                        }
                    }


                    //---dato1
                    , {
                        "$addFields": {
                            "produccion_rayadas_x_kg": {
                                "$cond": {
                                    "if": { "$eq": ["$num_rayadas", 0] },
                                    "then": 0,
                                    "else": { "$divide": ["$sum_kg_latex", "$num_rayadas"] }
                                }
                            }
                        }
                    }
                    //----2 decimales
                    , {
                        "$addFields": {
                            "produccion_rayadas_x_kg": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, 1] }] }, 100] }
                        }
                    }


                    //---dato2
                    , {
                        "$addFields": {
                            "produccion_rayadas_x_kg_x_arbol": {
                                "$cond": {
                                    "if": { "$eq": ["$num_arboles", 0] },
                                    "then": 0,
                                    "else": { "$divide": ["$produccion_rayadas_x_kg", "$num_arboles"] }
                                }
                            }
                        }
                    }
                    //----2 decimales
                    , {
                        "$addFields": {
                            "produccion_rayadas_x_kg_x_arbol": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, 1] }] }, 100] }
                        }
                    }


                    //---dato3
                    , {
                        "$addFields": {
                            "pct_cumplimiento": {
                                "$cond": {
                                    "if": { "$eq": ["$ppto", 0] },
                                    "then": 0,
                                    "else": { "$multiply": [{ "$divide": ["$sum_kg_latex", "$ppto"] }, 100] }
                                }
                            }
                        }
                    }
                    //----2 decimales
                    , {
                        "$addFields": {
                            "pct_cumplimiento": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_cumplimiento", 100] }, { "$mod": [{ "$multiply": ["$pct_cumplimiento", 100] }, 1] }] }, 100] }
                        }
                    }



                    //-----Meses finales ordenados
                    , {
                        "$addFields": {
                            "Mes_Txt": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                                    ],
                                    "default": "Mes desconocido"
                                }
                            }
                        }
                    }




                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




    ]
)