//reporte
db.form_registrodeproducciondiaria.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-10-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        //=======================FECHAS

        //------FILTRO FECHAS OTRAS
        //---variables de fechas
        { "$addFields": { "variable_fecha": "$Fecha y hora Inicio" } },//----EDITAR
        { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
        { "$addFields": { "mes": { "$month": "$variable_fecha" } } },
        //---condiciones de fechas
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } }

        //---filtro de fechas
        , {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        }

        //mes
        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        },


        //===============CARTOGRAFIA


        // //---seleccionar solo registros con cartografia
        // ,{
        //     "$match":{
        //         "Tarea latex.type":"selection"
        //     }
        // }

        //=======================CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Tarea latex" //🚩editar
            }
        },
        // //---seleccionar solo registros con cartografia
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        //--TAREA
        {
            "$addFields": {
                "tarea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "samplingPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$tarea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "tarea": { "$ifNull": ["$tarea.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Tarea latex": 0
                , "Point": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0
            }
        }








        // //Agrupar
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",

                    "tarea_cartografia": "$tarea"
                    // , "tarea_codigo": "$Tarea Codigo"

                    , "anio": "$anio"
                    , "num_mes": "$mes"
                    , "mes": "$Mes_Txt"
                }
                // , "data": { "$push": "$$ROOT" }

                , "sum_kg_latex": { "$sum": { "$toDouble": "$Latex en Kg" } }
                , "num_rayadas": { "$sum": 1 }
            }
        }






        //----------CRUCE2
        //----Cruzar con Puente (form_inventariodecultivo)
        , {
            "$lookup": {
                "from": "form_inventariodecultivo",
                "as": "info_inventario_cultivo",
                "let": {
                    "tarea_cartografia": "$_id.tarea_cartografia"
                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    // { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                    { "$ne": [{ "$type": "$Tarea.features.properties.name" }, "missing"] },
                                    { "$in": ["$$tarea_cartografia", "$Tarea.features.properties.name"] },

                                    { "$eq": ["$Mes", "$$mes"] }

                                    //--año (OJO---tipos de variables)
                                    , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                                ]
                            }
                        }
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_inventario_cultivo",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "num_arboles": { "$ifNull": ["$info_inventario_cultivo.Numero de Arboles", 0] }
            }
        }

        , {
            "$project": {
                "info_inventario_cultivo": 0
            }
        }




        //----------CRUCE3
        //----Cruzar con PPTO (form_informacionproyecciondeproduccion)
        , {
            "$lookup": {
                "from": "form_informacionproyecciondeproduccion",
                "as": "info_ppto",
                "let": {
                    "tarea_cartografia": "$_id.tarea_cartografia"
                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                    , { "$eq": ["$Mes", "$$mes"] }

                                    //--año (OJO---tipos de variables)
                                    , { "$eq": [{ "$toDouble": "$Anno" }, "$$anio"] }
                                ]
                            }
                        }
                    }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_ppto",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "ppto": { "$ifNull": ["$info_ppto.Produccion programada", 0] }
            }
        }

        , {
            "$project": {
                "info_ppto": 0
            }
        }


        //---mostrar valores
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "sum_kg_latex": "$sum_kg_latex",
                            "num_rayadas": "$num_rayadas",
                            "num_arboles": "$num_arboles",
                            "ppto": "$ppto"
                        }
                    ]
                }
            }
        }


        //---dato1
        , {
            "$addFields": {
                "produccion_rayadas_x_kg": {
                    "$cond": {
                        "if": { "$eq": ["$num_rayadas", 0] },
                        "then": 0,
                        "else": { "$divide": ["$sum_kg_latex", "$num_rayadas"] }
                    }
                }
            }
        }
        //----2 decimales
        , {
            "$addFields": {
                "produccion_rayadas_x_kg": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg", 100] }, 1] }] }, 100] }
            }
        }


        //---dato2
        , {
            "$addFields": {
                "produccion_rayadas_x_kg_x_arbol": {
                    "$cond": {
                        "if": { "$eq": ["$num_arboles", 0] },
                        "then": 0,
                        "else": { "$divide": ["$produccion_rayadas_x_kg", "$num_arboles"] }
                    }
                }
            }
        }
        //----2 decimales
        , {
            "$addFields": {
                "produccion_rayadas_x_kg_x_arbol": { "$divide": [{ "$subtract": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, { "$mod": [{ "$multiply": ["$produccion_rayadas_x_kg_x_arbol", 100] }, 1] }] }, 100] }
            }
        }


        //---dato3
        , {
            "$addFields": {
                "pct_cumplimiento": {
                    "$cond": {
                        "if": { "$eq": ["$ppto", 0] },
                        "then": 0,
                        "else": { "$multiply": [{ "$divide": ["$sum_kg_latex", "$ppto"] }, 100] }
                    }
                }
            }
        }
        //----2 decimales
        , {
            "$addFields": {
                "pct_cumplimiento": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_cumplimiento", 100] }, { "$mod": [{ "$multiply": ["$pct_cumplimiento", 100] }, 1] }] }, 100] }
            }
        }



        //-----Meses finales ordenados
        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }





    ]
)
