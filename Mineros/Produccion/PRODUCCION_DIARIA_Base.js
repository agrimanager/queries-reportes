db.form_registrodeproducciondiaria.aggregate(
    [
        //---TIPO DE VARIABLE CARTOGRAFIA (object,string)
        {
            "$addFields": {
                "tipo_variable_cartografia": { "$type": "$Tarea latex" }
            }
        }

        , {
            "$addFields": {
                "valor_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_variable_cartografia", "object"] },
                        "then": "$Tarea latex.features.properties.name",
                        "else": "$Tarea latex"
                    }
                }
            }
        }
        , { "$unwind": "$valor_variable_cartografia" }

        , {
            "$match": {
                "valor_variable_cartografia": { "$ne": "" }
            }
        }



        //---FINCA
        , { "$addFields": { "finca_oid": { "$toObjectId": "$Point.farm" } } }
        , {
            "$lookup": {
                "from": "farms",
                "localField": "finca_oid",
                "foreignField": "_id",
                "as": "Finca"
            }
        }

        , { "$unwind": "$Finca" }

        , { "$addFields": { "Finca": "$Finca.name" } }

        //----------CRUCE1
        //----Cruzar con Puente (form_puentetareas)
        , {
            "$lookup": {
                "from": "form_puentetareas",
                "as": "info_cartografia",
                "let": {
                    "finca": "$Finca",
                    "valor_variable_cartografia": "$valor_variable_cartografia"
                },
                "pipeline": [

                    //----filtro de finca
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Tarea Finca", "$$finca"]
                            }
                        }
                    },


                    //---filtro cartografia
                    {
                        "$match": {
                            "$expr": {
                                "$or": [
                                    { "$eq": ["$Tarea Cartografia", "$$valor_variable_cartografia"] }
                                    , { "$eq": ["$Tarea Codigo", "$$valor_variable_cartografia"] }
                                ]
                            }
                        }
                    }

                    , {
                        "$project": {
                            "Tarea Cartografia": 1,
                            "Tarea Codigo": 1,
                            "Tarea Finca": 1
                        }
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "Tarea Cartografia": "$info_cartografia.Tarea Cartografia",
                "Tarea Codigo": "$info_cartografia.Tarea Codigo",
                "Tarea Finca": "$info_cartografia.Tarea Finca"
            }
        }




        , {
            "$project": {
                "tipo_variable_cartografia": 0,
                "valor_variable_cartografia": 0,
                "finca_oid": 0,
                "info_cartografia": 0
                , "Finca": 0

            }
        },



        //----variables de cruces (tarea, año, mes)

        //---variables de fechas
        { "$addFields": { "anio": { "$year": "$Fecha y hora Inicio" } } },
        { "$addFields": { "mes": { "$month": "$Fecha y hora Inicio" } } },
        //---condiciones deechas
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }






        //----------CRUCE2
        //----Cruzar con Puente (form_inventariodecultivo)
        //.....



        //----------CRUCE3
        //----Cruzar con PPTO (form_informacionproyecciondeproduccion)
        , {
            "$lookup": {
                "from": "form_informacionproyecciondeproduccion",
                "as": "info_ppto",
                "let": {
                    "tarea_cartografia": "$Tarea Cartografia"
                    , "anio": "$anio"
                    , "mes": "$Mes_Txt"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Tarea", "$$tarea_cartografia"] }
                                    , { "$eq": ["$Mes", "$$mes"] }

                                    //--año (OJO---tipos de variables)
                                ]
                            }
                        }
                    }

                    // , {
                    //     "$project": {
                    //         "Tarea Cartografia": 1,
                    //         "Tarea Codigo": 1,
                    //         "Tarea Finca": 1
                    //     }
                    // }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_ppto",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "ppto": { "$ifNull": ["$info_ppto.Produccion programada", 0] }
            }
        }

        , {
            "$project": {
                "info_ppto": 0
            }
        }





        // //---test
        // //---Agrupar
        // , {
        //     "$group": {
        //         "_id": {
        //             "tarea": "$tipo_var_cartografiaiable"
        //             , "item": "abc"
        //         }
        //         , "cantidad_censos": { "$sum": 1 }

        //         // , "data": { "$push": "$$ROOT" }
        //     }
        // }





    ]
)
    .sort({ _id: -1 })