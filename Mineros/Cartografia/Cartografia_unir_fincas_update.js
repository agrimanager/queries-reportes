
//----- QUERY UPDATE

//--query cartografia fincas
var cursor_cartografia = db.cartography.aggregate(
    {
        $match: {
            type: "Feature"
        }
    }

    //---desechar las otras fincas
    , {
        $match: {
            path: {
                $not: {
                    $regex: `^,5ebc580fe6a57b26e6d6af59,` ////----santa elena
                }
            }
        }
    }
    , {
        $match: {
            path: {
                $not: {
                    $regex: `^,5ebc5795e6a57b26e6d6af52,` ////----la sierrita
                }
            }
        }
    }
    , {
        $match: {
            path: {
                $not: {
                    $regex: `^,5ebc57ede6a57b26e6d6af57,` ////----Natalia
                }
            }
        }
    }
)

//--id finca nueva
var id_finca_nueva = "605126833abafc311c16a3f3";


//---ciclar datos
cursor_cartografia.forEach(item_cartografia => {

    console.log("---------*****-----------");

    var path_old = item_cartografia.path;
    console.log("--path_old");
    console.log(path_old);

    var path_new = ",";
    var path_new_array = path_old.split(",");
    console.log("--path_new");

    //---generar path nuevo
    var num_iteracion = 0;
    for (var i = 1; i < path_new_array.length - 1; i++) {
        num_iteracion++;

        if (num_iteracion == 1) {
            path_new = path_new + id_finca_nueva + ",";
        } else {
            path_new = path_new + path_new_array[i] + ",";
        }
    }

    console.log(path_new);//ver


    //---actualizar datos
    db.cartography.update(
      {
          _id:item_cartografia._id
      },
      {
          $set:{
            path:path_new
          }
      }
    );
    console.log("--------------------");
})
