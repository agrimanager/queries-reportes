[

    { "$limit": 1 },

    {
        "$lookup": {
            "from": "tasks",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  
                "filtro_fecha_fin": "$Busqueda fin"         
            },
            "pipeline": [


                {
                    "$addFields": {
                        "rgDate": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                    }
                },
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "crop": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$crop" }, "object"] },
                                "then": [],
                                "else": "$crop"
                            }
                        },
                        "lots": {
                            "$cond": {
                                "if": { "$eq": ["$lots", {}] },
                                "then": [],
                                "else": "$lots"
                            }
                        }
                    }
                },


                {
                    "$lookup": {
                        "from": "users",
                        "localField": "uid",
                        "foreignField": "_id",
                        "as": "user_timezone"
                    }
                }, { "$unwind": "$user_timezone" }, { "$addFields": { "user_timezone": "$user_timezone.timezone" } }, {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                }, { "$unwind": "$activity" }, {
                    "$lookup": {
                        "from": "costsCenters",
                        "localField": "ccid",
                        "foreignField": "_id",
                        "as": "costsCenter"
                    }
                }, { "$unwind": "$costsCenter" }, {
                    "$lookup": {
                        "from": "farms",
                        "localField": "farm",
                        "foreignField": "_id",
                        "as": "farm"
                    }
                }, { "$unwind": "$farm" }, {
                    "$lookup": {
                        "from": "employees",
                        "localField": "supervisor",
                        "foreignField": "_id",
                        "as": "supervisor"
                    }
                }, {
                    "$unwind": {
                        "path": "$supervisor",
                        "preserveNullAndEmptyArrays": true
                    }
                }, {
                    "$addFields": {
                        "employees": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$employees" }, "array"] },
                                "then": "$employees",
                                "else": { "$map": { "input": { "$objectToArray": "$employees" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                            }
                        },
                        "productivityReport": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$productivityReport" }, "array"] },
                                "then": "$productivityReport",
                                "else": {
                                    "$map": {
                                        "input": { "$objectToArray": "$productivityReport" },
                                        "as": "productivityReportKV",
                                        "in": "$$productivityReportKV.v"
                                    }
                                }
                            }
                        },
                        "lots": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$lots" }, "array"] },
                                "then": "$lots",
                                "else": { "$map": { "input": { "$objectToArray": "$lots" }, "as": "lotKV", "in": "$$lotKV.v" } }
                            }
                        }
                    }
                }, { "$addFields": { "total_productos_seleccionados_labor": { "$size": "$supplies" } } }, {
                    "$unwind": {
                        "path": "$productivityReport",
                        "includeArrayIndex": "arrayIndex",
                        "preserveNullAndEmptyArrays": false
                    }
                }

                , {
                    "$addFields": {
                        "employees": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$eq": [{ "$type": "$productivityReport.employee" }, "string"] },
                                        { "$lt": [{ "$strLenCP": "$productivityReport.employee" }, 24] }
                                    ]
                                },
                                "then": [],
                                "else": { "$toObjectId": "$productivityReport.employee" }
                            }
                        }
                    }
                }



                , {
                    "$unwind": {
                        "path": "$productivityReport",
                        "preserveNullAndEmptyArrays": true
                    }
                }, { "$addFields": { "ProductividadEmpleado": { "$toDouble": "$productivityReport.quantity" } } }, {
                    "$lookup": {
                        "from": "employees",
                        "localField": "employees",
                        "foreignField": "_id",
                        "as": "Empleado"
                    }
                }, {
                    "$unwind": {
                        "path": "$Empleado",
                        "preserveNullAndEmptyArrays": true
                    }
                }, { "$addFields": { "TotalPagoEmpleado": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$ProductividadEmpleado" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] } } }, {
                    "$addFields": {
                        "productos": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$supplies" }, "array"] },
                                "then": "$supplies",
                                "else": { "$map": { "input": { "$objectToArray": "$employees" }, "as": "suppliesKV", "in": "$$suppliesKV.v" } }
                            }
                        }
                    }
                }, {
                    "$addFields": {
                        "productos": {
                            "$map": {
                                "input": "$productos._id",
                                "as": "strid",
                                "in": { "$toObjectId": "$$strid" }
                            }
                        }
                    }
                }, { "$lookup": { "from": "supplies", "localField": "productos", "foreignField": "_id", "as": "productos" } }, {
                    "$addFields": {
                        "user": { "$arrayElemAt": ["$user.timezone", 0] },
                        "productos": {
                            "$reduce": {
                                "input": "$productos.name",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                        "then": "$$this",
                                        "else": { "$concat": ["$$value", "; ", "$$this"] }
                                    }
                                }
                            }
                        },
                        "Etiquetas": {
                            "$ifNull": [{
                                "$reduce": {
                                    "input": "$tags",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            }, "--sin etiquetas--"]
                        },
                        "Tipo_cultivo": {
                            "$reduce": {
                                "input": "$crop", "initialValue": "", "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] }, "then": {
                                            "$switch": {
                                                "branches": [{
                                                    "case": { "$eq": ["$$this", "coffee"] },
                                                    "then": "Café"
                                                }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                    "case": { "$eq": ["$$this", "avocado"] },
                                                    "then": "Aguacate"
                                                }, {
                                                    "case": { "$eq": ["$$this", "orange"] },
                                                    "then": "Naranja"
                                                }, {
                                                    "case": { "$eq": ["$$this", "tangerine"] },
                                                    "then": "Mandarina"
                                                }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                    "case": { "$eq": ["$$this", "cacao"] },
                                                    "then": "Cacao"
                                                }, {
                                                    "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                    "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                                }, {
                                                    "case": { "$eq": ["$$this", "vegetable"] },
                                                    "then": "Hortalizas"
                                                }, {
                                                    "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                    "then": "Frutos tropicales"
                                                }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                    "case": { "$eq": ["$$this", "flower"] },
                                                    "then": "Flores"
                                                }, { "case": { "$eq": ["$$this", "cereal"] }, "then": "Cereales" }, {
                                                    "case": { "$eq": ["$$this", "cattle"] },
                                                    "then": "Ganado"
                                                }, { "case": { "$eq": ["$$this", "pork"] }, "then": "Cerdos" }, {
                                                    "case": { "$eq": ["$$this", "apiculture"] },
                                                    "then": "Apicultura"
                                                }, {
                                                    "case": { "$eq": ["$$this", "other-birds"] },
                                                    "then": "Otras aves"
                                                }, {
                                                    "case": { "$eq": ["$$this", "chicken"] },
                                                    "then": "Pollos"
                                                }, {
                                                    "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                    "then": "Animales acuáticos"
                                                }, {
                                                    "case": { "$eq": ["$$this", "other-animals"] },
                                                    "then": "Otros animales"
                                                }, {
                                                    "case": { "$eq": ["$$this", "woods"] },
                                                    "then": "Bosques"
                                                }, {
                                                    "case": { "$eq": ["$$this", "greenhouses"] },
                                                    "then": "Invernaderos"
                                                }, {
                                                    "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                    "then": "Otros (no limitativo)"
                                                }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                    "case": { "$eq": ["$$this", "yucca"] },
                                                    "then": "Yuca"
                                                }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                            }
                                        }, "else": {
                                            "$concat": ["$$value", "; ", {
                                                "$switch": {
                                                    "branches": [{
                                                        "case": { "$eq": ["$$this", "coffee"] },
                                                        "then": "Café"
                                                    }, { "case": { "$eq": ["$$this", "banana"] }, "then": "Banana" }, {
                                                        "case": { "$eq": ["$$this", "avocado"] },
                                                        "then": "Aguacate"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "orange"] },
                                                        "then": "Naranja"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tangerine"] },
                                                        "then": "Mandarina"
                                                    }, { "case": { "$eq": ["$$this", "lemon"] }, "then": "Limón" }, {
                                                        "case": { "$eq": ["$$this", "cacao"] },
                                                        "then": "Cacao"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "hight-density-fruit"] },
                                                        "then": "Frutos de alta densidad (uvas, fresas, cerezas, otros)"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "vegetable"] },
                                                        "then": "Hortalizas"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "tropical-fruit"] },
                                                        "then": "Frutos tropicales"
                                                    }, { "case": { "$eq": ["$$this", "palm"] }, "then": "Palma" }, {
                                                        "case": { "$eq": ["$$this", "flower"] },
                                                        "then": "Flores"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "cereal"] },
                                                        "then": "Cereales"
                                                    }, { "case": { "$eq": ["$$this", "cattle"] }, "then": "Ganado" }, {
                                                        "case": { "$eq": ["$$this", "pork"] },
                                                        "then": "Cerdos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "apiculture"] },
                                                        "then": "Apicultura"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-birds"] },
                                                        "then": "Otras aves"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "chicken"] },
                                                        "then": "Pollos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "aquatic-animals"] },
                                                        "then": "Animales acuáticos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-animals"] },
                                                        "then": "Otros animales"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "woods"] },
                                                        "then": "Bosques"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "greenhouses"] },
                                                        "then": "Invernaderos"
                                                    }, {
                                                        "case": { "$eq": ["$$this", "other-(non-limiting)"] },
                                                        "then": "Otros (no limitativo)"
                                                    }, { "case": { "$eq": ["$$this", "platano"] }, "then": "Platano" }, {
                                                        "case": { "$eq": ["$$this", "yucca"] },
                                                        "then": "Yuca"
                                                    }, { "case": { "$eq": ["$$this", "pasilla"] }, "then": "Pasilla" }], "default": null
                                                }
                                            }]
                                        }
                                    }
                                }
                            }
                        },
                        "Blancos_biologicos": {
                            "$reduce": {
                                "input": {
                                    "$cond": {
                                        "if": { "$eq": ["$biologicalTarget", ""] },
                                        "then": null,
                                        "else": "$biologicalTarget"
                                    }
                                },
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                        "then": "$$this",
                                        "else": { "$concat": ["$$value", "; ", "$$this"] }
                                    }
                                }
                            }
                        }
                    }
                }



                , {
                    "$addFields": {
                        "finca": { "$ifNull": ["$farm.name", "sin finca"] },
                        "ceco": { "$ifNull": ["$costsCenter.name", "sin ceco"] },

                        "cantidad_cartografia_seleccionada": { "$size": "$cartography.features" }
                    }
                }
                , {
                    "$project": {
                        "farm": 0,
                        "costsCenter": 0
                    }
                }


                , {
                    "$unwind": {
                        "path": "$cartography.features",
                        "preserveNullAndEmptyArrays": true
                    }
                }
                , {
                    "$addFields": {
                        "nombre_cartografia_seleccionada": { "$ifNull": ["$cartography.features.properties.name", "sin lote"] }
                    }
                }

                , {
                    "$addFields": {
                        "variable_cartografia": "$cartography"
                    }
                },


                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },



                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },
                
                {
                    "$addFields": {
                        "bloque": {
                            "$cond": {
                                "if": { "$eq": ["$nombre_cartografia_seleccionada", "sin lote"] },
                                "then": "no existe",
                                "else": "$bloque"
                            }
                        }
                    }
                },


                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },
                
                {
                    "$addFields": {
                        "lote": {
                            "$cond": {
                                "if": { "$eq": ["$nombre_cartografia_seleccionada", "sin lote"] },
                                "then": "no existe",
                                "else": "$lote"
                            }
                        }
                    }
                },


                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0

                        , "cartography": 0
                    }
                }


                , {
                    "$lookup": {
                        "from": "form_puentecentrodecostovsetapa",
                        "let": { "nombre_ceco": "$ceco" },
                        "as": "ceco_etapa",
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            { "$eq": ["$$nombre_ceco", "$Centro de costo"] }
                                        ]
                                    }
                                }
                            }
                            , {
                                "$limit": 1
                            }
                            , {
                                "$project": {
                                    "Etapa o Codigo": 1
                                }
                            }
                        ]
                    }
                }

                , {
                    "$unwind": {
                        "path": "$ceco_etapa",
                        "preserveNullAndEmptyArrays": false
                    }
                }

                , {
                    "$addFields": {
                        "ceco_etapa_codigo": { "$ifNull": ["$ceco_etapa.Etapa o Codigo", "sin etapa"] }
                    }
                }
                , {
                    "$project": {
                        "ceco_etapa": 0
                    }
                }

                , {
                    "$lookup": {
                        "from": "form_puentevsordenantigua",
                        "let": {
                            "nombre_ceco": "$ceco",
                            "nombre_finca": "$finca"
                            
                            , "nombre_finca_bloque": "$bloque"

                        },
                        "as": "orden_antigua",
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            { "$eq": ["$$nombre_ceco", "$Centro de costo"] },
                                            {
                                                "$or": [
                                                    { "$eq": ["$$nombre_finca", "$Finca"] },
                                                    { "$eq": ["$$nombre_finca_bloque", "$Finca"] }
                                                ]
                                            }
                                        ]
                                    }
                                }
                            }
                            , {
                                "$limit": 1
                            }
                            , {
                                "$project": {
                                    "Orden antigua": 1,
                                    "Finca": 1,
                                    "Centro de costo": 1
                                }
                            }
                        ]
                    }
                }

                , {
                    "$unwind": {

                        "path": "$orden_antigua",
                        "preserveNullAndEmptyArrays": false
                    }
                }

                , {
                    "$addFields": {
                        "orden_antigua": { "$ifNull": ["$orden_antigua.Orden antigua", "sin orden antigua"] }
                    }
                }



                , {
                    "$lookup": {
                        "from": "form_puentelotesvsordeninterna",
                        "let": { "nombre_lote": "$nombre_cartografia_seleccionada" },
                        "as": "orden_interna",
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                            { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                        ]
                                    }
                                }
                            }
                            , {
                                "$limit": 1
                            }
                            , {
                                "$project": {
                                    "ORDEN": 1,
                                    "CONCEPTO": 1
                                }
                            }
                        ]
                    }
                }

                , {
                    "$unwind": {
                        "path": "$orden_interna",
                        "preserveNullAndEmptyArrays": true
                    }
                }

                , {
                    "$addFields": {
                        "orden_interna_orden": { "$ifNull": ["$orden_interna.ORDEN", "sin orden interna"] },
                        "orden_interna_concepto": { "$ifNull": ["$orden_interna.CONCEPTO", "sin orden interna"] }
                    }
                }
                , {
                    "$project": {
                        "orden_interna": 0
                    }
                }



                
                , {
                    "$addFields": {
                        
                        "orden_interna_orden": {
                            "$cond": {
                                "if": { "$eq": ["$ceco", "Generales Mantenimiento"] },
                                "then": "1281450110",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$ceco", "Generales Produccion"] },
                                        "then": "1281450101",
                                        "else": "$orden_interna_orden"
                                    }
                                }
                            }
                        },
                        "orden_interna_concepto": {
                            "$cond": {
                                "if": { "$eq": ["$ceco", "Generales Mantenimiento"] },
                                "then": "LOTE 99999  -- Generales Mantenimiento",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$ceco", "Generales Produccion"] },
                                        "then": "LOTE 99999  -- Generales Produccion",
                                        "else": "$orden_interna_concepto"
                                    }
                                }
                            }
                        },
                        "nombre_cartografia_seleccionada": {
                            "$cond": {
                                "if": { "$eq": ["$ceco", "Generales Mantenimiento"] },
                                "then": "99999",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$ceco", "Generales Produccion"] },
                                        "then": "99999",
                                        "else": "$nombre_cartografia_seleccionada"
                                    }
                                }
                            }
                        }
                    }
                }



                , {
                    "$addFields": {
                        "cantidad_cartografia_seleccionada": {
                            "$cond": {
                                "if": { "$eq": ["$cantidad_cartografia_seleccionada", 0] },
                                "then": 1,
                                "else": "$cantidad_cartografia_seleccionada"
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "TotalPagoEmpleado_x_lote": {
                            "$divide": ["$TotalPagoEmpleado", "$cantidad_cartografia_seleccionada"]
                        },
                        "TotalProductividadEmpleado_x_lote": {
                            "$divide": [{ "$ifNull": ["$ProductividadEmpleado", 0] }, "$cantidad_cartografia_seleccionada"]
                        }
                    }
                }





                , {
                    "$project": {
                        "_id": 0
                    }
                }


                , {
                    "$project": {

                        
                        "Codigo Labor": "$cod",
                        "Estado Labor": {
                            "$switch": {
                                "branches": [{
                                    "case": { "$eq": ["$status", "To do"] },
                                    "then": "⏰Por hacer"
                                }, { "case": { "$eq": ["$status", "Doing"] }, "then": "⚽ En progreso" }, {
                                    "case": { "$eq": ["$status", "Done"] },
                                    "then": "✔ Listo"
                                }]
                            }
                        },

                        
                        "Actividad": "$activity.name",
                        "Unidad Medida de Actividad": {
                            "$switch": {
                                "branches": [{
                                    "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                                    "then": "Bloque"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                                    "then": "Lotes"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                                    "then": "Linea"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                                    "then": "Árboles"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                                    "then": "Centro frutero"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                                    "then": "Poligono de muestreo"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                                    "then": "Valvula"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                                    "then": "Drenaje"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                                    "then": "Aspersors"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                                    "then": "Red de riego uno"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                                    "then": "Red de riego dos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                                    "then": "Red de riego tres"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                                    "then": "Trampa"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                                    "then": "Vías"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                                    "then": "Bosque"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                                    "then": "Sensor"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                                    "then": "Cable vía"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                                    "then": "Edificio"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                                    "then": "Cuerpo de agua"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                                    "then": "Poligonos adicionales"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                                    "then": "Unidades de cultivo"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                                    "then": "Jornales"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                                    "then": "Cantidades"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                                    "then": "Metros"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "km"] },
                                    "then": "Kilometros"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                                    "then": "Centimetros"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                                    "then": "Millas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                                    "then": "Yardas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                                    "then": "Pies"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                                    "then": "Pulgadas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                                    "then": "Kilogramos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                                    "then": "Gramos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                                    "then": "Miligramos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                                    "then": "Toneladas estadounidenses"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "us/ton"] },
                                    "then": "Toneladas estadounidenses"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                                    "then": "Toneladas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                                    "then": "Onzas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                                    "then": "Libras"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                                    "then": "Litros"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                                    "then": "Galones estadounidenses"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                                    "then": "Galones"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                                    "then": "Pies cúbicos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                                    "then": "Pulgadas cúbicas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                                    "then": "Centimetros cúbicos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                                    "then": "Metros cúbicos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                                    "then": "Bultos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                                    "then": "Bolsas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                                    "then": "sacks"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                                    "then": "Yemas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                                    "then": "Factura"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                                    "then": "Flete"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                                    "then": "Picadero"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                                    "then": "Hora"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                                    "then": "Por cantidad"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                                    "then": "Hectáreas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                                    "then": "Cuadras"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                                    "then": "Canecas"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                                    "then": "Racimos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                                    "then": "Metro cúbico"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "metro-line"] },
                                    "then": "Metro Lineal"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "square-meter"] },
                                    "then": "Metro cuadrado"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "estaquilla"] },
                                    "then": "estaquilla"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "holes"] },
                                    "then": "Hoyos"
                                }, {
                                    "case": { "$eq": ["$productivityPrice.measure", "rastas"] },
                                    "then": "Rastas"
                                }, { "case": { "$eq": ["$productivityPrice.measure", "postes"] }, "then": "Postes" }], "default": "--------"
                            }
                        },
                        "Precio x unidad de Actividad": "$productivityPrice.price",

                        
                        "Finca": "$finca",
                        "Centro de costos": "$ceco",
                        "Lote": "$nombre_cartografia_seleccionada",

                        
                        "Etapa": "$ceco_etapa_codigo",
                        "OI Agroforestal Concepto": "$orden_interna_concepto",
                        "OI Agroforestal": "$orden_interna_orden",
                        "OI Antigua": "$orden_antigua",


                        "Nombre de Empleado": { "$ifNull": [{ "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"] }, "--sin empleados--"] },
                        "Identificacion": { "$ifNull": ["$Empleado.numberID", "--"] },
                        "TotalProductividadEmpleado_x_lote": { "$ifNull": ["$TotalProductividadEmpleado_x_lote", 0] },
                        "TotalPagoEmpleado_x_lote": "$TotalPagoEmpleado_x_lote",

                     
                        "NIT Empresa": "811021071",
                        "Observaciones": "$observation",
                        "Supervisor": { "$ifNull": [{ "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"] }, "--sin supervisor--"] },
                        "Etiquetas": { "$cond": { "if": { "$eq": ["$Etiquetas", ""] }, "then": "--sin etiquetas--", "else": "$Etiquetas" } },



                        
                        "Fecha inicio": {
                            "$dateToString": {
                                "date": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] },
                                "format": "%Y-%m-%d %H:%M",
                                "timezone": "$user_timezone"
                            }
                        },
                        "Fecha fin": {
                            "$dateToString": {

                                "date": { "$arrayElemAt": ["$when.finish", { "$subtract": [{ "$size": "$when.finish" }, 1] }] },
                                "format": "%Y-%m-%d %H:%M",
                                "timezone": "$user_timezone"
                            }
                        },
                        "rgDate": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                    }
                }


            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    
    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
            "num_semana": { "$week": { "date": "$rgDate" } },
            "num_dia_semana": { "$dayOfWeek": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }


            , "Dia_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                    ],
                    "default": "dia de la semana desconocido"
                }
            }
        }
    },

    
    { "$project": { "num_dia_semana": 0 } }
    
    




]