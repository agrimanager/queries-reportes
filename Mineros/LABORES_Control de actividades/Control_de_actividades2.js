//----🚩 (REPORTE SIN FORMULARIO) ---- SIN FILTRO DE FECHAS
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-02-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        { "$limit": 1 },

        {
            "$lookup": {
                "from": "tasks",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [


                    {
                        "$addFields": {
                            "inicio": { "$arrayElemAt": ["$when.start", -1] },
                            "fin": { "$arrayElemAt": ["$when.finish", -1] }
                        }
                    },



                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$inicio", "$$filtro_fecha_inicio"] },
                                    { "$lte": ["$fin", "$$filtro_fecha_fin"] }
                                ]
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "productivityReport.employees": {
                                "$map": {
                                    "input": "$productivityReport",
                                    "as": "strid",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$type": "$$strid.employee" }, "objectId"] },
                                            "then": "$$strid.employee",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$eq": [{ "$strLenCP": "$$strid.employee" }, 24] },
                                                    "then": { "$toObjectId": "$$strid.employee" },
                                                    "else": null
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    },

                    { "$unwind": "$user_timezone" },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    },

                    { "$unwind": "$farm" },

                    {
                        "$lookup": {
                            "from": "warehouses",
                            "localField": "supplies.wid",
                            "foreignField": "_id",
                            "as": "warehouse_reference"
                        }
                    },

                    { "$unwind": { "path": "$warehouse_reference", "preserveNullAndEmptyArrays": true } },

                    {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },

                    { "$unwind": "$activity" },

                    {
                        "$lookup": {
                            "from": "costsCenters",
                            "localField": "ccid",
                            "foreignField": "_id",
                            "as": "costsCenter"
                        }
                    },

                    { "$unwind": "$costsCenter" },

                    { "$unwind": "$productivityReport" },

                    {
                        "$lookup": {
                            "from": "employees",
                            "localField": "productivityReport.employee",
                            "foreignField": "_id",
                            "as": "Empleados"
                        }
                    },


                    {
                        "$addFields": {
                            "productos diferentes": { "$size": "$supplies" }
                        }
                    },

                    { "$unwind": { "path": "$supplies", "preserveNullAndEmptyArrays": true } },

                    {
                        "$lookup": {
                            "from": "supplies",
                            "localField": "supplies._id",
                            "foreignField": "_id",
                            "as": "supplies_reference"
                        }
                    },

                    { "$unwind": { "path": "$supplies_reference", "preserveNullAndEmptyArrays": true } },

                    //--cruce1 ==== ceco - etapa
                    {
                        "$lookup": {
                            "from": "form_puentecentrodecostovsetapa",
                            "localField": "costsCenter.name",
                            "foreignField": "Centro de costo",
                            "as": "etapa_arr"
                        }
                    },


                    {
                        "$addFields": {
                            "user_timezone": "$user_timezone.timezone",

                            "Empleados_fullNames": {
                                "$map": {
                                    "input": "$Empleados",
                                    "as": "empleado",
                                    "in": { "$concat": ["$$empleado.firstName", " ", "$$empleado.lastName"] }
                                }
                            },
                            "etapa": { "$arrayElemAt": ["$etapa_arr", 0] }
                        }
                    },


                    {
                        "$addFields": {
                            "Lote": "$cartography.features.properties.name"
                        }
                    },

                    { "$unwind": { "path": "$Lote", "preserveNullAndEmptyArrays": true } },

                    {
                        "$addFields": {
                            "Lote": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$ne": [{ "$type": "$Lote" }, "string"] }, { "$eq": ["$etapa.Etapa o Codigo", "199-1"] }]
                                    },
                                    "then": "99999",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$ne": [{ "$type": "$Lote" }, "string"] }, { "$eq": ["$etapa.Etapa o Codigo", "199-2"] }]
                                            },
                                            "then": "9999",
                                            "else": "$Lote"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    //--cruce2 ==== lote - OI
                    {
                        "$lookup": {
                            "from": "form_puentelotesvsordeninterna",
                            "let": { "lote": "$Lote" },
                            "pipeline": [
                                {
                                    "$addFields": {
                                        "lote": { "$arrayElemAt": ["$LOTE.features", 0] }
                                    }
                                },
                                {
                                    "$addFields": {
                                        "lote": "$lote.properties.name"
                                    }
                                },
                                {
                                    "$match": {
                                        "$expr": { "$eq": ["$lote", "$$lote"] }
                                    }
                                },
                                {
                                    "$project": {
                                        "ORDEN": 1,
                                        "lote": 1
                                    }
                                }
                            ],
                            "as": "orden_arr"
                        }
                    },

                    //--cruce3 ==== ceco,finca - etapa
                    {
                        "$lookup": {
                            "from": "form_puentevsordenantigua",
                            "let": {
                                "cc": "$costsCenter.name",
                                "finca": "$farm.name"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Finca", "$$finca"] },
                                                { "$eq": ["$Centro de costo", "$$cc"] }
                                            ]

                                        }
                                    }
                                },
                                {
                                    "$project": {
                                        "Orden antigua": 1,
                                        "Finca": 1,
                                        "Centro de costo": 1
                                    }
                                }
                            ],
                            "as": "antigua_arr"
                        }
                    },

                    // {
                    //     "$addFields": {
                    //         "OI Agroforestal": { "$arrayElemAt": ["$orden_arr", 0] },
                    //         "orden antigua": { "$arrayElemAt": ["$antigua_arr", 0] },

                    //         "($) Total Pago Empleado": {
                    //             "$multiply": ["$productivityReport.quantity", "$productivityPrice.price"]
                    //         }
                    //     }
                    // },

                    // {
                    //     "$addFields": {
                    //         "OI Agroforestal.ORDEN": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$Lote", "99999"] },
                    //                 "then": "1281450110",
                    //                 "else": {
                    //                     "$cond": {
                    //                         "if": { "$eq": ["$Lote", "9999"] },
                    //                         "then": "1281450101",
                    //                         "else": "$OI Agroforestal.ORDEN"
                    //                     }
                    //                 }
                    //             }
                    //         }
                    //     }
                    // },

                    // {
                    //     "$addFields": {
                    //         "Pago total segun producto": {
                    //             "$cond": {
                    //                 "if": { "$gt": ["$productos diferentes", 0] },
                    //                 "then": { "$divide": ["$($) Total Pago Empleado", "$productos diferentes"] },
                    //                 "else": "$($) Total Pago Empleado"
                    //             }
                    //         },

                    //         "Productividad segun producto": {
                    //             "$cond": {
                    //                 "if": { "$gt": ["$productos diferentes", 0] },
                    //                 "then": { "$divide": ["$productivityReport.quantity", "$productos diferentes"] },
                    //                 "else": "$productivityReport.quantity"
                    //             }
                    //         }
                    //     }
                    // },




                    // {
                    //     "$project": {

                    //         "_id": 0,
                    //         "Codigo Labor": "$cod",
                    //         "Estado Labor": {
                    //             "$switch": {
                    //                 "branches": [{
                    //                     "case": { "$eq": ["$status", "To do"] },
                    //                     "then": "⏰Por hacer"
                    //                 }, { "case": { "$eq": ["$status", "Doing"] }, "then": "� En progreso" }, {
                    //                     "case": { "$eq": ["$status", "Done"] },
                    //                     "then": "✔ Listo"
                    //                 }]
                    //             }
                    //         },

                    //         "Fecha inicio": {
                    //             "$dateToString": {
                    //                 "date": {
                    //                     "$arrayElemAt": ["$when.start", -1]
                    //                 },
                    //                 "format": "%Y-%m-%d %H:%M",
                    //                 "timezone": "$user_timezone"
                    //             }
                    //         },
                    //         "Fecha fin": {
                    //             "$dateToString": {
                    //                 "date": {
                    //                     "$arrayElemAt": ["$when.finish", -1]
                    //                 },
                    //                 "format": "%Y-%m-%d %H:%M",
                    //                 "timezone": "$user_timezone"
                    //             }
                    //         },

                    //         "Finca": "$farm.name",
                    //         "Lote": "$Lote",
                    //         "Bodega": "$warehouse_reference.name",
                    //         "OI Agroforestal": "$OI Agroforestal.ORDEN",
                    //         "etapa": "$etapa.Etapa o Codigo",
                    //         "orden antigua": "$orden antigua.Orden antigua",
                    //         "Actividad": "$activity.name",
                    //         "Colaborador": { "$arrayElemAt": ["$Empleados_fullNames", 0] },
                    //         "CC/NIT": "811021071",
                    //         "centro de costo": "$costsCenter.name",
                    //         "Duración": { "$ifNull": ["$Etiquetas", "--sin datos--"] },
                    //         "Producto": { "$ifNull": ["$supplies_reference.name", "$supplies.supplieName"] },
                    //         "Cantidad Producto": "$supplies.quantity",
                    //         "UM Producto": { "$ifNull": ["$supplies_reference.inputMeasureEquivalence", "$supplies.inputmeasure"] },

                    //         "Mes (Contabilizado)": {
                    //             "$cond": {
                    //                 "if": {
                    //                     "$or": [
                    //                         { "$eq": [{ "$type": "$observation" }, "string"] },
                    //                         { "$eq": [{ "$type": "$observation" }, "null"] }
                    //                     ]
                    //                 },
                    //                 "then": { "$month": "$rgDate" },
                    //                 "else": "$observation"
                    //             }
                    //         },

                    //         "Unidad Medida de Labor": {
                    //             "$switch": {
                    //                 "branches": [
                    //                     {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                    //                         "then": "Bloque"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                    //                         "then": "Lotes"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                    //                         "then": "Linea"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                    //                         "then": "Árboles"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                    //                         "then": "Centro frutero"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                    //                         "then": "Poligono de muestreo"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                    //                         "then": "Valvula"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                    //                         "then": "Drenaje"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                    //                         "then": "Aspersors"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                    //                         "then": "Red de riego uno"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                    //                         "then": "Red de riego dos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                    //                         "then": "Red de riego tres"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                    //                         "then": "Trampa"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                    //                         "then": "Vías"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                    //                         "then": "Bosque"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                    //                         "then": "Sensor"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                    //                         "then": "Cable vía"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                    //                         "then": "Edificio"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                    //                         "then": "Cuerpo de agua"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                    //                         "then": "Poligonos adicionales"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                    //                         "then": "Unidades de cultivo"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                    //                         "then": "Jornales"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                    //                         "then": "Cantidades"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                    //                         "then": "Metros"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "km"] },
                    //                         "then": "Kilometros"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                    //                         "then": "Centimetros"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                    //                         "then": "Millas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                    //                         "then": "Yardas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                    //                         "then": "Pies"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                    //                         "then": "Pulgadas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                    //                         "then": "Kilogramos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                    //                         "then": "Gramos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                    //                         "then": "Miligramos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                    //                         "then": "Toneladas estadounidenses"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "us/ton"] },
                    //                         "then": "Toneladas estadounidenses"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                    //                         "then": "Toneladas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                    //                         "then": "Onzas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                    //                         "then": "Libras"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                    //                         "then": "Litros"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                    //                         "then": "Galones estadounidenses"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                    //                         "then": "Galones"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                    //                         "then": "Pies cúbicos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                    //                         "then": "Pulgadas cúbicas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                    //                         "then": "Centimetros cúbicos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                    //                         "then": "Metros cúbicos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                    //                         "then": "Bultos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                    //                         "then": "Bolsas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                    //                         "then": "sacks"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                    //                         "then": "Yemas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                    //                         "then": "Factura"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                    //                         "then": "Flete"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                    //                         "then": "Picadero"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                    //                         "then": "Hora"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                    //                         "then": "Por cantidad"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                    //                         "then": "Hectáreas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                    //                         "then": "Cuadras"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                    //                         "then": "Canecas"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                    //                         "then": "Racimos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                    //                         "then": "Metro cúbico"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "metro-line"] },
                    //                         "then": "Metro Lineal"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "square-meter"] },
                    //                         "then": "Metro cuadrado"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "estaquilla"] },
                    //                         "then": "estaquilla"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "holes"] },
                    //                         "then": "Hoyos"
                    //                     }, {
                    //                         "case": { "$eq": ["$productivityPrice.measure", "rastas"] },
                    //                         "then": "Rastas"
                    //                     }, { "case": { "$eq": ["$productivityPrice.measure", "postes"] }, "then": "Postes" }
                    //                 ],
                    //                 "default": "--------"
                    //             }
                    //         },

                    //         "Productividad dividido x producto": "$Productividad segun producto",
                    //         "Productividad": "$productivityReport.quantity",
                    //         "($) Valor unitario según UM": "$productivityPrice.price",
                    //         "($) Pago total dividido x producto": "$Pago total segun producto",
                    //         "($) Total Pago Empleado": "$($) Total Pago Empleado",
                    //         "rgDate": "$$filtro_fecha_inicio"
                    //     }
                    // }
                ]
            }
        },

        {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        },

        { "$unwind": "$datos" },

        { "$replaceRoot": { "newRoot": "$datos" } },

        // {
        //     "$addFields": {
        //         "rgDate": "$rgDate"
        //     }
        // }

    ]
)
