var labores = db.tasks.aggregate(


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
            "Busqueda fin": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------


)

// //--ver
// labores



var result = [];


//---ciclar labores
labores.forEach(item_labores => {

    var actividades = db.activities.aggregate();


    // console.log(item_labores);

    //---ciclar actividades
    actividades.forEach(item_actividades => {

        // console.log(item_actividades._id);


        //---!!!!NO ES CAPAZ CON ESTE CONDICIONAL
        //if (item_labores.activity === item_actividades._id) {
        if (item_labores.activity.toString() === item_actividades._id.toString()) {
            // console.log(item_actividades.productivity.quantity);
            // result.push(
            //     {
            //         acti: item_actividades.productivity.quantity
            //     }
            // )


            //---ACTUALIZAR
            db.tasks.update(
                {
                    _id:item_labores._id
                },
                {
                    $set:{
                        "productivityPrice.price":item_actividades.productivity.quantity
                    }

                }
            )


        }

    })

})


//--ver
result
