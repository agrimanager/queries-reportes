//----reporte1 = ALL
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "form_registrodelluvias",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //---condiciones de fechas
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha a registrar" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //---variables de fechas
                    { "$addFields": { "anio": { "$year": "$Fecha a registrar" } } },
                    { "$addFields": { "mes": { "$month": "$Fecha a registrar" } } }



                    //---Agrupar
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Finca"

                                , "anio": "$anio"
                                , "mes": "$mes"
                            }

                            , "sum_lluvia": { "$sum": { "$toDouble": "$Cantidad de lluvia  mm" } }
                            , "prom_lluvia": { "$avg": { "$toDouble": "$Cantidad de lluvia  mm" } }
                            , "cantidad_censos_lluvia": { "$sum": 1 }

                            // , "data": { "$push": "$$ROOT" }
                        }
                    }


                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        , {
            "$lookup": {
                "from": "form_registrodeproducciondiaria",
                "as": "data",
                "let": {
                    "finca": "$_id.finca"
                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                },
                "pipeline": [

                    //---condiciones de fechas
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha y hora Inicio" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //---variables de fechas
                    { "$addFields": { "anio": { "$year": "$Fecha y hora Inicio" } } },
                    { "$addFields": { "mes": { "$month": "$Fecha y hora Inicio" } } }

                    //---filtros fechas
                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$anio", "$$anio"]
                            }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$mes", "$$mes"]
                            }
                        }
                    }


                    //---Finca
                    , { "$addFields": { "finca_oid": { "$toObjectId": "$Point.farm" } } }
                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca_oid",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    }

                    , { "$unwind": "$Finca" }

                    , { "$addFields": { "Finca": "$Finca.name" } }


                    //----filtro de finca
                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Finca", "$$finca"]
                            }
                        }
                    }

                    //---Agrupar
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Finca"

                                , "anio": "$anio"
                                , "mes": "$mes"
                            }

                            , "cantidad_censos_produccion": { "$sum": 1 }

                            //---latex
                            , "sum_kg_latex": { "$sum": { "$toDouble": "$Latex en Kg" } }
                            , "prom_kg_latex": { "$avg": { "$toDouble": "$Latex en Kg" } }


                            //---rayadas
                            //....


                        }
                    }

                ]

            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }

        , { "$addFields": { "cantidad_censos_produccion": { "$ifNull": ["$data.cantidad_censos_produccion", 0] } } }

        , { "$addFields": { "sum_kg_latex": { "$ifNull": ["$data.sum_kg_latex", 0] } } }
        , { "$addFields": { "prom_kg_latex": { "$ifNull": ["$data.prom_kg_latex", 0] } } }

        //---rayadas
        //....


        //---no mostrar info_produccion
        , { "$project": { "data": 0 } }





        //---PROYECCION FINAL
        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"

                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                }

                , "data": { "$push": "$$ROOT" }

            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {

                            "cantidad_censos_lluvia": "$data.cantidad_censos_lluvia"
                            , "sum_lluvia": "$data.sum_lluvia"
                            , "prom_lluvia": "$data.prom_lluvia"

                            , "cantidad_censos_produccion": "$data.cantidad_censos_produccion"
                            , "sum_kg_latex": "$data.sum_kg_latex"
                            , "prom_kg_latex": "$data.prom_kg_latex"
                            // , "aaaaaa": "$data.aaaaaa"
                            // , "aaaaaa": "$data.aaaaaa"

                            , "rgDate": "$data.rgDate"

                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }





    ]
)