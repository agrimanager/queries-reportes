//----reporte1 = ALL
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "form_registrodelluvias",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //---condiciones de fechas
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha a registrar" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //---variables de fechas
                    { "$addFields": { "anio": { "$year": "$Fecha a registrar" } } },
                    { "$addFields": { "mes": { "$month": "$Fecha a registrar" } } }



                    //---Agrupar
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Finca"

                                , "anio": "$anio"
                                , "mes": "$mes"
                            }

                            , "sum_lluvia": { "$sum": { "$toDouble": "$Cantidad de lluvia  mm" } }
                            , "prom_lluvia": { "$avg": { "$toDouble": "$Cantidad de lluvia  mm" } }
                            , "cantidad_censos_lluvia": { "$sum": 1 }

                            // , "data": { "$push": "$$ROOT" }
                        }
                    }


                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        , {
            "$lookup": {
                "from": "form_registrodeproducciondiaria",
                "as": "data",
                "let": {
                    "finca": "$_id.finca"
                    , "anio": "$_id.anio"
                    , "mes": "$_id.mes"
                },
                "pipeline": [

                    //---condiciones de fechas
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha y hora Inicio" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //---variables de fechas
                    { "$addFields": { "anio": { "$year": "$Fecha y hora Inicio" } } },
                    { "$addFields": { "mes": { "$month": "$Fecha y hora Inicio" } } }

                    //---filtros fechas
                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$anio", "$$anio"]
                            }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$mes", "$$mes"]
                            }
                        }
                    }


                    //---Finca
                    , { "$addFields": { "finca_oid": { "$toObjectId": "$Point.farm" } } }
                    , {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca_oid",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    }

                    , { "$unwind": "$Finca" }

                    , { "$addFields": { "Finca": "$Finca.name" } }


                    //----filtro de finca
                    , {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Finca", "$$finca"]
                            }
                        }
                    }

                    //---Agrupar
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Finca"

                                , "anio": "$anio"
                                , "mes": "$mes"
                            }

                            , "cantidad_censos_produccion": { "$sum": 1 }

                            //---latex
                            , "sum_kg_latex": { "$sum": { "$toDouble": "$Latex en Kg" } }
                            , "prom_kg_latex": { "$avg": { "$toDouble": "$Latex en Kg" } }
                            
                            
                            //---rayadas
                            //....


                        }
                    }

                ]

            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }

        , { "$addFields": { "cantidad_censos_produccion": { "$ifNull": ["$data.cantidad_censos_produccion", 0] } } }

        , { "$addFields": { "sum_kg_latex": { "$ifNull": ["$data.sum_kg_latex", 0] } } }
        , { "$addFields": { "prom_kg_latex": { "$ifNull": ["$data.prom_kg_latex", 0] } } }
        
        //---rayadas
        //....


        //---no mostrar info_produccion
        , { "$project": { "data": 0 } }


    ]
)