db.form_registrodelluvias.aggregate(
    [


        //---condiciones de fechas
        { "$addFields": { "anio_filtro": { "$year": "$Fecha a registrar" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        //---variables de fechas
        { "$addFields": { "anio": { "$year": "$Fecha a registrar" } } },
        { "$addFields": { "mes": { "$month": "$Fecha a registrar" } } },



        //---Agrupar
        {
            "$group": {
                "_id": {
                    "finca": "$Finca"

                    , "anio": "$anio"
                    , "mes": "$mes"
                }

                , "sum_lluvia": { "$sum": { "$toDouble": "$Cantidad de lluvia  mm" } }
                , "prom_lluvia": { "$avg": { "$toDouble": "$Cantidad de lluvia  mm" } }
                , "cantidad_censos_lluvia": { "$sum": 1 }

                // , "data": { "$push": "$$ROOT" }
            }
        }




    ]
)