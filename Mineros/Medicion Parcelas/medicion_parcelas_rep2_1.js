db.form_mediciondeparcelas.aggregate(
    [

        {
            "$project": {
                "Point": 0,
                "Formula": 0
            }
        },


        {
            "$addFields": {
                "año": { "$year": "$rgDate" },
                "Edad": { "$ifNull": ["$Edad", 0] }
            }
        },


        {
            "$addFields": {
                "CAP Meta": {
                    "$cond": {
                        "if": { "$gte": ["$Edad", 0] },
                        "then": { "$multiply": [7.5, "$Edad"] },
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "cumplimiento": {
                    "$cond": {
                        "if": { "$gte": ["$PERIMETRO 2020", "$CAP Meta"] },
                        "then": "CUMPLE",
                        "else": "NO CUMPLE"
                    }
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$FINCA",
                    "lote": "$LOTE",
                    "arbol": "$NUMERO DE ARBOL",
                    "crecimiento": "$PERIMETRO 2020",
                    "edad": "$Edad",
                    "cap_meta": "$CAP Meta"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "lote": "$_id.lote"
                },
                "edad_promedio": { "$avg": "$_id.edad" },
                "cap_meta_promedio": { "$avg": "$_id.cap_meta" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "edad promedio": "$edad_promedio",
                            "cap meta propmedio": "$cap_meta_promedio"

                        }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "cumplimiento por finca y lote": {
                    "$cond": {
                        "if": { "$gte": ["$crecimiento promedio cm", "$cap meta propmedio"] },
                        "then": "INDICADOR POSITIVO",
                        "else": "INDICADOR NEGATIVO"
                    }
                }
            }
        },





        {
            "$addFields": {
                "no altura minima": {
                    "$cond": {
                        "if": { "$eq": ["$Altura minima", "No"] },
                        "then": 1,
                        "else": 0
                    }
                },
                "no existe": {
                    "$cond": {
                        "if": { "$eq": ["$Existe", "No"] },
                        "then": 1,
                        "else": 0
                    }
                },
                "no cumple": {
                    "$cond": {
                        "if": { "$eq": ["$cumplimiento", "NO CUMPLE"] },
                        "then": 1,
                        "else": 0
                    }
                },
                "aptos": {
                    "$cond": {
                        "if": { "$eq": ["$cumplimiento", "CUMPLE"] },
                        "then": 1,
                        "else": 0
                    }
                },
            }
        },








        {
            "$group": {
                "_id": {
                    "finca": "$FINCA",
                    "lote": "$LOTE",
                    "arbol": "$NUMERO DE ARBOL",

                    "no_altura_minima": "$no altura minima",
                    "no_existe": "$no existe",
                    "no_cumple": "$no cumple",
                    "aptos": "$aptos"

                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "lote": "$_id.lote"
                },
                "no_altura_minima_count": { "$sum": "$_id.no_altura_minima" },
                "no_existe_count": { "$sum": "$_id.no_existe" },
                "no_cumple_count": { "$sum": "$_id.no_cumple" },
                "aptos_count": { "$sum": "$_id.aptos" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "no altura minima": "$altura_minima_count",
                            "no existe": "$no_existe_count",
                            "no cumple": "$no_cumple_count",
                            "aptos": "$aptos_count"
                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "arboles evaluados": {
                    "$add": [
                        "$no altura minima",
                        "$no existe",
                        "$no cumple",
                        "$aptos"
                    ]
                }
            }
        },


        {
            "$addFields": {
                "pct_aptos_produccion": {
                    "$cond": {
                        "if": { "$gt": ["$arboles evaluados", 0] },
                        "then": {
                            "$multiply": [
                                {
                                    "$divide": [
                                        "$aptos",
                                        "$arboles evaluados"
                                    ]
                                },
                                100
                            ]
                        },
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "indicador de produccion": {
                    "$cond": {
                        "if": { "$gte": ["$pct_aptos_produccion", 51] },
                        "then": "POSITIVO",
                        "else": "NEGATIVO"
                    }
                }
            }
        },





        {
            "$group": {
                "_id": {
                    "finca": "$FINCA",
                    "lote": "$LOTE",
                    "arboles_ev": "$arboles evaluados"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                },
                "arboles_plantacion": { "$sum": "$_id.arboles_ev" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "arboles de la plantacion": "$arboles_plantacion" }
                    ]
                }
            }
        }

    ]
)
