[




    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Arbol": 0,
            "Point": 0,
            "bloque_id": 0,

            "Formula": 0,
            "Sampling": 0,
            "capture": 0,
            "uid": 0,

            "PLAGA_Mosca del ovario": 0,
            "PLAGA_Hemiberlesia lataniae": 0,
            "PLAGA_Bombacoccus": 0,
            "PLAGA_Falso medidor": 0,
            "PLAGA_Epitrix": 0,
            "PLAGA_Trips": 0,
            "PLAGA_Stenoma Catenifer": 0,
            "PLAGA_Cerambycidae": 0,
            "PLAGA_Acaro rojo": 0,
            "PUNTO CENSADO": 0
        }
    },



    {
        "$addFields": {
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    },
    {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    },



    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
            },
            "count_censados": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "arboles_censados_x_lote": "$count_censados"
                    }
                ]
            }
        }
    },



    {
        "$addFields": {
            "tiene_plaga": {
                "$cond": {
                    "if": {
                        "$and": [
                            { "$ne": ["$PLAGA", null] },
                            { "$ne": ["$PLAGA", "Ninguno"] },
                            { "$ne": ["$PLAGA", ""] },

                            {
                                "$or": [
                                    { "$gt": ["$Numero de huevos", 0] },
                                    { "$gt": ["$Numero Larvas", 0] },
                                    { "$gt": ["$Numero Ninfas", 0] },
                                    { "$gt": ["$Numero Pupas", 0] },
                                    { "$gt": ["$Numero Adultos", 0] }
                                ]
                            }
                        ]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "tiene_plaga": "$tiene_plaga"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
            },
            "count_afectados": { "$sum": "$_id.tiene_plaga" },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "arboles_afectados_x_lote": "$count_afectados" }
                ]
            }
        }
    },


    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "plaga": "$PLAGA",
                "tiene_plaga": "$tiene_plaga"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "plaga": "$_id.plaga"
            },
            "count_segun_plaga": { "$sum": "$_id.tiene_plaga" },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "arboles_segun_plaga_x_lote": "$count_segun_plaga" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "individuos_arr": [
                "$Numero de huevos",
                "$Numero Larvas",
                "$Numero Ninfas",
                "$Numero Pupas",
                "$Numero Adultos"
            ]
        }
    },
    {
        "$addFields": {
            "num_individuos": {
                "$reduce": {
                    "input": "$individuos_arr",
                    "initialValue": 0,
                    "in": { "$add": ["$$value", "$$this"] }
                }
            }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "plaga": "$PLAGA",
                "num_individuos": "$num_individuos"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "plaga": "$_id.plaga"
            },
            "count_individuos": { "$sum": "$_id.num_individuos" },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "total_individuos_x_lote": "$count_individuos" }
                ]
            }
        }
    },


    {
        "$addFields": {
            "incidencia": {
                "$cond": {
                    "if": { "$gt": ["$arboles_censados_x_lote", 0] },
                    "then": {
                        "$multiply": [
                            {
                                "$divide": [
                                    "$arboles_afectados_x_lote",
                                    "$arboles_censados_x_lote"
                                ]
                            },
                            100
                        ]
                    },
                    "else": 0
                }
            },

            "incidencia_segun_plago": {
                "$cond": {
                    "if": { "$gt": ["$arboles_censados_x_lote", 0] },
                    "then": {
                        "$multiply": [
                            {
                                "$divide": [
                                    "$arboles_segun_plaga_x_lote",
                                    "$arboles_censados_x_lote"
                                ]
                            },
                            100
                        ]
                    },
                    "else": 0
                }
            },

            "severidad": {
                "$cond": {
                    "if": { "$gt": ["$arboles_segun_plaga_x_lote", 0] },
                    "then": {
                        "$multiply": [
                            {
                                "$divide": [
                                    "$total_individuos_x_lote",
                                    "$arboles_segun_plaga_x_lote"
                                ]
                            },
                            100
                        ]
                    },
                    "else": 0
                }
            }
        }
    },



    {
        "$project": {
            "tiene_plaga": 0,
            "individuos_arr": 0,
            "num_mes": 0
        }
    }




    , {
        "$addFields": {
            "variable_label": { "$concat": ["$finca", "_", "$lote"] }

            , "variable_dataset": "$PLAGA"
        }
    }



    , {
        "$group": {
            "_id": {
                "dashboard_label": "$variable_label"
                , "dashboard_dataset": "$variable_dataset"
            }
            , "dashboard_cantidad": { "$min": "$incidencia_segun_plago" }

        }
    }

    , {
        "$group": {
            "_id": {
                "dashboard_dataset": "$_id.dashboard_dataset"
            }
            , "data_group": { "$push": "$$ROOT" }

        }
    }

    , {
        "$sort": {
            "_id.dashboard_dataset": 1
        }
    }


    , {
        "$group": {
            "_id": null
            , "data_group": { "$push": "$$ROOT" }
            , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
        }
    }

    , { "$unwind": "$data_group" }

    , { "$unwind": "$data_group.data_group" }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_group.data_group",
                    {
                        "array_dashboard_dataset": "$array_dashboard_dataset"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "dashboard_label": "$_id.dashboard_label"
            }
            , "data_group": { "$push": "$$ROOT" }
        }
    }
    , {
        "$sort": {
            "_id.dashboard_label": 1
        }
    }


    , {
        "$group": {
            "_id": null
            , "dashboard_data": {
                "$push": "$$ROOT"
            }
        }
    }


    , {
        "$addFields": {
            "datos_dashboard": {
                "$map": {
                    "input": "$dashboard_data",
                    "as": "item_dashboard_data",
                    "in": {
                        "$reduce": {
                            "input": "$$item_dashboard_data.data_group",
                            "initialValue": [],
                            "in": {
                                "$concatArrays": [
                                    "$$value",
                                    [
                                        {
                                            "dashboard_label": "$$this._id.dashboard_label",
                                            "dashboard_dataset": "$$this._id.dashboard_dataset",
                                            "dashboard_cantidad": "$$this.dashboard_cantidad"
                                        }
                                    ]
                                ]
                            }
                        }
                    }

                }
            }
        }
    }


    , {
        "$addFields": {
            "datos_dashboard": {
                "$reduce": {
                    "input": "$datos_dashboard",
                    "initialValue": [],
                    "in": {
                        "$concatArrays": [
                            "$$value",
                            "$$this"
                        ]
                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
        }
    }


    , {
        "$addFields": {
            "info_datasets": {
                "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
            }
        }
    }

    , {
        "$addFields": {
            "array_colores": [
                "#008000",
                "#ffff00",
                "#0000ff",
                "#ff0000",
                "#ffa500",
                "#ee82ee",

                "#63b598",
                "#ce7d78",
                "#ea9e70",
                "#a48a9e",
                "#c6e1e8",
                "#648177",
                "#0d5ac1",
                "#f205e6",
                "#1c0365",
                "#14a9ad",
                "#4ca2f9",
                "#a4e43f",
                "#d298e2",
                "#6119d0",
                "#d2737d",
                "#c0a43c",
                "#f2510e",
                "#651be6",
                "#79806e",
                "#61da5e",
                "#cd2f00",
                "#9348af",
                "#01ac53",
                "#c5a4fb",
                "#996635",
                "#b11573",
                "#4bb473",
                "#75d89e",
                "#2f3f94",
                "#2f7b99",
                "#da967d",
                "#34891f",
                "#b0d87b",
                "#ca4751",
                "#7e50a8",
                "#c4d647",
                "#e0eeb8",
                "#11dec1",
                "#289812",
                "#566ca0",
                "#ffdbe1",
                "#2f1179",
                "#935b6d",
                "#916988",
                "#513d98",
                "#aead3a",
                "#9e6d71",
                "#4b5bdc",
                "#0cd36d",
                "#250662",
                "#cb5bea",
                "#228916",
                "#ac3e1b",
                "#df514a",
                "#539397",
                "#880977",
                "#f697c1",
                "#ba96ce",
                "#679c9d",
                "#c6c42c",
                "#5d2c52",
                "#48b41b",
                "#e1cf3b",
                "#5be4f0",
                "#57c4d8",
                "#a4d17a",
                "#be608b",
                "#96b00c",
                "#088baf",
                "#f158bf",
                "#e145ba",
                "#ee91e3",
                "#05d371",
                "#5426e0",
                "#4834d0",
                "#802234",
                "#6749e8",
                "#0971f0",
                "#8fb413",
                "#b2b4f0",
                "#c3c89d",
                "#c9a941",
                "#41d158",
                "#fb21a3",
                "#51aed9",
                "#5bb32d",
                "#21538e",
                "#89d534",
                "#d36647",
                "#7fb411",
                "#0023b8",
                "#3b8c2a",
                "#986b53",
                "#f50422",
                "#983f7a",
                "#ea24a3",
                "#79352c",
                "#521250",
                "#c79ed2",
                "#d6dd92",
                "#e33e52",
                "#b2be57",
                "#fa06ec",
                "#1bb699",
                "#6b2e5f",
                "#64820f",
                "#21538e",
                "#89d534",
                "#d36647",
                "#7fb411",
                "#0023b8",
                "#3b8c2a",
                "#986b53",
                "#f50422",
                "#983f7a",
                "#ea24a3",
                "#79352c",
                "#521250",
                "#c79ed2",
                "#d6dd92",
                "#e33e52",
                "#b2be57",
                "#fa06ec",
                "#1bb699",
                "#6b2e5f",
                "#64820f",
                "#9cb64a",
                "#996c48",
                "#9ab9b7",
                "#06e052",
                "#e3a481",
                "#0eb621",
                "#fc458e",
                "#b2db15",
                "#aa226d",
                "#792ed8",
                "#73872a",
                "#520d3a",
                "#cefcb8",
                "#a5b3d9",
                "#7d1d85",
                "#c4fd57",
                "#f1ae16",
                "#8fe22a",
                "#ef6e3c",
                "#243eeb",
                "#dd93fd",
                "#3f8473",
                "#e7dbce",
                "#421f79",
                "#7a3d93",
                "#635f6d",
                "#93f2d7",
                "#9b5c2a",
                "#15b9ee",
                "#0f5997",
                "#409188",
                "#911e20",
                "#1350ce",
                "#10e5b1",
                "#fff4d7",
                "#cb2582",
                "#ce00be",
                "#32d5d6",
                "#608572",
                "#c79bc2",
                "#00f87c",
                "#77772a",
                "#6995ba",
                "#fc6b57",
                "#f07815",
                "#8fd883",
                "#060e27",
                "#96e591",
                "#21d52e",
                "#d00043",
                "#b47162",
                "#1ec227",
                "#4f0f6f",
                "#1d1d58",
                "#947002",
                "#bde052",
                "#e08c56",
                "#28fcfd",
                "#36486a",
                "#d02e29",
                "#1ae6db",
                "#3e464c",
                "#a84a8f",
                "#911e7e",
                "#3f16d9",
                "#0f525f",
                "#ac7c0a",
                "#b4c086",
                "#c9d730",
                "#30cc49",
                "#3d6751",
                "#fb4c03",
                "#640fc1",
                "#62c03e",
                "#d3493a",
                "#88aa0b",
                "#406df9",
                "#615af0",
                "#2a3434",
                "#4a543f",
                "#79bca0",
                "#a8b8d4",
                "#00efd4",
                "#7ad236",
                "#7260d8",
                "#1deaa7",
                "#06f43a",
                "#823c59",
                "#e3d94c",
                "#dc1c06",
                "#f53b2a",
                "#b46238",
                "#2dfff6",
                "#a82b89",
                "#1a8011",
                "#436a9f",
                "#1a806a",
                "#4cf09d",
                "#c188a2",
                "#67eb4b",
                "#b308d3",
                "#fc7e41",
                "#af3101",
                "#71b1f4",
                "#a2f8a5",
                "#e23dd0",
                "#d3486d",
                "#00f7f9",
                "#474893",
                "#3cec35",
                "#1c65cb",
                "#5d1d0c",
                "#2d7d2a",
                "#ff3420",
                "#5cdd87",
                "#a259a4",
                "#e4ac44",
                "#1bede6",
                "#8798a4",
                "#d7790f",
                "#b2c24f",
                "#de73c2",
                "#d70a9c",
                "#88e9b8",
                "#c2b0e2",
                "#86e98f",
                "#ae90e2",
                "#1a806b",
                "#436a9e",
                "#0ec0ff",
                "#f812b3",
                "#b17fc9",
                "#8d6c2f",
                "#d3277a",
                "#2ca1ae",
                "#9685eb",
                "#8a96c6",
                "#dba2e6",
                "#76fc1b",
                "#608fa4",
                "#20f6ba",
                "#07d7f6",
                "#dce77a",
                "#77ecca"
            ]
        }
    }




    , {
        "$addFields": {
            "DATA_ARRAY_DATASETS": {
                "$map": {
                    "input": "$info_datasets",
                    "as": "item_info_datasets",
                    "in": {
                        "label": "$$item_info_datasets",
                        "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
                        "borderColor": "#000000",
                        "borderWidth": 1,
                        "data": {
                            "$map": {
                                "input": "$DATA_LABELS",
                                "as": "item_data_labels",
                                "in": {
                                    "$reduce": {
                                        "input": "$datos_dashboard",
                                        "initialValue": 0,
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [
                                                        { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
                                                        , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
                                                    ]

                                                },
                                                "then": "$$this.dashboard_cantidad",
                                                "else": "$$value"
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
    }




    , { "$project": { "_id": 0 } }


    , {
        "$project": {

            "data": {
                "labels": "$DATA_LABELS",
                "datasets": "$DATA_ARRAY_DATASETS"
            },
            "options": {
                "title": {
                    "display": true,
                    "text": "Chart.js Bar Chart"
                }
            }

        }
    }





]
