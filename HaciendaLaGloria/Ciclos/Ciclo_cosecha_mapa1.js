//--mapa
db.form_recoleccioncosecha.aggregate(
    [

        // //-----
        { $unwind: "$Lote.features" },
        {
            "$addFields": {
                "today": new Date,
                "idform": "123",
                "Cartography": "$Lote.features"
            }
        },

        {
            "$sort": {
                "Fecha corte": -1
            }
        },
        {
            "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }
        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        //---obtener ultimo
        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        },


        // , {
        //     "$addFields": {
        //         "estado de ciclo": {
        //             "$reduce": {
        //                 "input": "$data.Ciclo cosecha",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "fecha inicio de ciclo": {
        //             "$reduce": {
        //                 "input": "$data.Fecha ciclo",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "rgDate": {
        //             "$reduce": {
        //                 "input": "$data.Fecha ciclo",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "Point": {
        //             "$reduce": {
        //                 "input": "$data.Point",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "penultimo_censo": {
        //             "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "fecha finalizacion de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "finaliza"] },
        //                 "then": "$fecha inicio de ciclo",
        //                 "else": ""
        //             }
        //         }

        //     }
        // },

        // {
        //     "$addFields": {
        //         "fecha inicio de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "inicia"] },
        //                 "then": "$fecha inicio de ciclo",
        //                 "else": "$penultimo_censo.Fecha ciclo"
        //             }
        //         }

        //     }
        // },


        // {
        //     "$addFields": {
        //         "dias de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "inicia"] },
        //                 "then": -1,
        //                 "else": {
        //                     "$floor": {
        //                         "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$floor": {
                        //"$divide": [{ "$subtract": ["$_id.today", "$data.rgDate"] }, 86400000]
                        "$divide": [{ "$subtract": ["$_id.today", "$data.Fecha corte"] }, 86400000]
                    }
                }
            }
        },



        //====COLOR
        //---nueva (2020-06-03)
        //En proceso: #666666, Ciclo cosecha de 0 a 9 dias: #2570A9, Ciclo cosecha de 10 a 12 dias: #9ACD32, Ciclo cosecha de 13 a 15 dias: #FFFD1F, Ciclo cosecha >= 16 dias: #E84C3F

        {
            "$addFields": {
                // "estado de ciclo": {
                //     "$cond": {
                //         "if": { "$eq": ["$estado de ciclo", "inicia"] },
                //         "then": "En proceso",
                //         "else": "Finalizado"
                //     }
                // },
                "color": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "#666666",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "#2570A9",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                        },
                                        "then": "#9ACD32",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "#FFFD1F",
                                                "else": "#E84C3F"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "A-En proceso",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "B-Ciclo cosecha de 0 a 9 dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                        },
                                        "then": "C-Ciclo cosecha de 10 a 12 dias",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "D-Ciclo cosecha de 13 a 15 dias",
                                                "else": "E-Ciclo cosecha >= 16 dias"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            "$project": {
                // "_id": {"$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]},
                "_id": "$data.elemnq",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_lote",
                    // "Estado del Ciclo": "$estado de ciclo",
                    "Rango": "$rango",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                //"geometry": {"$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]}
                "geometry": "$data.Cartography.features.geometry"
            }
        }
    ]
)