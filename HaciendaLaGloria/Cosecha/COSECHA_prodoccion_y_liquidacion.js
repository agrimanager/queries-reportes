db.form_recoleccioncosecha.aggregate(

    [


        { "$addFields": { "anio_filtro": { "$year": "$Fecha corte" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        //---info fechas

        {
            "$addFields": {
                "num_anio": { "$year": { "date": "$Fecha corte" } },
                "num_mes": { "$month": { "date": "$Fecha corte" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$Fecha corte" } },
                "num_semana": { "$week": { "date": "$Fecha corte" } },
                "num_dia_semana": { "$dayOfWeek": { "date": "$Fecha corte" } }
            }
        }

        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha corte" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


                , "Dia_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        },

        //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        { "$project": { "num_dia_semana": 0 } },





        { "$match": { "Lote.path": { "$ne": "" } } },

        //----cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        { "$unwind": "$Finca" }


        , {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        }

        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0

                //---aux
                , "Lote": 0
                , "Point": 0
            }
        }



        //----info lote
        , {
            "$lookup": {
                "from": "form_informaciondelotes",
                "as": "info_lotes",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$nombre_lote", "$Lote"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lotes",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de Siembra", 0] },
                "Hectareas": { "$ifNull": ["$info_lotes.Area Total", 0] },
                "Plantas_x_lote": { "$ifNull": ["$info_lotes.Numero de Plantas", 0] },
                "Variedad": { "$ifNull": ["$info_lotes.Variedad", ""] },
                "Tarifa Kg por siembra": { "$ifNull": ["$info_lotes.Tarifa Kg por siembra", 0] },

                "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lotes": 0
            }
        }




        //----empleados
        , { "$unwind": "$Empleado" }


        , {
            "$addFields": {
                "oid_empleado": { "$toObjectId": "$Empleado._id" },

                "empleado_cargo": "$Empleado.reference",
                "empleado_valor": "$Empleado.value"
            }
        }


        , {
            "$lookup": {
                "from": "employees",
                "localField": "oid_empleado",
                "foreignField": "_id",
                "as": "info_empleado"
            }
        }

        , { "$unwind": "$info_empleado" }


        , {
            "$addFields": {
                "empleado_codigo": "$info_empleado.code",
                "empleado_nombre": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                "empleado_identificacion": "$info_empleado.numberID"
            }
        }


        , {
            "$project": {
                "oid_empleado": 0,
                "info_empleado": 0
            }
        }

        , {
            "$addFields": {
                "total_racimos_alzados": {
                    "$cond": {
                        "if": {
                            "$eq": ["$empleado_cargo", "Bufalero"]
                        },
                        "then": "$empleado_valor",
                        "else": 0
                    }
                }
            }
        }

        // //-----peso aproximado

        , {
            "$addFields":
            {
                "Peso aproximado Alzados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$total_racimos_alzados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                    ]
                }
            }
        }


        , {
            "$addFields":
            {
                "Numero de envio": { "$ifNull": [{ "$toDouble": "$Numero de envio" }, -1] }
            }
        }




        //---despacho de cosecha
        , {
            "$lookup": {
                "from": "form_despachodefruta",
                "as": "despacho_cosecha",
                "let": {
                    "num_envio": "$Numero de envio",
                    "fecha": "$Fecha corte"
                },
                "pipeline": [

                    {
                        "$match": {
                            "Numero de envio": { "$exists": true }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Numero de envio", "$$num_envio"] },
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de corte" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de corte" } } }
                                        ]
                                    }


                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": 1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$despacho_cosecha",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields":
            {
                //5) PESO EXTRACTORA (de despacho)
                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Peso segun Extractora" }, -1] },
                "Numero de remision": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Numero de remision" }, -1] },
                "Numero de ticket": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.N ticket extractora" }, -1] }
            }
        }

        , {
            "$project": {
                "despacho_cosecha": 0
                , "Empleado": 0
            }
        }




        // //-----peso real lote
        , {
            "$group": {
                "_id": {
                    "ticket": "$Numero de ticket",
                    "num_envio": "$Numero de envio",
                    "lote": "$lote"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                //1) racimos alzados x lote
                "total_racimos_alzados_viaje_lote": {
                    "$sum": "$total_racimos_alzados"
                },
                //2) peso promedio alzados x lote
                "total_peso_aproximado_racimos_alzados_viaje_lote": {
                    "$sum": "$Peso aproximado Alzados"
                }
            }
        }
        , {
            "$group": {
                "_id": {
                    "ticket": "$_id.ticket",
                    "num_envio": "$_id.num_envio"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                //3) peso promedio alzados x ENVIO {SUM(2)}
                "total_peso_aproximado_racimos_alzados_viaje": {
                    "$sum": "$total_peso_aproximado_racimos_alzados_viaje_lote"
                },
                "total_racimos_alzados_viaje": {
                    "$sum": "$total_racimos_alzados_viaje_lote"
                }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$addFields":
            {
                "total_peso_aproximado_racimos_alzados_viaje_lote": "$data.total_peso_aproximado_racimos_alzados_viaje_lote",
                "total_racimos_alzados_viaje_lote": "$data.total_racimos_alzados_viaje_lote"
            }
        },


        {
            "$unwind": "$data.data"
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            //1) racimos alzados x lote
                            "total_racimos_alzados_viaje_lote": "$total_racimos_alzados_viaje_lote",

                            //2) peso promedio alzados x lote
                            "total_peso_aproximado_racimos_alzados_viaje_lote": "$total_peso_aproximado_racimos_alzados_viaje_lote",

                            //3) peso promedio alzados x ENVIO {SUM(2)}
                            "total_peso_aproximado_racimos_alzados_viaje": "$total_peso_aproximado_racimos_alzados_viaje"
                            //,"total_racimos_alzados_viaje": "$total_racimos_alzados_viaje" // no se necesita


                            //4) %alzados x lote {(2) / (3)}
                            , "(%) Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_peso_aproximado_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$total_peso_aproximado_racimos_alzados_viaje_lote", "$total_peso_aproximado_racimos_alzados_viaje"]
                                    }
                                }
                            }

                            //5) PESO EXTRACTORA (de despacho)
                            , "Total Peso ticket": "$data.data.Total Peso despachados (ticket)"
                        }
                    ]
                }
            }
        }

        , { "$project": { "Total Peso despachados (ticket)": 0 } }



        , {
            "$addFields":
            {
                //6) PESO REAL ALZADOS LOTE = { (5) * (4) }
                "total_peso_REAL_racimos_alzados_viaje_lote": {
                    "$multiply": ["$Total Peso ticket", "$(%) Alzados x lote"]
                }
            }
        }


        , {
            "$addFields":
            {
                //7) PESO REAL RACIMO LOTE = { (6) / (1) }
                "Peso REAL lote": {
                    "$divide": ["$total_peso_REAL_racimos_alzados_viaje_lote", "$total_racimos_alzados_viaje_lote"]
                }
            }
        }
        
        
        
        //---para liquidacion
        , {
            "$addFields":{
                //peso_real * cantidad_racimos_empleado
                "empleado_kg_real": {
                    "$multiply": ["$Peso REAL lote", "$empleado_valor"]
                }
            }
        }
        
        , {
            "$addFields":{
                //empleado_kg_real * tarifa_kg_lote
                "empleado_pago": {
                    "$multiply": ["$empleado_kg_real", "$Tarifa Kg por siembra"]
                }
            }
        }
        
        
        
        //---toneladas alzadas
        //
        , {
            "$addFields":{
                "Kg REAL Alzados": {
                    "$multiply": ["$total_racimos_alzados", "$Peso REAL lote"]
                }
            }
        }
        
        , {
            "$addFields":{
                "Ton REAL Alzados": {
                    "$divide": ["$Kg REAL Alzados", 1000]
                }
            }
        }
        



    ]
)

    .sort({ "Numero de envio": -1 })

