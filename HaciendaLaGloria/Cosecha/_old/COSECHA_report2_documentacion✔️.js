[


    { "$addFields": { "anio_filtro": { "$year": "$Fecha corte" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    { "$match": { "Lote.path": { "$ne": "" } } },

    
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" }


    , {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    }

    , {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "Formula": 0

            
            , "Lote": 0
            , "Point": 0
        }
    }



    
    , {
        "$lookup": {
            "from": "form_informaciondelotes",
            "as": "info_lotes",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$nombre_lote", "$Lote"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$info_lotes",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de Siembra", 0] },
            "Hectareas": { "$ifNull": ["$info_lotes.Area Total", 0] },
            "Plantas_x_lote": { "$ifNull": ["$info_lotes.Numero de Plantas", 0] },
            "Variedad": { "$ifNull": ["$info_lotes.Variedad", ""] },
            "Tarifa Kg por siembra": { "$ifNull": ["$info_lotes.Tarifa Kg por siembra", 0] },

            "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso Promedio", 0] }
        }
    }

    , {
        "$project": {
            "info_lotes": 0
        }
    }




    
    , { "$unwind": "$Empleado" }


    , {
        "$addFields": {
            "oid_empleado": { "$toObjectId": "$Empleado._id" },

            "empleado_cargo": "$Empleado.reference",
            "empleado_valor": "$Empleado.value"
        }
    }


    , {
        "$lookup": {
            "from": "employees",
            "localField": "oid_empleado",
            "foreignField": "_id",
            "as": "info_empleado"
        }
    }

    , { "$unwind": "$info_empleado" }


    , {
        "$addFields": {
            "empleado_codigo": "$info_empleado.code",
            "empleado_nombre": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
            "empleado_identificacion": "$info_empleado.numberID"
        }
    }


    , {
        "$project": {
            "oid_empleado": 0,
            "info_empleado": 0
        }
    }

    , {
        "$addFields": {
            "total_racimos_alzados": {
                "$cond": {
                    "if": {
                        "$eq": ["$empleado_cargo", "Bufalero"]
                    },
                    "then": "$empleado_valor",
                    "else": 0
                }
            }
        }
    }

    

    , {
        "$addFields":
        {
            "Peso aproximado Alzados": {
                "$multiply": [
                    { "$ifNull": [{ "$toDouble": "$total_racimos_alzados" }, 0] },
                    { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                ]
            }
        }
    }


    , {
        "$addFields":
        {
            "Numero de envio": { "$ifNull": [{ "$toDouble": "$Numero de envio" }, -1] }
        }
    }




    
    , {
        "$lookup": {
            "from": "form_despachodefruta",
            "as": "despacho_cosecha",
            "let": {
                "num_envio": "$Numero de envio",
                "fecha": "$Fecha corte"
            },
            "pipeline": [

                {
                    "$match": {
                        "Numero de envio": { "$exists": true }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Numero de envio", "$$num_envio"] },
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de corte" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de corte" } } }
                                    ]
                                }


                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": 1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$despacho_cosecha",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields":
        {
            "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Peso segun Extractora" }, -1] },
            "Numero de remision": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Numero de remision" }, -1] },
            "Numero de ticket": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.N ticket extractora" }, -1] }
        }
    }

    , {
        "$project": {
            "despacho_cosecha": 0
            ,"Empleado":0
        }
    }




    
    , {
        "$group": {
            "_id": {
                "ticket": "$Numero de ticket",
                "num_envio": "$Numero de envio",
                "lote": "$lote"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_racimos_alzados_viaje_lote": {
                "$sum": "$total_racimos_alzados"
            },
            "total_peso_aproximado_racimos_alzados_viaje_lote": {
                "$sum": "$Peso aproximado Alzados"
            }
        }
    }
    , {
        "$group": {
            "_id": {
                "ticket": "$_id.ticket",
                "num_envio": "$_id.num_envio"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_peso_aproximado_racimos_alzados_viaje": {
                "$sum": "$total_peso_aproximado_racimos_alzados_viaje_lote"
            },
            "total_racimos_alzados_viaje": {
                "$sum": "$total_racimos_alzados_viaje_lote"
            }
        }
    }

    , { "$unwind": "$data" }

    , {
        "$addFields":
        {
            "total_peso_aproximado_racimos_alzados_viaje_lote": "$data.total_peso_aproximado_racimos_alzados_viaje_lote",
            "total_racimos_alzados_viaje_lote": "$data.total_racimos_alzados_viaje_lote"
        }
    },


    {
        "$unwind": "$data.data"
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        
                        "total_racimos_alzados_viaje_lote": "$total_racimos_alzados_viaje_lote",
                        
                        
                        "total_peso_aproximado_racimos_alzados_viaje_lote": "$total_peso_aproximado_racimos_alzados_viaje_lote",

                        
                        "total_peso_aproximado_racimos_alzados_viaje": "$total_peso_aproximado_racimos_alzados_viaje"
                        


                        
                        , "(%) Alzados x lote": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$total_peso_aproximado_racimos_alzados_viaje", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": ["$total_peso_aproximado_racimos_alzados_viaje_lote", "$total_peso_aproximado_racimos_alzados_viaje"]
                                }
                            }
                        }

                        
                        , "Total Peso ticket": "$data.data.Total Peso despachados (ticket)"
                    }
                ]
            }
        }
    }

    , { "$project": { "Total Peso despachados (ticket)": 0 } }
    
    
    
    ,{
        "$addFields":
        {
            
            "total_peso_REAL_racimos_alzados_viaje_lote":{
                "$multiply":["$Total Peso ticket","$(%) Alzados x lote"]
            }
        }
    }
    
    
    ,{
        "$addFields":
        {
            
            "Peso REAL lote":{
                "$divide":["$total_peso_REAL_racimos_alzados_viaje_lote","$total_racimos_alzados_viaje_lote"]
            }
        }
    }



]