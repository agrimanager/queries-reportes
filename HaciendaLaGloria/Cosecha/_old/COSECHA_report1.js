db.form_recoleccioncosecha.aggregate(

    [


        // { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
        // { "$match": { "anio_filtro": { "$gt": 2000 } } },
        // { "$match": { "anio_filtro": { "$lt": 3000 } } },


        // {"$match": {"Lote.path": { "$ne": "" }}},

        //----cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        { "$unwind": "$Finca" }


        , {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        }

        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0

                //---aux
                , "Lote": 0
                , "Point": 0
            }
        }



        //----info lote
        , {
            "$lookup": {
                "from": "form_informaciondelotes",
                "as": "info_lotes",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$nombre_lote", "$Lote"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lotes",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de Siembra", 0] },
                "Hectareas": { "$ifNull": ["$info_lotes.Area Total", 0] },
                "Plantas_x_lote": { "$ifNull": ["$info_lotes.Numero de Plantas", 0] },
                "Variedad": { "$ifNull": ["$info_lotes.Variedad", ""] },
                "Tarifa Kg por siembra": { "$ifNull": ["$info_lotes.Tarifa Kg por siembra", 0] },

                "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lotes": 0
            }
        }




        //----empleados
        , { "$unwind": "$Empleado" }


        , {
            "$addFields": {
                "oid_empleado": { "$toObjectId": "$Empleado._id" },

                "empleado_cargo": "$Empleado.reference",
                "empleado_valor": "$Empleado.value"
            }
        }


        , {
            "$lookup": {
                "from": "employees",
                "localField": "oid_empleado",
                "foreignField": "_id",
                "as": "info_empleado"
            }
        }

        , { "$unwind": "$info_empleado" }


        , {
            "$addFields": {
                "empleado_codigo": "$info_empleado.code",
                "empleado_nombre": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                "empleado_identificacion": "$info_empleado.numberID"
            }
        }


        , {
            "$project": {
                "oid_empleado": 0,
                "info_empleado": 0
            }
        }





    ]
)