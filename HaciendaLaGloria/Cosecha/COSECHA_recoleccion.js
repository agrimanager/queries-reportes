db.form_recoleccioncosecha.aggregate(

    [


        { "$addFields": { "anio_filtro": { "$year": "$Fecha corte" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },


        //---info fechas

        {
            "$addFields": {
                "num_anio": { "$year": { "date": "$Fecha corte" } },
                "num_mes": { "$month": { "date": "$Fecha corte" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$Fecha corte" } },
                "num_semana": { "$week": { "date": "$Fecha corte" } },
                "num_dia_semana": { "$dayOfWeek": { "date": "$Fecha corte" } }
            }
        }

        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha corte" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


                , "Dia_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        },

        //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        { "$project": { "num_dia_semana": 0 } },





        { "$match": { "Lote.path": { "$ne": "" } } },

        //----cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        { "$unwind": "$Finca" }


        , {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        }

        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0

                //---aux
                , "Lote": 0
                , "Point": 0
            }
        }



        //----info lote
        , {
            "$lookup": {
                "from": "form_informaciondelotes",
                "as": "info_lotes",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$nombre_lote", "$Lote"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lotes",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de Siembra", 0] },
                "Hectareas": { "$ifNull": ["$info_lotes.Area Total", 0] },
                "Plantas_x_lote": { "$ifNull": ["$info_lotes.Numero de Plantas", 0] },
                "Variedad": { "$ifNull": ["$info_lotes.Variedad", ""] },
                "Tarifa Kg por siembra": { "$ifNull": ["$info_lotes.Tarifa Kg por siembra", 0] },

                "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso Promedio", 0] }
            }
        }

        , {
            "$project": {
                "info_lotes": 0
            }
        }

        
        //Empleados full text
        , {
            "$addFields": {
                "Empleado": {
                    "$reduce": {
                        "input": "$Empleado.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": {
                                    "$eq": [
                                        {
                                            "$strLenCP": "$$value"
                                        },
                                        0
                                    ]
                                },
                                "then": "$$this",
                                "else": {
                                    "$concat": [
                                        "$$value",
                                        "; ",
                                        "$$this"
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }

        



    ]
)

    // .sort({ "Numero de envio": -1 })

