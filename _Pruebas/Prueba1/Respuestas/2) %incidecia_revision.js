db.form_enfermedadesriopaila.aggregate(
    [
    {
        "$addFields": {
            "split_path": {
                "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_o_id": {
                "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } }
            }
        }
    },
    {
        "$addFields": {
            "palma_o_id": {
                "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } }
            }
        }
    },
    {
        "$addFields": {
            "split_path_o_id": {
                "$concatArrays": [
                    "$split_path_o_id",
                    "$palma_o_id"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_o_id",
            "foreignField": "_id",
            "as": "jerarquia"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$jerarquia", 0] },
            "Bloque": { "$arrayElemAt": ["$jerarquia", 1] },
            "Lote": { "$arrayElemAt": ["$jerarquia", 2] },
            "Linea": { "$arrayElemAt": ["$jerarquia", 3] },
            "Planta": { "$arrayElemAt": ["$jerarquia", 4] }
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }  
    },
    {
        "$addFields": {
            "Finca": "$Finca.name",
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_o_id": 0,
            "palma_o_id": 0,
            "jerarquia": 0
        }
    },
    
    
    {
        $match:{
            Lote : "1-2"
        }
    },
    
    
    {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "planta": "$Planta"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            },
            "Suma_total_de_plantas_por_lote": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "Suma_total_de_plantas_por_lote": "$Suma_total_de_plantas_por_lote" }
                ]
            }
        }
    },
    {
        "$group": {
            "_id": {
                "enfermedad": "$Enfermedad",
                "lote": "$Lote",
                "planta": "$Planta"
            },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": {
                "enfermedad": "$_id.enfermedad",
                "lote": "$_id.lote"
            },
            "Suma_de_plantas_diferente_por_lote": { "$sum": 1 },
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "Suma_de_plantas_diferente_por_lote": "$Suma_de_plantas_diferente_por_lote" }
                ]
            }
        }
    },
    {
        "$addFields": {
            "%incidencia": {
                "$multiply": [{ "$divide": ["$Suma_de_plantas_diferente_por_lote", "$Suma_total_de_plantas_por_lote"] }, 100]
            }
        }
    }
]


)