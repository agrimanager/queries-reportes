db.cartography.aggregate(
    //pentagono: 5dbc34ad6c936a55ed5627be

    //--finca
    {
        $match: {
            path: {
                $regex: `^,5dbc34ad6c936a55ed5627be,` ////----FINCA
            }
        }
    }

    //---nivel PATH
    ,{
        $addFields: {
            "split_path": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    }
    ,{
        $addFields: {
            "split_path_size": { "$size":"$split_path"}
        }
    }


    //----LOGICA DE NIVELES

    // //---lineas (split_path debe tener 3 ids)
    // ,{
    //     $match: {
    //         "properties.type":"lines"
    //         ,"split_path_size":{$ne:3}
    //     }
    // }

    //---arboles (split_path debe tener 4 ids)
    ,{
        $match: {
            "properties.type":"trees"
            ,"split_path_size":{$ne:4}
        }
    }




    //---mostrar
    ,{
        $project:{
            geometry:0
            ,type:0
        }
    }

)
