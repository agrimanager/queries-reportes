var data = db.form_modulodeenfermedades.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-26T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------


        {
            $match: {
                "Enfermedad": {
                    $in: [
                        "Muerta por Rayo",
                        "P Basal seca",
                        "P Basal Humeda",
                        "Flecha seca"
                    ]
                }
            }
        }


        , {
            $addFields: {
                "alias_propiedad": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$eq": ["$Enfermedad", "Muerta por Rayo"]
                                },
                                "then": "Erradicada Rayo"
                            },
                            {
                                "case": {
                                    "$eq": ["$Enfermedad", "P Basal seca"]
                                },
                                "then": "Erradicada Pudrucion Estipite Seco"
                            },
                            {
                                "case": {
                                    "$eq": ["$Enfermedad", "P Basal Humeda"]
                                },
                                "then": "Erradicada Pudricion Estipite Humedo"
                            },
                            {
                                "case": {
                                    "$eq": ["$Enfermedad", "Flecha seca"]
                                },
                                "then": "Erradicada Flecha Seca"
                            }
                        ],
                        "default": ""
                    }
                }
            }
        }



        , {
            $unwind: "$Arbol.features"
        }


        , {
            $addFields: {
                "arbol_id": { $toObjectId: "$Arbol.features._id" }
            }
        }



        , {
            $group: {
                _id: {
                    "arbol": "$arbol_id"
                }
                , data: { $push: "$$ROOT" }
                , fecha_max: { $max: "$rgDate" }
            }
        }



        , {
            "$addFields": {
                "data": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.rgDate", "$fecha_max"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        // ,{$unwind: "$data"}

        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }



        , {
            $group: {
                _id: {
                    "alias_propiedad": "$alias_propiedad"
                }
                , arboles: { $push: "$arbol_id" }
            }
        }


        , {
            "$addFields": {
                "variable_custom": {
                    "$concat": ["properties.custom.", "$_id.alias_propiedad"]
                }
            }
        }


    ]
)


// data

data.forEach(i => {


    //1) borrar propiedades old
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                "properties.custom": {}
            }
        }

    )



    //2) agregar propiedad new
    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {
                //new
                [i.variable_custom]: {
                    "type": "bool",
                    "value": true
                }


            }
        }

    )

})





/*
"trees" : [
		{
			"name" : "Erradicada Rayo",
			"type" : "bool",
			"color" : "#FFCC00"
		},
		{
			"name" : "Erradicada Pudrucion Estipite Seco",
			"type" : "bool",
			"color" : "#CC0001"
		},
		{
			"name" : "Erradicada Pudricion Estipite Humedo",
			"type" : "bool",
			"color" : "#C712DE"
		},
		{
			"name" : "Erradicada Flecha Seca",
			"type" : "bool",
			"color" : "#0026FF"
		},
		{
			"name" : "Planta Nectarifera",
			"type" : "bool",
			"color" : "#66FF00"
		}
	],


*/
