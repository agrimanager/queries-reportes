# Columnas para cada pantalla de carga masiva

## Carga masiva de productos

- category: categoria
- description: descripcion,
- pc: periodo de carencia,
- name: nombre,
- water: Si el producto usa agua o no,
- pr: periodo de reingreso,
- pc2: periodo de carencia 2,
- pr2: periodo de reingreso 2,
- sku: codigo sku,
- puc: cuenta contable,
- type: tipo de producto,
- certifications: certificaciones,
- inputmeasure: Unidad de medida,
- activeIngredients: Ingredientes activos

## Carga masiva de transacciones (Entrada)

- wid: Bodega
- tax: iva,
- name: nombre,
- type: tipo de transaccion (Ingresos),
- rgDate: Fecha de registro (Este campo no es requerido, si no se va utilizar es mejor ni colocar la columna),
- quantity: Cantidad,
- invoiceNumber: Numero de factura,
- inf: informacion adicional,
- invoiceDate: Fecha de factura,
- dueDate: Fecha de vencimiento,
- inputmeasure: Unidad de medida,
- subtotal: Sub-total

## Carga masiva de transacciones (Salida)

- wid: Bodega,
- quantity: cantidad,
- name: nombre,
- inf: informacion adicional,
- inputmeasure: Unidad de medida,
- lbid: id de la labor/tarea,
- lbcode: Codigo de la labor/tarea

## Carga masiva de empleados

- job: Cargo
- cid: Empresa asociada
- code: Codigo del empleado
- bank: Entidad Bancaria
- email: correo electronico
- salary: Salario
- lastName: Ultimo nombre
- cellphone: Numero de telefono
- firstName: Primer nombre
- periodicity: Periodicidad del salario
- homeAddress: Dirección residencial
- vacationDays: Dias de vacaciones
- contractType: Tipo de contrato
- paymentMethods: Metodo de pago
- numberID: Numero del documento
- typeID: Tipo de documento
- accountType: Tipo de cuenta bancaria
- hours: Horas por semana
- fids: Fincas
- birthDate: Fecha de nacimiento
- hireDate: Fecha de contratacion
- otherSalaryIncome: Otros ingregos salariales
- accountNumber: Numero de cuenta bancaria
- otherNonSalaryIncome: Otros ingregos no salariales
- onlineAccess: Tiene acceso a la plataforma
- transportationSubsidy: Subsidio de transporte
- contractTerminationDate: Fecha de terminacion del contrato
- receiveEmail: Si recibe email con sus colillas de pago
- employeesProtections: Protecciones del empleado

## Carga masiva de actividades (Creación)

- color: Color de la actividad
- cuc: Cuenca contable
- name: Nombre de la actividad
- ccid: Centro de costo o conjunto de actividades
- causes: causas de finalización
- productivity_measure: Unidad de medida
- productivity_price: Cantidad
- productivity_expected: Productividad esperada por persona

## Carga masiva de actividades (Actualización de precios)

- name: Nombre de la actividad
- amount: Cantidad
