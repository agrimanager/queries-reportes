// region diccionarios

// Tipos de features de cartografia en ingles
var dict_feature_types = {
    "blocks": "blocks",
    "lot": "lot",
    "lines": "lines",
    "trees": "trees",
    "fruitCenters": "fruitCenters",
    "samplingPolygons": "samplingPolygons",
    "valves": "valves",
    "drainages": "drainages",
    "sprinklers": "sprinklers",
    "irrigationNetworkOne": "irrigationNetworkOne",
    "irrigationNetworkTwo": "irrigationNetworkTwo",
    "irrigationNetworkThree": "irrigationNetworkThree",
    "traps": "traps",
    "lanes": "lanes",
    "woods": "woods",
    "sensors": "sensors",
    "cableways": "cableways",
    "buildings": "buildings",
    "waterBodies": "waterBodies",
    "additionalPolygons": "additionalPolygons"
};


// Traducir tipos de features de cartografia a ESPAÑOL
var dict_translate_feature_type_es = {
    "blocks": "Bloque",
    "lot": "Lotes",
    "lines": "Linea",
    "trees": "Árboles",
    "fruitCenters": "Centro frutero",
    "samplingPolygons": "Poligono de muestreo",
    "valves": "Valvula",
    "drainages": "Drenaje",
    "sprinklers": "Aspersors",
    "irrigationNetworkOne": "Red de riego uno",
    "irrigationNetworkTwo": "Red de riego dos",
    "irrigationNetworkThree": "Red de riego tres",
    "traps": "Trampa",
    "lanes": "Vías",
    "woods": "Bosque",
    "sensors": "Sensor",
    "cableways": "Cable vía",
    "buildings": "Edificio",
    "waterBodies": "Cuerpo de agua",
    "additionalPolygons": "Poligonos adicionales",
};


// Traducir tipos de unidades de labores a ESPAÑOL
var dict_translate_measure_type_units_es = {
    // unidades de cartografia
    "blocks": "Bloque",
    "lot": "Lotes",
    "lines": "Linea",
    "trees": "Árboles",
    "fruitCenters": "Centro frutero",
    "samplingPolygons": "Poligono de muestreo",
    "valves": "Valvula",
    "drainages": "Drenaje",
    "sprinklers": "Aspersors",
    "irrigationNetworkOne": "Red de riego uno",
    "irrigationNetworkTwo": "Red de riego dos",
    "irrigationNetworkThree": "Red de riego tres",
    "traps": "Trampa",
    "lanes": "Vías",
    "woods": "Bosque",
    "sensors": "Sensor",
    "cableways": "Cable vía",
    "buildings": "Edificio",
    "waterBodies": "Cuerpo de agua",
    "additionalPolygons": "Poligonos adicionales",
    // otras unidades
    //"farming units": "Unidades de cultivo (Ejemplo: Árboles)",
    "farming units": "Unidades de cultivo",
    "wages": "Jornales",
    "quantity": "Cantidades",
    "mts": "Metros",
    "km": "Kilometros",
    "cm": "Centimetros",
    "mile": "Millas",
    "yard": "Yardas",
    "foot": "Pies",
    "inch": "Pulgadas",
    // consumibles -- peso
    "kg": "Kilogramos",
    "gr": "Gramos",
    "mg": "Miligramos",
    "US/ton": "Toneladas estadounidenses",
    "us/ton": "Toneladas estadounidenses",
    "ton": "Toneladas",
    "oz": "Onzas",
    "lb": "Libras",
    // consumibles -- volumen y area
    "lts": "Litros",
    "US/galon": "Galones estadounidenses",
    "galon": "Galones",
    "cf": "Pies cúbicos",
    "ci": "Pulgadas cúbicas",
    "cuc": "Centimetros cúbicos",
    "cum": "Metros cúbicos",
    "packages": "Bultos",
    "bags": "Bolsas",
    "sacks": "sacks",
    "yemas": "Yemas",
    "bun": "Factura",
    "cargo": "Flete",
    "manege": "Picadero",
    // tiempo
    // herramientas y maquinaria
    "hr": "Hora",
    "qty": "Por cantidad",
    "hectares": "Hectáreas",
    "squares": "Cuadras",
    "dustbin": "Canecas",
    "bunch": "Racimos",
    "cubic-meter": "Metro cúbico",
    "metro-line": "Metro Lineal",
    "square-meter":"Metro cuadrado",
    "estaquilla":"estaquilla",

    "holes":"Hoyos",
    "rastas":"Rastas",
    "postes":"Postes"
};



// Traducir tipo de Contrato de empleado a ESPAÑOL
var dict_translate_contract_type_es = {
    "fixed": "Contrato fijo",
    "temporary": "Contrato temporal",
    "indefinite": "Contrato indefinido",
    "services": "Contrato por servicios",
};

// Traducir Periodicidad de salario de empleado a ESPAÑOL
var dict_translate_salary_periodicity_es = {
    "weekly": "Semanal",
    "biweekly": "Quincenal",
    "monthly": "Mensual",
    "fourteenth": "Catorcenal",
};


// Traducir Metodo de pago de empleado a ESPAÑOL
var dict_translate_payment_methods_es = {
    "cash": "Efectivo",
    "check": "Cheque",
    "direct deposit": "Deposito directo",
    "transference": "Transferencia",
    "payment card": "Tarjetas de pago",
};


// Traducir tipo de cuenta de empleado a ESPAÑOL
var dict_translate_account_type_es = {
    "current acount": "Cuenta corriente",
    "savings acount": "Cuenta de ahorros",
    "payroll acount": "Cuenta de nomina",
    "does not apply": "No aplica",
};


// Traducir tipo de producto a ESPAÑOL
var dict_translate_supply_type_es = {
    "Consumables": "Consumibles",
    "Machinery": "Maquinarias",
    "Tools": "Herramientas",
};


// Traducir Unidad de medidad a ESPAÑOL
var dict_translate_measure_type_es = {
    // consumibles -- longitud
    "mts": "Metros",
    "km": "Kilometros",
    "cm": "Centimetros",
    "mile": "Millas",
    "yard": "Yardas",
    "foot": "Pies",
    "inch": "Pulgadas",
    // consumibles -- peso
    "kg": "Kilogramos",
    "gr": "Gramos",
    "mg": "Miligramos",
    "US/ton": "Toneladas estadounidenses",
    "ton": "Toneladas",
    "oz": "Onzas",
    "lb": "Libras",
    // consumibles -- volumen y area
    "lts": "Litros",
    "US/galon": "Galones estadounidenses",
    "galon": "Galones",

    "cf": "Pies cúbicos",
    "ci": "Pulgadas cúbicas",
    "cuc": "Centimetros cúbicos",
    "cum": "Metros cúbicos",
    "packages": "Bultos",
    "package": "Bultos",
    "bags": "Bolsas",
    "sacks": "Sacos",
    "yemas": "Yemas",
    "bun": "Factura",
    "cargo": "Flete",
    "manege": "Picadero",
    // tiempo
    // herramientas y maquinaria
    "hr": "Hora",
    "qty": "Por cantidad",
    "hectares": "Hectáreas",
    "blocks": "Cuadras",
    "dustbin": "Canecas",
    "bunch": "Racimos",
    "cubic-meter": "Metro cúbico",
    "metro-line": "Metro Lineal",
    "square-meter":"Metro cuadrado"


};


// Traducir tipo de cultivo de finca a ESPAÑOL
var dict_translate_crop_type_es = {
    "coffee": "Café",
    "banana": "Banana",
    "avocado": "Aguacate",
    "orange": "Naranja",
    "tangerine": "Mandarina",
    "lemon": "Limón",
    "cacao": "Cacao",
    "hight-density-fruit": "Frutos de alta densidad (uvas, fresas, cerezas, otros)",
    "vegetable": "Hortalizas",
    "tropical-fruit": "Frutos tropicales",
    "palm": "Palma",
    "flower": "Flores",
    "cereal": "Cereales",
    "cattle": "Ganado",
    "pork": "Cerdos",
    "apiculture": "Apicultura",
    "other-birds": "Otras aves",
    "chicken": "Pollos",
    "aquatic-animals": "Animales acuáticos",
    "other-animals": "Otros animales",
    "woods": "Bosques",
    "greenhouses": "Invernaderos",
    "other-(non-limiting)": "Otros (no limitativo)",
    "platano":"Platano",
    "yucca":"Yuca",
    "pasilla":"Pasilla"

};


// endregion diccionarios

// region switch_cases

// Funcion para genrar objetos con CASES
function generate_cases_types(dict_translate, data) {
    return Object.keys(dict_translate).map(key => {
        const value = dict_translate[key];
        return {case: {$eq: [data, key]}, then: value};
    });
}


// Generar switch con cases
// Tipos de unidades de labores
function switch_translate_measure_type_units_es(data) {
    var switch_feature_type = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_measure_type_units_es, data),
                // {case: {$eq: [data, "trees"]}, then: "Jornales"},
            ],
            default: '--------'
        }
    };

    return switch_feature_type;
}


// Tipos de features (objetos del cultivo)
function switch_translate_feature_type_es(data) {
    var switch_feature_type = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_feature_type_es, data),
                // {case: {$eq: [data, "trees"]}, then: "Árboles"},
            ],
            default: '--------'
        }
    };

    return switch_feature_type;
}


// Tipos de contratos de empleados
function switch_translate_contract_type_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_contract_type_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


// Tipos de Periodicidad del salario
function switch_translate_salary_periodicity_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_salary_periodicity_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


//Tipos de Metodos de pago
function switch_translate_payment_methods_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_payment_methods_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


//Tipos de Cuentas bancarias
function switch_translate_account_type_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_account_type_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


//Tipos de [unidades] de productos
function switch_translate_measure_type_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_measure_type_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


//Tipos de productos
function switch_translate_supply_type_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_supply_type_es, data),
            ],
            default: '--------'
        }
    };
    return switch_types;
}


//Tipos de cultivos
function switch_translate_crop_type_es(data) {
    var switch_types = {
        $switch: {
            branches: [
                ...generate_cases_types(dict_translate_crop_type_es, data),
            ],
            //default: '--------'
            default: null
        }
    };
    return switch_types;
}

// endregion switch_cases


//module.exports = dict_translate_feature_type_es;
module.exports = {
    switch_translate_measure_type_units_es,
    switch_translate_feature_type_es,
    switch_translate_contract_type_es,
    switch_translate_salary_periodicity_es,
    switch_translate_payment_methods_es,
    switch_translate_account_type_es,
    switch_translate_measure_type_es,
    switch_translate_supply_type_es,
    switch_translate_crop_type_es,
};
