
var data = db.form_monitoreoenfermedadesnov22.aggregate(

    {
        $match: {
            "Erradicada": "SI"
        }
    }

    , {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "variable_cartografia_oid": { "$toObjectId": "$variable_cartografia.features._id" }
        }
    },



    {
        "$project": {
            "variable_cartografia_oid": 1
        }
    }


    , {
        $group: {
            _id: null
            , arboles: { $push: "$variable_cartografia_oid" }
        }
    }


)


// data


data.forEach(i => {

    db.cartography.updateMany(
        {
            "_id": {
                $in: i.arboles
            }
        },
        {
            $set: {

                "properties.custom.Erradicada": {
                    "type": "bool",
                    "value": true
                }
            }
        }
    )

})
