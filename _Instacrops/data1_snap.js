{
    "auth": true,
    "message": "Metodo ejecutado correctamente!",
    "data": {
        "item_name": "El Canalón -Urrao",
        "item_id": 1281,
        "array_parameters": [
            {
                "parameter_relation_id": 853,
                "parameter_name": "YR Temp forecast",
                "calculado": 0,
                "unit": "°C",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": -9999,
                "max": 999999,
                "error": 0,
                "selected": 0,
                "array_values": [
                    {
                        "value": 28.6,
                        "ts": "2022-05-16T04:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 258,
                "parameter_name": "Horas Frío",
                "calculado": 1,
                "unit": "Horas",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0,
                        "ts": "2022-07-06T04:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 259,
                "parameter_name": "Horas Frío - Temporada",
                "calculado": 1,
                "unit": "Horas",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 1.88,
                        "ts": "2022-07-06T04:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 257,
                "parameter_name": "Evapotranspiración (Día)",
                "calculado": 1,
                "unit": "mm/día",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 2.48,
                        "ts": "2022-11-17T04:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 261,
                "parameter_name": "Grados Día",
                "calculado": 1,
                "unit": "°Cd",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 10.32,
                        "ts": "2022-11-17T04:59:59.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 493,
                "parameter_name": "Porciones Frío (InterE)",
                "calculado": 1,
                "unit": "PF",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0.50392828471653,
                        "ts": "2022-11-17T19:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 491,
                "parameter_name": "Porciones Frío",
                "calculado": 1,
                "unit": "PF",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0,
                        "ts": "2022-11-17T20:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 255,
                "parameter_name": "Precipitación - Temporada",
                "calculado": 1,
                "unit": "mm",
                "conversion_in": "X/0.254",
                "conversion_out": "X*0.254",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 15108,
                        "ts": "2022-11-17T22:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 254,
                "parameter_name": "Precipitación - Diaria",
                "calculado": 1,
                "unit": "mm",
                "conversion_in": "X/0.254",
                "conversion_out": "X*0.254",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0,
                        "ts": "2022-11-18T04:59:59.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 447,
                "parameter_name": "Precipitación - Hora",
                "calculado": 1,
                "unit": "mm",
                "conversion_in": "X/0.254",
                "conversion_out": "X*0.254",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0,
                        "ts": "2022-11-17T22:00:00.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 570,
                "parameter_name": "Velocidad del Viento",
                "calculado": 0,
                "unit": "Km/h",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0.01,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 488,
                "parameter_name": "Punto de Rocio",
                "calculado": 0,
                "unit": "°C",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 15.55,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 535,
                "parameter_name": "Déficit de presión de vapor",
                "calculado": 0,
                "unit": "kPa",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": 0,
                "max": 50,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0.19,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 558,
                "parameter_name": "Bateria",
                "calculado": 0,
                "unit": "V",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 12.8,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 559,
                "parameter_name": "Señal",
                "calculado": 0,
                "unit": "dBm",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": -99,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 560,
                "parameter_name": "Humedad Relativa",
                "calculado": 0,
                "unit": "%",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": 100,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 90.52,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 561,
                "parameter_name": "Temperatura Ambiente",
                "calculado": 0,
                "unit": "°C",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": -50,
                "max": 100,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 17.13,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 562,
                "parameter_name": "Radiación Solar",
                "calculado": 0,
                "unit": "W/m²",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 5.22,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 566,
                "parameter_name": "Precipitacion",
                "calculado": 0,
                "unit": "pulse",
                "conversion_in": "X",
                "conversion_out": "X*0.254",
                "min": 0,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 0,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 567,
                "parameter_name": "seconds",
                "calculado": 0,
                "unit": "Sec",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 605,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 568,
                "parameter_name": "Presión",
                "calculado": 0,
                "unit": "mbar",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 800,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            },
            {
                "parameter_relation_id": 569,
                "parameter_name": "Dirección del Viento",
                "calculado": 0,
                "unit": "°N",
                "conversion_in": "X",
                "conversion_out": "X",
                "min": null,
                "max": null,
                "error": null,
                "selected": 0,
                "array_values": [
                    {
                        "value": 128,
                        "ts": "2022-11-17T22:30:33.000Z"
                    }
                ]
            }
        ]
    }
}
