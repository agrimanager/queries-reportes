
/*
Moler datos de (_instacrops_historico_data2) para insertar en (form_instacropsdatosestaciones)
*/

var data2 = db._instacrops_historico_data2.aggregate(


    [
        // //filtro de fecha
        // {
        //     "$addFields": {
        //         "fecha_filtro": { "$toDate": "$_id" }
        //     }
        // },
        // {
        //     "$match": {
        //         "$and": [
        //             {
        //                 "fecha_filtro": {
        //                     "$gte": ISODate("2022-12-11T11:44:17.117-05:00")
        //                 }
        //             },
        //             {
        //                 "fecha_filtro": {
        //                     "$lte": ISODate("2022-12-13T11:44:17.117-05:00")
        //                 }
        //             }
        //         ]
        //     }
        // },



        //---datos finales
        {
            $project: {
                "_id": "$_id",
                "Estacion": "$item_name",
                "Estacion ID": "$item_id",
                "Parametro": "$parameter_name",
                "Parametro ID": "$parameter_relation_id",
                "Unidad Medida": "$unit",
                "Valor Medicion": { "$toDouble": "$value" },
                "Fecha": "$ts",
                "Point": {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            6.218834,
                            -76.109531
                        ]
                    },
                    "farm": "5d6950e6266b090e29be3be0"
                },
                "uid": ObjectId("5d546da75d2cb76e75171c2a"),
                "supervisor": "Support team",
                "rgDate": "$ts_date",
                "uDate": new Date,
                "capture": "W"

            }
        }

    ]
)


// data2


//--BULK INSERT
var bulk = db.form_instacropsdatosestaciones.initializeUnorderedBulkOp();

data2.forEach(i => {
    bulk.insert(i);
})


bulk.execute();
