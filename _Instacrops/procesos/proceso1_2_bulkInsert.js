
/*
Moler datos de (_instacrops_historico_data) para insertar en (_instacrops_historico_data2)
*/

// var data1 = db._test_instacrop_historico.aggregate(
var data1 = db._instacrops_historico_data.aggregate(
    [

    //   {
    //     $match:{
    //       "_id" : ObjectId("6397a208dd1510689c0ef7b8")
    //     }
    //   },

        {
            $unwind: "$data.array_parameters"
        }

        , {
            $unwind: "$data.array_parameters.array_values"
        }

        , {
            $addFields: {
                value: "$data.array_parameters.array_values.value",
                ts: "$data.array_parameters.array_values.ts",
                ts_date: {
                    $dateFromString: {
                        dateString: "$data.array_parameters.array_values.ts"
                        , timezone: "America/Bogota"
                        // , timezone: "-0500"
                        , format: "%Y-%m-%dT%H:%M:%S.%LZ"
                    }
                }
            }
        }


        //---datos finales
        , {
            $project: {
                //estacion
                "item_name": "$data.item_name" //estacion nombre
                , "item_id": "$data.item_id" //estacion id

                //parametro
                , "parameter_relation_id": "$data.array_parameters.parameter_relation_id"//parametro id
                , "parameter_name": "$data.array_parameters.parameter_name" //parametro nombre
                , "unit": "$data.array_parameters.unit" //parametro unidades

                //valores_medicion
                , "value": "$value" //valor_numero
                , "ts": "$ts"//fecha_txt
                , "ts_date": "$ts_date"//fecha_date

            }
        }
        , { $project: { _id: 0 } }


        // ,{
        //     $group:{
        //         _id:null
        //         ,datos:{$push:"$$ROOT"}
        //     }
        // }


    ]
)


// data1


//--BULK INSERT
var bulk = db._instacrops_historico_data2.initializeUnorderedBulkOp();

data1.forEach(i => {

    // db._instacrops_historico_data2.insert(
    //     i
    // )

    // bulk.insert( { item: "abc123", defaultQty: 100, status: "A", points: 100 } );

    bulk.insert(i);


})


bulk.execute();
