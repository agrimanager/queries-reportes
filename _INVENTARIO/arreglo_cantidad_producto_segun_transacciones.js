var cursor = db.suppliesTimeline.aggregate(

    {
        $match: {
            deleted: false
        }
    },

    {
        $group: {
            _id: {
                //wid:"$wid"
                sid: "$sid"
            }
            , "sum_quantity": { $sum: "$quantity" }
            , "sum_out": { $sum: "$outQuantityMovement" }
        }
    },


    {
        "$lookup": {
            "from": "supplies",
            "localField": "_id.sid",
            "foreignField": "_id",
            "as": "producto"
        }
    },
    { "$unwind": "$producto" },



    {
        $addFields: {
            producto: "$producto.quantity"
        }
    },

    {
        $addFields: {
            producto_cond: {
                $cond: {
                    if: {
                        $eq: ["$producto", "$sum_quantity"]

                    }, then: "igual", else: "diferente"
                }
            }
        }
    },


    {
        $match: {
            producto_cond: "diferente"
        }
    },



)

// cursor


//----editar
cursor.forEach(i => {
    db.supplies.update(
        {
            _id: i._id.sid
        },
        {
            $set: {
                "quantity": i.sum_quantity
            }

        },

    )
})
