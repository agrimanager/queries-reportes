db.form_polinizacionana.aggregate(
    [

        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Palma" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        //--linea
        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        //--arbol
        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Palma": 0
                , "Point": 0
                , "uid": 0
                , "Formula": 0
            }
        }



        //---info fechas

        //---MES REAL
        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },

                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
            }
        }




        //--racimos = Ana1 + Inflorecencia
        , {
            "$addFields": {
                "cantidad_racimos_estimados": {
                    "$sum": [
                        { "$toDouble": "$Inflorescencias en Antesis" },
                        { "$toDouble": "$Aplicacion ANA 1" }
                    ]
                }
            }
        }


        //--Agrupacion
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"

                    , "num_anio_real": "$num_anio"
                    , "num_mes_real": "$num_mes"
                }

                , "total_cantidad_racimos_estimados": { "$sum": "$cantidad_racimos_estimados" }

                // , "data": { "$push": "$$ROOT" }

            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        { "total_cantidad_racimos_estimados": "$total_cantidad_racimos_estimados" }
                    ]
                }
            }
        }



        // /*
        // //---descripcion de ticket

        //  Importante: Se debe de multiplicar por el peso promedio del Mes Estimado,
        //  NO del mes real. Si yo tengo el mes real enero y el mes estimado agosto,
        //  los 2748 racimos estimados que se muestran en el ejemplo
        //  se deben de multiplicar por el peso promedio del mes estimado que es agosto
        //  y eso me arrojará los kilos estimados.

        //  DEL AÑO PASADO
        // */

        //---MES ESTIMADO

        , {
            "$addFields": {
                //--formula de mes
                "num_mes_estimado": {
                    "$cond": {
                        "if": {
                            "$lte": ["$num_mes_real", 5]
                        },
                        "then": { "$sum": ["$num_mes_real", 7] },
                        "else": { "$subtract": ["$num_mes_real", 5] }
                    }
                }
                //--formula de anio
                , "num_anio_estimado": {
                    "$cond": {
                        "if": {
                            "$lte": ["$num_mes_real", 5]
                        },
                        "then": { "$subtract": ["$num_anio_real", 1] },
                        "else": "$num_anio_real"
                    }
                }
            }
        }




        // //--cruce de pesos promedios
        , {
            "$lookup": {
                "from": "form_puentepesospromedio",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"

                    , "num_anio_estimado": "$num_anio_estimado"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }

                                     , { "$eq": ["$$num_anio_estimado", "$Anno"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": true
            }
        }



        //---Peso promedio
        , {
            "$addFields": {
                "peso_promedio_estimado": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes_estimado", 1] }, "then": "$info_lote.ENERO" },
                            { "case": { "$eq": ["$num_mes_estimado", 2] }, "then": "$info_lote.FEBRERO" },
                            { "case": { "$eq": ["$num_mes_estimado", 3] }, "then": "$info_lote.MARZO" },
                            { "case": { "$eq": ["$num_mes_estimado", 4] }, "then": "$info_lote.ABRIL" },
                            { "case": { "$eq": ["$num_mes_estimado", 5] }, "then": "$info_lote.MAYO" },
                            { "case": { "$eq": ["$num_mes_estimado", 6] }, "then": "$info_lote.JUNIO" },
                            { "case": { "$eq": ["$num_mes_estimado", 7] }, "then": "$info_lote.JULIO" },
                            { "case": { "$eq": ["$num_mes_estimado", 8] }, "then": "$info_lote.AGOSTO" },
                            { "case": { "$eq": ["$num_mes_estimado", 9] }, "then": "$info_lote.SEPTIEMBRE" },
                            { "case": { "$eq": ["$num_mes_estimado", 10] }, "then": "$info_lote.OCTUBRE" },
                            { "case": { "$eq": ["$num_mes_estimado", 11] }, "then": "$info_lote.NOVIEMBRE" },
                            { "case": { "$eq": ["$num_mes_estimado", 12] }, "then": "$info_lote.DICIEMBRE" }
                        ],
                        "default": 0
                    }
                }
            }
        }

        , { "$addFields": { "peso_promedio_estimado": { "$ifNull": ["$peso_promedio_estimado", 0] } } }


        , {
            "$project": {
                "info_lote": 0
            }
        }

        //----KG
        , {
            "$addFields": {
                "total_kilogramos_estimados": {
                    "$multiply": [
                        "$total_cantidad_racimos_estimados",
                        "$peso_promedio_estimado"
                    ]
                }
            }
        }

        //--2 decimales
        , {
            "$addFields": {
                "total_kilogramos_estimados": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_kilogramos_estimados", 100] }, { "$mod": [{ "$multiply": ["$total_kilogramos_estimados", 100] }, 1] }] }, 100] }
            }
        }



        //---nombre de meses
        , {
            "$addFields": {
                "mes_real_txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes_real", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes_real", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes_real", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes_real", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes_real", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes_real", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes_real", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes_real", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes_real", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes_real", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes_real", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes_real", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }

            }
        }

        , {
            "$addFields": {
                "mes_estimado_txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes_estimado", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes_estimado", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes_estimado", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes_estimado", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes_estimado", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes_estimado", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes_estimado", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes_estimado", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes_estimado", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes_estimado", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes_estimado", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes_estimado", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }

            }
        }











    ]

)
