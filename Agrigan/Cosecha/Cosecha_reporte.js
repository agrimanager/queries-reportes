db.form_modulodecosechapalmas.aggregate(

    [
        {
            "$addFields": {
                "variable_cartografia": "$Lote"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },



        { "$addFields": { "lote_area": { "$ifNull": ["$lote.properties.custom.Numero de hectareas.value", 0] } } },
        { "$addFields": { "lote_num_plantas": { "$ifNull": ["$lote.properties.custom.Numero de plantas.value", 0] } } },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Lote": 0
            }
        }



        //empleado numerico
        , {
            "$match": {
                "Numero de racimos": { "$ne": "" }
            }
        }
        , {
            "$unwind": {
                "path": "$Numero de racimos",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "empleado_oid": { "$toObjectId": "$Numero de racimos._id" }
            }
        }


        , {
            "$lookup": {
                "from": "employees",
                "localField": "empleado_oid",
                "foreignField": "_id",
                "as": "empleado"
            }
        },
        { "$unwind": "$empleado" }




        //horas laboradas e inactivdas

        , {
            "$addFields": {
                "variable_fecha": {
                    "$toDate": {
                        "$dateToString": {
                            "date": "$rgDate",
                            "format": "%Y-%m-%d %H:%M",
                            "timezone": "America/Bogota"
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$variable_fecha" } },
                "num_mes": { "$month": { "date": "$variable_fecha" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },

                "num_dia_anio": { "$dayOfYear": { "date": "$variable_fecha" } },
                "num_hora": { "$hour": { "date": "$variable_fecha" } }

            }
        }

        , {
            "$group": {
                "_id": {
                    "num_anio": "$num_anio",
                    "num_dia_anio": "$num_dia_anio"
                }

                , "max_hora_dia": { "$max": "$num_hora" }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "max_hora_dia": "$max_hora_dia"
                        }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "horas_laboradas": {
                    "$subtract": ["$max_hora_dia", 7]
                }
            }
        }
        , {
            "$addFields": {
                "horas_inactivas": {
                    "$subtract": [8, "$horas_laboradas"]
                }
            }
        }






        //pryeccion final
        , {
            "$project": {

                "codigo": "20061212"

                , "Nombre": { "$concat": ["$empleado.firstName", " ", "$empleado.lastName"] }
                , "Cargo": "$Numero de racimos.reference"
                , "lote": "$lote"
                , "Material": "-----"
                , "Area": "$lote_area"
                , "Num Palmas": "$lote_num_plantas"
                , "Racimos Cosechado": { "$toDouble": "$Numero de racimos.value" }

                , "Horas Laboradas": "$horas_laboradas"
                , "Horas Inactivas": "$horas_inactivas"

                , "fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%Y-%m-%d %H:%M",
                        "timezone": "America/Bogota"
                    }
                }

            }
        }





    ]

)
