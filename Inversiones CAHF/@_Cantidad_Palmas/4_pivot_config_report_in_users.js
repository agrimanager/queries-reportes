"🌴Palmas X Linea" : {
			"grid" : [
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "finca",
					"index" : 1,
					"visible" : true,
					"field" : "finca",
					"width" : "153px",
					"uid" : "grid-column32"
				},
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "bloque",
					"index" : 2,
					"visible" : true,
					"field" : "bloque",
					"width" : "120px",
					"uid" : "grid-column33"
				},
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "lote",
					"index" : 3,
					"visible" : true,
					"field" : "lote",
					"width" : "117px",
					"uid" : "grid-column34"
				},
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "linea",
					"index" : 4,
					"visible" : true,
					"field" : "linea",
					"width" : "117px",
					"uid" : "grid-column35"
				},
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "cantidad_arboles",
					"index" : 0,
					"visible" : true,
					"field" : "cantidad_arboles",
					"width" : "175px",
					"uid" : "grid-column31"
				},
				{
					"disableHtmlEncode" : true,
					"allowSorting" : true,
					"allowResizing" : true,
					"allowFiltering" : true,
					"allowGrouping" : true,
					"allowReordering" : true,
					"showColumnMenu" : true,
					"enableGroupByFormat" : false,
					"allowEditing" : true,
					"showInColumnChooser" : true,
					"allowSearching" : true,
					"autoFit" : true,
					"sortDirection" : "Ascending",
					"allowSelection" : true,
					"type" : "string",
					"foreignKeyField" : "rgDate",
					"index" : 5,
					"visible" : false,
					"field" : "rgDate",
					"width" : "203px",
					"uid" : "grid-column36"
				}
			],
			"pivot" : {
				"rows" : [
					{
						"name" : "finca",
						"caption" : "finca",
						"isCalculatedField" : false,
						"isNamedSet" : false,
						"showNoDataItems" : false,
						"showSubTotals" : true,
						"type" : "Count"
					},
					{
						"name" : "bloque",
						"caption" : "bloque",
						"isCalculatedField" : false,
						"isNamedSet" : false,
						"showNoDataItems" : false,
						"showSubTotals" : true,
						"type" : "Count"
					},
					{
						"name" : "lote",
						"caption" : "lote",
						"isCalculatedField" : false,
						"isNamedSet" : false,
						"showNoDataItems" : false,
						"showSubTotals" : true,
						"type" : "Count"
					},
					{
						"name" : "linea",
						"caption" : "linea",
						"isCalculatedField" : false,
						"isNamedSet" : false,
						"showNoDataItems" : false,
						"showSubTotals" : true,
						"type" : "Count"
					}
				],
				"values" : [
					{
						"name" : "cantidad_arboles",
						"caption" : "cantidad_arboles",
						"isCalculatedField" : false,
						"isNamedSet" : false,
						"showNoDataItems" : false,
						"showSubTotals" : true,
						"type" : "Sum"
					}
				],
				"filters" : [ ],
				"columns" : [ ],
				"dataSource" : [ ],
				"expandAll" : true,
				"formatSettings" : [ ],
				"showColumnGrandTotals" : false,
				"showColumnSubTotals" : true,
				"showRowGrandTotals" : false,
				"showRowSubTotals" : true,
				"showGrandTotals" : false,
				"showSubTotals" : true,
				"enableSorting" : true,
				"providerType" : "Relational",
				"localeIdentifier" : 1033,
				"excludeFields" : [ ],
				"allowLabelFilter" : false,
				"allowValueFilter" : false,
				"allowMemberFilter" : true,
				"filterSettings" : [ ],
				"sortSettings" : [ ],
				"drilledMembers" : [ ],
				"valueSortSettings" : {
					"headerDelimiter" : ".",
					"sortOrder" : "None"
				},
				"valueAxis" : "column",
				"calculatedFieldSettings" : [ ],
				"showHeaderWhenEmpty" : true,
				"alwaysShowValueHeader" : false,
				"conditionalFormatSettings" : [ ],
				"groupSettings" : [ ],
				"showAggregationOnValueField" : true
			}
		}