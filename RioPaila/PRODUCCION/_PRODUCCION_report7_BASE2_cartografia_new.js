db.form_censodeproduccion.aggregate(
    [

        //=====CONDICIONALES BASE
        //---condicion de guion
        {
            "$match": {
                // "Numero de Racimos verdes": {$not: { $regex: "-" }}//--ejemplo
                "Numero de flor en Antesis": { "$not": { "$regex": "-" } },
                "Numero de flor en Postantesis": { "$not": { "$regex": "-" } },
                "Numero de Racimos verdes": { "$not": { "$regex": "-" } },
                "Numero de Racimos Premaduros": { "$not": { "$regex": "-" } },
                "Numero de Racimos Maduro": { "$not": { "$regex": "-" } }
            }
        },

        //---condicion de espacios
        {
            "$match": {
                // "Numero de Racimos verdes": { $not: { $regex: " " } }//--ejemplo
                "Numero de flor en Antesis": { "$not": { "$regex": " " } },
                "Numero de flor en Postantesis": { "$not": { "$regex": " " } },
                "Numero de Racimos verdes": { "$not": { "$regex": " " } },
                "Numero de Racimos Premaduros": { "$not": { "$regex": " " } },
                "Numero de Racimos Maduro": { "$not": { "$regex": " " } }
            }
        },

        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Palma" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--Finca
        {
            "$addFields": {
                "Finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        { "$unwind": "$Finca" },

        { "$addFields": { "Finca": { "$ifNull": ["$Finca.name", "no existe"] } } },


        //--Bloque
        {
            "$addFields": {
                "Bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Bloque": { "$ifNull": ["$Bloque.properties.name", "no existe"] } } },

        //--Lote
        {
            "$addFields": {
                "Lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Lote": { "$ifNull": ["$Lote.properties.name", "no existe"] } } },

        //--Linea
        {
            "$addFields": {
                "Linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Linea": { "$ifNull": ["$Linea.properties.name", "no existe"] } } },


        //--Planta
        {
            "$addFields": {
                "Planta": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Planta",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Planta": { "$ifNull": ["$Planta.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0
            }
        }


        //---Planta erradicada
        , {
            "$addFields": {
                "Planta_erradicada": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": [{ "$toDouble": "$Numero de flor en Antesis" }, 0] },
                                { "$eq": [{ "$toDouble": "$Numero de flor en Postantesis" }, 0] },
                                { "$eq": [{ "$toDouble": "$Numero de Racimos verdes" }, 0] },
                                { "$eq": [{ "$toDouble": "$Numero de Racimos Premaduros" }, 0] },
                                { "$eq": [{ "$toDouble": "$Numero de Racimos Maduro" }, 0] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }

                }
            }
        }



        //----otros

        //=====INFO LOTE
        , {
            "$lookup": {
                "from": "form_lineapalmariopaila",
                "as": "info_lote",
                "let": {
                    "lote": "$Lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "plantas_x_lote": "$info_lote.NUMERO PALMAS  EN CAMPO CORREGIDO",
                "lineas_x_lote": "$info_lote.LINEAS POR LOTE",
                "material": "$info_lote.MATERIAL",
                "densidad": "$info_lote.DENSIDAD  DE PALMAS POR HA",
                "area_hectareas": "$info_lote.AREA NETA SEMBRADA"
            }
        }

        , {
            "$project": {
                "info_lote": 0,
                "Formula": 0,
                "Produccion": 0
            }
        }

        //=====PLANTAS_CENSADAS_X_LOTE

        , {
            "$group": {
                "_id": {
                    "lote": "$Lote"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "num_palmas_censadas": { "$sum": 1 }

                , "sum_flores_antesis": { "$sum": { "$toDouble": "$Numero de flor en Antesis" } }
                , "sum_flores_postantesis": { "$sum": { "$toDouble": "$Numero de flor en Postantesis" } }

                , "sum_racimos_verdes": { "$sum": { "$toDouble": "$Numero de Racimos verdes" } }
                , "sum_racimos_maduros": { "$sum": { "$toDouble": "$Numero de Racimos Maduro" } }
                , "sum_racimos_inmaduros": { "$sum": { "$toDouble": "$Numero de Racimos Premaduros" } }
                // , "sum_total_estructura_productiva": { "$sum": { "$toDouble": "$Total de Estructura productivas" } }//---ojo ya no esta

                //---plantas erradicas
                , "sum_palmas_erradicadas": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Planta_erradicada", true] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }


            }
        }



        , {
            "$addFields": {
                "sum_total_flores": {
                    "$sum": ["$sum_flores_antesis", "$sum_flores_postantesis"]
                }

            }
        }


        , {
            "$addFields": {
                "sum_total_racimos": {
                    "$sum": ["$sum_racimos_verdes", "$sum_racimos_maduros", "$sum_racimos_inmaduros"]
                }
            }
        }


        , {
            "$addFields": {
                "sum_total_estructura_productiva": {
                    "$sum": ["$sum_total_flores", "$sum_total_racimos"]
                }
            }
        }


        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "num_palmas_censadas": "$num_palmas_censadas"
                            , "sum_palmas_erradicadas": "$sum_palmas_erradicadas"

                            , "sum_flores_antesis": "$sum_flores_antesis"
                            , "sum_flores_postantesis": "$sum_flores_postantesis"
                            , "sum_total_flores": "$sum_total_flores"

                            , "sum_racimos_verdes": "$sum_racimos_verdes"
                            , "sum_racimos_maduros": "$sum_racimos_maduros"
                            , "sum_racimos_inmaduros": "$sum_racimos_inmaduros"
                            , "sum_total_racimos": "$sum_total_racimos"

                            , "sum_total_estructura_productiva": "$sum_total_estructura_productiva"
                        }
                    ]
                }
            }
        }



        //---operaciones

        , {
            "$addFields": {
                "pct_muestra_palmas": {
                    "$multiply": [{ "$divide": ["$num_palmas_censadas", "$plantas_x_lote"] }, 100]
                }
            }
        }

        , {
            "$addFields": {
                "pct_palmas_erradicadas": {
                    "$multiply": [{ "$divide": ["$sum_palmas_erradicadas", "$num_palmas_censadas"] }, 100]
                }
            }
        }


        , {
            "$addFields": {
                "pct_castigo": {
                    "$multiply": ["$sum_total_flores", 0.95]
                }
            }
        }


        , {
            "$addFields": {
                "prom_estructuras_x_palma": {
                    "$divide": [{ "$sum": ["$sum_total_racimos", "$pct_castigo"] }, "$num_palmas_censadas"]
                }
            }
        }


        , {
            "$addFields": {
                "prom_estructuras_x_lote": {
                    "$multiply": ["$prom_estructuras_x_palma", "$plantas_x_lote"]
                }
            }
        }


        , {
            "$addFields": {
                "toneladas_estimadas_semestre": {
                    "$divide": [{ "$multiply": ["$prom_estructuras_x_lote", 8] }, 1000]
                }
            }
        }

        , {
            "$addFields": {
                "toneladas_estimadas_x_area": {
                    "$divide": ["$toneladas_estimadas_semestre", "$area_hectareas"]
                }
            }
        }






    ]


    , { "allowDiskUse": true })