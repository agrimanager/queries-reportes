// //--problema1
// db.form_censodeproduccion.find({})
//     .projection({})
//     .sort({ "Numero de Racimos verdes": 1 })
//     // .sort({ "Numero de flor en Antesis": -1 })
//     // .sort({ "Numero de flor en Postantesis": -1 })
//     // .sort({ "Numero de Racimos Premaduros": -1 })
//     // .sort({ "Numero de Racimos Maduro": -1 })


//--problema2
//db.form_censodeproduccion.find({ "Numero de flor en Antesis": "1-" })
// db.form_censodeproduccion.find({ "Numero de Racimos verdes": "8-" })
db.form_censodeproduccion.find({ "Numero de Racimos verdes": "3 " })




//---arreglo (condicional)
db.form_censodeproduccion_con_errores.aggregate(

    //---condicion de guion
    {
        "$match": {
            // "Numero de Racimos verdes": {$not: { $regex: "-" }}//--ejemplo
            "Numero de flor en Antesis": { "$not": { "$regex": "-" } },
            "Numero de flor en Postantesis": { "$not": { "$regex": "-" } },
            "Numero de Racimos verdes": { "$not": { "$regex": "-" } },
            "Numero de Racimos Premaduros": { "$not": { "$regex": "-" } },
            "Numero de Racimos Maduro": { "$not": { "$regex": "-" } }
        }
    },

    //---condicion de espacios
    {
        "$match": {
            // "Numero de Racimos verdes": { $not: { $regex: " " } }//--ejemplo
            "Numero de flor en Antesis": { "$not": { "$regex": " " } },
            "Numero de flor en Postantesis": { "$not": { "$regex": " " } },
            "Numero de Racimos verdes": { "$not": { "$regex": " " } },
            "Numero de Racimos Premaduros": { "$not": { "$regex": " " } },
            "Numero de Racimos Maduro": { "$not": { "$regex": " " } }
        }
    },
    
    //---condicion de cartografia
    {
        "$match": {
            "Palma.path": { "$ne": "" }
        }
    },

)
