[


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"

            , "Planta_erradicada": "$Planta.properties.custom.Erradicada.value"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "info_lote",
            "let": {
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_lote",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "plantas_x_lote": "$info_lote.NUMERO PALMAS  EN CAMPO CORREGIDO",
            "lineas_x_lote": "$info_lote.LINEAS POR LOTE",
            "material": "$info_lote.MATERIAL",
            "densidad": "$info_lote.DENSIDAD  DE PALMAS POR HA",
            "area_hectareas": "$info_lote.AREA NETA SEMBRADA"
        }
    }

    , {
        "$project": {
            "info_lote": 0,
            "Formula": 0,
            "Produccion": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$Lote"
            },
            "data": {
                "$push": "$$ROOT"
            }
            , "num_palmas_censadas": { "$sum": 1 }

            , "sum_flores_antesis": { "$sum": { "$toDouble": "$Numero de flor en Antesis" } }
            , "sum_flores_postantesis": { "$sum": { "$toDouble": "$Numero de flor en Postantesis" } }

            , "sum_racimos_verdes": { "$sum": { "$toDouble": "$Numero de Racimos verdes" } }
            , "sum_racimos_maduros": { "$sum": { "$toDouble": "$Numero de Racimos Maduro" } }
            , "sum_racimos_inmaduros": { "$sum": { "$toDouble": "$Numero de Racimos Premaduros" } }

            , "sum_palmas_erradicadas": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$Planta_erradicada", true] },
                                "then": 1
                            }
                        ],
                        "default": 0
                    }
                }
            }


        }
    }



    , {
        "$addFields": {
            "sum_total_flores": {
                "$sum": ["$sum_flores_antesis", "$sum_flores_postantesis"]
            }

        }
    }


    , {
        "$addFields": {
            "sum_total_racimos": {
                "$sum": ["$sum_racimos_verdes", "$sum_racimos_maduros", "$sum_racimos_inmaduros"]
            }
        }
    }


    , {
        "$addFields": {
            "sum_total_estructura_productiva": {
                "$sum": ["$sum_total_flores", "$sum_total_racimos"]
            }
        }
    }


    , { "$unwind": "$data" }
    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "num_palmas_censadas": "$num_palmas_censadas"
                        , "sum_palmas_erradicadas": "$sum_palmas_erradicadas"

                        , "sum_flores_antesis": "$sum_flores_antesis"
                        , "sum_flores_postantesis": "$sum_flores_postantesis"
                        , "sum_total_flores": "$sum_total_flores"

                        , "sum_racimos_verdes": "$sum_racimos_verdes"
                        , "sum_racimos_maduros": "$sum_racimos_maduros"
                        , "sum_racimos_inmaduros": "$sum_racimos_inmaduros"
                        , "sum_total_racimos": "$sum_total_racimos"

                        , "sum_total_estructura_productiva": "$sum_total_estructura_productiva"
                    }
                ]
            }
        }
    }





    , {
        "$addFields": {
            "pct_muestra_palmas": {
                "$multiply": [{ "$divide": ["$num_palmas_censadas", "$plantas_x_lote"] }, 100]
            }
        }
    }

    , {
        "$addFields": {
            "pct_palmas_erradicadas": {
                "$multiply": [{ "$divide": ["$sum_palmas_erradicadas", "$num_palmas_censadas"] }, 100]
            }
        }
    }


    , {
        "$addFields": {
            "pct_castigo": {
                "$multiply": ["$sum_total_flores", 0.95]
            }
        }
    }


    , {
        "$addFields": {
            "prom_estructuras_x_palma": {
                "$divide": [{ "$sum": ["$sum_total_racimos", "$pct_castigo"] }, "$num_palmas_censadas"]
            }
        }
    }


    , {
        "$addFields": {
            "prom_estructuras_x_lote": {
                "$multiply": ["$prom_estructuras_x_palma", "$plantas_x_lote"]
            }
        }
    }


    , {
        "$addFields": {
            "toneladas_estimadas_semestre": {
                "$divide": [{ "$multiply": ["$prom_estructuras_x_lote", 8] }, 1000]
            }
        }
    }

    , {
        "$addFields": {
            "toneladas_estimadas_x_area": {
                "$divide": ["$toneladas_estimadas_semestre", "$area_hectareas"]
            }
        }
    }


]