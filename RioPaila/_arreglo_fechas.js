
//----editar fecha (mes 4 por mes 5)
var data_censos = db.form_censodeproduccion.aggregate(

    {
        $match: {
            supervisor: "800035 WILMAR ANDRES"
            , "Racimo No Cosechado": { $exists: true }
        }
    }

    , {
        $addFields: {
            rgDate2: { $add: ["$rgDate", 2592000000] }//sumar 30 dias
        }
    }


)


// data_censos

data_censos.forEach(i => {

    db.form_censodeproduccion.update(
        {
            _id: i._id
        },
        {
            $set: {
                rgDate: i.rgDate2
            }
        }

    )

})
