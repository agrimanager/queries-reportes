db.form_censodeproduccion.aggregate(
    [

        //----filtro de fechas
        {
            "$match": {
                "$and": [
                    { "rgDate": { "$gte": ISODate("2021-01-01T11:44:17.117-05:00") } },
                    { "rgDate": { "$lte": ISODate("2021-01-05T11:44:17.117-05:00") } }
                ]
            }
        },



        // //=====CONDICIONALES BASE
        // //---condicion de guion
        // {
        //     "$match": {
        //         // "Numero de Racimos verdes": {$not: { $regex: "-" }}//--ejemplo
        //         "Numero de flor en Antesis": { "$not": { "$regex": "-" } },
        //         "Numero de flor en Postantesis": { "$not": { "$regex": "-" } },
        //         "Numero de Racimos verdes": { "$not": { "$regex": "-" } },
        //         "Numero de Racimos Premaduros": { "$not": { "$regex": "-" } },
        //         "Numero de Racimos Maduro": { "$not": { "$regex": "-" } }
        //     }
        // },

        // //---condicion de espacios
        // {
        //     "$match": {
        //         // "Numero de Racimos verdes": { $not: { $regex: " " } }//--ejemplo
        //         "Numero de flor en Antesis": { "$not": { "$regex": " " } },
        //         "Numero de flor en Postantesis": { "$not": { "$regex": " " } },
        //         "Numero de Racimos verdes": { "$not": { "$regex": " " } },
        //         "Numero de Racimos Premaduros": { "$not": { "$regex": " " } },
        //         "Numero de Racimos Maduro": { "$not": { "$regex": " " } }
        //     }
        // },

        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Palma" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--Finca
        {
            "$addFields": {
                "Finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        { "$unwind": "$Finca" },

        { "$addFields": { "Finca": { "$ifNull": ["$Finca.name", "no existe"] } } },


        //--Bloque
        {
            "$addFields": {
                "Bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Bloque": { "$ifNull": ["$Bloque.properties.name", "no existe"] } } },

        //--Lote
        {
            "$addFields": {
                "Lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Lote": { "$ifNull": ["$Lote.properties.name", "no existe"] } } },

        //--Linea
        {
            "$addFields": {
                "Linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "Linea": { "$ifNull": ["$Linea.properties.name", "no existe"] } } },


        //--Planta
        {
            "$addFields": {
                "Planta": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$Planta",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "lon": { "$arrayElemAt": ["$Planta.geometry.coordinates", 0] },
                "lat": { "$arrayElemAt": ["$Planta.geometry.coordinates", 1] }
            }
        },
        { "$addFields": { "Planta": { "$ifNull": ["$Planta.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0


                , "Palma": 0
                , "Formula": 0
                , "Point": 0
                , "Produccion": 0
                , "uid": 0
                , "supervisor": 0
                , "uDate": 0
            }
        }


    ]


    , { "allowDiskUse": true })
