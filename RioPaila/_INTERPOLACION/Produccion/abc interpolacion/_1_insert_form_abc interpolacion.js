db.getCollection("forms").insert({
    "uid": ObjectId("5e46fe5c67074a43d66b9d1f"),
    "name": "abc interpolacion",
    "fstructure": [
        "{\"name\":\"Palma\",\"type\":\"Cartography\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Numero de Racimos verdes\",\"type\":\"Number\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
    ],
    "icon": "050",
    "anchor": "form_abcinterpolacion",
    "uname": "Support team",
    "tracing": false,
    "timeTracing": "",
    "supervisors": [
        ObjectId("5e951150deaed5648bd3367d")
    ],
    "consecutives": [
        "trees"
    ],
    "projectColor": "",
    "queryMap": "",
    "principalMaster": "",
    "followField": "",
    "backOption": "fixed",
    "lastData": false,
    "employeesRoles": [],
    "rgDate": ISODate("2021-10-21T22:20:00.486-05:00"),
    "uDate": ISODate("2021-10-21T22:20:29.517-05:00")
})
