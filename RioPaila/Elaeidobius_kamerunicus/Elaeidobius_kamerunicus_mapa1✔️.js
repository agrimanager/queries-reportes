[

    {
        "$addFields": { "Cartography": "$Palmas  de Evaluacion", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },



    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }


    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }



    , {
        "$addFields": {
            "dato1_num_flores": {
                "$toDouble":
                    "$Numero de flores masculinas  en Antesis por Hectarea"
            }
        }
    }



    , {
        "$lookup": {
            "from": "form_conteodepolinizadoresenlaboratorio",
            "as": "form_conteodepolinizadoresenlaboratorio_aux",
            "let": {
                "material": "$Materiales",
                "lote": "$Lote",
                "num_anio": "$num_anio",
                "num_mes": "$num_mes"
            },
            "pipeline": [

                {
                    "$addFields": {
                        "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_oid",
                                "$features_oid"
                            ]
                        }
                    }
                },
                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },
                {
                    "$addFields": {
                        "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                        "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                        "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                    }
                },
                {
                    "$addFields": {
                        "Bloque": "$Bloque.properties.name",
                        "Lote": "$Lote.properties.name"
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "Finca._id",
                        "foreignField": "_id",
                        "as": "Finca"
                    }
                },
                {
                    "$addFields": {
                        "Finca": "$Finca.name"
                    }
                },
                { "$unwind": "$Finca" },
                {
                    "$project": {
                        "split_path": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "features_oid": 0
                    }
                }


                , {
                    "$addFields": {
                        "num_anio": { "$year": { "date": "$rgDate" } },
                        "num_mes": { "$month": { "date": "$rgDate" } }
                    }
                }


                , {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$material", "$Materiales"] },
                                { "$eq": ["$$lote", "$Lote"] },


                                { "$eq": ["$$num_anio", "$num_anio"] },
                                { "$eq": ["$$num_mes", "$num_mes"] }
                            ]
                        }
                    }
                }
                , {
                    "$limit": 1
                }
            ]
        }
    }



    , {
        "$match": {
            "form_conteodepolinizadoresenlaboratorio_aux": { "$ne": [] }
        }
    }

    , { "$unwind": "$form_conteodepolinizadoresenlaboratorio_aux" }




    , {
        "$addFields": {
            "dato2_num_insectos": {
                "$toDouble":
                    "$form_conteodepolinizadoresenlaboratorio_aux.Numero de insectos por inflorescencia"
            }
        }
    }



    , {
        "$addFields": {
            "dato3": {
                "$multiply": [
                    "$dato1_num_flores", "$dato2_num_insectos"
                ]
            }
        }
    }





    , {
        "$project": {
            "Censo de Polinizadores  en campo": 0,
            "form_conteodepolinizadoresenlaboratorio_aux": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$Lote",

                "anio": "$num_anio",
                "mes": "$Mes_Txt",
                "material": "$Materiales"

                , "idform": "$idform"
            }
            , "sum_dato3": { "$sum": "$dato3" }

            , "data": { "$push": "$$ROOT" }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$sum_dato3", 0] }, { "$lte": ["$sum_dato3", 10000] }]
                    },
                    "then": "#ff0000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$sum_dato3", 10000] }, { "$lte": ["$sum_dato3", 20000] }]
                            },
                            "then": "#1a73e8",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$sum_dato3", 20000] }, { "$lte": ["$sum_dato3", 40000] }]
                                    },
                                    "then": "#008000",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$sum_dato3", 40000] }, { "$lte": ["$sum_dato3", 60000] }]
                                            },
                                            "then": "#7B3F32",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$sum_dato3", 60000] }, { "$lte": ["$sum_dato3", 90000] }]
                                                    },
                                                    "then": "#e2ba1f",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$sum_dato3", 90000] }, { "$lte": ["$sum_dato3", 120000] }]
                                                            },
                                                            "then": "#9c9c9c",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$sum_dato3", 120000] }, { "$lte": ["$sum_dato3", 160000] }]
                                                                    },
                                                                    "then": "#ff8000",
                                                                    "else": {
                                                                        "$cond": {
                                                                            "if": {
                                                                                "$and": [{ "$gt": ["$sum_dato3", 160000] }, { "$lte": ["$sum_dato3", 190000] }]
                                                                            },
                                                                            "then": "#722f37",
                                                                            "else": "#3f3b69"
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },

            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$sum_dato3", 0] }, { "$lte": ["$sum_dato3", 10000] }]
                    },
                    "then": "A-[0 - 10000]",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$sum_dato3", 10000] }, { "$lte": ["$sum_dato3", 20000] }]
                            },
                            "then": "B-(10000 - 20000]",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$sum_dato3", 20000] }, { "$lte": ["$sum_dato3", 40000] }]
                                    },
                                    "then": "C-(20000 - 40000]",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$sum_dato3", 40000] }, { "$lte": ["$sum_dato3", 60000] }]
                                            },
                                            "then": "D-(40000 - 60000]",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$sum_dato3", 60000] }, { "$lte": ["$sum_dato3", 90000] }]
                                                    },
                                                    "then": "E-(60000 - 90000]",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$sum_dato3", 90000] }, { "$lte": ["$sum_dato3", 120000] }]
                                                            },
                                                            "then": "F-(90000 - 120000]",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$sum_dato3", 120000] }, { "$lte": ["$sum_dato3", 160000] }]
                                                                    },
                                                                    "then": "G-(120000 - 160000]",
                                                                    "else": {
                                                                        "$cond": {
                                                                            "if": {
                                                                                "$and": [{ "$gt": ["$sum_dato3", 160000] }, { "$lte": ["$sum_dato3", 190000] }]
                                                                            },
                                                                            "then": "H-(160000 - 190000]",
                                                                            "else": "I-(>190000)"
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Material": "$_id.material",
                "Mes": "$_id.mes",

                "Rango": "$rango",

                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
            }
        }
    }



]