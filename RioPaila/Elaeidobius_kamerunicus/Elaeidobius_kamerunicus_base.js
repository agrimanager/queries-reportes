db.form_censodeelaeidobiuskamerunicus.aggregate(
    [

        //--Cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Palmas  de Evaluacion.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Palmas  de Evaluacion.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        //--DATO1
        , {
            "$addFields": {
                "dato1_num_flores": "$Numero de flores masculinas  en Antesis por Hectarea"
            }
        }


        //---cruzar para obtener DATO2
        , {
            "$lookup": {
                "from": "form_conteodepolinizadoresenlaboratorio",
                "as": "form_conteodepolinizadoresenlaboratorio_aux",
                "let": {
                    "material": "$Materiales",
                    "lote": "$Lote"
                },
                "pipeline": [
                    //--Cartografia
                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },
                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                            // "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                            // "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                        }
                    },
                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "Lote": "$Lote.properties.name"
                            // "Linea": "$Linea.properties.name",
                            // "Planta": "$Planta.properties.name"
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },
                    {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    },
                    { "$unwind": "$Finca" },
                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }

                    // //---CONDICION DE LOOKUP
                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$material", "$Materiales"] },
                                    { "$eq": ["$$lote", "$Lote"] }
                                ]
                            }
                        }
                    }
                    , {
                        "$limit": 1
                    }
                ]
            }
        }



    ]
)