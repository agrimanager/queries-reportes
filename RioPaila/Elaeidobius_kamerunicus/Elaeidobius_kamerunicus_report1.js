db.form_censodeelaeidobiuskamerunicus.aggregate(
    [

        //--Cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Palmas  de Evaluacion.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Palmas  de Evaluacion.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        //--DATO1
        , {
            "$addFields": {
                "dato1_num_flores": {
                    "$toDouble":
                        "$Numero de flores masculinas  en Antesis por Hectarea"
                }
            }
        }


        //---FECHAS
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        //---cruzar para obtener DATO2
        , {
            "$lookup": {
                "from": "form_conteodepolinizadoresenlaboratorio",
                "as": "form_conteodepolinizadoresenlaboratorio_aux",
                "let": {
                    "material": "$Materiales",
                    "lote": "$Lote"
                },
                "pipeline": [
                    //--Cartografia
                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },
                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                            // "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                            // "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                        }
                    },
                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "Lote": "$Lote.properties.name"
                            // "Linea": "$Linea.properties.name",
                            // "Planta": "$Planta.properties.name"
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },
                    {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    },
                    { "$unwind": "$Finca" },
                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }

                    // //---CONDICION DE LOOKUP
                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$material", "$Materiales"] },
                                    { "$eq": ["$$lote", "$Lote"] }
                                ]
                            }
                        }
                    }
                    , {
                        "$limit": 1
                    }
                ]
            }
        }


        //---borrar datos que no cruzan
        , {
            "$match": {
                "form_conteodepolinizadoresenlaboratorio_aux": { "$ne": [] }
            }
        }

        , { "$unwind": "$form_conteodepolinizadoresenlaboratorio_aux" }



        //---CALCULAR VARIABLES
        //--DATO2
        , {
            "$addFields": {
                "dato2_num_insectos": {
                    "$toDouble":
                        "$form_conteodepolinizadoresenlaboratorio_aux.Numero de insectos por inflorescencia"
                }
            }
        }


        //--DATO3 = DATA1 * DATO2
        , {
            "$addFields": {
                "dato2_num_insectos": {
                    "$toDouble":
                        "$form_conteodepolinizadoresenlaboratorio_aux.Numero de insectos por inflorescencia"
                }
            }
        }




        //---no mostrar datos (array de objs,...etc)
        , {
            "$project": {
                "Censo de Polinizadores  en campo": 0,
                "form_conteodepolinizadoresenlaboratorio_aux": 0
            }
        }




    ]
)