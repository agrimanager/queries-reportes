//--mapa x lote
db.form_plagasriopaila.aggregate(
    [

        // // //-----
        // { $unwind: "$Arbol.features" },
        // {
        //     "$addFields": {
        //         "today": new Date,
        //         "Cartography": "$Arbol.features"
        //     }
        // },
        // // //-----

        {
            "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        //---
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Cartography.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        //---
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                // "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
                // ,"Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                // ,"Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },




        // {
        //     "$lookup": {
        //         "from": "cartography_polygon",
        //         "localField": "Lote._id",
        //         "foreignField": "_id",
        //         "as": "info_polygon"
        //     }
        // },
        // {
        //     "$addFields": {
        //         "info_polygon": "$info_polygon.internal_features"
        //     }
        // },
        // { "$unwind": "$info_polygon" },

        // {
        //     "$addFields": {
        //         "plantas_x_poligono": {
        //             "$filter": {
        //                 "input": "$info_polygon",
        //                 "as": "item",
        //                 "cond": { "$eq": ["$$item.type_features", "trees"] }
        //             }
        //         }
        //     }
        // },
        // { "$unwind": "$plantas_x_poligono" },
        // {
        //     "$addFields": {
        //         "plantas_x_poligono": "$plantas_x_poligono.count_features"
        //     }
        // },

        //---Cartography = lote (geometria a dibujar)
        {
            "$addFields": {
                "Cartography": "$Lote"
            }
        },


        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                // "Uma": "$Uma.properties.name"
                // ,"Linea": "$Linea.properties.n,ame"
                // ,"Planta": "$Planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_polygon": 0
            }
        }

        // , {
        //     "$group": {
        //         "_id": {
        //             "Finca": "$Finca",
        //             "Lote": "$Lote",
        //             "Enfermedad": "$Enfermedad",
        //             "plantas_x_poligono": "$plantas_x_poligono",

        //             "idform": "$idform"
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "num_plantas_dif": { "$sum": 1 }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "pct_incidencia": {
        //             "$multiply": [
        //                 { "$divide": ["$num_plantas_dif", "$_id.plantas_x_poligono"] }
        //                 , 100
        //             ]
        //         }
        //     }
        // }


        , {
            "$group": {
                "_id": {
                    "lote": "$Lote",
                    "plaga": "$Plagas",
                    "idform": "$idform"
                },
                "sum_larvas_x_plaga_x_lote": {
                    "$sum": { "$toDouble": "$Numero de Larva" }
                },
                "num_palmas_censadas": {
                    "$sum": 1
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {
        //                     "sum_larvas_x_plaga_x_lote": "$sum_larvas_x_plaga_x_lote",
        //                     "num_palmas_censadas": "$num_palmas_censadas"
        //                 }
        //             ]
        //         }
        //     }
        // }

        , {
            "$addFields": {
                "pct_infestacion": { "$multiply": [{ "$divide": ["$sum_larvas_x_plaga_x_lote", "$num_palmas_censadas"] }, 100] }
            }
        }



        // //--leyenda reporte
        //%Infestacion: #ffffff,[0%-10%):#verde,[10%-20%):#amarillo,[20%-30%):#azul,[30%-40%]:#rojo,(>40):#cafe
        //%Infestacion: #ffffff,[0%-10%):#00FF00,[10%-20%):#ffff00,[20%-30%):#1a73e8,[30%-40%]:#ff0000,(>40):#7B3F32

        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                        },
                        "then": "#00FF00",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                                },
                                "then": "#ffff00",
                                // "else": "#fd0100"
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                        },
                                        "then": "#1a73e8",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                                },
                                                "then": "#ff0000",
                                                "else": "#7B3F32"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                //%Infestacion: #ffffff,[0%-10%):#verde,[10%-20%):#amarillo,[20%-30%):#azul,[30%-40%]:#rojo,(>40):#cafe
                //%Infestacion: #ffffff,[0%-10%):#00FF00,[10%-20%):#ffff00,[20%-30%):#1a73e8,[30%-40%]:#ff0000,(>40):#7B3F32
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                        },
                        "then": "A-[0%-10%)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                                },
                                "then": "B-[10%-20%)",
                                // "else": "#fd0100"
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                        },
                                        "then": "C-[20%-30%)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                                },
                                                "then": "D-[30%-40%]",
                                                "else": "E-(>40)"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                //  "rango_incidencia": {
                //     "$cond": {
                //         "if": {
                //             "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                //         },
                //         "then": "[0%-3%]",
                //         "else": {
                //             "$cond": {
                //                 "if": {
                //                     "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia",6] }]
                //                 },
                //                 "then": "(3%-6%]",
                //                 "else": "(>6%)"
                //             }
                //         }
                //     }
                // }
            }
        }




        // ,{
        //     "$project": {
        //         "_id": {
        //             "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
        //         },
        //         "idform": "$_id.idform",
        //         "type": "Feature",
        //         "properties": {
        //             "Lote": "$_id.Lote",
        //             "Enfermedad": "$_id.Enfermedad",
        //             "Rango": "$rango_incidencia",

        //             "color": "$color"
        //         },
        //         "geometry": {
        //             //"$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
        //             "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
        //         }
        //     }
        // }



    ]

)