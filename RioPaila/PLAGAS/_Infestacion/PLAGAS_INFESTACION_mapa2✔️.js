[

    {
        "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "plaga": "$Plagas",
                "idform": "$idform"
            },
            "sum_larvas_x_plaga_x_lote": {
                "$sum": { "$toDouble": "$Numero de Larva" }
            },
            "num_palmas_censadas": {
                "$sum": 1
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "pct_infestacion": { "$multiply": [{ "$divide": ["$sum_larvas_x_plaga_x_lote", "$num_palmas_censadas"] }, 100] }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                    },
                    "then": "#00FF00",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                    },
                                    "then": "#1a73e8",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                            },
                                            "then": "#ff0000",
                                            "else": "#7B3F32"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                    },
                    "then": "A-[0%-10%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                            },
                            "then": "B-[10%-20%)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                    },
                                    "then": "C-[20%-30%)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                            },
                                            "then": "D-[30%-40%]",
                                            "else": "E-(>40)"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Plaga": "$_id.plaga",
                "Rango": "$rango",

                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
            }
        }
    }



]