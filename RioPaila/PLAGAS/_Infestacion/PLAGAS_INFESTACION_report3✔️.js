[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "form_lineapalmariopaila_aux",
            "let": {
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$form_lineapalmariopaila_aux",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
        }
    }

    , {
        "$project": {
            "form_lineapalmariopaila_aux": 0,
            "Formula": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "plaga": "$Plagas",
                "plantas_x_lote": "$plantas_x_lote"
            }
            , "sum_larvas_x_plaga_x_lote": {
                "$sum": { "$toDouble": "$Numero de Larva" }
            }
        }
    }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "sum_larvas_x_plaga_x_lote": "$sum_larvas_x_plaga_x_lote"
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "pct_infestacion": { "$multiply": [{ "$divide": ["$sum_larvas_x_plaga_x_lote", "$plantas_x_lote"] }, 100] }
        }
    }


    , {
        "$addFields": {
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                    },
                    "then": "A-[0%-10%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                            },
                            "then": "B-[10%-20%)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                    },
                                    "then": "C-[20%-30%)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                            },
                                            "then": "D-[30%-40%]",
                                            "else": "E-(>40)"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

]