[

    {
        "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },





    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "form_lineapalmariopaila_aux",
            "let": {

                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [

                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$form_lineapalmariopaila_aux",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
        }
    }

    , {
        "$project": {
            "form_lineapalmariopaila_aux": 0,
            "Formula": 0,

            "Evalucion de plagas": 0,

            "Point": 0,
            "Palma": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "plantas_x_lote": "$plantas_x_lote",
                "plaga": "$Plagas",
                "idform": "$idform"
            },
            "sum_larvas_x_plaga_x_lote": {
                "$sum": { "$toDouble": "$Numero de Larva" }
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }


    , {
        "$addFields": {
            "pct_infestacion": { "$multiply": [{ "$divide": ["$sum_larvas_x_plaga_x_lote", "$_id.plantas_x_lote"] }, 100] }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                    },
                    "then": "#00FF00",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                    },
                                    "then": "#1a73e8",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                            },
                                            "then": "#ff0000",
                                            "else": "#7B3F32"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_infestacion", 0] }, { "$lt": ["$pct_infestacion", 10] }]
                    },
                    "then": "A-[0%-10%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_infestacion", 10] }, { "$lt": ["$pct_infestacion", 20] }]
                            },
                            "then": "B-[10%-20%)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_infestacion", 20] }, { "$lt": ["$pct_infestacion", 30] }]
                                    },
                                    "then": "C-[20%-30%)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_infestacion", 30] }, { "$lte": ["$pct_infestacion", 40] }]
                                            },
                                            "then": "D-[30%-40%]",
                                            "else": "E-(>40)"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Plaga": "$_id.plaga",
                "Rango": "$rango",
                "%Infestacion": {
                    "$concat": [
                        { "$toString": "$pct_infestacion" },
                        " %"
                    ]
                },

                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
            }
        }
    }



]
