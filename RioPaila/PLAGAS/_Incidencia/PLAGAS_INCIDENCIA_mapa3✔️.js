[

    {
        "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "form_lineapalmariopaila_aux",
            "let": {
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$form_lineapalmariopaila_aux",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
        }
    }

    , {
        "$project": {
            "form_lineapalmariopaila_aux": 0,
            "Formula": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "plantas_x_lote": "$plantas_x_lote",
                "planta": "$Planta",
                "plaga": "$Plagas"
            }
            , "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote",
                "plantas_x_lote": "$_id.plantas_x_lote",

                "plaga": "$_id.plaga"
            }
            , "num_palmas_dif_censadas_x_lote_plaga": {
                "$sum": 1
            }
            , "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$addFields": {
            "pct_incidencia": { "$multiply": [{ "$divide": ["$num_palmas_dif_censadas_x_lote_plaga", "$_id.plantas_x_lote"] }, 100] }
        }
    }

    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lt": ["$pct_incidencia", 5] }]
                    },
                    "then": "#3ADF00",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_incidencia", 6] }, { "$lt": ["$pct_incidencia", 10] }]
                            },
                            "then": "#F7FE2E",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_incidencia", 11] }, { "$lt": ["$pct_incidencia", 16] }]
                                    },
                                    "then": "#0000FF",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_incidencia", 17] }, { "$lte": ["$pct_incidencia", 21] }]
                                            },
                                            "then": "#B8BAB6",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gte": ["$pct_incidencia", 22] }, { "$lte": ["$pct_incidencia", 30] }]
                                                    },
                                                    "then": "#080808",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gte": ["$pct_incidencia", 31] }, { "$lte": ["$pct_incidencia", 40] }]
                                                            },
                                                            "then": "#EB2020",
                                                            "else": "#7B3F32"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lt": ["$pct_incidencia", 5] }]
                    },
                    "then": "A-[0%-5%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pct_incidencia", 6] }, { "$lt": ["$pct_incidencia", 10] }]
                            },
                            "then": "B-[6%-10%)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$pct_incidencia", 11] }, { "$lt": ["$pct_incidencia", 16] }]
                                    },
                                    "then": "C-[11%-16%)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$pct_incidencia", 17] }, { "$lte": ["$pct_incidencia", 21] }]
                                            },
                                            "then": "D-[17%-21%]",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gte": ["$pct_incidencia", 22] }, { "$lt": ["$pct_incidencia", 30] }]
                                                    },
                                                    "then": "C-[22%-30%)",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gte": ["$pct_incidencia", 31] }, { "$lte": ["$pct_incidencia", 40] }]
                                                            },
                                                            "then": "D-[31%-40%]",
                                                            "else": "E-(>40)"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.data.elemnq", { "$subtract": [{ "$size": "$data.data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Plaga": "$_id.plaga",
                "Rango": "$rango",
                "%Incidencia": {
                    "$concat": [
                        { "$toString": "$pct_incidencia" },
                        " %"
                    ]
                },

                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": ["$data.data.Cartography.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.geometry" }, 1] }]
            }
        }
    }



]