//reporte
db.form_plagasriopaila.aggregate(

    [
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        , {
            "$lookup": {
                "from": "form_lineapalmariopaila",
                "as": "form_lineapalmariopaila_aux",
                "let": {
                    // "material": "$Materiales",
                    "lote": "$Lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    // { "$eq": ["$$material", "$MATERIAL"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$form_lineapalmariopaila_aux",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
            }
        }

        , {
            "$project": {
                "form_lineapalmariopaila_aux": 0,
                "Formula": 0
            }
        }


        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$Lote",
        //             // "planta": "$Planta",
        //             // "plaga": "$Plagas"
        //         },
        //         "num_palmas_censadas": {
        //             "$sum": 1
        //         },
        //         "data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }

        // , { "$unwind": "$data" }
        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "num_palmas_censadas": "$num_palmas_censadas"
        //                 }
        //             ]
        //         }
        //     }
        // }


        , {
            "$group": {
                "_id": {
                    "lote": "$Lote",
                    "plaga": "$Plagas",
                    // "num_palmas_censadas":"$num_palmas_censadas"
                    "plantas_x_lote": "$plantas_x_lote"
                },
                "num_palmas_censadas_x_lote_plaga": { "$sum": 1 }
            }
        }

        , {
            "$addFields": {
                //"pct_incidencia": {"$multiply":[{"$divide":["$num_palmas_censadas_x_lote_plaga","$_id.num_palmas_censadas"]},100]}
                "pct_incidencia": { "$multiply": [{ "$divide": ["$num_palmas_censadas_x_lote_plaga", "$_id.plantas_x_lote"] }, 100] }
            }
        }
    ]
)