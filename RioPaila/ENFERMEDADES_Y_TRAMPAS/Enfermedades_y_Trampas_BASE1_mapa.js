//---mapa
db.form_trampasderpalmarum.aggregate(
    [
        //-----❌ Borrar
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-02-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------

        //-----❌ Borrar
        //-----simular filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //---------------------------



        //------CARTOGRAFIA
        {
            "$addFields": {
                "variable_cartografia": "$Trampa"//---🚩EDITAR
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        //--id mapa
        {
            "$addFields": { "elemnq": { "$toObjectId": "$variable_cartografia.features._id" } }
        },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$project": {
                //"variable_cartografia": 0, //---geometria

                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Point": 0,
                "Formula": 0,
                "uid": 0,

                "Trampa": 0 //---🚩EDITAR
            }
        },


        //------COLOR
        {
            "$addFields": {
                "color": "#000000",
                "tipo": "Trampas",
                "elemento": { "$concat": ["#Trampa: ", { "$toString": "$Numero de trampa" }] },
                "sanidad": "$Plagas"
            }
        },



        //-----PROYECCION FINAL
        {
            "$project": {
                
                //---para cruzar fechas con otro form
                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",


                "_id": "$elemnq",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "color": "$color"

                    , "finca": "$finca"
                    , "bloque": "$bloque"
                    , "lote": "$lote"

                    , "elemento": "$elemento"
                    , "tipo": "$tipo"
                    , "sanidad": "$sanidad"

                    // , "anio": { "$toString": { "$year": "$Busqueda inicio" } }
                },
                "geometry": "$variable_cartografia.features.geometry"
            }
        }




        // //================ REPORTE CRUZADO DE MAPAS

        ,{
            "$group": {
                "_id": {
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin"
                }
                , "data_trampas": { "$push": "$$ROOT" }
            }
        },




        {
            "$lookup": {
                "from": "form_enfermedadesriopaila",
                "as": "data_enfermedades",
                "let": {
                    "filtro_fecha_inicio": "$_id.Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$_id.Busqueda fin"         //--filtro_fecha2
                },
                //query 
                "pipeline": [

                    //---filtro fechas 
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    //--------query enfermedades
                    //---idform enfermedades
                    {
                        "$addFields": { "idform": { "$toObjectId": "5e83a74ef73ac3547c912c68" } }
                    },

                    //---filtro sanidad
                    {
                        "$match": {
                            "Enfermedad": "Anillo    rojo"
                        }
                    }



                    //------CARTOGRAFIA
                    , {
                        "$addFields": {
                            "variable_cartografia": "$Palma"//---🚩EDITAR
                        }
                    },

                    { "$unwind": "$variable_cartografia.features" },

                    //--id mapa
                    {
                        "$addFields": { "elemnq": { "$toObjectId": "$variable_cartografia.features._id" } }
                    },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    //--arbol
                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },





                    {
                        "$project": {
                            //"variable_cartografia": 0, //---geometria

                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0,
                            "Point": 0,
                            "Formula": 0,
                            "uid": 0,

                            "Palma": 0 //---🚩EDITAR
                        }
                    },



                    //------COLOR
                    {
                        "$addFields": {
                            "color": "#ff0000",
                            "tipo": "Enfermedades",
                            "elemento": { "$concat": ["#Palma: ", { "$toString": "$arbol" }] },
                            "sanidad": "$Enfermedad"
                        }
                    },



                    //-----PROYECCION FINAL
                    {
                        "$project": {
                            "_id": "$elemnq",
                            "idform": "$idform",
                            "type": "Feature",
                            "properties": {
                                "color": "$color"

                                , "finca": "$finca"
                                , "bloque": "$bloque"
                                , "lote": "$lote"

                                , "elemento": "$elemento"
                                , "tipo": "$tipo"
                                , "sanidad": "$sanidad"

                            },
                            "geometry": "$variable_cartografia.features.geometry"
                        }
                    }


                ]
            }
        }


        //----unir los datos
        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data_trampas"
                        , "$data_enfermedades"
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        //===========================================
        
        
        
        ,{
            "$project": {
                "Busqueda inicio": 0,
                "Busqueda fin": 0
            }
        }





    ]

)