db.form_enfermedadesriopaila.aggregate(

    [

        //--Condicionales base
        {
            "$match": {
                "Enfermedad": { "$ne": "" }
            }
        },
        
        //---------ENFERMEDADES DE ALTO RIEGO
        //-----------------------------------
        // Anillo    rojo
        // Pudriccion basal
        // Marchitez letal
        // Marchitez sorpresiva
        //-----------------------------------
        {
            "$match": {
                "Enfermedad": { "$in":[
                        "Anillo    rojo",
                        "Pudriccion basal",
                        "Marchitez letal",
                        "Marchitez sorpresiva"
                    ] }
            }
        },
        

        //--Cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        //---plantas_dif_censadas_x_lote_x_enfermedad
        //--palmas afectadas (x lote x enfermedad)
        , {
            "$group": {
                "_id": {
                    "Enfermedad": "$Enfermedad",
                    "lote": "$Lote",
                    "planta": "$Planta"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Enfermedad": "$_id.Enfermedad",
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            //--palmas afectadas (x enfermedad x lote)
                            "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad"
                        }
                    ]
                }
            }
        }
        
        
        //---plantas_dif_censadas_x_lote
        //--palmas afectadas (x lote)
        , {
            "$group": {
                "_id": {
                    // "Enfermedad": "$Enfermedad",
                    "lote": "$Lote",
                    "planta": "$Planta"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    // "Enfermedad": "$_id.Enfermedad",
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            //--palmas afectadas (x enfermedad x lote)
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }
        
        
          , {
            "$addFields": {
                "pct_incidencia": { "$multiply": [
                    { "$divide": ["$plantas_dif_censadas_x_lote_x_enfermedad", "$plantas_dif_censadas_x_lote"] }
                    , 100] }
            }
        }

        
 

    ]
)