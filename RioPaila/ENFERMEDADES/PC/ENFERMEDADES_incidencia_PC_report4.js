//reporte PC
db.form_enfermedadesriopaila.aggregate(

    [

        {
            "$match": {
                "Enfermedad": { "$ne": "" }
            }
        },

        {
            "$match": {
                "Enfermedad": {
                    "$in": [
                        "Pudriccion de  Cogollo"
                    ]
                }
            }
        },


        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        , {
            "$lookup": {
                "from": "form_lineapalmariopaila",
                "as": "form_lineapalmariopaila_aux",
                "let": {
                    "lote": "$Lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$form_lineapalmariopaila_aux",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
            }
        }

        , {
            "$project": {
                "form_lineapalmariopaila_aux": 0,
                "Formula": 0,
                "Sampling": 0
            }
        }

        , {
            "$addFields": {
                "grado_PC": { "$ifNull": ["$Enfermedad_Pudriccion de  Cogollo", ""] }
            }
        }


        , {
            "$group": {
                "_id": {
                    "Enfermedad": "$Enfermedad",
                    "grado_PC": "$grado_PC",
                    "lote": "$Lote",
                    "plantas_x_lote": "$plantas_x_lote",
                    "planta": "$Planta"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Enfermedad": "$_id.Enfermedad",
                    "grado_PC": "$_id.grado_PC",
                    "plantas_x_lote": "$_id.plantas_x_lote",
                    "lote": "$_id.lote"
                }
                , "plantas_dif_censadas_x_lote_x_enfermedad_x_grado": { "$sum": 1 }
            }
        }


        , {
            "$addFields": {
                "pct_incidencia_grado": {
                    "$multiply": [
                        { "$divide": ["$plantas_dif_censadas_x_lote_x_enfermedad_x_grado", "$_id.plantas_x_lote"] }
                        , 100]
                }
            }
        }



        , {
            "$group": {
                "_id": {
                    "Enfermedad": "$_id.Enfermedad",
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": "$plantas_dif_censadas_x_lote_x_enfermedad_x_grado" }
                , "pct_incidencia_lote": { "$sum": "$pct_incidencia_grado" }
            }
        }


        , { "$unwind": "$data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad"
                            , "pct_incidencia_lote": { "$sum": "$pct_incidencia_lote" }
                        }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "rango_lote": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_incidencia_lote", 0] }, { "$lte": ["$pct_incidencia_lote", 0.04] }]
                        },
                        "then": "A-[0 - 0.04]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia_lote", 0.04] }, { "$lte": ["$pct_incidencia_lote", 0.07] }]
                                },
                                "then": "B-(0.04 - 0.07]",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gt": ["$pct_incidencia_lote", 0.007] }, { "$lte": ["$pct_incidencia_lote", 0.1] }]
                                        },
                                        "then": "C-(0.07 - 0.1]",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gt": ["$pct_incidencia_lote", 0.1] }, { "$lte": ["$pct_incidencia_lote", 0.5] }]
                                                },
                                                "then": "D-(0.1 - 0.5]",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gt": ["$pct_incidencia_lote", 0.5] }, { "$lte": ["$pct_incidencia_lote", 1] }]
                                                        },
                                                        "then": "E-(0.5 - 1.0]",
                                                        "else": {
                                                            "$cond": {
                                                                "if": {
                                                                    "$and": [{ "$gt": ["$pct_incidencia_lote", 1] }, { "$lte": ["$pct_incidencia_lote", 1.6] }]
                                                                },
                                                                "then": "F-(1.0 - 1.6]",
                                                                "else": {
                                                                    "$cond": {
                                                                        "if": {
                                                                            "$and": [{ "$gt": ["$pct_incidencia_lote", 1.6] }, { "$lte": ["$pct_incidencia_lote", 2.2] }]
                                                                        },
                                                                        "then": "G-(1.6 - 2.2]",
                                                                        "else": "H-(>2.2)"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_lote_x_enfermedad_x_grado": "$plantas_dif_censadas_x_lote_x_enfermedad_x_grado",
                            "pct_incidencia_grado": "$pct_incidencia_grado",
                            "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad",
                            "pct_incidencia_lote": "$pct_incidencia_lote",
                            "rango_lote": "$rango_lote"
                        }
                    ]
                }
            }
        }





    ]
)