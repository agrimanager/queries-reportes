[
    {
        "$match": {
            "Enfermedad": { "$ne": "" }
        }
    },

    {
        "$match": {
            "Enfermedad": {
                "$in": [
                    "Pudriccion de  Cogollo"
                ]
            }
        }
    },


    {
        "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },


    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "form_lineapalmariopaila_aux",
            "let": {
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$form_lineapalmariopaila_aux",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "plantas_x_lote": "$form_lineapalmariopaila_aux.NUMERO PALMAS  EN CAMPO CORREGIDO"
        }
    }

    , {
        "$project": {
            "form_lineapalmariopaila_aux": 0,
            "Formula": 0,
            "Sampling": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "Enfermedad": "$Enfermedad",
                "lote": "$Lote",
                "plantas_x_lote": "$plantas_x_lote",
                "planta": "$Planta"

                , "idform": "$idform"
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$group": {
            "_id": {
                "Enfermedad": "$_id.Enfermedad",
                "lote": "$_id.lote",
                "plantas_x_lote": "$_id.plantas_x_lote"

                , "idform": "$_id.idform"
            }
            , "data": { "$push": "$$ROOT" }
            , "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": 1 }
        }
    }


    , {
        "$addFields": {
            "pct_incidencia": {
                "$multiply": [
                    { "$divide": ["$plantas_dif_censadas_x_lote_x_enfermedad", "$_id.plantas_x_lote"] }
                    , 100]
            }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 0.04] }]
                    },
                    "then": "#008000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$pct_incidencia", 0.04] }, { "$lte": ["$pct_incidencia", 0.07] }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$pct_incidencia", 0.007] }, { "$lte": ["$pct_incidencia", 0.1] }]
                                    },
                                    "then": "#ff0000",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$pct_incidencia", 0.1] }, { "$lte": ["$pct_incidencia", 0.5] }]
                                            },
                                            "then": "#9c9c9c",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$pct_incidencia", 0.5] }, { "$lte": ["$pct_incidencia", 1] }]
                                                    },
                                                    "then": "#1a73e8",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$pct_incidencia", 1] }, { "$lte": ["$pct_incidencia", 1.6] }]
                                                            },
                                                            "then": "#000000",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$pct_incidencia", 1.6] }, { "$lte": ["$pct_incidencia", 2.2] }]
                                                                    },
                                                                    "then": "#ff0000",
                                                                    "else": "#3f3b69"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },

            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 0.04] }]
                    },
                    "then": "A-[0 - 0.04]",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$pct_incidencia", 0.04] }, { "$lte": ["$pct_incidencia", 0.07] }]
                            },
                            "then": "B-(0.04 - 0.07]",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$pct_incidencia", 0.007] }, { "$lte": ["$pct_incidencia", 0.1] }]
                                    },
                                    "then": "C-(0.07 - 0.1]",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$pct_incidencia", 0.1] }, { "$lte": ["$pct_incidencia", 0.5] }]
                                            },
                                            "then": "D-(0.1 - 0.5]",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$pct_incidencia", 0.5] }, { "$lte": ["$pct_incidencia", 1] }]
                                                    },
                                                    "then": "E-(0.5 - 1.0]",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$pct_incidencia", 1] }, { "$lte": ["$pct_incidencia", 1.6] }]
                                                            },
                                                            "then": "F-(1.0 - 1.6]",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$pct_incidencia", 1.6] }, { "$lte": ["$pct_incidencia", 2.2] }]
                                                                    },
                                                                    "then": "G-(1.6 - 2.2]",
                                                                    "else": "H-(>2.2)"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }



    , {
        "$project": {
            "_id": {
                "$arrayElemAt": [
                    { "$arrayElemAt": ["$data.data.elemnq", { "$subtract": [{ "$size": "$data.data.elemnq" }, 1] }] }
                    , 0]

            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Enfermedad": "$_id.Enfermedad",
                "Rango": "$rango",
                "%Incidencia": {
                    "$concat": [
                        { "$toString": "$pct_incidencia" },
                        " %"
                    ]
                },

                "color": "$color"
            },
            "geometry": {
                "$arrayElemAt": [
                    { "$arrayElemAt": ["$data.data.Cartography.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.geometry" }, 1] }] }
                    , 0]
            }
        }
    }



]