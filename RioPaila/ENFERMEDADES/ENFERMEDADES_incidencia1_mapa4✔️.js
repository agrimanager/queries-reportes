[

    {
        "$addFields": { "Cartography": "$Palma", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },



    {
        "$addFields": {
            "Cartography": "$Lote"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_polygon": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "Enfermedad": "$Enfermedad",
                "lote": "$Lote",
                "planta": "$Planta"
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$group": {
            "_id": {
                "Enfermedad": "$_id.Enfermedad",
                "lote": "$_id.lote"
            }
            , "data": { "$push": "$$ROOT" }
            , "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "planta": "$Planta"
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            }
            , "data": { "$push": "$$ROOT" }
            , "plantas_dif_censadas_x_lote": { "$sum": 1 }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "pct_incidencia": {
                "$multiply": [
                    { "$divide": ["$plantas_dif_censadas_x_lote_x_enfermedad", "$plantas_dif_censadas_x_lote"] }
                    , 100]
            }
        }
    }


    , {
        "$match": {
            "Enfermedad": {
                "$in": [
                    "Anillo    rojo",
                    "Pudriccion basal",
                    "Marchitez letal",
                    "Marchitez sorpresiva"
                ]
            }
        }
    }





    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "enfermedad": "$Enfermedad",
                "pct_incidencia": "$pct_incidencia",

                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }


    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$_id.pct_incidencia", 0] }, { "$lte": ["$_id.pct_incidencia", 0.04] }]
                    },
                    "then": "#008000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$_id.pct_incidencia", 0.04] }, { "$lte": ["$_id.pct_incidencia", 0.07] }]
                            },
                            "then": "#ffff00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 0.007] }, { "$lte": ["$_id.pct_incidencia", 0.1] }]
                                    },
                                    "then": "#ff0000",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$_id.pct_incidencia", 0.1] }, { "$lte": ["$_id.pct_incidencia", 0.5] }]
                                            },
                                            "then": "#9c9c9c",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 0.5] }, { "$lte": ["$_id.pct_incidencia", 1] }]
                                                    },
                                                    "then": "#1a73e8",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$_id.pct_incidencia", 1] }, { "$lte": ["$_id.pct_incidencia", 1.6] }]
                                                            },
                                                            "then": "#000000",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 1.6] }, { "$lte": ["$_id.pct_incidencia", 2.2] }]
                                                                    },
                                                                    "then": "#ff0000",
                                                                    "else": "#3f3b69"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },


            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$_id.pct_incidencia", 0] }, { "$lte": ["$_id.pct_incidencia", 0.04] }]
                    },
                    "then": "A-[0 - 0.04]",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$_id.pct_incidencia", 0.04] }, { "$lte": ["$_id.pct_incidencia", 0.07] }]
                            },
                            "then": "B-(0.04 - 0.07]",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 0.007] }, { "$lte": ["$_id.pct_incidencia", 0.1] }]
                                    },
                                    "then": "C-(0.07 - 0.1]",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$_id.pct_incidencia", 0.1] }, { "$lte": ["$_id.pct_incidencia", 0.5] }]
                                            },
                                            "then": "D-(0.1 - 0.5]",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 0.5] }, { "$lte": ["$_id.pct_incidencia", 1] }]
                                                    },
                                                    "then": "E-(0.5 - 1.0]",
                                                    "else": {
                                                        "$cond": {
                                                            "if": {
                                                                "$and": [{ "$gt": ["$_id.pct_incidencia", 1] }, { "$lte": ["$_id.pct_incidencia", 1.6] }]
                                                            },
                                                            "then": "F-(1.0 - 1.6]",
                                                            "else": {
                                                                "$cond": {
                                                                    "if": {
                                                                        "$and": [{ "$gt": ["$_id.pct_incidencia", 1.6] }, { "$lte": ["$_id.pct_incidencia", 2.2] }]
                                                                    },
                                                                    "then": "G-(1.6 - 2.2]",
                                                                    "else": "H-(>2.2)"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }



    , {
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Enfermedad": "$_id.enfermedad",
                "Rango": "$rango",

                "color": "$color"
            },
            "geometry": {

                "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
            }
        }
    }



]