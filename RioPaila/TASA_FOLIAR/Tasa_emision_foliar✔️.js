[


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palmas en observacin.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palmas en observacin.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "info_lote",
            "let": {
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_lote",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "plantas_x_lote": "$info_lote.NUMERO PALMAS  EN CAMPO CORREGIDO",
            "lineas_x_lote": "$info_lote.LINEAS POR LOTE",
            "material": "$info_lote.MATERIAL",
            "densidad": "$info_lote.DENSIDAD  DE PALMAS POR HA",
            "area_hectareas": "$info_lote.AREA NETA SEMBRADA"
        }
    }

    , {
        "$project": {
            "info_lote": 0,
            "Formula": 0,
            "Produccion": 0,
            "Point": 0,
            "uid": 0
        }
    }



    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
            "num_semana": { "$week": { "date": "$rgDate" } },
            "num_dia_semana": { "$dayOfWeek": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

            , "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }


            , "Dia_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                    ],
                    "default": "dia de la semana desconocido"
                }
            }
        }
    },


    { "$project": { "num_dia_semana": 0 } }



]