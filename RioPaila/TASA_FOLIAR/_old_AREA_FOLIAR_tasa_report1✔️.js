[

    {
        "$match": {
            "$expr": {
                "$or": [
                    { "$ne": ["$Numero de Muestra", "0"] },
                    { "$ne": ["$Numero hojas", "0"] },
                    { "$ne": ["$Numero de foliolos", "0"] }
                ]
            }
        }
    },

    {
        "$match": {
            "Palma muestreo.path": { "$ne": "" }
        }
    },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma muestreo.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma muestreo.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_lineapalmariopaila",
            "as": "form_lineapalmariopaila_aux",
            "let": {
                "material": "$Materiales",
                "lote": "$Lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$material", "$MATERIAL"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$form_lineapalmariopaila_aux",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "densidad_lote_material": "$form_lineapalmariopaila_aux.DENSIDAD  DE PALMAS POR HA"
        }
    }

    , {
        "$project": {
            "form_lineapalmariopaila_aux": 0,
            "Formula": 0,
            "Datos de Campo": 0
        }
    }


    , {
        "$addFields": {
            "medidas_ancho_largo": [
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 1 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolio 1 Ancho" }, 100] }
                },
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 2 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolo 2  Ancho" }, 100] }
                },
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 3 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolo 3 Ancho" }, 100] }
                },
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 4 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolo 4 Ancho" }, 100] }
                },
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 5 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolo 5 Ancho" }, 100] }
                },
                {
                    "largo": { "$divide": [{ "$toDouble": "$Foliolo 6 Largo" }, 100] },
                    "ancho": { "$divide": [{ "$toDouble": "$Foliolo 6 Ancho" }, 100] }
                }

            ]

        }
    }


    , {
        "$addFields": {
            "medidas_ancho_largo": {
                "$filter": {
                    "input": "$medidas_ancho_largo",
                    "as": "one_medidas_ancho_largo",
                    "cond": {
                        "$and": [
                            { "$ne": ["$$one_medidas_ancho_largo.ancho", 0] },
                            { "$ne": ["$$one_medidas_ancho_largo.largo", 0] }
                        ]
                    }
                }
            }
        }
    }

    , {
        "$match": {
            "medidas_ancho_largo": { "$ne": [] }
        }
    }



    , {
        "$addFields": {
            "medidas_area_foliar": {
                "$map": {
                    "input": "$medidas_ancho_largo",
                    "as": "one_medidas_ancho_largo",
                    "in": {
                        "$divide": [
                            { "$multiply": ["$$one_medidas_ancho_largo.largo", "$$one_medidas_ancho_largo.ancho"] }
                            , { "$toDouble": "$Numero de Muestra" }
                        ]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "medidas_area_hojas": {
                "$map": {
                    "input": "$medidas_area_foliar",
                    "as": "one_medidas_area_foliar",
                    "in": { "$multiply": ["$$one_medidas_area_foliar", { "$toDouble": "$Numero de foliolos" }] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "medidas_area_foliar_palma": {
                "$map": {
                    "input": "$medidas_area_hojas",
                    "as": "one_medidas_area_hojas",
                    "in": { "$multiply": ["$$one_medidas_area_hojas", { "$toDouble": "$Numero hojas" }, 0.54] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "promedio_medidas_area_foliar_palma": {
                "$avg": "$medidas_area_foliar_palma"
            }
        }
    }


    , {
        "$addFields": {
            "indice_area_foliar": {
                "$divide": [
                    { "$multiply": ["$promedio_medidas_area_foliar_palma", "$densidad_lote_material"] }
                    , 10000
                ]
            }
        }
    }


    , {
        "$project": {
            "medidas_ancho_largo": 0
        }
    }

     , {
        "$addFields": {
            "Numero de Muestra": { "$toDouble": "$Numero de Muestra" },
            "Numero hojas": { "$toDouble": "$Numero hojas" },
            "Numero de foliolos": { "$toDouble": "$Numero de foliolos" }
        }
    }





]