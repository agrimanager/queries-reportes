
// //-----------------------EJEMPLO1
// //---array con info
// var array_info = [
//     "form_controlcalidadcacaoplateomanual",
//     "form_palmaevaluacionesdecalidadprueba",
// ];

// array_info.forEach(i => {
//     //console.log(i)
//     //db.getCollection(i).drop();
//     db.forms.deleteMany(
//         {
//             "anchor": i
//         }
//     )
// })



//----PALMA

//-----------------------EJEMPLO2
//---array con info
var array_info = [
    { "lote": "A-16", "cod_lote": "14A16981" },
    { "lote": "A-17", "cod_lote": "14A17981" },
    { "lote": "A-18A", "cod_lote": "14A18A911" },
    { "lote": "A-18B", "cod_lote": "14A18B981" },
    { "lote": "A-18C", "cod_lote": "14A18C981" },
    { "lote": "A-19A", "cod_lote": "14A19A891" },
    { "lote": "A-19B", "cod_lote": "14A19B911" },
    { "lote": "A-20", "cod_lote": "14A20891" },
    { "lote": "A-21A", "cod_lote": "14A21A891" },
    { "lote": "A-21B", "cod_lote": "14A21B951" },
    { "lote": "A-22A", "cod_lote": "14A22A891" },
    { "lote": "A-22B", "cod_lote": "14A22B951" },
    { "lote": "A-23", "cod_lote": "14A23951" },
    { "lote": "A-24", "cod_lote": "14A24951" },
    { "lote": "A-25", "cod_lote": "14A25951" },
    { "lote": "A-26", "cod_lote": "14A26951" },
    { "lote": "A-27", "cod_lote": "14A27951" },
    { "lote": "A-MUISCAS", "cod_lote": "14AMSC981" },
    { "lote": "AP-18", "cod_lote": "14AP18891" },
    { "lote": "AP-19", "cod_lote": "14AP19891" },
    { "lote": "AP-20A", "cod_lote": "14AP20A891" },
    { "lote": "AP-20B", "cod_lote": "14AP20B961" },
    { "lote": "AP-20C", "cod_lote": "14AP20C001" },
    { "lote": "AP-20D", "cod_lote": "14AP20D101" },
    { "lote": "AP-21A", "cod_lote": "14AP21A951" },
    { "lote": "AP-21B", "cod_lote": "14AP21B961" },
    { "lote": "AP-21C", "cod_lote": "14AP21C101" },
    { "lote": "AP-22", "cod_lote": "14AP22951" },
    { "lote": "AP-23", "cod_lote": "14AP23951" },
    { "lote": "AP-24", "cod_lote": "14AP24951" },
    { "lote": "AP-25A", "cod_lote": "14AP25A951" },
    { "lote": "AP-25B", "cod_lote": "14AP25B001" },
    { "lote": "AP-26", "cod_lote": "14AP26951" },
    { "lote": "AP-CAPRICHO", "cod_lote": "14CHCP001" },
    { "lote": "B-20", "cod_lote": "14B20891" },
    { "lote": "B-21", "cod_lote": "14B21891" },
    { "lote": "B-22", "cod_lote": "14B22891" },
    { "lote": "B-23A", "cod_lote": "14B23A891" },
    { "lote": "B-23B", "cod_lote": "14B23B951" },
    { "lote": "B-24", "cod_lote": "14B24951" },
    { "lote": "B-25", "cod_lote": "14B25101" },
    { "lote": "B-26R", "cod_lote": "14B26131" },
    { "lote": "B-27R", "cod_lote": "14B27161" },
    { "lote": "B-28AR", "cod_lote": "14B28A161" },
    { "lote": "B-28B", "cod_lote": "14B28B951" },
    { "lote": "B-29AR", "cod_lote": "14B29A161" },
    { "lote": "B-29B", "cod_lote": "14B29B951" },
    { "lote": "B-30B", "cod_lote": "14B30B951" },
    { "lote": "C-23AR", "cod_lote": "14C23A121" },
    { "lote": "C-23B", "cod_lote": "14C23B981" },
    { "lote": "C-24R", "cod_lote": "14C24131" },
    { "lote": "C-25R", "cod_lote": "14C25161" },
    { "lote": "C-26R", "cod_lote": "14C26151" },
    { "lote": "C-27R", "cod_lote": "14C27151" },
    { "lote": "CHU-10R", "cod_lote": "14CH10141" },
    { "lote": "CHU-1AR", "cod_lote": "14CH01A141" },
    { "lote": "CHU-1BR", "cod_lote": "14CH01B141" },
    { "lote": "CHU-1R", "cod_lote": "14CH01141" },
    { "lote": "CHU-27R", "cod_lote": "14CH27131" },
    { "lote": "CHU-2R", "cod_lote": "14CH02131" },
    { "lote": "CHU-3DR", "cod_lote": "14CH03D111" },
    { "lote": "CHU-3IR", "cod_lote": "14CH03I111" },
    { "lote": "CHU-50R", "cod_lote": "14CH50121" },
    { "lote": "CHU-CRISTALINA", "cod_lote": "14CHCR991" },
    { "lote": "CHU-ISLR", "cod_lote": "14CHIS141" },
    { "lote": "CHU-PLR", "cod_lote": "14CHPL141" },
    { "lote": "CHU-SANJNT", "cod_lote": "14CHSJ991" },
    { "lote": "D-21R", "cod_lote": "14D21151" },
    { "lote": "D-22R", "cod_lote": "14D22151" },
    { "lote": "D-23R", "cod_lote": "14D23151" },
    { "lote": "D-24R", "cod_lote": "14D24161" },
    { "lote": "E-17R", "cod_lote": "14E17161" },
    { "lote": "E-18R", "cod_lote": "14E18161" },
    { "lote": "E-19R", "cod_lote": "14E19161" },
    { "lote": "E-20R", "cod_lote": "14E20161" },
    { "lote": "E-21R", "cod_lote": "14E21141" },
    { "lote": "E-22R", "cod_lote": "14E22111" },
    { "lote": "E-23R", "cod_lote": "14E23111" },
    { "lote": "E-24R", "cod_lote": "14E24131" },
    { "lote": "E-25R", "cod_lote": "14E25151" },
    { "lote": "E-26R", "cod_lote": "14E26151" },
    { "lote": "E-27R", "cod_lote": "14E27151" },
    { "lote": "E-28R", "cod_lote": "14E28151" },
    { "lote": "E-34R", "cod_lote": "14E34131" },
    { "lote": "F-15R", "cod_lote": "14F15111" },
    { "lote": "F-16R", "cod_lote": "14F16111" },
    { "lote": "F-17R", "cod_lote": "14F17151" },
    { "lote": "F-18R", "cod_lote": "14F18161" },
    { "lote": "F-19R", "cod_lote": "14F19161" },
    { "lote": "F-20AR", "cod_lote": "14F20A111" },
    { "lote": "F-20BR", "cod_lote": "14F20B121" },
    { "lote": "F-21AR", "cod_lote": "14F21A111" },
    { "lote": "F-21BR", "cod_lote": "14F21B121" },
    { "lote": "F-22AR", "cod_lote": "14F22A131" },
    { "lote": "F-22BR", "cod_lote": "14F22B131" },
    { "lote": "F-23AR", "cod_lote": "14F23A131" },
    { "lote": "F-23BR", "cod_lote": "14F23B131" },
    { "lote": "F-24AR", "cod_lote": "14F24A151" },
    { "lote": "F-24BR", "cod_lote": "14F24B131" },
    { "lote": "F-25R", "cod_lote": "14F25151" },
    { "lote": "F-26R", "cod_lote": "14F26151" },
    { "lote": "F-27R", "cod_lote": "14F27151" },
    { "lote": "F-28R", "cod_lote": "14F28141" },
    { "lote": "F-29R", "cod_lote": "14F29141" },
    { "lote": "F-30R", "cod_lote": "14F30141" },
    { "lote": "F-31R", "cod_lote": "14F31141" },
    { "lote": "F-32R", "cod_lote": "14F32141" },
    { "lote": "F-33R", "cod_lote": "14F33141" },
    { "lote": "G-11R", "cod_lote": "14G11131" },
    { "lote": "G-12R", "cod_lote": "14G12131" },
    { "lote": "G-13R", "cod_lote": "14G13131" },
    { "lote": "G-14R", "cod_lote": "14G14101" },
    { "lote": "G-15AR", "cod_lote": "14G15A081" },
    { "lote": "G-15BR", "cod_lote": "14G15B081" },
    { "lote": "G-16AR", "cod_lote": "14G16A081" },
    { "lote": "G-16BR", "cod_lote": "14G16B061" },
    { "lote": "G-17AR", "cod_lote": "14G17A081" },
    { "lote": "G-17BR", "cod_lote": "14G17B061" },
    { "lote": "G-18AR", "cod_lote": "14G18A081" },
    { "lote": "G-18BR", "cod_lote": "14G18B111" },
    { "lote": "G-19AR", "cod_lote": "14G19A101" },
    { "lote": "G-19BR", "cod_lote": "14G19B081" },
    { "lote": "G-20R", "cod_lote": "14G20111" },
    { "lote": "G-21R", "cod_lote": "14G21111" },
    { "lote": "G-22R", "cod_lote": "14G22131" },
    { "lote": "G-23R", "cod_lote": "14G23141" },
    { "lote": "G-24R", "cod_lote": "14G24141" },
    { "lote": "G-25R", "cod_lote": "14G25141" },
    { "lote": "G-26R", "cod_lote": "14G26141" },
    { "lote": "G-27R", "cod_lote": "14G27141" },
    { "lote": "H-10", "cod_lote": "14H10881" },
    { "lote": "H-11AR", "cod_lote": "14H11A101" },
    { "lote": "H-11B", "cod_lote": "14H11B881" },
    { "lote": "H-12AR", "cod_lote": "14H12A101" },
    { "lote": "H-12BR", "cod_lote": "14H12B131" },
    { "lote": "H-13AR", "cod_lote": "14H13A101" },
    { "lote": "H-13BR", "cod_lote": "14H13B131" },
    { "lote": "H-14AR", "cod_lote": "14H14A101" },
    { "lote": "H-15R", "cod_lote": "14H15111" },
    { "lote": "H-16R", "cod_lote": "14H16111" },
    { "lote": "H-17R", "cod_lote": "14H17121" },
    { "lote": "H-18R", "cod_lote": "14H18111" },
    { "lote": "H-19R", "cod_lote": "14H19121" },
    { "lote": "H-20R", "cod_lote": "14H20121" },
    { "lote": "H-21R", "cod_lote": "14H21121" },
    { "lote": "H-22R", "cod_lote": "14H22111" },
    { "lote": "H-23R", "cod_lote": "14H23111" },
    { "lote": "H-9R", "cod_lote": "14H09161" },
    { "lote": "I-10R", "cod_lote": "14I10161" },
    { "lote": "I-11R", "cod_lote": "14I11101" },
    { "lote": "I-12R", "cod_lote": "14I12101" },
    { "lote": "I-13R", "cod_lote": "14I13101" },
    { "lote": "I-14R", "cod_lote": "14I14101" },
    { "lote": "I-15R", "cod_lote": "14I15101" },
    { "lote": "I-16R", "cod_lote": "14I16101" },
    { "lote": "I-17R", "cod_lote": "14I17101" },
    { "lote": "I-5R", "cod_lote": "14I05161" },
    { "lote": "I-6B", "cod_lote": "14I06B081" },
    { "lote": "I-6R", "cod_lote": "14I06161" },
    { "lote": "I-7B", "cod_lote": "14I07B101" },
    { "lote": "I-7R", "cod_lote": "14I07161" },
    { "lote": "I-8R", "cod_lote": "14I08161" },
    { "lote": "I-9R", "cod_lote": "14I09161" },
    { "lote": "J-10R", "cod_lote": "14J10121" },
    { "lote": "J-11R", "cod_lote": "14J11121" },
    { "lote": "J-12R", "cod_lote": "14J12121" },
    { "lote": "J-13R", "cod_lote": "14J13121" },
    { "lote": "J-14R", "cod_lote": "14J14121" },
    { "lote": "J-9R", "cod_lote": "14J09121" },
    { "lote": "MP-1A", "cod_lote": "14MP01A991" },
    { "lote": "MP-1B", "cod_lote": "14MP01B991" },
    { "lote": "MP-1C", "cod_lote": "14MP01C991" },
    { "lote": "MP-2A", "cod_lote": "14MP02A991" },
    { "lote": "MP-2B", "cod_lote": "14MP02B991" },
    { "lote": "MP-3A", "cod_lote": "14MP03A991" },
    { "lote": "MP-3B", "cod_lote": "14MP03B991" },
    { "lote": "MP-3C", "cod_lote": "14MP03C991" },
    { "lote": "MP-4", "cod_lote": "14MP04991" },
    { "lote": "Vivero Palma", "cod_lote": "140101" },


];




array_info.forEach(i => {
    // console.log(i.cod_lote)

    db.form_cargueinformaciondeplantas.update(
        {
            "LOTE.features.properties.name": i.lote,
            // "CULTIVO": i.cultivo
            "CULTIVO": "PALMA"
        },
        {
            $set: {
                "CODIGO LOTE": i.cod_lote
            }
        }
    )
})






//-----CACAO

//-----------------------EJEMPLO2
//---array con info
var array_info = [
  {"lote": "B-30A","cod_lote": "15B30A17"},
{"lote": "B-31","cod_lote": "15B3117"},
{"lote": "B-32","cod_lote": "15B3217"},
{"lote": "B-33","cod_lote": "15B3317"},
{"lote": "B-34","cod_lote": "15B3417"},
{"lote": "C-28","cod_lote": "15C2817"},
{"lote": "C-29","cod_lote": "15C2917"},
{"lote": "C-30","cod_lote": "15C3017"},
{"lote": "C-31","cod_lote": "15C3117"},
{"lote": "C-32","cod_lote": "15C3217"},
{"lote": "C-33","cod_lote": "15C3317"},
{"lote": "C-34","cod_lote": "15C3417"},
{"lote": "C-35","cod_lote": "15C3517"},
{"lote": "D-25","cod_lote": "15D2517"},
{"lote": "D-26","cod_lote": "15D2617"},
{"lote": "D-27","cod_lote": "15D2717"},
{"lote": "D-28","cod_lote": "15D2817"},
{"lote": "D-29","cod_lote": "15D2917"},
{"lote": "D-30","cod_lote": "15D3017"},
{"lote": "D-31","cod_lote": "15D3117"},
{"lote": "D-32","cod_lote": "15D3217"},
{"lote": "D-33","cod_lote": "15D3317"},
{"lote": "D-34","cod_lote": "15D3417"},
{"lote": "E-29","cod_lote": "15E2917"},
{"lote": "E-30","cod_lote": "15E3017"},
{"lote": "E-31","cod_lote": "15E3117"},
{"lote": "E-32","cod_lote": "15E3217"},
{"lote": "E-33","cod_lote": "15E3317"},
{"lote": "AP-18","cod_lote": "15AP1818"},
{"lote": "AP-19","cod_lote": "15AP1918"},
{"lote": "AP-20A","cod_lote": "15AP20A18"},
{"lote": "A-18A","cod_lote": "15A18A18"},
{"lote": "A-19A","cod_lote": "15A19A18"},
{"lote": "A-19B","cod_lote": "15A19B18"},
{"lote": "A-20","cod_lote": "15A2018"},
{"lote": "B-20","cod_lote": "15B2018"},
{"lote": "B-21","cod_lote": "15B2118"},
{"lote": "B-22","cod_lote": "15B2218"},
{"lote": "B-23A","cod_lote": "15B23A18"},
{"lote": "H-10","cod_lote": "15H1018"},
{"lote": "H-11B","cod_lote": "15H11B18"},



];




array_info.forEach(i => {
    // console.log(i.cod_lote)

    db.form_cargueinformaciondeplantas.update(
        {
            "LOTE.features.properties.name": i.lote,
            // "CULTIVO": i.cultivo
            "CULTIVO": "CACAO"
        },
        {
            $set: {
                "CODIGO LOTE": i.cod_lote
            }
        }
    )
})
