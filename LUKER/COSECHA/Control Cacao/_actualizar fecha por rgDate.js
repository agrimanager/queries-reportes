

var cursor = db.form_registrocontrolcosechacacao.aggregate(

    {
        $match: {
            "Fecha de Registro": ISODate("1969-12-31T19:00:00.000-05:00")
        }
    }

);

cursor.forEach(i => {
    db.form_registrocontrolcosechacacao.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Fecha de Registro": i.rgDate
            }
        }
    )
})
