db.form_recolecciondecosechaxempleados.aggregate(
    {
        $addFields: {
            tipo_fecha: { $type: "$Fecha de puesta en caja" }
        }
    }
    ,{
        $match: {
            tipo_fecha:"string"
        }
    }
    ,{
        $group:{
            _id:"$tipo_fecha"
             ,data:{"$push":"$$ROOT"}
        }
    }
    
)