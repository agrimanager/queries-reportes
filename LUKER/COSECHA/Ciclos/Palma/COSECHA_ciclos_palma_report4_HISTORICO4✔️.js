[

    {
        "$addFields": {
            "variable_fecha": "$Fecha ciclo" 
        }
    },

    { "$addFields": { "type_filtro": { "$type": "$variable_fecha" } } },
    { "$match": { "type_filtro": { "$eq": "date" } } },

    { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    { "$sort": { "variable_fecha": 1 } },

    { "$addFields": { "variable_fecha_txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } } },




    {
        "$addFields": {
            "variable_cartografia": "$Lote" 
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "anio_filtro": 0
            , "type_filtro": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0

            , "Point": 0
            , "Lote": 0

        }
    }


    , {
        "$addFields": {
            "estado_ciclo": "$Ciclo cosecha" 
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$lote"
                , "fecha": "$variable_fecha_txt"
                , "estado": "$estado_ciclo"
            }
        }
    }

    , { "$sort": { "_id.fecha": 1, "_id.estado": -1 } }


    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$addFields": {
            "data_inicios": {
                "$filter": {
                    "input": "$data._id",
                    "as": "item_data",
                    "cond": {
                        "$eq": ["$$item_data.estado", "inicia"]
                    }
                }
            }
        }

    }

    , {
        "$addFields": {
            "data_fines": {
                "$filter": {
                    "input": "$data._id",
                    "as": "item_data",
                    "cond": {
                        "$eq": ["$$item_data.estado", "finaliza"]
                    }
                }
            }
        }

    }

    , {
        "$addFields": {
            "data_inicios_proximos": {
                "$reduce": {
                    "input": "$data_inicios",
                    "initialValue": [],
                    "in": {
                        "$cond": {
                            "if": { "$eq": ["$$value", []] },
                            "then": [
                                {
                                    "fecha_inicio_actual": "$$this.fecha",
                                    "fecha_inicio_anterior": null
                                }
                            ]
                            ,
                            "else": {
                                "$concatArrays": [
                                    "$$value",
                                    [
                                        {
                                            "fecha_inicio_actual": "$$this.fecha",
                                            "fecha_inicio_anterior": {
                                                "$arrayElemAt": ["$$value.fecha_inicio_actual", { "$subtract": [{ "$size": "$$value.fecha_inicio_actual" }, 1] }]
                                            }

                                        }
                                    ]
                                ]
                            }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields":
        {
            "data_inicios_proximos": {
                "$filter": {
                    "input": "$data_inicios_proximos",
                    "as": "item",
                    "cond": {
                        "$ne": ["$$item.fecha_inicio_anterior", null]
                    }
                }
            }
        }
    }


    , {
        "$addFields":
        {
            "data_ciclos": {
                "$map": {
                    "input": "$data_inicios_proximos",
                    "as": "item",
                    "in": {
                        "fecha_inicio_actual": "$$item.fecha_inicio_actual",
                        "fecha_inicio_anterior": "$$item.fecha_inicio_anterior",

                        "fecha_fin":
                        {
                            "$arrayElemAt": [
                            
                                {
                                    "$filter": {
                                        "input": "$data_fines.fecha",
                                        "as": "item_data_fin",
                                        "cond": {
                                            "$and": [
                                                {
                                                    "$gt": [
                                                        { "$toDate": "$$item.fecha_inicio_actual" }
                                                        ,
                                                        { "$toDate": "$$item_data_fin" }
                                                    ]
                                                },
                                                {
                                                    "$lte": [
                                                        { "$toDate": "$$item.fecha_inicio_anterior" }
                                                        ,
                                                        { "$toDate": "$$item_data_fin" }
                                                    ]
                                                }
                                            ]
                                        }
                                    }
                                }
                                , 0
                            ]
                        }
                    }
                }
            }
        }
    }





    , {
        "$addFields":
        {
            "data_ciclos": {
                "$map": {
                    "input": "$data_ciclos",
                    "as": "item",
                    "in": {
                        "fecha_inicio_actual": "$$item.fecha_inicio_actual",
                        "fecha_inicio_anterior": "$$item.fecha_inicio_anterior",
                        "fecha_fin": {
                            "$ifNull": ["$$item.fecha_fin", "--fecha_intermedia--"]
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields":
        {
            "data_ciclos": {
                "$filter": {
                    "input": "$data_ciclos",
                    "as": "item_ciclo",
                    "cond": {
                        "$ne": ["$$item_ciclo.fecha_fin", "--fecha_intermedia--"]
                    }
                }
            }
        }
    }

    , {
        "$project": {
            "data": 0
            , "data_inicios": 0
            , "data_fines": 0
            , "data_inicios_proximos": 0
        }
    }


    , {
        "$unwind": "$data_ciclos"
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    "$data_ciclos"
                ]
            }
        }
    }



    , {
        "$addFields": {
            "dias_ausencia_ciclo": {
                "$floor": {
                    "$divide": [
                        {
                            "$subtract": [
                                { "$toDate": "$fecha_inicio_actual" }
                                ,
                                { "$toDate": "$fecha_fin" }
                            ]
                        }
                        , 86400000
                    ]
                }
            }
        }
    }






]