//--reporte BASE DE CICLOS -- HISTORICO
db.form_cicloscosecha.aggregate(
    [

        //-----❌ Borrar
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-03-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------

        //-----❌ Borrar
        //-----SIMULACION DE FILTRO DE REPORTE CON FORMULARIO FECHAS INICIAL -
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }




        },
        //---------------------------


        // //====================NUEVO PROCESO BASE

        //-----✔️ EDITAR
        //---FECHAS
        {
            "$addFields": {
                "variable_fecha": "$Fecha ciclo" //🚩editar
            }
        },

        //--condiciones de fecha
        { "$addFields": { "type_filtro": { "$type": "$variable_fecha" } } },
        { "$match": { "type_filtro": { "$eq": "date" } } },

        { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        //--ordenar fecha
        { "$sort": { "variable_fecha": 1 } },

        { "$addFields": { "variable_fecha_txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } } },



        //-----✔️ EDITAR
        //---CARTOGRAFIA NEW
        {
            "$addFields": {
                "variable_cartografia": "$Lote" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "anio_filtro": 0
                , "type_filtro": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0

                , "Point": 0
                , "Lote": 0 //🚩editar

            }
        }


        //-----✔️ EDITAR
        //---ESTADO DE CICLO (inicia o finaliza)
        , {
            "$addFields": {
                //inicia , finaliza
                "estado_ciclo": "$Ciclo cosecha" //🚩editar
            }
        }


        //--test
        , {
            $match: {
                lote: "AP-25A"
                // lote:"CHU-2R"
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    // ,"today": "$today"
                    , "fecha": "$variable_fecha_txt"
                    , "estado": "$estado_ciclo"
                }
                // ,"data": {"$push": "$$ROOT"}
            }
        }

        //--ordenar
        , { "$sort": { "_id.fecha": 1, "_id.estado": -1 } }


        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"

                    // , "estado": "$_id.estado"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        // //--copiar data
        // , {9
        //     "$addFields": {
        //         "data_aux": "$data"
        //     }
        // }

        , {
            "$addFields": {
                "data_inicios": {
                    "$filter": {
                        "input": "$data._id",
                        "as": "item_data",
                        "cond": {
                            "$eq": ["$$item_data.estado", "inicia"]
                        }
                    }
                }
            }

        }
        
        
        
           //--new
        , {
            "$addFields": {
                //---reduce
                "data_inicios_proximos": {
                    "$reduce": {
                        "input": "$data_inicios",
                        "initialValue": [],
                        "in": {
                            "$cond": {
                                "if": { "$eq": ["$$value", []] },
                                "then": [
                                    {
                                        "fecha_inicio_actual": "$$this.fecha",
                                        "fecha_inicio_anterior": null
                                    }
                                ]
                                ,
                                "else": {
                                    $concatArrays: [
                                        "$$value",
                                        [
                                            {
                                                "fecha_inicio_actual": "$$this.fecha",
                                                // "fecha_inicio_anterior": "$$value.fecha_inicio_actual"
                                                // "fecha_inicio_anterior": { $slice: [ "$$value", 1 ] }
                                                // "fecha_inicio_anterior": { $slice: [ "$$value.fecha_inicio_actual", 1 ] }
                                                // "fecha_inicio_anterior": { $slice: [ "$$value.fecha_inicio_actual", {$size:"$$value"} ] }
                                                
                                                "fecha_inicio_anterior": {
                                                    // $slice: [ "$$value.fecha_inicio_actual", {$size:"$$value"} ] 
                                                    "$arrayElemAt": ["$$value.fecha_inicio_actual", { "$subtract": [{ "$size": "$$value.fecha_inicio_actual" }, 1] }]
                                                }
                                                
                                            }
                                        ] 
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
        
        
        //   //--new
        // , {
        //     "$addFields": {
        //         //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$value", []] },
        //                         "then": [
        //                             {
        //                                 "fecha_inicio_actual": "$$this.fecha",
        //                                 "fecha_inicio_anterior": null
        //                             }
        //                         ]
        //                         ,
        //                         "else": {
        //                             $concatArrays: [
        //                                 "$$value",
        //                                 [
        //                                     {
        //                                         "fecha_inicio_actual": "$$this.fecha",
        //                                         // "fecha_inicio_anterior": "$$value.fecha_inicio_actual"
        //                                         // "fecha_inicio_anterior": { $slice: [ "$$value", 1 ] }
        //                                         // "fecha_inicio_anterior": { $slice: [ "$$value.fecha_inicio_actual", 1 ] }
        //                                         "fecha_inicio_anterior": { $slice: [ "$$value.fecha_inicio_actual", {$size:"$$value"} ] }
        //                                     }
        //                                 ] 
        //                             ]
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        
        //  //--new
        // , {
        //     "$addFields": {
        //         //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$value", []] },
        //                         "then": [
        //                             {
        //                                 "fecha_inicio_actual": "$$this.fecha",
        //                                 "fecha_inicio_anterior": null
        //                             }
        //                         ]
        //                         ,
        //                         "else": {
        //                             $concatArrays: [
        //                                 "$$value",
        //                                 [
        //                                     {
        //                                         "fecha_inicio_actual": "$$this.fecha",
        //                                         "fecha_inicio_anterior": "$$value.fecha_inicio_actual"
        //                                     }
        //                                 ] 
        //                             ]
        //                         }
        //                         //"else": "proximo"
        //                         // "else": {
        //                         //     $concatArrays: [
        //                         //         "$$value",
        //                         //         ["$$this.fecha"] 
        //                         //     ]
        //                         // }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        


        // //--new
        // , {
        //     "$addFields": {
        //         //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$value", []] },
        //                         "then": [
        //                             {
        //                                 "fecha_inicio_actual": "$$this.fecha",
        //                                 "fecha_inicio_anterior": null
        //                             }
        //                         ]
        //                         ,
        //                         //"else": "proximo"
        //                         "else": {
        //                             $concatArrays: [
        //                                 "$$value",
        //                                 ["$$this.fecha"] 
        //                             ]
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }




        // //--new
        // , {
        //     "$addFields": {
        //         //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$value", []] },
        //                         "then": {
        //                             [
        //                                 {
        //                                     "fecha_inicio_actual":"$$this.fecha",
        //                                 }
        //                             ]
        //                         },
        //                         "else": "proximo"
        //                     }
        //                 }

        //                 // "in": {
        //                 //     "$cond": {
        //                 //         "if": {
        //                 //             "$eq": ["$$value", []]
        //                 //         },
        //                 //         "then": "inicio",
        //                 //         "else": "proximo"
        //                 //     }
        //                 // }

        //                 // "in" : {
        //                 //     $concatArrays: [
        //                 //             "$$value",
        //                 //             ["$$this.fecha"] 
        //                 //     ]
        //                 // }
        //                 // "in" : {
        //                 //     $concatArrays: [
        //                 //             "$$value",
        //                 //             ["$$this"] 
        //                 //     ]
        //                 // }
        //             }
        //         }
        //     }
        // }




        //   //--new
        // , {
        //     "$addFields": {
        //           //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in" : {
        //                     $concatArrays: [
        //                             "$$value",
        //                             ["$$this.fecha"] 
        //                     ]
        //                 }
        //                 // "in" : {
        //                 //     $concatArrays: [
        //                 //             "$$value",
        //                 //             ["$$this"] 
        //                 //     ]
        //                 // }
        //             }
        //         }
        //     }
        // }



        // //--new
        // , {
        //     "$addFields": {
        //           //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 // "in": "$$this"
        //                 "in" : {
        //                     $concatArrays: [ "$$value", ["$$this"] ]
        //                 }
        //             }
        //         }
        //     }
        // }





        // //--new
        // , {
        //     "$addFields": {
        //           //---reduce
        //         "data_inicios_proximos": {
        //             "$reduce": {
        //                 "input": "$data_inicios",
        //                 "initialValue": [],
        //                 "in": "$$this"
        //             }
        //         }
        //     }
        // }




        // //--new
        // , {
        //     "$addFields": {
        //           //---map
        //         "data_inicios_proximos": {
        //             "$map": {
        //                 "input": "$data_inicios",
        //                 "as": "item",
        //                 "in": "$$item"
        //                 // "in": {
        //                 //     "$cond": {
        //                 //         "if": { "$eq": ["$$item._id.estado", "inicia"] },
        //                 //         "then": {
        //                 //             "fecha_inicio": "$$item._id.fecha"
        //                 //             , "fecha_fin": null
        //                 //         },
        //                 //         "else": null
        //                 //     }
        //                 // }
        //             }
        //         }
        //     }
        // }



        // //--new
        // , {
        //     "$addFields": {



        //           //---map
        //         "ciclos_map": {
        //             "$map": {
        //                 "input": "$data",
        //                 "as": "item",
        //                 "in": {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$item._id.estado", "inicia"] },
        //                         "then": {
        //                             "fecha_inicio": "$$item._id.fecha"
        //                             , "fecha_fin": null
        //                         },
        //                         "else": null
        //                     }
        //                 }

        //             }
        //         }




        //         // //---map
        //         // "ciclos_map": {
        //         //     "$map": {
        //         //         "input": "$data",
        //         //         "as": "item",
        //         //         "in": {
        //         //             "$cond": {
        //         //                 "if": { "$eq": ["$$item._id.estado", "inicia"] },
        //         //                 "then": {
        //         //                     "fecha_inicio": "$$item._id.fecha"
        //         //                     , "fecha_fin": null
        //         //                 },
        //         //                 "else": null
        //         //             }
        //         //         }

        //         //     }
        //         // }


        //         // //---reduce
        //         // "ciclos_reduce": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": [],
        //         //         // "in": {
        //         //         //     "$cond": {
        //         //         //         "if": { "$eq": ["$$this._id.estado", "inicia"] },
        //         //         //         "then": "$$this._id.fecha",
        //         //         //         "else": "XXXX"
        //         //         //     }
        //         //         // }
        //         //         "in": {
        //         //              "collapsed": {
        //         //                 $concatArrays: [ "$$value._id", "$$this" ]
        //         //               },
        //         //               "firstValues": {
        //         //                 $concatArrays: [ "$$value.firstValues", { $slice: [ "$$this", 1 ] } ]
        //         //               }
        //         //         }

        //         //     }
        //         // }



        //         // //---reduce
        //         // "ciclos_reduce": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": [],
        //         //         "in": {
        //         //             "$cond": {
        //         //                 "if": { "$eq": ["$$this._id.estado", "inicia"] },
        //         //                 "then": "$$this._id.fecha",
        //         //                 "else": "XXX"
        //         //             }
        //         //         }

        //         //     }
        //         // }



        //         // //---map
        //         // "ciclos_map": {
        //         //     "$map": {
        //         //         "input": "$data",
        //         //         "as": "item",
        //         //         "in": {
        //         //             "$cond": {
        //         //                 "if": { "$eq": ["$$item._id.estado", "inicia"] },
        //         //                 "then": {
        //         //                     "fecha_inicio": "$$item._id.fecha"
        //         //                     , "fecha_fin": null
        //         //                 },
        //         //                 "else": null
        //         //             }
        //         //         }

        //         //     }
        //         // }


        //         //  //---map
        //         //  "ciclos_map": {
        //         //     "$map": {
        //         //         "input": "$data",
        //         //         "as": "item",
        //         //         "in": {
        //         //             "$cond":{
        //         //                 "if":{"$eq":["$$item._id.estado","inicia"]},
        //         //                 "then": {
        //         //                     "fecha_inicio": "$$item._id.fecha"
        //         //                     ,"fecha_fin": null
        //         //                 },
        //         //                 "else": null
        //         //             }
        //         //         }

        //         //     }
        //         // }


        //         // //---map
        //         //  "ciclos_map": {
        //         //     "$map": {
        //         //         "input": "$data",
        //         //         "as": "item",
        //         //         "in": {
        //         //             "$cond":{
        //         //                 "if":{"$eq":["$$item._id.estado","inicia"]},
        //         //                 "then": "$$item._id.fecha",
        //         //                 "else": null
        //         //             }
        //         //         }

        //         //     }
        //         // }


        //         // //---reduce
        //         //  "ciclos_reduce": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": [],
        //         //         "in": {
        //         //             "$cond":{
        //         //                 "if":{"$eq":["$$this._id.estado","inicia"]},
        //         //                 "then": {"$push":"$$this._id.fecha"},
        //         //                 "else": "XXX"
        //         //             }
        //         //         }

        //         //     }
        //         // }

        //     }
        // }

        //--new
        // , {
        //     "$addFields": {



        //          "ciclos": {
        //             "$reduce": {
        //                 "input": "$data",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$cond":{
        //                         "if":{"$eq":["$$this._id.estado","inicia"]},
        //                         "then": {"$push":"$$this._id.fecha"},
        //                         "else": "XXX"
        //                     }
        //                 }

        //             }
        //         }



        //         //  "ciclos": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": [],
        //         //         "in": {
        //         //             "$cond":{
        //         //                 //"if":{"$eq":["$$this._id.estado","inicia"]},
        //         //                 "if":{"$eq":["$$this._id.estado","finaliza"]},
        //         //                 "then":"$$this._id.fecha",
        //         //                 "else":""
        //         //             }
        //         //         }

        //         //     }
        //         // }

        //         // "ciclos": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": [],
        //         //         "in": "$$this._id.fecha"

        //         //         // in: {
        //         //         //     "collapsed": {
        //         //         //         $concatArrays: ["$$value.collapsed", "$$this"]
        //         //         //     },
        //         //         //     "firstValues": {
        //         //         //         $concatArrays: ["$$value.firstValues", { $slice: ["$$this", 1] }]
        //         //         //     }
        //         //         // }
        //         //     }
        //         // }

        //         // "ciclos": {
        //         //     "$reduce": {
        //         //         "input": "$data",
        //         //         "initialValue": "",
        //         //         "in": "$$this._id.fecha"
        //         //     }
        //         // }

        //     }
        // }



        // , {
        //     "$addFields": {
        //         "estado de ciclo": {
        //             "$reduce": {
        //                 "input": "$data.Ciclo cosecha",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "fecha inicio de ciclo": {
        //             "$reduce": {
        //                 "input": "$data.Fecha ciclo",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "rgDate": {
        //             "$reduce": {
        //                 "input": "$data.Fecha ciclo",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         },
        //         "Point": {
        //             "$reduce": {
        //                 "input": "$data.Point",
        //                 "initialValue": "",
        //                 "in": "$$this"
        //             }
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "penultimo_censo": {
        //             "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "fecha finalizacion de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "finaliza"] },
        //                 "then": "$fecha inicio de ciclo",
        //                 "else": ""
        //             }
        //         }

        //     }
        // },

        // {
        //     "$addFields": {
        //         "fecha inicio de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "inicia"] },
        //                 "then": "$fecha inicio de ciclo",
        //                 "else": "$penultimo_censo.Fecha ciclo"
        //             }
        //         }

        //     }
        // },


        // {
        //     "$addFields": {
        //         "dias de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "inicia"] },
        //                 "then": -1,
        //                 "else": {
        //                     "$floor": {
        //                         "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "estado de ciclo": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado de ciclo", "inicia"] },
        //                 "then": "En proceso",
        //                 "else": "Finalizado"
        //             }
        //         }
        //     }
        // },


        // {
        //     "$project": {
        //         "_id": 0,
        //         "lote": "$_id.lote",
        //         "estado de ciclo": "$estado de ciclo",
        //         "fecha inicio de ciclo": "$fecha inicio de ciclo",
        //         "fecha final de ciclo": "$fecha finalizacion de ciclo",
        //         "dias de ciclo": "$dias de ciclo",
        //         "supervisor": "$penultimo_censo.supervisor",
        //         "Point": "$Point",
        //         "rgDate": "$rgDate"
        //     }
        // },
        // {
        //     "$lookup": {
        //         "from": "form_cargueinformaciondeplantas",
        //         "as": "referencia_siembras",
        //         "let": {
        //             "nombre_lote": "$lote"
        //         },
        //         "pipeline": [
        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$eq": ["$CULTIVO", "PALMA"] },
        //                             { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
        //                             { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
        //                         ]
        //                     }
        //                 }
        //             },

        //             {
        //                 "$sort": {
        //                     "rgDate": -1
        //                 }
        //             },
        //             {
        //                 "$limit": 1
        //             }

        //         ]
        //     }
        // },

        // {
        //     "$unwind": {
        //         "path": "$referencia_siembras",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // },

        // {
        //     "$addFields": {
        //         "Hectareas": "$referencia_siembras.HECTAREAS",
        //         "Siembra": "$referencia_siembras.SIEMBRA",
        //         "num_palmas": "$referencia_siembras.PALMAS",
        //         "Material": "$referencia_siembras.MATERIAL"
        //     }
        // },

        // //---nueva (2020-06-03)
        // //En proceso: #666666, Ciclo cosecha de 0 a 9 dias: #2570A9, Ciclo cosecha de 10 a 12 dias: #9ACD32, Ciclo cosecha de 13 a 15 dias: #FFFD1F, Ciclo cosecha >= 16 dias: #E84C3F

        // {
        //     "$addFields": {
        //         "rango": {
        //             "$cond": {
        //                 "if": {
        //                     "$eq": ["$dias de ciclo", -1]
        //                 },
        //                 "then": "A-En proceso",
        //                 "else": {
        //                     "$cond": {
        //                         "if": {
        //                             "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
        //                         },
        //                         "then": "B-Ciclo cosecha de 0 a 9 dias",
        //                         "else": {
        //                             "$cond": {
        //                                 "if": {
        //                                     "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

        //                                 },
        //                                 "then": "C-Ciclo cosecha de 10 a 12 dias",
        //                                 "else": {
        //                                     "$cond": {
        //                                         "if": {
        //                                             "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
        //                                         },
        //                                         "then": "D-Ciclo cosecha de 13 a 15 dias",
        //                                         "else": "E-Ciclo cosecha >= 16 dias"
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // },

        // {
        //     "$project": {
        //         "referencia_siembras": 0
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "total_Ha": {
        //             "$sum": "$Hectareas"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "total_Ha": "$total_Ha"
        //                 }
        //             ]
        //         }
        //     }
        // }

    ]
)
