//--reporte BASE DE CICLOS -- HISTORICO
db.form_cicloscosecha.aggregate(
    [

        //-----❌ Borrar
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-03-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------

        //-----❌ Borrar
        //-----SIMULACION DE FILTRO DE REPORTE CON FORMULARIO FECHAS INICIAL -
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //---------------------------


        // //====================NUEVO PROCESO BASE

        //-----✔️ EDITAR
        //---FECHAS
        {
            "$addFields": {
                "variable_fecha": "$Fecha ciclo" //🚩editar
            }
        },

        //--condiciones de fecha
        { "$addFields": { "type_filtro": { "$type": "$variable_fecha" } } },
        { "$match": { "type_filtro": { "$eq": "date" } } },

        { "$addFields": { "anio_filtro": { "$year": "$variable_fecha" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        //--ordenar fecha
        { "$sort": { "variable_fecha": 1 } },

        { "$addFields": { "variable_fecha_txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } } },



        //-----✔️ EDITAR
        //---CARTOGRAFIA NEW
        {
            "$addFields": {
                "variable_cartografia": "$Lote" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "anio_filtro": 0
                , "type_filtro": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0

                , "Point": 0
                , "Lote": 0 //🚩editar

            }
        }


        //-----✔️ EDITAR
        //---ESTADO DE CICLO (inicia o finaliza)
        , {
            "$addFields": {
                //inicia , finaliza
                "estado_ciclo": "$Ciclo cosecha" //🚩editar
            }
        }


        // //--test
        // , {
        //     $match: {
        //         lote: "AP-25A"
        //         // lote:"CHU-2R"
        //     }
        // }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    // ,"today": "$today"
                    , "fecha": "$variable_fecha_txt"
                    , "estado": "$estado_ciclo"
                }
                // ,"data": {"$push": "$$ROOT"}
            }
        }

        //--ordenar
        , { "$sort": { "_id.fecha": 1, "_id.estado": -1 } }


        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                    // , "estado": "$_id.estado"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$addFields": {
                "data_inicios": {
                    "$filter": {
                        "input": "$data._id",
                        "as": "item_data",
                        "cond": {
                            "$eq": ["$$item_data.estado", "inicia"]
                        }
                    }
                }
            }

        }

        , {
            "$addFields": {
                "data_fines": {
                    "$filter": {
                        "input": "$data._id",
                        "as": "item_data",
                        "cond": {
                            "$eq": ["$$item_data.estado", "finaliza"]
                        }
                    }
                }
            }

        }


        //--new
        , {
            "$addFields": {
                //---reduce
                "data_inicios_proximos": {
                    "$reduce": {
                        "input": "$data_inicios",
                        "initialValue": [],
                        "in": {
                            "$cond": {
                                "if": { "$eq": ["$$value", []] },
                                "then": [
                                    {
                                        "fecha_inicio_actual": "$$this.fecha",
                                        "fecha_inicio_anterior": null
                                    }
                                ]
                                ,
                                "else": {
                                    "$concatArrays": [
                                        "$$value",
                                        [
                                            {
                                                "fecha_inicio_actual": "$$this.fecha",
                                                "fecha_inicio_anterior": {
                                                    "$arrayElemAt": ["$$value.fecha_inicio_actual", { "$subtract": [{ "$size": "$$value.fecha_inicio_actual" }, 1] }]
                                                }

                                            }
                                        ]
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }

        //--filtrar dato inicial
        , {
            "$addFields":
            {
                "data_inicios_proximos": {
                    "$filter": {
                        "input": "$data_inicios_proximos",
                        "as": "item",
                        "cond": {
                            "$ne": ["$$item.fecha_inicio_anterior", null]
                        }
                    }
                }
            }
        }



        //---agregar fecha fin de ciclo entre actual y anterior
        , {
            "$addFields":
            {
                "data_ciclos": {
                    "$map": {
                        "input": "$data_inicios_proximos",
                        "as": "item",
                        "in": {
                            "fecha_inicio_actual": "$$item.fecha_inicio_actual",
                            "fecha_inicio_anterior": "$$item.fecha_inicio_anterior",

                            //--encontrar fecha fin
                            "fecha_fin":
                            {
                                "$arrayElemAt": [
                                    //--array
                                    {
                                        "$filter": {
                                            "input": "$data_fines.fecha",
                                            "as": "item_data_fin",
                                            "cond": {
                                                "$and": [
                                                    //---condicional de fechas
                                                    {
                                                        // "$gte": [
                                                        "$gt": [
                                                            { "$toDate": "$$item.fecha_inicio_actual" }
                                                            ,
                                                            { "$toDate": "$$item_data_fin" }
                                                        ]
                                                    },
                                                    {
                                                        "$lte": [
                                                            { "$toDate": "$$item.fecha_inicio_anterior" }
                                                            ,
                                                            { "$toDate": "$$item_data_fin" }
                                                        ]
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                    // , { "$subtract": [{ "$size": "$$value.fecha_inicio_actual" }, 1] }
                                    , 0
                                ]
                            }
                        }
                    }
                }
            }
        }






        // //---eliminar datos intermedios ---ejemplo inicia -- inicia -- finaliza
        , {
            "$addFields":
            {
                "data_ciclos": {
                    "$map": {
                        "input": "$data_ciclos",
                        "as": "item",
                        "in": {
                            "fecha_inicio_actual": "$$item.fecha_inicio_actual",
                            "fecha_inicio_anterior": "$$item.fecha_inicio_anterior",
                            "fecha_fin": {
                                "$ifNull": ["$$item.fecha_fin", "--fecha_intermedia--"]
                            }
                        }
                    }
                }
            }
        }

        , {
            "$addFields":
            {
                "data_ciclos": {
                    "$filter": {
                        "input": "$data_ciclos",
                        "as": "item_ciclo",
                        "cond": {
                            "$ne": ["$$item_ciclo.fecha_fin", "--fecha_intermedia--"]
                        }
                    }
                }
            }
        }



        //======CALCULOS DE REPORTE

        // //.....
        // //.....
        , {
            "$project": {
                "data": 0
                , "data_inicios": 0
                , "data_fines": 0
                , "data_inicios_proximos": 0
            }
        }


        , {
            "$unwind": "$data_ciclos"
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        "$data_ciclos"
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "dias_ausencia_ciclo": {
                    "$floor": {
                        "$divide": [
                            {
                                "$subtract": [
                                    { "$toDate": "$fecha_inicio_actual" }
                                    ,
                                    { "$toDate": "$fecha_fin" }
                                ]
                            }
                            , 86400000
                        ]
                    }
                }
            }
        }






    ]
)
