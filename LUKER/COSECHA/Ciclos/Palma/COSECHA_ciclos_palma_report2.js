//--reporte
db.form_cicloscosecha.aggregate(
      [
        // // //-----
        // //{ $unwind: "$Lote.features" },
        // {
        //     "$addFields": {
        //         "today": new Date,
        //         //"Cartography": "$Lote.features"
        //     }
        // }, 
        
         
         //-----❌ Borrar
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-02-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "idform": "123",
            }
        },
        //---------------------

        //-----❌ Borrar
        //-----simular filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //---------------------------
          
        {
            "$sort": {
                "Fecha ciclo": 1
            }
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$addFields": {
                "Finca": "$Finca.name",
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"

            }
        },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "today": "$today"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "estado de ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo cosecha",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha inicio de ciclo": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "rgDate": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "Point": {
                    "$reduce": {
                        "input": "$data.Point",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },

        {
            "$addFields": {
                "penultimo_censo": {
                    "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                }
            }
        },

        {
            "$addFields": {
                "fecha finalizacion de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "finaliza"] },
                        "then": "$fecha inicio de ciclo",
                        "else": ""
                    }
                }

            }
        },

        {
            "$addFields": {
                "fecha inicio de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": "$fecha inicio de ciclo",
                        "else": "$penultimo_censo.Fecha ciclo"
                    }
                }

            }
        },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": -1,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "estado de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": "En proceso",
                        "else": "Finalizado"
                    }
                }
            }
        },


        {
            "$project": {
                "_id": 0,
                "lote": "$_id.lote",
                "estado de ciclo": "$estado de ciclo",
                "fecha inicio de ciclo": "$fecha inicio de ciclo",
                "fecha final de ciclo": "$fecha finalizacion de ciclo",
                "dias de ciclo": "$dias de ciclo",
                "supervisor":"$penultimo_censo.supervisor",
                "Point": "$Point",
                "rgDate": "$rgDate"
            }
        },
        {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "PALMA"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },
        
        //---nueva (2020-06-03)
        //En proceso: #666666, Ciclo cosecha de 0 a 9 dias: #2570A9, Ciclo cosecha de 10 a 12 dias: #9ACD32, Ciclo cosecha de 13 a 15 dias: #FFFD1F, Ciclo cosecha >= 16 dias: #E84C3F

        {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "A-En proceso",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                                },
                                "then": "B-Ciclo cosecha de 0 a 9 dias",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                        },
                                        "then": "C-Ciclo cosecha de 10 a 12 dias",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                                },
                                                "then": "D-Ciclo cosecha de 13 a 15 dias",
                                                "else": "E-Ciclo cosecha >= 16 dias"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        },

        {
            "$group": {
                "_id": null,
                "data": {
                    "$push": "$$ROOT"
                },
                "total_Ha": {
                    "$sum": "$Hectareas"
                }
            }
        },
        { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_Ha": "$total_Ha"
                        }
                    ]
                }
            }
        }

    ]
)
