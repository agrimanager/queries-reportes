//--report
db.form_ciclodecosechacacao.aggregate(
    [
        // //-----
        //{ $unwind: "$Lote.features" },
        {
            "$addFields": {
                "today": new Date,
                //"Cartography": "$Lote.features"
            }
        }, 
          
        {
            "$sort": {
                "Fecha ciclo": 1
            }
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$addFields": {
                "Finca": "$Finca.name",
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"

            }
        },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "today": "$today"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "estado de ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo cosecha",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha inicio de ciclo": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "rgDate": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "Point": {
                    "$reduce": {
                        "input": "$data.Point",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },

        {
            "$addFields": {
                "penultimo_censo": {
                    "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                }
            }
        },

        {
            "$addFields": {
                "fecha finalizacion de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Finalizar"] },
                        "then": "$fecha inicio de ciclo",
                        "else": ""
                    }
                }

            }
        },

        {
            "$addFields": {
                "fecha inicio de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicio"] },
                        "then": "$fecha inicio de ciclo",
                        "else": "$penultimo_censo.Fecha ciclo"
                    }
                }

            }
        },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicio"] },
                        "then": -1,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "estado de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicio"] },
                        "then": "En proceso",
                        "else": "Finalizado"
                    }
                }
            }
        },


        {
            "$project": {
                "_id": 0,
                "lote": "$_id.lote",
                "estado de ciclo": "$estado de ciclo",
                "fecha inicio de ciclo": "$fecha inicio de ciclo",
                "fecha final de ciclo": "$fecha finalizacion de ciclo",
                "dias de ciclo": "$dias de ciclo",
                "supervisor":"$penultimo_censo.supervisor",
                "Point": "$Point",
                "rgDate": "$rgDate"
            }
        },
        {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "CACAO"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        //En proceso: #666666, Ciclo cosecha de 0 a 9: #2570A9, Ciclo cosecha de 9 a 12: #9ACD32, Ciclo cosecha de 12 a 15: #FFFD1F, Ciclo cosecha > 15 :  #E84C3F
        //En proceso: #666666,0 a 9: #2570A9,9 a 12: #9ACD32,12 a 15: #FFFD1F,> 15 :  #E84C3F
        {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "A-En proceso",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lt": ["$dias de ciclo", 9] }]
                                },
                                "then": "B-Ciclo cosecha de 0 a 9",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 9] }, { "$lt": ["$dias de ciclo", 12] }]

                                        },
                                        "then": "C-Ciclo cosecha de 9 a 12",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$dias de ciclo", 12] }, { "$lt": ["$dias de ciclo", 15] }]
                                                },
                                                "then":"D-Ciclo cosecha de 12 a 15",
                                                "else": "E-Ciclo cosecha > 15"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        },

        {
            "$group": {
                "_id": null,
                "data": {
                    "$push": "$$ROOT"
                },
                "total_Ha": {
                    "$sum": "$Hectareas"
                }
            }
        },
        {"$unwind":"$data"}

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_Ha": "$total_Ha"
                        }
                    ]
                }
            }
        }

    ]  
)