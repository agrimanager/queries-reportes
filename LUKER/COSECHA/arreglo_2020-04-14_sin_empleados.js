
var cursor = db.form_recolecciondecosechaxempleados.aggregate(

    //---arreglo--2020-04-14
    // { "$match": { "Empleados de Cosecha": { "$exists": false } } },
    { "$match": { "Empleados de Cosecha": { "$eq": "" } } },
    // { "$addFields": { "empleados_filtro": { "$size": "$Empleados de Cosecha" } } },
    // { "$match": { "empleados_filtro": { "$eq": 0 } } },

);

cursor



// collection: users
{
	"_id" : ObjectId("5e9142359efa44297ecdfd32"),
	"Lote" : "F-28R",
	"Fecha de puesta en caja" : ISODate("2020-04-08T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 1,
	"Tractor" : "Jd15 jd16 ",
	"Codigo Vagon  Asociado a Despacho" : "ESC70",
	"Numero de Viaje de Vagon" : "01",
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Transportador",
	"Empleado adicional 1 cantidad" : 1412,
	"Empleado adicional 2 cantidad" : 0,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : "",
	"Empleado adicional 2" : "",
	"Empleado adicional 2 opcion" : "",
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : "P570 RAMON ANTONIO MARIN GARCIA",
	"rgDate" : ISODate("2020-04-10T22:18:53.000-05:00"),
	"uDate" : ISODate("2020-04-14T08:28:04.667-05:00"),
	"capture" : "W",
	"supervisor_u" : "ARENAS LOPEZ LEIDY MARCELA",
	"anio_filtro" : 2020,
	"Finca" : "Palma Villanueva Casanare",
	"Bloque" : "F",
	"lote" : "F-28R",
	"_id_str_farm" : "5d2648a845a0dd2e9e204fe2",
	"cultivo" : "PALMA",
	"Peso promedio lote" : 5.16,
	"empleados_cosecha_adicionales" : [
		{
			"_id" : "",
			"name" : "F999 - AGROINDUSTRIA FELEDA SA ",
			"reference" : "Transportador",
			"value" : 1412,
			"code" : "F999"
		}
	],
	"info_empleado_adicional" : [
		{
			"_id" : ObjectId("5dd56cacffe0f15cd5443e92"),
			"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
			"code" : "F999",
			"firstName" : "AGROINDUSTRIA FELEDA SA",
			"lastName" : "",
			"typeID" : "nit",
			"numberID" : "900557052-9",
			"email" : "",
			"receiveEmail" : true,
			"onlineAccess" : true,
			"onlineAccessKey" : "9217",
			"permissionProfile" : [ ],
			"contractType" : "services",
			"hireDate" : "",
			"contractTerminationDate" : "",
			"salary" : 0,
			"otherSalaryIncome" : 0,
			"otherNonSalaryIncome" : 0,
			"job" : "EMPRESA",
			"days" : null,
			"hoursByWeek" : 0,
			"transportationSubsidy" : false,
			"vacationDays" : 0,
			"periodicity" : "",
			"paymentMethods" : "",
			"bank" : "",
			"accountType" : "",
			"accountNumber" : "",
			"birthDate" : "",
			"homeAddress" : "",
			"cellphone" : "",
			"employeesProtections" : [ ],
			"fids" : [
				ObjectId("5d2648a845a0dd2e9e204fe2"),
				ObjectId("5d26491264f5b87ffc809eba")
			],
			"cid" : ObjectId("5d5c1ba13a8f8a3ef65d874e"),
			"status" : true,
			"rgDate" : ISODate("2019-11-20T11:41:16.318-05:00"),
			"uDate" : ISODate("2019-11-20T11:41:16.318-05:00")
		}
	],
	"empleados_cosecha_adicionales2" : [
		{
			"_id" : "5dd56cacffe0f15cd5443e92",
			"name" : "F999 - AGROINDUSTRIA FELEDA SA ",
			"reference" : "Transportador",
			"value" : 1412,
			"code" : "F999"
		}
	]
}