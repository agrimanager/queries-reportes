/* 1 createdAt:4/9/2020, 4:45:49 a. m.*/
{
	"_id" : ObjectId("5f520ccd42c8502a80dac602"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18a97",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8d,",
				"properties" : {
					"type" : "lot",
					"name" : "E-28R",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14E28151"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "E28R"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.83771108336,
								4.59496652979768
							],
							[
								-72.8378758000301,
								4.5970788172324
							],
							[
								-72.8290952784177,
								4.59774057223919
							],
							[
								-72.8289315988152,
								4.59562944389832
							],
							[
								-72.83771108336,
								4.59496652979768
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8d,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-03T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 2,
	"Tractor" : "JD11 KT19 JD12",
	"Codigo Vagon  Asociado a Despacho" : "ESC64",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 381,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 381,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "P564 - JOSE NIDIO ZAPATA CASTILLO",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf2602a",
			"value" : 42
		},
		{
			"name" : "N404 - GERMAN GOMEZ",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25f98",
			"value" : 54
		},
		{
			"name" : "N481 - JUAN DANIEL SAN JUAN CORRO",
			"reference" : "CortadorEncallador",
			"_id" : "5e9defa750ce4f0e63d8ca88",
			"value" : 39
		},
		{
			"name" : "P085 - RAMIRO ANTONIO HIGUERA PEREZ",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf26013",
			"value" : 35
		},
		{
			"name" : "P574 - CELSO VARGAS CAMACHO",
			"reference" : "CortadorEncallador",
			"_id" : "5de9046abb9fb44b83729afc",
			"value" : 50
		},
		{
			"name" : "P562 - JOSE LIBARDO COLLAZO VALENCIA",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf2600d",
			"value" : 33
		},
		{
			"name" : "P582 - EVER ALEXIS ALFONSO",
			"reference" : "CortadorEncallador",
			"_id" : "5ed6f34bd1e4c414dc80f993",
			"value" : 37
		},
		{
			"name" : "N426 - JOSE MAXIMILIANO RIVERO SUCRE",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25fbf",
			"value" : 66
		},
		{
			"name" : "N468 - JORGE ANDRES CEBALLO PEREZ",
			"reference" : "CortadorEncallador",
			"_id" : "5e9f066bbfbd7019aaabb8f9",
			"value" : 25
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-03T20:56:40.000-05:00"),
	"uDate" : ISODate("2020-09-04T11:29:03.691-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 2 createdAt:2/9/2020, 7:35:01 p. m.*/
{
	"_id" : ObjectId("5f503a35203d835e51b6b415"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5dcb0c8a6376b1f293895f16",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a92,",
				"properties" : {
					"type" : "lot",
					"name" : "H-11B",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14H11B881"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Nombre" : {
							"type" : "string",
							"value" : "H-11B"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 1897
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8082042224059,
								4.56107008439766
							],
							[
								-72.8076954962016,
								4.56109157233432
							],
							[
								-72.8068763607915,
								4.5611130602515
							],
							[
								-72.806290032281,
								4.56117322645765
							],
							[
								-72.8055226317322,
								4.56121620230535
							],
							[
								-72.8045569141944,
								4.56127207090535
							],
							[
								-72.803530839301,
								4.56129355883603
							],
							[
								-72.8032980324013,
								4.56129355883603
							],
							[
								-72.8033152773519,
								4.56095834716186
							],
							[
								-72.8032894099165,
								4.56058875461363
							],
							[
								-72.8033454560297,
								4.56019337632736
							],
							[
								-72.8034661707219,
								4.55996990154672
							],
							[
								-72.8035564186138,
								4.55960919085045
							],
							[
								-72.8035564186138,
								4.55934273993094
							],
							[
								-72.8035650411176,
								4.55911518948486
							],
							[
								-72.8038582054334,
								4.55906791601637
							],
							[
								-72.8046256059823,
								4.55903353522426
							],
							[
								-72.8056732370687,
								4.55898626165373
							],
							[
								-72.8062466318615,
								4.55895617845025
							],
							[
								-72.8066777557596,
								4.55892609526438
							],
							[
								-72.8075744934759,
								4.55887882168678
							],
							[
								-72.8079668162304,
								4.55886592888889
							],
							[
								-72.8080745972049,
								4.55886592888889
							],
							[
								-72.8081090871156,
								4.55892609526438
							],
							[
								-72.808156510744,
								4.5600520652677
							],
							[
								-72.8081651332193,
								4.56033140869597
							],
							[
								-72.8081996231545,
								4.56086860745494
							],
							[
								-72.8082042224059,
								4.56107008439766
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a92,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-02T00:00:00.000-05:00"),
	"Zona" : 2,
	"Hora de LLenado" : 2,
	"Codigo Vagon  Asociado a Despacho" : "ESC80",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 491,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 491,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "P111 - JOSE ANTONIO MILLAN ESCOBAR",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25ff8",
			"value" : 63
		},
		{
			"name" : "N384 - JOSE RODRIGO MARTINEZ CUESTA",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25fba",
			"value" : 55
		},
		{
			"name" : "P588 - WILLIAM CARABALLO MIRANDA",
			"reference" : "Cortador",
			"_id" : "5f2b00ed8f894a2e2f1c3f5c",
			"value" : 70
		},
		{
			"name" : "N401 - ARQUIMEDES ESQUIVEL TOVAR",
			"reference" : "Cortador",
			"_id" : "5d9682ad30493a16fbf25f57",
			"value" : 37
		},
		{
			"name" : "P589 - VICTOR ALFONSO CORTES SANCHEZ",
			"reference" : "Cortador",
			"_id" : "5f2b01328f894a2e2f1c3f7c",
			"value" : 76
		},
		{
			"name" : "P189 - EMILIANO VEGA VELASCO",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf26001",
			"value" : 54
		},
		{
			"name" : "P319 - JOSE MIYER QUINTERO RODRIGUEZ",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25fad",
			"value" : 46
		},
		{
			"name" : "P112 - FABIO ALONSO MINA NORIEGA",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25f76",
			"value" : 74
		},
		{
			"name" : "P595 - DAVID PRECIADO ANGULO",
			"reference" : "Cortador",
			"_id" : "5f2b04848f894a2e2f1c406c",
			"value" : 16
		},
		{
			"name" : "N356 - RAFAEL DIONISIO ARROYO PEÑARANDA",
			"reference" : "Encallador",
			"_id" : "5e1878f7ad1b6f5b8d9d46ed",
			"value" : 63
		},
		{
			"name" : "N389 - DIEGO FERNANDO CORDOBA YARA",
			"reference" : "Encallador",
			"_id" : "5e29ad4dc4340e02fa6a6534",
			"value" : 55
		},
		{
			"name" : "N405 - JAIME GERARDO MINA APONZA",
			"reference" : "Encallador",
			"_id" : "5e2aeac3c4340e02fa6a6e92",
			"value" : 70
		},
		{
			"name" : "N475 - JOVAN FERNEY MENDEZ MARIN",
			"reference" : "Encallador",
			"_id" : "5e9f096fbfbd7019aaabbb79",
			"value" : 37
		},
		{
			"name" : "N424 - LUIS ALBERTO BARRETO GAMEZ",
			"reference" : "Encallador",
			"_id" : "5d9682ad30493a16fbf25f3e",
			"value" : 76
		},
		{
			"name" : "P584 - JHON JAIRO VEGA VEGA",
			"reference" : "Encallador",
			"_id" : "5ed6f3b0d1e4c414dc80f99e",
			"value" : 54
		},
		{
			"name" : "N446 - CARLOS ANDRES VEGA SALCEDO",
			"reference" : "Encallador",
			"_id" : "5d9682ae30493a16fbf2601d",
			"value" : 46
		},
		{
			"name" : "N442 - MICHEL ROMARIO MINA VASQUEZ",
			"reference" : "Encallador",
			"_id" : "5e626974bb451f3ccf76ee7f",
			"value" : 74
		},
		{
			"name" : "P587 - UBERNEY PRECIADO ANGULO",
			"reference" : "Encallador",
			"_id" : "5ed6f4bdd1e4c414dc80f9bc",
			"value" : 16
		}
	],
	"Tractor" : "kt21",
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-02T19:33:39.000-05:00"),
	"uDate" : ISODate("2020-09-03T10:34:53.378-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 3 createdAt:2/9/2020, 7:33:51 p. m.*/
{
	"_id" : ObjectId("5f5039ef203d835e51b6b40c"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18ab3",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a88,",
				"properties" : {
					"type" : "lot",
					"name" : "F-33R",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14F33141"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "F33R"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8250248055524,
								4.60733585167157
							],
							[
								-72.824412522421,
								4.60695827915509
							],
							[
								-72.8245813818657,
								4.60654145155982
							],
							[
								-72.824638726246,
								4.60664226207305
							],
							[
								-72.8246946971697,
								4.60670278395959
							],
							[
								-72.8247662680101,
								4.60676125928776
							],
							[
								-72.8253795006257,
								4.60713808329899
							],
							[
								-72.8262227730746,
								4.60708473334566
							],
							[
								-72.829113794362,
								4.60686543837415
							],
							[
								-72.8295827389646,
								4.60683867800891
							],
							[
								-72.8297887472239,
								4.60912103519562
							],
							[
								-72.8297763660529,
								4.60916437452279
							],
							[
								-72.8297102841838,
								4.609201918788
							],
							[
								-72.8283704362683,
								4.60928982187365
							],
							[
								-72.8282337355277,
								4.60925363248407
							],
							[
								-72.8281784282685,
								4.6092126129289
							],
							[
								-72.8279511105231,
								4.60903447136492
							],
							[
								-72.8272984630158,
								4.60861187891091
							],
							[
								-72.8269120776171,
								4.60836438574693
							],
							[
								-72.8265160546645,
								4.60815047684455
							],
							[
								-72.8258853576192,
								4.60787003941038
							],
							[
								-72.8254519249327,
								4.60760513001812
							],
							[
								-72.8250248055524,
								4.60733585167157
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a88,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-02T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 2,
	"Tractor" : "Kt23 jD12",
	"Codigo Vagon  Asociado a Despacho" : "ESC67",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 634,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 634,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N437 - LUIS DIAZ ALGARRA",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ad30493a16fbf25ee1",
			"value" : 70
		},
		{
			"name" : "N468 - JORGE ANDRES CEBALLO PEREZ",
			"reference" : "CortadorEncallador",
			"_id" : "5e9f066bbfbd7019aaabb8f9",
			"value" : 107
		},
		{
			"name" : "N436 - LUZ MYRIAM SANTOS MELENDEZ",
			"reference" : "CortadorEncallador",
			"_id" : "5e332bf0b970955b03b28af0",
			"value" : 46
		},
		{
			"name" : "P594 - JAVIER ALBERTO PIRAGAUTA ROA",
			"reference" : "CortadorEncallador",
			"_id" : "5f2b044a8f894a2e2f1c404b",
			"value" : 50
		},
		{
			"name" : "P576 - HERIBERTO MENDOZA MURCIA",
			"reference" : "CortadorEncallador",
			"_id" : "5ed6593e768fc239ba56148b",
			"value" : 27
		},
		{
			"name" : "P242 - LUIS ERNESTO ARIAS ROMERO",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ad30493a16fbf25f68",
			"value" : 56
		},
		{
			"name" : "P573 - LUIS FERNANDO VIAFARA PALOMINO",
			"reference" : "CortadorEncallador",
			"_id" : "5de9057fbb9fb44b83729b0b",
			"value" : 92
		},
		{
			"name" : "P582 - EVER ALEXIS ALFONSO",
			"reference" : "CortadorEncallador",
			"_id" : "5ed6f34bd1e4c414dc80f993",
			"value" : 44
		},
		{
			"name" : "N481 - JUAN DANIEL SAN JUAN CORRO",
			"reference" : "CortadorEncallador",
			"_id" : "5e9defa750ce4f0e63d8ca88",
			"value" : 34
		},
		{
			"name" : "P574 - CELSO VARGAS CAMACHO",
			"reference" : "CortadorEncallador",
			"_id" : "5de9046abb9fb44b83729afc",
			"value" : 69
		},
		{
			"name" : "N404 - GERMAN GOMEZ",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25f98",
			"value" : 6
		},
		{
			"name" : "N454 - JHAIDER ARBEY RIVAS NARVAEZ",
			"reference" : "CortadorEncallador",
			"_id" : "5e95d5644bcff94d94518abd",
			"value" : 33
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-02T19:30:19.000-05:00"),
	"uDate" : ISODate("2020-09-03T11:01:48.594-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 4 createdAt:2/9/2020, 6:01:29 p. m.*/
{
	"_id" : ObjectId("5f502449203d835e51b6b347"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b16",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8d,",
				"properties" : {
					"type" : "lot",
					"name" : "E-26R",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14E26151"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "E26R"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8287645642889,
								4.59331676799529
							],
							[
								-72.8286012128102,
								4.59116331711707
							],
							[
								-72.8317884663247,
								4.59091619318803
							],
							[
								-72.8373758904391,
								4.59049868525852
							],
							[
								-72.8374746345346,
								4.59175043240867
							],
							[
								-72.8375276027212,
								4.59232957252106
							],
							[
								-72.8375547172848,
								4.59250919570732
							],
							[
								-72.8375587869182,
								4.59263089471266
							],
							[
								-72.8375508206334,
								4.59266562064388
							],
							[
								-72.8287645642889,
								4.59331676799529
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8d,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-02T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 1,
	"Tractor" : "kt19 kt21",
	"Codigo Vagon  Asociado a Despacho" : "ESC75",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 960,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 960,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N471 - JHON FREDY GARCIA BERRIO",
			"reference" : "CortadorEncallador",
			"_id" : "5e9f0772bfbd7019aaabb983",
			"value" : 30
		},
		{
			"name" : "N469 - HILTON HAIR LOPEZ CARRION",
			"reference" : "CortadorEncallador",
			"_id" : "5e9f06bcbfbd7019aaabb911",
			"value" : 96
		},
		{
			"name" : "N440 - OSCAR ORLANDO RUGE CESPEDES",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ad30493a16fbf25f01",
			"value" : 87
		},
		{
			"name" : "N483 - NIXON FABIAN AREVALO DAZA",
			"reference" : "CortadorEncallador",
			"_id" : "5e9def5a50ce4f0e63d8ca81",
			"value" : 40
		},
		{
			"name" : "N498 - WILMER MINA ARIAS",
			"reference" : "CortadorEncallador",
			"_id" : "5eb16b9c47258e79a0951b1e",
			"value" : 70
		},
		{
			"name" : "N383 - RUBISTEN CASTILLO MINA",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25f97",
			"value" : 58
		},
		{
			"name" : "P571 - JOSE DAVID MEDINA MONTEALEGRE",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf26020",
			"value" : 67
		},
		{
			"name" : "P586 - GUILLERMO JOSE VARELA OJITO",
			"reference" : "CortadorEncallador",
			"_id" : "5ed6f46bd1e4c414dc80f9b3",
			"value" : 43
		},
		{
			"name" : "N460 - JOSÉ VICENTE MENDOZA MURCIA",
			"reference" : "CortadorEncallador",
			"_id" : "5e95cf144bcff94d945187c3",
			"value" : 94
		},
		{
			"name" : "P580 - JEISSON ANDRES VEGA MOLANO",
			"reference" : "CortadorEncallador",
			"_id" : "5ed6ee3bd1e4c414dc80f918",
			"value" : 110
		},
		{
			"name" : "N439 - JEAN JAIR MONTAÑA CASANOVA",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25f95",
			"value" : 62
		},
		{
			"name" : "N463 - SANDRA PATRICIA FERNANDEZ FRANCO",
			"reference" : "CortadorEncallador",
			"_id" : "5e95d35e4bcff94d945189ac",
			"value" : 36
		},
		{
			"name" : "P572 - JAIDER JOBANY VEGA MOLANO",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf25fb7",
			"value" : 126
		},
		{
			"name" : "P317 - DUMAR MORENO",
			"reference" : "CortadorEncallador",
			"_id" : "5d9682ae30493a16fbf26022",
			"value" : 41
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-02T17:53:10.000-05:00"),
	"uDate" : ISODate("2020-09-03T08:47:34.163-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 5 createdAt:2/9/2020, 4:14:08 p. m.*/
{
	"_id" : ObjectId("5f500b20203d835e51b6b262"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b07",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,",
				"properties" : {
					"type" : "lot",
					"name" : "MP-2B",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14MP02B991"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "MP-2B"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8086603344176,
								4.55550110213732
							],
							[
								-72.8086612470764,
								4.55516727490359
							],
							[
								-72.8088095188726,
								4.55524720976521
							],
							[
								-72.8090393741992,
								4.55539218611922
							],
							[
								-72.8092319233571,
								4.55549773906721
							],
							[
								-72.8094261521382,
								4.55554986967807
							],
							[
								-72.809587304417,
								4.55571973878893
							],
							[
								-72.8098779513633,
								4.5560680958342
							],
							[
								-72.810001183059,
								4.55601313031032
							],
							[
								-72.8099256128452,
								4.55589793127351
							],
							[
								-72.8097849886112,
								4.55585582574584
							],
							[
								-72.8096819385695,
								4.55569367265541
							],
							[
								-72.8096134937482,
								4.55554389875615
							],
							[
								-72.8099673931002,
								4.55532150887158
							],
							[
								-72.810681655214,
								4.55504091665409
							],
							[
								-72.8107163077475,
								4.5551385756964
							],
							[
								-72.8107702909487,
								4.55520858738614
							],
							[
								-72.8109150093994,
								4.55532682303976
							],
							[
								-72.8110192581891,
								4.55549210768402
							],
							[
								-72.8111062623096,
								4.55572045230762
							],
							[
								-72.8111754025612,
								4.55603811583768
							],
							[
								-72.8112915698417,
								4.55636232925457
							],
							[
								-72.8113499635846,
								4.55662810611818
							],
							[
								-72.8113978629546,
								4.55696720526356
							],
							[
								-72.8113852583993,
								4.55711700026868
							],
							[
								-72.811482141661,
								4.55749233341543
							],
							[
								-72.8115557090886,
								4.55763141458062
							],
							[
								-72.8117040999337,
								4.55778916592497
							],
							[
								-72.8118754171003,
								4.5579341964754
							],
							[
								-72.8119654775718,
								4.55797177315289
							],
							[
								-72.8120309920496,
								4.55802957738047
							],
							[
								-72.812170800337,
								4.55823289428616
							],
							[
								-72.8120730397092,
								4.55839181277424
							],
							[
								-72.8118939073988,
								4.55863508640463
							],
							[
								-72.8118197232001,
								4.55869788494181
							],
							[
								-72.8115047889988,
								4.55887516566587
							],
							[
								-72.8112988608752,
								4.55873666667206
							],
							[
								-72.8107778469843,
								4.55841255955376
							],
							[
								-72.8106778005053,
								4.5583529093887
							],
							[
								-72.8104039830673,
								4.55817274849711
							],
							[
								-72.8102997017492,
								4.55810403686343
							],
							[
								-72.8102151489051,
								4.558010939915
							],
							[
								-72.8101960131166,
								4.55796987485055
							],
							[
								-72.8101925411937,
								4.55793819314658
							],
							[
								-72.8101961887129,
								4.55790630294502
							],
							[
								-72.8102082864614,
								4.55785948554705
							],
							[
								-72.8103335223378,
								4.55754787785215
							],
							[
								-72.8103750021131,
								4.55746084431228
							],
							[
								-72.8103914377338,
								4.55742919320635
							],
							[
								-72.810401981233,
								4.55739638265625
							],
							[
								-72.8104076542962,
								4.55730957084966
							],
							[
								-72.8103953396908,
								4.55723662696931
							],
							[
								-72.8103722937635,
								4.55716999196664
							],
							[
								-72.8103374591683,
								4.55711534265157
							],
							[
								-72.8102238690979,
								4.5569560798955
							],
							[
								-72.8100244779499,
								4.5567265445101
							],
							[
								-72.8099403596833,
								4.55661220574538
							],
							[
								-72.8094938400116,
								4.55609623814011
							],
							[
								-72.8093918519403,
								4.55597182033752
							],
							[
								-72.8093085561663,
								4.55587210310359
							],
							[
								-72.8092536106123,
								4.55581539357102
							],
							[
								-72.8089461618757,
								4.55564317124968
							],
							[
								-72.8086603344176,
								4.55550110213732
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-02T00:00:00.000-05:00"),
	"Zona" : 2,
	"Hora de LLenado" : 2,
	"Tractor" : "Jd11 KT13",
	"Codigo Vagon  Asociado a Despacho" : "ESC68",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 249,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 249,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N497 - JHON FREDY CORTES SANCHEZ",
			"reference" : "Cortador",
			"_id" : "5eb16b2047258e79a0951af8",
			"value" : 61
		},
		{
			"name" : "N476 - EULICES MORA",
			"reference" : "Cortador",
			"_id" : "5e9f09b2bfbd7019aaabbb8e",
			"value" : 63
		},
		{
			"name" : "P585 - LISANDRO ESTEBAN LOPEZ PARRA",
			"reference" : "Cortador",
			"_id" : "5ed6f41ad1e4c414dc80f9a8",
			"value" : 53
		},
		{
			"name" : "N499 - JAS WILDER BOTIA HUMAY",
			"reference" : "Cortador",
			"_id" : "5eb16bdc47258e79a0951b2e",
			"value" : 72
		},
		{
			"name" : "N466 - JOSE DANIRO PARADA MORENO",
			"reference" : "Encallador",
			"_id" : "5e95cf524bcff94d945187da",
			"value" : 61
		},
		{
			"name" : "N511 - ROSALBA PRADA BEDOYA",
			"reference" : "Encallador",
			"_id" : "5ebd5dda3a8d3a04bb41fe25",
			"value" : 63
		},
		{
			"name" : "N411 - JUAN CARLOS GONZALEZ LAGOS",
			"reference" : "Encallador",
			"_id" : "5d9682ad30493a16fbf25f41",
			"value" : 53
		},
		{
			"name" : "N377 - EDGAR NARANJO GONZALEZ",
			"reference" : "Encallador",
			"_id" : "5e29d98cc4340e02fa6a6777",
			"value" : 72
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-02T16:07:34.000-05:00"),
	"uDate" : ISODate("2020-09-03T09:49:54.853-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 6 createdAt:2/9/2020, 4:14:08 p. m.*/
{
	"_id" : ObjectId("5f500b20203d835e51b6b25d"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b2f",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,",
				"properties" : {
					"type" : "lot",
					"name" : "MP-1C",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14MP01C991"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "MP1C"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8086533403062,
								4.55549392742204
							],
							[
								-72.808653337063,
								4.55549714689061
							],
							[
								-72.8077344628483,
								4.5550408423378
							],
							[
								-72.8076846513825,
								4.55515749711492
							],
							[
								-72.8085263269921,
								4.5555862826135
							],
							[
								-72.808557566166,
								4.55562943986748
							],
							[
								-72.8085698233721,
								4.55565832894614
							],
							[
								-72.807931963761,
								4.55689401559093
							],
							[
								-72.8076351444018,
								4.55748169443794
							],
							[
								-72.8075934143331,
								4.55752188048212
							],
							[
								-72.8075697881647,
								4.55751529012502
							],
							[
								-72.8075785890721,
								4.5574451270192
							],
							[
								-72.8075866348847,
								4.55739747846522
							],
							[
								-72.8075352145615,
								4.5573366907428
							],
							[
								-72.8075091990149,
								4.55733249468177
							],
							[
								-72.8074959091489,
								4.55740045553187
							],
							[
								-72.8074828789669,
								4.55740946129073
							],
							[
								-72.8074602207892,
								4.55740074913743
							],
							[
								-72.8059415202287,
								4.55625456049041
							],
							[
								-72.8059168284613,
								4.55622078654463
							],
							[
								-72.8059061189737,
								4.55617036291109
							],
							[
								-72.8059196055512,
								4.55615557848158
							],
							[
								-72.8059000366931,
								4.55613527531209
							],
							[
								-72.8063213800281,
								4.55446298846959
							],
							[
								-72.8076846492506,
								4.55515749602884
							],
							[
								-72.8077344616717,
								4.5550408417489
							],
							[
								-72.8065355016054,
								4.55443949369406
							],
							[
								-72.8065007705757,
								4.55442141181015
							],
							[
								-72.8064640135089,
								4.55439176383615
							],
							[
								-72.8064458879157,
								4.5543528034903
							],
							[
								-72.8064408034154,
								4.55429026881309
							],
							[
								-72.8064466478139,
								4.55419014257607
							],
							[
								-72.8064565726415,
								4.55405602014994
							],
							[
								-72.8064543290315,
								4.55390016635177
							],
							[
								-72.8065122096551,
								4.55384710000015
							],
							[
								-72.8066419736036,
								4.55396754599917
							],
							[
								-72.8068200641992,
								4.55414707092821
							],
							[
								-72.8068325866405,
								4.55420772356282
							],
							[
								-72.8070561575306,
								4.55435238026917
							],
							[
								-72.8071538175414,
								4.5543500249417
							],
							[
								-72.8073413049851,
								4.55441847918684
							],
							[
								-72.8076965896358,
								4.55464918152497
							],
							[
								-72.8083090915075,
								4.5549835938453
							],
							[
								-72.8086536415862,
								4.55516392848789
							],
							[
								-72.8086533403062,
								4.55549392742204
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-02T00:00:00.000-05:00"),
	"Zona" : 2,
	"Hora de LLenado" : 2,
	"Tractor" : "Jd11 KT13",
	"Codigo Vagon  Asociado a Despacho" : "ESC86",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 282,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 282,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "P585 - LISANDRO ESTEBAN LOPEZ PARRA",
			"reference" : "Cortador",
			"_id" : "5ed6f41ad1e4c414dc80f9a8",
			"value" : 78
		},
		{
			"name" : "P560 - HERNANDO ALBERTO SAURITH SOTO",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25ffe",
			"value" : 84
		},
		{
			"name" : "P111 - JOSE ANTONIO MILLAN ESCOBAR",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25ff8",
			"value" : 58
		},
		{
			"name" : "P583 - LUIS CARLOS AVILA TOLOZA",
			"reference" : "Cortador",
			"_id" : "5ed69f80490ee45c022d8998",
			"value" : 62
		},
		{
			"name" : "N411 - JUAN CARLOS GONZALEZ LAGOS",
			"reference" : "Encallador",
			"_id" : "5d9682ad30493a16fbf25f41",
			"value" : 140
		},
		{
			"name" : "N373 - GONZALEZ AYA JULIAN",
			"reference" : "Encallador",
			"_id" : "5e29d086c4340e02fa6a66e1",
			"value" : 84
		},
		{
			"name" : "N377 - EDGAR NARANJO GONZALEZ",
			"reference" : "Encallador",
			"_id" : "5e29d98cc4340e02fa6a6777",
			"value" : 58
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-02T15:58:00.000-05:00"),
	"uDate" : ISODate("2020-09-03T09:07:15.836-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 7 createdAt:1/9/2020, 10:16:20 p. m.*/
{
	"_id" : ObjectId("5f4f0e84ea79e455cc9dce31"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5dcb062417bfe96ac8adb315",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a87,",
				"properties" : {
					"type" : "lot",
					"name" : "AP-20A",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14AP20A891"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Nombre" : {
							"type" : "string",
							"value" : "AP-20A"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8800315761024,
								4.57363328207325
							],
							[
								-72.8799574578142,
								4.57396040537969
							],
							[
								-72.8795450596315,
								4.57402191919304
							],
							[
								-72.8793821052995,
								4.57398446392105
							],
							[
								-72.8793720088243,
								4.57386972953282
							],
							[
								-72.8792927036413,
								4.57383101122363
							],
							[
								-72.8791851031742,
								4.57378689450431
							],
							[
								-72.8792052130245,
								4.57389163758356
							],
							[
								-72.8791190576001,
								4.57401230890051
							],
							[
								-72.8790202097026,
								4.57415192955762
							],
							[
								-72.878505301825,
								4.5744130955247
							],
							[
								-72.8779576650488,
								4.57453132486329
							],
							[
								-72.8772000664558,
								4.57476828887617
							],
							[
								-72.8765323225318,
								4.57499194485631
							],
							[
								-72.8759821595027,
								4.57507797888621
							],
							[
								-72.8757015082685,
								4.5752195152384
							],
							[
								-72.8752119412342,
								4.57539832004285
							],
							[
								-72.8745186555446,
								4.57570131854628
							],
							[
								-72.874204798611,
								4.57594327431681
							],
							[
								-72.8737606770288,
								4.57614855809163
							],
							[
								-72.8732105402737,
								4.57638549464325
							],
							[
								-72.8725049206915,
								4.57672435561462
							],
							[
								-72.8724589661337,
								4.57612445198433
							],
							[
								-72.8723789844553,
								4.57514433613633
							],
							[
								-72.872576622924,
								4.5749500578105
							],
							[
								-72.8726348367517,
								4.57481926144794
							],
							[
								-72.8725959909908,
								4.57467053517613
							],
							[
								-72.872368772046,
								4.57468876457678
							],
							[
								-72.8723326151091,
								4.5741999595204
							],
							[
								-72.8723370361523,
								4.57392546353047
							],
							[
								-72.8728953599905,
								4.57384453279249
							],
							[
								-72.8731151288486,
								4.57378233111771
							],
							[
								-72.873278383913,
								4.57372638879902
							],
							[
								-72.8733905423968,
								4.5737015374399
							],
							[
								-72.8734841156553,
								4.57372638879902
							],
							[
								-72.8736336030243,
								4.57380098682115
							],
							[
								-72.8738522535793,
								4.57385075550275
							],
							[
								-72.8739872267304,
								4.57384294782906
							],
							[
								-72.8741192211068,
								4.57388884386823
							],
							[
								-72.8742512821449,
								4.57395318368737
							],
							[
								-72.8744330106489,
								4.57395011762421
							],
							[
								-72.8746237181672,
								4.57396238323453
							],
							[
								-72.8747899722778,
								4.57399914659182
							],
							[
								-72.8749224890964,
								4.57405429862659
							],
							[
								-72.8750735775954,
								4.57405122508301
							],
							[
								-72.8751535923167,
								4.57402664492089
							],
							[
								-72.8753783217052,
								4.5739928708992
							],
							[
								-72.8755321929067,
								4.57397446025742
							],
							[
								-72.8757711357346,
								4.57396832518863
							],
							[
								-72.8759986353365,
								4.57394072859663
							],
							[
								-72.8761922373033,
								4.57391927723701
							],
							[
								-72.8763858711089,
								4.57390396163134
							],
							[
								-72.87663768333,
								4.57387946839859
							],
							[
								-72.8769240106352,
								4.57387949678785
							],
							[
								-72.8771670574041,
								4.57385191646559
							],
							[
								-72.8773423368978,
								4.57383353976766
							],
							[
								-72.877582248208,
								4.57382129315715
							],
							[
								-72.87777921045,
								4.57382129315715
							],
							[
								-72.877868138016,
								4.57380292999238
							],
							[
								-72.8780155324676,
								4.57378763357224
							],
							[
								-72.8782089423832,
								4.57376928529156
							],
							[
								-72.8785617415525,
								4.5737452916417
							],
							[
								-72.8788389282603,
								4.57372379271558
							],
							[
								-72.8790265166328,
								4.57369616757775
							],
							[
								-72.879204901452,
								4.57367776101866
							],
							[
								-72.8793989721963,
								4.57367776101866
							],
							[
								-72.8795618360288,
								4.57365936259565
							],
							[
								-72.8798447136212,
								4.57364097233037
							],
							[
								-72.879933557858,
								4.57363909013365
							],
							[
								-72.8800315761024,
								4.57363328207325
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a87,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-01T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 1,
	"Tractor" : "Jd16",
	"Codigo Vagon  Asociado a Despacho" : "ESC30",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Transportador",
	"Empleado adicional 1 cantidad" : 453,
	"Empleado adicional 2 cantidad" : 0,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "P565 - WILLIAM RICHAR AVENDAÑO PEÑA",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25f9c",
			"value" : 140
		},
		{
			"name" : "N413 - WILLIAM JOSE GRANADOS DAZA",
			"reference" : "Cortador",
			"_id" : "5e2ae8bac4340e02fa6a6e51",
			"value" : 83
		},
		{
			"name" : "P592 - MIGUEL ANGEL VARGAS CAMACHO",
			"reference" : "Cortador",
			"_id" : "5f2b03788f894a2e2f1c401f",
			"value" : 53
		},
		{
			"name" : "N364 - MIGUEL ANGEL AREVALO TORRES",
			"reference" : "Cortador",
			"_id" : "5dfb9de65cad3a5e0d7927d7",
			"value" : 82
		},
		{
			"name" : "P570 - RAMON ANTONIO MARIN GARCIA",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf26015",
			"value" : 25
		},
		{
			"name" : "P575 - RAFAEL REYES RIVERA",
			"reference" : "Cortador",
			"_id" : "5de904c8bb9fb44b83729b05",
			"value" : 70
		},
		{
			"name" : "N500 - WILIAM ALEXANDER TORRES ESPEJO",
			"reference" : "Encallador",
			"_id" : "5eb16c1c47258e79a0951b3c",
			"value" : 153
		},
		{
			"name" : "N474 - PASTOR EVELIO ALDANA QUINTERO",
			"reference" : "Encallador",
			"_id" : "5e9f0858bfbd7019aaabba00",
			"value" : 155
		},
		{
			"name" : "N415 - JUAN SEBASTIAN BEJARANO RODRIGUEZ",
			"reference" : "Encallador",
			"_id" : "5e2ae767c4340e02fa6a6e2b",
			"value" : 88
		},
		{
			"name" : "P593 - ILDER ESNIVER MEDINA PABON",
			"reference" : "Encallador",
			"_id" : "5f2b03d38f894a2e2f1c4037",
			"value" : 57
		},
		{
			"name" : "F999 - AGROINDUSTRIA FELEDA SA  ",
			"reference" : "Alzador",
			"_id" : "5dd56cacffe0f15cd5443e92",
			"value" : 453
		}
	],
	"Empleado adicional 2" : "",
	"Empleado adicional 2 opcion" : "",
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-01T22:15:25.000-05:00"),
	"uDate" : ISODate("2020-09-02T08:58:21.564-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 8 createdAt:1/9/2020, 8:01:40 p. m.*/
{
	"_id" : ObjectId("5f4eeef4ea79e455cc9dcd59"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18ae7",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a88,",
				"properties" : {
					"type" : "lot",
					"name" : "F-22AR",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : ""
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14F22A131"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : "1"
						},
						"Nombre" : {
							"type" : "string",
							"value" : "F22AR"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : ""
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : ""
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : ""
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8220712470281,
								4.58273461798587
							],
							[
								-72.8222424652976,
								4.58271994605992
							],
							[
								-72.8222966608097,
								4.582728798975
							],
							[
								-72.8223306019182,
								4.58275799482921
							],
							[
								-72.8228457647233,
								4.58334645740194
							],
							[
								-72.8234868136644,
								4.58409303367407
							],
							[
								-72.8238765483587,
								4.58454572618809
							],
							[
								-72.8239287662185,
								4.58461689222842
							],
							[
								-72.8239566202216,
								4.58467161245189
							],
							[
								-72.8239343675131,
								4.58468256553101
							],
							[
								-72.8238919574312,
								4.58468955564962
							],
							[
								-72.8191375717226,
								4.58504326737208
							],
							[
								-72.8189868430176,
								4.58294661403372
							],
							[
								-72.820508250295,
								4.58284984963182
							],
							[
								-72.8220712470281,
								4.58273461798587
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a88,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-01T00:00:00.000-05:00"),
	"Zona" : 2,
	"Hora de LLenado" : 2,
	"Tractor" : "Kt13 Jd11 ",
	"Codigo Vagon  Asociado a Despacho" : "ESC02",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 358,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 358,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N399 - JAIRO RODRIGUEZ COLMENARES",
			"reference" : "CortadorEncallador",
			"_id" : "5e31a018b970955b03b27e07",
			"value" : 71
		},
		{
			"name" : "N414 - URIEL INOCENCIO TUMAY",
			"reference" : "CortadorEncallador",
			"_id" : "5e2ae93ec4340e02fa6a6e6b",
			"value" : 70
		},
		{
			"name" : "N491 - LEANDRO ACEVEDO PLAZAS",
			"reference" : "CortadorEncallador",
			"_id" : "5ea1a63f7c28d51c5c84395f",
			"value" : 149
		},
		{
			"name" : "N492 - DUMAR ROBERTO ACEVEDO PLAZAS",
			"reference" : "CortadorEncallador",
			"_id" : "5ea1a66e7c28d51c5c843967",
			"value" : 68
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-01T19:04:50.000-05:00"),
	"uDate" : ISODate("2020-09-02T11:10:42.187-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 9 createdAt:1/9/2020, 8:01:40 p. m.*/
{
	"_id" : ObjectId("5f4eeef4ea79e455cc9dcd36"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b0b",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,",
				"properties" : {
					"type" : "lot",
					"name" : "MP-3C",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14MP03C991"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "MP3C"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8140967490374,
								4.56260272949607
							],
							[
								-72.8140301233567,
								4.56246832038885
							],
							[
								-72.8142378742634,
								4.56041846876972
							],
							[
								-72.8143894833538,
								4.56049422101323
							],
							[
								-72.8149258204967,
								4.56074836208631
							],
							[
								-72.8152485420461,
								4.56088734093437
							],
							[
								-72.8157024042845,
								4.56123411017728
							],
							[
								-72.8158350896805,
								4.561299316303
							],
							[
								-72.8160547976542,
								4.56145758817612
							],
							[
								-72.8162079975989,
								4.5615909599887
							],
							[
								-72.8162916276965,
								4.56170403388962
							],
							[
								-72.8163452351494,
								4.56178279541743
							],
							[
								-72.816428992129,
								4.56191170603775
							],
							[
								-72.8164775668109,
								4.56205531755602
							],
							[
								-72.8164962330339,
								4.56209605556091
							],
							[
								-72.8165353848826,
								4.56212366437639
							],
							[
								-72.816573452099,
								4.56219080121731
							],
							[
								-72.8166499214534,
								4.56233847345673
							],
							[
								-72.816713990451,
								4.56240523965491
							],
							[
								-72.8167446657618,
								4.56251236898926
							],
							[
								-72.8162245985819,
								4.56395090987968
							],
							[
								-72.8161931840247,
								4.56411911942346
							],
							[
								-72.8161609706556,
								4.56414907923413
							],
							[
								-72.8161006592012,
								4.56415543148945
							],
							[
								-72.816024817917,
								4.56421742441812
							],
							[
								-72.8152795395469,
								4.56363461255367
							],
							[
								-72.8151054540583,
								4.56346092648824
							],
							[
								-72.8150196723507,
								4.56339376151033
							],
							[
								-72.8149327632432,
								4.56335129866522
							],
							[
								-72.8147954507138,
								4.56326466351939
							],
							[
								-72.8145335004823,
								4.56304330925332
							],
							[
								-72.8143404328986,
								4.562909406477
							],
							[
								-72.8141262228067,
								4.56268318890701
							],
							[
								-72.8140967490374,
								4.56260272949607
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a91,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-01T00:00:00.000-05:00"),
	"Zona" : 2,
	"Hora de LLenado" : 2,
	"Tractor" : "Kt13 Jd11 ",
	"Codigo Vagon  Asociado a Despacho" : "ESC02",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 508,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 508,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N476 - EULICES MORA",
			"reference" : "Cortador",
			"_id" : "5e9f09b2bfbd7019aaabbb8e",
			"value" : 199
		},
		{
			"name" : "N435 - CIRO FREDY CAICEDO LANDAZURI",
			"reference" : "Cortador",
			"_id" : "5e332bbdb970955b03b28ae8",
			"value" : 96
		},
		{
			"name" : "N497 - JHON FREDY CORTES SANCHEZ",
			"reference" : "Cortador",
			"_id" : "5eb16b2047258e79a0951af8",
			"value" : 132
		},
		{
			"name" : "N499 - JAS WILDER BOTIA HUMAY",
			"reference" : "Cortador",
			"_id" : "5eb16bdc47258e79a0951b2e",
			"value" : 81
		},
		{
			"name" : "N511 - ROSALBA PRADA BEDOYA",
			"reference" : "Encallador",
			"_id" : "5ebd5dda3a8d3a04bb41fe25",
			"value" : 199
		},
		{
			"name" : "N466 - JOSE DANIRO PARADA MORENO",
			"reference" : "Encallador",
			"_id" : "5e95cf524bcff94d945187da",
			"value" : 35
		},
		{
			"name" : "N493 - EDGAR JOSE MARTINEZ RANGEL",
			"reference" : "Encallador",
			"_id" : "5ea1a7947c28d51c5c84398d",
			"value" : 132
		},
		{
			"name" : "N377 - EDGAR NARANJO GONZALEZ",
			"reference" : "Encallador",
			"_id" : "5e29d98cc4340e02fa6a6777",
			"value" : 81
		},
		{
			"name" : "N373 - GONZALEZ AYA JULIAN",
			"reference" : "Encallador",
			"_id" : "5e29d086c4340e02fa6a66e1",
			"value" : 61
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-01T18:47:59.000-05:00"),
	"uDate" : ISODate("2020-09-02T11:09:21.690-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 10 createdAt:1/9/2020, 5:45:53 p. m.*/
{
	"_id" : ObjectId("5f4ecf21ea79e455cc9dc9f1"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b19",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8b,",
				"properties" : {
					"type" : "lot",
					"name" : "A-22B",
					"production" : true,
					"status" : true,
					"order" : 0,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14A22B951"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "A22B"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8686660164245,
								4.57942456041914
							],
							[
								-72.8686861449055,
								4.57943681911814
							],
							[
								-72.8687211135752,
								4.57967689215472
							],
							[
								-72.8687769924637,
								4.57970885904893
							],
							[
								-72.8688451688403,
								4.57973709573862
							],
							[
								-72.868863465131,
								4.57995385261003
							],
							[
								-72.8688166332374,
								4.5799812911412
							],
							[
								-72.8688143139598,
								4.58000854160083
							],
							[
								-72.8688275462773,
								4.58003666145916
							],
							[
								-72.8688676283993,
								4.58003867584921
							],
							[
								-72.8688802538995,
								4.58006508230081
							],
							[
								-72.8688809668829,
								4.58009756483614
							],
							[
								-72.8688702530914,
								4.5801331151097
							],
							[
								-72.8688830931784,
								4.58018433391751
							],
							[
								-72.8688259040554,
								4.58023802549233
							],
							[
								-72.8687701532233,
								4.58027138155594
							],
							[
								-72.8687448185177,
								4.58027893120093
							],
							[
								-72.8687552044726,
								4.58032263318971
							],
							[
								-72.8687674618083,
								4.58034589757085
							],
							[
								-72.8687409734766,
								4.58037651470748
							],
							[
								-72.8687145426723,
								4.58040706534968
							],
							[
								-72.8686960949854,
								4.58041303593485
							],
							[
								-72.8686984130045,
								4.58043969315456
							],
							[
								-72.8686990033183,
								4.58046929745766
							],
							[
								-72.8686842460791,
								4.58048706881384
							],
							[
								-72.8686325504399,
								4.58050757030764
							],
							[
								-72.8686196003549,
								4.58051502877551
							],
							[
								-72.8685948669088,
								4.58063034442222
							],
							[
								-72.8685812347602,
								4.58069351450869
							],
							[
								-72.8685760749807,
								4.58076791174532
							],
							[
								-72.8685818230613,
								4.58089108520948
							],
							[
								-72.8686332868124,
								4.58089309620984
							],
							[
								-72.8686370908241,
								4.58083648202249
							],
							[
								-72.8686917849122,
								4.58082261101882
							],
							[
								-72.8687069068384,
								4.58073270529612
							],
							[
								-72.8686809525991,
								4.58065371150948
							],
							[
								-72.8686937769663,
								4.58053936797613
							],
							[
								-72.8687824882233,
								4.58050750082559
							],
							[
								-72.8688131549503,
								4.58049648462266
							],
							[
								-72.8688287167112,
								4.58047711822822
							],
							[
								-72.8688141735942,
								4.58045224310398
							],
							[
								-72.8688004551219,
								4.58043367210776
							],
							[
								-72.8688004058452,
								4.58038174267691
							],
							[
								-72.868861223654,
								4.58035712808918
							],
							[
								-72.8688786297312,
								4.58033458418561
							],
							[
								-72.8688913618019,
								4.5802957659744
							],
							[
								-72.8688902538952,
								4.58023689445173
							],
							[
								-72.8689489412908,
								4.58021031460116
							],
							[
								-72.8689766548502,
								4.58020823069819
							],
							[
								-72.8689761230998,
								4.58018414727601
							],
							[
								-72.8689214197963,
								4.58018853203474
							],
							[
								-72.8689214259756,
								4.58018668230838
							],
							[
								-72.8689213168756,
								4.58007165624929
							],
							[
								-72.8689514969975,
								4.58003673218385
							],
							[
								-72.8689924209469,
								4.58002559146137
							],
							[
								-72.8690571489858,
								4.57996027488791
							],
							[
								-72.8689614980415,
								4.57987887762038
							],
							[
								-72.8690127757645,
								4.5797611343492
							],
							[
								-72.8689687723653,
								4.57970034516538
							],
							[
								-72.8688525612751,
								4.57938151743263
							],
							[
								-72.8687809952211,
								4.57937950244093
							],
							[
								-72.8687273882434,
								4.57929575072571
							],
							[
								-72.8687270339736,
								4.57913774493539
							],
							[
								-72.868900427801,
								4.57912182066297
							],
							[
								-72.8689777343068,
								4.57911494523543
							],
							[
								-72.8690037116422,
								4.57911720501766
							],
							[
								-72.8690414563589,
								4.579124246028
							],
							[
								-72.869078625715,
								4.57910284490719
							],
							[
								-72.8691368837538,
								4.57909986811512
							],
							[
								-72.8691886740455,
								4.57911610424499
							],
							[
								-72.8692175941296,
								4.57908487027725
							],
							[
								-72.8692674866951,
								4.5790836519775
							],
							[
								-72.8692872278723,
								4.57910424894162
							],
							[
								-72.8693439518485,
								4.57910508369218
							],
							[
								-72.869365149296,
								4.57908592816417
							],
							[
								-72.8695165911133,
								4.5791365581974
							],
							[
								-72.8698362158218,
								4.57908420424582
							],
							[
								-72.8699036720486,
								4.57901783856534
							],
							[
								-72.8704543991521,
								4.57903887854505
							],
							[
								-72.8706107787671,
								4.57897702309862
							],
							[
								-72.8708055075387,
								4.57900355112271
							],
							[
								-72.8708888190559,
								4.57896552601666
							],
							[
								-72.8710186015932,
								4.57897885655264
							],
							[
								-72.871050377641,
								4.57901008648928
							],
							[
								-72.8710749763638,
								4.57900505555781
							],
							[
								-72.8710917638381,
								4.57898817410697
							],
							[
								-72.8711868041149,
								4.57894634828432
							],
							[
								-72.8712887293562,
								4.57894620434843
							],
							[
								-72.8713857241718,
								4.57892239293084
							],
							[
								-72.8714663316489,
								4.57891297558549
							],
							[
								-72.8718952293659,
								4.57888155554868
							],
							[
								-72.8719484074659,
								4.57887842851911
							],
							[
								-72.8719770791048,
								4.57889876372346
							],
							[
								-72.8719906839205,
								4.57891057134365
							],
							[
								-72.8720117974957,
								4.57891410732268
							],
							[
								-72.8720264666225,
								4.57890912776001
							],
							[
								-72.8720416754494,
								4.57890396499143
							],
							[
								-72.8720487579998,
								4.57887252761092
							],
							[
								-72.8724174493026,
								4.57885084747188
							],
							[
								-72.8725240411084,
								4.57884047396259
							],
							[
								-72.8725394270819,
								4.57909132726925
							],
							[
								-72.8725937624779,
								4.57971286618019
							],
							[
								-72.8726084938895,
								4.57989796264046
							],
							[
								-72.8724961500454,
								4.57990725588954
							],
							[
								-72.8720665287545,
								4.57994401778778
							],
							[
								-72.8716608849816,
								4.57997603717769
							],
							[
								-72.8712342308375,
								4.58000768095339
							],
							[
								-72.8708270988535,
								4.58003949150853
							],
							[
								-72.8703884647927,
								4.58007679945308
							],
							[
								-72.8699684023373,
								4.58010756789112
							],
							[
								-72.8695727365706,
								4.58013712023455
							],
							[
								-72.8689761231236,
								4.58018414727125
							],
							[
								-72.8689766549341,
								4.58020823069187
							],
							[
								-72.8695768354606,
								4.58016310050968
							],
							[
								-72.8699692772553,
								4.58013280658612
							],
							[
								-72.8703904314187,
								4.58010131158417
							],
							[
								-72.8708271018656,
								4.58006361294593
							],
							[
								-72.8712364865747,
								4.58003262400211
							],
							[
								-72.8716618083861,
								4.58000027109199
							],
							[
								-72.8720683422437,
								4.57996609390835
							],
							[
								-72.8724991706071,
								4.57993721578723
							],
							[
								-72.8726109270205,
								4.57994174975602
							],
							[
								-72.8726281885152,
								4.58014500118247
							],
							[
								-72.8726628313923,
								4.58056124615326
							],
							[
								-72.8726964936498,
								4.58101040483361
							],
							[
								-72.8726989885187,
								4.58104429340475
							],
							[
								-72.8726676792083,
								4.5810773414881
							],
							[
								-72.8725567532135,
								4.58108590200048
							],
							[
								-72.8721405026855,
								4.58111146430546
							],
							[
								-72.8717362952309,
								4.58113759983635
							],
							[
								-72.8713267128941,
								4.58117716741513
							],
							[
								-72.8707388493226,
								4.58121854757421
							],
							[
								-72.8706414843342,
								4.5812020773179
							],
							[
								-72.8704954573165,
								4.58121507054555
							],
							[
								-72.870490273219,
								4.5812412471701
							],
							[
								-72.8700454786412,
								4.58127265388437
							],
							[
								-72.8697467551169,
								4.58129332336731
							],
							[
								-72.8696528573714,
								4.58130315527159
							],
							[
								-72.8695387136164,
								4.58130604301955
							],
							[
								-72.8693121104071,
								4.58132805167096
							],
							[
								-72.8690004974025,
								4.5813563838698
							],
							[
								-72.8688289516047,
								4.58135698596719
							],
							[
								-72.8686566886607,
								4.58137269584477
							],
							[
								-72.8686027360593,
								4.58131360868153
							],
							[
								-72.8686033742212,
								4.58123245568885
							],
							[
								-72.8686041620737,
								4.58119661621943
							],
							[
								-72.8685899989681,
								4.58114235481271
							],
							[
								-72.8686497346033,
								4.58108873850427
							],
							[
								-72.868650458904,
								4.58098648355403
							],
							[
								-72.8686319993185,
								4.58091246793015
							],
							[
								-72.8686332868124,
								4.5808930962146
							],
							[
								-72.868581823067,
								4.58089108533019
							],
							[
								-72.8685857339466,
								4.5809748901018
							],
							[
								-72.8685244742819,
								4.58102539024387
							],
							[
								-72.8685076570672,
								4.58104252280504
							],
							[
								-72.8684688974439,
								4.58106032359137
							],
							[
								-72.8684608160462,
								4.58108294345651
							],
							[
								-72.8684624983272,
								4.58110753925076
							],
							[
								-72.8685385453103,
								4.58115902902721
							],
							[
								-72.8685419679912,
								4.58138534804932
							],
							[
								-72.8669635045912,
								4.58150461269966
							],
							[
								-72.8663691628648,
								4.58154208505431
							],
							[
								-72.8656316737272,
								4.58159986171716
							],
							[
								-72.8649687202628,
								4.58164738110884
							],
							[
								-72.864696569206,
								4.58166301798071
							],
							[
								-72.8643423706073,
								4.58169368920008
							],
							[
								-72.8640591946965,
								4.58171332430956
							],
							[
								-72.8639695897059,
								4.58173430361065
							],
							[
								-72.8639901131315,
								4.58164709806495
							],
							[
								-72.8640486886195,
								4.58159806546276
							],
							[
								-72.8641414468885,
								4.58162201520254
							],
							[
								-72.8642182650371,
								4.58157865101164
							],
							[
								-72.8643113604949,
								4.58151513659998
							],
							[
								-72.8646487027,
								4.58126920867953
							],
							[
								-72.8647064196324,
								4.58123663217027
							],
							[
								-72.8647745846058,
								4.58125073353704
							],
							[
								-72.86509119704,
								4.58104575482855
							],
							[
								-72.8651836869109,
								4.58105230328838
							],
							[
								-72.8652723807657,
								4.58100181278779
							],
							[
								-72.8660790514147,
								4.58047562245071
							],
							[
								-72.8661966614163,
								4.58047310401659
							],
							[
								-72.8662320406757,
								4.58050691570864
							],
							[
								-72.8662699869999,
								4.58053107632223
							],
							[
								-72.866315843586,
								4.58055157921831
							],
							[
								-72.8663500467233,
								4.58055807515922
							],
							[
								-72.866376548268,
								4.58054787467413
							],
							[
								-72.8663962946837,
								4.58053214502426
							],
							[
								-72.8662749240291,
								4.58042230801127
							],
							[
								-72.8664170681861,
								4.58034139435418
							],
							[
								-72.8664608082468,
								4.5803534219045
							],
							[
								-72.8664916588156,
								4.58036783456228
							],
							[
								-72.8665090439589,
								4.58040260484887
							],
							[
								-72.8665234808564,
								4.58042573371693
							],
							[
								-72.8665489690357,
								4.58041280859554
							],
							[
								-72.8665692741251,
								4.5803819861229
							],
							[
								-72.8665635174078,
								4.58034001833301
							],
							[
								-72.866565835427,
								4.58031220210373
							],
							[
								-72.8667419861166,
								4.58018149060189
							],
							[
								-72.8670807976896,
								4.57995498102668
							],
							[
								-72.8671036158596,
								4.57998999744798
							],
							[
								-72.8671267960506,
								4.57999811051485
							],
							[
								-72.8671441811939,
								4.57998999744798
							],
							[
								-72.867157143387,
								4.57997420023288
							],
							[
								-72.8671752269887,
								4.57996516745994
							],
							[
								-72.8672009761964,
								4.5799581042304
							],
							[
								-72.8672229938435,
								4.5799598631996
							],
							[
								-72.8672670362065,
								4.57998999744798
							],
							[
								-72.8672890573881,
								4.57998767942888
							],
							[
								-72.8673052835218,
								4.57996681725692
							],
							[
								-72.8672971704549,
								4.57987641451178
							],
							[
								-72.8673633553663,
								4.57984087239099
							],
							[
								-72.8674169008659,
								4.57981209683823
							],
							[
								-72.8674895660407,
								4.57983816719653
							],
							[
								-72.8676419655012,
								4.57975936956282
							],
							[
								-72.8676513338521,
								4.57973170659507
							],
							[
								-72.8676763828529,
								4.57972096497728
							],
							[
								-72.8676935517221,
								4.57972458426033
							],
							[
								-72.867735276066,
								4.57975935454693
							],
							[
								-72.8677589243201,
								4.57974343736693
							],
							[
								-72.8678917872965,
								4.57966322052716
							],
							[
								-72.8682301034312,
								4.57943226508443
							],
							[
								-72.8682634835994,
								4.57945244213173
							],
							[
								-72.8683017930461,
								4.57946248424389
							],
							[
								-72.8684306219,
								4.57936891376749
							],
							[
								-72.8684648197906,
								4.57937958219248
							],
							[
								-72.8685002429995,
								4.57939984407057
							],
							[
								-72.8685737289349,
								4.57936685102232
							],
							[
								-72.8686128804576,
								4.57938886468147
							],
							[
								-72.8686245863229,
								4.57940286602239
							],
							[
								-72.8686416215365,
								4.57942324177654
							],
							[
								-72.8686660164245,
								4.57942456041914
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8b,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("2020-09-01T00:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 1,
	"Tractor" : "jd12 kt19 FIAT 100-90",
	"Codigo Vagon  Asociado a Despacho" : "SUJ435",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				"4.590770",
				"-72.837037"
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 97,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 97,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"name" : "N440 - OSCAR ORLANDO RUGE CESPEDES",
			"reference" : "Cortador",
			"_id" : "5d9682ad30493a16fbf25f01",
			"value" : 27
		},
		{
			"name" : "N483 - NIXON FABIAN AREVALO DAZA",
			"reference" : "Cortador",
			"_id" : "5e9def5a50ce4f0e63d8ca81",
			"value" : 24
		},
		{
			"name" : "N469 - HILTON HAIR LOPEZ CARRION",
			"reference" : "Cortador",
			"_id" : "5e9f06bcbfbd7019aaabb911",
			"value" : 27
		},
		{
			"name" : "N439 - JEAN JAIR MONTAÑA CASANOVA",
			"reference" : "Cortador",
			"_id" : "5d9682ae30493a16fbf25f95",
			"value" : 19
		},
		{
			"name" : "P317 - DUMAR MORENO",
			"reference" : "Encallador",
			"_id" : "5d9682ae30493a16fbf26022",
			"value" : 27
		},
		{
			"name" : "N448 - FERNEY ARIAS",
			"reference" : "Encallador",
			"_id" : "5e76285405a0de36ae9fd568",
			"value" : 51
		},
		{
			"name" : "N383 - RUBISTEN CASTILLO MINA",
			"reference" : "Encallador",
			"_id" : "5d9682ae30493a16fbf25f97",
			"value" : 19
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-09-01T17:01:47.000-05:00"),
	"uDate" : ISODate("2020-09-02T10:27:23.504-05:00"),
	"capture" : "W",
	"supervisor_u" : "ESPINOZA MENDEZ BLANCA NIDIA"
},

/* 11 createdAt:1/9/2020, 5:45:53 p. m.*/
{
	"_id" : ObjectId("5f4ecf21ea79e455cc9dc9eb"),
	"Lote" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5db0c997851b0a53e3c18b1a",
				"type" : "Feature",
				"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8b,",
				"properties" : {
					"type" : "lot",
					"name" : "A-23",
					"production" : true,
					"status" : true,
					"order" : 23,
					"custom" : {
						"Año de siembra" : {
							"type" : "number",
							"value" : 0
						},
						"Centro de Costo" : {
							"type" : "string",
							"value" : "14A23951"
						},
						"Mes de siembra" : {
							"type" : "number",
							"value" : 1
						},
						"Nombre" : {
							"type" : "string",
							"value" : "A23"
						},
						"Numero de hectareas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas" : {
							"type" : "number",
							"value" : 0
						},
						"Numero de plantas erradicadas" : {
							"type" : "number",
							"value" : 0
						},
						"color" : {
							"type" : "color",
							"value" : "#F6CECE"
						}
					}
				},
				"geometry" : {
					"type" : "Polygon",
					"coordinates" : [
						[
							[
								-72.8717397188449,
								4.58116480649549
							],
							[
								-72.8722953363786,
								4.58112955591987
							],
							[
								-72.8727044157688,
								4.58111712599309
							],
							[
								-72.8728595967874,
								4.58323186578614
							],
							[
								-72.8727815961178,
								4.58324024724766
							],
							[
								-72.8727091429647,
								4.5832247365554
							],
							[
								-72.8726543006733,
								4.58320539292652
							],
							[
								-72.8726102254692,
								4.58319773579279
							],
							[
								-72.8725381516316,
								4.58321824917059
							],
							[
								-72.8724924307763,
								4.58326593794967
							],
							[
								-72.8724482682708,
								4.58329692481462
							],
							[
								-72.8723750760535,
								4.58337864944276
							],
							[
								-72.8713046267257,
								4.58344900441354
							],
							[
								-72.8702210723322,
								4.58352996505158
							],
							[
								-72.8697366764472,
								4.58357822900715
							],
							[
								-72.8695769422457,
								4.58334185307539
							],
							[
								-72.8695713259126,
								4.58322105388575
							],
							[
								-72.8695179878248,
								4.58318628022992
							],
							[
								-72.8694482921853,
								4.58311410769822
							],
							[
								-72.8693760958193,
								4.58292437200039
							],
							[
								-72.869240813152,
								4.58271218669829
							],
							[
								-72.8689039136565,
								4.58229919325919
							],
							[
								-72.8688623574197,
								4.58201944438199
							],
							[
								-72.8688207413017,
								4.58202549062521
							],
							[
								-72.8688636261137,
								4.58230066504762
							],
							[
								-72.8692183515804,
								4.58272103413002
							],
							[
								-72.8693570524762,
								4.58293496307709
							],
							[
								-72.869428991889,
								4.58312159933773
							],
							[
								-72.8694698364757,
								4.58321843078373
							],
							[
								-72.869493235781,
								4.58324724373863
							],
							[
								-72.8695149352345,
								4.58330766229357
							],
							[
								-72.8695143557298,
								4.5833464891136
							],
							[
								-72.8696633923839,
								4.58356297299993
							],
							[
								-72.8696639018436,
								4.58356371833968
							],
							[
								-72.8685794259235,
								4.5836628659404
							],
							[
								-72.8656042214397,
								4.58385199064041
							],
							[
								-72.8649930364609,
								4.58391775936091
							],
							[
								-72.8640815116302,
								4.58396788298381
							],
							[
								-72.8639860205413,
								4.58265692477664
							],
							[
								-72.8639690745232,
								4.58230859605885
							],
							[
								-72.8639448958344,
								4.58211100694031
							],
							[
								-72.8639528483895,
								4.58209302028198
							],
							[
								-72.8640076581601,
								4.58207302118916
							],
							[
								-72.864004882955,
								4.58200707601797
							],
							[
								-72.8639413498626,
								4.58199768849055
							],
							[
								-72.8639392429431,
								4.58193035776481
							],
							[
								-72.8639110419227,
								4.58179045656925
							],
							[
								-72.8639328991264,
								4.58178417088347
							],
							[
								-72.8640317380531,
								4.58175574682492
							],
							[
								-72.8646452484679,
								4.5816917700884
							],
							[
								-72.8650159713125,
								4.58167001457405
							],
							[
								-72.8654352946078,
								4.58164005815044
							],
							[
								-72.8658644071735,
								4.58160810006395
							],
							[
								-72.8662875963677,
								4.58157109240165
							],
							[
								-72.8667197614067,
								4.58154052624274
							],
							[
								-72.8671220435557,
								4.58151364892673
							],
							[
								-72.8675460023777,
								4.58148500194409
							],
							[
								-72.8678560738035,
								4.58146227821266
							],
							[
								-72.8679157817925,
								4.58147767060187
							],
							[
								-72.8679807728176,
								4.58150271097456
							],
							[
								-72.8679938802844,
								4.58153516159696
							],
							[
								-72.8679995329245,
								4.58156861483674
							],
							[
								-72.8680315607511,
								4.58156097100679
							],
							[
								-72.8680578112112,
								4.58151309044211
							],
							[
								-72.8681802396906,
								4.58144512136474
							],
							[
								-72.8683857164354,
								4.58142196085657
							],
							[
								-72.8685689878719,
								4.58141422512972
							],
							[
								-72.8686449677829,
								4.5817113088025
							],
							[
								-72.8686830198556,
								4.5817442565181
							],
							[
								-72.8687533379935,
								4.58194012128666
							],
							[
								-72.8688205059296,
								4.58202398033813
							],
							[
								-72.8688207412966,
								4.58202549059257
							],
							[
								-72.8688623574154,
								4.58201944436232
							],
							[
								-72.8688013663634,
								4.58190913838328
							],
							[
								-72.8686734090174,
								4.58159090124971
							],
							[
								-72.8686880039854,
								4.58156727727268
							],
							[
								-72.8687022160256,
								4.58153552053366
							],
							[
								-72.8686682974332,
								4.58140237623302
							],
							[
								-72.8688378692729,
								4.58138461073889
							],
							[
								-72.8692038777793,
								4.58136180027538
							],
							[
								-72.8693944810673,
								4.58134778095093
							],
							[
								-72.8695520235403,
								4.58133444852952
							],
							[
								-72.8697297130318,
								4.58132339009761
							],
							[
								-72.8700564029848,
								4.58130218542609
							],
							[
								-72.8705205363607,
								4.58126559193846
							],
							[
								-72.8709225991959,
								4.58123275227603
							],
							[
								-72.8713283313378,
								4.58120639164064
							],
							[
								-72.8717397188449,
								4.58116480649549
							]
						]
					]
				}
			}
		],
		"path" : ",5d2648a845a0dd2e9e204fe2,5db0c996851b0a53e3c18a8b,"
	},
	"Formula" : "",
	"Fecha de puesta en caja" : ISODate("1969-12-31T19:00:00.000-05:00"),
	"Zona" : 1,
	"Hora de LLenado" : 1,
	"Tractor" : "kt19 jd16 jd12",
	"Codigo Vagon  Asociado a Despacho" : "ESC60",
	"Numero de Viaje de Vagon" : "01",
	"Point" : {
		"farm" : "5d2648a845a0dd2e9e204fe2",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-72.929773,
				4.6057072
			]
		}
	},
	"Empleado adicional 1" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 1 opcion" : "Alzador",
	"Empleado adicional 1 cantidad" : 422,
	"Empleado adicional 2" : "F999 - AGROINDUSTRIA FELEDA SA ",
	"Empleado adicional 2 opcion" : "Transportador",
	"Empleado adicional 2 cantidad" : 422,
	"Empleado adicional 3 cantidad" : 0,
	"Empleados de Cosecha" : [
		{
			"_id" : "5d9682ad30493a16fbf25f01",
			"name" : "N440 - OSCAR ORLANDORUGE CESPEDES",
			"reference" : "Cortador",
			"value" : 44
		},
		{
			"_id" : "5ed6f46bd1e4c414dc80f9b3",
			"name" : "P586 - GUILLERMO JOSEVARELA OJITO",
			"reference" : "Cortador",
			"value" : 48
		},
		{
			"_id" : "5d9682ae30493a16fbf25f95",
			"name" : "N439 - JEAN JAIRMONTAÑA CASANOVA",
			"reference" : "Cortador",
			"value" : 102
		},
		{
			"_id" : "5e9def5a50ce4f0e63d8ca81",
			"name" : "N483 - NIXON FABIANAREVALO DAZA",
			"reference" : "Cortador",
			"value" : 58
		},
		{
			"_id" : "5d9682ae30493a16fbf26020",
			"name" : "P571 - JOSE DAVIDMEDINA MONTEALEGRE",
			"reference" : "Cortador",
			"value" : 54
		},
		{
			"_id" : "5e9f06bcbfbd7019aaabb911",
			"name" : "N469 - HILTON HAIRLOPEZ CARRION",
			"reference" : "Cortador",
			"value" : 67
		},
		{
			"_id" : "5e95cf144bcff94d945187c3",
			"name" : "N460 - JOSÉ VICENTEMENDOZA MURCIA",
			"reference" : "Cortador",
			"value" : 49
		},
		{
			"_id" : "5d9682ae30493a16fbf26022",
			"name" : "P317 - DUMARMORENO",
			"reference" : "Encallador",
			"value" : 44
		},
		{
			"_id" : "5e95d35e4bcff94d945189ac",
			"name" : "N463 - SANDRA PATRICIAFERNANDEZ FRANCO",
			"reference" : "Encallador",
			"value" : 48
		},
		{
			"_id" : "5d9682ae30493a16fbf25f97",
			"name" : "N383 - RUBISTENCASTILLO MINA",
			"reference" : "Encallador",
			"value" : 41
		},
		{
			"_id" : "5e76285405a0de36ae9fd568",
			"name" : "N448 - FERNEYARIAS",
			"reference" : "Encallador",
			"value" : 125
		},
		{
			"_id" : "5ed6ee3bd1e4c414dc80f918",
			"name" : "P580 - JEISSON ANDRESVEGA MOLANO",
			"reference" : "Encallador",
			"value" : 54
		},
		{
			"_id" : "5e9f0772bfbd7019aaabb983",
			"name" : "N471 - JHON FREDYGARCIA BERRIO",
			"reference" : "Encallador",
			"value" : 61
		},
		{
			"_id" : "5d9682ae30493a16fbf25fb7",
			"name" : "P572 - JAIDER JOBANYVEGA MOLANO",
			"reference" : "Encallador",
			"value" : 49
		}
	],
	"Empleado adicional 3" : "",
	"empleado Adicional opcion3" : "",
	"uid" : ObjectId("5d263f5545a0dd2e9e204fc3"),
	"supervisor" : null,
	"rgDate" : ISODate("2020-08-31T17:45:18.000-05:00"),
	"uDate" : ISODate("2020-08-31T17:45:18.000-05:00"),
	"capture" : "M"
}