// db.form_recolecciondecosechaxempleados.find({})
//   .projection({})
//   .sort({_id:-1})




var cursor = db.form_recolecciondecosechaxempleados.aggregate(

    // { $match: { "farm": ObjectId("5d2648a845a0dd2e9e204fe2") } }

    //----con cartografia duplicadas
    {
        $addFields: {
            "num_lotes": { $size: "$Lote.features" }
        }
    }
    , { $match: { "num_lotes": { $gt: 1 } } }
    //, { $match: { "num_lotes": { $eq: 2 } } }

);

//cursor


// // //----hacer varias veces pop por que saca solo 1 del array
cursor.forEach(i => {
    // console.log(i)
    
        db.form_recolecciondecosechaxempleados.update(
        	{
                "_id":i._id
        	},
        	{
        		$pop:{
                    "Lote.features":1
        		}
        	}
        )
});