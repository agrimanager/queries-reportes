    [


        { "$project": { "_id": "$_id" } }

        , {
            "$lookup": {
                "from": "form_recolecciondecosechaxempleados",
                "as": "data_liquidacion_cosecha",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$$id_usr", "$uid"]
                            }
                        }
                    },

                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "lote": "$lote.properties.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },

                    { "$unwind": "$Finca" },

                    {
                        "$addFields": {
                            "_id_str_farm": { "$toString": "$Finca._id" }
                        }
                    }

                    , {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    }

                    , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }

                    , {
                        "$addFields": {
                            "cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                                    "then": "PALMA",
                                    "else": "CACAO"
                                }
                            }
                        }
                    },




                    {
                        "$lookup": {
                            "from": "form_pesospromedios",
                            "as": "Peso promedio lote",
                            "let": {
                                "nombre_lote": "$lote",
                                "anio_registro": { "$year": "$Fecha de puesta en caja" },
                                "mes_registro": { "$month": "$Fecha de puesta en caja" }
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] },
                                                { "$eq": ["$$anio_registro", "$Anio"] },
                                                { "$eq": ["$$mes_registro", "$Mes"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$Peso promedio lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$addFields": {
                            "Peso promedio lote": {
                                "$ifNull": ["$Peso promedio lote.Peso Promedio", 0]
                            }
                        }
                    },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "empleados_cosecha_adicionales": {
                                    "$filter": {
                                        "input": [
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 1", ""] },
                                                "reference": "$Empleado adicional 1 opcion",
                                                "value": "$Empleado adicional 1 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 1", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 2", ""] },
                                                "reference": "$Empleado adicional 2 opcion",
                                                "value": "$Empleado adicional 2 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 2", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 3", ""] },
                                                "reference": "$empleado Adicional opcion3",
                                                "value": "$Empleado adicional 3 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 3", " -"] }, 0] }
                                            }


                                        ],
                                        "as": "empleados_cosecha_adicionales",
                                        "cond": {
                                            "$ne": ["$$empleados_cosecha_adicionales.name", ""]
                                        }
                                    }
                                }


                            }

                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleados_cosecha_adicionales.code",
                            "foreignField": "code",
                            "as": "info_empleado_adicional"
                        }
                    }

                    , {
                        "$addFields": {
                            "Empleados de Cosecha": {
                                "$concatArrays": [
                                    "$Empleados de Cosecha",
                                    {
                                        "$map": {
                                            "input": "$empleados_cosecha_adicionales",
                                            "as": "empleado_cosecha_adicional",
                                            "in": {
                                                "$mergeObjects": [
                                                    "$$empleado_cosecha_adicional",
                                                    {
                                                        "_id": {
                                                            "$reduce": {
                                                                "input": "$info_empleado_adicional",
                                                                "initialValue": null,
                                                                "in": {
                                                                    "$cond": {
                                                                        "if": {
                                                                            "$eq": [
                                                                                "$$empleado_cosecha_adicional.code",
                                                                                "$$this.code"
                                                                            ]
                                                                        },
                                                                        "then": {
                                                                            "$toString": "$$this._id"
                                                                        },
                                                                        "else": "$$value"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Total Cortados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Cortador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Encallados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Encallador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Cortados y Encallados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "CortadorEncallador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Alzados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Alzador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "Total Cortados": {
                                "$reduce": {
                                    "input": "$Total Cortados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Encallados": {
                                "$reduce": {
                                    "input": "$Total Encallados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Cortados y Encallados": {
                                "$reduce": {
                                    "input": "$Total Cortados y Encallados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Alzados": {
                                "$reduce": {
                                    "input": "$Total Alzados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields":
                            {
                                "Peso aproximado Alzados": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$Total Alzados" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                                    ]
                                }
                            }
                    }

                    , {
                        "$lookup": {
                            "from": "form_despachodecosecha",
                            "as": "form_despacho_cosecha",
                            "let": {
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "fecha": "$Fecha de puesta en caja"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Codigo Vagon Asociado a Despacho", "$$vagon"] },
                                                { "$eq": ["$Numero de Viaje de Vagon", "$$num_viaje_vagon"] },
                                                {
                                                    "$gte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de Llenado" } } }
                                                    ]
                                                },

                                                {
                                                    "$lte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de llenado" } } }
                                                    ]
                                                }


                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": 1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$form_despacho_cosecha",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    {
                        "$addFields":
                            {
                                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso total segun ticket" }, -1] },
                                "Numero de ticket": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Numero de Ticket" }, -1] }
                            }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "ticket": "$Numero de ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_lote_viaje": {
                                "$sum": "$Peso aproximado Alzados"
                            },
                            "total_alzados_lote": {
                                "$sum": "$Total Alzados"
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "vagon": "$_id.vagon",
                                "num_viaje_vagon": "$_id.num_viaje_vagon",
                                "ticket": "$_id.ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_viaje": {
                                "$sum": "$total_peso_racimos_alzados_lote_viaje"
                            }

                        }
                    },
                    {
                        "$unwind": "$data"
                    },

                    {
                        "$addFields":
                            {
                                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                                "total_alzados_lote": "$data.total_alzados_lote"
                            }
                    },

                    {
                        "$unwind": "$data.data"
                    },
                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                                        "total_alzados_lote": "$total_alzados_lote"
                                        , "ticket": "$_id.ticket"
                                        , "(%) Alzados x lote": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                                },
                                                "then": 0,
                                                "else": {
                                                    "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "Peso REAL Alzados": {
                                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

                                }
                            }
                    }

                    , {
                        "$addFields": {
                            "Peso REAL lote": {
                                "$cond": {
                                    "if": { "$eq": ["$Total Alzados", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                                    }
                                }
                            }
                        }
                    }
                    , {
                        "$project": {
                            "form_despacho_cosecha": 0,
                            "empleados_cosecha_adicionales": 0,
                            "info_empleado_adicional": 0,
                            "anio_filtro": 0
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "form_cargueinformaciondeplantas",
                            "as": "referencia_siembras",
                            "let": {
                                "nombre_lote": "$lote",
                                "cultivo": "$cultivo"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$CULTIVO", "$$cultivo"] },
                                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }

                            ]
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$referencia_siembras",
                            "preserveNullAndEmptyArrays": false
                        }
                    },



                    {
                        "$addFields": {
                            "Hectareas": "$referencia_siembras.HECTAREAS",
                            "Siembra lote": { "$toString": "$referencia_siembras.SIEMBRA" },
                            "num_palmas": "$referencia_siembras.PALMAS",
                            "Material": "$referencia_siembras.MATERIAL"
                        }
                    },

                    {
                        "$project": {
                            "referencia_siembras": 0
                        }
                    }


                    , { "$unwind": "$Empleados de Cosecha" }

                    , {
                        "$addFields": {
                            "oid_empleado": { "$toObjectId": "$Empleados de Cosecha._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "oid_empleado",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , { "$unwind": "$info_empleado" }

                    , {
                        "$lookup": {
                            "from": "companies",
                            "localField": "info_empleado.cid",
                            "foreignField": "_id",
                            "as": "info_empresa"
                        }
                    }
                    , { "$unwind": "$info_empresa" }

                    , {
                        "$addFields": {
                            "Empresa empleado": "$info_empresa.name"
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "form_costodeactividadesxempresa",
                            "as": "costos_actividades_x_empresa",
                            "let": {
                                "fecha_siembra": "$Siembra lote",
                                "empresa": "$Empresa empleado"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$fecha_siembra", "$Fecha de Siembra"] },
                                                { "$eq": ["$$empresa", "$Empresa"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$costos_actividades_x_empresa",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , {
                        "$addFields": {
                            "peso": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$Empleados de Cosecha.value" }, 0] }, "$Peso REAL lote"] },
                            "cargo": "$Empleados de Cosecha.reference"
                        }
                    }


                    , {
                        "$addFields": {
                            "precio_actividad": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE FRUTO" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            },

                            "precio_herramienta": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE HERRAMIENTA" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "costo_actividad": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_actividad" }, 0] }, "$peso"] },
                            "costo_herramienta": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_herramienta" }, 0] }, "$peso"] }
                        }
                    }


                    , {
                        "$addFields": {
                            "costo": { "$sum": ["$costo_actividad", "$costo_herramienta"] }
                        }
                    }


                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    , {
                        "$project": {


                            "Finca": "$Finca",
                            "Lote": "$lote",
                            "Siembra": "$Siembra lote",
                            "cultivo": "PALMA",
                            "Centro de costos": "Corte y recoleccion",
                            "Nombre Labor": "$cargo",
                            "Codigo Labor": "COSECHA",
                            "Estado Labor": {
                                "$cond": {
                                    "if": { "$eq": ["$ticket", -1] },


                                    "then": "💪 En progreso",
                                    "else": "✔ Listo"



                                }
                            },

                            "Fecha inicio": "$Fecha de puesta en caja",
                            "Fecha fin": "$Fecha de puesta en caja",

                            "Codigo empleado": "$info_empleado.code",
                            "Nombre empleado": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                            "Cedula": { "$ifNull": ["$info_empleado.numberID", "--"] },
                            "Empresa": "$Empresa empleado",
                            "Unidad": "Kilogramos",
                            "Cantidad": "$peso",
                            "vr unit trabajador": "$precio_actividad",
                            "vr unit herramienta": "$precio_herramienta",
                            "vr Total trabajador": "$costo_actividad",
                            "vr Total herramienta": "$costo_herramienta",
                            "TOTAL PAGO": "$costo",
                            "rgDate": "$Fecha de puesta en caja",
                            "centro costos presupuesto": "Corte y recoleccion",

                            "Anio": { "$year": "$Fecha de puesta en caja" },
                            "Mes": { "$month": "$Fecha de puesta en caja" },
                            "Semana": { "$week": "$Fecha de puesta en caja" }
                        }
                    }
                ]

            }
        }


        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data_liquidacion_cosecha",
                            []
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        , {
            "$addFields": {
                "unix_inicio": {
                    "$toDouble": "$Fecha inicio"
                },
                "unix_fin": {
                    "$toDouble": "$Fecha fin"
                }
            }
        },
        {
            "$addFields": {
                "dia_inicio": {
                    "$ceil": {
                        "$divide": [
                            "$unix_inicio",
                            86400000
                        ]
                    }
                },
                "dia_fin": {
                    "$ceil": {
                        "$divide": [
                            "$unix_fin",
                            86400000
                        ]
                    }
                },
                "semana_inicio": {
                    "$ceil": {
                        "$divide": [
                            { "$sum": ["$unix_inicio", 259200000] },
                            604800000
                        ]
                    }
                },
                "semana_fin": {
                    "$ceil": {
                        "$divide": [
                            { "$sum": ["$unix_fin", 259200000] },
                            604800000
                        ]
                    }
                }
            }
        },
        {
            "$addFields": {
                "cantidad_dias": {
                    "$sum": [
                        "$dia_fin",
                        { "$multiply": ["$dia_inicio", -1] },
                        1
                    ]
                },
                "cantidad_semanas": {
                    "$sum": [
                        "$semana_fin",
                        { "$multiply": ["$semana_inicio", -1] },
                        1
                    ]
                }
            }
        },
        {
            "$addFields": {
                "semanas": {
                    "$range": [
                        "$semana_inicio",
                        { "$sum": ["$semana_fin", 1] },
                        1
                    ]
                },
                "dias": {
                    "$range": [
                        "$dia_inicio",
                        { "$sum": ["$dia_fin", 1] },
                        1
                    ]
                }
            }
        },
        {
            "$unwind": "$dias"
        }


        , {
            "$addFields": {
                "pago_x_dia": {
                    "$cond": {
                        "if": {
                            "$eq": ["$cantidad_dias", 1]
                        },
                        "then": "$vr Total trabajador",
                        "else": {
                            "$divide": ["$vr Total trabajador", "$cantidad_dias"]
                        }
                    }
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "dia": "$dias",
                    "Nombre empleado": "$Nombre empleado",
                    "Codigo empleado": "$Codigo empleado",
                    "Cedula": "$Cedula"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "fecha": { "$toDate": { "$multiply": ["$_id.dia", 86400000] } }
            }
        }

        , {
            "$addFields": {
                "fechas": {
                    "Fecha": "$fecha",
                    "Anio": { "$year": { "date": "$fecha", "timezone": "-0500" } },
                    "Mes": { "$month": { "date": "$fecha", "timezone": "-0500" } },
                    "Semana": { "$week": { "date": "$fecha", "timezone": "-0500" } },
                    "Dia": { "$dayOfYear": { "date": "$fecha", "timezone": "-0500" } },
                    "Dia_semana": { "$dayOfWeek": { "date": "$fecha", "timezone": "-0500" } }
                }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        "$fechas"
                    ]
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Empresa": "$Empresa",
                    
                    "Nombre empleado": "$Nombre empleado",
                    "Codigo empleado": "$Codigo empleado",
                    "Cedula": "$Cedula",
                    "anio": "$Anio",
                    "semana": "$Semana",
                    "dia": "$Dia",
                    "dia_semana": "$Dia_semana"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "total_pago_x_dia": {
                    "$sum": "$pago_x_dia"
                }
                , "fecha_min": {
                    "$min": "$Fecha"
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "Empresa": "$_id.Empresa",
                    
                    "Nombre empleado": "$_id.Nombre empleado",
                    "Codigo empleado": "$_id.Codigo empleado",
                    "Cedula": "$_id.Cedula",
                    "anio": "$_id.anio",
                    "semana": "$_id.semana"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_pago_x_semana": {
                    "$sum": "$total_pago_x_dia"
                }
                , "dias_semana_trabajados": {
                    "$push": "$_id.dia_semana"
                }
                , "fecha_min": {
                    "$min": "$fecha_min"
                }
            }
        },

        {
            "$addFields": {
                "tiene_dominal": {
                    "$eq": [
                        {
                            "$size": {
                                "$setDifference": [[2, 3, 4, 5, 6, 7], "$dias_semana_trabajados"]
                            }
                        },
                        0
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "pago_dominical": {
                    "$cond": {
                        "if": {
                            "$and": [
                                "$tiene_dominal",
                                { "$ne": ["$_id.Empresa", "Agroindustria Feleda"] }
                            ]
                        },
                        "then": { "$divide": ["$total_pago_x_semana", 6] },
                        "else": 0
                    }
                }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_pago_x_semana": "$total_pago_x_semana",
                            "pago_dominical": "$pago_dominical",
                            "fecha_min": "$fecha_min"
                        }
                    ]
                }
            }
        }


        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_pago_x_dia": "$total_pago_x_dia",
                            "total_pago_x_semana": "$total_pago_x_semana",
                            "pago_dominical": "$pago_dominical",
                            "rgDate": "$fecha_min"
                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$Mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$Mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$Mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$Mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$Mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$Mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$Mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$Mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$Mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$Mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$Mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$Mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } },

                "dia_semana_txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$Dia_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$Dia_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$Dia_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$Dia_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$Dia_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$Dia_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$Dia_semana", 1] }, "then": "07-Domingo" }
                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        }

        , {
            "$project": {
                "semanas": 0,
                "unix_inicio": 0,
                "unix_fin": 0,
                "dia_inicio": 0,
                "dia_fin": 0,
                "semana_inicio": 0,
                "semana_fin": 0,
                "dias": 0
            }
        }

        , {
            "$addFields": {
                "pago_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pago_x_dia", 100] }, { "$mod": [{ "$multiply": ["$pago_x_dia", 100] }, 1] }] }, 100] },
                "total_pago_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_pago_x_dia", 100] }, { "$mod": [{ "$multiply": ["$total_pago_x_dia", 100] }, 1] }] }, 100] },
                "total_pago_x_semana": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_pago_x_semana", 100] }, { "$mod": [{ "$multiply": ["$total_pago_x_semana", 100] }, 1] }] }, 100] },
                "pago_dominical": { "$divide": [{ "$subtract": [{ "$multiply": ["$pago_dominical", 100] }, { "$mod": [{ "$multiply": ["$pago_dominical", 100] }, 1] }] }, 100] }
            }
        }


        , {
            "$group": {
                "_id": {
                    "Empresa": "$Empresa",

                    "Nombre empleado": "$Nombre empleado",
                    "Codigo empleado": "$Codigo empleado",
                    "Cedula": "$Cedula",

                    "anio": "$Anio",
                    "semana": "$Semana",
                    "pago_dominical": "$pago_dominical"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        },


        {
            "$addFields": {
                "data": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$_id.pago_dominical", 0] },
                                { "$eq": ["$_id.Empresa", "Agroindustria Feleda"] }
                            ]
                        },
                        "then": "$data",
                        "else": {
                            "$concatArrays": [
                                "$data",
                                [
                                    {
                                        "Finca": { "$arrayElemAt": ["$data.Finca", 0] },
                                        "Lote": "",
                                        "Siembra": "",
                                        "cultivo": { "$arrayElemAt": ["$data.cultivo", 0] },
                                        "Centro de costos": "Corte y recoleccion",
                                        "Nombre Labor": "DOMINICAL",
                                        "Codigo Labor": "DOMINICAL",
                                        "Estado Labor": "DOMINICAL",
                                        "Fecha inicio": { "$arrayElemAt": ["$data.Fecha inicio", 0] },
                                        "Fecha fin": { "$arrayElemAt": ["$data.Fecha fin", 0] },
                                        "Codigo empleado": { "$arrayElemAt": ["$data.Codigo empleado", 0] },
                                        "Nombre empleado": { "$arrayElemAt": ["$data.Nombre empleado", 0] },
                                        "Cedula": { "$arrayElemAt": ["$data.Cedula", 0] },
                                        "Empresa": { "$arrayElemAt": ["$data.Empresa", 0] },
                                        "Unidad": "DOMINICAL",
                                        "Cantidad": 1,
                                        "vr unit trabajador": 0,
                                        "vr unit herramienta": 0,
                                        "vr Total trabajador": 0,
                                        "vr Total herramienta": 0,
                                        "TOTAL PAGO": 0,
                                        "rgDate": { "$arrayElemAt": ["$data.rgDate", 0] },
                                        "centro costos presupuesto": "Corte y recoleccion",
                                        "Anio": { "$arrayElemAt": ["$data.Anio", 0] },
                                        "Mes": { "$arrayElemAt": ["$data.Mes", 0] },
                                        "Semana": { "$arrayElemAt": ["$data.Semana", 0] },
                                        "cantidad_dias": 1,
                                        "cantidad_semanas": 1,
                                        "pago_x_dia": { "$arrayElemAt": ["$data.pago_dominical", 0] },
                                        "Fecha": { "$arrayElemAt": ["$data.Fecha", 0] },
                                        "Dia": -1,
                                        "Dia_semana": 8,
                                        "total_pago_x_dia": { "$arrayElemAt": ["$data.pago_dominical", 0] },
                                        "total_pago_x_semana": {
                                            "$sum": [{ "$arrayElemAt": ["$data.pago_dominical", 0] }, { "$arrayElemAt": ["$data.total_pago_x_semana", 0] }]
                                        },
                                        "pago_dominical": { "$arrayElemAt": ["$data.pago_dominical", 0] },
                                        "Mes_Txt": { "$arrayElemAt": ["$data.Mes_Txt", 0] },
                                        "dia_semana_txt": "08-dominical"
                                    }
                                ]
                            ]
                        }
                    }
                }
            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }

        , {
            "$addFields": {
                "peso_alzados": {
                    "$cond": {
                        "if": { "$eq": ["$Nombre Labor", "Alzador"] },
                        "then": "$Cantidad",
                        "else": 0
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_cargaprestacionalporempresa",
                "localField": "Empresa",
                "foreignField": "Empresa",
                "as": "carga_prestacional_x_empresa"
            }
        }
        , {
            "$unwind": {
                "path": "$carga_prestacional_x_empresa",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields":
                {
                    "carga_prestacional_x_empresa": { "$ifNull": ["$carga_prestacional_x_empresa.Porcentaje prestacional", 0] }
                }
        }


    ]