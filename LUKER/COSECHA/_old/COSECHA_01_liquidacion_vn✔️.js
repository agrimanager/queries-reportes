[
    {
        "$match": {
            "Lote.path": { "$ne": "" }
        }
    },
    { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" },


    {
        "$addFields": {
            "_id_str_farm": { "$toString": "$Finca._id" }
        }
    }
    
    ,{
        "$addFields": {
            "Finca": "$Finca.name"
        }
    }

    , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }

    , {
        "$addFields": {
            "cultivo": {
                "$cond": {
                    "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                    "then": "PALMA",
                    "else": "CACAO"
                }
            }
        }
    },


    {
        "$lookup": {
            "from": "form_pesospromedios",
            "as": "Peso promedio lote",
            "let": {
                "nombre_lote": "$lote",
                "anio_registro": { "$year": "$Fecha de puesta en caja" },
                "mes_registro": { "$month": "$Fecha de puesta en caja" }
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] },
                                { "$eq": ["$$anio_registro", "$Anio"] },
                                { "$eq": ["$$mes_registro", "$Mes"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$Peso promedio lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Peso promedio lote": {
                "$ifNull": ["$Peso promedio lote.Peso Promedio", 0]
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    , {
        "$addFields":
            {
                "empleados_cosecha_adicionales": {
                    "$filter": {
                        "input": [
                            {
                                "_id": "",
                                "name": { "$ifNull": ["$Empleado adicional 1", ""] },
                                "reference": "$Empleado adicional 1 opcion",
                                "value": "$Empleado adicional 1 cantidad",
                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 1", " -"] }, 0] }
                            },
                            {
                                "_id": "",
                                "name": { "$ifNull": ["$Empleado adicional 2", ""] },
                                "reference": "$Empleado adicional 2 opcion",
                                "value": "$Empleado adicional 2 cantidad",
                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 2", " -"] }, 0] }
                            },
                            {
                                "_id": "",
                                "name": { "$ifNull": ["$Empleado adicional 3", ""] },
                                "reference": "$empleado Adicional opcion3",
                                "value": "$Empleado adicional 3 cantidad",
                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 3", " -"] }, 0] }
                            }


                        ],
                        "as": "empleados_cosecha_adicionales",
                        "cond": {
                            "$ne": ["$$empleados_cosecha_adicionales.name", ""]
                        }
                    }
                }


            }

    }

    , {
        "$lookup": {
            "from": "employees",
            "localField": "empleados_cosecha_adicionales.code",
            "foreignField": "code",
            "as": "info_empleado_adicional"
        }
    }

    , {
        "$addFields": {
            "Empleados de Cosecha": {
                "$concatArrays": [
                    "$Empleados de Cosecha",
                    {
                        "$map": {
                            "input": "$empleados_cosecha_adicionales",
                            "as": "empleado_cosecha_adicional",
                            "in": {
                                "$mergeObjects": [
                                    "$$empleado_cosecha_adicional",
                                    {
                                        "_id": {
                                            "$reduce": {
                                                "input": "$info_empleado_adicional",
                                                "initialValue": null,
                                                "in": {
                                                    "$cond": {
                                                        "if": {
                                                            "$eq": [
                                                                "$$empleado_cosecha_adicional.code",
                                                                "$$this.code"
                                                            ]
                                                        },
                                                        "then": {
                                                            "$toString": "$$this._id"
                                                        },
                                                        "else": "$$value"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "Total Cortados": {
                "$map": {
                    "input": "$Empleados de Cosecha",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": {
                                "$eq": ["$$item.reference", "Cortador"]
                            },
                            "then": "$$item.value",
                            "else": 0
                        }
                    }
                }
            },
            "Total Encallados": {
                "$map": {
                    "input": "$Empleados de Cosecha",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": {
                                "$eq": ["$$item.reference", "Encallador"]
                            },
                            "then": "$$item.value",
                            "else": 0
                        }
                    }
                }
            },
            "Total Cortados y Encallados": {
                "$map": {
                    "input": "$Empleados de Cosecha",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": {
                                "$eq": ["$$item.reference", "CortadorEncallador"]
                            },
                            "then": "$$item.value",
                            "else": 0
                        }
                    }
                }
            },
            "Total Alzados": {
                "$map": {
                    "input": "$Empleados de Cosecha",
                    "as": "item",
                    "in": {
                        "$cond": {
                            "if": {
                                "$eq": ["$$item.reference", "Alzador"]
                            },
                            "then": "$$item.value",
                            "else": 0
                        }
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "Total Cortados": {
                "$reduce": {
                    "input": "$Total Cortados",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            },
            "Total Encallados": {
                "$reduce": {
                    "input": "$Total Encallados",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            },
            "Total Cortados y Encallados": {
                "$reduce": {
                    "input": "$Total Cortados y Encallados",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            },
            "Total Alzados": {
                "$reduce": {
                    "input": "$Total Alzados",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    },

    {
        "$addFields":
            {
                "Peso aproximado Alzados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$Total Alzados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                    ]
                }
            }
    }

    , {
        "$lookup": {
            "from": "form_despachodecosecha",
            "as": "form_despacho_cosecha",
            "let": {
                "vagon": "$Codigo Vagon  Asociado a Despacho",
                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                "fecha": "$Fecha de puesta en caja"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Codigo Vagon Asociado a Despacho", "$$vagon"] },
                                { "$eq": ["$Numero de Viaje de Vagon", "$$num_viaje_vagon"] },
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de Llenado" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de llenado" } } }
                                    ]
                                }


                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": 1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$form_despacho_cosecha",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields":
            {
                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso total segun ticket" }, -1] },
                "Numero de ticket": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Numero de Ticket" }, -1] }
            }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "vagon": "$Codigo Vagon  Asociado a Despacho",
                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                "ticket": "$Numero de ticket"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_peso_racimos_alzados_lote_viaje": {
                "$sum": "$Peso aproximado Alzados"
            },
            "total_alzados_lote": {
                "$sum": "$Total Alzados"
            }
        }
    },
    {
        "$group": {
            "_id": {
                "vagon": "$_id.vagon",
                "num_viaje_vagon": "$_id.num_viaje_vagon",
                "ticket": "$_id.ticket"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "total_peso_racimos_alzados_viaje": {
                "$sum": "$total_peso_racimos_alzados_lote_viaje"
            }

        }
    },
    {
        "$unwind": "$data"
    },

    {
        "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_alzados_lote"
            }
    },

    {
        "$unwind": "$data.data"
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                        "total_alzados_lote": "$total_alzados_lote"
                        , "(%) Alzados x lote": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                }
                            }
                        }
                    }
                ]
            }
        }
    }

    , {
        "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

                }
            }
    }

    , {
        "$addFields": {
            "Peso REAL lote": {
                "$cond": {
                    "if": { "$eq": ["$Total Alzados", 0] },
                    "then": 0,
                    "else": {
                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                    }
                }
            }
        }
    }
    , {
        "$project": {
            "form_despacho_cosecha": 0,
            "empleados_cosecha_adicionales": 0,
            "info_empleado_adicional": 0,
            "anio_filtro": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote",
                "cultivo": "$cultivo"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] },
                                { "$eq": ["$CULTIVO", "$$cultivo"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": false
        }
    },



    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra lote": {"$toString":"$referencia_siembras.SIEMBRA"},
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    },

    {
        "$project": {
            "referencia_siembras": 0
        }
    }


    , { "$unwind": "$Empleados de Cosecha" }

    , {
        "$addFields": {
            "oid_empleado": { "$toObjectId": "$Empleados de Cosecha._id" }
        }
    }

    , {
        "$lookup": {
            "from": "employees",
            "localField": "oid_empleado",
            "foreignField": "_id",
            "as": "info_empleado"
        }
    }
    , { "$unwind": "$info_empleado" }

    , {
        "$lookup": {
            "from": "companies",
            "localField": "info_empleado.cid",
            "foreignField": "_id",
            "as": "info_empresa"
        }
    }
    , { "$unwind": "$info_empresa" }

    , {
        "$addFields": {
            "Empresa empleado": "$info_empresa.name"
        }
    }

    , {
        "$lookup": {
            "from": "form_costodeactividadesxempresa",
            "as": "costos_actividades_x_empresa",
            "let": {
                "fecha_siembra": "$Siembra lote",
                "empresa": "$Empresa empleado"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$fecha_siembra", "$Fecha de Siembra"] },
                                { "$eq": ["$$empresa", "$Empresa"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$costos_actividades_x_empresa",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "peso": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$Empleados de Cosecha.value" }, 0] }, "$Peso REAL lote"] },
            "cargo": "$Empleados de Cosecha.reference"
        }
    }


    , {
        "$addFields": {
            "precio_actividad": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE FRUTO" }, 0] }
                        }

                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE FRUTO" }, 0] }
                        }
                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE FRUTO" }, 0] }
                        }

                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE FRUTO" }, 0] }
                        }
                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE FRUTO" }, 0] }
                        }
                    ],
                    "default": 0
                }
            },

            "precio_herramienta": {
                "$switch": {
                    "branches": [

                        {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE HERRAMIENTA" }, 0] }
                        }

                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE HERRAMIENTA" }, 0] }
                        }
                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE HERRAMIENTA" }, 0] }
                        }

                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE HERRAMIENTA" }, 0] }
                        }
                        , {
                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE HERRAMIENTA" }, 0] }
                        }
                    ],
                    "default": 0
                }
            }
        }
    },


    {
        "$addFields": {
            "costo_actividad": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_actividad" }, 0] }, "$peso"] },
            "costo_herramienta": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_herramienta" }, 0] }, "$peso"] }
        }
    }


    , {
        "$addFields": {
            "costo": { "$sum": ["$costo_actividad", "$costo_herramienta"] }
        }
    }

    , {
        "$project": {
            "vagon": "$Codigo Vagon  Asociado a Despacho",
            "num_viaje": "$Numero de Viaje de Vagon",
            "finca": "$Finca",
            "bloque": "$Bloque",
            "lote": "$lote",
             "siembra": "$Siembra lote",
            "Hectareas": "$Hectareas",
            "num_palmas": "$num_palmas",
            "Material": "$Material",
            
            "fecha": "$Fecha de puesta en caja",
            "fecha puesta en caja": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de puesta en caja" } },
            "ticket": "$Numero de ticket",
            "cargo": "$cargo",
            "nombre": { "$concat": ["$info_empleado.code", "-", "$info_empleado.firstName", " ", "$info_empleado.lastName"] },
            "empresa": "$Empresa empleado",
            "racimos": { "$ifNull": [{ "$toDouble": "$Empleados de Cosecha.value" }, 0] },
            "peso_real_lote": "$Peso REAL lote",
            "peso": "$peso",
            "precio_actividad": "$precio_actividad",
            "precio_herramienta": "$precio_herramienta",
            "costo_actividad": "$costo_actividad",
            "costo_herramienta": "$costo_herramienta",
            "total_pago_empleado": "$costo",
            "Point": "$Point",
            "rgDate": "$Fecha de puesta en caja"
        }
    }


    , {
        "$addFields": {
            "_NOTA_": {
                "$cond": {
                    "if": { "$eq": ["$ticket", -1] },
                    "then": "Sin Despacho (vagon,fecha puesta en caja y fecha inicio y/o fin de llenado en despacho",
                    "else": ""
                }
            }
        }
    }

]