[
    
    {"$match": {"LOTE.path": { "$ne": "" }}},
    
    { "$addFields": { "anio_filtro": { "$year": "$FECHA DE COSECHA" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },
    { "$addFields": { "rgDate": "$FECHA DE COSECHA" } }
    
    ,{
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": 
                            { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": 
                            { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },
                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": 
                            { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": 
                            { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$LOTE.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$LOTE.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }

    , {
        "$addFields": {
            "anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }
    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote",
                "nombre_finca": "$finca"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "CACAO"] },
                                { "$eq": ["$FINCA", "$$nombre_finca"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra": "$referencia_siembras.SIEMBRA",
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    }

    , {
        "$project": {
            "referencia_siembras": 0
        }
    }



]