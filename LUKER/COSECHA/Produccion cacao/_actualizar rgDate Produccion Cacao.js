var cursor = db.form_producciondecacao.aggregate(
    
    {
        $addFields:{
            fecha: "$FECHA DE COSECHA"
        }
    }
    
);

cursor.forEach(i=>{
    db.form_producciondecacao.update(
        {
            _id: i._id
        },
        {
            $set:{
                rgDate:i.fecha
            }
        }
        )
})
   