//--mapa
db.form_producciondecacao.aggregate(
    [

        //--condicionales base
        { "$match": { "LOTE.path": { "$ne": "" } } },
        { "$addFields": { "anio_filtro": { "$year": "$FECHA DE COSECHA" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        //---fechas
        { "$addFields": { "rgDate": "$FECHA DE COSECHA" } }
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }
        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }
        
        
        ,{
            "$addFields": { "Cartography": "$LOTE", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }

        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "Mes_Txt": "$Mes_Txt",//---agregado
                    // "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "Total BABA KILO": { "$sum": "$BABA KILO" }
            }
        }
        
        ,{
            "$addFields": {
                "Total SECO KILO": {
                    "$divide":["$Total BABA KILO",2.75]//FACTOR DE CONVERSION DE BABA A SECOS
                }
            }
        }
        
        
        
        
        //---Color
        
        // KG Baba:#ffffff,[0]:#666666,(0-50):#538032,[50-100):#30f633,[100-200):#ffff00,[200-500):#bf8f00,[>=500):#ff0000
        // KG Secos:#ffffff,[0]:#666666,(0-50):#538032,[50-100):#30f633,[100-200):#ffff00,[200-500):#bf8f00,[>=500):#ff0000
        ,{
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$eq": ["$Total SECO KILO", 0]
                        },
                        "then": "#666666",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$Total SECO KILO", 0] }, { "$lt": ["$Total SECO KILO", 50] }]
                                },
                                "then": "#538032",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$Total SECO KILO", 50] }, { "$lt": ["$Total SECO KILO", 100] }]

                                        },
                                        "then": "#30f633",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$Total SECO KILO", 100] }, { "$lt": ["$Total SECO KILO", 200] }]
                                                },
                                                "then": "#ffff00",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$Total SECO KILO", 200] }, { "$lt": ["$Total SECO KILO", 500] }]
                                                        },
                                                        "then": "#bf8f00",
                                                        "else": "#ff0000"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                // KG Baba:#ffffff,[0]:#666666,(0-50):#538032,[50-100):#30f633,[100-200):#ffff00,[200-500):#bf8f00,[>=500):#ff0000
                // KG Secos:#ffffff,[0]:#666666,(0-50):#538032,[50-100):#30f633,[100-200):#ffff00,[200-500):#bf8f00,[>=500):#ff0000
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$Total SECO KILO", 0]
                        },
                        "then": "A-[0]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$Total SECO KILO", 0] }, { "$lt": ["$Total SECO KILO", 50] }]
                                },
                                "then": "B-(0-50)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$Total SECO KILO", 50] }, { "$lt": ["$Total SECO KILO", 100] }]

                                        },
                                        "then": "C-[50-100)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$Total SECO KILO", 100] }, { "$lt": ["$Total SECO KILO", 200] }]
                                                },
                                                "then": "C-[100-200)",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$Total SECO KILO", 200] }, { "$lt": ["$Total SECO KILO", 500] }]
                                                        },
                                                        "then": "E-[200-500)",
                                                        "else": "F-[>=500)"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // ,{
        //     "$project": {
        //         "_id": {
        //             "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
        //         },
        //         "idform": "$_id.idform",
        //         "type": "Feature",
        //         "properties": {
        //             "Lote": "$_id.nombre_lote",
        //             "rango": "$rango",
        //             "color": "$color",
        //             //"Total Baba Kg": "$Total BABA KILO"
        //             "Mes": "$_id.Mes_Txt"
        //         },
        //         "geometry": {
        //             "$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
        //         }
        //     }
        // }
    ]
)