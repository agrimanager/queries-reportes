[
    
    {
        "$addFields": { "Cartography": "$LOTE", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    }
    , {
        "$group": {
            "_id": {
                "nombre_lote": "$Cartography.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
            ,"Total BABA KILO":{"$sum":"$BABA KILO"}
        }
    }

    ,{
        "$project": {
            "_id": {
                "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
            },
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.nombre_lote",
                "Total Baba Kg": "$Total BABA KILO"
            },
            "geometry": {
                "$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
            }
        }
    }
]