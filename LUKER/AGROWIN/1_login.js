
//Escriba a continuación la URL donde se encuentra su Agente de servicios web de ContaPyme.
var URLUbicacion = '200.31.20.182:9000'
var URLFuncion = '/datasnap/rest/TBasicoGeneral/"GetAuth"/'; 
//Se construye la URL completa la cual es la concatenación de la ubicación y la función
var URL = URLUbicacion + URLFuncion;
//Invocamos la función que retorna controlKey para modo aprendizaje
var controlkey = getControlKey(URLUbicacion);
//1001 es el iapp configurado para agente de servicios web de ContaPyme.
var iapp = "1001";

//dataJSON: parámetros de entrada para la función
var dataJSON = {
    "email": "hfernanez@sigag.com",
    "password": "c4ca4238a0b923820dcc509a6f75849b",
    "idmaquina": "SRVPALNECMZ01"
};
//Se arma los 4 parámetros de entrada de la funcion
var JSONSend ={ "_parameters" : [ JSON.stringify(dataJSON), controlkey, iapp ,"0" ] };

//se constuye objeto para realizar la petición desde JavaScript
var xhr = new XMLHttpRequest();
//Se inicializa la solicitud enviando el verbo y la URL a invocar
xhr.open("POST",URL);
//Se define el evento que se disparará cuando se resuelva la petición
xhr.onreadystatechange = function() {
//se verifica que la petición se hubiese terminado
if (xhr.readyState == 4 && xhr.status == 200) {
//se envia la respuesta del servidor para que se imprima
imprimirRespuesta(xhr.responseText)
    }
};
//Envía la solicitud adjuntando el JSONSend que contiene los 4 parametros de la función
xhr.send(JSON.stringify(JSONSend));
