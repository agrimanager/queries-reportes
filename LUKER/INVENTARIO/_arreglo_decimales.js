// db.suppliesTimeline.updateMany(
//     {
//         "information": "carga masiva de transacciones de ingresos"
//     },
//     {
//         $set: {
//             "subtotal": 201850216,
//             "total": 201850216,
//             "valueByUnity": 4923176,
//         }
//     }


// )


var cursor = db.suppliesTimeline.aggregate(
    {
        $match: {
            "information": "carga masiva de transacciones de ingresos"
        }
    }
)

//cursor

cursor.forEach(i => {
    db.suppliesTimeline.update(
        {
            "_id": i._id
        },
        {
            $set: {
                "subtotal": (i.subtotal/100),
                "total": (i.total/100),
                "valueByUnity": (i.valueByUnity/100)
            }
        }
        )
})