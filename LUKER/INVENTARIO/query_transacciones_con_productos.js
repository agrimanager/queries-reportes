db.suppliesTimeline.aggregate(

    {
        $lookup: {
            from: "supplies",
            as: "supply",
            localField: "sid",
            foreignField: "_id"
        }
    },
    {
        $unwind: "$supply"
    },
    {
        $addFields:{
            "supply":"$supply.name"
        }
    }

)