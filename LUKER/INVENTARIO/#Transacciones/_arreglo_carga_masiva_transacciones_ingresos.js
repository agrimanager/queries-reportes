
//....cambiar typeSupply=Ingresos de transaccion de ingreso
db.suppliesTimeline.aggregate([
    {
        $match: {
            typeSupply: "Ingresos"
        }
    },
    {
        $project: {
            sid: 1
        }
    },
    {
        $lookup: {
            from: "supplies",
            as: "supply",
            localField: "sid",
            foreignField: "_id"
        }
    },
    {
        $unwind: "$supply"
    },
    {
        $project: {
            supplyType: "$supply.type"
        }
    }
]).forEach(cambio => {
    //db.suppliesTimeline.update({_id: cambio._id}, {$set: {typeSupply: cambio.supplyType}})
    db.suppliesTimeline.update({_id: cambio._id}, {$set: {typeSupply: cambio.supplyType, "invoiceNumber" : "0"}})
});