[
    { "$project": { "_id": "$_id" } }


    , {
        "$lookup": {
            "from": "tasks",
            "as": "labores",
            "let": {
                "id_usr": "$_id"
            },
            "pipeline": [
                {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                },
                { "$unwind": "$activity" },


                {
                    "$addFields": {
                        "activity": "$activity.name"
                    }
                },



                {
                    "$match": {
                        "activity": {
                            "$in":
                                [
                                    "HORA TRACTOR PQ CONTROL ENFERMEDADES CACAO-Agricola El Poleo-V C C",
                                    "HORA TRACTOR PQ CONTROL ENFERMEDADES CACAO-Agroindustria Feleda-V C C",
                                    "LABORES LABORATORIO DE SANIDAD-ENFERMEDADES-Agroindustria Feleda-P V C",
                                    "SERVICIO MANTENIMIENTO CONTROL ENFERMEDADES PALMA-Agroindustria Feleda-P V C",
                                    "SERVICIO PISTA FUMIGACION AEREA-ENFERMEDADES-Agroindustria Feleda-P V C",
                                    "SERVICIO PISTA FUMIGACION AEREA-ENFERMEDADES-Nexarte Servicios temporales S.A-P V C",
                                    "CENSO DE ENFERMEDADES-LUKER AGRICOLA-P V C",
                                    "SERVICIO TRACTOR - CONTROL ENFERMEDADES-Agricola El Poleo-N C A",
                                    "SERVICIO TRACTR - CONTROL ENFERMEDADES-LUKER AGRICOLA-N C A"

                                ]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "farm_str": { "$toString": "$farm" }
                        , "finca": "Palma Villanueva Casanare"
                    }
                }
                , {
                    "$match": {
                        "farm_str": "5d2648a845a0dd2e9e204fe2"
                    }
                }


                , {
                    "$match": {
                        "cartography.features": { "$ne": [] }
                    }
                }
                , { "$unwind": "$cartography.features" }

                , {
                    "$addFields": {
                        "lote": "$cartography.features.properties.name"
                    }
                }



                , {
                    "$addFields": {
                        "fecha": {
                            "$arrayElemAt": [
                                "$when",
                                { "$subtract": [{ "$size": "$when" }, 1] }
                            ]
                        }
                    }
                }
                , {
                    "$addFields": {
                        "fecha": "$fecha.start"
                    }
                }
                , {
                    "$addFields": {
                        "anio": { "$year": "$fecha" },
                        "num_mes": { "$month": "$fecha" },
                        "semana": { "$week": "$fecha" }
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "lote": "$lote",

                            "anio": "$anio",
                            "num_mes": "$num_mes"

                        }
                        , "fecha_min": { "$min": "$fecha" }
                    }
                }

                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$_id",
                                {
                                    "fecha_min": "$fecha_min",
                                    "tipo": "programado_labores"
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }

    , {
        "$lookup": {
            "from": "form_sanidadenfermedades",
            "as": "censo_enfermedades",
            "let": {
                "id_usr": "$_id"
            },
            "pipeline": [

                {
                    "$match": {
                        "Lote.path": { "$ne": "" }
                    }
                },
                {
                    "$addFields": {
                        "split_path": {
                            "$split": [{
                                "$trim": {
                                    "input": "$Lote.path",
                                    "chars": ","
                                }
                            }, ","]
                        }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$map": {
                                "input": "$split_path",
                                "as": "strid",
                                "in": {
                                    "$toObjectId": "$$strid"
                                }
                            }
                        }
                    }
                },
                {
                    "$addFields": {
                        "features_oid": {
                            "$map": {
                                "input": "$Lote.features",
                                "as": "item",
                                "in": {
                                    "$toObjectId": "$$item._id"
                                }
                            }
                        }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_oid",
                                "$features_oid"
                            ]
                        }
                    }
                },
                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },
                {
                    "$addFields": {
                        "finca": {
                            "$arrayElemAt": ["$objetos_del_cultivo", 0]
                        },
                        "bloque": {
                            "$arrayElemAt": ["$objetos_del_cultivo", 1]
                        },
                        "lote": {
                            "$arrayElemAt": ["$objetos_del_cultivo", 2]
                        }
                    }
                },
                {
                    "$addFields": {
                        "bloque": "$bloque.properties.name",
                        "lote": "$lote.properties.name"
                    }
                },

                {
                    "$addFields": {
                        "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
                    }
                },
                {
                    "$addFields": {
                        "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                {
                    "$addFields": {
                        "finca": "$finca.name"
                    }
                },
                {
                    "$unwind": "$finca"
                },
                {
                    "$project": {
                        "split_path": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "features_oid": 0
                    }
                }

                , {
                    "$addFields": {
                        "Mes_Num": {
                            "$month": {
                                "date": "$rgDate"
                            }
                        },
                        "anio_Num": {
                            "$year": {
                                "date": "$rgDate"
                            }
                        }
                    }
                }


                , {
                    "$project": {
                        "Point": "$Point",
                        "rgDate": "$rgDate",
                        "finca": "$finca",
                        "bloque": "$bloque",
                        "lote": 1,
                        "linea": 1,
                        "planta": 1,
                        "anio_Num": 1,
                        "Mes_Num": 1,
                        "Enfermedades": 1,
                        "supervisor": 1
                    }
                }

                , {
                    "$addFields": {
                        "anio": { "$year": "$rgDate" },
                        "num_mes": { "$month": "$rgDate" },
                        "semana": { "$week": "$rgDate" }
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "lote": "$lote",

                            "anio": "$anio_Num",
                            "num_mes": "$Mes_Num"

                        }
                        , "fecha_min": { "$min": "$rgDate" }
                    }
                }

                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$_id",
                                {
                                    "fecha_min": "$fecha_min",
                                    "tipo": "ejecutado_censo"
                                }
                            ]
                        }
                    }
                }

            ]

        }
    }


    , {
        "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$labores"
                        , "$censo_enfermedades"
                    ]
                }
            }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }
    , {
        "$addFields": {
            "rgDate": "$fecha_min"
        }
    }
    , {
        "$addFields": {
            "fecha_min": {
                "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_min" }
            }
        }
    }


    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] },
                                { "$eq": ["$CULTIVO", "PALMA"] }
                            ]
                        }
                    }
                },

                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": false
        }
    },



    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra": "$referencia_siembras.SIEMBRA",
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    },

    {
        "$project": {
            "referencia_siembras": 0
        }
    }

    , {
        "$addFields": {
            "mes": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }



    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "lote": "$lote",
                "anio": "$anio",
                "num_mes": "$num_mes",
                "mes": "$mes",

                "Hectareas": "$Hectareas",
                "Siembra": "$Siembra",
                "num_palmas": "$num_palmas",
                "Material": "$Material"

            },
            "data": {
                "$push": "$$ROOT"
            }
            , "rgDate": {
                "$min": "$rgDate"
            }
        }
    }



    , {
        "$addFields": {
            "tipo_filtro_programado": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": {
                        "$eq": ["$$item.tipo", "programado_labores"]
                    }
                }
            },
            "tipo_filtro_ejecucion": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": {
                        "$eq": ["$$item.tipo", "ejecutado_censo"]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "tipo_filtro_programado": {
                "$cond": {
                    "if": { "$gt": [{ "$size": "$tipo_filtro_programado" }, 0] },
                    "then": "SI",
                    "else": "NO"
                }
            },
            "tipo_filtro_ejecucion": {
                "$cond": {
                    "if": { "$gt": [{ "$size": "$tipo_filtro_ejecucion" }, 0] },
                    "then": "SI",
                    "else": "NO"
                }
            }
        }
    }

    , {
        "$addFields": {
            "ha_programado": {
                "$cond": {
                    "if": { "$eq": ["$tipo_filtro_programado", "SI"] },
                    "then": "$_id.Hectareas",
                    "else": 0
                }
            },
            "ha_ejecucion": {
                "$cond": {
                    "if": { "$eq": ["$tipo_filtro_ejecucion", "SI"] },
                    "then": "$_id.Hectareas",
                    "else": 0
                }
            }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "tipo_filtro_programado": "$tipo_filtro_programado",
                        "tipo_filtro_ejecucion": "$tipo_filtro_ejecucion",
                        "ha_programado": "$ha_programado",
                        "ha_ejecucion": "$ha_ejecucion",

                        "rgDate": "$rgDate"
                    }
                ]
            }
        }
    }

]