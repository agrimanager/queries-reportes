db.form_sanidadcacaoenfermedades.aggregate(
    [


        // //----------------TEST
        // {
        //     "$match": {
        //         "$and": [
        //             {
        //                 "rgDate": {
        //                     //"$gte": "2020-01-01T21:10:17.000Z"
        //                     //"$gte": {"$toDate":"2020-01-01T21:10:17.000Z"}
        //                     "$gte": ISODate("2020-07-20T11:44:17.117-05:00")
        //                 }
        //             },
        //             {
        //                 "rgDate": {
        //                     //"$lte": {"$toDate":"2020-02-20T21:10:17.000Z"}
        //                     "$lte": ISODate("2020-09-01T11:44:17.117-05:00")
        //                 }
        //             }
        //         ]
        //     }
        // },

        // {
        //     "$match": {
        //         "Point.farm": "5d26499b64f5b87ffc809ebf"
        //     }
        // },
        // //----------------


        //===== CONDICIONALES BASE
        //t1
        {
            "$match": {
                "Enfermedad": { "$ne": "Planta Sana" }
            }
        },




        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },



        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0
            }
        }


        //===== MESTRO ENLAZADO
        //=====================================================
        //--nombre_maestro : aaaaa
        //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
        //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
        //--valor_mestro_enlazado : ccc cc c
        //=====================================================

        //--Maestro principal
        , {
            "$addFields": {
                "nombre_maestro_principal": "Enfermedad_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }


        //--Mestro enlazado
        , {
            "$addFields": {
                // "nombre_mestro_enlazado": {
                "Grupo_nombre": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        //"then": "$$dataKV.k",
                                        "then": {
                                            "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                { "$strLenCP": "$$dataKV.k" }]
                                        },
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

        /*
        , {
            "$unwind": {
                // "path": "$nombre_mestro_enlazado",
                "path": "$Grupo_nombre",
                "preserveNullAndEmptyArrays": true
            }
        }
        */

        //----FALLA EN LA WEB
        //---//obtener el primero
        , {
            "$addFields": {
                "Grupo_nombre": { "$arrayElemAt": ["$Grupo_nombre", 0] }
            }
        }

        //--Valor Mestro enlazado
        , {
            "$addFields": {
                // "valor_mestro_enlazado": {
                "Grupo_valor": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

        , {
            "$unwind": {
                // "path": "$valor_mestro_enlazado",
                "path": "$Grupo_valor",
                //"preserveNullAndEmptyArrays": true
                "preserveNullAndEmptyArrays": false
            }
        }



        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0,

                "Enfermedad_Planta Sana": 0,
                "Enfermedad_Moni Grup 1": 0,
                "Enfermedad_Moni Grup 2": 0,
                "Enfermedad_Escoba de bruja Moniliophthora pernisiosa": 0,
                "Enfermedad_Monilia grup1": 0,
                "Enfermedad_Phytophthora en tallo": 0,
                "Enfermedad_moninilis grupo 2": 0

            }
        }



        //====== FECHAS
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }



        // //====== INFO LOTE
        // , {
        //     "$lookup": {
        //         "from": "form_cargueinformaciondeplantas",
        //         "as": "referencia_siembras",
        //         "let": {
        //             "nombre_lote": "$lote",
        //             "nombre_finca": "$finca"
        //         },
        //         "pipeline": [
        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$eq": ["$CULTIVO", "CACAO"] },
        //                             { "$eq": ["$FINCA", "$$nombre_finca"] },
        //                             { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
        //                             { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
        //                         ]
        //                     }
        //                 }
        //             },
        //             {
        //                 "$limit": 1
        //             }

        //         ]
        //     }
        // },

        // {
        //     "$unwind": {
        //         "path": "$referencia_siembras",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // },

        // {
        //     "$addFields": {
        //         "Hectareas": "$referencia_siembras.HECTAREAS",
        //         "Siembra": "$referencia_siembras.SIEMBRA",
        //         //         "num_palmas": "$referencia_siembras.PALMAS",
        //         "Material": "$referencia_siembras.MATERIAL"
        //     }
        // }

        // , {
        //     "$project": {
        //         "referencia_siembras": 0
        //     }
        // }




        //====== INCIDENCIA


        // ,{
        //     $match:{
        //         Enfermedad:"Planta Sana"
        //     }
        // }



        //---group
        , {
            "$group": {
                "_id": {
                    // "finca": "$finca",
                    // "lote": "$lote",

                    "enfermedad": "$Enfermedad",
                    //"grupo_nombre": "$Grupo_nombre",
                    "grupo_valor": "$Grupo_valor"
                }
                , "count": { "$sum": 1 }

                , "sum_cantidad": { "$sum": "$Cantidad" }
                , "sum_cantidad_mazorcas": { "$sum": "$Cant  mazorcas  P1 a P6" }
                , "sum_plantas_a_evaluar": { "$sum": "$Planta a evaluar" }

                //, "data":{"$push":"$$ROOT"}


            }
        }

        //t2
        //---proceso1
        , {
            "$addFields": {
                "filtro_data": {
                    "$switch": {
                        "branches": [
                            //---MONILIA
                            //--g1
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Enfermos"] }
                                        , { "$ne": ["$_id.enfermedad", "Moni Grup 1"] }
                                    ]
                                },
                                "then": "si borrar"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Sanos"] }
                                        , { "$ne": ["$_id.enfermedad", "Moni Grup 1"] }
                                    ]
                                },
                                "then": "si borrar"
                            },

                            //--g2
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.grupo_valor", "Moni Grup 2 FrutsEnfermos"] }
                                        , { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
                                    ]
                                },
                                "then": "si borrar"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.grupo_valor", "Moni Grup 2 Frutos sanos"] }
                                        , { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
                                    ]
                                },
                                "then": "si borrar"
                            },
                        ],
                        "default": "no borrar"
                    }
                }
            }
        }


        , {
            "$match": {
                "filtro_data": "no borrar"
            }
        }

        //---proceso2
        , {
            "$addFields": {
                "filtro_data": {
                    "$switch": {
                        "branches": [
                            //---MONILIA
                            //--g1
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.enfermedad", "Moni Grup 1"] }
                                        , {
                                            "$not":
                                                { "$in": ["$_id.grupo_valor", ["Moni Grup 1 Fruts Enfermos", "Moni Grup 1 Fruts Sanos"]] }
                                        }
                                    ]
                                },
                                "then": "si borrar"
                            },
                            //--g2
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$_id.enfermedad", "Moni Grup 2"] }
                                        , {
                                            "$not":
                                                { "$in": ["$_id.grupo_valor", ["Moni Grup 2 FrutsEnfermos", "Moni Grup 2 Frutos sanos"]] }
                                        }
                                    ]
                                },
                                "then": "si borrar"
                            }
                        ],
                        "default": "no borrar"
                    }
                }
            }
        }


        , {
            "$match": {
                "filtro_data": "no borrar"
            }
        }










    ]
)