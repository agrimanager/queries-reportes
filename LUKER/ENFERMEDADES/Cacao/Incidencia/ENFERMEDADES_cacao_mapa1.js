//reporte
db.form_sanidadcacaoenfermedades.aggregate(
    [

        // {
        //     "$addFields": {
        //         "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": {
        //             "$concatArrays": [
        //                 "$split_path_oid",
        //                 "$features_oid"
        //             ]
        //         }
        //     }
        // },


        //---test
        {
            "$match": {
                "$and": [
                    {
                        "rgDate": {
                            //"$gte": "2020-01-01T21:10:17.000Z"
                            //"$gte": {"$toDate":"2020-01-01T21:10:17.000Z"}
                            "$gte": ISODate("2020-07-06T11:44:17.117-05:00")
                        }
                    },
                    {
                        "rgDate": {
                            //"$lte": {"$toDate":"2020-02-20T21:10:17.000Z"}
                            "$lte": ISODate("2020-09-20T11:44:17.117-05:00")
                        }
                    }
                ]
            }
        },


        //---new
        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        //---new
        {
            "$addFields": {
                "Cartography": "$lote"
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },

        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_muestradas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }



        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_muestradas_x_lote": "$plantas_muestradas_x_lote"
                        }
                    ]
                }
            }
        }



        //---
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "plaga": "$Plaga"

                    , "plantas_muestradas_x_lote": "$plantas_muestradas_x_lote"
                }
                , "total_muertos": { "$sum": "$Número de Muertas" }
                , "data": { "$push": "$$ROOT" }
            }
        }

        // , { "$unwind": "$data" }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "total_muertos": "$total_muertos"
        //                 }
        //             ]
        //         }
        //     }
        // }


        , {
            "$addFields": {
                "indicador": { "$multiply": [{ "$divide": ["$total_muertos", "$_id.plantas_muestradas_x_lote"] }, 100] }
            }
        }


        //%Incidencia: #ffffff,[0% - 10%):#ff0000,[10%-30%):#ff8000,[30%-50%):#ffff00,[50%-90%) :#008000,[90%-100%):#00FF00,(>=100%):#3f3b69


        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$indicador", 0] }, { "$lt": ["$indicador", 10] }]
                        },
                        "then": "#ff0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$indicador", 10] }, { "$lt": ["$indicador", 30] }]
                                },
                                "then": "#ff8000",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 30] }, { "$lt": ["$indicador", 50] }]
                                        },
                                        "then": "#ffff00",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 50] }, { "$lte": ["$indicador", 90] }]
                                                },
                                                "then": "#008000",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$indicador", 90] }, { "$lte": ["$indicador", 100] }]
                                                        },
                                                        "then": "#00FF00",
                                                        "else": "#3f3b69"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$indicador", 0] }, { "$lt": ["$indicador", 10] }]
                        },
                        "then": "A-[0% - 10%)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$indicador", 10] }, { "$lt": ["$indicador", 30] }]
                                },
                                "then": "B-[10%-30%)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 30] }, { "$lt": ["$indicador", 50] }]
                                        },
                                        "then": "C-[30%-50%)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 50] }, { "$lte": ["$indicador", 90] }]
                                                },
                                                "then": "D-[50%-90%)",
                                                "else": {
                                                    "$cond": {
                                                        "if": {
                                                            "$and": [{ "$gte": ["$indicador", 90] }, { "$lte": ["$indicador", 100] }]
                                                        },
                                                        "then": "E-[90%-100%)",
                                                        "else": "F-(>=100%)"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
            }
        }


        , {
            "$project": {
                "_id": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                    // "$arrayElemAt": ["$data.data.elemnq", { "$subtract": [{ "$size": "$data.data.elemnq" }, 1] }]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.lote",
                    "Plaga": "$_id.plaga",
                    "Rango": "$rango",
                    "%Incidencia": {
                        "$concat": [
                            { "$toString": "$indicador" },
                            " %"
                        ]
                    },

                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                    // "$arrayElemAt": ["$data.data.Cartography.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.geometry" }, 1] }]
                }
            }
        }






    ], { allowDiskUse: true }
)