[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" }
    , {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }




    , {
        "$addFields": {
            "nombre_maestro_principal": "Enfermedad_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }


    , {
        "$addFields": {
            "Grupo_nombre": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "Grupo_nombre": { "$arrayElemAt": ["$Grupo_nombre", 0] }
        }
    }


    , {
        "$addFields": {
            "Grupo_valor": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$Grupo_valor",
            "preserveNullAndEmptyArrays": true
        }
    }



    , {
        "$project": {
            "nombre_maestro_principal": 0,
            "num_letras_nombre_maestro_principal": 0,

            "Enfermedad_Planta Sana": 0,
            "Enfermedad_Moni Grup 1": 0,
            "Enfermedad_Moni Grup 2": 0,
            "Enfermedad_Escoba de bruja Moniliophthora pernisiosa": 0,
            "Enfermedad_Monilia grup1" :0,
            "Enfermedad_Phytophthora en tallo": 0,
            "Enfermedad_moninilis grupo 2" :0

        }
    }

     ,{
        "$addFields": {
            "anio":{ "$year": { "date": "$rgDate" }},
            "num_mes":{ "$month": { "date": "$rgDate" }}
        }
    }

    ,{
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }

]