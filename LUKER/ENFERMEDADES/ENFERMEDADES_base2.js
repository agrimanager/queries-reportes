db.form_sanidadenfermedades.aggregate(
    [
        {
            "$match": {
                "Lote.path": { "$ne": "" }
            }
        },
        {
            "$addFields": {
                "split_path": {
                    "$split": [{
                        "$trim": {
                            "input": "$Lote.path",
                            "chars": ","
                        }
                    }, ","]
                }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$map": {
                        "input": "$split_path",
                        "as": "strid",
                        "in": {
                            "$toObjectId": "$$strid"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "features_oid": {
                    "$map": {
                        "input": "$Lote.features",
                        "as": "item",
                        "in": {
                            "$toObjectId": "$$item._id"
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": {
                    "$arrayElemAt": ["$objetos_del_cultivo", 0]
                },
                "bloque": {
                    "$arrayElemAt": ["$objetos_del_cultivo", 1]
                },
                "lote": {
                    "$arrayElemAt": ["$objetos_del_cultivo", 2]
                }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$addFields": {
                "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        {
            "$unwind": "$finca"
        },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        , {
            "$addFields": {
                "Mes_Num": {
                    "$month": {
                        "date": "$rgDate"
                    }
                },
                "anio_Num": {
                    "$year": {
                        "date": "$rgDate"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [{
                            "case": {
                                "$eq": ["$Mes_Num", 1]
                            },
                            "then": "01-Enero"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 2]
                            },
                            "then": "02-Febrero"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 3]
                            },
                            "then": "03-Marzo"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 4]
                            },
                            "then": "04-Abril"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 5]
                            },
                            "then": "05-Mayo"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 6]
                            },
                            "then": "06-Junio"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 7]
                            },
                            "then": "07-Julio"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 8]
                            },
                            "then": "08-Agosto"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 9]
                            },
                            "then": "09-Septiembre"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 10]
                            },
                            "then": "10-Octubre"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 11]
                            },
                            "then": "11-Noviembre"
                        },
                        {
                            "case": {
                                "$eq": ["$Mes_Num", 12]
                            },
                            "then": "12-Diciembre"
                        }
                        ],
                        "default": "Mes no desconocido"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "num_planta": { "$split": ["$planta", "-"] }
            }
        }

        , {
            "$addFields": {
                "num_planta": {
                    "$sum": [
                        {
                            "$multiply": [
                                { "$toInt": { "$arrayElemAt": ["$num_planta", 2] } },
                                10000
                            ]
                        },
                        { "$toInt": { "$arrayElemAt": ["$num_planta", 3] } }
                    ]
                }
            }
        }

        , {
            "$project": {
                "Point": "$Point",
                "rgDate2": "$rgDate",
                "Fecha de registro": "$Fecha de registro",
                "bloque": "$bloque",
                "lote": 1,
                "Linea": 1,
                "Palma": 1,
                "linea": 1,
                "planta": 1,
                "anio_Num": 1,
                "Mes_Txt": 1,
                "Enfermedades": 1,
                "num_planta": 1,
                "supervisor": 1
            }
        }

        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "PALMA"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }

    ]

)