db.form_sanidadenfermedades.aggregate(
    [
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },
        {
            "$addFields": {
                "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        , {
            "$group": {
                "_id": {
                    "Point": "$Point",

                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "linea": "$linea",
                    "planta": "$planta",
                    "enfermedad": "$Enfermedades"
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "Point": "$_id.Point",

                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote",
                    "linea": "$linea",
                    "enfermedad": "$_id.enfermedad"
                },
                "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": 1 }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad"
                        }
                    ]
                }
            }
        }
        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_informacionLote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },

        { "$unwind": "$referencia_informacionLote" },

        {
            "$addFields": {
                "num_plantas_x_lote": "$referencia_informacionLote.PALMAS"
            }
        },


        {
            "$match": {
                "num_plantas_x_lote": { "$gt": 0 }
            }
        },


        {
            "$addFields": {
                "incidencia_lote_enfermedad": {
                    "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_enfermedad", "$num_plantas_x_lote"] }, 100]
                }
            }
        }

        , {
            "$project": {
                "referencia_informacionLote": 0
            }
        }

        , {
            "$addFields": {
                "incidencia_lote_enfermedad": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia_lote_enfermedad", 100] }, { "$mod": [{ "$multiply": ["$incidencia_lote_enfermedad", 100] }, 1] }] }, 100] }
            }
        }

        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },
        {
            "$project": {
                "referencia_siembras": 0
            }
        }
    ]

)