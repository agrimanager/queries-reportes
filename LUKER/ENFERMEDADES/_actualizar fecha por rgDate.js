// db.form_sanidadcacaoenfermedades.find({
//     "Point.farm": "5d26491264f5b87ffc809eba",
//     "Fecha de Registro": ISODate("1969-12-31T19:00:00.000-05:00"),

// })
//     .projection({})
//     .sort({ _id: -1 })


var cursor = db.form_sanidadcacaoenfermedades.aggregate(

    {
        $match: {
            "Point.farm": "5d26491264f5b87ffc809eba",
            "Fecha de Registro": ISODate("1969-12-31T19:00:00.000-05:00"),
        }
    }

);

cursor.forEach(i => {
    db.form_sanidadcacaoenfermedades.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Fecha de Registro": i.rgDate
            }
        }
    )
})
