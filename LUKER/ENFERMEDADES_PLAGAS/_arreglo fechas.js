

//--1)agregar fecha a datos sin fechas
//----------------------------------------------------------
var cursor = db.form_modulocensoenfermedadplagas.aggregate(
    {
        $match:{
            "Fecha de Registro":{$exists:false}//sin fechas
        }
    }
);
cursor.forEach(i => {
    db.form_modulocensoenfermedadplagas.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Fecha de Registro": i.rgDate
            }
        }
    )
});



//--2)arreglar datos con fechas malas
//----------------------------------------------------------
var cursor = db.form_modulocensoenfermedadplagas.aggregate(
    {
        $match:{
            "Fecha de Registro":{$exists:true} //con fechas
        }
    }
    ,{
        $match:{
            "Fecha de Registro" : ISODate("1969-12-31T19:00:00.000-05:00")//fechas malas
        }
    }
);

cursor.forEach(i => {
    db.form_modulocensoenfermedadplagas.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Fecha de Registro": i.rgDate
            }
        }
    )
});