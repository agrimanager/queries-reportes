db.form_modulocensoenfermedadplagas.aggregate(
    
    
    //--Cartografia
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" }

    , {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    }

    , {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    //---enfermedad
    , {
        "$addFields": {
            "enfermedad": "$Enfermedades"
        }
    },

    //=====================================================
    //--nombre_maestro : aaaaa
    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
    //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
    //--valor_mestro_enlazado : ccc cc c
    //=====================================================

    //--Maestro principal
    {
        "$addFields": {
            "nombre_maestro_principal": "Enfermedades_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }


    //--Mestro enlazado
    , {
        "$addFields": {
            "nombre_grupo_enfermedad": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    //"then": "$$dataKV.k",
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }


    //----FALLA EN LA WEB
    /*
    , {
        "$unwind": {
            "path": "$nombre_mestro_enlazado",
            "preserveNullAndEmptyArrays": true
        }
    }
    */

    //---//obtener el primero
    , {
        "$addFields": {
            "nombre_grupo_enfermedad": { "$arrayElemAt": ["$nombre_grupo_enfermedad", 0] }
        }
    }
    , {
        "$addFields": {
            "nombre_grupo_enfermedad": {"$ifNull":["$nombre_grupo_enfermedad",""]}
        }
    }




    //--Valor Mestro enlazado
    , {
        "$addFields": {
            "valor_grupo_enfermedad": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_grupo_enfermedad",
            "preserveNullAndEmptyArrays": true
        }
    }
    
    , {
        "$addFields": {
            "valor_grupo_enfermedad": {"$ifNull":["$valor_grupo_enfermedad",""]}
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal": 0,
            "num_letras_nombre_maestro_principal": 0
        }
    }



    //---plagas
    , {
        "$addFields": {
            "plaga1": "$Plaga 1"
        }
    },

    //--Maestro principal
    {
        "$addFields": {
            "nombre_maestro_principal": "Plaga 1_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }


    //--Mestro enlazado
    , {
        "$addFields": {
            "nombre_grupo_plaga1": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    //"then": "$$dataKV.k",
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }


    //----FALLA EN LA WEB
    /*
    , {
        "$unwind": {
            "path": "$nombre_mestro_enlazado",
            "preserveNullAndEmptyArrays": true
        }
    }
    */

    //---//obtener el primero
    , {
        "$addFields": {
            "nombre_grupo_plaga1": { "$arrayElemAt": ["$nombre_grupo_plaga1", 0] }
        }
    }
    , {
        "$addFields": {
            "nombre_grupo_plaga1": {"$ifNull":["$nombre_grupo_plaga1",""]}
        }
    }




    //--Valor Mestro enlazado
    , {
        "$addFields": {
            "valor_grupo_plaga1": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$valor_grupo_plaga1",
            "preserveNullAndEmptyArrays": true
        }
    }
    
    , {
        "$addFields": {
            "valor_grupo_plaga1": {"$ifNull":["$valor_grupo_plaga1",""]},
            "cantidad_plaga1": {"$ifNull":["$Cantidad Plaga 1",0]}
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal": 0,
            "num_letras_nombre_maestro_principal": 0
        }
    }







)