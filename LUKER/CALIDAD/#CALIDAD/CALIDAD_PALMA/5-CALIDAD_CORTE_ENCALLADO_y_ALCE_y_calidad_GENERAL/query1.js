db.users.aggregate(
    [


        { "$project": { "_id": "$_id" } }

        //--data_x
        //Data_labores
        , {
            "$lookup": {
                "from": "tasks",
                "as": "Data_labores",
                "let": {"id_usr": "$_id"},
                "pipeline": []
            }
        }
        
        ,{$unwind:"$Data_labores"}
        ,{
            $addFields:{
                "Data_labores":"$Data_labores._id"
            }
        }
        
        //--data_x
        //--Data_cosecha
        , {
            "$lookup": {
                "from": "form_recolecciondecosechaxempleados",
                "as": "Data_cosecha",
                "let": {"id_usr": "$_id"},
                "pipeline": []
            }
        }
        ,{$unwind:"$Data_cosecha"}
        ,{
            $addFields:{
                "Data_cosecha":"$Data_cosecha._id"
            }
        }


        

], { "allowDiskUse": true });