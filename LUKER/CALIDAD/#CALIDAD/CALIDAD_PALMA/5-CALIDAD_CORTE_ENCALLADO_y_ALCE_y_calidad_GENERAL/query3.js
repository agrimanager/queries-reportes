db.users.aggregate(
    [


        { "$project": { "_id": "$_id" } }


        //---test filtro de fechas
        , {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-10T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        }

        //--data_x
        //Data_labores
        , {
            "$lookup": {
                "from": "tasks",
                "as": "Data_labores",
                //"let": {"id_usr": "$_id"},
                "let": {
                    "id_usr": "$_id",
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    //----filtro de fechas
                    //"Fecha inicio": { "$max": "$when.start" },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    
                    {
                        $project:{
                            _id:1
                        }
                    }
                ]
            }
        }

        //--data_x
        //--Data_cosecha
        , {
            "$lookup": {
                "from": "form_recolecciondecosechaxempleados",
                "as": "Data_cosecha",
                // "let": {"id_usr": "$_id"},
                "let": {
                    "id_usr": "$_id",
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    { "$match": { "Empleados de Cosecha": { "$exists": true } } },
                    { "$match": { "Empleados de Cosecha": { "$ne": "" } } },
                    { "$addFields": { "empleados_filtro": { "$size": "$Empleados de Cosecha" } } },
                    { "$match": { "empleados_filtro": { "$gt": 0 } } },


                    //----filtro de fechas
                    //"Fecha inicio": { "$max": "$when.start" },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de puesta en caja" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de puesta en caja" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    
                     {
                        $project:{
                            _id:1
                        }
                    }
                ]
            }
        }




    ], { "allowDiskUse": true });