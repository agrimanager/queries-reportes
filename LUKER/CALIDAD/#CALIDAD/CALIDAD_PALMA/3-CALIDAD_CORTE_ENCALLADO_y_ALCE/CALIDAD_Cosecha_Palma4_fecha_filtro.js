db.users.aggregate(
    [


        // { "$project": { "_id": "$_id" } }

        //---test filtro de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        //--------------------------------------
        //--alce
        //form_evaluacionpostcosechaalce
        {
            "$lookup": {
                "from": "form_evaluacionpostcosechaalce",
                "as": "data1",
                // "let": { "id_usr": "$_id" },
                "let": {
                    // "id_usr": "$_id",
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    //----filtro de fechas
                    //rgDate
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    //--condicionales base
                    {
                        "$match": {
                            "Lotes.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },
                    //--fehca string (sin horas)
                    {
                        "$addFields": {
                            "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } }
                        }
                    },

                    { "$match": { "Empleados alce": { "$exists": true } } },
                    { "$match": { "Empleados alce": { "$ne": "" } } },
                    { "$addFields": { "empleados_filtro": { "$size": "$Empleados alce" } } },
                    { "$match": { "empleados_filtro": { "$gt": 0 } } },

                    //--cartografia
                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lotes.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$Lotes.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$Lotes.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    }


                    , {
                        "$project": {
                            "Lotes": 0,
                            "Point": 0,

                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0,


                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "supervisor_u": 0
                        }
                    }

                    //---plantas_dif_censadas_x_lote
                    , {
                        "$addFields": {
                            "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
                        }
                    }
                    , {
                        "$addFields": {
                            "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "planta": "$planta_aux",
                                "Fecha de Registro": "$Fecha de Registro"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$_id.lote",
                                "Fecha de Registro": "$_id.Fecha de Registro"
                            },
                            "plantas_dif_censadas_x_lote": { "$sum": 1 },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                    }
                                ]
                            }
                        }
                    }

                    //---pasar a numero el maestro numerico
                    , {
                        "$addFields": {
                            "tusa  en vagon": { "$toDouble": "$tusa  en vagon" },
                            "tusa  zorrrillo": { "$toDouble": "$tusa  zorrrillo" },
                            "Pepa sin recoger": { "$toDouble": "$Pepa sin recoger" },
                            "Racimos  sin recoger": { "$toDouble": "$Racimos  sin recoger" },
                            "Fruto sin recoger acopio": { "$toDouble": "$Fruto sin recoger acopio" }
                        }
                    }

                    //---array con criterios
                    , {
                        "$addFields": {
                            "criterios_y_valores": [
                                {
                                    "criterio": "tusa  en vagon",
                                    "valor": { "$toDouble": "$tusa  en vagon" }
                                },
                                {
                                    "criterio": "tusa  zorrrillo",
                                    "valor": { "$toDouble": "$tusa  zorrrillo" }
                                },
                                {
                                    "criterio": "Pepa sin recoger",
                                    "valor": { "$toDouble": "$Pepa sin recoger" }
                                },
                                {
                                    "criterio": "Racimos  sin recoger",
                                    "valor": { "$toDouble": "$Racimos  sin recoger" }
                                },
                                {
                                    "criterio": "Fruto sin recoger acopio",
                                    "valor": { "$toDouble": "$Fruto sin recoger acopio" }
                                }
                            ]
                        }
                    }

                    , { "$unwind": "$criterios_y_valores" }


                    , {
                        "$addFields": {
                            "criterio": "$criterios_y_valores.criterio",
                            "valor": "$criterios_y_valores.valor"
                        }
                    }

                    , {
                        "$project": {
                            "criterios_y_valores": 0,
                            "tusa  en vagon": 0,
                            "tusa  zorrrillo": 0,
                            "Pepa sin recoger": 0,
                            "Racimos  sin recoger": 0,
                            "Fruto sin recoger acopio": 0,
                            "Criterios de evaluacion": 0
                        }
                    }

                    //---valores a mostrar
                    , {
                        "$project": {
                            "fecha": "$Fecha de Registro",
                            "lote": "$lote",

                            "palmas_alce": "$plantas_dif_censadas_x_lote",
                            "palmas_cye": { "$toDouble": 0 },

                            "evaluacion": "palma cosecha alce",
                            "criterio": "$criterio",
                            "cantidad": "$valor"

                            , "rgDate": "$rgDate"


                            // , "filtro_fecha_inicio": "$$filtro_fecha_inicio"
                            // , "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }




                ]

            }
        }


        //--------------------------------------
        //--corte y encallado
        //form_formularioevaluacionpostcosecha
        , {
            "$lookup": {
                "from": "form_formularioevaluacionpostcosecha",
                "as": "data2",
                // "let": { "id_usr": "$_id" },
                "let": {
                    // "id_usr": "$_id",
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    //----filtro de fechas
                    //rgDate
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    //--condicionales base
                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fech de Registro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },
                    //--fehca string (sin horas)
                    {
                        "$addFields": {
                            "Fech de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fech de Registro" } }
                        }
                    },

                    { "$match": { "Empleado": { "$exists": true } } },
                    { "$match": { "Empleado": { "$ne": "" } } },
                    { "$addFields": { "empleados_filtro": { "$size": "$Empleado" } } },
                    { "$match": { "empleados_filtro": { "$gt": 0 } } },

                    //--cartografia
                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$Lote.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$project": {
                            "Lote": 0,
                            "Point": 0,

                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0,


                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "supervisor_u": 0
                        }
                    }

                    //---plantas_dif_censadas_x_lote
                    , {
                        "$addFields": {
                            "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
                        }
                    }
                    , {
                        "$addFields": {
                            "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "planta": "$planta_aux",
                                "Fech de Registro": "$Fech de Registro"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$_id.lote",
                                "Fech de Registro": "$_id.Fech de Registro"
                            },
                            "plantas_dif_censadas_x_lote": { "$sum": 1 },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                    }
                                ]
                            }
                        }
                    }


                    //---valores de empleados

                    , { "$unwind": "$Empleado" }


                    , {
                        "$addFields": {
                            "nombre_Empleado": "$Empleado.name",
                            "labor_Empleado": "$Empleado.reference",
                            "valor_Empleado": "$Empleado.value"
                        }
                    }
                    , {
                        "$project": {
                            "Empleado": 0
                        }
                    }



                    //----------------------------------------------------------
                    //----CONDICION PARA NO MOSTRAR ambiguedades
                    //--🏁DANGER!! //--🏁VENENO!!
                    , {
                        "$match": {
                            "$or": [
                                {
                                    "Criterio Cortador": { "$not": { "$ne": "" } },
                                    "Criterio Encallador": { "$not": { "$ne": "" } }
                                },
                                { "labor_Empleado": { "$not": { "$eq": "" } } }
                            ]
                        }
                    }
                    //----------------------------------------------------------

                    , {
                        "$addFields": {
                            "criterio_Empleado": {
                                "$cond": {
                                    "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                                    "then": "$Criterio Cortador",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                            "then": "$Criterio Encallador",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                                    "then": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$Criterio Encallador", ""] },
                                                            "then": "$Criterio Cortador",
                                                            "else": "$Criterio Encallador"
                                                        }
                                                    },
                                                    "else": "SIN CARGO"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "evaluacion": {
                                "$cond": {
                                    "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                                    "then": "palma cosecha corte",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                            "then": "palma cosecha encallado",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                                    "then": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$Criterio Encallador", ""] },
                                                            "then": "palma cosecha corte",
                                                            "else": "palma cosecha encallado"
                                                        }
                                                    },
                                                    "else": "SIN EVALUACION"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //---valores a mostrar
                    , {
                        "$project": {
                            "fecha": "$Fech de Registro",
                            "lote": "$lote",

                            "palmas_alce": { "$toDouble": 0 },
                            "palmas_cye": "$plantas_dif_censadas_x_lote",


                            "evaluacion": "$evaluacion",
                            "criterio": "$criterio_Empleado",
                            "cantidad": "$valor_Empleado"

                            , "rgDate": "$rgDate"

                            // , "filtro_fecha_inicio": "$$filtro_fecha_inicio"
                            // , "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }


                ]

            }
        }


        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data1"
                            , "$data2"
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //  //----filtro de fechas
        // //rgDate
        // ,{
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //                 {
        //                     "$gte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
        //                         ,
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha_inicio" } } }
        //                     ]
        //                 },

        //                 {
        //                     "$lte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
        //                         ,
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha_fin" } } }
        //                     ]
        //                 }
        //             ]
        //         }
        //     }
        // }



        //---cruzar con afectacion
        , {
            "$lookup": {
                "from": "form_configuracionevaluaciondecalidad",
                "as": "configuracion",
                "let": {
                    "evaluacion": "$evaluacion",
                    "criterio": "$criterio"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$evaluacion", "$Evaluacion"] },
                                    { "$eq": ["$$criterio", "$Criterio"] }

                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$configuracion",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "Afecta calificacion": {
                    "$ifNull": ["$configuracion.Afecta calificacion", "DESACTUALIZADO"]
                }
            }
        }

        , {
            "$addFields": {
                "Afecta calificacion": {
                    "$cond": {
                        "if": { "$eq": ["$Afecta calificacion", "SI"] },
                        "then": "MALA",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$Afecta calificacion", "NO"] },
                                "then": "BUENA",
                                "else": "$Afecta calificacion"
                            }
                        }
                    }
                }
            }
        }


        , {
            "$project": {
                "configuracion": 0
            }
        }

        //----CONDICION PARA NO MOSTRAR datos desactualizados
        , {
            "$match": {
                "Afecta calificacion": { "$ne": "DESACTUALIZADO" }
            }
        }


        //---agregar tipo de criterio
        , {
            "$addFields": {
                "tipo_criterio": {
                    "$cond": {
                        "if": {
                            "$in": ["$criterio",
                                [
                                    "Racimos  sin recoger",
                                    "Racimos enfermos C",
                                    "Racimos maduros C",
                                    "Racimos pedunculo largo E",
                                    "Racimos sin cortar C",
                                    "Racimos sin encallar E",
                                    "Racimos sobre maduros C",
                                    "Racimos vacios C",
                                    "Racimos verdes C",
                                    "Razimos mal polinizados C",
                                    "tusa  en vagon",
                                    "tusa  zorrrillo",
                                    "tuza mal ubicada E"

                                ]]
                        },
                        "then": "Racimos",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$in": ["$criterio",
                                        [
                                            "Corte por detras C",
                                            "Hoja en canales E",
                                            "Hoja sin cortar C",
                                            "Hoja sin encallar E"
                                        ]]
                                },
                                "then": "Hojas",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$in": ["$criterio",
                                                [
                                                    "Fruto de axila C",
                                                    "Fruto sin recoger acomodador E",
                                                    "Fruto sin recoger acopio",
                                                    "Pepa sin recoger"
                                                ]]
                                        },
                                        "then": "Fruto",
                                        "else": "SIN TIPO"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        //--agrupar x lote y fecha
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "fecha": "$fecha",
                }

                , "palmas_alce": { "$max": "$palmas_alce" }
                , "palmas_cye": { "$max": "$palmas_cye" }

                , "sum_cantidad_racimos": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$tipo_criterio", "Racimos"] },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }
                , "sum_cantidad_racimos_malos": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": {
                                        "$and": [
                                            { "$eq": ["$tipo_criterio", "Racimos"] },
                                            { "$eq": ["$Afecta calificacion", "MALA"] }
                                        ]
                                    },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }
                , "sum_cantidad_racimos_buenos": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": {
                                        "$and": [
                                            { "$eq": ["$tipo_criterio", "Racimos"] },
                                            { "$eq": ["$Afecta calificacion", "BUENA"] }
                                        ]
                                    },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }
                , "sum_cantidad_hojas": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$tipo_criterio", "Hojas"] },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }
                , "sum_frutos_cye": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": {
                                        "$in": ["$criterio", [
                                            "Fruto de axila C",
                                            "Fruto sin recoger acomodador E"
                                        ]]
                                    },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }
                , "sum_frutos_alce": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": {
                                        "$in": ["$criterio", [
                                            "Fruto sin recoger acopio",
                                            "Pepa sin recoger",

                                        ]]
                                    },
                                    "then": "$cantidad"
                                }
                            ],
                            "default": 0
                        }
                    }
                }

            }
        }


        //--operaciones de variables
        , {
            "$addFields": {
                "prom_frutos_cye": {
                    "$cond": {
                        "if": { "$eq": ["$palmas_cye", 0] },
                        "then": 0,
                        "else": { "$divide": ["$sum_frutos_cye", "$palmas_cye"] }
                    }
                },
                "prom_frutos_alce": {
                    "$cond": {
                        "if": { "$eq": ["$palmas_alce", 0] },
                        "then": 0,
                        "else": { "$divide": ["$sum_frutos_alce", "$palmas_alce"] }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "porcentaje_frutos_cye": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$prom_frutos_cye", 0] }, { "$lt": ["$prom_frutos_cye", 4] }]
                        },
                        "then": 0,
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$prom_frutos_cye", 4] }, { "$lt": ["$prom_frutos_cye", 6] }]
                                },
                                "then": 10,
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$prom_frutos_cye", 6] }, { "$lt": ["$prom_frutos_cye", 20] }]
                                        },
                                        "then": 30,
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$prom_frutos_cye", 20] }, { "$lt": ["$prom_frutos_cye", 40] }]
                                                },
                                                "then": 60,
                                                "else": 100
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "porcentaje_frutos_alce": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$prom_frutos_alce", 0] }, { "$lt": ["$prom_frutos_alce", 4] }]
                        },
                        "then": 0,
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$prom_frutos_alce", 4] }, { "$lt": ["$prom_frutos_alce", 6] }]
                                },
                                "then": 10,
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$prom_frutos_alce", 6] }, { "$lt": ["$prom_frutos_alce", 20] }]
                                        },
                                        "then": 30,
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$prom_frutos_alce", 20] }, { "$lt": ["$prom_frutos_alce", 40] }]
                                                },
                                                "then": 60,
                                                "else": 100
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //---porcentajes finales
        , {
            "$addFields": {
                "total_porcentaje_frutos": {
                    "$sum": ["$porcentaje_frutos_cye", "$porcentaje_frutos_alce"]
                },
                //--MULTIPLICAR X 100
                "total_porcentaje_hojas": {
                    "$cond": {
                        "if": { "$eq": ["$palmas_cye", 0] },
                        "then": 0,
                        //"else": { "$divide": ["$sum_cantidad_hojas", "$palmas_cye"] }
                        "else": { "$multiply": [{ "$divide": ["$sum_cantidad_hojas", "$palmas_cye"] }, 100] }
                    }
                },
                //--MULTIPLICAR X 100
                "total_porcentaje_racimos_malos": {
                    "$cond": {
                        "if": { "$eq": ["$sum_cantidad_racimos", 0] },
                        "then": 0,
                        //"else": { "$divide": ["$sum_cantidad_racimos_malos", "$sum_cantidad_racimos"] }
                        "else": { "$multiply": [{ "$divide": ["$sum_cantidad_racimos_malos", "$sum_cantidad_racimos"] }, 100] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "total_porcentaje_FINAL": {
                    "$sum": ["$total_porcentaje_frutos", "$total_porcentaje_hojas", "$total_porcentaje_racimos_malos"]
                }
            }
        }


        //--SI total > 100 ==>> total = 100
        , {
            "$addFields": {
                "total_porcentaje_FINAL": {
                    "$cond": {
                        "if": { "$gte": ["$total_porcentaje_FINAL", 100] },
                        "then": 100,
                        "else": "$total_porcentaje_FINAL"
                    }
                }
            }
        }


        //--Sacar variables de _id
        //.....
        , {
            "$addFields": {
                "lote": "$_id.lote",
                "fecha": "$_id.fecha"
            }
        }

        , {
            "$project": {
                "_id": 0
            }
        }


        //--AGREGAR FECHA rgDate
        //.....
        , {
            "$addFields": {
                "rgDate": { "$toDate": "$fecha" }
            }
        }



        //---test
        // ,{
        //     $match:{
        //         "fecha":"2020-05-01"
        //     }
        // }



    ]
)