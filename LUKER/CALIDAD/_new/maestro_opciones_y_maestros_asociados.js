
//---
var maestro_principal = db.masters.aggregate(
    {
        $match: {
            //"name" : "Calidad Evalucion"
            "_id": ObjectId("5e5ecb986712a43d860b9357")
        }
    }

    , { $unwind: "$options" }

    , {
        $addFields: {
            "valor_maestro": "$options.name",
            "valor_maestro_tiene_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$options.master", ""] },
                    "then": "NO",
                    "else": "SI"
                }
            }
        }
    }
    
    , {
        $addFields: {
            "id_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$valor_maestro_tiene_maestro_asociado", "SI"] },
                    "then": { "$toObjectId": "$options.master"},
                    "else": ""
                }
            }
        }
    }
);

maestro_principal
