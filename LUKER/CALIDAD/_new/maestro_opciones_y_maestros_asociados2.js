
//---
var maestro_principal = db.masters.aggregate(
    {
        $match: {
            //"name" : "Calidad Evalucion"
            "_id": ObjectId("5e5ecb986712a43d860b9357")
        }
    }

    , { $unwind: "$options" }

    , {
        $addFields: {
            "valor_maestro": "$options.name",
            "valor_maestro_tiene_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$options.master", ""] },
                    "then": "NO",
                    "else": "SI"
                }
            }
        }
    }

    , {
        $addFields: {
            "id_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$valor_maestro_tiene_maestro_asociado", "SI"] },
                    "then": { "$toObjectId": "$options.master" },
                    "else": ""
                }
            }
        }
    }
);

//maestro_principal


maestro_principal.forEach(m => {
    if (m.valor_maestro_tiene_maestro_asociado == "SI") {
        // console.log("--")
        
        //---query
        var maestro_asociado = db.masters.aggregate(
            {
                $match: {
                    "_id": m.id_maestro_asociado
                }
            }

            , { $unwind: "$options" }

            , {
                $addFields: {
                    "valor_maestro": "$options.name",
                    "valor_maestro_tiene_maestro_asociado": {
                        "$cond": {
                            "if": { "$eq": ["$options.master", ""] },
                            "then": "NO",
                            "else": "SI"
                        }
                    }
                }
            }

            , {
                $addFields: {
                    "id_maestro_asociado": {
                        "$cond": {
                            "if": { "$eq": ["$valor_maestro_tiene_maestro_asociado", "SI"] },
                            "then": { "$toObjectId": "$options.master" },
                            "else": ""
                        }
                    }
                }
            }
        );
        // console.log(maestro_asociado);
        console.log("-----");
        console.log(m.valor_maestro);
        //console.log(maestro_asociado.valor_maestro);
        maestro_asociado.forEach(m2 => {
            console.log(m2.valor_maestro);
        });
        console.log("-----");
        
    }
})



