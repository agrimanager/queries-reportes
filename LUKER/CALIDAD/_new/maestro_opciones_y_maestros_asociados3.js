
//---

var results = [];


var maestro_principal = db.masters.aggregate(
    {
        $match: {
            //"name" : "Calidad Evalucion"
            "_id": ObjectId("5e5ecb986712a43d860b9357")
        }
    }

    , { $unwind: "$options" }

    , {
        $addFields: {
            "valor_maestro": "$options.name",
            "valor_maestro_tiene_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$options.master", ""] },
                    "then": "NO",
                    "else": "SI"
                }
            }
        }
    }

    , {
        $addFields: {
            "id_maestro_asociado": {
                "$cond": {
                    "if": { "$eq": ["$valor_maestro_tiene_maestro_asociado", "SI"] },
                    "then": { "$toObjectId": "$options.master" },
                    "else": ""
                }
            },
            "tipo_maestro": "M1-Principal"
        }
    }
);

//maestro_principal


maestro_principal.forEach(m => {

    console.log("-----");//--------------
    console.log(m.valor_maestro);
    results.push(m);//--ADD

    if (m.valor_maestro_tiene_maestro_asociado == "SI") {
        // console.log("--")

        //---query
        var maestro_asociado = db.masters.aggregate(
            {
                $match: {
                    "_id": m.id_maestro_asociado
                }
            }

            , { $unwind: "$options" }

            , {
                $addFields: {
                    "valor_maestro": "$options.name",
                    "valor_maestro_tiene_maestro_asociado": {
                        "$cond": {
                            "if": { "$eq": ["$options.master", ""] },
                            "then": "NO",
                            "else": "SI"
                        }
                    }
                }
            }

            , {
                $addFields: {
                    "id_maestro_asociado": {
                        "$cond": {
                            "if": { "$eq": ["$valor_maestro_tiene_maestro_asociado", "SI"] },
                            "then": { "$toObjectId": "$options.master" },
                            "else": ""
                        }
                    },
                    "tipo_maestro": "M2-Enlazado"
                }
            }
        );
        // console.log("-----");//--------------
        // console.log(m.valor_maestro);
        // results.push(m);//--ADD
        maestro_asociado.forEach(m2 => {
            console.log(m2.valor_maestro);
            results.push(m2);//--ADD
        });
        console.log("-----");//--------------

    }
});

results



