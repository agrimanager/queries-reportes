db.users.aggregate(
    [
        //----CALIDAD

        { "$project": { "_id": "$_id" } }

        //Evaluacion de Calidad (general)
        , {
            "$lookup": {
                "from": "form_evaluaciondecalidad",
                "as": "data_form_1",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [

                    //--condicionales base
                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    //--cartografia

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$Lote.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$project": {
                            //---ARREGLO
                            "Lote": 0,
                            "Point": 0,
                            
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0
                        }
                    }

                    // //------OBTENER VALOR DE MAESTRO ASOCIADO-----

                    , {
                        "$addFields": {
                            "Evaluacion_calidad": "$Evaluacion",
                            "Criterio_evaluacion": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": { "$eq": [{ "$substr": ["$$dataKV.k", 0, 11] }, "Evaluacion_"] },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , { "$unwind": "$Criterio_evaluacion" }
                    
                    


                    , {
                        "$project": {
                            //--Empleado
                            "Trabajador": "$Trabajador",
                            
                            //--Cartografia
                            "Finca": "$finca",
                            "Bloque": "$bloque",
                            "Lote": "$lote",
                            "Linea": "$Linea",
                            "Palma": "$Palma",
                            
                            //--Fechas
                            "Fecha": "$Fecha de Registro",
                            "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } },
                            "Fecha_anio": { "$year": { "date": "$Fecha de Registro" }},
                            "Fecha_mes": { "$month": { "date": "$Fecha de Registro" }},
                            

                            //--Evaluacion, Criterio y Cantidad
                            "Evaluacion_calidad": "$Evaluacion_calidad",
                            "Criterio_evaluacion": "$Criterio_evaluacion",
                            "Cantidad": "$Cantidad",

                            //--Otros
                            "Observacion": "$Observacion",
                            "supervisor": "$supervisor",
                            "rgDate": "$rgDate",
                            "capture": "$capture"
                            
                            
                        }
                    }

                ]

            }
        }
        
        
        
        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data_form_1"
                            ,[]
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



    ]
)