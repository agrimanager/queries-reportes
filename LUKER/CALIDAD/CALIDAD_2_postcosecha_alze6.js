//--alce
db.form_evaluacionpostcosechaalce.aggregate(
    [

        //--condicionales base
        {
            "$match": {
                "Lotes.path": { "$ne": "" }
            }
        },
        { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },
        //--fehca string (sin horas)
        {
            "$addFields": {
                "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } }
            }
        },

        { "$match": { "Empleados alce": { "$exists": true } } },
        { "$match": { "Empleados alce": { "$ne": "" } } },
        { "$addFields": { "empleados_filtro": { "$size": "$Empleados alce" } } },
        { "$match": { "empleados_filtro": { "$gt": 0 } } },


        //--cartografia

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lotes.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lotes.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lotes.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        }


        , {
            "$project": {
                //---ARREGLO
                "Lotes": 0,
                "Point": 0,

                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0,


                "uid": 0,
                "uDate": 0,
                "Formula": 0,
                "supervisor_u": 0
            }
        }

        //---------------------


        //---plantas_dif_censadas_x_lote
        , {
            "$addFields": {
                "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        }
        , {
            "$addFields": {
                "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta_aux",
                    "Fecha de Registro": "$Fecha de Registro"//--x fecha
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote",
                    "Fecha de Registro": "$_id.Fecha de Registro"//--x fecha
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        //---pasar a numero el maestro numerico
        , {
            "$addFields": {
                "tusa  en vagon": { "$toDouble": "$tusa  en vagon" },
                "tusa  zorrrillo": { "$toDouble": "$tusa  zorrrillo" },
                "Pepa sin recoger": { "$toDouble": "$Pepa sin recoger" },
                "Racimos  sin recoger": { "$toDouble": "$Racimos  sin recoger" },
                "Fruto sin recoger acopio": { "$toDouble": "$Fruto sin recoger acopio" }
            }
        }



        //---array con criterios
        , {
            "$addFields": {
                "criterios_y_valores": [
                    {
                        "criterio": "tusa  en vagon",
                        "valor": { "$toDouble": "$tusa  en vagon" }
                    },
                    {
                        "criterio": "tusa  zorrrillo",
                        "valor": { "$toDouble": "$tusa  zorrrillo" }
                    },
                    {
                        "criterio": "Pepa sin recoger",
                        "valor": { "$toDouble": "$Pepa sin recoger" }
                    },
                    {
                        "criterio": "Racimos  sin recoger",
                        "valor": { "$toDouble": "$Racimos  sin recoger" }
                    },
                    {
                        "criterio": "Fruto sin recoger acopio",
                        "valor": { "$toDouble": "$Fruto sin recoger acopio" }
                    }
                ]
            }
        }

        , { "$unwind": "$criterios_y_valores" }


        , {
            "$addFields": {
                "criterio": "$criterios_y_valores.criterio",
                "valor": "$criterios_y_valores.valor"
            }
        }

        , {
            "$project": {
                "criterios_y_valores": 0,
                "tusa  en vagon": 0,
                "tusa  zorrrillo": 0,
                "Pepa sin recoger": 0,
                "Racimos  sin recoger": 0,
                "Fruto sin recoger acopio": 0,
                "Criterios de evaluacion" : 0
            }
        }






        //-----empleados
        //  , { "$unwind": "$Empleados alce" }


        // , {
        //     "$addFields": {
        //         "nombre_Empleado": "$Empleados alce.name",
        //         // "labor_Empleado": "$Empleados alce.reference",
        //         "labor_Empleado": "Alzador",
        //         "valor_Empleado": "$Empleados alce.value"
        //     }
        // }
        // , {
        //     "$project": {
        //         "Empleados alce": 0,
        //         "Criterios de evaluacion": 0
        //     }
        // }




        //----criterio_Empleado
        //--ejemplos

        // ejemplo1:
        // 5e95189ca26a71470f866e9f
        // Pepa sin recojer = 20
        // valor de empleados = 10 , 10


        // ejemplo2:
        // 5e59d09c01fba818d7394a39
        // Pepa sin recojer = 2
        // valor de empleados = 0


    ]
)