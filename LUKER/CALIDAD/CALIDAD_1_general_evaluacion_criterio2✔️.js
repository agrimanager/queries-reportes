[
    
    {
        "$match": {
            "Lote.path": { "$ne": "" }
        }
    },
    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },
    
    
    
     {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$Lote.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }

    , {
        "$addFields": {
            "Evaluacion_calidad": "$Evaluacion",
            "Criterio_evaluacion": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$substr": ["$$dataKV.k", 0, 11] }, "Evaluacion_"] },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , { "$unwind": "$Criterio_evaluacion" }


    , {
        "$project": {
            "Lote": "$Lote",

            "Evaluacion_calidad": "$Evaluacion_calidad",
            "Criterio_evaluacion": "$Criterio_evaluacion",
            "Cantidad": "$Cantidad",

            "FechaR": "$Fecha de Registro",
            "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } },
            "Trabajador": "$Trabajador",

            "Finca": "$finca",
            "Bloque": "$bloque",
            "lote": "$lote",
            "Linea": "$Linea",
            "Palma": "$Palma",
            "Observacion": "$Observacion",


            "Point": "$Point",
            "uid": "$uid",
            "supervisor": "$supervisor",
            "rgDate": "$rgDate",
            "uDate": "$uDate",
            "Formula": "$Formula",
            "capture": "$capture"
        }
    }

    , {
        "$lookup": {
            "from": "form_configuracionevaluaciondecalidad",
            "as": "configuracion",
            "let": {
                "evaluacion": "$Evaluacion_calidad",
                "criterio": "$Criterio_evaluacion"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$evaluacion", "$Evaluacion"] },
                                { "$eq": ["$$criterio", "$Criterio"] }

                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$configuracion",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "Afecta calificacion": "$configuracion.Afecta calificacion"
        }
    }
    , {
        "$project": {
            "configuracion": 0
        }
    }




]