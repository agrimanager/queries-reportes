db.users.aggregate(
    [


        { "$project": { "_id": "$_id" } }

        //--alce
        //form_evaluacionpostcosechaalce
        , {
            "$lookup": {
                "from": "form_evaluacionpostcosechaalce",
                "as": "data1",
                "let": { "id_usr": "$_id" },
                //---------------QUERY----------
                "pipeline": [
                    //------CONDICION INICIAL
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$eq": ["$$id_usr", "$uid"]
                    //         }
                    //     }
                    // },


                    //.......

                    //--condicionales base
                    {
                        "$match": {
                            "Lotes.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },
                    //--fehca string (sin horas)
                    {
                        "$addFields": {
                            "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } }
                        }
                    },

                    { "$match": { "Empleados alce": { "$exists": true } } },
                    { "$match": { "Empleados alce": { "$ne": "" } } },
                    { "$addFields": { "empleados_filtro": { "$size": "$Empleados alce" } } },
                    { "$match": { "empleados_filtro": { "$gt": 0 } } },


                    //--cartografia

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lotes.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$Lotes.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$Lotes.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    }


                    , {
                        "$project": {
                            //---ARREGLO
                            "Lotes": 0,
                            "Point": 0,

                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0,


                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "supervisor_u": 0
                        }
                    }

                    //---------------------


                    //---plantas_dif_censadas_x_lote
                    , {
                        "$addFields": {
                            "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
                        }
                    }
                    , {
                        "$addFields": {
                            "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "planta": "$planta_aux",
                                "Fecha de Registro": "$Fecha de Registro"//--x fecha
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$_id.lote",
                                "Fecha de Registro": "$_id.Fecha de Registro"//--x fecha
                            },
                            "plantas_dif_censadas_x_lote": { "$sum": 1 },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                    }
                                ]
                            }
                        }
                    }


                    //---pasar a numero el maestro numerico
                    , {
                        "$addFields": {
                            "tusa  en vagon": { "$toDouble": "$tusa  en vagon" },
                            "tusa  zorrrillo": { "$toDouble": "$tusa  zorrrillo" },
                            "Pepa sin recoger": { "$toDouble": "$Pepa sin recoger" },
                            "Racimos  sin recoger": { "$toDouble": "$Racimos  sin recoger" },
                            "Fruto sin recoger acopio": { "$toDouble": "$Fruto sin recoger acopio" }
                        }
                    }



                    //---array con criterios
                    , {
                        "$addFields": {
                            "criterios_y_valores": [
                                {
                                    "criterio": "tusa  en vagon",
                                    "valor": { "$toDouble": "$tusa  en vagon" }
                                },
                                {
                                    "criterio": "tusa  zorrrillo",
                                    "valor": { "$toDouble": "$tusa  zorrrillo" }
                                },
                                {
                                    "criterio": "Pepa sin recoger",
                                    "valor": { "$toDouble": "$Pepa sin recoger" }
                                },
                                {
                                    "criterio": "Racimos  sin recoger",
                                    "valor": { "$toDouble": "$Racimos  sin recoger" }
                                },
                                {
                                    "criterio": "Fruto sin recoger acopio",
                                    "valor": { "$toDouble": "$Fruto sin recoger acopio" }
                                }
                            ]
                        }
                    }

                    , { "$unwind": "$criterios_y_valores" }


                    , {
                        "$addFields": {
                            "criterio": "$criterios_y_valores.criterio",
                            "valor": "$criterios_y_valores.valor"
                        }
                    }

                    , {
                        "$project": {
                            "criterios_y_valores": 0,
                            "tusa  en vagon": 0,
                            "tusa  zorrrillo": 0,
                            "Pepa sin recoger": 0,
                            "Racimos  sin recoger": 0,
                            "Fruto sin recoger acopio": 0,
                            "Criterios de evaluacion": 0
                        }
                    }


                    //---valores a mostrar
                    , {
                        "$project": {
                            "fecha": "$Fecha de Registro",
                            "lote": "$lote",

                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
                            "evaluacion": "palma cosecha alce",//cte
                            "criterio": "$criterio",
                            "cantidad": "$valor"
                        }
                    }




                ]

            }
        }

        //--corte y encallado
        //form_formularioevaluacionpostcosecha
        , {
            "$lookup": {
                "from": "form_formularioevaluacionpostcosecha",
                "as": "data2",
                "let": { "id_usr": "$_id" },
                //---------------QUERY----------
                "pipeline": [
                    //------CONDICION INICIAL
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$eq": ["$$id_usr", "$uid"]
                    //         }
                    //     }
                    // },



                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fech de Registro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },
                    //--fehca string (sin horas)
                    {
                        "$addFields": {
                            "Fech de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fech de Registro" } }
                        }
                    },

                    { "$match": { "Empleado": { "$exists": true } } },
                    { "$match": { "Empleado": { "$ne": "" } } },
                    { "$addFields": { "empleados_filtro": { "$size": "$Empleado" } } },
                    { "$match": { "empleados_filtro": { "$gt": 0 } } },


                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$Lote.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$project": {
                            "Lote": 0,
                            "Point": 0,

                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0,


                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "supervisor_u": 0
                        }
                    }

                    , {
                        "$addFields": {
                            "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
                        }
                    }
                    , {
                        "$addFields": {
                            "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "planta": "$planta_aux",
                                "Fech de Registro": "$Fech de Registro"//--x fecha
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$_id.lote",
                                "Fech de Registro": "$_id.Fech de Registro"//--x fecha
                            },
                            "plantas_dif_censadas_x_lote": { "$sum": 1 },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                    }
                                ]
                            }
                        }
                    }


                    , { "$unwind": "$Empleado" }


                    , {
                        "$addFields": {
                            "nombre_Empleado": "$Empleado.name",
                            "labor_Empleado": "$Empleado.reference",
                            "valor_Empleado": "$Empleado.value"
                        }
                    }
                    , {
                        "$project": {
                            "Empleado": 0
                        }
                    }


                    , {
                        "$match": {
                            "$or": [
                                {
                                    "Criterio Cortador": { "$not": { "$ne": "" } },
                                    "Criterio Encallador": { "$not": { "$ne": "" } }
                                },
                                { "labor_Empleado": { "$not": { "$eq": "" } } }
                            ]
                        }
                    }

                    , {
                        "$addFields": {
                            "criterio_Empleado": {
                                "$cond": {
                                    "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                                    "then": "$Criterio Cortador",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                            "then": "$Criterio Encallador",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                                    "then": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$Criterio Encallador", ""] },
                                                            "then": "$Criterio Cortador",
                                                            "else": "$Criterio Encallador"
                                                        }
                                                    },
                                                    "else": "SIN CARGO"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "evaluacion": {
                                "$cond": {
                                    "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                                    "then": "palma cosecha corte",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                            "then": "palma cosecha encallado",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                                    "then": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$Criterio Encallador", ""] },
                                                            "then": "palma cosecha corte",
                                                            "else": "palma cosecha encallado"
                                                        }
                                                    },
                                                    "else": "SIN EVALUACION"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    //---valores a mostrar
                    , {
                        "$project": {
                            "fecha": "$Fech de Registro",
                            "lote": "$lote",

                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
                            "evaluacion": "$evaluacion",
                            "criterio": "$criterio_Empleado",
                            "cantidad": "$valor_Empleado"
                        }
                    }


                ]

            }
        }


        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data1"
                            ,"$data2"
                            // , []
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

    ]
)