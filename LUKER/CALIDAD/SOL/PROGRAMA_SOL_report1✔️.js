[


    { "$addFields": { "anio_filtro": { "$year": "$Fecha de inspeccion" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },
    {
        "$addFields": {
            "Fecha de inspeccion": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de inspeccion" } }
        }
    },



    { "$addFields": { "oidfarm": { "$toObjectId": "$Point.farm" } } },
    {
        "$lookup": {
            "from": "farms",
            "localField": "oidfarm",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },

    {
        "$project": {
            "Criterios Ambientales": 0,
            "uid": 0,
            "anio_filtro": 0,
            "oidfarm": 0,
            "supervisor_u": 0,
            "supervisor": 0,

            "Point": 0,
            "uDate": 0
        }
    }

    , {
        "$addFields": {
            "criterios_y_valores": [
                {
                    "criterio": "Ahorro y uso eficiente del agua",
                    "valor": { "$toDouble": "$Ahorro y uso eficiente del agua" }
                },
                {
                    "criterio": "Cuidado y mantto de arboles sembrados y zonas verdes",
                    "valor": { "$toDouble": "$Cuidado y mantto de arboles sembrados y zonas verdes" }
                },
                {
                    "criterio": "Orden y Aseo en vaqueria y reas asignadas",
                    "valor": { "$toDouble": "$Orden y Aseo en vaqueria y reas asignadas" }
                },
                {
                    "criterio": "Separacin en la fuente",
                    "valor": { "$toDouble": "$Separacin en la fuente" }
                },
                {
                    "criterio": "Manejo seguro de RESPEL  envases empaques embalajes de agroquimicos y otros",
                    "valor": { "$toDouble": "$Manejo seguro de RESPEL  envases empaques embalajes de agroquimicos y otros" }
                }
            ]
        }
    }


    , {
        "$addFields": {
            "criterios_valores_filter": {
                "$filter": {
                    "input": "$criterios_y_valores.valor",
                    "as": "item",
                    "cond": { "$ne": ["$$item", 0] }
                }
            }
        }
    }


    , {
        "$addFields": {
            "promedio_criterios": { "$avg": "$criterios_valores_filter" }
        }
    }

    , {
        "$project": {
            "criterios_y_valores": 0,
            "criterios_valores_filter": 0
        }
    }



]