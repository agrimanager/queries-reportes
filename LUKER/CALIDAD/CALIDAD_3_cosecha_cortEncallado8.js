//--corte y encallado
db.form_formularioevaluacionpostcosecha.aggregate(
    [

        {
            "$match": {
                "Lote.path": { "$ne": "" }
            }
        },
        { "$addFields": { "anio_filtro": { "$year": "$Fech de Registro" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },
        //--fehca string (sin horas)
        {
            "$addFields": {
                "Fech de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fech de Registro" } } 
            }
        },

        { "$match": { "Empleado": { "$exists": true } } },
        { "$match": { "Empleado": { "$ne": "" } } },
        { "$addFields": { "empleados_filtro": { "$size": "$Empleado" } } },
        { "$match": { "empleados_filtro": { "$gt": 0 } } },
        

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "Lote": 0,
                "Point": 0,

                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0,


                "uid": 0,
                "uDate": 0,
                "Formula": 0,
                "supervisor_u" :0
            }
        }

        , {
            "$addFields": {
                "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        }
        ,{
            "$addFields": {
                "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta_aux",
                    "Fech de Registro":"$Fech de Registro"//--x fecha
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote",
                    "Fech de Registro":"$_id.Fech de Registro"//--x fecha
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        , { "$unwind": "$Empleado" }


        , {
            "$addFields": {
                "nombre_Empleado": "$Empleado.name",
                "labor_Empleado": "$Empleado.reference",
                "valor_Empleado": "$Empleado.value"
            }
        }
        , {
            "$project": {
                "Empleado": 0
            }
        }


        , {
            "$match": {
                "$or": [
                    {
                        "Criterio Cortador": { "$not": { "$ne": "" } },
                        "Criterio Encallador": { "$not": { "$ne": "" } }
                    },
                    { "labor_Empleado": { "$not": { "$eq": "" } } }
                ]
            }
        }

        , {
            "$addFields": {
                "criterio_Empleado": {
                    "$cond": {
                        "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                        "then": "$Criterio Cortador",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                "then": "$Criterio Encallador",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                        "then": {
                                            "$cond": {
                                                "if": { "$eq": ["$Criterio Encallador", ""] },
                                                "then": "$Criterio Cortador",
                                                "else": "$Criterio Encallador"
                                            }
                                        },
                                        "else": "SIN CARGO"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "evaluacion": {
                    "$cond": {
                        "if": { "$eq": ["$labor_Empleado", "Cortero"] },
                        "then": "palma cosecha corte",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$labor_Empleado", "Encallador"] },
                                "then": "palma cosecha encallado",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$labor_Empleado", "Corte/Encallado"] },
                                        "then": {
                                            "$cond": {
                                                "if": { "$eq": ["$Criterio Encallador", ""] },
                                                "then": "palma cosecha corte",
                                                "else": "palma cosecha encallado"
                                            }
                                        },
                                        "else": "SIN EVALUACION"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    ]
)
