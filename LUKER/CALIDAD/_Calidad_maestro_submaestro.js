db.form_evaluaciondecalidad.aggregate(

    //--Campo y Maestro principal
    //campo =Evaluacion ---> maestro =Calidad Evalucion
    //--Maestros enlazados 
    //.."Evaluacion" : "Cacao  Cosecha",
    //.."Evaluacion_Cacao  Cosecha" : "MAZORCA BIEN COSECHADA",
    //(key)
    //---->>Evaluacion_(valor maestro pricipal)


    {
        "$addFields":{
            
            //evaluacion (maestro)
            "valor_maestro_principal": "$Evaluacion",
            
            //criterio evaluacion (submaestro)
            "valor_maestro_enlazado": "Evaluacion_" + "$Evaluacion"
        }
    }
    
    ,{
        $project:{
            "valor_maestro_principal":1,
            "valor_maestro_enlazado":1,
            
        }
    }

)
