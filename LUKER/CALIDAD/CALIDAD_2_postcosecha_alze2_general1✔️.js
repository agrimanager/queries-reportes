[


    {
        "$match": {
            "Lotes.path": { "$ne": "" }
        }
    },
    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },

    { "$match": { "Empleados alce": { "$exists": true } } },
    { "$match": { "Empleados alce": { "$ne": "" } } },
    { "$addFields": { "empleados_filtro": { "$size": "$Empleados alce" } } },
    { "$match": { "empleados_filtro": { "$gt": 0 } } },




    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lotes.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$Lotes.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Lotes.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "Lotes": 0,
            "Point": 0,

            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }

    , {
        "$addFields": {
            "tusa  en vagon": { "$toDouble": "$tusa  en vagon" },
            "tusa  zorrrillo": { "$toDouble": "$tusa  zorrrillo" },
            "Pepa sin recoger": { "$toDouble": "$Pepa sin recoger" },
            "Racimos  sin recoger": { "$toDouble": "$Racimos  sin recoger" },
            "Fruto sin recoger acopio": { "$toDouble": "$Fruto sin recoger acopio" }
        }
    }

    , {
        "$addFields": {
            "num_empleados": "$empleados_filtro",
            "rgDate2": "$rgDate"
        }
    }


    , {
        "$project": {
            "Formula": 0,
            "Empleados alce": 0,

            "Criterios de evaluacion": 0,
            "uid": 0,
            "uDate": 0,
            "rgDate": 0,
            "anio_filtro": 0,

            "empleados_filtro": 0
        }
    }



]