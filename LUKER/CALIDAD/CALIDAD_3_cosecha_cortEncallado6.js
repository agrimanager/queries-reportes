db.form_formularioevaluacionpostcosecha.aggregate(
    [



        //--condicionales base
        {
            "$match": {
                "Lote.path": { "$ne": "" }
            }
        },
        { "$addFields": { "anio_filtro": { "$year": "$Fech de Registro" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        { "$match": { "Empleado": { "$exists": true } } },
        { "$match": { "Empleado": { "$ne": "" } } },
        { "$addFields": { "empleados_filtro": { "$size": "$Empleado" } } },
        { "$match": { "empleados_filtro": { "$gt": 0 } } },


        //--cartografia

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                //---ARREGLO
                "Lote": 0,
                "Point": 0,

                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0
            }
        }

        //----arboles DIFERENTES censados x lote
        , {
            "$addFields": {
                "linea_aux": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta_aux": { "$concat": ["$linea_aux", "-", { "$toString": "$Palma" }] }
            }
        }


        // , {
        //     "$addFields": {
        //         "num_planta": { "$split": ["$planta_aux", "-"] }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "num_planta": {
        //             "$sum": [
        //                 {
        //                     "$multiply": [
        //                         { "$toInt": { "$arrayElemAt": ["$num_planta", 2] } },
        //                         10000
        //                     ]
        //                 },
        //                 { "$toInt": { "$arrayElemAt": ["$num_planta", 3] } }
        //             ]
        //         }
        //     }
        // }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta_aux"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }







        // //Empleados
        // , { "$unwind": "$Empleado" }


        // , {
        //     "$addFields": {
        //         "nombre_Empleado": "$Empleado.name",
        //         "labor_Empleado": "$Empleado.reference",
        //         "valor_Empleado": "$Empleado.value"
        //     }
        // }
        // , {
        //     "$project": {
        //         "Empleado": 0
        //     }
        // }









        // //---Como obtener cantidad ?????


        // // //==>proyeccion de informacion
        // // , {
        // //     "$project": {
        // //         //--Empleado
        // //         "Trabajador": "$Empleados alce",

        // //         //--Cartografia
        // //         "Finca": "$finca",
        // //         "Bloque": "$bloque",
        // //         "Lote": "$lote",
        // //         "Linea": "$Linea",
        // //         "Palma": "$Palma",

        // //         //--Fechas
        // //         "Fecha": "$Fecha de Registro",
        // //         "Fecha de Registro": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Registro" } },
        // //         "Fecha_anio": { "$year": { "date": "$Fecha de Registro" } },
        // //         "Fecha_mes": { "$month": { "date": "$Fecha de Registro" } },


        // //         //--Evaluacion, Criterio y Cantidad
        // //         "Evaluacion_calidad": "post cosecha alce", ///------------valor cte
        // //         "Criterio_evaluacion": "Criterio_evaluacion",//---criterio de maestro numerico
        // //         "Cantidad": "123", ///------------??cual valor cantidad de empleado o maestro numerico

        // //         //--Otros
        // //         "Observacion": "$Observacion",
        // //         "supervisor": "$supervisor",
        // //         "rgDate": "$rgDate",
        // //         "capture": "$capture"


        // //     }
        // // }


        // // //----test//---arreglo
        // // ,{
        // //     $match: {
        // //         "Criterio Cortador": { $ne: "" },
        // //         "Criterio Encallador": { $ne: "" }
        // //     }
        // // }


        // // //----test//---arreglo
        // // ,{
        // //     $match: {
        // //         "labor_Empleado" : ""
        // //     }
        // // }



        // // //----test//---arreglo
        // // , {
        // //     $match: {
        // //         $and: [
        // //             {
        // //                 $and: [
        // //                     {"Criterio Cortador": { $ne: "" }},
        // //                     {"Criterio Encallador": { $ne: "" }},
        // //                 ]
        // //             },
        // //             {"labor_Empleado":{$eq: ""}}
        // //         ]
        // //     }
        // // }


        // // //----test//---arreglo
        // // , {
        // //     $match: {

        // //         // "Criterio Cortador":{$not:{ $ne: "" }},
        // //         // "Criterio Encallador":{$not:{ $ne: "" }},
        // //         // "labor_Empleado":{$not:{ $ne: "" }},
        // //         $and: [
        // //             {
        // //                 "Criterio Cortador": { $not: { $eq: "" } },
        // //                 "Criterio Encallador": { $not: { $eq: "" } },
        // //             },
        // //             {"labor_Empleado": { $not: { $ne: "" } }},
        // //             // { "labor_Empleado": { $eq: "" } }
        // //         ]
        // //     }
        // // }


        // //----test//---arreglo
        // , {
        //     $match: {
        //         // $and: [
        //         //     {
        //         //         "Criterio Cortador": { $not: { $eq: "" } },
        //         //         "Criterio Encallador": { $not: { $eq: "" } },
        //         //     },
        //         //     {"labor_Empleado": { $not: { $ne: "" } }},
        //         // ]

        //         $or: [
        //             {
        //                 "Criterio Cortador": { $not: { $ne: "" } },
        //                 "Criterio Encallador": { $not: { $ne: "" } },
        //             },
        //             {"labor_Empleado": { $not: { $eq: "" } }},
        //         ]
        //     }
        // }




        // // //----cruzar criterio con cargo
        // // ,{
        // //     $match:{
        // //         //"labor_Empleado" : "Corte/Encallado",
        // //         "Linea" : 0,
        // //         // "Palma" : 0,
        // //     }
        // // }






    ]
)
