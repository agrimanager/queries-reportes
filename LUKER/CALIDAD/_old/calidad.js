db.form_evaluaciondecalidad.aggregate(
    [
        // {
        //     "$addFields": {
        //         "__Evaluacion": "$Evaluacion",
        //         "__Criterio": {
        //             "$map": {
        //                 "input": { "$objectToArray": "$$ROOT" },
        //                 "as": "dataKV",
        //                 //"in": "$$dataKV.v"
        //                 "in": {
        //                     "$cond": {
        //                         "if": {"$eq": ["$$dataKV.k", "Palma"]},
        //                         "then": "$$dataKV.v",
        //                         "else": null
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        {
            "$addFields": {
                "__Evaluacion": "$Evaluacion",
                //"__Evaluacion2": { "$substr": [ "Evaluacion_Palma xxx", 0, 11 ] },
                "__Criterio": {
                    "$filter": {
                        "input":{
                                "$map": {
                                    "input": { "$objectToArray": "$$ROOT" },
                                    "as": "dataKV",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$substr": [ "$$dataKV.k", 0, 11 ] }, "Evaluacion_"] },
                                            "then": "$$dataKV.v",
                                            "else": ""
                                        }
                                    }
                                }
                            },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }


        // {
        //     "$addFields": {
        //         "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": {
        //             "$concatArrays": [
        //                 "$split_path_oid",
        //                 "$features_oid"
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$lookup": {
        //         "from": "cartography",
        //         "localField": "split_path_oid",
        //         "foreignField": "_id",
        //         "as": "objetos_del_cultivo"
        //     }
        // },

        // {
        //     "$addFields": {
        //         "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
        //         "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
        //         "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        //     }
        // },

        // {
        //     "$addFields": {
        //         "Bloque": "$Bloque.properties.name",
        //         "lote": "$lote.properties.name"
        //     }
        // },

        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "Finca._id",
        //         "foreignField": "_id",
        //         "as": "Finca"
        //     }
        // },

        // {
        //     "$addFields": {
        //         "Finca": "$Finca.name"
        //     }
        // },
        // { "$unwind": "$Finca" }
        // , {
        //     "$project": {
        //         "split_path": 0,
        //         "split_path_oid": 0,
        //         "objetos_del_cultivo": 0,
        //         "features_oid": 0
        //     }
        // }


        // // ,{
        // //     "$group":{
        // //         "_id":{
        // //             "lote":"$lote"
        // //         },
        // //         "data": {
        // //             "$push": "$$ROOT"
        // //         },
        // //         "data_obj_array": {
        // //             "$push": { $objectToArray: "$$ROOT" }
        // //         }

        // //     }
        // // }



        // , {
        //     "$group": {
        //         "_id": {
        //             "data": "$$ROOT"
        //         },
        //         "data_obj_array": {
        //             "$push": { $objectToArray: "$$ROOT" }
        //         }

        //     }
        // }
        // , { "$unwind": "$data_obj_array" }


        // ,{
        //     "$addFields":{
        //         "Evaluacion":"$_id.data.Evaluacion",
        //         //"criterio_evaluacion":"xxxxx"
        //         "criterio_evaluacion":{
        //                     "$map": {
        //                         "input": { "$objectToArray": "$employees" },
        //                         "as": "suppliesKV",
        //                         "in": "$$suppliesKV.v"
        //                     }
        //                 }
        //     }
        // }




    ]
)
