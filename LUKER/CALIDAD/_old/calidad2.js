[

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" }
        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        , {
            "$addFields": {
                "Evaluacion_calidad": "$Evaluacion",
                "Criterio_evaluacion": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$substr": ["$$dataKV.k", 0, 11] }, "Evaluacion_"] },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

        , { "$unwind": "$Criterio_evaluacion" }


        , {
            "$project": {
                "Lote": "$Lote",

                "Evaluacion_calidad": "$Evaluacion_calidad",
                "Criterio_evaluacion": "$Criterio_evaluacion",
                "Cantidad": "$Cantidad",

                "Fecha de Registro": "$Fecha de Registro",
                "Trabajador": "$Trabajador",

                "Finca": "$FInca",
                "Bloque": "$Bloque",
                "lote": "$lote",
                "Linea": "$Linea",
                "Palma": "$Palma",


                "Point": "$Point",
                "uid": "$uid",
                "supervisor": "$supervisor",
                "rgDate": "$rgDate",
                "uDate": "$uDate",
                "Formula": "$Formula",
                "capture": "$capture"
            }
        }

        , {
            "$lookup": {
                "from": "form_configuracionevaluaciondecalidad",
                "as": "configuracion",
                "let": {
                    "evaluacion": "$Evaluacion_calidad",
                    "criterio": "$Criterio_evaluacion"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$evaluacion", "$Evaluacion"] },
                                    { "$eq": ["$$criterio", "$Criterio"] }

                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$configuracion",
                "preserveNullAndEmptyArrays": true
            }
        }
        
        
        ,{
            "$addFields": {
                "Afecta calificacion": "$configuracion.Afecta calificacion"
            }
        }
        , {
            "$project": {
                "configuracion": 0
            }
        }


    ]