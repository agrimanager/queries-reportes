
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lotes.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lotes.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        { "$unwind": "$Finca" },


        {
            "$addFields": {
                "_id_str_farm": { "$toString": "$Finca._id" }
            }
        }

        , {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        }

        , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }

        , {
            "$addFields": {
                "cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                        "then": "PALMA",
                        "else": "CACAO"
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }
        
        ,{ "$unwind": "$Empleados alce" }
        
        
        , {
            "$addFields": {
                "Empleados alce": "$Empleados alce.name"
            }
        }
        
        
    ]
