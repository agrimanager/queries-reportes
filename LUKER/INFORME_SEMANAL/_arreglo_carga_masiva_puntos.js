
//---form_configuracionactividadesxcecospresupuesto
var cursor = db.form_configuracionactividadesxcecospresupuesto.aggregate(
    {
        $match: {
            "Actividad Agrimanager": { $regex: `XXXXX` }
        }
    }
    , { "$addFields": { "Actividad_Agrimanager": "$Actividad Agrimanager" } }
);



cursor.forEach(i => {

    var item_actual = i.Actividad_Agrimanager
    var item_nuevo = item_actual.replace('XXXXX', '.')
    // console.log(item_nuevo)

    db.form_configuracionactividadesxcecospresupuesto.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Actividad Agrimanager": item_nuevo
            }
        }
    )


});





//---form_presupuestoactividades
var cursor = db.form_presupuestoactividades.aggregate(
    {
        $match: {
            "Actividad Agrimanager": { $regex: `XXXXX` }
        }
    }
    , { "$addFields": { "Actividad_Agrimanager": "$Actividad Agrimanager" } }
);



cursor.forEach(i => {

    var item_actual = i.Actividad_Agrimanager
    var item_nuevo = item_actual.replace('XXXXX', '.')
    // console.log(item_nuevo)

    db.form_presupuestoactividades.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Actividad Agrimanager": item_nuevo
            }
        }
    )
});