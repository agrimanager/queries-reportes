//1_Labores_X_Empleado_base
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //------------------------------------------------------------------


        //---filtros de anio y plantacion
        {
            "$addFields": {
                "busqueda_anio": { "$year": "$Busqueda inicio" },
                "busqueda_plantacion": "Villanueva" //--plantacion
            }
        },

        //*********************PASO#1 (obtener PPTO)

        //---cruzar con form_presupuestoactividades (obtener presupuesto ANIO)
        {
            "$lookup": {
                "from": "form_presupuestoactividades",
                "as": "form_presupuestoactividades_aux",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin",
                    "busqueda_anio": "$busqueda_anio",
                    "busqueda_plantacion": "$busqueda_plantacion"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Plantacion", "$$busqueda_plantacion"] },
                                    { "$eq": ["$anio", "$$busqueda_anio"] }
                                ]
                            }
                        }
                    }

                    , {
                        "$project": {
                            // "Actividad Agrimanager": "$Actividad Agrimanager",
                            "actividad": "$Actividad Agrimanager",
                            // "Plantacion": "$Plantacion",
                            "anio": "$anio",
                            "mes": "$mes",
                            "cantidad presupuesto": "$cantidad presupuesto"

                            , "Busqueda inicio": "$$filtro_fecha_inicio"
                            , "Busqueda fin": "$$filtro_fecha_fin"
                        }
                    }

                ]
            }
        }

        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$form_presupuestoactividades_aux"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //*********************PASO#2 (OBTENER GRUPO LABOR DE ACTIVIDADES)

        //---cruzar con form_configuracionactividadesxcecospresupuesto (obtener grupo labor)
        , {
            "$lookup": {
                "from": "form_configuracionactividadesxcecospresupuesto",
                "as": "config_grupo_labor",
                "let": {
                    // "labor": "$Actividad Agrimanager"
                    "labor": "$actividad"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": { "$eq": ["$Actividad Agrimanager", "$$labor"] }
                        }
                    },
                    { "$limit": 1 }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$config_grupo_labor",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                // "Ceco presupuesto actividad": { "$ifNull": ["$config_grupo_labor.Ceco Presupuesto", ""] }
                "grupo_labor": { "$ifNull": ["$config_grupo_labor.Ceco Presupuesto", "DESACTUALIZADO"] }

            }
        },

        {
            "$project": {
                "config_grupo_labor": 0

                , "_id": 0
            }
        }





        //*********************PASO#3 (obtener PROGRAMACION - EJECUCION)

        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {
                    "actividad": "$actividad",

                    "anio": "$anio",
                    "mes": "$mes",

                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    
                    //--filtro fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": { "$max": "$when.start" }
                                                    }
                                                }
                                            }
                                            ,
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$filtro_fecha_inicio"
                                                    }
                                                }
                                            }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": { "$max": "$when.start" }
                                                    }
                                                }
                                            }
                                            ,
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$filtro_fecha_fin"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    
                    //--filtro finca //--plantacion
                    { "$match": { "farm": { "$ne": "" } } }
                    ,{"$addFields": {"_id_str_farm": { "$toString": "$farm" }}}
                    , { "$match": { "_id_str_farm": { "$in": ["5d26491264f5b87ffc809eba"] } } }


                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },
                    { "$addFields": { "Labor": "$activity.name" } },


                    {
                        "$addFields": {
                            "anio_labor": { "$year": { "$max": "$when.start" } },
                            "mes_labor": { "$month": { "$max": "$when.start" } }
                        }
                    },



                    //---CONDICIONALES
                    {
                        "$match": {
                            "$expr": {
                                "$and":
                                    [
                                        { "$eq": ["$Labor", "$$actividad"] },
                                        { "$eq": ["$anio_labor", "$$anio"] },
                                        { "$eq": ["$mes_labor", "$$mes"] }
                                    ]
                            }
                        }
                    }

                    //---Unidad de medida
                    , {
                        "$addFields": {
                            "Unidad labor": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                                        "then": "Bloque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                                        "then": "Lotes"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                                        "then": "Linea"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                                        "then": "Árboles"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                                        "then": "Centro frutero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                                        "then": "Poligono de muestreo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                                        "then": "Valvula"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                                        "then": "Drenaje"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                                        "then": "Aspersors"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                                        "then": "Red de riego uno"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                                        "then": "Red de riego dos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                                        "then": "Red de riego tres"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                                        "then": "Trampa"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                                        "then": "Vías"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                                        "then": "Bosque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                                        "then": "Sensor"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                                        "then": "Cable vía"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                                        "then": "Edificio"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                                        "then": "Cuerpo de agua"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                                        "then": "Poligonos adicionales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                                        "then": "Unidades de cultivo (Ejemplo: Árboles)"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                                        "then": "Jornales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                                        "then": "Cantidades"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                                        "then": "Metros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "km"] },
                                        "then": "Kilometros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                                        "then": "Centimetros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                                        "then": "Millas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                                        "then": "Yardas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                                        "then": "Pies"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                                        "then": "Gramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                                        "then": "Miligramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                                        "then": "Toneladas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                                        "then": "Onzas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                                        "then": "Libras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                                        "then": "Litros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                                        "then": "Galones"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                                        "then": "Bultos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                                        "then": "Bolsas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                                        "then": "sacks"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                                        "then": "Yemas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                                        "then": "Factura"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                                        "then": "Flete"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                                        "then": "Picadero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                                        "then": "Hora"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                                        "then": "Cuadras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                                        "then": "Canecas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                                        "then": "Racimos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "metro-line"] },
                                        "then": "Metro Lineal"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "square-meter"] },
                                        "then": "Metro Cuadrado"
                                    }],
                                    "default": "--------"
                                }
                            }
                        }
                    }



                    //--Agrupacion
                    , {
                        "$group": {
                            "_id": {
                                "labor": "$Labor",
                                "unidad_medida": "$Unidad labor",
                                "anio": "$anio_labor",
                                "mes": "$mes_labor"
                            }
                            ,"cantidad ejecucion": {"$sum": "$productivityAchieved"}
                            // , "data": { "$push": "$_id" }
                        }
                    }


                    // ,{
                    //     "$addFields": {
                    //         "unidad_medida": "$_id.unidad_medida"
                    //     }
                    // }



                    // , {
                    //     "$project": {
                    //         "_id": 0,
                    //         "cantidad ejecucion": 1,
                    //         "unidad_medida":1

                    //         ,"data":1
                    //     }
                    // }




                ]

            }
        }

        // , {
        //     "$unwind": {
        //         "path": "$data_labores",
        //         "preserveNullAndEmptyArrays": true
        //     }p
        // }


        // , {
        //     "$addFields": {
        //         // "Ceco presupuesto actividad": { "$ifNull": ["$config_grupo_labor.Ceco Presupuesto", ""] }
        //         "cantidad ejecucion": { "$ifNull": ["$data_labores.cantidad ejecucion", 0] }

        //     }
        // }

        // , {
        //     "$project": {
        //         "data_labores": 0

        //         // , "_id": 0
        //     }
        // }






    ]
)