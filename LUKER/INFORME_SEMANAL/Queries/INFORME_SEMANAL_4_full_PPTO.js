//1_Labores_X_Empleado_base
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //------------------------------------------------------------------


        //---filtros de anio y plantacion
        {
            "$addFields": {
                "busqueda_anio": { "$year": "$Busqueda inicio" },
                "busqueda_plantacion": "Villanueva" //--plantacion
            }
        },

        //*********************PASO#1 (obtener PPTO)

        //---cruzar con form_presupuestoactividades (obtener presupuesto ANIO)
        {
            "$lookup": {
                "from": "form_presupuestoactividades",
                "as": "form_presupuestoactividades_aux",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin",
                    "busqueda_anio": "$busqueda_anio",
                    "busqueda_plantacion": "$busqueda_plantacion"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Plantacion", "$$busqueda_plantacion"] },
                                    { "$eq": ["$anio", "$$busqueda_anio"] }
                                ]
                            }
                        }
                    }

                    , {
                        "$project": {
                            "Actividad Agrimanager": "$Actividad Agrimanager",
                            "Plantacion": "$Plantacion",
                            "anio": "$anio",
                            "mes": "$mes",
                            "cantidad presupuesto": "$cantidad presupuesto"

                            , "Busqueda inicio": "$$filtro_fecha_inicio"
                            , "Busqueda fin": "$$filtro_fecha_fin"
                        }
                    }

                ]
            }
        }

        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$form_presupuestoactividades_aux"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //*********************PASO#2 (OBTENER GRUPO LABOR DE ACTIVIDADES)

        //---cruzar con form_configuracionactividadesxcecospresupuesto (obtener grupo labor)
        , {
            "$lookup": {
                "from": "form_configuracionactividadesxcecospresupuesto",
                "as": "config_grupo_labor",
                "let": {
                    "labor": "$Actividad Agrimanager"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": { "$eq": ["$Actividad Agrimanager", "$$labor"] }
                        }
                    },
                    { "$limit": 1 }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$config_grupo_labor",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "Ceco presupuesto actividad": { "$ifNull": ["$config_grupo_labor.Ceco Presupuesto", ""] }
            }
        },

        {
            "$project": {
                "config_grupo_labor": 0
            }
        }



    ]
)