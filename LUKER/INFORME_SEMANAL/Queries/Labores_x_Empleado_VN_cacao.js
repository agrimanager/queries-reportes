//1_Labores_X_Empleado_base
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //------------------------------------------------------------------


        {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores_x_empleado",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": { "$max": "$when.start" }
                                                    }
                                                }
                                            }
                                            ,
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$filtro_fecha_inicio"
                                                    }
                                                }
                                            }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": { "$max": "$when.start" }
                                                    }
                                                }
                                            }
                                            ,
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$filtro_fecha_fin"
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    { "$match": { "farm": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "_id_str_farm": { "$toString": "$farm" }
                        }
                    }

                    //, { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }
                    , { "$match": { "_id_str_farm": { "$in": ["5d26491264f5b87ffc809eba"] } } }

                    // , {
                    //     "$addFields": {
                    //         "cultivo": {
                    //             "$cond": {
                    //                 "if": {"$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"]},
                    //                 "then": "PALMA",
                    //                 "else": "CACAO"
                    //             }
                    //         }
                    //     }
                    // }


                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },
                    { "$addFields": { "Labor": "$activity.name" } },

                    {
                        "$lookup": {
                            "from": "costsCenters",
                            "localField": "ccid",
                            "foreignField": "_id",
                            "as": "costsCenter"
                        }
                    },
                    { "$unwind": "$costsCenter" },
                    { "$addFields": { "Ceco": "$costsCenter.name" } },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    },
                    { "$unwind": "$farm" },
                    { "$addFields": { "Finca": "$farm.name" } },



                    { "$addFields": { "Fecha inicio": { "$max": "$when.start" } } },
                    {
                        "$addFields": {
                            "Anio": { "$year": "$Fecha inicio" },
                            "Mes": { "$month": "$Fecha inicio" },
                            "Semana": { "$week": "$Fecha inicio" }
                        }
                    },


                    {
                        "$project": {
                            // "_id": 0,

                            "Finca": "$Finca",
                            "Ceco": "$Ceco",
                            "Labor": "$Labor",
                            "Codigo Labor": "$cod",
                            "Estado Labor": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$status", "To do"] }, "then": "⏰ Por hacer" },
                                        { "case": { "$eq": ["$status", "Doing"] }, "then": "💪 En progreso" },
                                        { "case": { "$eq": ["$status", "Done"] }, "then": "✔ Listo" }
                                    ]
                                }
                            },

                            "Fecha": {"$dateToString": {"format": "%Y-%m-%d","date": "$Fecha inicio"}},
                            "Fecha inicio": "$Fecha inicio",

                            "Anio": "$Anio",
                            "Mes": "$Mes",
                            "Semana": "$Semana",

                            "Cantidad labor": "$productivityAchieved",
                            "Unidad labor": {
                                "$switch": {
                                    "branches": [{
                                        "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                                        "then": "Bloque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                                        "then": "Lotes"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                                        "then": "Linea"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                                        "then": "Árboles"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                                        "then": "Centro frutero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                                        "then": "Poligono de muestreo"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                                        "then": "Valvula"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                                        "then": "Drenaje"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                                        "then": "Aspersors"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                                        "then": "Red de riego uno"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                                        "then": "Red de riego dos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                                        "then": "Red de riego tres"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                                        "then": "Trampa"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                                        "then": "Vías"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                                        "then": "Bosque"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                                        "then": "Sensor"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                                        "then": "Cable vía"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                                        "then": "Edificio"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                                        "then": "Cuerpo de agua"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                                        "then": "Poligonos adicionales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                                        "then": "Unidades de cultivo (Ejemplo: Árboles)"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                                        "then": "Jornales"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                                        "then": "Cantidades"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                                        "then": "Metros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "km"] },
                                        "then": "Kilometros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                                        "then": "Centimetros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                                        "then": "Millas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                                        "then": "Yardas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                                        "then": "Pies"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                                        "then": "Pulgadas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                                        "then": "Kilogramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                                        "then": "Gramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                                        "then": "Miligramos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                                        "then": "Toneladas estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                                        "then": "Toneladas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                                        "then": "Onzas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                                        "then": "Libras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                                        "then": "Litros"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                                        "then": "Galones estadounidenses"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                                        "then": "Galones"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                                        "then": "Pies cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                                        "then": "Pulgadas cúbicas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                                        "then": "Centimetros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                                        "then": "Metros cúbicos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                                        "then": "Bultos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                                        "then": "Bolsas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                                        "then": "sacks"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                                        "then": "Yemas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                                        "then": "Factura"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                                        "then": "Flete"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                                        "then": "Picadero"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                                        "then": "Hora"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                                        "then": "Por cantidad"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                                        "then": "Hectáreas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                                        "then": "Cuadras"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                                        "then": "Canecas"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                                        "then": "Racimos"
                                    }, {
                                        "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                                        "then": "Metro cúbico"
                                    }, {
                                        "case": { "$eq": ["$productyPrice.measure", "metro-line"] },
                                        "then": "Metro Lineal"
                                    }],
                                    "default": "--------"
                                }
                            },



                        }
                    },





                ]

            }
        }


        , {
        "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data_labores_x_empleado"
                        , []
                    ]
                }
            }
    }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)