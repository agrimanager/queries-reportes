[

    {
        "$addFields": {
            "busqueda_anio": { "$year": "$Busqueda inicio" },
            "busqueda_plantacion": "Villanueva" 
        }
    },

    {
        "$lookup": {
            "from": "form_presupuestoactividades",
            "as": "form_presupuestoactividades_aux",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin",
                "busqueda_anio": "$busqueda_anio",
                "busqueda_plantacion": "$busqueda_plantacion"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Plantacion", "$$busqueda_plantacion"] },
                                { "$eq": ["$anio", "$$busqueda_anio"] }
                            ]
                        }
                    }
                }

                , {
                    "$project": {
                        "actividad": "$Actividad Agrimanager",
                        "anio": "$anio",
                        "mes": "$mes",
                        "cantidad presupuesto": "$cantidad presupuesto"

                        , "Busqueda inicio": "$$filtro_fecha_inicio"
                        , "Busqueda fin": "$$filtro_fecha_fin"
                    }
                }

            ]
        }
    }

    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$form_presupuestoactividades_aux"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


    , {
        "$lookup": {
            "from": "form_configuracionactividadesxcecospresupuesto",
            "as": "config_grupo_labor",
            "let": {
                "labor": "$actividad"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": { "$eq": ["$Actividad Agrimanager", "$$labor"] }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$config_grupo_labor",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "grupo_labor": { "$ifNull": ["$config_grupo_labor.Ceco Presupuesto", "--DESACTUALIZADO--"] }
        }
    },

    {
        "$project": {
            "config_grupo_labor": 0
            , "_id": 0
        }
    }


    , {
        "$lookup": {
            "from": "activities",
            "as": "activities_aux",
            "let": {
                "labor": "$actividad"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": { "$eq": ["$name", "$$labor"] }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    },

    {
        "$unwind": {
            "path": "$activities_aux",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "unidad_medida": { "$ifNull": ["$activities_aux.productivity.measure", "--DESACTUALIZADO--"] }

        }
    },

    {
        "$project": {
            "activities_aux": 0

            , "_id": 0
        }
    }




    , {
        "$lookup": {
            "from": "tasks",
            "as": "data_labores",
            "let": {
                "actividad": "$actividad",

                "anio": "$anio",
                "mes": "$mes",

                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                { "$match": { "farm": { "$ne": "" } } }
                , { "$addFields": { "_id_str_farm": { "$toString": "$farm" } } }
                , { "$match": { "_id_str_farm": { "$in": ["5d26491264f5b87ffc809eba"] } } }


                , {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                },
                { "$unwind": "$activity" },
                { "$addFields": { "Labor": "$activity.name" } },


                {
                    "$addFields": {
                        "anio_labor": { "$year": { "$max": "$when.start" } },
                        "mes_labor": { "$month": { "$max": "$when.start" } }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and":
                                [
                                    { "$eq": ["$Labor", "$$actividad"] },
                                    { "$eq": ["$anio_labor", "$$anio"] },
                                    { "$eq": ["$mes_labor", "$$mes"] }
                                ]
                        }
                    }
                }




                , {
                    "$group": {
                        "_id": {
                            "labor": "$Labor",

                            "anio": "$anio_labor",
                            "mes": "$mes_labor"
                        }
                        , "cantidad ejecucion": { "$sum": "$productivityAchieved" }
                    }
                }

                , {
                    "$project": {
                        "_id": 0,
                        "cantidad ejecucion": 1
                    }
                }

            ]

        }
    }

    , {
        "$unwind": {
            "path": "$data_labores",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "cantidad ejecucion": { "$ifNull": ["$data_labores.cantidad ejecucion", 0] }
        }
    }

    , {
        "$project": {
            "data_labores": 0
        }
    }





    , {
        "$addFields": {
            "unidad_medida": {
                "$switch": {
                    "branches": [{
                        "case": { "$eq": ["$unidad_medida", "blocks"] },
                        "then": "Bloque"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "lot"] },
                        "then": "Lotes"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "lines"] },
                        "then": "Linea"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "trees"] },
                        "then": "Árboles"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "fruitCenters"] },
                        "then": "Centro frutero"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "samplingPolygons"] },
                        "then": "Poligono de muestreo"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "valves"] },
                        "then": "Valvula"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "drainages"] },
                        "then": "Drenaje"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "sprinklers"] },
                        "then": "Aspersors"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "irrigationNetworkOne"] },
                        "then": "Red de riego uno"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "irrigationNetworkTwo"] },
                        "then": "Red de riego dos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "irrigationNetworkThree"] },
                        "then": "Red de riego tres"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "traps"] },
                        "then": "Trampa"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "lanes"] },
                        "then": "Vías"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "woods"] },
                        "then": "Bosque"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "sensors"] },
                        "then": "Sensor"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cableways"] },
                        "then": "Cable vía"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "buildings"] },
                        "then": "Edificio"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "waterBodies"] },
                        "then": "Cuerpo de agua"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "additionalPolygons"] },
                        "then": "Poligonos adicionales"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "farming units"] },
                        "then": "Unidades de cultivo (Ejemplo: Árboles)"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "wages"] },
                        "then": "Jornales"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "quantity"] },
                        "then": "Cantidades"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "mts"] },
                        "then": "Metros"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "km"] },
                        "then": "Kilometros"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cm"] },
                        "then": "Centimetros"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "mile"] },
                        "then": "Millas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "yard"] },
                        "then": "Yardas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "foot"] },
                        "then": "Pies"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "inch"] },
                        "then": "Pulgadas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "kg"] },
                        "then": "Kilogramos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "gr"] },
                        "then": "Gramos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "mg"] },
                        "then": "Miligramos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "US/ton"] },
                        "then": "Toneladas estadounidenses"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "ton"] },
                        "then": "Toneladas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "oz"] },
                        "then": "Onzas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "lb"] },
                        "then": "Libras"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "lts"] },
                        "then": "Litros"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "US/galon"] },
                        "then": "Galones estadounidenses"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "galon"] },
                        "then": "Galones"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cf"] },
                        "then": "Pies cúbicos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "ci"] },
                        "then": "Pulgadas cúbicas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cuc"] },
                        "then": "Centimetros cúbicos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cum"] },
                        "then": "Metros cúbicos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "packages"] },
                        "then": "Bultos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "bags"] },
                        "then": "Bolsas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "sacks"] },
                        "then": "sacks"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "yemas"] },
                        "then": "Yemas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "bun"] },
                        "then": "Factura"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cargo"] },
                        "then": "Flete"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "manege"] },
                        "then": "Picadero"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "hr"] },
                        "then": "Hora"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "qty"] },
                        "then": "Por cantidad"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "hectares"] },
                        "then": "Hectáreas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "squares"] },
                        "then": "Cuadras"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "dustbin"] },
                        "then": "Canecas"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "bunch"] },
                        "then": "Racimos"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "cubic-meter"] },
                        "then": "Metro cúbico"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "metro-line"] },
                        "then": "Metro Lineal"
                    }, {
                        "case": { "$eq": ["$unidad_medida", "square-meter"] },
                        "then": "Metro Cuadrado"
                    }],
                    "default": "--NO EXISTE--"
                }
            }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$addFields": {
            "rgDate": "$Busqueda inicio"
        }
    }
    
    , {
        "$project": {
            "Grupo Labor": "$grupo_labor",
            "Labor": "$actividad",
            "Unidad": "$unidad_medida",
            
            "Ejecucion": "$cantidad ejecucion",
            "Ppto": "$cantidad presupuesto",
            
            "Anio": "$anio",
            "Mes": "$Mes_Txt",
            
            "Busqueda inicio": "$Busqueda inicio",
            "Busqueda fin": "$Busqueda fin",
            "rgDate": "$rgDate"
            
        }
    }


]