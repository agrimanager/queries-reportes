db.form_turnosdefertirriego.aggregate(

    [
        //===info valvulas y lotes
        {
            "$lookup": {
                "from": "form_valvulas",
                "as": "form_valvulas_aux",
                "let": {
                    "turno": "$TURNO",
                    "sector": "$SECTOR",
                    "farm_id": "$Point.farm",
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$TURNO", "$$turno"] },
                                    { "$eq": ["$SECTOR", "$$sector"] },
                                    { "$eq": ["$Point.farm", "$$farm_id"] }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$LOTE.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    { "$unwind": "$LOTE.features" },
                    {
                        "$addFields": {
                            "features_oid": [{ "$toObjectId": "$LOTE.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                                    "then": "$feature_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                                            "then": "$feature_2",
                                            "else": "$feature_3"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$addFields": {
                            "bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "lote": {
                                "$cond": {
                                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                    "then": "$feature_1.properties.name",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                            "then": "$feature_2.properties.name",
                                            "else": "$feature_3.properties.name"
                                        }
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0,

                            "feature_1": 0,
                            "feature_2": 0,
                            "feature_3": 0


                            , "LOTE": 0
                            , "Point": 0
                            , "uid": 0
                            , "rgDate": 0
                            , "uDate": 0
                            , "supervisor": 0
                            , "capture": 0
                            , "_id": 0

                        }
                    }


                ]
            }
        }

        , {
            "$addFields": {
                "Area_Total_lotes": {
                    "$reduce": {
                        "input": "$form_valvulas_aux.AREA x LOTE",
                        "initialValue": 0,
                        "in": {
                            "$sum": ["$$this", "$$value"]
                        }
                    }
                }
            }
        }


        , { "$unwind": "$form_valvulas_aux" }


        , {
            "$addFields": {
                "finca": "$form_valvulas_aux.finca",
                "bloque": "$form_valvulas_aux.bloque",
                "lote": "$form_valvulas_aux.lote",

                "VALVULA": "$form_valvulas_aux.VALVULA",
                "AREA x LOTE": "$form_valvulas_aux.AREA x LOTE"
            }
        }


        , {
            "$project": {
                "form_valvulas_aux": 0
            }
        }


        //=====calculos

        , {
            "$addFields": {
                "pct_area_lote": {
                    "$cond": {
                        "if": { "$eq": ["$Area_Total_lotes", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$AREA x LOTE", "$Area_Total_lotes"]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "cantidad_x_lote": {
                    "$multiply": ["$CANTIDAD", "$pct_area_lote"]
                }
            }
        }

    ]

)