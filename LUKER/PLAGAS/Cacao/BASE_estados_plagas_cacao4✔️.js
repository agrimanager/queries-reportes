[
  
    
     {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    
    { "$unwind": "$Lote.features" },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    
    
    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }
    
   
    , {
        "$addFields": {
            "nombre_maestro_principal": "Plaga_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }

    , {
        "$addFields": {
            "Estado_plaga_nombre": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Estado_plaga_nombre": { "$arrayElemAt": ["$Estado_plaga_nombre", 0] }
        }
    }

    , {
        "$addFields": {
            "Estado_plaga_valor": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , {
        "$unwind": {
            "path": "$Estado_plaga_valor",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$sort": {
            "Estado_plaga_valor": -1
        }
    }

    , {
        "$project": {
            "nombre_maestro_principal": 0,
            "num_letras_nombre_maestro_principal": 0,

            "Plaga_Hormiga arriera Atta Cephalotes": 0,
            "Plaga_Estado Plagas LHPPA": 0,
            "Plaga_Cacao - Hormiga arriera": 0,
            "Plaga_Acaros Afidos Pulgones  y lorito verde": 0,
            "Plaga_hormiga arriera cacao": 0,
            "Plaga_Lepidopteros": 0,
            "Plaga_Planta Sana": 0,
            "Plaga_Termita": 0

        }
    }


     ,{
        "$addFields": {
            "anio":{ "$year": { "date": "$rgDate" }},
            "num_mes":{ "$month": { "date": "$rgDate" }}
        }
    }

    ,{
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }

]
