[

    {
        "$match": {
            "Plaga": { "$ne": "Planta Sana" }
        }
    },

    {
        "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$addFields": {
            "lote_id_str": "$Cartography.features._id"
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },



    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "Cartography": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            },

            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }




    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote",
                "nombre_finca": "$finca"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "CACAO"] },
                                { "$eq": ["$FINCA", "$$nombre_finca"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra": "$referencia_siembras.SIEMBRA",
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    }

    , {
        "$project": {
            "referencia_siembras": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "lote_id_str": "$lote_id_str",
                "bloque": "$bloque",
                "lote": "$lote",
                "lote_ha": "$Hectareas",
                "plaga": "$Plaga"

                , "filtro_fecha_inicio": "$Busqueda inicio"
                , "filtro_fecha_fin": "$Busqueda fin"
            }
            , "sum_cantidad": { "$sum": "$Cantidad" }
            , "sum_plantas_a_evaluar": { "$sum": "$Plantas a evaluar" }

            , "data": { "$push": "$$ROOT" }


        }
    }


    , {
        "$lookup": {
            "from": "form_sanidadcacaoenfermedades",
            "as": "data_enfermedades",
            "let": {
                "lote_id_str": "$_id.lote_id_str",
                "lote": "$_id.lote"

                , "filtro_fecha_inicio": "$_id.filtro_fecha_inicio"
                , "filtro_fecha_fin": "$_id.filtro_fecha_fin"
            },
            "pipeline": [
                {
                    "$match": {
                        "Enfermedad_Moni Grup 2": { "$exists": true }
                    }
                },
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Enfermedad", "Moni Grup 2"] },
                                { "$in": ["$Enfermedad_Moni Grup 2", ["Moni Grup 2 Frutos sanos", "Moni Grup 2 FrutsEnfermos"]] }
                            ]
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$in": ["$$lote_id_str", "$Lote.features._id"] }
                            ]
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },

                {
                    "$group": {
                        "_id": "$$lote_id_str"
                        , "sum_cantidad": { "$sum": "$Cantidad" }
                    }
                }

            ]

        }
    }

    , {
        "$unwind": {
            "path": "$data_enfermedades",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$group": {
            "_id": {
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "plaga": "$_id.plaga"
            }

            , "numerador": { "$sum": "$sum_cantidad" }

            , "denominador": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$_id.plaga", "Hormiga arriera Atta Cephalotes"] }
                                        , { "$eq": ["$_id.plaga", "Hormiga  Arriera"] }
                                    ]
                                },
                                "then": { "$multiply": ["$_id.lote_ha", 10000] }
                            },

                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$_id.plaga", "Ardilla Sciurus sp"] }
                                        , { "$eq": ["$_id.plaga", "Pajaros"] }
                                    ]
                                },
                                "then": "$data_enfermedades.sum_cantidad"
                            }
                        ],
                        "default": "$sum_plantas_a_evaluar"
                    }
                }
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$addFields": {
            "pct_incidencia": {
                "$cond": {
                    "if": { "$eq": ["$denominador", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{ "$divide": ["$numerador", "$denominador"] }, 100]
                    }
                }
            }

        }
    }

    , {
        "$addFields": {
            "pct_incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia", 100] }, 1] }] }, 100] }

        }
    }

    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$eq": ["$pct_incidencia", 0] }]
                    },
                    "then": "#00FF00",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                            },
                            "then": "#008000",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia", 5] }]
                                    },
                                    "then": "#ffff00",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$pct_incidencia", 5] }, { "$lte": ["$pct_incidencia", 8] }]
                                            },
                                            "then": "#ff8000",
                                            "else": "#ff0000"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$eq": ["$pct_incidencia", 0] }]
                    },
                    "then": "A-[=0%]",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                            },
                            "then": "B-(0% - 3%]",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia", 5] }]
                                    },
                                    "then": "C-(3% - 5%]",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gt": ["$pct_incidencia", 5] }, { "$lte": ["$pct_incidencia", 8] }]
                                            },
                                            "then": "D-(5% - 8%]",
                                            "else": "E-(>8%)"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "_id": { "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.elemnq", 0] }, 0]},
            "idform": { "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.idform", 0] }, 0] },
            "type": "Feature",
            "properties": {
                "Bloque": "$_id.bloque",
                "Lote": "$_id.lote",
                "Plaga": "$_id.plaga",
                "Rango": "$rango",
                "%Incidencia": {
                    "$concat": [
                        { "$toString": "$pct_incidencia" },
                        " %"
                    ]
                },

                "color": "$color"
            },
            "geometry": {"$arrayElemAt": [{ "$arrayElemAt": ["$data.data.Cartography.geometry", 0] }, 0]}
        }
    }





]