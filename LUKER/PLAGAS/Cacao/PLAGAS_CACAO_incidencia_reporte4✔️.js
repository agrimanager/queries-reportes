[

 
    {
        "$match": {
            "Plaga": { "$ne": "Planta Sana" }
        }
    },



    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    { "$unwind": "$Lote.features" },

    {
        "$addFields": {
            "lote_id_str": "$Lote.features._id"
        }
    },

    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },



    {
        "$addFields": {
            "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.type", "Farm"] },
                    "then": "$feature_1",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.type", "Farm"] },
                            "then": "$feature_2",
                            "else": "$feature_3"
                        }
                    }
                }
            }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$addFields": {
            "bloque": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "lote": {
                "$cond": {
                    "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                    "then": "$feature_1.properties.name",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                            "then": "$feature_2.properties.name",
                            "else": "$feature_3.properties.name"
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,

            "feature_1": 0,
            "feature_2": 0,
            "feature_3": 0
        }
    }

    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote",
                "nombre_finca": "$finca"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "CACAO"] },
                                { "$eq": ["$FINCA", "$$nombre_finca"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra": "$referencia_siembras.SIEMBRA",
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    }

    , {
        "$project": {
            "referencia_siembras": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "lote_id_str": "$lote_id_str",


                "bloque": "$bloque",
                "lote": "$lote",
                "lote_ha": "$Hectareas",
                "plaga": "$Plaga"

                , "filtro_fecha_inicio": "$Busqueda inicio"
                , "filtro_fecha_fin": "$Busqueda fin"
            }
            , "sum_cantidad": { "$sum": "$Cantidad" }
            , "sum_plantas_a_evaluar": { "$sum": "$Plantas a evaluar" }

        }
    }


    , {
        "$lookup": {
            "from": "form_sanidadcacaoenfermedades",
            "as": "data_enfermedades",
            "let": {
                "lote_id_str": "$_id.lote_id_str",
                "lote": "$_id.lote"

                , "filtro_fecha_inicio": "$_id.filtro_fecha_inicio"
                , "filtro_fecha_fin": "$_id.filtro_fecha_fin"
            },
            "pipeline": [

                {
                    "$match": {
                        "Enfermedad_Moni Grup 2": { "$exists": true }
                    }
                },
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Enfermedad", "Moni Grup 2"] },
                                { "$in": ["$Enfermedad_Moni Grup 2", ["Moni Grup 2 Frutos sanos", "Moni Grup 2 FrutsEnfermos"]] }
                            ]
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$in": ["$$lote_id_str", "$Lote.features._id"] }
                            ]
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },

                {
                    "$group": {
                        "_id": "$$lote_id_str"
                        , "sum_cantidad": { "$sum": "$Cantidad" }
                    }
                }

            ]

        }
    }

    , {
        "$unwind": {
            "path": "$data_enfermedades",
            "preserveNullAndEmptyArrays": true
        }
    }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "sum_cantidad": "$sum_cantidad",
                        "sum_plantas_a_evaluar": "$sum_plantas_a_evaluar",
                        "data_enfermedades_mon_g2": "$data_enfermedades.sum_cantidad"

                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "bloque": "$bloque",
                "lote": "$lote",
                "plaga": "$plaga"
            }

            , "numerador": { "$sum": "$sum_cantidad" }

            , "denominador": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$plaga", "Hormiga arriera Atta Cephalotes"] }
                                        , { "$eq": ["$plaga", "Hormiga  Arriera"] }
                                    ]
                                },
                                "then": { "$multiply": ["$lote_ha", 10000] }
                            },
                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$plaga", "Ardilla Sciurus sp"] }
                                        , { "$eq": ["$plaga", "Pajaros"] }
                                    ]
                                },
                                "then": "$data_enfermedades_mon_g2"
                            }
                        ],
                        "default": "$sum_plantas_a_evaluar"
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "pct_incidencia": {
                "$cond": {
                    "if": { "$eq": ["$denominador", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{ "$divide": ["$numerador", "$denominador"] }, 100]
                    }
                }
            }

        }
    }


    , {
        "$addFields": {
            "pct_incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia", 100] }, 1] }] }, 100] }

        }
    }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "numerador": "$numerador",
                        "denominador": "$denominador",
                        "pct_incidencia": "$pct_incidencia"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "nota_formula": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$or": [
                                    { "$eq": ["$plaga", "Hormiga arriera Atta Cephalotes"] }
                                    , { "$eq": ["$plaga", "Hormiga  Arriera"] }
                                ]
                            },
                            "then": "numerador= Cantidad | denominador= Ha Lote"
                        },

                        {
                            "case": {
                                "$or": [
                                    { "$eq": ["$plaga", "Ardilla Sciurus sp"] }
                                    , { "$eq": ["$plaga", "Pajaros"] }
                                ]
                            },
                            "then": "numerador= Cantidad | denominador= Enfermedades Monilia Grupo 2 (Sanos y enfermos)"
                        }
                    ],
                    "default": "numerador= Cantidad | denominador= Plantas a evaluar"
                }
            }

        }
    }

]