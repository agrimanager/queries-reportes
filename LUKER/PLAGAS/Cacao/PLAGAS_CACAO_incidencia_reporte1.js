db.form_cacaoplagas.aggregate(
    [

        // //----------------TEST
        {
            "$match": {
                "$and": [
                    {
                        "rgDate": {
                            //"$gte": "2020-01-01T21:10:17.000Z"
                            //"$gte": {"$toDate":"2020-01-01T21:10:17.000Z"}
                            "$gte": ISODate("2020-08-13T11:44:17.117-05:00")
                        }
                    },
                    {
                        "rgDate": {
                            //"$lte": {"$toDate":"2020-02-20T21:10:17.000Z"}
                            "$lte": ISODate("2020-09-15T11:44:17.117-05:00")
                        }
                    }
                ]
            }
        },

        {
            "$match": {
                "Point.farm": "5d26499b64f5b87ffc809ebf"
            }
        },
        // //----------------


        //===== CONDICIONALES BASE
        //t1
        {
            "$match": {
                "Plaga": { "$ne": "Planta Sana" }
            }
        },


        //======CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },



        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0
            }
        }




        //====== FECHAS
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }




        //====== INFO LOTE
        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote",
                    "nombre_finca": "$finca"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "CACAO"] },
                                    { "$eq": ["$FINCA", "$$nombre_finca"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        }

        , {
            "$project": {
                "referencia_siembras": 0
            }
        }




        //====== INCIDENCIA


        // ,{
        //     $match:{
        //         Enfermedad:"Planta Sana"
        //     }
        // }



        //---group
        , {
            "$group": {
                "_id": {
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "lote_ha": "$Hectareas",
                    "plaga": "$Plaga"
                }
                , "count": { "$sum": 1 }
                , "sum_cantidad": { "$sum": "$Cantidad" }
                , "sum_plantas_a_evaluar": { "$sum": "$Plantas a evaluar" }

                //, "data":{"$push":"$$ROOT"}


            }
        }

        // //t2
        // //---proceso1
        // , {
        //     "$addFields": {
        //         "filtro_data": {
        //             "$switch": {
        //                 "branches": [
        //                     //---MONILIA
        //                     //--g1
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Enfermos"] }
        //                                 , { "$ne": ["$_id.enfermedad", "Moni Grup 1"] }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     },
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Sanos"] }
        //                                 , { "$ne": ["$_id.enfermedad", "Moni Grup 1"] }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     },

        //                     //--g2
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.grupo_valor", "Moni Grup 2 FrutsEnfermos"] }
        //                                 , { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     },
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.grupo_valor", "Moni Grup 2 Frutos sanos"] }
        //                                 , { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     },
        //                 ],
        //                 "default": "no borrar"
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$match": {
        //         "filtro_data": "no borrar"
        //     }
        // }

        // //---proceso2
        // , {
        //     "$addFields": {
        //         "filtro_data": {
        //             "$switch": {
        //                 "branches": [
        //                     //---MONILIA
        //                     //--g1
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.enfermedad", "Moni Grup 1"] }
        //                                 , {
        //                                     "$not":
        //                                         { "$in": ["$_id.grupo_valor", ["Moni Grup 1 Fruts Enfermos", "Moni Grup 1 Fruts Sanos"]] }
        //                                 }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     },
        //                     //--g2
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$_id.enfermedad", "Moni Grup 2"] }
        //                                 , {
        //                                     "$not":
        //                                         { "$in": ["$_id.grupo_valor", ["Moni Grup 2 FrutsEnfermos", "Moni Grup 2 Frutos sanos"]] }
        //                                 }
        //                             ]
        //                         },
        //                         "then": "si borrar"
        //                     }
        //                 ],
        //                 "default": "no borrar"
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$match": {
        //         "filtro_data": "no borrar"
        //     }
        // }



        // //---variables de calculos


        // //---group
        // , {
        //     "$group": {
        //         "_id": {
        //             "bloque": "$_id.bloque",
        //             "lote": "$_id.lote",
        //             "enfermedad": "$_id.enfermedad"
        //         }

        //         //A
        //         , "numerador": {
        //             "$sum": {
        //                 "$switch": {
        //                     "branches": [
        //                         //--caso1
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Phytophthora fruto"] }
        //                                     , { "$eq": ["$_id.grupo_valor", "Fruto afectado"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         },

        //                         //--caso2
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Moni Grup 1"] }
        //                                     , { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Enfermos"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         }
        //                         , {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Moni Grup 2"] }
        //                                     , { "$eq": ["$_id.grupo_valor", "Moni Grup 2 FrutsEnfermos"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         },

        //                         //--caso3
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$ne": ["$_id.enfermedad", "Phytophthora fruto"] },
        //                                     { "$ne": ["$_id.enfermedad", "Moni Grup 1"] },
        //                                     { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         }


        //                     ],
        //                     //"default": "$sum_cantidad"
        //                     "default": 0
        //                 }
        //             }
        //         }

        //         //B

        //         , "denominador": {
        //             "$sum": {
        //                 "$switch": {
        //                     "branches": [
        //                         //--caso1
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Phytophthora fruto"] }
        //                                     , { "$eq": ["$_id.grupo_valor", "Fruto afectado"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad_mazorcas"
        //                         },

        //                         //--caso2
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Moni Grup 1"] }
        //                                     //-----CAMBIO
        //                                     // , { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Sanos"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         }
        //                         , {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$eq": ["$_id.enfermedad", "Moni Grup 2"] }
        //                                     //-----CAMBIO
        //                                     // , { "$eq": ["$_id.grupo_valor", "Moni Grup 2 Frutos sanos"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_cantidad"
        //                         },

        //                         //--caso3
        //                         {
        //                             "case": {
        //                                 "$and": [
        //                                     { "$ne": ["$_id.enfermedad", "Phytophthora fruto"] },
        //                                     { "$ne": ["$_id.enfermedad", "Moni Grup 1"] },
        //                                     { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
        //                                 ]
        //                             },
        //                             "then": "$sum_plantas_a_evaluar"
        //                         }
        //                     ],
        //                     //"default": "$sum_plantas_a_evaluar"
        //                     "default": 0
        //                 }
        //             }
        //         }

        //         //, "data":{"$push":"$$ROOT"}


        //     }
        // }

        // , {
        //     "$addFields": {
        //         "pct_incidencia": {
        //             "$cond": {
        //                 "if": { "$eq": ["$denominador", 0] },
        //                 "then": 0,
        //                 "else": {
        //                     "$multiply": [{ "$divide": ["$numerador", "$denominador"] }, 100]
        //                 }
        //             }
        //         }

        //     }
        // }


        // //---2 decimales
        // , {
        //     "$addFields": {
        //         "pct_incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia", 100] }, 1] }] }, 100] }

        //     }
        // }



        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {
        //                     "numerador": "$numerador",
        //                     "denominador": "$denominador",
        //                     "pct_incidencia": "$pct_incidencia"
        //                 }
        //             ]
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "nota_formula": {
        //             "$switch": {
        //                 "branches": [
        //                     //--caso1
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$eq": ["$enfermedad", "Phytophthora fruto"] }
        //                             ]
        //                         },
        //                         "then": "numerador= Cantidad | denominador= Mazorcas"
        //                     },

        //                     //--caso2
        //                     {
        //                         "case": {
        //                             "$or": [
        //                                 { "$eq": ["$enfermedad", "Moni Grup 1"] },
        //                                 { "$eq": ["$enfermedad", "Moni Grup 2"] }
        //                             ]
        //                         },
        //                         "then": "numerador= Enfermos | denominador= Enfermos y Sanos"
        //                     },

        //                     //--caso3
        //                     {
        //                         "case": {
        //                             "$and": [
        //                                 { "$ne": ["$enfermedad", "Phytophthora fruto"] },
        //                                 { "$ne": ["$enfermedad", "Moni Grup 1"] },
        //                                 { "$ne": ["$enfermedad", "Moni Grup 2"] }
        //                             ]
        //                         },
        //                         "then": "numerador= Cantidad | denominador= Plantas a evaluar"
        //                     }
        //                 ],
        //                 //"default": "$sum_plantas_a_evaluar"
        //                 "default": ""
        //             }
        //         }

        //     }
        // }






    ]

)