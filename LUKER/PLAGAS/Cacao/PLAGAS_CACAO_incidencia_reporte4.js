db.form_cacaoplagas.aggregate(
    [

        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-08-13T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------

        {
            "$match": {
                "Point.farm": "5d26499b64f5b87ffc809ebf"
            }
        },
        // //----------------


        //===== CONDICIONALES BASE
        //t1
        {
            "$match": {
                "Plaga": { "$ne": "Planta Sana" }
            }
        },


        //======CARTOGRAFIA

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },

        //-----IMPORTANTE PARA CRUCE DE ENFERMEDADES
        {
            "$addFields": {
                "lote_id_str": "$Lote.features._id"
            }
        },

        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },



        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0
            }
        }




        // //====== FECHAS
        // , {
        //     "$addFields": {
        //         "anio": { "$year": { "date": "$rgDate" } },
        //         "num_mes": { "$month": { "date": "$rgDate" } }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "Mes_Txt": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
        //                     { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
        //                     { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
        //                     { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
        //                     { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
        //                     { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
        //                     { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
        //                     { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
        //                     { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
        //                     { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
        //                     { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
        //                     { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
        //                 ],
        //                 "default": "Mes desconocido"
        //             }
        //         }
        //     }
        // }




        //====== INFO LOTE
        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote",
                    "nombre_finca": "$finca"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "CACAO"] },
                                    { "$eq": ["$FINCA", "$$nombre_finca"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        }

        , {
            "$project": {
                "referencia_siembras": 0
            }
        }




        // //====== INCIDENCIA

        //---group
        , {
            "$group": {
                "_id": {
                    "lote_id_str": "$lote_id_str",

                    // "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "lote_ha": "$Hectareas",
                    "plaga": "$Plaga"

                    , "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                }
                // , "cantidad_registros": { "$sum": 1 }
                , "sum_cantidad": { "$sum": "$Cantidad" }
                , "sum_plantas_a_evaluar": { "$sum": "$Plantas a evaluar" }

                //, "data":{"$push":"$$ROOT"}


            }
        }


        //---!!!CRUZAR CON QUERY DE ENFERMEDADES
        //----PARA OBTENER SANOS Y ENFERMOS DE MONIA GRUPO 2

        , {
            "$lookup": {
                "from": "form_sanidadcacaoenfermedades",
                "as": "data_enfermedades",
                "let": {
                    "lote_id_str": "$_id.lote_id_str",

                    // "finca": "$_id.finca",
                    "lote": "$_id.lote"

                    , "filtro_fecha_inicio": "$_id.filtro_fecha_inicio"
                    , "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "pipeline": [

                    //----filtro de enfermedad (Monilia grupo 2)
                    {
                        "$match": {
                            "Enfermedad_Moni Grup 2": { "$exists": true }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Enfermedad", "Moni Grup 2"] },
                                    { "$in": ["$Enfermedad_Moni Grup 2", ["Moni Grup 2 Frutos sanos", "Moni Grup 2 FrutsEnfermos"]] }
                                ]
                            }
                        }
                    },

                    //----filtro de cartografia (lote)
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    // { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$lote_id_str", "$Lote.features._id"] }
                                ]
                            }
                        }
                    },


                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$group": {
                            "_id": "$$lote_id_str"
                            , "sum_cantidad": { "$sum": "$Cantidad" }
                        }
                    }

                ]

            }
        }

        , {
            "$unwind": {
                "path": "$data_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "sum_cantidad": "$sum_cantidad",
                            "sum_plantas_a_evaluar": "$sum_plantas_a_evaluar",
                            "data_enfermedades_mon_g2": "$data_enfermedades.sum_cantidad"

                        }
                    ]
                }
            }
        }


        //---variables de calculos


        //---group
        , {
            "$group": {
                "_id": {
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "plaga": "$plaga"
                }

                //A
                , "numerador": { "$sum": "$sum_cantidad" }

                //B

                , "denominador": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                //--caso1
                                {
                                    "case": {
                                        "$or": [
                                            { "$eq": ["$plaga", "Hormiga arriera Atta Cephalotes"] }
                                            , { "$eq": ["$plaga", "Hormiga  Arriera"] }
                                        ]
                                    },
                                    //"then": "$lote_ha" //*10000
                                    "then": { "$multiply": ["$lote_ha", 10000] }
                                },

                                //--caso2
                                {
                                    "case": {
                                        "$or": [
                                            { "$eq": ["$plaga", "Ardilla Sciurus sp"] }
                                            , { "$eq": ["$plaga", "Pajaros"] }
                                        ]
                                    },
                                    "then": "$data_enfermedades_mon_g2"
                                }
                            ],
                            "default": "$sum_plantas_a_evaluar"
                            // "default": 0
                        }
                    }
                }
                //, "data":{"$push":"$$ROOT"}
            }
        }

        , {
            "$addFields": {
                "pct_incidencia": {
                    "$cond": {
                        "if": { "$eq": ["$denominador", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [{ "$divide": ["$numerador", "$denominador"] }, 100]
                        }
                    }
                }

            }
        }


        //---2 decimales
        , {
            "$addFields": {
                "pct_incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia", 100] }, 1] }] }, 100] }

            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "numerador": "$numerador",
                            "denominador": "$denominador",
                            "pct_incidencia": "$pct_incidencia"
                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "nota_formula": {
                    "$switch": {
                        "branches": [
                            //--caso1
                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$plaga", "Hormiga arriera Atta Cephalotes"] }
                                        , { "$eq": ["$plaga", "Hormiga  Arriera"] }
                                    ]
                                },
                                "then": "numerador= Cantidad | denominador= Ha Lote"
                            },

                            //--caso2
                            {
                                "case": {
                                    "$or": [
                                        { "$eq": ["$plaga", "Ardilla Sciurus sp"] }
                                        , { "$eq": ["$plaga", "Pajaros"] }
                                    ]
                                },
                                "then": "numerador= Cantidad | denominador= Enfermedades Monilia Grupo 2 (Sanos y enfermos)"
                            }
                        ],
                        "default": "numerador= Cantidad | denominador= Plantas a evaluar"
                    }
                }

            }
        }

    ]

)