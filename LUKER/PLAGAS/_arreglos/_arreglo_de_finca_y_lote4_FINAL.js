//--Editar Point.farm, Lote.features y Lote.path


//----obtener registros de formulario
var formulario = db.form_cacaoplagas.aggregate(
    {
        $match: {
            //--finca de vn_palma
            "Point.farm": "5d2648a845a0dd2e9e204fe2"
        }
    },
    {
        $addFields: {
            "lote": "$Lote.features.properties.name"
        }
    },
    { $unwind: "$lote" }
);
//formulario

//----obtener cartografia de features
var cartografia_nueva = db.cartography.aggregate(
    {
        $match: {
            "properties.name": {
                $in: ["AP-20A", "B-32", "B-34", "C-35", "D-33", "E-32", "H-11B"]
            }
        }
    },
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$split_path", 0] }
        }
    },
    {
        $match: {
            //--finca de vn_cacao
            "finca": "5d26491264f5b87ffc809eba"
        }
    },
);
// cartografia_nueva

//---convertir en array para ciclar y obtener valores
var cartografia_nueva_array = cartografia_nueva.toArray();


//--DOBLE CICLO
formulario.forEach(i_form => {
    for (var idx = 0; idx < cartografia_nueva_array.length; idx++) {

        //condicion de lote
        if (i_form.lote == cartografia_nueva_array[idx].properties.name) {
            
            //--Editar Point.farm, Lote.features y Lote.path
            db.form_cacaoplagas.update(
                {"_id": i_form._id},
                {
                    $set: {
                        //--finca de vn_cacao
                        // "Point.farm": "5d26491264f5b87ffc809eba",
                        "Point.farm": cartografia_nueva_array[idx].finca,
                        
                        "Lote.path": cartografia_nueva_array[idx].path,

                        "Lote.features": [cartografia_nueva_array[idx]],
                    }
                }

            )
        }
    }
});



