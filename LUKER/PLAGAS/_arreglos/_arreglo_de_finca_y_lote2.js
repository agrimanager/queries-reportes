

//---formulario de plagas de cacao

//---finca a cambiar = vn_palma X vn_cacao
//---lotes a cambiar = ["AP-20A", "B-32", "B-34", "C-35", "D-33", "E-32", "H-11B"]


//=====ANALISIS

//--1)identificar los registros con finca erronea
db.form_cacaoplagas.aggregate(
    {
        $match: {
            "Point.farm": "5d2648a845a0dd2e9e204fe2"
        }
    },
    {
        $addFields:{
            "lote":"$Lote.features.properties.name"
        }
    },
    {$unwind:"$lote"}
)

//--2)identificar los lotes los registros con finca erronea
db.cartography.aggregate(

    {
        $match: {
            "properties.name": {
                $in: ["AP-20A", "B-32", "B-34", "C-35", "D-33", "E-32", "H-11B"]
            }
        }
    },
    
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },

	{
	    "$addFields": {
	        "finca": { "$arrayElemAt": ["$split_path", 0] }
	    }
	},

	// {
    //     $match: {
    //         //--finca de vn_cacao
    //         "finca": "5d26491264f5b87ffc809eba"
    //     }
    // },
)



//=====EDICIONES


//--1)Editar Lote.features y Lote.path
var formulario = db.form_cacaoplagas.aggregate(
    {
        $match: {
            "Point.farm": "5d2648a845a0dd2e9e204fe2"
        }
    },
    {
        $addFields: {
            "lote": "$Lote.features.properties.name"
        }
    },
    { $unwind: "$lote" }
);

//formulario


formulario.forEach(i => {
    db.form_cacaoplagas.update(
        {
            "_id": i._id
        },
        {
            $set: {
                //--finca de vn_cacao
                "Point.farm": "5d26491264f5b87ffc809eba",

                "Lote.path": "XXXXXXXXXX",

                "Lote.features": "XXXXXXXXXX",
            }
        }

    )
})





// //--2)Editar Point.farm
// db.form_cacaoplagas.updateMany(
//     {
//     	//--finca de vn_palma
// 		"Point.farm": "5d2648a845a0dd2e9e204fe2"
//     },
//     {
//     	$set:{
//     		//--finca de vn_cacao
//     		"Point.farm": "5d26491264f5b87ffc809eba"
//     	}
//     }
// )