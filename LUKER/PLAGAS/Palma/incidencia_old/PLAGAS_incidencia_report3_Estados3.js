//---incidencia
db.form_modulodeplagas.aggregate(
    [
        //--tests
        // {$match:{"Plaga 1":{$eq:""}}},
        // {$match:{"Plaga 1":{$ne:""}}},
        //{ $sort: { rgDate: -1 } },
        // {$match: {"Plaga 1_Estado Plagas LHPPA":{$exists: true} }},
        {
            $match: {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2000-05-01T01:21:23.000-05:00") } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2020-07-01T01:21:23.000-05:00") } } }
                            ]
                        }
                    ]
                }
            }
        },
        //---------------------------------




        //----CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },
        {
            "$addFields": {
                "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                //----no mostrar otros datos 
                "Formula": 0,
                "Point": 0,
                "uid": 0,
                "Lote": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,

            }
        }

        //--plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        //----PLAGAS Y ESTADOS
        //antes el formulario tenia 6 campos (p1,p2,p3..,p6)
        //ya solo hay 1 campo plaga1 (p1)


        , {
            "$addFields": {
                "array_plagas": [
                    {
                        "p": { "$ifNull": ["$Plaga 1", ""] },
                        "c": { "$ifNull": ["$cantidad plaga 1", 0] },
                        // "e": "$Plaga 1_Estado Plagas LHPPA"
                    },
                    {
                        "p": { "$ifNull": ["$Plaga 2", ""] },
                        "c": { "$ifNull": ["$cantidad Plagas 2",0] },
                        // "e": "$Plaga 2_Estado Plagas LHPPA"
                    },
                    {
                        "p": { "$ifNull": ["$Plagas 3", ""] },
                        "c": { "$ifNull": ["$Cantidad Plaga 3",0] },
                        // "e": "$Plagas 3_Estado Plagas LHPPA"
                    },
                    {
                        "p": { "$ifNull": ["$plaga 4", ""] },
                        "c": { "$ifNull": ["$Cantidad plaga 4",0] },
                        // "e": "$plaga 4_Estado Plagas LHPPA"
                    },
                    {
                        "p": { "$ifNull": ["$Plagas 5", ""] },
                        "c": { "$ifNull": ["$Cantidad Plaga 5",0] },
                        // "e": "$Plagas 5_Estado Plagas LHPPA"
                    }


                ]
            }
        }





        // //------------------------plaga1
        // //--Maestro principal
        // , { "$addFields": { "nombre_maestro_principal_1": "Plaga 1_" } }
        // , { "$addFields": { "num_letras_nombre_maestro_principal_1": { "$strLenCP": "$nombre_maestro_principal_1" } } }

        // //--Mestro enlazado
        // , {
        //     "$addFields": {
        //         "nombre_mestro_enlazado_1": {
        //             "$filter": {
        //                 "input": {
        //                     "$map": {
        //                         "input": { "$objectToArray": "$$ROOT" },
        //                         "as": "dataKV",
        //                         "in": {
        //                             "$cond": {
        //                                 "if": {
        //                                     "$eq": [{
        //                                         "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal_1"]
        //                                     }, "$nombre_maestro_principal_1"]
        //                                 },
        //                                 //"then": "$$dataKV.k",
        //                                 "then": {
        //                                     "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal_1",
        //                                         { "$strLenCP": "$$dataKV.k" }]
        //                                 },
        //                                 "else": ""
        //                             }
        //                         }
        //                     }
        //                 },
        //                 "as": "item",
        //                 "cond": { "$ne": ["$$item", ""] }
        //             }
        //         }
        //     }
        // }
        // , { "$addFields": { "nombre_mestro_enlazado_1": { "$arrayElemAt": ["$nombre_mestro_enlazado_1", 0] } } }
        // , { "$addFields": { "nombre_mestro_enlazado_1": { "$ifNull": ["$nombre_mestro_enlazado_1", ""] } } }


        // //--Valor Mestro enlazado
        // , {
        //     "$addFields": {
        //         "valor_mestro_enlazado_1": {
        //             "$filter": {
        //                 "input": {
        //                     "$map": {
        //                         "input": { "$objectToArray": "$$ROOT" },
        //                         "as": "dataKV",
        //                         "in": {
        //                             "$cond": {
        //                                 "if": {
        //                                     "$eq": [{
        //                                         "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal_1"]
        //                                     }, "$nombre_maestro_principal_1"]
        //                                 },
        //                                 // "then": "$$dataKV.v",
        //                                 //---ESTANDARIZAR TEXTO
        //                                 // "then": {"$toLower": "$$dataKV.v"},//minusculas
        //                                 // "then": {"$toUpper": "$$dataKV.v"},//MAYUSCULAS
        //                                 "then": { "$trim": { "input":{"$toUpper": "$$dataKV.v"}}},//TRIM
        //                                 "else": ""
        //                             }
        //                         }
        //                     }
        //                 },
        //                 "as": "item",
        //                 "cond": { "$ne": ["$$item", ""] }
        //             }
        //         }
        //     }
        // }
        // , { "$unwind": { "path": "$valor_mestro_enlazado_1", "preserveNullAndEmptyArrays": true } }
        // , { "$addFields": { "valor_mestro_enlazado_1": { "$ifNull": ["$valor_mestro_enlazado_1", ""] } } }

        // //---no mostrar  
        // , {
        //     "$project": {
        //         "nombre_maestro_principal_1": 0,
        //         "num_letras_nombre_maestro_principal_1": 0
        //     }
        // }
        //------//fin------------------plaga1



        // //-------------------ARREGLO 2020-06-10
        // , {
        //     "$addFields":
        //     {
        //         "aux_array_plaga": {
        //             "$filter": {
        //                 "input": [
        //                     {
        //                         "p": "$Plaga 1",
        //                         "c": "$cantidad plaga 1",
        //                         "e": "$Plaga 1_Estado Plagas LHPPA"
        //                     },
        //                     {
        //                         //"p": "$Plaga 2",
        //                         "p": { "$ifNull": ["$Plaga 2", ""] },
        //                         "c": "$cantidad Plagas 2",
        //                         "e": "$Plaga 2_Estado Plagas LHPPA"
        //                     },
        //                     {
        //                         // "p": "$Plagas 3",
        //                         "p": { "$ifNull": ["$Plagas 3", ""] },
        //                         "c": "$Cantidad Plaga 3",
        //                         "e": "$Plagas 3_Estado Plagas LHPPA"
        //                     },
        //                     {
        //                         // "p": "$plaga 4",
        //                         "p": { "$ifNull": ["$plaga 4", ""] },
        //                         "c": "$Cantidad plaga 4",
        //                         "e": "$plaga 4_Estado Plagas LHPPA"
        //                     },
        //                     {
        //                         // "p": "$Plagas 5",
        //                         "p": { "$ifNull": ["$Plagas 5", ""] },
        //                         "c": "$Cantidad Plaga 5",
        //                         "e": "$Plagas 5_Estado Plagas LHPPA"
        //                     }


        //                 ],
        //                 "as": "aux_array_plaga_ITEM",
        //                 "cond": {
        //                     "$ne": ["$$aux_array_plaga_ITEM.p", ""]
        //                 }
        //             }
        //         }


        //     }

        // }

        // , { "$unwind": "$aux_array_plaga" }

        // , { "$sort": { "aux_array_plaga.e": 1 } }

        //----old

        // , {
        //     "$addFields": {
        //         "aux_plaga_1": [{
        //             "p": "$Plaga 1",
        //             "c": "$cantidad plaga 1",
        //             "e": "$Plaga 1_Estado Plagas LHPPA"
        //         }
        //         ],
        //         "aux_plaga_2": [
        //             {
        //                 "p": "$Plaga 2",
        //                 "c": "$cantidad Plagas 2",
        //                 "e": "$Plaga 2_Estado Plagas LHPPA"
        //             }
        //         ],
        //         "aux_plaga_3": [
        //             {
        //                 "p": "$Plagas 3",
        //                 "c": "$Cantidad Plaga 3",
        //                 "e": "$Plagas 3_Estado Plagas LHPPA"
        //             }
        //         ],
        //         "aux_plaga_4": [
        //             {
        //                 "p": "$plaga 4",
        //                 "c": "$Cantidad plaga 4",
        //                 "e": "$plaga 4_Estado Plagas LHPPA"
        //             }
        //         ],
        //         "aux_plaga_5": [
        //             {
        //                 "p": "$Plagas 5",
        //                 "c": "$Cantidad Plaga 5",
        //                 "e": "$Plagas 5_Estado Plagas LHPPA"
        //             }
        //         ]

        //     }
        // },

        // {
        //     "$addFields": {
        //         "aux_array_plaga": {
        //             "$concatArrays": [
        //                 { "$filter": { "input": "$aux_plaga_1", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
        //                 { "$filter": { "input": "$aux_plaga_2", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
        //                 { "$filter": { "input": "$aux_plaga_3", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
        //                 { "$filter": { "input": "$aux_plaga_4", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
        //                 { "$filter": { "input": "$aux_plaga_5", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } }
        //             ]
        //         }
        //     }
        // },

        // { "$unwind": "$aux_array_plaga" }

        // , { "$sort": { "aux_array_plaga.e": 1 } }

        //-------------------


        // , {
        //     "$addFields": {

        //         "plaga_censo": "$aux_array_plaga.p",
        //         "plaga_estado": { "$ifNull": ["$aux_array_plaga.e", ""] },
        //         "plaga_cantidad": { "$ifNull": ["$aux_array_plaga.c", ""] },

        //         "huevos": {
        //             "$cond": {
        //                 "if": { "$eq": ["$aux_array_plaga.e", "HUEVO"] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         },
        //         "larvas": {
        //             "$cond": {
        //                 "if": { "$eq": ["$aux_array_plaga.e", "LARVA"] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         },
        //         "otros_estados": {
        //             "$cond": {
        //                 "if": { "$and": [{ "$ne": ["$aux_array_plaga.e", "LARVA"] }, { "$ne": ["$aux_array_plaga.e", "HUEVO"] }] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "suma_huevos_larvas": {
        //             "$sum": ["$huevos", "$larvas"]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": {
        //             "$divide": ["$suma_huevos_larvas", "$plantas_dif_censadas_x_lote"]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador_huevo_larva", 100] }, { "$mod": [{ "$multiply": ["$indicador_huevo_larva", 100] }, 1] }] }, 100] }
        //     }
        // }

        // , {
        //     "$project": {

        //         "finca": "$finca",
        //         "bloque": "$bloque",
        //         "lote": "$lote",
        //         "linea": "$linea",
        //         "planta": "$planta",

        //         "PLAGA": "$plaga_censo",
        //         "estado": "$plaga_estado",
        //         "cantidad": "$plaga_cantidad",
        //         "Fecha de Registro": "$Fecha de Registro",


        //         "huevos": "$huevos",
        //         "larvas": "$larvas",
        //         "otros_estados": "$otros_estados",
        //         "suma_huevos_larvas": "$suma_huevos_larvas",
        //         "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
        //         "indicador_huevo_larva": "$indicador_huevo_larva",

        //         "num_Linea": "$Linea",
        //         "num_Palma": "$Palma",

        //         "Hoja": "$Hoja",

        //         "Observaciones": "$Observaciones",
        //         "supervisor": "$supervisor",
        //         "capture": "$capture",


        //         "Point": "$Point",
        //         "rgDate": "$rgDate"
        //     }
        // }

        // , {
        //     "$lookup": {
        //         "from": "form_cargueinformaciondeplantas",
        //         "as": "referencia_siembras",
        //         "let": {
        //             "nombre_lote": "$lote"
        //         },
        //         "pipeline": [
        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
        //                             { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
        //                         ]
        //                     }
        //                 }
        //             },

        //             {
        //                 "$sort": {
        //                     "rgDate": -1
        //                 }
        //             },
        //             {
        //                 "$limit": 1
        //             }

        //         ]
        //     }
        // },

        // {
        //     "$unwind": {
        //         "path": "$referencia_siembras",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // },



        // {
        //     "$addFields": {
        //         "Hectareas": "$referencia_siembras.HECTAREAS",
        //         "Siembra": "$referencia_siembras.SIEMBRA",
        //         "num_palmas": "$referencia_siembras.PALMAS",
        //         "Material": "$referencia_siembras.MATERIAL"
        //     }
        // },

        // {
        //     "$project": {
        //         "referencia_siembras": 0
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "anio": { "$year": "$rgDate" },
        //         "num_mes": { "$month": "$rgDate" },
        //         "semana": { "$week": "$rgDate" }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "mes": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
        //                     { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
        //                     { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
        //                     { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
        //                     { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
        //                     { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
        //                     { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
        //                     { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
        //                     { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
        //                     { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
        //                     { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
        //                     { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
        //                 ],
        //                 "default": "Mes desconocido"
        //             }
        //         }
        //     }
        // }



    ], { "allowDiskUse": true })