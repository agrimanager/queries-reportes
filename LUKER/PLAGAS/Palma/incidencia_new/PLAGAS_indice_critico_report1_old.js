//---indice critico
db.form_modulodeplagas.aggregate(
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },
        {
            "$addFields": {
                "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "aux_plaga_1": [{
                    "p": "$Plaga 1",
                    "c": "$cantidad plaga 1",
                    "e": "$Plaga 1_Estado Plagas LHPPA"
                }
                ],
                "aux_plaga_2": [
                    {
                        "p": "$Plaga 2",
                        "c": "$cantidad Plagas 2",
                        "e": "$Plaga 2_Estado Plagas LHPPA"
                    }
                ],
                "aux_plaga_3": [
                    {
                        "p": "$Plagas 3",
                        "c": "$Cantidad Plaga 3",
                        "e": "$Plagas 3_Estado Plagas LHPPA"
                    }
                ],
                "aux_plaga_4": [
                    {
                        "p": "$plaga 4",
                        "c": "$Cantidad plaga 4",
                        "e": "$plaga 4_Estado Plagas LHPPA"
                    }
                ],
                "aux_plaga_5": [
                    {
                        "p": "$Plagas 5",
                        "c": "$Cantidad Plaga 5",
                        "e": "$Plagas 5_Estado Plagas LHPPA"
                    }
                ]

            }
        },

        {
            "$addFields": {
                "aux_array_plaga": {
                    "$concatArrays": [
                        { "$filter": { "input": "$aux_plaga_1", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
                        { "$filter": { "input": "$aux_plaga_2", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
                        { "$filter": { "input": "$aux_plaga_3", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
                        { "$filter": { "input": "$aux_plaga_4", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } },
                        { "$filter": { "input": "$aux_plaga_5", "as": "item", "cond": { "$and": [{ "$ne": ["$$item.p", ""] }, { "$ne": ["$$item.e", ""] }] } } }
                    ]
                }
            }
        },

        { "$unwind": "$aux_array_plaga" }

        , { "$sort": { "aux_array_plaga.e": 1 } }


        , {
            "$addFields": {

                "plaga_censo": "$aux_array_plaga.p",
                "plaga_estado": { "$ifNull": ["$aux_array_plaga.e", ""] },
                "plaga_cantidad": { "$ifNull": ["$aux_array_plaga.c", ""] },

                "huevos": {
                    "$cond": {
                        "if": { "$eq": ["$aux_array_plaga.e", "HUEVO"] },
                        "then": "$aux_array_plaga.c",
                        "else": 0
                    }
                },
                "larvas": {
                    "$cond": {
                        "if": { "$eq": ["$aux_array_plaga.e", "LARVA"] },
                        "then": "$aux_array_plaga.c",
                        "else": 0
                    }
                },
                "otros_estados": {
                    "$cond": {
                        "if": { "$and": [{ "$ne": ["$aux_array_plaga.e", "LARVA"] }, { "$ne": ["$aux_array_plaga.e", "HUEVO"] }] },
                        "then": "$aux_array_plaga.c",
                        "else": 0
                    }
                }
            }
        }


        , {
            "$addFields": {
                "suma_huevos_larvas": {
                    "$sum": ["$huevos", "$larvas"]
                }
            }
        }

        , {
            "$addFields": {
                "indicador_huevo_larva": {
                    "$divide": ["$suma_huevos_larvas", "$plantas_dif_censadas_x_lote"]
                }
            }
        }

        , {
            "$addFields": {
                "indicador_huevo_larva": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador_huevo_larva", 100] }, { "$mod": [{ "$multiply": ["$indicador_huevo_larva", 100] }, 1] }] }, 100] }
            }
        }

        , {
            "$project": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "Linea": "$Linea",
                "Palma": "$Palma",
                "linea": "$linea",
                "planta": "$planta",

                "PLAGA": "$plaga_censo",
                "estado": "$plaga_estado",
                "cantidad": "$plaga_cantidad",
                "Fecha de Registro": "$Fecha de Registro",


                "huevos": "$huevos",
                "larvas": "$larvas",
                "otros_estados": "$otros_estados",
                "suma_huevos_larvas": "$suma_huevos_larvas",
                "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
                "indicador_huevo_larva": "$indicador_huevo_larva",

                "num_Linea": "$Linea",
                "num_Palma": "$Palma",

                "Hoja": "$Hoja",

                "Observaciones": "$Observaciones",
                "supervisor": "$supervisor",
                "capture": "$capture",


                "Point": "$Point",
                "rgDate2": "$rgDate",
                "rgDate": "$rgDate"
            }
        }

        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "PALMA"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }


        , {
            "$addFields": {
                "anio": { "$year": "$rgDate" },
                "num_mes": { "$month": "$rgDate" },
                "semana": { "$week": "$rgDate" }
            }
        }

        , {
            "$addFields": {
                "mes": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }

    ]
)