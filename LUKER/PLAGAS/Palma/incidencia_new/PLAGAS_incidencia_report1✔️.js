[
    {
        "$project": {
            "rgDate día":0,
            "rgDate mes": 0,
            "rgDate año": 0,
            "rgDate hora": 0,
            
            "uDate día":0,
            "uDate mes": 0,
            "uDate año": 0,
            "uDate hora": 0
        }
    },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },
    {
        "$addFields": {
            "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
        }
    },
    {
        "$addFields": {
            "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "Formula": 0,
            "Point": 0,
            "uid": 0,
            "Lote": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "planta": "$planta"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }

    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "PALMA"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },

                { "$sort": { "rgDate": -1 } },
                { "$limit": 1 }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": false
        }
    },



    {
        "$addFields": {
            "Hectareas": "$referencia_siembras.HECTAREAS",
            "Siembra": "$referencia_siembras.SIEMBRA",
            "num_palmas": "$referencia_siembras.PALMAS",
            "Material": "$referencia_siembras.MATERIAL"
        }
    },

    {
        "$project": {
            "referencia_siembras": 0
        }
    }

    , {
        "$addFields": {
            "anio": { "$year": "$rgDate" },
            "num_mes": { "$month": "$rgDate" },
            "semana": { "$week": "$rgDate" }
        }
    }

    , {
        "$addFields": {
            "mes": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas": [
                {
                    "nombre_variable": "Plaga 1_",
                    "plaga": { "$ifNull": ["$Plaga 1", ""] },
                    "cantidad": { "$ifNull": ["$cantidad plaga 1", 0] }
                },
                {
                    "nombre_variable": "Plaga 2_",
                    "plaga": { "$ifNull": ["$Plaga 2", ""] },
                    "cantidad": { "$ifNull": ["$cantidad Plagas 2", 0] }
                },
                {
                    "nombre_variable": "Plagas 3_",
                    "plaga": { "$ifNull": ["$Plagas 3", ""] },
                    "cantidad": { "$ifNull": ["$Cantidad Plaga 3", 0] }
                },
                {
                    "nombre_variable": "plaga 4_",
                    "plaga": { "$ifNull": ["$plaga 4", ""] },
                    "cantidad": { "$ifNull": ["$Cantidad plaga 4", 0] }
                },
                {
                    "nombre_variable": "Plagas 5_",
                    "plaga": { "$ifNull": ["$Plagas 5", ""] },
                    "cantidad": { "$ifNull": ["$Cantidad Plaga 5", 0] }
                }
            ]
        }
    }


    , {
        "$addFields": {
            "array_plagas": {
                "$filter": {
                    "input": "$array_plagas",
                    "as": "item_aux_array_plaga",
                    "cond": {
                        "$ne": ["$$item_aux_array_plaga.plaga", ""]
                    }
                }
            }
        }
    }

    , { "$match": { "array_plagas": { "$ne": [] } } }


    , { "$unwind": "$array_plagas" }


    , { "$addFields": { "nombre_maestro_principal": "$array_plagas.nombre_variable" } }

    , { "$addFields": { "num_letras_nombre_maestro_principal": { "$strLenCP": "$nombre_maestro_principal" } } }

    , {
        "$addFields": {
            "nombre_maestro_enlazado": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , { "$addFields": { "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] } } }
    , { "$addFields": { "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] } } }


    , {
        "$addFields": {
            "valor_maestro_enlazado": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": { "$trim": { "input": { "$toUpper": "$$dataKV.v" } } },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }
    , { "$unwind": { "path": "$valor_maestro_enlazado", "preserveNullAndEmptyArrays": true } }
    , { "$addFields": { "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] } } }

    , {
        "$project": {
            "nombre_maestro_principal": 0,
            "num_letras_nombre_maestro_principal": 0
        }
    }

    , {
        "$project": {

            "finca": "$finca",
            "bloque": "$bloque",
            "lote": "$lote",
            "linea": "$Linea",
            "linea_aux": "$linea",
            "palma": "$Palma",
            "palma_aux": "$planta",
            "Hoja": "$Hoja",

            "PLAGA": "$array_plagas.plaga",
            "cantidad": "$array_plagas.cantidad",
            "grupo": "$nombre_maestro_enlazado",
            "estado": "$valor_maestro_enlazado",

            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",

            "Hectareas": "$Hectareas",
            "Siembra": "$Siembra",
            "num_palmas": "$num_palmas",
            "Material": "$Material",

            "Observaciones": "$Observaciones",
            "supervisor": "$supervisor",
            "capture": "$capture",

            "Fecha de Registro": "$Fecha de Registro",
            "rgDate": "$rgDate",
            "anio": "$anio",
            "mes": "$mes",
            "semana": "$semana"

        }
    }




]