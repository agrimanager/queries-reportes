//---incidencia
db.form_modulodeplagas.aggregate(
    [
        //===TEST
        // {$match:{"Plaga 1":{$eq:""}}},
        // {$match:{"Plaga 1":{$ne:""}}},
        //{ $sort: { rgDate: -1 } },
        // {$match: {"Plaga 1_Estado Plagas LHPPA":{$exists: true} }},
        {
            $match: {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2000-05-01T01:21:23.000-05:00") } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": ISODate("2020-07-01T01:21:23.000-05:00") } } }
                            ]
                        }
                    ]
                }
            }
        },
        //---------------------------------


        //-----!!!DANGER (desproyectar fechas)
        //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
        {
            "$project": {
                "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },




        //===CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },
        {
            "$addFields": {
                "linea": { "$concat": ["$lote", "-", { "$toString": "$Linea" }] }
            }
        },
        {
            "$addFields": {
                "planta": { "$concat": ["$linea", "-", { "$toString": "$Palma" }] }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                //----no mostrar otros datos 
                "Formula": 0,
                "Point": 0,
                "uid": 0,
                "Lote": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,
                // "XXXXXX": 0,

            }
        }

        //===plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        //===info lote
        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "PALMA"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    { "$sort": { "rgDate": -1 } },
                    { "$limit": 1 }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }


        //===fechas
        , {
            "$addFields": {
                "anio": { "$year": "$rgDate" },
                "num_mes": { "$month": "$rgDate" },
                "semana": { "$week": "$rgDate" }
            }
        }

        , {
            "$addFields": {
                "mes": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }

        //===PLAGAS Y ESTADOS

        //----PLAGAS Y ESTADOS
        //antes el formulario tenia 6 campos (p1,p2,p3..,p6)
        //ya solo hay 1 campo plaga1 (p1)

        //array
        , {
            "$addFields": {
                "array_plagas": [
                    {
                        "nombre_variable": "Plaga 1_",
                        "plaga": { "$ifNull": ["$Plaga 1", ""] },
                        "cantidad": { "$ifNull": ["$cantidad plaga 1", 0] },
                    },
                    {
                        "nombre_variable": "Plaga 2_",
                        "plaga": { "$ifNull": ["$Plaga 2", ""] },
                        "cantidad": { "$ifNull": ["$cantidad Plagas 2", 0] },
                    },
                    {
                        "nombre_variable": "Plagas 3_",
                        "plaga": { "$ifNull": ["$Plagas 3", ""] },
                        "cantidad": { "$ifNull": ["$Cantidad Plaga 3", 0] },
                    },
                    {
                        "nombre_variable": "plaga 4_",
                        "plaga": { "$ifNull": ["$plaga 4", ""] },
                        "cantidad": { "$ifNull": ["$Cantidad plaga 4", 0] },
                    },
                    {
                        "nombre_variable": "Plagas 5_",
                        "plaga": { "$ifNull": ["$Plagas 5", ""] },
                        "cantidad": { "$ifNull": ["$Cantidad Plaga 5", 0] },
                    }


                ]
            }
        }


        , {
            "$addFields": {
                "array_plagas": {
                    "$filter": {
                        "input": "$array_plagas",
                        "as": "item_aux_array_plaga",
                        "cond": {
                            "$ne": ["$$item_aux_array_plaga.plaga", ""]
                        }
                    }
                }
            }
        }


        //--condicion para array_plagas_filter = []
        , { "$match": { "array_plagas": { "$ne": [] } } }


        , { "$unwind": "$array_plagas" }





        //===MAESTRO ASOCIADO PLAGA

        //--Maestro principal
        // , { "$addFields": { "nombre_maestro_principal": "Plaga 1_" } }
        , { "$addFields": { "nombre_maestro_principal": "$array_plagas.nombre_variable" } }

        , { "$addFields": { "num_letras_nombre_maestro_principal": { "$strLenCP": "$nombre_maestro_principal" } } }

        //--Mestro enlazado
        , {
            "$addFields": {
                "nombre_maestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        //"then": "$$dataKV.k",
                                        "then": {
                                            "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                { "$strLenCP": "$$dataKV.k" }]
                                        },
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , { "$addFields": { "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] } } }
        , { "$addFields": { "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] } } }


        //--Valor Mestro enlazado
        , {
            "$addFields": {
                "valor_maestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        // "then": "$$dataKV.v",
                                        //---ESTANDARIZAR TEXTO
                                        "then": { "$trim": { "input": { "$toUpper": "$$dataKV.v" } } },//TRIM MAYUS
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , { "$unwind": { "path": "$valor_maestro_enlazado", "preserveNullAndEmptyArrays": true } }
        , { "$addFields": { "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] } } }

        //---no mostrar  
        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0
            }
        }

        //===DATOS FINALES DE REPORTE

        , {
            "$project": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$Linea",
                "linea_aux": "$linea",
                "palma": "$Palma",
                "palma_aux": "$planta",
                "Hoja": "$Hoja",

                "PLAGA": "$array_plagas.plaga",
                "cantidad": "$array_plagas.cantidad",
                "grupo": "$nombre_maestro_enlazado",
                "estado": "$valor_maestro_enlazado",

                "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",

                "Hectareas": "$Hectareas",
                "Siembra": "$Siembra",
                "num_palmas": "$num_palmas",
                "Material": "$Material",

                "Observaciones": "$Observaciones",
                "supervisor": "$supervisor",
                "capture": "$capture",

                "Fecha de Registro": "$Fecha de Registro",
                "rgDate": "$rgDate",
                "anio": "$anio",
                "mes": "$mes",
                "semana": "$semana",


                // "huevos": "$huevos",
                // "larvas": "$larvas",
                // "otros_estados": "$otros_estados",
                // "suma_huevos_larvas": "$suma_huevos_larvas",
                // "indicador_huevo_larva": "$indicador_huevo_larva",
            }
        }






        //===CALCULOS REPORTE


        // //----new
        // , {
        //     "$addFields": {

        //         // "plaga_censo": "$aux_array_plaga.p",
        //         // "plaga_estado": { "$ifNull": ["$aux_array_plaga.e", ""] },
        //         // "plaga_cantidad": { "$ifNull": ["$aux_array_plaga.c", ""] },

        //         "huevos": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado", "HUEVO"] },
        //                 "then": "$cantidad",
        //                 "else": 0
        //             }
        //         },
        //         "larvas": {
        //             "$cond": {
        //                 "if": { "$eq": ["$estado", "LARVA"] },
        //                 "then": "$cantidad",
        //                 "else": 0
        //             }
        //         },
        //         "otros_estados": {
        //             "$cond": {
        //                 "if": { "$and": [{ "$ne": ["$estado", "LARVA"] }, { "$ne": ["$estado", "HUEVO"] }] },
        //                 "then": "$cantidad",
        //                 "else": 0
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "suma_huevos_larvas": {
        //             "$sum": ["$huevos", "$larvas"]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": {
        //             "$divide": ["$suma_huevos_larvas", "$plantas_dif_censadas_x_lote"]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador_huevo_larva", 100] }, { "$mod": [{ "$multiply": ["$indicador_huevo_larva", 100] }, 1] }] }, 100] }
        //     }
        // }




        //------old
        // , {
        //     "$addFields": {

        //         "plaga_censo": "$aux_array_plaga.p",
        //         "plaga_estado": { "$ifNull": ["$aux_array_plaga.e", ""] },
        //         "plaga_cantidad": { "$ifNull": ["$aux_array_plaga.c", ""] },

        //         "huevos": {
        //             "$cond": {
        //                 "if": { "$eq": ["$aux_array_plaga.e", "HUEVO"] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         },
        //         "larvas": {
        //             "$cond": {
        //                 "if": { "$eq": ["$aux_array_plaga.e", "LARVA"] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         },
        //         "otros_estados": {
        //             "$cond": {
        //                 "if": { "$and": [{ "$ne": ["$aux_array_plaga.e", "LARVA"] }, { "$ne": ["$aux_array_plaga.e", "HUEVO"] }] },
        //                 "then": "$aux_array_plaga.c",
        //                 "else": 0
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "suma_huevos_larvas": {
        //             "$sum": ["$huevos", "$larvas"]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": {
        //             "$divide": ["$suma_huevos_larvas", "plantas_dif_censadas_x_lote"]
        //         }
        // }

        // , {
        //     "$addFields": {
        //         "indicador_huevo_larva": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador_huevo_larva", 100] }, { "$mod": [{ "$multiply": ["$indicador_huevo_larva", 100] }, 1] }] }, 100] }
        //     }
        // }



        //----CONDICION indice_critico
        , {
            "$match": {
                "$expr": {
                    "$or": [
                        { "$eq": ["$estado", "LARVA"] },
                        { "$eq": ["$estado", "HUEVO"] }
                    ]
                }

            }
        }



    ], { "allowDiskUse": true })//---incidencia
