db.users.aggregate(

    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //------------------------------------------------------------------


        {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores_x_empleado",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                "pipeline": [

                    //----filtro de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            //--fecha_data
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            //--fecha_data
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    { "$match": { "farm": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "_id_str_farm": { "$toString": "$farm" }
                        }
                    }

                    //--fincas y cultivo
                    //-------------------------
                    //VN
                    , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }
                    
                    ,{
                        "$addFields": {
                            "total_empleados_seleccionados_labor": { "$size": "$employees" },
                            "total_productos_seleccionados_labor": { "$size": "$supplies" }
                        }
                    },


                ]

            }
        }



        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data_labores_x_empleado"
                            , []
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }
        
        
        ,{
            $match:{
                // "planningEmployees" : 3
                "total_productos_seleccionados_labor" : {$ne:0},
                "total_empleados_seleccionados_labor" : {$ne:0}
            }
        }

// ,{
//     $sort:{
//         "planningEmployees":-1
//     }
// }

    ]
)