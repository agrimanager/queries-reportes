db.users.aggregate(


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2020-04-01T19:00:00.000-05:00"),
            "Busqueda fin": new Date,
            "today": new Date
        }
    },
    //------------------------------------------------------------------


    {
        "$lookup": {
            "from": "tasks",
            "as": "data_labores",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
            },
            "pipeline": [
                //----filtro de fechas
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        //--fecha_data
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        //--fecha_data
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": { "$max": "$when.start" } } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },


                //query
                //.....


            ]

        }
    }


    , {
        "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data_labores"
                        , []
                    ]
                }
            }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }
    
    
    //...projectar rgDate para filtro de API



)