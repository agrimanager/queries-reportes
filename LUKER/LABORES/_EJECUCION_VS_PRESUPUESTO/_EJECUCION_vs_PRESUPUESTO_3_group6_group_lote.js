db.users.aggregate(
    [

        { "$project": { "_id": "$_id" } }

        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores_x_empleado",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$$id_usr", "$uid"]
                            }
                        }
                    },

                    
                    { "$addFields": { "anio_filtro": { "$year": { "$max": "$when.start" } } } },
                    { "$match": { "anio_filtro": { "$eq": 2020 } } },

                    { "$match": { "farm": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "_id_str_farm": { "$toString": "$farm" }
                        }
                    }

                    , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }

                    , {
                        "$addFields": {
                            "cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                                    "then": "PALMA",
                                    "else": "CACAO"
                                }
                            }
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },

                    {
                        "$lookup": {
                            "from": "costsCenters",
                            "localField": "ccid",
                            "foreignField": "_id",
                            "as": "costsCenter"
                        }
                    },
                    { "$unwind": "$costsCenter" },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm",
                            "foreignField": "_id",
                            "as": "farm"
                        }
                    },
                    { "$unwind": "$farm" },

                    {
                        "$addFields": {
                            "employees": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{ "$type": "$employees" }, "array"]
                                    },
                                    "then": "$employees",
                                    "else": {
                                        "$map": {
                                            "input": { "$objectToArray": "$employees" },
                                            "as": "employeeKV",
                                            "in": "$$employeeKV.v"
                                        }
                                    }
                                }
                            },
                            "productivityReport": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{ "$type": "$productivityReport" }, "array"]
                                    },
                                    "then": "$productivityReport",
                                    "else": {
                                        "$map": {
                                            "input": { "$objectToArray": "$productivityReport" },
                                            "as": "productivityReportKV",
                                            "in": "$$productivityReportKV.v"
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$employees",
                            "includeArrayIndex": "arrayIndex",
                            "preserveNullAndEmptyArrays": false
                        }
                    },
                    {
                        "$addFields": {
                            "employees": {
                                "$toObjectId": "$employees"
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "productivityReport": {
                                "$map": {
                                    "input": "$productivityReport",
                                    "as": "item",
                                    "in": {
                                        "$mergeObjects": [
                                            "$$item",
                                            {
                                                "employee": { "$toObjectId": "$$item.employee" }
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "productivityReport": {
                                "$filter": {
                                    "input": "$productivityReport",
                                    "as": "item",
                                    "cond": {
                                        "$eq": ["$$item.employee", "$employees"]
                                    }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$productivityReport",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$addFields": {
                            "ProductividadEmpleado": { "$toDouble": "$productivityReport.quantity" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "employees",
                            "localField": "employees",
                            "foreignField": "_id",
                            "as": "Empleado"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$Empleado",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$lookup": {
                            "from": "companies",
                            "localField": "Empleado.cid",
                            "foreignField": "_id",
                            "as": "Empresa"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$Empresa",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields":
                            {
                                "TotalPagoEmpleado": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$ProductividadEmpleado" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }
                                    ]
                                }
                            }
                    },


                    {
                        "$addFields": {

                            "Cartografia_seleccionada_mapa_tipos": {
                                "$reduce": {
                                    "input": {
                                        "$reduce": {
                                            "input": "$cartography.features.properties.type",
                                            "initialValue": [],
                                            "in": {
                                                "$cond": {
                                                    "if": { "$lt": [{ "$indexOfArray": ["$$value", "$$this"] }, 0] },
                                                    "then": { "$concatArrays": ["$$value", ["$$this"]] },
                                                    "else": "$$value"
                                                }
                                            }
                                        }
                                    },
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },

                            "Cartografia_seleccionada_mapa_elementos": {
                                "$reduce": {
                                    "input": "$cartography.features.properties.name",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                                        }
                                    }
                                }
                            },


                            "Cartografia_seleccionada_lista_lotes_y_estados": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lots",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin lotes seleccionados--"]
                            },

                            "lista_lots": {
                                "$map": {
                                    "input": "$lots",
                                    "as": "lot",
                                    "in": {
                                        "$split": ["$$lot", ", "]
                                    }
                                }
                            }


                        }
                    },

                    {
                        "$addFields": {
                            "lista_lotes": {
                                "$map": {
                                    "input": "$lista_lots",
                                    "as": "lot",
                                    "in": {
                                        "$arrayElemAt": ["$$lot", 0]
                                    }
                                }
                            },
                            "lista_estados": {
                                "$map": {
                                    "input": "$lista_lots",
                                    "as": "lot",
                                    "in": {
                                        "$arrayElemAt": ["$$lot", 1]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "lista_lotes": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lista_lotes",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin lotes seleccionados--"]
                            },
                            "lista_estados": {
                                "$ifNull": [{
                                    "$reduce": {
                                        "input": "$lista_estados",
                                        "initialValue": "",
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                                "then": "$$this",
                                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                                            }
                                        }
                                    }
                                }, "--sin estados seleccionados--"]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "form_costodeherramientasporactividad",
                            "as": "CostoActividadHerramienta",
                            "let": {
                                "actividad": "$activity.name"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Actividad", "$$actividad"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$limit": 1
                                }

                            ]
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$CostoActividadHerramienta",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$lookup": {
                            "from": "form_cargueinformaciondeplantas",
                            "as": "referencia_siembras",
                            "let": {
                                "nombre_lote_lista": "$lista_lotes",
                                "nombre_lote_cartografia": "$Cartografia_seleccionada_mapa_elementos",
                                "cultivo": "$cultivo"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                                { "$eq": ["$CULTIVO", "$$cultivo"] },
                                                {
                                                    "$or": [
                                                        { "$in": ["$$nombre_lote_lista", "$LOTE.features.properties.name"] },
                                                        { "$in": ["$$nombre_lote_cartografia", "$LOTE.features.properties.name"] }
                                                    ]
                                                }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$limit": 1
                                }

                            ]
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$referencia_siembras",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    {
                        "$project": {
                            "_id": 0,
                            "Actividad": "$activity.name",
                            "Costo de Herramienta": { "$ifNull": ["$CostoActividadHerramienta.Costo de herramienta", 0] },

                            "Codigo Labor": "$cod",
                            "Estado Labor": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$status", "To do"] }, "then": "⏰ Por hacer" },
                                        { "case": { "$eq": ["$status", "Doing"] }, "then": "💪 En progreso" },
                                        { "case": { "$eq": ["$status", "Done"] }, "then": "✔ Listo" }
                                    ]
                                }
                            },
                            "Finca": "$farm.name",
                            
                            "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                            
                            "Lista lotes": "$lista_lotes",
                            "Siembra": { "$ifNull": [{ "$toString": "$referencia_siembras.SIEMBRA" }, "0"] },
                            "Cod_Lote": { "$ifNull": ["$referencia_siembras.CODIGO LOTE", ""] },


                            "Centro de costos": "$costsCenter.name",
                          
                            "[Unidad] Medida de Actividad": "$productivityPrice.measure",
                            "($) Precio x unidad de Actividad": "$productivityPrice.price",

                            

                            "Fecha inicio": { "$max": "$when.start" },
                            "Fecha fin": { "$max": "$when.finish" },
                          

                            "Nombre de Empleado": { "$ifNull": [{ "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"] }, "--sin empleados--"] },
                            "Identificacion": { "$ifNull": ["$Empleado.numberID", "--"] },
                            "Codigo de Empleado": { "$ifNull": ["$Empleado.code", "--"] },
                            "Empresa": "$Empresa.name",
                            "(#) Productividad de Empleado": { "$ifNull": ["$ProductividadEmpleado", 0] },
                            "($) Total Pago Empleado": "$TotalPagoEmpleado",

                            "cultivo": "$cultivo"


                        }
                    },


                    {
                        "$project": {
                            "Finca": "$Finca",
                            "Lote": {
                                "$cond": {
                                    "if": { "$ne": ["$elementos Cartografia", ""] },
                                    "then": "$elementos Cartografia",
                                    "else": {
                                        "$cond": {
                                            "if": { "$ne": ["$Lista lotes", "[]"] },
                                            "then": "$Lista lotes",
                                            "else": "--sin cartografia--"
                                        }
                                    }
                                }
                            },
                            "Cod_Lote": "$Cod_Lote",

                            "Siembra": "$Siembra",
                            "cultivo": "$cultivo",

                            "Centro de costos": "$Centro de costos",
                            "Nombre Labor": "$Actividad",
                            "Codigo Labor": "$Codigo Labor",
                            "Estado Labor": "$Estado Labor",

                            "Fecha inicio": "$Fecha inicio",
                            "Fecha fin": "$Fecha fin",

                            "Codigo empleado": "$Codigo de Empleado",
                            "Nombre empleado": "$Nombre de Empleado",
                            "Cedula": "$Identificacion",
                            "Empresa": "$Empresa",

                            "Unidad": "$[Unidad] Medida de Actividad",
                            "Cantidad": "$(#) Productividad de Empleado",
                            "vr unit trabajador": "$($) Precio x unidad de Actividad",
                            "vr unit herramienta": "$Costo de Herramienta"
                        }
                    },

                    {
                        "$addFields":
                            {
                                "vr Total trabajador": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$Cantidad" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$vr unit trabajador" }, 0] }
                                    ]
                                },

                                "vr Total herramienta": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$Cantidad" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$vr unit herramienta" }, 0] }
                                    ]
                                }
                            }
                    },



                    {
                        "$addFields":
                            {
                                "TOTAL PAGO": {
                                    "$sum": [
                                        { "$ifNull": [{ "$toDouble": "$vr Total trabajador" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$vr Total herramienta" }, 0] }
                                    ]
                                },
                                "rgDate": "$Fecha inicio"
                            }
                    },

                    
                    {
                        "$lookup": {
                            "from": "form_configuracioncentrosdecostospresupuesto",
                            "localField": "Centro de costos",
                            "foreignField": "Ceco Agrimanager",
                            "as": "centro costos presupuesto"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$centro costos presupuesto",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields":
                            {
                                "Anio": { "$year": "$Fecha inicio" },
                                "Mes": { "$month": "$Fecha inicio" },
                                "Semana": { "$week": "$Fecha inicio" },

                                "centro costos presupuesto": "$centro costos presupuesto.Ceco Presupuesto"
                            }
                    }


                    , {
                        "$lookup": {
                            "from": "form_centrosdecostospresupuesto",
                            "localField": "centro costos presupuesto",
                            "foreignField": "Ceco Presupuesto",
                            "as": "aux_form_centrosdecostospresupuesto"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$aux_form_centrosdecostospresupuesto",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields": {
                            "Cuenta Contable Mano de Obra": { "$ifNull": ["$aux_form_centrosdecostospresupuesto.Cuenta Contable Mano de Obra", ""] },
                            "Cuenta Contable Insumo": { "$ifNull": ["$aux_form_centrosdecostospresupuesto.Cuenta Contable Insumo", ""] }
                        }
                    },
                    { "$project": { "aux_form_centrosdecostospresupuesto": 0 } }



                ]

            }
        }

        , {
            "$lookup": {
                "from": "form_recolecciondecosechaxempleados",
                "as": "data_liquidacion_cosecha",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$$id_usr", "$uid"]
                            }
                        }
                    },

                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
                    { "$match": { "anio_filtro": { "$eq": 2020 } } },


                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "lote": "$lote.properties.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },

                    { "$unwind": "$Finca" },

                    {
                        "$addFields": {
                            "_id_str_farm": { "$toString": "$Finca._id" }
                        }
                    }

                    , {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    }

                    , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }

                    , {
                        "$addFields": {
                            "cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                                    "then": "PALMA",
                                    "else": "CACAO"
                                }
                            }
                        }
                    },




                    {
                        "$lookup": {
                            "from": "form_pesospromedios",
                            "as": "Peso promedio lote",
                            "let": {
                                "nombre_lote": "$lote",
                                "anio_registro": { "$year": "$Fecha de puesta en caja" },
                                "mes_registro": { "$month": "$Fecha de puesta en caja" }
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] },
                                                { "$eq": ["$$anio_registro", "$Anio"] },
                                                { "$eq": ["$$mes_registro", "$Mes"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$Peso promedio lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$addFields": {
                            "Peso promedio lote": {
                                "$ifNull": ["$Peso promedio lote.Peso Promedio", 0]
                            }
                        }
                    },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "empleados_cosecha_adicionales": {
                                    "$filter": {
                                        "input": [
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 1", ""] },
                                                "reference": "$Empleado adicional 1 opcion",
                                                "value": "$Empleado adicional 1 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 1", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 2", ""] },
                                                "reference": "$Empleado adicional 2 opcion",
                                                "value": "$Empleado adicional 2 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 2", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 3", ""] },
                                                "reference": "$empleado Adicional opcion3",
                                                "value": "$Empleado adicional 3 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 3", " -"] }, 0] }
                                            }


                                        ],
                                        "as": "empleados_cosecha_adicionales",
                                        "cond": {
                                            "$ne": ["$$empleados_cosecha_adicionales.name", ""]
                                        }
                                    }
                                }


                            }

                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleados_cosecha_adicionales.code",
                            "foreignField": "code",
                            "as": "info_empleado_adicional"
                        }
                    }

                    , {
                        "$addFields": {
                            "Empleados de Cosecha": {
                                "$concatArrays": [
                                    "$Empleados de Cosecha",
                                    {
                                        "$map": {
                                            "input": "$empleados_cosecha_adicionales",
                                            "as": "empleado_cosecha_adicional",
                                            "in": {
                                                "$mergeObjects": [
                                                    "$$empleado_cosecha_adicional",
                                                    {
                                                        "_id": {
                                                            "$reduce": {
                                                                "input": "$info_empleado_adicional",
                                                                "initialValue": null,
                                                                "in": {
                                                                    "$cond": {
                                                                        "if": {
                                                                            "$eq": [
                                                                                "$$empleado_cosecha_adicional.code",
                                                                                "$$this.code"
                                                                            ]
                                                                        },
                                                                        "then": {
                                                                            "$toString": "$$this._id"
                                                                        },
                                                                        "else": "$$value"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Total Alzados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Alzador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "Total Alzados": {
                                "$reduce": {
                                    "input": "$Total Alzados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields":
                            {
                                "Peso aproximado Alzados": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$Total Alzados" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                                    ]
                                }
                            }
                    }

                    , {
                        "$lookup": {
                            "from": "form_despachodecosecha",
                            "as": "form_despacho_cosecha",
                            "let": {
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "fecha": "$Fecha de puesta en caja"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Codigo Vagon Asociado a Despacho", "$$vagon"] },
                                                { "$eq": ["$Numero de Viaje de Vagon", "$$num_viaje_vagon"] },
                                                {
                                                    "$gte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de Llenado" } } }
                                                    ]
                                                },

                                                {
                                                    "$lte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de llenado" } } }
                                                    ]
                                                }


                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": 1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$form_despacho_cosecha",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    {
                        "$addFields":
                            {
                                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso total segun ticket" }, -1] },
                                "Numero de ticket": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Numero de Ticket" }, -1] }
                            }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "ticket": "$Numero de ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_lote_viaje": {
                                "$sum": "$Peso aproximado Alzados"
                            },
                            "total_alzados_lote": {
                                "$sum": "$Total Alzados"
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "vagon": "$_id.vagon",
                                "num_viaje_vagon": "$_id.num_viaje_vagon",
                                "ticket": "$_id.ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_viaje": {
                                "$sum": "$total_peso_racimos_alzados_lote_viaje"
                            }

                        }
                    },
                    {
                        "$unwind": "$data"
                    },

                    {
                        "$addFields":
                            {
                                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                                "total_alzados_lote": "$data.total_alzados_lote"
                            }
                    },

                    {
                        "$unwind": "$data.data"
                    },
                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                                        "total_alzados_lote": "$total_alzados_lote"
                                        , "ticket": "$_id.ticket"
                                        , "(%) Alzados x lote": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                                },
                                                "then": 0,
                                                "else": {
                                                    "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "Peso REAL Alzados": {
                                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

                                }
                            }
                    }

                    , {
                        "$addFields": {
                            "Peso REAL lote": {
                                "$cond": {
                                    "if": { "$eq": ["$Total Alzados", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                                    }
                                }
                            }
                        }
                    }
                    , {
                        "$project": {
                            "form_despacho_cosecha": 0,
                            "empleados_cosecha_adicionales": 0,
                            "info_empleado_adicional": 0,
                            "anio_filtro": 0
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "form_cargueinformaciondeplantas",
                            "as": "referencia_siembras",
                            "let": {
                                "nombre_lote": "$lote",
                                "cultivo": "$cultivo"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$CULTIVO", "$$cultivo"] },
                                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$limit": 1
                                }

                            ]
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$referencia_siembras",
                            "preserveNullAndEmptyArrays": false
                        }
                    },



                    {
                        "$addFields": {
                            "Hectareas": "$referencia_siembras.HECTAREAS",
                            "Siembra lote": { "$toString": "$referencia_siembras.SIEMBRA" },
                            "num_palmas": "$referencia_siembras.PALMAS",
                            "Material": "$referencia_siembras.MATERIAL",
                            "Cod_Lote": "$referencia_siembras.CODIGO LOTE"
                        }
                    },

                    {
                        "$project": {
                            "referencia_siembras": 0
                        }
                    }


                    , { "$unwind": "$Empleados de Cosecha" }

                    , {
                        "$addFields": {
                            "oid_empleado": { "$toObjectId": "$Empleados de Cosecha._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "oid_empleado",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , { "$unwind": "$info_empleado" }

                    , {
                        "$lookup": {
                            "from": "companies",
                            "localField": "info_empleado.cid",
                            "foreignField": "_id",
                            "as": "info_empresa"
                        }
                    }
                    , { "$unwind": "$info_empresa" }

                    , {
                        "$addFields": {
                            "Empresa empleado": "$info_empresa.name"
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "form_costodeactividadesxempresa",
                            "as": "costos_actividades_x_empresa",
                            "let": {
                                "fecha_siembra": "$Siembra lote",
                                "empresa": "$Empresa empleado"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$fecha_siembra", "$Fecha de Siembra"] },
                                                { "$eq": ["$$empresa", "$Empresa"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$costos_actividades_x_empresa",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , {
                        "$addFields": {
                            "peso": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$Empleados de Cosecha.value" }, 0] }, "$Peso REAL lote"] },
                            "cargo": "$Empleados de Cosecha.reference"
                        }
                    }


                    , {
                        "$addFields": {
                            "precio_actividad": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE FRUTO" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            },

                            "precio_herramienta": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE HERRAMIENTA" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "costo_actividad": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_actividad" }, 0] }, "$peso"] },
                            "costo_herramienta": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_herramienta" }, 0] }, "$peso"] }
                        }
                    }


                    , {
                        "$addFields": {
                            "costo": { "$sum": ["$costo_actividad", "$costo_herramienta"] }
                        }
                    }


                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    , {
                        "$project": {


                            "Finca": "$Finca",
                            "Lote": "$lote",
                            "Cod_Lote": { "$ifNull": ["$Cod_Lote", ""] },
                            "Siembra": "$Siembra lote",
                            "cultivo": "PALMA",
                            "Centro de costos": "Corte y recoleccion",
                            "Nombre Labor": "$cargo",
                            "Codigo Labor": "COSECHA",
                            "Estado Labor": {
                                "$cond": {
                                    "if": { "$eq": ["$ticket", -1] },
                                    "then": "💪 En progreso",
                                    "else": "✔ Listo"
                                }
                            },

                            "Fecha inicio": "$Fecha de puesta en caja",
                            "Fecha fin": "$Fecha de puesta en caja",

                            "Codigo empleado": "$info_empleado.code",
                            "Nombre empleado": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                            "Cedula": { "$ifNull": ["$info_empleado.numberID", "--"] },
                            "Empresa": "$Empresa empleado",
                            "Unidad": "Kilogramos",
                            "Cantidad": "$peso",
                            "vr unit trabajador": "$precio_actividad",
                            "vr unit herramienta": "$precio_herramienta",
                            "vr Total trabajador": "$costo_actividad",
                            "vr Total herramienta": "$costo_herramienta",
                            "TOTAL PAGO": "$costo",
                            "rgDate": "$Fecha de puesta en caja",
                            "centro costos presupuesto": "Corte y recoleccion",

                            "Anio": { "$year": "$Fecha de puesta en caja" },
                            "Mes": { "$month": "$Fecha de puesta en caja" },
                            "Semana": { "$week": "$Fecha de puesta en caja" }
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_centrosdecostospresupuesto",
                            "localField": "centro costos presupuesto",
                            "foreignField": "Ceco Presupuesto",
                            "as": "aux_form_centrosdecostospresupuesto"
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$aux_form_centrosdecostospresupuesto",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields": {
                            "Cuenta Contable Mano de Obra": { "$ifNull": ["$aux_form_centrosdecostospresupuesto.Cuenta Contable Mano de Obra", ""] },
                            "Cuenta Contable Insumo": { "$ifNull": ["$aux_form_centrosdecostospresupuesto.Cuenta Contable Insumo", ""] }
                        }
                    },
                    { "$project": { "aux_form_centrosdecostospresupuesto": 0 } }
                ]

            }
        }


        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$data_labores_x_empleado"
                            , "$data_liquidacion_cosecha"
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        , {
            "$addFields": {
                "unix_inicio": {
                    "$toDouble": "$Fecha inicio"
                },
                "unix_fin": {
                    "$toDouble": "$Fecha fin"
                }
            }
        },
        {
            "$addFields": {
                "dia_inicio": {
                    "$ceil": {
                        "$divide": [
                            "$unix_inicio",
                            86400000
                        ]
                    }
                },
                "dia_fin": {
                    "$ceil": {
                        "$divide": [
                            "$unix_fin",
                            86400000
                        ]
                    }
                },
                "semana_inicio": {
                    "$ceil": {
                        "$divide": [
                            { "$sum": ["$unix_inicio", 259200000] },
                            604800000
                        ]
                    }
                },
                "semana_fin": {
                    "$ceil": {
                        "$divide": [
                            { "$sum": ["$unix_fin", 259200000] },
                            604800000
                        ]
                    }
                }
            }
        },
        {
            "$addFields": {
                "cantidad_dias": {
                    "$sum": [
                        "$dia_fin",
                        { "$multiply": ["$dia_inicio", -1] },
                        1
                    ]
                },
                "cantidad_semanas": {
                    "$sum": [
                        "$semana_fin",
                        { "$multiply": ["$semana_inicio", -1] },
                        1
                    ]
                }
            }
        },
        {
            "$addFields": {
                "semanas": {
                    "$range": [
                        "$semana_inicio",
                        { "$sum": ["$semana_fin", 1] },
                        1
                    ]
                },
                "dias": {
                    "$range": [
                        "$dia_inicio",
                        { "$sum": ["$dia_fin", 1] },
                        1
                    ]
                }
            }
        },
        {
            "$unwind": "$dias"
        }


        , {
            "$addFields": {
                "pago_x_dia": {
                    "$cond": {
                        "if": {
                            "$eq": ["$cantidad_dias", 1]
                        },
                        "then": "$vr Total trabajador",
                        "else": {
                            "$divide": ["$vr Total trabajador", "$cantidad_dias"]
                        }
                    }
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "dia": "$dias",
                    "Nombre empleado": "$Nombre empleado",
                    "Codigo empleado": "$Codigo empleado",
                    "Cedula": "$Cedula"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "fecha": { "$toDate": { "$multiply": ["$_id.dia", 86400000] } }
            }
        }

        , {
            "$addFields": {
                "fechas": {
                    "Fecha": "$fecha",
                    "Anio": { "$year": { "date": "$fecha", "timezone": "-0500" } },
                    "Mes": { "$month": { "date": "$fecha", "timezone": "-0500" } },
                    "Semana": { "$week": { "date": "$fecha", "timezone": "-0500" } },
                    "Dia": { "$dayOfYear": { "date": "$fecha", "timezone": "-0500" } },
                    "Dia_semana": { "$dayOfWeek": { "date": "$fecha", "timezone": "-0500" } }
                }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        "$fechas"
                    ]
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Empresa": "$Empresa",

                    "Nombre empleado": "$Nombre empleado",
                    "Codigo empleado": "$Codigo empleado",

                    "anio": "$Anio",
                    "semana": "$Semana",
                    "dia": "$Dia",
                    "dia_semana": "$Dia_semana"
                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "total_pago_x_dia": {
                    "$sum": "$pago_x_dia"
                }
                , "fecha_min": {
                    "$min": "$Fecha"
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "Empresa": "$_id.Empresa",

                    "Nombre empleado": "$_id.Nombre empleado",
                    "Codigo empleado": "$_id.Codigo empleado",

                    "anio": "$_id.anio",
                    "semana": "$_id.semana"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_pago_x_semana": {
                    "$sum": "$total_pago_x_dia"
                }
                , "dias_semana_trabajados": {
                    "$push": "$_id.dia_semana"
                }
                , "fecha_min": {
                    "$min": "$fecha_min"
                }
            }
        },

        {
            "$addFields": {
                "tiene_dominal": {
                    "$eq": [
                        {
                            "$size": {
                                "$setDifference": [[2, 3, 4, 5, 6, 7], "$dias_semana_trabajados"]
                            }
                        },
                        0
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "pago_dominical": {
                    "$cond": {
                        "if": {
                            "$and": [
                                "$tiene_dominal",
                                { "$ne": ["$_id.Empresa", "Agroindustria Feleda"] },
                                { "$ne": ["$_id.Empresa", "Agricola El Poleo"] }

                            ]
                        },
                        "then": { "$divide": ["$total_pago_x_semana", 6] },
                        "else": 0
                    }
                }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_pago_x_semana": "$total_pago_x_semana",
                            "pago_dominical": "$pago_dominical",
                            "fecha_min": "$fecha_min"
                        }
                    ]
                }
            }
        }


        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_pago_x_dia": "$total_pago_x_dia",
                            "total_pago_x_semana": "$total_pago_x_semana",
                            "pago_dominical": "$pago_dominical",
                            "rgDate": "$fecha_min"
                        }
                    ]
                }
            }
        }




        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
            }
        }

        , {
            "$project": {
                "semanas": 0,
                "unix_inicio": 0,
                "unix_fin": 0,
                "dia_inicio": 0,
                "dia_fin": 0,
                "semana_inicio": 0,
                "semana_fin": 0,
                "dias": 0
            }
        }

        , {
            "$addFields": {
                "pago_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$pago_x_dia", 100] }, { "$mod": [{ "$multiply": ["$pago_x_dia", 100] }, 1] }] }, 100] },
                "total_pago_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_pago_x_dia", 100] }, { "$mod": [{ "$multiply": ["$total_pago_x_dia", 100] }, 1] }] }, 100] },
                "total_pago_x_semana": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_pago_x_semana", 100] }, { "$mod": [{ "$multiply": ["$total_pago_x_semana", 100] }, 1] }] }, 100] },
                "pago_dominical": { "$divide": [{ "$subtract": [{ "$multiply": ["$pago_dominical", 100] }, { "$mod": [{ "$multiply": ["$pago_dominical", 100] }, 1] }] }, 100] }
            }
        }


        , {
            "$lookup": {
                "from": "form_cargaprestacionalporempresa",
                "localField": "Empresa",
                "foreignField": "Empresa",
                "as": "carga_prestacional_x_empresa"
            }
        }
        , {
            "$unwind": {
                "path": "$carga_prestacional_x_empresa",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields":
                {
                    "carga_prestacional_x_empresa": { "$ifNull": ["$carga_prestacional_x_empresa.Porcentaje prestacional", 0] }
                }
        }


        , {
            "$addFields": {
                "pct_dominical": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$ne": ["$pago_dominical", 0] },
                                { "$ne": ["$total_pago_x_semana", 0] },
                                { "$ne": ["$Dia", -1] }
                            ]
                        },
                        "then": { "$divide": ["$pago_x_dia", "$total_pago_x_semana"] },
                        "else": 0
                    }
                }
            }
        }

        , {
            "$addFields": {
                "costo_unitario_dominical": {
                    "$multiply": ["$pct_dominical", "$pago_dominical"]
                }
            }
        }

        , {
            "$match": {
                "cultivo": "PALMA"
            }
        }


        , {
            "$addFields": {
                "COSTO_1_MANO_OBRA": "$pago_x_dia"
            }
        }
        , {
            "$addFields": {
                "COSTO_2_MANO_OBRA_CON_DONIMICAL": {
                    "$sum": ["$COSTO_1_MANO_OBRA", "$costo_unitario_dominical"]
                }
            }
        }
        , {
            "$addFields": {
                "COSTO_3_MANO_OBRA_CON_DONIMICAL_Y_PRESTACIONAL": {
                    "$sum": [
                        "$COSTO_2_MANO_OBRA_CON_DONIMICAL",
                        { "$multiply": ["$COSTO_2_MANO_OBRA_CON_DONIMICAL", "$carga_prestacional_x_empresa"] }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "COSTO_4_TOTAL_MANO_OBRA_Y_HERRAMIENTA": {
                    "$sum": [
                        "$COSTO_3_MANO_OBRA_CON_DONIMICAL_Y_PRESTACIONAL",
                        "$vr Total herramienta"
                    ]
                }
            }
        }
        
        //---variables ejecucion
        , {
            "$addFields":
                {
                    "EJECUCION_MO": "$COSTO_3_MANO_OBRA_CON_DONIMICAL_Y_PRESTACIONAL",
                    "EJECUCION_INV": "$vr Total herramienta",
                    "EJECUCION_TOTAL": "$COSTO_4_TOTAL_MANO_OBRA_Y_HERRAMIENTA"
                }
        }

        , {
            "$group": {
                "_id": {
                    "centro_de_costos": "$centro costos presupuesto",
                    "lote": "$Lote",
                    "anio": "$Anio",
                    "mes": "$Mes"
                },
                // "data": {
                //     "$push": "$$ROOT"
                // }
                "EJECUCION_MO": {
                    "$sum": "$EJECUCION_MO"
                },
                "EJECUCION_INV": {
                    "$sum": "$EJECUCION_INV"
                },
                "EJECUCION_TOTAL": {
                    "$sum": "$EJECUCION_TOTAL"
                }
            }
        }


        // , {
        //     "$lookup": {
        //         "from": "form_presupuestopalma",
        //         "as": "Presupuesto",
        //         "let": {
        //             "centro_de_costos": "$_id.centro_de_costos",
        //             "lote": "$_id.lote",
                    
        //             "mes": "$_id.mes"
        //         },
        //         "pipeline": [
        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
                                    
        //                             { "$eq": ["$$mes", "$Num_Mes"] },
        //                             { "$eq": ["$$lote", "$Lote"] },
        //                             { "$eq": ["$$centro_de_costos", "$Conjunto de Actividades"] }

        //                         ]
        //                     }
        //                 }
        //             }
        //             , { "$limit": 1 }
        //         ]
        //     }
        // }


        // , {
        //     "$unwind": {
        //         "path": "$Presupuesto",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }



        // , {
        //     "$addFields":
        //         {
        //             "Costo de Mano de Obra": { "$ifNull": [{ "$toDouble": "$Presupuesto.Costo de Mano de Obra" }, 0] },
        //             "Costo de Inventario": { "$ifNull": [{ "$toDouble": "$Presupuesto.Costo de Inventario" }, 0] }
        //         }
        // }

        // , {
        //     "$project": {
        //         "Presupuesto": 0
        //     }
        // }

        // , { "$unwind": "$data" }
        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "PRESUPUESTO_MO": "$Costo de Mano de Obra",
        //                     "PRESUPUESTO_INV": "$Costo de Inventario",
        //                     "PRESUPUESTO_TOTAL": {
        //                         "$sum": ["$Costo de Mano de Obra", "$Costo de Inventario"]
        //                     }
        //                 }
        //             ]
        //         }
        //     }
        // }

        
        // xx-----
        // // , {
        // //     "$addFields":
        // //         {
        // //             "EJECUCION_MO": "$COSTO_3_MANO_OBRA_CON_DONIMICAL_Y_PRESTACIONAL",
        // //             "EJECUCION_INV": "$vr Total herramienta",
        // //             "EJECUCION_TOTAL": "$COSTO_4_TOTAL_MANO_OBRA_Y_HERRAMIENTA"
        // //         }
        // // }





    ]
)