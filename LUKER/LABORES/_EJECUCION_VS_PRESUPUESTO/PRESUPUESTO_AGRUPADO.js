db.form_presupuestopalma.aggregate(

    {
        $group: {
            _id: {
                //"Lote": "$Lote",
                "Conjunto de Actividades": "$Conjunto de Actividades",
                "Num_Mes": "$Num_Mes",
                "Anio" : "$Anio"
            },

            "PRESUPUESTO_MO": { $sum: "$Costo de Mano de Obra" },
            "PRESUPUESTO_INV": { $sum: "$Costo de Inventario" },
        }
    }

    , {
        "$addFields": {
            "PRESUPUESTO_TOTAL": {
                $sum: ["$PRESUPUESTO_MO", "$PRESUPUESTO_INV"]
            }
        }
    }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "PRESUPUESTO_MO": "$PRESUPUESTO_MO",
                        "PRESUPUESTO_INV": "$PRESUPUESTO_INV",
                        "PRESUPUESTO_TOTAL": "$PRESUPUESTO_TOTAL"
                    }
                ]
            }
        }
    }
    
    ,{
        //$out:"form_presupuestopalma__x__lote"
         $out:"form_presupuestopalma__x__ceco"
    }

)