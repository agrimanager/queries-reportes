[

    {
        "$lookup": {
            "from": "tasks",
            "localField": "_id",
            "foreignField": "uid",
            "as": "labores"
        }
    }
    , {
    "$unwind": {
        "path": "$labores"
    }
}

    , {
    "$replaceRoot": {
        "newRoot": {
            "$mergeObjects": [
                "$labores"
            ]
        }
    }
},



    { "$match": { "farm": { "$ne": "" } } },

    {
        "$lookup": {
            "from": "activities",
            "localField": "activity",
            "foreignField": "_id",
            "as": "activity"
        }
    },
    { "$unwind": "$activity" },

    {
        "$lookup": {
            "from": "costsCenters",
            "localField": "ccid",
            "foreignField": "_id",
            "as": "costsCenter"
        }
    },
    { "$unwind": "$costsCenter" },

    {
        "$lookup": {
            "from": "farms",
            "localField": "farm",
            "foreignField": "_id",
            "as": "farm"
        }
    },
    { "$unwind": "$farm" },

    {
        "$lookup": {
            "from": "employees",
            "localField": "supervisor",
            "foreignField": "_id",
            "as": "supervisor"
        }
    },
    {
        "$unwind": {
            "path": "$supervisor",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "employees": {
                "$cond": {
                    "if": {
                        "$eq": [{ "$type": "$employees" }, "array"]
                    },
                    "then": "$employees",
                    "else": {
                        "$map": {
                            "input": { "$objectToArray": "$employees" },
                            "as": "employeeKV",
                            "in": "$$employeeKV.v"
                        }
                    }
                }
            },
            "productivityReport": {
                "$cond": {
                    "if": {
                        "$eq": [{ "$type": "$productivityReport" }, "array"]
                    },
                    "then": "$productivityReport",
                    "else": {
                        "$map": {
                            "input": { "$objectToArray": "$productivityReport" },
                            "as": "productivityReportKV",
                            "in": "$$productivityReportKV.v"
                        }
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "total_empleados_seleccionados_labor": { "$size": "$employees" },
            "total_productos_seleccionados_labor": { "$size": "$supplies" }
        }
    },

    {
        "$unwind": {
            "path": "$employees",
            "includeArrayIndex": "arrayIndex",
            "preserveNullAndEmptyArrays": false
        }
    },
    {
        "$addFields": {
            "employees": {
                "$toObjectId": "$employees"
            }
        }
    },
    {
        "$addFields": {
            "productivityReport": {
                "$map": {
                    "input": "$productivityReport",
                    "as": "item",
                    "in": {
                        "$mergeObjects": [
                            "$$item",
                            {
                                "employee": { "$toObjectId": "$$item.employee" }
                            }
                        ]
                    }
                }
            }
        }
    },
    {
        "$addFields": {
            "productivityReport": {
                "$filter": {
                    "input": "$productivityReport",
                    "as": "item",
                    "cond": {
                        "$eq": ["$$item.employee", "$employees"]
                    }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$productivityReport",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "ProductividadEmpleado": { "$toDouble": "$productivityReport.quantity" }
        }
    },

    {
        "$lookup": {
            "from": "employees",
            "localField": "employees",
            "foreignField": "_id",
            "as": "Empleado"
        }
    },
    {
        "$unwind": {
            "path": "$Empleado",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields":
            {
                "TotalPagoEmpleado": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$ProductividadEmpleado" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }
                    ]
                }
            }
    },

    {
        "$addFields": {
            "productos": {
                "$cond": {
                    "if": {
                        "$eq": [{ "$type": "$supplies" }, "array"]
                    },
                    "then": "$supplies",
                    "else": {
                        "$map": {
                            "input": { "$objectToArray": "$employees" },
                            "as": "suppliesKV",
                            "in": "$$suppliesKV.v"
                        }
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "productos": { "$map": { "input": "$productos._id", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "productos",
            "foreignField": "_id",
            "as": "productos"
        }
    },

    {
        "$addFields": {

            "productos": {
                "$reduce": {
                    "input": "$productos.name",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },

            "Etiquetas": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$tags",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                }, "--sin etiquetas--"]
            },


            "Tipo_cultivo": {
                "$reduce": {
                    "input": "$crop",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },

            "Blancos_biologicos": {
                "$reduce": {
                    "input": "$biologicalTarget",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },

            "Cartografia_seleccionada_mapa_tipos": {
                "$reduce": {
                    "input": {
                        "$reduce": {
                            "input": "$cartography.features.properties.type",
                            "initialValue": [],
                            "in": {
                                "$cond": {
                                    "if": { "$lt": [{ "$indexOfArray": ["$$value", "$$this"] }, 0] },
                                    "then": { "$concatArrays": ["$$value", ["$$this"]] },
                                    "else": "$$value"
                                }
                            }
                        }
                    },
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },

            "Cartografia_seleccionada_mapa_elementos": {
                "$reduce": {
                    "input": "$cartography.features.properties.name",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", "; ", "$$this"] }
                        }
                    }
                }
            },


            "Cartografia_seleccionada_lista_lotes_y_estados": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$lots",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                }, "--sin lotes seleccionados--"]
            },

            "lista_lots": {
                "$map": {
                    "input": "$lots",
                    "as": "lot",
                    "in": {
                        "$split": ["$$lot", ", "]
                    }
                }
            }


        }
    },

    {
        "$addFields": {
            "lista_lotes": {
                "$map": {
                    "input": "$lista_lots",
                    "as": "lot",
                    "in": {
                        "$arrayElemAt": ["$$lot", 0]
                    }
                }
            },
            "lista_estados": {
                "$map": {
                    "input": "$lista_lots",
                    "as": "lot",
                    "in": {
                        "$arrayElemAt": ["$$lot", 1]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "lista_lotes": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$lista_lotes",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                }, "--sin lotes seleccionados--"]
            },
            "lista_estados": {
                "$ifNull": [{
                    "$reduce": {
                        "input": "$lista_estados",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                }, "--sin estados seleccionados--"]
            }
        }
    },

    {
        "$project": {
            "_id": 0,
            "Actividad": "$activity.name",
            "Codigo Labor": "$cod",
            "Estado Labor": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$status", "To do"] }, "then": "⏰ Por hacer" },
                        { "case": { "$eq": ["$status", "Doing"] }, "then": "💪 En progreso" },
                        { "case": { "$eq": ["$status", "Done"] }, "then": "✔ Listo" }
                    ]
                }
            },
            "Finca": "$farm.name",
            "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
            "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
            "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
            "Lista lotes": "$lista_lotes",
            "Lista estados": "$lista_estados",

            "Tipo cultivo labor": { "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"] },
            "Etiquetas": {
                "$cond": {
                    "if": { "$eq": ["$Etiquetas", ""] },
                    "then": "--sin etiquetas--",
                    "else": "$Etiquetas"
                }
            },


            "Centro de costos": "$costsCenter.name",
            "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
            "[Unidad] Medida de Actividad": "$productivityPrice.measure",
            "($) Precio x unidad de Actividad": "$productivityPrice.price",

            "Semana de registro labor": { "$sum": [{ "$week": "$rgDate" }, 1] },

            "Fecha inicio": { "$max": "$when.start" },
            "Fecha fin": { "$max": "$when.finish" },
            "Semana de inicio labor": { "$sum": [{ "$week": { "$arrayElemAt": ["$when.start", 0] } }, 1] },
            "Semana de fin labor": { "$sum": [{ "$week": { "$arrayElemAt": ["$when.finish", 0] } }, 1] },

            "Blancos biologicos de labor": { "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"] },
            "Observaciones": "$observation",

            "Nombre de Empleado": { "$ifNull": [{ "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"] }, "--sin empleados--"] },
            "Identificacion": { "$ifNull": ["$Empleado.numberID", "--"] },
            "(#) Productividad de Empleado": { "$ifNull": ["$ProductividadEmpleado", 0] },
            "($) Total Pago Empleado": "$TotalPagoEmpleado",
            "Firma de empleado": "* ",
            "Aviso legal": "* ",
            "Supervisor": { "$ifNull": [{ "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"] }, "--sin supervisor--"] },
            "(#) Productos asignados": "$total_productos_seleccionados_labor",
            "Productos asignados": {
                "$cond": {
                    "if": { "$eq": ["$productos", ""] },
                    "then": "--sin productos--",
                    "else": "$productos"
                }
            },
            "(#) Empleados asignados": "$total_empleados_seleccionados_labor",
            "Point": "$Point"
        }
    },

    {
        "$addFields": {
            "unix_inicio": {
                "$toDouble": "$Fecha inicio"
            },
            "unix_fin": {
                "$toDouble": "$Fecha fin"
            }
        }
    },
    {
        "$addFields": {
            "dia_inicio": {
                "$ceil": {
                    "$divide": [
                        "$unix_inicio",
                        86400000
                    ]
                }
            },
            "dia_fin": {
                "$ceil": {
                    "$divide": [
                        "$unix_fin",
                        86400000
                    ]
                }
            },
            "semana_inicio": {
                "$ceil": {
                    "$divide": [
                        { "$sum": ["$unix_inicio", 259200000] },
                        604800000
                    ]
                }
            },
            "semana_fin": {
                "$ceil": {
                    "$divide": [
                        { "$sum": ["$unix_fin", 259200000] },
                        604800000
                    ]
                }
            }
        }
    },
    {
        "$addFields": {
            "cantidad_dias": {
                "$sum": [
                    "$dia_fin",
                    { "$multiply": ["$dia_inicio", -1] },
                    1
                ]
            },
            "cantidad_semanas": {
                "$sum": [
                    "$semana_fin",
                    { "$multiply": ["$semana_inicio", -1] },
                    1
                ]
            }
        }
    },
    {
        "$addFields": {
            "semanas": {
                "$range": [
                    "$semana_inicio",
                    { "$sum": ["$semana_fin", 1] },
                    1
                ]
            },
            "dias": {
                "$range": [
                    "$dia_inicio",
                    { "$sum": ["$dia_fin", 1] },
                    1
                ]
            }
        }
    },
    {
        "$unwind": "$dias"
    }

    , {
    "$addFields": {

        "pago_x_dia": {
            "$cond": {
                "if": {
                    "$eq": ["$cantidad_dias", 1]
                },
                "then": "$($) Total Pago Empleado",
                "else": {
                    "$divide": ["$($) Total Pago Empleado", "$cantidad_dias"]
                }
            }
        }
    }
}

    , {
    "$group": {
        "_id": {
            "dia": "$dias",
            "empleado": "$Nombre de Empleado"
        },
        "data": {
            "$push": "$$ROOT"
        }
    }
},
    {
        "$sort": {
            "empleado": 1,
            "dia": 1
        }
    }
    , {
    "$addFields": {
        "fecha": { "$toDate": { "$multiply": ["$_id.dia", 86400000] } }
    }
}

    , {
    "$addFields": {
        "anio": { "$year": { "date": "$fecha", "timezone": "-0500" } },
        "mes": { "$month": { "date": "$fecha", "timezone": "-0500" } },
        "semana": { "$week": { "date": "$fecha", "timezone": "-0500" } },
        "dia": { "$dayOfYear": { "date": "$fecha", "timezone": "-0500" } },
        "dia_semana": { "$dayOfWeek": { "date": "$fecha", "timezone": "-0500" } }
    }
}


    , {
    "$addFields": {
        "dia_semana_txt": {
            "$switch": {
                "branches": [
                    { "case": { "$eq": ["$dia_semana", 2] }, "then": "01-Lunes" },
                    { "case": { "$eq": ["$dia_semana", 3] }, "then": "02-Martes" },
                    { "case": { "$eq": ["$dia_semana", 4] }, "then": "03-Miercoles" },
                    { "case": { "$eq": ["$dia_semana", 5] }, "then": "04-Jueves" },
                    { "case": { "$eq": ["$dia_semana", 6] }, "then": "05-Viernes" },
                    { "case": { "$eq": ["$dia_semana", 7] }, "then": "06-sabado" },
                    { "case": { "$eq": ["$dia_semana", 1] }, "then": "07-Domingo" }
                ],
                "default": "dia de la semana desconocido"
            }
        }
    }
}

    , { "$unwind": "$data" }

    , {
    "$replaceRoot": {
        "newRoot": {
            "$mergeObjects": [
                {
                    "Empleado": "$_id.empleado",
                    "Identificacion empleado": "$data.Identificacion",
                    "Fecha": "$fecha",
                    "Actividad": "$data.Actividad",
                    "Codigo Labor": "$data.Codigo Labor",
                    "Pago empleado x dia": "$data.pago_x_dia",
                    "anio": "$anio",
                    "mes": "$mes",
                    "semana": "$semana",
                    "dia": "$dia",
                    "dia_semana": "$dia_semana",
                    "dia_semana_txt": "$dia_semana_txt",

                    "Point": "$Point",
                    "rgDate": "$fecha"
                }
            ]
        }
    }
}

    , {
    "$group": {
        "_id": {
            "Empleado": "$Empleado",
            "anio": "$anio",
            "semana": "$semana",
            "dia": "$dia",
            "dia_semana": "$dia_semana"
        },
        "data": {
            "$push": "$$ROOT"
        }
        , "Pago total empleado x dia": {
            "$sum": "$Pago empleado x dia"
        }
    }
},

    { "$sort": { "_id.dia_semana": 1 } },

    {
        "$group": {
            "_id": {
                "Empleado": "$_id.Empleado",
                "anio": "$_id.anio",
                "semana": "$_id.semana"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "Pago total empleado x semana": {
                "$sum": "$Pago total empleado x dia"
            }
            , "dias_semana_trabajados": {
                "$push": "$_id.dia_semana"
            }
        }
    },


    {
        "$addFields": {
            "data_domingo": {
                "$eq": [
                    {
                        "$size": {
                            "$setDifference": [[2, 3, 4, 5, 6, 7], "$dias_semana_trabajados"]
                        }
                    },
                    0
                ]
            }
        }
    },

    {
        "$addFields": {
            "data": {
                "$cond": {
                    "if": "$data_domingo",
                    "then": {
                        "$concatArrays": [
                            "$data",
                            [{
                                "_id" : {
                                    "Empleado" : "$_id.Empleado",
                                    "anio" : "$_id.anio",
                                    "semana" : "$_id.semana",
                                    "dia" : -1,
                                    "dia_semana" : -1
                                },
                                "data" : [
                                    {
                                        "Empleado" : "$_id.Empleado",
                                        "Identificacion empleado" : {
                                            "$arrayElemAt": [
                                                {
                                                    "$arrayElemAt": [
                                                        "$data.data.Identificacion empleado",
                                                        0
                                                    ]
                                                },
                                                0
                                            ]
                                        },
                                        "Fecha": {
                                            "$arrayElemAt": [
                                                {
                                                    "$arrayElemAt": [
                                                        "$data.data.Fecha",
                                                        0
                                                    ]
                                                },
                                                0
                                            ]
                                        },
                                        "Actividad" : "Pago dominical",
                                        "Codigo Labor" : "DOMINICAL",
                                        "Pago empleado x dia" : {
                                            "$divide": [
                                                "$Pago total empleado x semana",
                                                6
                                            ]
                                        },
                                        "anio" : "$_id.anio",
                                        "semana" : "$_id.semana",
                                        "dia" : -1,
                                        "dia_semana" : -1,
                                        "mes": {
                                            "$arrayElemAt": [
                                                {
                                                    "$arrayElemAt": [
                                                        "$data.data.mes",
                                                        0
                                                    ]
                                                },
                                                0
                                            ]
                                        },
                                        "dia_semana_txt" : "08-Dominical",
                                        "rgDate" : {
                                            "$arrayElemAt": [
                                                {
                                                    "$arrayElemAt": [
                                                        "$data.data.rgDate",
                                                        0
                                                    ]
                                                },
                                                0
                                            ]
                                        }
                                    }
                                ],
                                "Pago total empleado x dia" : {
                                    "$divide": [
                                        "$Pago total empleado x semana",
                                        6
                                    ]
                                }
                            }]
                        ]
                    },
                    "else": "$data"
                }
            }
        }
    }

    ,{"$unwind":"$data"}

    ,{
    "$replaceRoot": {
        "newRoot": {
            "$mergeObjects": [
                "$data",
                {
                    "data_domingo": "$data_domingo",
                    "Pago total empleado x semana":"$Pago total empleado x semana"
                }
            ]
        }
    }
}

    ,{"$unwind":"$data"}

    ,{
    "$replaceRoot": {
        "newRoot": {
            "$mergeObjects": [
                "$data",
                {
                    "data_domingo": "$data_domingo",
                    "Pago total empleado x dia":"$Pago total empleado x dia",
                    "Pago total empleado x semana":"$Pago total empleado x semana"
                }
            ]
        }
    }
}

    ,{
    "$project":{
        "dia":0,
        "dia_semana":0,
        "data_domingo":0,
        "Pago total empleado x semana":0

    }
}

]