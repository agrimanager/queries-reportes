[

        {
            "$lookup": {
                "from": "tasks",
                "localField": "_id",
                "foreignField": "uid",
                "as": "labores"
            }
        }
        , {
            "$unwind": {
                "path": "$labores"
            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$labores"
                    ]
                }
            }
        },



        { "$match": { "farm": { "$ne": "" } } },

        {
            "$addFields": {
                "_id_str_farm": { "$toString": "$farm" }
            }
        }

        , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } },


        {
            "$lookup": {
                "from": "activities",
                "localField": "activity",
                "foreignField": "_id",
                "as": "activity"
            }
        },
        { "$unwind": "$activity" },

        {
            "$lookup": {
                "from": "costsCenters",
                "localField": "ccid",
                "foreignField": "_id",
                "as": "costsCenter"
            }
        },
        { "$unwind": "$costsCenter" },

        {
            "$lookup": {
                "from": "farms",
                "localField": "farm",
                "foreignField": "_id",
                "as": "farm"
            }
        },
        { "$unwind": "$farm" },


        {
            "$lookup": {
                "from": "employees",
                "localField": "supervisor",
                "foreignField": "_id",
                "as": "supervisor"
            }
        },
        {
            "$unwind": {
                "path": "$supervisor",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "employees": {
                    "$cond": {
                        "if": {
                            "$eq": [{ "$type": "$employees" }, "array"]
                        },
                        "then": "$employees",
                        "else": {
                            "$map": {
                                "input": { "$objectToArray": "$employees" },
                                "as": "employeeKV",
                                "in": "$$employeeKV.v"
                            }
                        }
                    }
                },
                "productivityReport": {
                    "$cond": {
                        "if": {
                            "$eq": [{ "$type": "$productivityReport" }, "array"]
                        },
                        "then": "$productivityReport",
                        "else": {
                            "$map": {
                                "input": { "$objectToArray": "$productivityReport" },
                                "as": "productivityReportKV",
                                "in": "$$productivityReportKV.v"
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "total_empleados_seleccionados_labor": { "$size": "$employees" },
                "total_productos_seleccionados_labor": { "$size": "$supplies" }
            }
        },

        {
            "$unwind": {
                "path": "$employees",
                "includeArrayIndex": "arrayIndex",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            "$addFields": {
                "employees": {
                    "$toObjectId": "$employees"
                }
            }
        },
        {
            "$addFields": {
                "productivityReport": {
                    "$map": {
                        "input": "$productivityReport",
                        "as": "item",
                        "in": {
                            "$mergeObjects": [
                                "$$item",
                                {
                                    "employee": { "$toObjectId": "$$item.employee" }
                                }
                            ]
                        }
                    }
                }
            }
        },
        {
            "$addFields": {
                "productivityReport": {
                    "$filter": {
                        "input": "$productivityReport",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.employee", "$employees"]
                        }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$productivityReport",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$addFields": {
                "ProductividadEmpleado": { "$toDouble": "$productivityReport.quantity" }
            }
        },

        {
            "$lookup": {
                "from": "employees",
                "localField": "employees",
                "foreignField": "_id",
                "as": "Empleado"
            }
        },
        {
            "$unwind": {
                "path": "$Empleado",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "companies",
                "localField": "Empleado.cid",
                "foreignField": "_id",
                "as": "Empresa"
            }
        },
        {
            "$unwind": {
                "path": "$Empresa",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields":
                {
                    "TotalPagoEmpleado": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$ProductividadEmpleado" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }
                        ]
                    }
                }
        },

        {
            "$addFields": {
                "productos": {
                    "$cond": {
                        "if": {
                            "$eq": [{ "$type": "$supplies" }, "array"]
                        },
                        "then": "$supplies",
                        "else": {
                            "$map": {
                                "input": { "$objectToArray": "$employees" },
                                "as": "suppliesKV",
                                "in": "$$suppliesKV.v"
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "productos": { "$map": { "input": "$productos._id", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$lookup": {
                "from": "supplies",
                "localField": "productos",
                "foreignField": "_id",
                "as": "productos"
            }
        },

        {
            "$addFields": {

                "productos": {
                    "$reduce": {
                        "input": "$productos.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                },

                "Etiquetas": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$tags",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin etiquetas--"]
                },


                "Tipo_cultivo": {
                    "$reduce": {
                        "input": "$crop",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                },

                "Blancos_biologicos": {
                    "$reduce": {
                        "input": "$biologicalTarget",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                },

                "Cartografia_seleccionada_mapa_tipos": {
                    "$reduce": {
                        "input": {
                            "$reduce": {
                                "input": "$cartography.features.properties.type",
                                "initialValue": [],
                                "in": {
                                    "$cond": {
                                        "if": { "$lt": [{ "$indexOfArray": ["$$value", "$$this"] }, 0] },
                                        "then": { "$concatArrays": ["$$value", ["$$this"]] },
                                        "else": "$$value"
                                    }
                                }
                            }
                        },
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                },

                "Cartografia_seleccionada_mapa_elementos": {
                    "$reduce": {
                        "input": "$cartography.features.properties.name",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", "; ", "$$this"] }
                            }
                        }
                    }
                },


                "Cartografia_seleccionada_lista_lotes_y_estados": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lots",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },

                "lista_lots": {
                    "$map": {
                        "input": "$lots",
                        "as": "lot",
                        "in": {
                            "$split": ["$$lot", ", "]
                        }
                    }
                }


            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 0]
                        }
                    }
                },
                "lista_estados": {
                    "$map": {
                        "input": "$lista_lots",
                        "as": "lot",
                        "in": {
                            "$arrayElemAt": ["$$lot", 1]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "lista_lotes": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lista_lotes",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin lotes seleccionados--"]
                },
                "lista_estados": {
                    "$ifNull": [{
                        "$reduce": {
                            "input": "$lista_estados",
                            "initialValue": "",
                            "in": {
                                "$cond": {
                                    "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                    "then": "$$this",
                                    "else": { "$concat": ["$$value", "; ", "$$this"] }
                                }
                            }
                        }
                    }, "--sin estados seleccionados--"]
                }
            }
        },

        {
            "$lookup": {
                "from": "form_costodeherramientasporactividad",
                "localField": "activity.name",
                "foreignField": "Actividad",
                "as": "CostoActividadHerramienta"
            }
        },
        {
            "$unwind": {
                "path": "$CostoActividadHerramienta",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote_lista": "$lista_lotes",
                    "nombre_lote_cartografia": "$Cartografia_seleccionada_mapa_elementos"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    {
                                        "$or": [
                                            { "$in": ["$$nombre_lote_lista", "$LOTE.features.properties.name"] },
                                            { "$in": ["$$nombre_lote_cartografia", "$LOTE.features.properties.name"] }
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$project": {
                "_id": 0,
                "Actividad": "$activity.name",
                "Costo de Herramienta": { "$ifNull": ["$CostoActividadHerramienta.Costo de herramienta", 0] },

                "Codigo Labor": "$cod",
                "Estado Labor": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$status", "To do"] }, "then": "⏰ Por hacer" },
                            { "case": { "$eq": ["$status", "Doing"] }, "then": "💪 En progreso" },
                            { "case": { "$eq": ["$status", "Done"] }, "then": "✔ Listo" }
                        ]
                    }
                },
                "Finca": "$farm.name",
                "tipos Cartografia": "$Cartografia_seleccionada_mapa_tipos",
                "elementos Cartografia": "$Cartografia_seleccionada_mapa_elementos",
                "Lista lotes-estado": "$Cartografia_seleccionada_lista_lotes_y_estados",
                "Lista lotes": "$lista_lotes",
                "Siembra": { "$ifNull": ["$referencia_siembras.SIEMBRA", 0] },
                "Lista estados": "$lista_estados",

                "Tipo cultivo labor": { "$ifNull": ["$Tipo_cultivo", "--sin tipo cultivo--"] },
                "Etiquetas": {
                    "$cond": {
                        "if": { "$eq": ["$Etiquetas", ""] },
                        "then": "--sin etiquetas--",
                        "else": "$Etiquetas"
                    }
                },


                "Centro de costos": "$costsCenter.name",
                "(#) Productividad esperada de Labor": "$productivityPrice.expectedValue",
                "[Unidad] Medida de Actividad": {
                    "$switch": {
                        "branches": [{
                            "case": { "$eq": ["$productivityPrice.measure", "blocks"] },
                            "then": "Bloque"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "lot"] },
                            "then": "Lotes"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "lines"] },
                            "then": "Linea"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "trees"] },
                            "then": "Árboles"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "fruitCenters"] },
                            "then": "Centro frutero"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "samplingPolygons"] },
                            "then": "Poligono de muestreo"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "valves"] },
                            "then": "Valvula"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "drainages"] },
                            "then": "Drenaje"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "sprinklers"] },
                            "then": "Aspersors"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkOne"] },
                            "then": "Red de riego uno"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkTwo"] },
                            "then": "Red de riego dos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "irrigationNetworkThree"] },
                            "then": "Red de riego tres"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "traps"] },
                            "then": "Trampa"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "lanes"] },
                            "then": "Vías"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "woods"] },
                            "then": "Bosque"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "sensors"] },
                            "then": "Sensor"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cableways"] },
                            "then": "Cable vía"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "buildings"] },
                            "then": "Edificio"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "waterBodies"] },
                            "then": "Cuerpo de agua"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "additionalPolygons"] },
                            "then": "Poligonos adicionales"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "farming units"] },
                            "then": "Unidades de cultivo (Ejemplo: Árboles)"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "wages"] },
                            "then": "Jornales"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "quantity"] },
                            "then": "Cantidades"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "mts"] },
                            "then": "Metros"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "km"] },
                            "then": "Kilometros"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cm"] },
                            "then": "Centimetros"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "mile"] },
                            "then": "Millas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "yard"] },
                            "then": "Yardas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "foot"] },
                            "then": "Pies"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "inch"] },
                            "then": "Pulgadas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "kg"] },
                            "then": "Kilogramos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "gr"] },
                            "then": "Gramos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "mg"] },
                            "then": "Miligramos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "US/ton"] },
                            "then": "Toneladas estadounidenses"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "ton"] },
                            "then": "Toneladas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "oz"] },
                            "then": "Onzas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "lb"] },
                            "then": "Libras"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "lts"] },
                            "then": "Litros"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "US/galon"] },
                            "then": "Galones estadounidenses"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "galon"] },
                            "then": "Galones"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cf"] },
                            "then": "Pies cúbicos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "ci"] },
                            "then": "Pulgadas cúbicas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cuc"] },
                            "then": "Centimetros cúbicos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cum"] },
                            "then": "Metros cúbicos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "packages"] },
                            "then": "Bultos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "bags"] },
                            "then": "Bolsas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "sacks"] },
                            "then": "sacks"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "yemas"] },
                            "then": "Yemas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "bun"] },
                            "then": "Factura"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cargo"] },
                            "then": "Flete"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "manege"] },
                            "then": "Picadero"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "hr"] },
                            "then": "Hora"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "qty"] },
                            "then": "Por cantidad"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "hectares"] },
                            "then": "Hectáreas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "squares"] },
                            "then": "Cuadras"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "dustbin"] },
                            "then": "Canecas"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "bunch"] },
                            "then": "Racimos"
                        }, {
                            "case": { "$eq": ["$productivityPrice.measure", "cubic-meter"] },
                            "then": "Metro cúbico"
                        }, { "case": { "$eq": ["$productivityPrice.measure", "metro-line"] }, "then": "Metro Lineal" }],
                        "default": "--------"
                    }
                },
                "($) Precio x unidad de Actividad": "$productivityPrice.price",

                "Semana de registro labor": { "$sum": [{ "$week": "$rgDate" }, 1] },

                "Fecha inicio": { "$max": "$when.start" },
                "Fecha fin": { "$max": "$when.finish" },
                "Semana de inicio labor": { "$sum": [{ "$week": { "$arrayElemAt": ["$when.start", 0] } }, 1] },
                "Semana de fin labor": { "$sum": [{ "$week": { "$arrayElemAt": ["$when.finish", 0] } }, 1] },

                "Blancos biologicos de labor": { "$ifNull": ["$Blancos_biologicos", "--sin blancos biologicos--"] },
                "Observaciones": "$observation",

                "Nombre de Empleado": { "$ifNull": [{ "$concat": ["$Empleado.firstName", " ", "$Empleado.lastName"] }, "--sin empleados--"] },
                "Identificacion": { "$ifNull": ["$Empleado.numberID", "--"] },
                "Codigo de Empleado": { "$ifNull": ["$Empleado.code", "--"] },
                "Empresa": "$Empresa.name",
                "(#) Productividad de Empleado": { "$ifNull": ["$ProductividadEmpleado", 0] },
                "($) Total Pago Empleado": "$TotalPagoEmpleado",
                "Firma de empleado": "* ",
                "Aviso legal": "* ",
                "Supervisor": { "$ifNull": [{ "$concat": ["$supervisor.firstName", " ", "$supervisor.lastName"] }, "--sin supervisor--"] },
                "(#) Productos asignados": "$total_productos_seleccionados_labor",
                "Productos asignados": {
                    "$cond": {
                        "if": { "$eq": ["$productos", ""] },
                        "then": "--sin productos--",
                        "else": "$productos"
                    }
                },
                "(#) Empleados asignados": "$total_empleados_seleccionados_labor",
                "Point": "$Point"



            }
        },


        {
            "$project": {
                "Finca": "$Finca",
                "Lote": {
                    "$cond": {
                        "if": { "$ne": ["$elementos Cartografia", ""] },
                        "then": "$elementos Cartografia",
                        "else": {
                            "$cond": {
                                "if": { "$ne": ["$Lista lotes", "[]"] },
                                "then": "$Lista lotes",
                                "else": "--sin cartografia--"
                            }
                        }
                    }
                },

                "Siembra": "$Siembra",

                "Centro de costos": "$Centro de costos",
                "Nombre Labor": "$Actividad",
                "Codigo Labor": "$Codigo Labor",
                "Estado Labor": "$Estado Labor",

                "Fecha inicio": "$Fecha inicio",
                "Fecha fin": "$Fecha fin",

                "Codigo empleado": "$Codigo de Empleado",
                "Nombre empleado": "$Nombre de Empleado",
                "Cedula": "$Identificacion",
                "Empresa": "$Empresa",

                "Unidad": "$[Unidad] Medida de Actividad",
                "Cantidad": "$(#) Productividad de Empleado",
                "vr unit trabajador": "$($) Precio x unidad de Actividad",
                "vr unit herramienta": "$Costo de Herramienta"
            }
        },

        {
            "$addFields":
                {
                    "vr Total trabajador": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$Cantidad" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$vr unit trabajador" }, 0] }
                        ]
                    },

                    "vr Total herramienta": {
                        "$multiply": [
                            { "$ifNull": [{ "$toDouble": "$Cantidad" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$vr unit herramienta" }, 0] }
                        ]
                    }
                }
        },



        {
            "$addFields":
                {
                    "TOTAL PAGO": {
                        "$sum": [
                            { "$ifNull": [{ "$toDouble": "$vr Total trabajador" }, 0] },
                            { "$ifNull": [{ "$toDouble": "$vr Total herramienta" }, 0] }
                        ]
                    },
                    "rgDate": "$Fecha inicio",
                    "Point": "$Point"
                }
        },

        {
            "$addFields":
                {
                    "vr Total trabajador": { "$divide": [{ "$subtract": [{ "$multiply": ["$vr Total trabajador", 100] }, { "$mod": [{ "$multiply": ["$vr Total trabajador", 100] }, 1] }] }, 100] },
                    "vr Total herramienta": { "$divide": [{ "$subtract": [{ "$multiply": ["$vr Total herramienta", 100] }, { "$mod": [{ "$multiply": ["$vr Total herramienta", 100] }, 1] }] }, 100] },
                    "TOTAL PAGO": { "$divide": [{ "$subtract": [{ "$multiply": ["$TOTAL PAGO", 100] }, { "$mod": [{ "$multiply": ["$TOTAL PAGO", 100] }, 1] }] }, 100] }
                }
        },



        {
            "$lookup": {
                "from": "form_configuracioncentrosdecostospresupuesto",
                "localField": "Centro de costos",
                "foreignField": "Ceco Agrimanager",
                "as": "centro costos presupuesto"
            }
        },
        {
            "$unwind": {
                "path": "$centro costos presupuesto",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields":
                {
                    "Anio": { "$year": "$Fecha inicio" },
                    "Mes": { "$month": "$Fecha inicio" },
                    "Semana": { "$week": "$Fecha inicio" },

                    "centro costos presupuesto": "$centro costos presupuesto.Ceco Presupuesto"
                }
        }



        , {
            "$group": {
                "_id": {
                    "usr": { "$toObjectId": "5d263f5545a0dd2e9e204fc3" }
                },
                "data_labores_x_empleado": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$lookup": {
                "from": "form_recolecciondecosechaxempleados",
                "as": "data_liquidacion_cosecha",
                "let": {
                    "id_usr": "$_id.usr"
                },
                "pipeline": [
                    {
                        "$match": {


                            "$expr": {
                                "$eq": ["$$id_usr", "$uid"]
                            }
                        }
                    },

                    {
                        "$match": {
                            "Lote.path": { "$ne": "" }
                        }
                    },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha de puesta en caja" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                        }
                    },

                    {
                        "$addFields": {
                            "Bloque": "$Bloque.properties.name",
                            "lote": "$lote.properties.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": "$Finca.name"
                        }
                    },
                    { "$unwind": "$Finca" },


                    {
                        "$lookup": {
                            "from": "form_pesospromedios",
                            "as": "Peso promedio lote",
                            "let": {
                                "nombre_lote": "$lote",
                                "anio_registro": { "$year": "$Fecha de puesta en caja" },
                                "mes_registro": { "$month": "$Fecha de puesta en caja" }
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] },
                                                { "$eq": ["$$anio_registro", "$Anio"] },
                                                { "$eq": ["$$mes_registro", "$Mes"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$Peso promedio lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$addFields": {
                            "Peso promedio lote": {
                                "$ifNull": ["$Peso promedio lote.Peso Promedio", 0]
                            }
                        }
                    },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "empleados_cosecha_adicionales": {
                                    "$filter": {
                                        "input": [
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 1", ""] },
                                                "reference": "$Empleado adicional 1 opcion",
                                                "value": "$Empleado adicional 1 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 1", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 2", ""] },
                                                "reference": "$Empleado adicional 2 opcion",
                                                "value": "$Empleado adicional 2 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 2", " -"] }, 0] }
                                            },
                                            {
                                                "_id": "",
                                                "name": { "$ifNull": ["$Empleado adicional 3", ""] },
                                                "reference": "$empleado Adicional opcion3",
                                                "value": "$Empleado adicional 3 cantidad",
                                                "code": { "$arrayElemAt": [{ "$split": ["$Empleado adicional 3", " -"] }, 0] }
                                            }


                                        ],
                                        "as": "empleados_cosecha_adicionales",
                                        "cond": {
                                            "$ne": ["$$empleados_cosecha_adicionales.name", ""]
                                        }
                                    }
                                }


                            }

                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleados_cosecha_adicionales.code",
                            "foreignField": "code",
                            "as": "info_empleado_adicional"
                        }
                    }

                    , {
                        "$addFields": {
                            "Empleados de Cosecha": {
                                "$concatArrays": [
                                    "$Empleados de Cosecha",
                                    {
                                        "$map": {
                                            "input": "$empleados_cosecha_adicionales",
                                            "as": "empleado_cosecha_adicional",
                                            "in": {
                                                "$mergeObjects": [
                                                    "$$empleado_cosecha_adicional",
                                                    {
                                                        "_id": {
                                                            "$reduce": {
                                                                "input": "$info_empleado_adicional",
                                                                "initialValue": null,
                                                                "in": {
                                                                    "$cond": {
                                                                        "if": {
                                                                            "$eq": [
                                                                                "$$empleado_cosecha_adicional.code",
                                                                                "$$this.code"
                                                                            ]
                                                                        },
                                                                        "then": {
                                                                            "$toString": "$$this._id"
                                                                        },
                                                                        "else": "$$value"
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Total Cortados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Cortador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Encallados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Encallador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Cortados y Encallados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "CortadorEncallador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            },
                            "Total Alzados": {
                                "$map": {
                                    "input": "$Empleados de Cosecha",
                                    "as": "item",
                                    "in": {
                                        "$cond": {
                                            "if": {
                                                "$eq": ["$$item.reference", "Alzador"]
                                            },
                                            "then": "$$item.value",
                                            "else": 0
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "Total Cortados": {
                                "$reduce": {
                                    "input": "$Total Cortados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Encallados": {
                                "$reduce": {
                                    "input": "$Total Encallados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Cortados y Encallados": {
                                "$reduce": {
                                    "input": "$Total Cortados y Encallados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            },
                            "Total Alzados": {
                                "$reduce": {
                                    "input": "$Total Alzados",
                                    "initialValue": 0,
                                    "in": {
                                        "$sum": ["$$this", "$$value"]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields":
                            {
                                "Peso aproximado Alzados": {
                                    "$multiply": [
                                        { "$ifNull": [{ "$toDouble": "$Total Alzados" }, 0] },
                                        { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                                    ]
                                }
                            }
                    }

                    , {
                        "$lookup": {
                            "from": "form_despachodecosecha",
                            "as": "form_despacho_cosecha",
                            "let": {
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "fecha": "$Fecha de puesta en caja"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$Codigo Vagon Asociado a Despacho", "$$vagon"] },
                                                { "$eq": ["$Numero de Viaje de Vagon", "$$num_viaje_vagon"] },
                                                {
                                                    "$gte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de Llenado" } } }
                                                    ]
                                                },

                                                {
                                                    "$lte": [
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                                        ,
                                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha fin de llenado" } } }
                                                    ]
                                                }


                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": 1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$form_despacho_cosecha",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    {
                        "$addFields":
                            {
                                "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Peso total segun ticket" }, -1] },
                                "Numero de ticket": { "$ifNull": [{ "$toDouble": "$form_despacho_cosecha.Numero de Ticket" }, -1] }
                            }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote",
                                "vagon": "$Codigo Vagon  Asociado a Despacho",
                                "num_viaje_vagon": "$Numero de Viaje de Vagon",
                                "ticket": "$Numero de ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_lote_viaje": {
                                "$sum": "$Peso aproximado Alzados"
                            },
                            "total_alzados_lote": {
                                "$sum": "$Total Alzados"
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "vagon": "$_id.vagon",
                                "num_viaje_vagon": "$_id.num_viaje_vagon",
                                "ticket": "$_id.ticket"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            },
                            "total_peso_racimos_alzados_viaje": {
                                "$sum": "$total_peso_racimos_alzados_lote_viaje"
                            }

                        }
                    },
                    {
                        "$unwind": "$data"
                    },

                    {
                        "$addFields":
                            {
                                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                                "total_alzados_lote": "$data.total_alzados_lote"
                            }
                    },

                    {
                        "$unwind": "$data.data"
                    },
                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                                        "total_alzados_lote": "$total_alzados_lote"
                                        , "(%) Alzados x lote": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                                },
                                                "then": 0,
                                                "else": {
                                                    "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields":
                            {
                                "Peso REAL Alzados": {
                                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$Total Peso despachados (ticket)" }, 0] }, "$(%) Alzados x lote"]

                                }
                            }
                    }

                    , {
                        "$addFields": {
                            "Peso REAL lote": {
                                "$cond": {
                                    "if": { "$eq": ["$Total Alzados", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                                    }
                                }
                            }
                        }
                    }
                    , {
                        "$project": {
                            "form_despacho_cosecha": 0,
                            "empleados_cosecha_adicionales": 0,
                            "info_empleado_adicional": 0,
                            "anio_filtro": 0
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_lotesfechasiembraspalma",
                            "as": "Siembra lote",
                            "let": {
                                "nombre_lote": "$lote"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$ne": [{ "$type": "$Lotes.features.properties.name" }, "missing"] },
                                                { "$in": ["$$nombre_lote", "$Lotes.features.properties.name"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    }
                    , { "$unwind": "$Siembra lote" }

                    , { "$addFields": { "Siembra lote": "$Siembra lote.Ao de Siembra" } }


                    , { "$unwind": "$Empleados de Cosecha" }

                    , {
                        "$addFields": {
                            "oid_empleado": { "$toObjectId": "$Empleados de Cosecha._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "oid_empleado",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , { "$unwind": "$info_empleado" }

                    , {
                        "$lookup": {
                            "from": "companies",
                            "localField": "info_empleado.cid",
                            "foreignField": "_id",
                            "as": "info_empresa"
                        }
                    }
                    , { "$unwind": "$info_empresa" }

                    , {
                        "$addFields": {
                            "Empresa empleado": "$info_empresa.name"
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "form_costodeactividadesxempresa",
                            "as": "costos_actividades_x_empresa",
                            "let": {
                                "fecha_siembra": "$Siembra lote",
                                "empresa": "$Empresa empleado"
                            },
                            "pipeline": [
                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$fecha_siembra", "$Fecha de Siembra"] },
                                                { "$eq": ["$$empresa", "$Empresa"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    "$sort": {
                                        "rgDate": -1
                                    }
                                },
                                {
                                    "$limit": 1
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$costos_actividades_x_empresa",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , {
                        "$addFields": {
                            "peso": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$Empleados de Cosecha.value" }, 0] }, "$Peso REAL lote"] },
                            "cargo": "$Empleados de Cosecha.reference"
                        }
                    }


                    , {
                        "$addFields": {
                            "precio_actividad": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE FRUTO" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE FRUTO" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE FRUTO" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            },

                            "precio_herramienta": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Cortador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Encallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "CortadorEncallador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.CORTE Y ENCALLADO DE HERRAMIENTA" }, 0] }
                                        }

                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Alzador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.ALCE DE HERRAMIENTA" }, 0] }
                                        }
                                        , {
                                            "case": { "$eq": ["$Empleados de Cosecha.reference", "Transportador"] },
                                            "then": { "$ifNull": [{ "$toDouble": "$costos_actividades_x_empresa.TRANSPORTE DE HERRAMIENTA" }, 0] }
                                        }
                                    ],
                                    "default": 0
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "costo_actividad": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_actividad" }, 0] }, "$peso"] },
                            "costo_herramienta": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$precio_herramienta" }, 0] }, "$peso"] }
                        }
                    }


                    , {
                        "$addFields": {
                            "costo": { "$sum": ["$costo_actividad", "$costo_herramienta"] }
                        }
                    }


                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    , {
                        "$project": {


                            "Finca": "$Finca",
                            "Lote": "$lote",
                            "Siembra": "$Siembra lote",
                            "Centro de costos": "Corte y recoleccion",
                            "Nombre Labor": "$cargo",
                            "Codigo Labor": "COSECHA",
                            "Estado Labor": {
                                "$cond": {
                                    "if": { "$eq": ["$ticket", -1] },
                                    "then": "💪 En progreso",
                                    "else": "✔ Listo"
                                }
                            },

                            "Fecha inicio": "$Fecha de puesta en caja",
                            "Fecha fin": "$Fecha de puesta en caja",

                            "Codigo empleado": "$info_empleado.code",
                            "Nombre empleado": { "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"] },
                            "Cedula": { "$ifNull": ["$info_empleado.numberID", "--"] },
                            "Empresa": "$Empresa empleado",
                            "Unidad": "Kilogramos",
                            "Cantidad": "$peso",
                            "vr unit trabajador": "$precio_actividad",
                            "vr unit herramienta": "$precio_herramienta",
                            "vr Total trabajador": "$costo_actividad",
                            "vr Total herramienta": "$costo_herramienta",
                            "TOTAL PAGO": "$costo",
                            "rgDate": "$Fecha de puesta en caja",
                            "centro costos presupuesto": "Corte y recoleccion",

                            "Anio": { "$year": "$Fecha de puesta en caja" },
                            "Mes": { "$month": "$Fecha de puesta en caja" },
                            "Semana": { "$week": "$Fecha de puesta en caja" }
                        }
                    }
                ]

            }
        }


        
        , {
            "$project":
                {
                    "datos": { "$concatArrays": ["$data_labores_x_empleado", "$data_liquidacion_cosecha"] }
                }
        }
        
        ,{ "$unwind": "$datos" }
        ,{ "$replaceRoot": { "newRoot": "$datos" } }


    ]