
//====================================

var cursor = db.tasks.aggregate(

    //--finca
    { $match: { "farm": ObjectId("5d2648a845a0dd2e9e204fe2") } }
    
    // // //1)----con cartografia duplicada (mapa)
    // , {
    //     $addFields: {
    //         "num_lotes": { $size: "$cartography.features" }
    //     }
    // }
    // , { $match: { "num_lotes": { $gt: 1 } } }
    
    
    
    //2)----con cartografia multiple (lista,mapa)
    ,{
        $match: {
            // //--A)con listay con mapa
            // "lots": {$ne:["[]"] },
            // "cartography.features":{$ne:[]}
            
            // //--B)con lista y sin mapa
            // "lots": {$ne:["[]"] },
            // "cartography.features":{$eq:[]}
            
            // //--C)sin lista y con mapa
            // "lots": {$eq:["[]"] },
            // "cartography.features":{$ne:[]}
            
            //--D)con mapa
            "cartography.features":{$ne:[]}
            
        }
    }
    
    
    // // //3)----sin cartografia ()
    // ,{
    //     $match: {
    //         "lots": ["[]"],
    //         "cartography.features":[]
    //     }
    // }
    
    


);

cursor

