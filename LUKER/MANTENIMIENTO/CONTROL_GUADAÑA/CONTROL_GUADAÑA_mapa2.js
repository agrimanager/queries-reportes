//--mapa
db.form_ciclocontrolguadanacacao.aggregate(
    [

        // //-----
        // { $unwind: "$Lote.features" },
        {
            "$addFields": {
                "today": new Date,
                // "Cartography": "$Lote.features"
            }
        },
        //-----

        {
            "$match": {
                "Ciclo control Guadaña": { "$ne": "" },
            }
        },

        {
            "$sort": {
                "Fecha Control": 1
            }
        },
        {
            "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }
        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }
        , {
            "$addFields": {
                "estado de ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo control Guadaña",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha inicio de ciclo": {
                    "$reduce": {
                        "input": "$data.Fecha Control",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "rgDate": {
                    "$reduce": {
                        "input": "$data.Fecha Control",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "Point": {
                    "$reduce": {
                        "input": "$data.Point",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },

        {
            "$addFields": {
                "penultimo_censo": {
                    "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                }
            }
        },

        {
            "$addFields": {
                "fecha finalizacion de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Finaliza"] },
                        "then": "$fecha inicio de ciclo",
                        "else": ""
                    }
                }

            }
        },

        {
            "$addFields": {
                "fecha inicio de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicia"] },
                        "then": "$fecha inicio de ciclo",
                        "else": "$penultimo_censo.Fecha Control"
                    }
                }

            }
        },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicia"] },
                        "then": -1,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },



        //🔄Ciclos 🎮Control ✂️Guadaña 🍫Cacao

        // Dias:#ffffff,En progreso:#666666,(<60):#00b050, [=60]:#bf8f00,(>60):#ff0000

        {
            "$addFields": {
                "estado de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicia"] },
                        "then": "En proceso",
                        "else": "Finalizado"
                    }
                },
                "color": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "#666666",
                        "else": {
                            "$cond": {
                                "if": { "$lt": ["$dias de ciclo", 60] },
                                "then": "#00b050",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$dias de ciclo", 60] },
                                        "then": "#bf8f00",
                                        "else": "#ff0000"
                                    }
                                }
                            }
                        }
                    }
                },
                // Dias:#ffffff,En progreso:#666666,(<60):#00b050, [=60]:#bf8f00,(>60):#ff0000
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "A-En progreso",
                        "else": {
                            "$cond": {
                                "if": { "$lt": ["$dias de ciclo", 60] },
                                "then": "(<60)",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$dias de ciclo", 60] },
                                        "then": "[=60]",
                                        "else": "(>60)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },





        {
            "$project": {
                "_id": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_lote",
                    "Estado del Ciclo": "$estado de ciclo",
                    "Rango ciclo": "$rango",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    { "$toString": "$dias de ciclo" },
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
                }
            }
        }
    ]


)