// db.form_registrodehumedad.find({})
//   .projection({})
//   .sort({_id:-1})


var cursor = db.form_registrodehumedad.aggregate(

    // { $match: { "farm": ObjectId("5d2648a845a0dd2e9e204fe2") } },

    //----con cartografia duplicadas
    {
        $addFields: {
            "num_lotes": { $size: "$Lote.features" }
        }
    }
    , { $match: { "num_lotes": { $gt: 1 } } }
    //, { $match: { "num_lotes": { $eq: 2 } } }

);

//cursor

//---ajecutar varias veces
cursor.forEach(i => {
    // console.log(i)
    
        db.form_registrodehumedad.update(
        	{
                "_id":i._id
        	},
        	{
        		$pop:{
                    "Lote.features":1
        		}
        	}
        )
});
