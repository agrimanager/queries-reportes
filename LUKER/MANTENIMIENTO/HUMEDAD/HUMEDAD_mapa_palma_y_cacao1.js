db.form_registrodehumedad.aggregate(
        [
        // //-----
        { $unwind: "$Lote.features" },
        {
            "$addFields": {
                "today": new Date,
                "Cartography": "$Lote.features"
            }
        },
       
        
        //{ "$match": { "Point.farm":"5d2648a845a0dd2e9e204fe2" } } ,
            
            
        {
            "$addFields": {
                "Promedio_Lectura": {
                    "$divide": [
                        { "$sum": ["$Lectura 1 30cm", "$Lectura 2 60cm"] },
                        2
                    ]
                }

            }
        },

        //Promedio_Lectura:#ffffff,[0-25):#FF0000,(25-35]:#ff8000,(35-45]:#ffff00,(45-70]:#3cff07,(70-95]:#008000,(95-100]:#1500e2,(>100):#353d40

        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lte": ["$Promedio_Lectura", 25] }] },
                        "then": "#FF0000",
                        "else": {
                            "$cond": {
                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                                "then": "#ff8000",
                                "else": {
                                    "$cond": {
                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                        "then": "#ffff00",
                                        "else": {
                                            "$cond": {
                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                                "then": "#3cff07",
                                                "else": {
                                                    "$cond": {
                                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                        "then": "#008000",
                                                        "else": {
                                                            "$cond": {
                                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                                "then": "#1500e2",
                                                                "else": "#353d40"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                 "rango": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lt": ["$Promedio_Lectura", 25] }] },
                        "then": "[0-25)",
                        "else": {
                            "$cond": {
                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                                "then": "(25-35]",
                                "else": {
                                    "$cond": {
                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                        "then": "(35-45]",
                                        "else": {
                                            "$cond": {
                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                                "then": "(45-70]",
                                                "else": {
                                                    "$cond": {
                                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                        "then": "(70-95]",
                                                        "else": {
                                                            "$cond": {
                                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                                "then": "(95-100]",
                                                                "else": "(>100)"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "_id": "$Cartography._id",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "color": "$color",
                    "lote": "$Cartography.properties.name",
                    "rango": "$rango"
                    
                },
                "geometry": "$Cartography.geometry"
            }
        }
    ]
)