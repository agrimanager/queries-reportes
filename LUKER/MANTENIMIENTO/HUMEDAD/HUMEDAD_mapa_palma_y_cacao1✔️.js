[


    {
        "$addFields": {
            "Promedio_Lectura": {
                "$divide": [
                    { "$sum": ["$Lectura 1 30cm", "$Lectura 2 60cm"] },
                    2
                ]
            }

        }
    },

    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lte": ["$Promedio_Lectura", 25] }] },
                    "then": "#FF0000",
                    "else": {
                        "$cond": {
                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                            "then": "#ff8000",
                            "else": {
                                "$cond": {
                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                    "then": "#ffff00",
                                    "else": {
                                        "$cond": {
                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                            "then": "#3cff07",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                    "then": "#008000",
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                            "then": "#1500e2",
                                                            "else": "#353d40"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
             "rango": {
                "$cond": {
                    "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lt": ["$Promedio_Lectura", 25] }] },
                    "then": "[0-25)",
                    "else": {
                        "$cond": {
                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                            "then": "(25-35]",
                            "else": {
                                "$cond": {
                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                    "then": "(35-45]",
                                    "else": {
                                        "$cond": {
                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                            "then": "(45-70]",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                    "then": "(70-95]",
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                            "then": "(95-100]",
                                                            "else": "(>100)"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },


    {
        "$project": {
            "_id": "$Cartography._id",
            "idform": "$idform",
            "type": "Feature",
            "properties": {
                "color": "$color",
                "lote": "$Cartography.properties.name",
                "rango": "$rango"

            },
            "geometry": "$Cartography.geometry"
        }
    }
]