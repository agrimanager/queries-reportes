db.form_registrodehumedad.aggregate(
    [
        //Cultivo
        {
            "$addFields": {
                "cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$Point.farm", "5d2648a845a0dd2e9e204fe2"] },
                        "then": "PALMA",
                        "else": "CACAO"
                    }
                }
            }
        },

        //--Finca
        //---palma
        { "$match": { "Point.farm": { "$eq": "5d2648a845a0dd2e9e204fe2" } } },
        //---cacao
        // { "$match": { "Point.farm": {  "$ne": "5d2648a845a0dd2e9e204fe2" }} },



        { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$unwind": "$Lote.features" },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Lote.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.type", "Farm"] },
                        "then": "$feature_1",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.type", "Farm"] },
                                "then": "$feature_2",
                                "else": "$feature_3"
                            }
                        }
                    }
                }
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        { "$unwind": "$finca" },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },



        {
            "$addFields": {
                "bloque": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                        "then": "$feature_1.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                "then": "$feature_2.properties.name",
                                "else": "$feature_3.properties.name"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0
            }
        }


        , {
            "$addFields": {
                "Promedio_Lectura": {
                    "$divide": [
                        { "$sum": ["$Lectura 1 30cm", "$Lectura 2 60cm"] },
                        2
                    ]
                }

            }
        }

        , {
            "$addFields": {
                 "rango": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lt": ["$Promedio_Lectura", 25] }] },
                        "then": "[0-25)",
                        "else": {
                            "$cond": {
                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                                "then": "(25-35]",
                                "else": {
                                    "$cond": {
                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                        "then": "(35-45]",
                                        "else": {
                                            "$cond": {
                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                                "then": "(45-70]",
                                                "else": {
                                                    "$cond": {
                                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                        "then": "(70-95]",
                                                        "else": {
                                                            "$cond": {
                                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                                "then": "(95-100]",
                                                                "else": "(>100)"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote",
                    "cultivo": "$cultivo",
                    "nombre_finca": "$finca"//--AGREGADO
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "$$cultivo"] },
                                    { "$eq": ["$FINCA", "$$nombre_finca"] },//--AGREGADO
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    // {
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": true
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }


        , {
            "$lookup": {
                "from": "form_cicloderiegopalma",
                "as": "ciclo_riego",
                "let": {
                    "aux_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$in": ["$$aux_lote", "$Lote.features.properties.name"] },
                                    { "$eq": ["$Ciclo Riego", "finaliza"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": 1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$ciclo_riego",
                "preserveNullAndEmptyArrays": true
            }
        }
        , {
            "$addFields": {
                "fecha de finalizacion": "$ciclo_riego.Fecha ciclo"
            }
        }


        , {
            "$project": {
                "ciclo_riego": 0
            }
        }

        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": {
                    "$divide": [
                        {
                            "$subtract": [
                                "$fecha de finalizacion",
                                "$Fecha de Registro"
                            ]
                        },
                        86400000
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": {
                    "$cond": {
                        "if": { "$ne": ["$dias_despues_ultimo_ciclo_riego", null] },
                        "then": "$dias_despues_ultimo_ciclo_riego",
                        "else": 0
                    }
                }
            }
        }



        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": { "$divide": [{ "$subtract": [{ "$multiply": ["$dias_despues_ultimo_ciclo_riego", 100] }, { "$mod": [{ "$multiply": ["$dias_despues_ultimo_ciclo_riego", 100] }, 1] }] }, 100] }
            }
        }

        , {
            "$sort": {
                "fecha de finalizacion": -1
            }
        }



    ]

)