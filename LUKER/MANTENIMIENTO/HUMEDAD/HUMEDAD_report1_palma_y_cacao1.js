db.form_registrodehumedad.aggregate(
    [
        { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
        { "$match": { "anio_filtro": { "$gt": 2000 } } },
        { "$match": { "anio_filtro": { "$lt": 3000 } } },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        }
        
        ,{ "$unwind": "$finca" }
        
        
        ,{
            "$addFields": {
                "_id_str_farm": { "$toString": "$finca._id" }
            }
        }

        , { "$match": { "_id_str_farm": { "$in": ["5d2648a845a0dd2e9e204fe2", "5d26491264f5b87ffc809eba"] } } }
        //, { "$match": { "_id_str_farm":"5d2648a845a0dd2e9e204fe2" } } 

        , {
            "$addFields": {
                "cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$_id_str_farm", "5d2648a845a0dd2e9e204fe2"] },
                        "then": "PALMA",
                        "else": "CACAO"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "finca": "$finca.name"
            }
        }
        // ,{ "$unwind": "$finca" }


        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        , {
            "$addFields": {
                "Promedio_Lectura": {
                    "$divide": [
                        { "$sum": ["$Lectura 1 30cm", "$Lectura 2 60cm"] },
                        2
                    ]
                }

            }
        }

        , {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lt": ["$Promedio_Lectura", 25] }] },
                        "then": "A-[0-25)",
                        "else": {
                            "$cond": {
                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                                "then": "B-(25-35]",
                                "else": {
                                    "$cond": {
                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                        "then": "C-(35-45]",
                                        "else": {
                                            "$cond": {
                                                "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                                "then": "D-(45-70]",
                                                "else": {
                                                    "$cond": {
                                                        "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                        "then": "E-(70-100]",
                                                        "else": "F-(>100)"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_cicloderiegopalma",
                "as": "ciclo_riego",
                "let": {
                    "aux_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$in": ["$$aux_lote", "$Lote.features.properties.name"] },
                                    { "$eq": ["$Ciclo Riego", "finaliza"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": 1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$ciclo_riego",
                "preserveNullAndEmptyArrays": true
            }
        }
        , {
            "$addFields": {
                "fecha de finalizacion": "$ciclo_riego.Fecha ciclo"
            }
        }


        , {
            "$project": {
                "ciclo_riego": 0
            }
        }

        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": {
                    "$divide": [
                        {
                            "$subtract": [
                                "$fecha de finalizacion",
                                "$Fecha de Registro"
                            ]
                        },
                        86400000
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": {
                    "$cond": {
                        "if": { "$ne": ["$dias_despues_ultimo_ciclo_riego", null] },
                        "then": "$dias_despues_ultimo_ciclo_riego",
                        "else": 0
                    }
                }
            }
        }



        , {
            "$addFields": {
                "dias_despues_ultimo_ciclo_riego": { "$divide": [{ "$subtract": [{ "$multiply": ["$dias_despues_ultimo_ciclo_riego", 100] }, { "$mod": [{ "$multiply": ["$dias_despues_ultimo_ciclo_riego", 100] }, 1] }] }, 100] }
            }
        }

        , {
            "$sort": {
                "fecha de finalizacion": -1
            }
        }

        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote",
                    "cultivo": "$cultivo"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "$$cultivo"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }

    ]

)