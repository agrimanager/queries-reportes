[

    { "$addFields": { "anio_filtro": { "$year": "$Fecha de Registro" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },

    {
        "$addFields": {
            "Promedio_Lectura": {
                "$divide": [
                    { "$sum": ["$Lectura 1 30cm", "$Lectura 2 60cm"] },
                    2
                ]
            }

        }
    },

    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lte": ["$Promedio_Lectura", 25] }] },
                    "then": "#FF0000",
                    "else": {
                        "$cond": {
                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                            "then": "#ff8000",
                            "else": {
                                "$cond": {
                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                    "then": "#ffff00",
                                    "else": {
                                        "$cond": {
                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                            "then": "#3cff07",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                    "then": "#008000",
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                            "then": "#1500e2",
                                                            "else": "#353d40"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": { "$and": [{ "$gte": ["$Promedio_Lectura", 0] }, { "$lt": ["$Promedio_Lectura", 25] }] },
                    "then": "[0-25)",
                    "else": {
                        "$cond": {
                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 25] }, { "$lte": ["$Promedio_Lectura", 35] }] },
                            "then": "(25-35]",
                            "else": {
                                "$cond": {
                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 35] }, { "$lte": ["$Promedio_Lectura", 45] }] },
                                    "then": "(35-45]",
                                    "else": {
                                        "$cond": {
                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 45] }, { "$lte": ["$Promedio_Lectura", 70] }] },
                                            "then": "(45-70]",
                                            "else": {
                                                "$cond": {
                                                    "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 70] }, { "$lte": ["$Promedio_Lectura", 95] }] },
                                                    "then": "(70-95]",
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$and": [{ "$gt": ["$Promedio_Lectura", 95] }, { "$lte": ["$Promedio_Lectura", 100] }] },
                                                            "then": "(95-100]",
                                                            "else": "(>100)"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$project": {
            "_id": "$Cartography._id",
            "idform": "$idform",
            "type": "Feature",
            "properties": {
                "color": "$color",
                "mes": "$Mes_Txt",
                "lote": "$Cartography.properties.name",
                "rango": "$rango"

            },
            "geometry": "$Cartography.geometry"
        }
    }
]