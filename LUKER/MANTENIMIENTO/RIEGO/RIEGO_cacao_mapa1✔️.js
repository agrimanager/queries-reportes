 [
       
        {
            "$sort": {
                "Fecha ciclo": 1
            }
        },
         {
          "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        }
        , {
            "$group": {
                "_id": {
                    "nombre_lote": "$Cartography.features.properties.name",
                    "today": "$today",
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }
        , {
            "$addFields": {
                "estado de ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo de riego",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha inicio de ciclo": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "rgDate": {
                    "$reduce": {
                        "input": "$data.Fecha ciclo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "Point": {
                    "$reduce": {
                        "input": "$data.Point",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },

        {
            "$addFields": {
                "penultimo_censo": {
                    "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                }
            }
        },

        {
            "$addFields": {
                "fecha finalizacion de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "finaliza"] },
                        "then": "$fecha inicio de ciclo",
                        "else": ""
                    }
                }

            }
        },

        {
            "$addFields": {
                "fecha inicio de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicio"] },
                        "then": "$fecha inicio de ciclo",
                        "else": "$penultimo_censo.Fecha ciclo"
                    }
                }

            }
        },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicio"] },
                        "then": -1,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },
        
        
        

        {
            "$addFields": {
                "estado de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "Inicio"] },
                        "then": "En proceso",
                        "else": "Finalizado"
                    }
                },
                "color": {
                    "$cond": {
                        "if": {
                             "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "#808080",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lt": ["$dias de ciclo", 15] }]
                                },
                                "then":  "#00BFFF",
                                "else": "#FF0000"
                                 
                                }
                                    
                            }
                        }
                    }
                }
            },
      
        
        {
            "$project": {
                "_id": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.nombre_lote",
                    "Estado del Ciclo": "$estado de ciclo",
                    "Dias Ciclo": {
                        "$cond": {
                            "if": { "$eq": ["$dias de ciclo", -1] },
                            "then": "-1",
                            "else": {
                                "$concat": [
                                    {"$toString": "$dias de ciclo"},
                                    " dias"
                                ]
                            }
                        }
                    },
                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
                }
            }
        }
    ]