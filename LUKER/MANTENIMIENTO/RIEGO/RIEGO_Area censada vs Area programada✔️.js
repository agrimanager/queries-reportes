[
        { "$project": { "_id": "$_id" } }

        , {
            "$lookup": {
                "from": "tasks",
                "as": "labores",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [
                    {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },


                    {
                        "$addFields": {
                            "activity": "$activity.name"
                        }
                    },



                    {
                        "$match": {
                            "activity": {
                                "$in":
                                    [
                                        "RIEGO PLANTACION CACAO-Agricola El Poleo-V C C",
                                        "SERVICIO AGRONOMIA RIEGO CACAO-Agricola El Poleo-V C C",
                                        "SERVICIO DE RIEGO CACAO-Agricola El Poleo-V C C",
                                        "MAQUINARIA TRACTOR GRANDE LABORES RIEGO-Agroindustria Feleda-P V C",
                                        "RIEGO PLANTACION-Agroindustria Feleda-P V C",
                                        "RIEGO PLANTACION CACAO-Agroindustria Feleda-V C C",
                                        "SERVICIO DE RIEGO-Agroindustria Feleda-P V C",
                                        "SERVICIO DE RIEGO 1-Agroindustria Feleda-P V C",
                                        "SERVICIO DE RIEGO CACAO-Agroindustria Feleda-V C C",
                                        "SERVICIO LABORES DE RIEGO-Agroindustria Feleda-P V C",
                                        "SERVICIO SUPERVISION RIEGO Y DRENAJES-Agroindustria Feleda-P V C",
                                        "SERVICIO AGRONOMIA RIEGO-CACAO-Nexarte Servicios temporales S.A-V C C",
                                        "RIEGO PLANTACION-LUKER AGRICOLA-P V C",
                                        "RIEGO PLANTACION CACAO-LUKER AGRICOLA-V C C",
                                        "FERTIRRIEGO-Agricola El Poleo-N C A",
                                        "SERVICIO TRACTOR - RIEGO-Agricola El Poleo-N C A",
                                        "OPERACION DE RIEGO -Agricola El Poleo-N C A",
                                        "OPERADOR MOTORES DE RIEGO-Agricola El Poleo-N C A",
                                        "MANTENIMIENTO SISTEMA DE RIEGO-Agricola El Poleo-N C A",
                                        "OPERARIO SISTEMA RIEGO-Nexarte Servicios temporales S.A-G C H",
                                        "AUX SISTEMA DE RIEGO-Nexarte Servicios temporales S.A-G C H",
                                        "MAQUINARIA TRACTR GRANDE LABORES RIEGO CACAO-Agroindustria Feleda-P V C"


                                    ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "farm_str": { "$toString": "$farm" }
                            , "finca": "Palma Villanueva Casanare"
                        }
                    }
                    , {
                        "$match": {
                            "farm_str": "5d2648a845a0dd2e9e204fe2"
                        }
                    }


                    , {
                        "$match": {
                            "cartography.features": { "$ne": [] }
                        }
                    }
                    , { "$unwind": "$cartography.features" }

                    , {
                        "$addFields": {
                            "lote": "$cartography.features.properties.name"
                        }
                    }



                    , {
                        "$addFields": {
                            "fecha": {
                                "$arrayElemAt": [
                                    "$when",
                                    { "$subtract": [{ "$size": "$when" }, 1] }
                                ]
                            }
                        }
                    }
                    , {
                        "$addFields": {
                            "fecha": "$fecha.start"
                        }
                    }
                    , {
                        "$addFields": {
                            "anio": { "$year": "$fecha" },
                            "num_mes": { "$month": "$fecha" },
                            "semana": { "$week": "$fecha" }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "lote": "$lote",

                                "anio": "$anio",
                                "num_mes": "$num_mes"

                            }
                            , "fecha_min": { "$min": "$fecha" }
                        }
                    }

                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id",
                                    {
                                        "fecha_min": "$fecha_min",
                                        "tipo": "programado_labores"
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }

        , {
            "$lookup": {
                "from": "form_cicloderiegopalma",
                "as": "censo",
                "let": {
                    "id_usr": "$_id"
                },
                "pipeline": [

                    {
                        "$sort": {
                            "Fecha ciclo": 1
                        }
                    },

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "Finca._id",
                            "foreignField": "_id",
                            "as": "Finca"
                        }
                    },
                    { "$unwind": "$Finca" },
                    {
                        "$addFields": {
                            "Finca": "$Finca.name",
                            "Bloque": "$Bloque.properties.name",
                            "lote": "$lote.properties.name"

                        }
                    },
                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }
                    , {
                        "$group": {
                            "_id": {
                                "finca": "$Finca",
                                "lote": "$lote",
                                "today": "$today"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "estado de ciclo": {
                                "$reduce": {
                                    "input": "$data.Ciclo Riego",
                                    "initialValue": "",
                                    "in": "$$this"
                                }
                            },
                            "fecha inicio de ciclo": {
                                "$reduce": {
                                    "input": "$data.Fecha ciclo",
                                    "initialValue": "",
                                    "in": "$$this"
                                }
                            },
                            "rgDate": {
                                "$reduce": {
                                    "input": "$data.Fecha ciclo",
                                    "initialValue": "",
                                    "in": "$$this"
                                }
                            },
                            "Point": {
                                "$reduce": {
                                    "input": "$data.Point",
                                    "initialValue": "",
                                    "in": "$$this"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "penultimo_censo": {
                                "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "fecha finalizacion de ciclo": {
                                "$cond": {
                                    "if": { "$eq": ["$estado de ciclo", "finaliza"] },
                                    "then": "$fecha inicio de ciclo",
                                    "else": ""
                                }
                            }

                        }
                    },

                    {
                        "$addFields": {
                            "fecha inicio de ciclo": {
                                "$cond": {
                                    "if": { "$eq": ["$estado de ciclo", "inicia"] },
                                    "then": "$fecha inicio de ciclo",
                                    "else": "$penultimo_censo.Fecha ciclo"
                                }
                            }

                        }
                    },


                    {
                        "$addFields": {
                            "dias de ciclo": {
                                "$cond": {
                                    "if": { "$eq": ["$estado de ciclo", "inicia"] },
                                    "then": -1,
                                    "else": {
                                        "$floor": {
                                            "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                                        }
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "estado de ciclo": {
                                "$cond": {
                                    "if": { "$eq": ["$estado de ciclo", "inicia"] },
                                    "then": "En proceso",
                                    "else": "Finalizado"
                                }
                            }
                        }
                    },


                    {
                        "$project": {
                            "_id": 0,
                            "finca": "$_id.finca",
                            "lote": "$_id.lote",
                            "estado de ciclo": "$estado de ciclo",
                            "fecha inicio de ciclo": "$fecha inicio de ciclo",
                            "fecha final de ciclo": "$fecha finalizacion de ciclo",
                            "dias de ciclo": "$dias de ciclo",
                            "Point": "$Point",
                            "rgDate": "$rgDate"
                        }
                    }


                    , {
                        "$project": {
                            "finca": "$finca",
                            "lote": "$lote",

                            "anio": { "$year": "$fecha inicio de ciclo" },
                            "num_mes": { "$month": "$fecha inicio de ciclo" },
                            "fecha_min": "$fecha inicio de ciclo",

                            "tipo": "ejecutado_censo"
                        }
                    }

                ]

            }
        }


        , {
            "$project":
                {
                    "datos": {
                        "$concatArrays": [
                            "$labores"
                            , "$censo"
                        ]
                    }
                }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        , {
            "$addFields": {
                "rgDate": "$fecha_min"
            }
        }
        , {
            "$addFields": {
                "fecha_min": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_min" }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] },
                                    { "$eq": ["$CULTIVO", "PALMA"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },



        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },

        {
            "$project": {
                "referencia_siembras": 0
            }
        }

        , {
            "$addFields": {
                "mes": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }



        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "lote": "$lote",
                    "anio": "$anio",
                    "num_mes": "$num_mes",
                    "mes": "$mes",

                    "Hectareas": "$Hectareas",
                    "Siembra": "$Siembra",
                    "num_palmas": "$num_palmas",
                    "Material": "$Material"

                },
                "data": {
                    "$push": "$$ROOT"
                }
                , "rgDate": {
                    "$min": "$rgDate"
                }
            }
        }



        , {
            "$addFields": {
                "tipo_filtro_programado": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.tipo", "programado_labores"]
                        }
                    }
                },
                "tipo_filtro_ejecucion": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": {
                            "$eq": ["$$item.tipo", "ejecutado_censo"]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "tipo_filtro_programado": {
                    "$cond": {
                        "if": { "$gt": [{ "$size": "$tipo_filtro_programado" }, 0] },
                        "then": "SI",
                        "else": "NO"
                    }
                },
                "tipo_filtro_ejecucion": {
                    "$cond": {
                        "if": { "$gt": [{ "$size": "$tipo_filtro_ejecucion" }, 0] },
                        "then": "SI",
                        "else": "NO"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "ha_programado": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_filtro_programado", "SI"] },
                        "then": "$_id.Hectareas",
                        "else": 0
                    }
                },
                "ha_ejecucion": {
                    "$cond": {
                        "if": { "$eq": ["$tipo_filtro_ejecucion", "SI"] },
                        "then": "$_id.Hectareas",
                        "else": 0
                    }
                }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "tipo_filtro_programado": "$tipo_filtro_programado",
                            "tipo_filtro_ejecucion": "$tipo_filtro_ejecucion",
                            "ha_programado": "$ha_programado",
                            "ha_ejecucion": "$ha_ejecucion",

                            "rgDate": "$rgDate"
                        }
                    ]
                }
            }
        }
    ]