[
        
        {
            "$sort": {
                "Fecha plateo": 1
            }
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$addFields": {
                "Finca": "$Finca.name",
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"

            }
        },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "today": "$today"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$addFields": {
                "estado de ciclo": {
                    "$reduce": {
                        "input": "$data.Ciclo plateo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "fecha inicio de ciclo": {
                    "$reduce": {
                        "input": "$data.Fecha plateo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "rgDate": {
                    "$reduce": {
                        "input": "$data.Fecha plateo",
                        "initialValue": "",
                        "in": "$$this"
                    }
                },
                "Point": {
                    "$reduce": {
                        "input": "$data.Point",
                        "initialValue": "",
                        "in": "$$this"
                    }
                }
            }
        },

        {
            "$addFields": {
                "penultimo_censo": {
                    "$arrayElemAt": ["$data", { "$subtract": [{ "$size": "$data" }, 2] }]
                }
            }
        },

        {
            "$addFields": {
                "fecha finalizacion de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "finaliza"] },
                        "then": "$fecha inicio de ciclo",
                        "else": ""
                    }
                }

            }
        },

        {
            "$addFields": {
                "fecha inicio de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": "$fecha inicio de ciclo",
                        "else": "$penultimo_censo.Fecha plateo"
                    }
                }

            }
        },


        {
            "$addFields": {
                "dias de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": -1,
                        "else": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$_id.today", "$fecha finalizacion de ciclo"] }, 86400000]
                            }
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "estado de ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$estado de ciclo", "inicia"] },
                        "then": "En proceso",
                        "else": "Finalizado"
                    }
                }
            }
        },


        {
            "$project": {
                "_id": 0,
                "lote": "$_id.lote",
                "estado de ciclo": "$estado de ciclo",
                "fecha inicio de ciclo": "$fecha inicio de ciclo",
                "fecha final de ciclo": "$fecha finalizacion de ciclo",
                "dias de ciclo": "$dias de ciclo",
                "supervisor":"$penultimo_censo.supervisor",
                "Point": "$Point",
                "rgDate": "$rgDate"
            }
        },
        {
            "$lookup": {
                "from": "form_cargueinformaciondeplantas",
                "as": "referencia_siembras",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$CULTIVO", "PALMA"] },
                                    { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                                ]
                            }
                        }
                    },

                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$referencia_siembras",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "Hectareas": "$referencia_siembras.HECTAREAS",
                "Siembra": "$referencia_siembras.SIEMBRA",
                "num_palmas": "$referencia_siembras.PALMAS",
                "Material": "$referencia_siembras.MATERIAL"
            }
        },
        
        {
            "$project": {
                "referencia_siembras": 0
            }
        },
        
        {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$dias de ciclo", -1]
                        },
                        "then": "A-En progreso",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lt": ["$dias de ciclo", 30] }]
                                },
                                "then": "B-Normal [0-30)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$dias de ciclo", 30] }, { "$lte": ["$dias de ciclo", 60] }]
                                        },
                                        "then": "C-Medio [30-60]",
                                        "else": "D-Critico (>60)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        

        

    ]