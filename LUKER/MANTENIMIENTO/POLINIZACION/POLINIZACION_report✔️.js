
[
    {
        "$match": {
            "Lote.path": { "$ne": "" }
        }
    },
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }

        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"

        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },

    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }


    , {
        "$addFields": {
            "anio": { "$year": "$rgDate" },
            "num_mes": { "$month": "$rgDate" },
            "semana": { "$week": "$rgDate" }
        }
    }

    , {
        "$group": {
            "_id": {
                "Finca": "$Finca",
                "Bloque": "$Bloque",
                "lote": "$lote",

                "anio": "$anio",
                "num_mes": "$num_mes"

            },

            "fecha_min": { "$min": "$rgDate" },

            "suma_pase1": { "$sum": "$Pase 1" },
            "suma_pase2": { "$sum": "$Pase 2" },
            "suma_pase3": { "$sum": "$Pase 3" }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$_id.num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$_id.num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$_id.num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$_id.num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$_id.num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$_id.num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$_id.num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$_id.num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$_id.num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$_id.num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$_id.num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$_id.num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "nombre_lote": "$_id.lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "PALMA"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$LOTE.features.properties.name"] }
                            ]
                        }
                    }
                },

                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "MATERIAL": "$referencia_siembras.MATERIAL",
            "SIEMBRA": "$referencia_siembras.SIEMBRA",
            "PALMAS": "$referencia_siembras.PALMAS",
            "HECTAREAS": "$referencia_siembras.HECTAREAS"
        }
    }

    , {
        "$project": {
            "referencia_siembras": 0
        }
    }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "rgDate": "$fecha_min",
                        "suma_pase1": "$suma_pase1",
                        "suma_pase2": "$suma_pase2",
                        "suma_pase3": "$suma_pase3",
                        "MATERIAL": "$MATERIAL",
                        "SIEMBRA": "$SIEMBRA",
                        "PALMAS": "$PALMAS",
                        "HECTAREAS": "$HECTAREAS",

                        "pase1_Ha": { "$divide": ["$suma_pase1", "$HECTAREAS"] }
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "pase1_Ha": { "$divide": [{ "$subtract": [{ "$multiply": ["$pase1_Ha", 100] }, { "$mod": [{ "$multiply": ["$pase1_Ha", 100] }, 1] }] }, 100] }
        }
    }
    
    
   ,{
        "$addFields": {
            "rango": {
                "$cond": {
                    "if": {
                        "$lt": ["$pase1_Ha", 130]
                    },
                    "then": "A-(<129)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$pase1_Ha", 130] }, { "$lte": ["$pase1_Ha", 180] }]
                            },
                            "then": "B-[130-180]",
                            "else": "C-(>180)"
                        }
                    }
                }
            }
        }
    }

]

