
[
    
    {
        "$group": {
            "_id": {
                "idform": "$idform",
                "Cartography": "$Cartography"
            },
            "suma_pase1": {
                "$sum": "$Pase 1"
            }
        }
    }

    , {
        "$lookup": {
            "from": "form_cargueinformaciondeplantas",
            "as": "referencia_siembras",
            "let": {
                "str_id_lote": "$_id.Cartography._id"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$CULTIVO", "PALMA"] },
                                { "$ne": [{ "$type": "$LOTE.features.properties.name" }, "missing"] },
                                { "$in": ["$$str_id_lote", "$LOTE.features._id"] }
                            ]
                        }
                    }
                },

                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$referencia_siembras",
            "preserveNullAndEmptyArrays": false
        }
    }

    , {
        "$addFields": {
            "HECTAREAS": { "$ifNull": ["$referencia_siembras.HECTAREAS", 0] }
        }
    }


    , {
        "$addFields": {
            "Indicador": { "$divide": ["$suma_pase1", "$HECTAREAS"] }
        }
    },
    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$lt": ["$Indicador", 130]
                    },
                    "then": "#ff8000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$Indicador", 130] }, { "$lte": ["$Indicador", 180] }]
                            },
                            "then": "#f0f200",
                            "else": "#0B610B"
                        }
                    }
                }
            }
        }
    },

    {
        "$project": {
            "_id": "$_id.Cartography._id",
            "geometry": "$_id.Cartography.geometry",
            "idform": "$_id.idform",
            "type": "Feature",

            "properties": {
                "color": "$color",
                "lote": "$_id.Cartography.properties.name"
            }
        }
    }


]

