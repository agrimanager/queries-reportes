db.users.aggregate(
    [


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_modulopolinizacioninductores",
                "as": "data",
                "let": {
                    
                },
                
                "pipeline": []
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)