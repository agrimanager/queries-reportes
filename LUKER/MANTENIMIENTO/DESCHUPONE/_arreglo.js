
//--corregir ortografia
db.form_ciclodeschuponecacao.updateMany(
    {
        "Ciclo Deschupone": "inicio"
    }, {
        $set: {
            "Ciclo Deschupone": "Inicia"
        }
    }

);



//--corregir fechas x rgDate

var cursor = db.form_ciclodeschuponecacao.aggregate(

    {
        $match: {
            "Fecha de Deschupone" : ""
        }
    }

);

cursor.forEach(i => {
    db.form_ciclodeschuponecacao.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Fecha de Deschupone" : i.rgDate
            }
        }
    )
});


