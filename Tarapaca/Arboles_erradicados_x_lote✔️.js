[


        {
            "$lookup": {
                "from": "cartography",
                "as": "data_cartografia",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    }



                    , {
                        "$project": {
                            "arbol_nombre": "$properties.name",
                            "arbol_erradicada": "$properties.custom.Erradicada.value",
                            "arbol_path": "$path"

                            ,"rgDate":"$$filtro_fecha_inicio"
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }



                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data_cartografia"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }




        , {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$arbol_path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
            }
        },

        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name"

                , "arbol": "$arbol_nombre"
                , "arbol_erradicado": "$arbol_erradicada"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        { "$unwind": "$Finca" }


        , {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        }


        , {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "arbol_path": 0
                , "arbol_nombre": 0
                , "arbol_erradicada": 0
            }
        }




    ]