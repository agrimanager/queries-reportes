//--parte1
Los colores serían: 

0% : Verde claro
0,1%-0,9%; Verde oscuro
1% a 1,9% : amarillo
2% a 2,9% naranja
>3%: rojo


//---ejemplo
Blanco:#FFFFFF
Gris:#9c9c9c
Verde Claro:#00FF00
Verde Oscuro:   #008000}
Amarillo claro: #ffff00
Amarillo Oscuro:#e2ba1f
Naranja:#ff8000
Cafe:#7B3F32
Rojo:#ff0000 
Azul:#1a73e8



//--parte3
%Incidencia: #ffffff,
[=0%]:#00FF00,
(0% - 1%):#e2ba1f,
[1% - 2%):#ffff00,
[2% - 3%):#ff8000,
(>=3%):#ff0000


//--parte4
%Incidencia: #ffffff,[=0%]:#00FF00,(0% - 1%):#e2ba1f,[1% - 2%):#ffff00,[2% - 3%):#ff8000,(>=3%):#ff0000
