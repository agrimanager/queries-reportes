//--mapa
db.form_modulodeenfermedades.aggregate(
    [

        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------




        //===== CONDICIONES BASE
        {
            "$match": {
                "Enfermedad": { "$in": ["anillo rojo", "Anillo Rojo"] }
                // "Enfermedad": { "$in": ["Pudricin de Estipe"] }
                // "Enfermedad": { "$in": ["Pudrición de cogollo PC", "pudricion de cogollo pc"] }

            }
        },
        {
            "$addFields": {
                "Enfermedad": "Anillo Rojo"
                //"Enfermedad":"Pudrición de Estipe"
                // "Enfermedad":"Pudrición de cogollo PC"
            }
        },



        //=========== MAPA ==============
        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },
        //===============================



        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        //=========== MAPA ==============
        {
            "$addFields": {
                "Cartography": "$lote"
            }
        },
        //===============================



        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Arbol": 0
                , "Point": 0

                , "Formula": 0
                , "Sampling": 0
            }
        }


        //====== FECHAS
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }


        //====== FECHAS filtro busqueda
        , {
            "$addFields": {
                "anio_busqueda_inicio": { "$year": { "date": "$Busqueda inicio" } },
                "num_mes_busqueda_inicio": { "$month": { "date": "$Busqueda inicio" } },

                "anio_busqueda_fin": { "$year": { "date": "$Busqueda fin" } },
                "num_mes_busqueda_fin": { "$month": { "date": "$Busqueda fin" } }
            }
        }



        //====== INFO LOTE
        , {
            "$lookup": {
                "from": "form_inventariopalmasiniciales",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales",
                "Hectareas": "$info_lote.Hectareas Iniciales",
                "Material": "$info_lote.Material Siembra"
            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }






        // //====== AGRUPACION MULTIPLE

        //----plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"

                    , "anio": "$anio"
                    , "num_mes": "$num_mes"

                    , "anio_busqueda_fin": "$anio_busqueda_fin"
                    , "num_mes_bueda_fin": "$num_mes_busqueda_fin"

                    , "num_palmas": "$num_palmas"

                    //---!!!FILTRO FINAL
                    , "rgDate": "$Busqueda fin"
                }
                //=========== MAPA ==============
                , "data": { "$push": "$$ROOT" }
                //===============================

            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                    //"planta": "$planta"

                    , "anio": "$_id.anio"
                    , "num_mes": "$_id.num_mes"


                    , "anio_busqueda_fin": "$_id.anio_busqueda_fin"
                    , "num_mes_busqueda_fin": "$_id.num_mes_busqueda_fin"

                    , "num_palmas": "$_id.num_palmas"

                    //---!!!FILTRO FINAL
                    , "rgDate": "$_id.rgDate"
                }
                , "plantas_dif_censadas_x_lote_x_anio_x_mes": { "$sum": 1 }
                //=========== MAPA ==============
                , "data": { "$push": "$$ROOT" }
                //===============================
            }
        }

        //=====FECHA MES
        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$_id.num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$_id.num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$_id.num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$_id.num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$_id.num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$_id.num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$_id.num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$_id.num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$_id.num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$_id.num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$_id.num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$_id.num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }




        //---proceso1
        , {
            "$addFields": {
                "pct_incidencia_mes": {
                    "$cond": {
                        "if": { "$eq": ["$_id.num_palmas", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_anio_x_mes", "$_id.num_palmas"] }, 100]
                        }
                    }
                }
            }
        }


        //---2 decimales
        , {
            "$addFields": {
                "pct_incidencia_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia_mes", 100] }, 1] }] }, 100] }
            }
        }


        //=========== MAPA FINAL ==============
        //*************************************

        , {
            "$addFields": {
                "indicador": "$pct_incidencia_mes"
            }
        }

        //%Incidencia: #ffffff,[=0%]:#00FF00,(0% - 1%):#e2ba1f,[1% - 2%):#ffff00,[2% - 3%):#ff8000,(>=3%):#ff0000
        //%Incidencia: #ffffff,:#,:#,:#,:#,:#

        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$eq": ["$indicador", 0] },
                        "then": "#00FF00",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }]
                                },
                                "then": "#e2ba1f",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 2] }]
                                        },
                                        "then": "#ffff00",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 2] }, { "$lt": ["$indicador", 3] }]
                                                },
                                                "then": "#ff8000",
                                                "else": "#ff0000"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },


                "rango": {
                    "$cond": {
                        "if": { "$eq": ["$indicador", 0] },
                        "then": "A-[=0%]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }]
                                },
                                "then": "B-(0% - 1%)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 2] }]
                                        },
                                        "then": "C-[1% - 2%)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$indicador", 2] }, { "$lt": ["$indicador", 3] }]
                                                },
                                                "then": "D-[2% - 3%)",
                                                "else": "E-(>=3%)"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }


        , {
            "$project": {
                "_id": {
                    //"$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                    //"$arrayElemAt": ["$data.data.elemnq", { "$subtract": [{ "$size": "$data.data.elemnq" }, 1] }]
                    "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.elemnq", 0] }, 0]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.lote",
                    "Mes": "$Mes_Txt",
                    "Enfermedad": "Anillo Rojo",
                    "Rango": "$rango",
                    "%Incidencia_Mes": {
                        "$concat": [
                            { "$toString": "$indicador" },
                            " %"
                        ]
                    },

                    "color": "$color"
                },
                "geometry": {
                    //"$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                    // "$arrayElemAt": ["$data.data.Cartography.geometry", { "$subtract": [{ "$size": "$data.data.Cartography.geometry" }, 1] }]
                    "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.Cartography.geometry", 0] }, 0]
                }
            }
        }


    ]

)