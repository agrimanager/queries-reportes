
[


    { "$limit": 1 },


    {
        "$lookup": {
            "from": "form_modulodeenfermedades",
            "as": "registro_anterior",
            "let": {
                "fecha_filtro_inicio": "$Busqueda inicio",
                "fecha_filtro_fin": "$Busqueda fin"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$in": ["$Enfermedad", ["anillo rojo", "Anillo Rojo"]] },
                                { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                            ]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "Busqueda fin": "$$fecha_filtro_fin",
                        "Busqueda inicio": "$$fecha_filtro_inicio"
                    }
                }
            ]
        }
    }
    , { "$unwind": "$registro_anterior" }


    , {
        "$replaceRoot": {
            "newRoot": "$registro_anterior"
        }
    }

    , {
        "$addFields": {
            "Enfermedad": "Anillo Rojo"
        }
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "linea": "$linea.properties.name",
            "planta": "$planta.properties.name"

        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0

            , "Arbol": 0
            , "Point": 0

            , "Formula": 0
            , "Sampling": 0
        }
    }


    , {
        "$addFields": {
            "anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "anio_busqueda_inicio": { "$year": { "date": "$Busqueda inicio" } },
            "num_mes_busqueda_inicio": { "$month": { "date": "$Busqueda inicio" } },

            "anio_busqueda_fin": { "$year": { "date": "$Busqueda fin" } },
            "num_mes_busqueda_fin": { "$month": { "date": "$Busqueda fin" } }
        }
    }


    , {
        "$lookup": {
            "from": "form_inventariopalmasiniciales",
            "as": "info_lote",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$info_lote",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "num_palmas": "$info_lote.Palmas Iniciales",
            "Hectareas": "$info_lote.Hectareas Iniciales",
            "Material": "$info_lote.Material Siembra"
        }
    }

    , {
        "$project": {
            "info_lote": 0
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "planta": "$planta"

                , "anio": "$anio"
                , "num_mes": "$num_mes"

                , "anio_busqueda_fin": "$anio_busqueda_fin"
                , "num_mes_busqueda_fin": "$num_mes_busqueda_fin"

                , "num_palmas": "$num_palmas"

                , "rgDate": "$Busqueda fin"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"

                , "anio": "$_id.anio"
                , "num_mes": "$_id.num_mes"


                , "anio_busqueda_fin": "$_id.anio_busqueda_fin"
                , "num_mes_busqueda_fin": "$_id.num_mes_busqueda_fin"

                , "num_palmas": "$_id.num_palmas"
                , "rgDate": "$_id.rgDate"
            }
            , "plantas_dif_censadas_x_lote_x_anio_x_mes": { "$sum": 1 }
        }
    }



    , {
        "$addFields": {
            "pct_incidencia_mes": {
                "$cond": {
                    "if": { "$eq": ["$_id.num_palmas", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_anio_x_mes", "$_id.num_palmas"] }, 100]
                    }
                }
            }

        }
    }

    , {
        "$addFields": {
            "pct_incidencia_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia_mes", 100] }, 1] }] }, 100] }
        }
    }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "pct_incidencia_mes": "$pct_incidencia_mes"
                    }
                ]
            }
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$lote"

                , "anio": "$anio"
                , "num_mes_busqueda_fin": "$num_mes_busqueda_fin"
            }
            , "data": { "$push": "$$ROOT" }

            , "pct_incidencia_mes_anio_acum": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$num_mes", "$num_mes_busqueda_fin"] },
                                        { "$lte": ["$anio", "$anio_busqueda_fin"] }
                                    ]
                                },
                                "then": "$pct_incidencia_mes"
                            }
                        ],
                        "default": 0
                    }
                }
            }
        }
    }


    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "pct_incidencia_mes_anio_acum": "$pct_incidencia_mes_anio_acum"
                    }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$lote"

                , "anio": "$anio"
                , "num_mes_busqueda_fin": "$num_mes_busqueda_fin"
            }
            , "data": { "$push": "$$ROOT" }

            , "pct_incidencia_anio_acum": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$anio", "$anio_busqueda_fin"] }
                                    ]
                                },
                                "then": "$pct_incidencia_mes"
                            }
                        ],
                        "default": 0
                    }
                }
            }
        }
    }


    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "pct_incidencia_anio_acum": "$pct_incidencia_anio_acum"
                    }
                ]
            }
        }
    }

    , {
        "$match": {
            "$expr": {
                "$and": [
                    { "$eq": ["$anio_busqueda_fin", "$anio"] }
                ]
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "lote": "$lote"

                , "anio": "$anio"
                , "num_mes_busqueda_fin": "$num_mes_busqueda_fin"
                , "rgDate": "$rgDate"
            }
            , "data": { "$push": "$$ROOT" }

            , "pct_mes": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$num_mes", "$num_mes_busqueda_fin"] },
                                        { "$eq": ["$anio", "$anio_busqueda_fin"] }
                                    ]
                                },
                                "then": "$pct_incidencia_mes"
                            }
                        ],
                        "default": 0
                    }
                }
            }

            , "pct_mes_anio_acum": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$num_mes", "$num_mes_busqueda_fin"] },
                                        { "$lte": ["$anio", "$anio_busqueda_fin"] }
                                    ]
                                },
                                "then": "$pct_incidencia_mes"
                            }
                        ],
                        "default": 0
                    }
                }
            }

            , "pct_anio_acum": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$anio", "$anio_busqueda_fin"] }
                                    ]
                                },
                                "then": "$pct_incidencia_mes"
                            }
                        ],
                        "default": 0
                    }
                }
            }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "pct_mes": "$pct_mes",
                        "pct_mes_anio_acum": "$pct_mes_anio_acum",
                        "pct_anio_acum": "$pct_anio_acum"
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "pct_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_mes", 100] }, 1] }] }, 100] },
            "pct_mes_anio_acum": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_mes_anio_acum", 100] }, { "$mod": [{ "$multiply": ["$pct_mes_anio_acum", 100] }, 1] }] }, 100] },
            "pct_anio_acum": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_anio_acum", 100] }, { "$mod": [{ "$multiply": ["$pct_anio_acum", 100] }, 1] }] }, 100] }
        }
    }





    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes_busqueda_fin", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "lote",
            "foreignField": "properties.name",
            "as": "obj_cartography"
        }
    }
    , { "$unwind": "$obj_cartography" }
    , {
        "$addFields": {
            "indicador": "$pct_mes"
        }
    }

    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": { "$eq": ["$indicador", 0] },
                    "then": "#00FF00",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }]
                            },
                            "then": "#e2ba1f",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 2] }]
                                    },
                                    "then": "#ffff00",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$indicador", 2] }, { "$lt": ["$indicador", 3] }]
                                            },
                                            "then": "#ff8000",
                                            "else": "#ff0000"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },


            "rango": {
                "$cond": {
                    "if": { "$eq": ["$indicador", 0] },
                    "then": "A-[=0%]",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gt": ["$indicador", 0] }, { "$lt": ["$indicador", 1] }]
                            },
                            "then": "B-(0% - 1%)",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$indicador", 1] }, { "$lt": ["$indicador", 2] }]
                                    },
                                    "then": "C-[1% - 2%)",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$indicador", 2] }, { "$lt": ["$indicador", 3] }]
                                            },
                                            "then": "D-[2% - 3%)",
                                            "else": "E-(>=3%)"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }


    , {
        "$project": {

            "_id": null,
            "idform": null,
            "type": "Feature",
            "properties": {
                "Lote": "$_lote",
                "Mes": "$Mes_Txt",
                "Enfermedad": "Anillo Rojo",
                "Rango": "$rango",
                "%Incidencia_Mes": {
                    "$concat": [
                        { "$toString": "$indicador" },
                        " %"
                    ]
                },

                "color": "$color"
            },
            "geometry": "$obj_cartography.geometry"
        }
    }


]