db.form_modulodeenfermedades.aggregate(
    [

        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-06-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------


        //===== CONDICIONES BASE
        { "$limit": 1 },


        //===== CRAZAR CON MISMA TABLA
        //----join con misma tabla
        {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "registro_anterior",
                "let": {
                    // "nombre_enfermedad": "$Enfermedad",
                    "fecha_filtro_inicio": "$Busqueda inicio",
                    "fecha_filtro_fin": "$Busqueda fin"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$in": ["$Enfermedad", ["anillo rojo", "Anillo Rojo"]] },
                                    // { "$in": ["$Enfermedad", ["Pudricin de Estipe"]] },
                                    // { "$in": ["$Enfermedad", ["Pudrición de cogollo PC", "pudricion de cogollo pc"]] },

                                    { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Busqueda fin": "$$fecha_filtro_fin",
                            "Busqueda inicio": "$$fecha_filtro_inicio"
                        }
                    }
                ]
            }
        }
        , { "$unwind": "$registro_anterior" }


        , {
            "$replaceRoot": {
                "newRoot": "$registro_anterior"
            }
        }

        , {
            "$addFields": {
                "Enfermedad": "Anillo Rojo"
                //"Enfermedad":"Pudrición de Estipe"
                // "Enfermedad":"Pudrición de cogollo PC"
            }
        },



        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Arbol": 0
                , "Point": 0

                , "Formula": 0
                , "Sampling": 0
            }
        }


        //====== FECHAS
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        //====== FECHAS filtro busqueda
        , {
            "$addFields": {
                "anio_busqueda_inicio": { "$year": { "date": "$Busqueda inicio" } },
                "num_mes_busqueda_inicio": { "$month": { "date": "$Busqueda inicio" } },

                "anio_busqueda_fin": { "$year": { "date": "$Busqueda fin" } },
                "num_mes_busqueda_fin": { "$month": { "date": "$Busqueda fin" } }
            }
        }



        //====== INFO LOTE
        , {
            "$lookup": {
                "from": "form_inventariopalmasiniciales",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales",
                "Hectareas": "$info_lote.Hectareas Iniciales",
                "Material": "$info_lote.Material Siembra"
            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }






        // //====== AGRUPACION MULTIPLE

        //----plantas_dif_censadas_x_lote
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$planta"

                    , "anio": "$anio"
                    , "num_mes": "$num_mes"

                    , "anio_busqueda_fin": "$anio_busqueda_fin"
                    , "num_mes_busqueda_fin": "$num_mes_busqueda_fin"

                    , "num_palmas": "$num_palmas"
                }
                // ,"data": {"$push": "$$ROOT"}
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                    //"planta": "$planta"

                    , "anio": "$_id.anio"
                    , "num_mes": "$_id.num_mes"


                    , "anio_busqueda_fin": "$_id.anio_busqueda_fin"
                    , "num_mes_busqueda_fin": "$_id.num_mes_busqueda_fin"

                    , "num_palmas": "$_id.num_palmas"
                }
                , "plantas_dif_censadas_x_lote_x_anio_x_mes": { "$sum": 1 },
                // ,"data": {"$push": "$$ROOT"}
            }
        }



        //---proceso1
        , {
            "$addFields": {
                "pct_incidencia_mes": {
                    "$cond": {
                        "if": { "$eq": ["$_id.num_palmas", 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_anio_x_mes", "$_id.num_palmas"] }, 100]
                        }
                    }
                }

            }
        }


        //---2 decimales
        , {
            "$addFields": {
                "pct_incidencia_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia_mes", 100] }, 1] }] }, 100] }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "pct_incidencia_mes": "$pct_incidencia_mes"
                        }
                    ]
                }
            }
        }


        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$_id.lote"
        //             //"planta": "$planta"

        //             , "anio": "$_id.anio"
        //             // , "num_mes": "$_id.num_mes"


        //             // , "anio_busqueda_fin": "$_id.anio_busqueda_fin"
        //             , "num_mes_busqueda_fin": "$_id.num_mes_busqueda_fin"

        //             // , "num_palmas": "$_id.num_palmas"
        //         }
        //         , "pct_incidencia_mes": { "$sum": "$pct_incidencia_mes" },
        //         // ,"data": {"$push": "$$ROOT"}
        //     }
        // }



        // , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }


        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data.data",
        //                 {
        //                     "plantas_dif_censadas_x_lote_x_anio_x_mes": "$plantas_dif_censadas_x_lote_x_anio_x_mes"
        //                 }
        //             ]
        //         }
        //     }
        // }






        // // //===== CALCULOS
        // //---proceso1
        // , {
        //     "$addFields": {
        //         "pct_incidencia_mes": {
        //             "$cond": {
        //                 "if": { "$eq": ["$num_palmas", 0] },
        //                 "then": 0,
        //                 "else": {
        //                     "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_anio_x_mes", "$num_palmas"] }, 100]
        //                 }
        //             }
        //         }

        //     }
        // }


        // //---2 decimales
        // , {
        //     "$addFields": {
        //         "pct_incidencia_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia_mes", 100] }, 1] }] }, 100] }
        //     }
        // }




        // // //===== CALCULOS

        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "lote": "$lote"

        // //             , "anio": "$anio"
        // //             , "num_mes": "$num_mes"

        // //             , "num_palmas": "$num_palmas"
        // //             , "plantas_dif_censadas_x_lote_x_anio_x_mes": "$plantas_dif_censadas_x_lote_x_anio_x_mes"
        // //         }
        // //         , "data": { "$push": "$$ROOT" }
        // //     }
        // // }


        // // //---proceso1
        // // , {
        // //     "$addFields": {
        // //         "pct_incidencia1_x_lote_x_anio_x_mes": {
        // //             "$cond": {
        // //                 "if": { "$eq": ["$_id.num_palmas", 0] },
        // //                 "then": 0,
        // //                 "else": {
        // //                     "$multiply": [{ "$divide": ["$_id.plantas_dif_censadas_x_lote_x_anio_x_mes", "$_id.num_palmas"] }, 100]
        // //                 }
        // //             }
        // //         }

        // //     }
        // // }


        // // //---2 decimales
        // // , {
        // //     "$addFields": {
        // //         "pct_incidencia1_x_lote_x_anio_x_mes": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_incidencia1_x_lote_x_anio_x_mes", 100] }, { "$mod": [{ "$multiply": ["$pct_incidencia1_x_lote_x_anio_x_mes", 100] }, 1] }] }, 100] }
        // //     }
        // // }

        // // , { "$unwind": "$data" }
        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$data",
        // //                 {
        // //                     "pct_incidencia1_x_lote_x_anio_x_mes": "$pct_incidencia1_x_lote_x_anio_x_mes"
        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }


        // // //====== FECHAS filtro busqueda
        // // , {
        // //     "$addFields": {
        // //         "anio_busqueda_inicio": { "$year": { "date": "$Busqueda inicio" } },
        // //         "num_mes_busqueda_inicio": { "$month": { "date": "$Busqueda inicio" } }
        // //     }
        // // }


        // // //---proceso2
        // // , {
        // //     "$group": {
        // //         "_id": {
        // //             "lote": "$lote"

        // //             , "anio": "$anio"
        // //             // , "num_mes": "$num_mes"
        // //             , "num_mes_busqueda_inicio": "$num_mes_busqueda_inicio"

        // //             // , "num_palmas": "$num_palmas"
        // //             // , "plantas_dif_censadas_x_lote_x_anio_x_mes": "$plantas_dif_censadas_x_lote_x_anio_x_mes"
        // //         }
        // //         // , "data": { "$push": "$$ROOT" }


        // //         //--p1
        // //         , "pct_mes": {
        // //             "$sum": {
        // //                 "$switch": {
        // //                     "branches": [
        // //                         //--caso1
        // //                         {
        // //                             "case": {
        // //                                 "$and": [
        // //                                     { "$eq": ["$num_mes_busqueda_inicio", "$num_mes"] }
        // //                                     , { "$eq": ["", ""] }
        // //                                 ]
        // //                             },
        // //                             "then": "$pct_incidencia1_x_lote_x_anio_x_mes"
        // //                         },

        // //                         // //--caso2
        // //                         // {
        // //                         //     "case": {
        // //                         //         "$and": [
        // //                         //             { "$eq": ["$_id.enfermedad", "Moni Grup 1"] }
        // //                         //             , { "$eq": ["$_id.grupo_valor", "Moni Grup 1 Fruts Enfermos"] }
        // //                         //         ]
        // //                         //     },
        // //                         //     "then": "$sum_cantidad"
        // //                         // }
        // //                         // , {
        // //                         //     "case": {
        // //                         //         "$and": [
        // //                         //             { "$eq": ["$_id.enfermedad", "Moni Grup 2"] }
        // //                         //             , { "$eq": ["$_id.grupo_valor", "Moni Grup 2 FrutsEnfermos"] }
        // //                         //         ]
        // //                         //     },
        // //                         //     "then": "$sum_cantidad"
        // //                         // },

        // //                         // //--caso3
        // //                         // {
        // //                         //     "case": {
        // //                         //         "$and": [
        // //                         //             { "$ne": ["$_id.enfermedad", "Phytophthora fruto"] },
        // //                         //             { "$ne": ["$_id.enfermedad", "Moni Grup 1"] },
        // //                         //             { "$ne": ["$_id.enfermedad", "Moni Grup 2"] }
        // //                         //         ]
        // //                         //     },
        // //                         //     "then": "$sum_cantidad"
        // //                         // }


        // //                     ],
        // //                     //"default": "$sum_cantidad"
        // //                     "default": 0
        // //                 }
        // //             }
        // //         }
        // //     }
        // // }



    ]

)