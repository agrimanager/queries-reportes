db.form_modulodeenfermedades.aggregate(
    [

        //----------------test filtro de fechas
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-06-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },

        //------------------------------------------


        //===== CONDICIONES BASE
        {
            "$match": {
                "Enfermedad": { "$in": ["anillo rojo", "Anillo Rojo"] }
                // "Enfermedad": { "$in": ["Pudricin de Estipe"] }
                // "Enfermedad": { "$in": ["Pudrición de cogollo PC", "pudricion de cogollo pc"] }

            }
        },
        {
            "$addFields": {
                "Enfermedad": "Anillo Rojo"
                //"Enfermedad":"Pudrición de Estipe"
                // "Enfermedad":"Pudrición de cogollo PC"
            }
        },


        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "planta": "$planta.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Arbol": 0
                , "Point": 0

                , "Formula": 0
                , "Sampling": 0
            }
        }


        //====== INFO LOTE
        , {
            "$lookup": {
                "from": "form_inventariopalmasiniciales",
                "as": "info_lote",
                "let": {
                    "nombre_lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                                ]
                            }
                        }
                    },
                    {
                        "$limit": 1
                    }

                ]
            }
        },

        {
            "$unwind": {
                "path": "$info_lote",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "num_palmas": "$info_lote.Palmas Iniciales",
                "Hectareas": "$info_lote.Hectareas Iniciales",
                "Material": "$info_lote.Material Siembra"
            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }



        //====== FECHAS
        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
            }
        }




        // // //====== AGRUPACION
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    // ,"planta": "$planta"

                    , "anio": "$anio"
                    , "num_mes": "$num_mes"
                    , "num_dia_mes": "$num_dia_mes"

                    , "lote_ha": "$Hectareas"

                }
                // ,"data": {"$push": "$$ROOT"}
            }
        }


    ]

)