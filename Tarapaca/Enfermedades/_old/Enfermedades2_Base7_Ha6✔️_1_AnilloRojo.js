[


    {
        "$match": {
            "Enfermedad": { "$in": ["anillo rojo", "Anillo Rojo"] }

        }
    },
    {
        "$addFields": {
            "Enfermedad": "Anillo Rojo"
        }
    },



    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "linea": "$linea.properties.name",
            "planta": "$planta.properties.name"

        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0

            , "Arbol": 0
            , "Point": 0

            , "Formula": 0
            , "Sampling": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_inventariopalmasiniciales",
            "as": "info_lote",
            "let": {
                "nombre_lote": "$lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }
                            ]
                        }
                    }
                },
                {
                    "$limit": 1
                }

            ]
        }
    },

    {
        "$unwind": {
            "path": "$info_lote",
            "preserveNullAndEmptyArrays": false
        }
    },

    {
        "$addFields": {
            "num_palmas": "$info_lote.Palmas Iniciales",
            "Hectareas": "$info_lote.Hectareas Iniciales",
            "Material": "$info_lote.Material Siembra"
        }
    }

    , {
        "$project": {
            "info_lote": 0
        }
    }




    , {
        "$addFields": {
            "anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } }
        }
    }





    , {
        "$group": {
            "_id": {
                "lote": "$lote"


                , "anio": "$anio"
                , "num_mes": "$num_mes"
                , "num_dia_mes": "$num_dia_mes"

                , "lote_ha": "$Hectareas"

            }

        }
    }


    , {
        "$sort": { "_id.num_dia_mes": 1 }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"


                , "anio": "$_id.anio"
                , "num_mes": "$_id.num_mes"


                , "lote_ha": "$_id.lote_ha"

            }

            , "dias_mes": { "$push": "$_id.num_dia_mes" }
        }
    }

    , {
        "$sort": {
            "_id.lote": 1,
            "_id.anio": 1,



            "_id.num_mes": 1
        }
    }



    , {
        "$addFields": {
            "cantidad_dias": { "$size": "$dias_mes" },
            "min_dia": { "$min": "$dias_mes" }
        }
    }


    , {
        "$addFields": {
            "ha_censada": {
                "$reduce": {
                    "input": "$dias_mes",

                    "initialValue": {
                        "dia": "$min_dia",
                        "tasa": 1
                    },

                    "in": {
                        "dia": "$$this",
                        "tasa": {
                            "$cond": {
                                "if": { "$gte": [{ "$subtract": ["$$this", "$$value.dia"] }, 8] },
                                "then": { "$add": ["$$value.tasa", 1] },
                                "else": { "$add": ["$$value.tasa", 0] }
                            }
                        }
                    }

                }
            }
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "tasa_ha": "$ha_censada.tasa"
                        , "dias_mes": "$dias_mes"
                        , "cantidad_dias": "$cantidad_dias"
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "ha_censadas": { "$multiply": ["$lote_ha", "$tasa_ha"] }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }



]