//---Adrian
[
        {
            "$addFields": {
                "tramp": { "$arrayElemAt": ["$Trampa.features.properties.name", 0] }
            }
        },
        {
            "$addFields": {
                "split_tramp": {
                    "$split": [{ "$trim": { "input": "$tramp", "chars": "-" } }, "-"]
                }
            }
        },
        {
            "$addFields": {
                "tramp": { "$arrayElemAt": ["$split_tramp", -1] }
            }
        },
        {
            "$match": {
                "tramp": { "$regex": "^2" }
            }
        },
        {
            "$addFields": {
                "split_path": {
                    "$split": [{ "$trim": { "input": "$Trampa.path", "chars": "," } }, ","]
                }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } }
                },
                "trampa_oid": {
                    "$map": { "input": "$Trampa.features", "as": "item", "in": { "$toObjectId": "$$item._id" } }
                }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$trampa_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "jerarquia"
            }
        },
        {
            "$addFields": {
                "Bloque": { "$arrayElemAt": ["$jerarquia", 1] },
                "Lote": { "$arrayElemAt": ["$jerarquia", 2] },
                "Trampa": { "$arrayElemAt": ["$jerarquia", 3] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Trampa": "$Trampa.properties.name"
            }
        },
        {
            "$project": {
                "split_tramp": 0,
                "tramp": 0,
                "split_path": 0,
                "split_path_oid": 0,
                "trampa_oid": 0,
                "jerarquia": 0
            }
        },
        {
            "$addFields": {
                "mes": { "$dateToString": { "date": "$rgDate", "format": "%m-%Y" } },
                "Número de Insectos": { "$sum": ["$Número de Machos", "$Número de Hembras"] }
            }
        },
        {
            "$lookup": {
                "from": "form_pluviometria",
                "let": { "id": "$mes" },
                "pipeline": [
                    {
                        "$addFields": {
                            "mes": { "$dateToString": { "date": "$Fecha", "format": "%m-%Y" } }
                        }
                    },
                    {
                        "$match": {
                            "$expr": { "$eq": ["$mes", "$$id"] }
                        }
                    },
                    {
                        "$group": {
                            "_id": "$mes",
                            "suma precipitacion por mes": { "$sum": "$Precipitacion" }
                        }
                    }
                ],
                "as": "presipitacion"
            }
        },
        {
            "$addFields": {
                "presipitacion": "$presipitacion.suma precipitacion por mes"
            }
        },
        {
            "$addFields": {
                "suma precipitacion por mes": { "$arrayElemAt": ["$presipitacion", 0] }
            }
        },
        {
            "$project": {
                "presipitacion": 0
            }
        },
        {
            "$group": {
                "_id": {
                    "mes": "$mes",
                    "bloque": "$Bloque"
                },
                "count": { "$sum": "$Número de Insectos" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "suma de insectos por bloque y mes": "$count" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "mes": "$mes",
                    "lote": "$Lote" },
                "count": { "$sum": "$Número de Insectos" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
           "$replaceRoot": {
                "newRoot": {
                   "$mergeObjects": [
                        "$data",
                        { "suma de insectos por lote y mes": "$count" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": { "mes": "$mes" },
                "hembras": { "$sum": "$Número de Hembras" },
                "machos": { "$sum": "$Número de Machos" },
                "total de insectos": { "$sum": "$Número de Insectos" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "suma de hembras por mes": "$hembras" },
                        { "suma de machos por mes": "$machos" },
                        { "total de insectos por mes": "$total de insectos" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": 0,
                "count": { "$sum": "$Número de Insectos" },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "suma total de insectos": "$count" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "trampa": "$Trampa",
                    "bloque": "$Bloque"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": { "bloque": "$_id.bloque" },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" }, 
        {
           "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "numero de trampas por bloque": "$count" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": {
                    "trampa": "$Trampa",
                    "lote": "$Lote"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": { "lote": "$_id.lote" },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" }, 
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                    "$data.data",
                    { "numero de trampas por lote": "$count" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": { 
                    "trampa":"$Trampa"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": 0,
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" }, 
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "numero total de trampas": "$count" }
                    ]
                }
            }
        },
        {
            "$addFields": {
                "promedio de capturas por bloque y mes": { 
                    "$cond": {
                        "if": { "$eq": ["$numero de trampas por bloque", 0] },
                        "then": 0,
                        "else": { "$divide": ["$suma de insectos por bloque y mes", "$numero de trampas por bloque"] }
                    }
                },
                "promedio de capturas por lote y mes": {
                    "$cond": {
                        "if": { "$eq": ["$numero de trampas por lote", 0] },
                        "then": 0,
                        "else": { "$divide": ["$suma de insectos por lote y mes", "$numero de trampas por lote"] }
                    }
                },
                "promedio de capturas por trampa": {
                    "$cond": {
                        "if": { "$eq": ["$numero total de trampas", 0] },
                        "then": 0,
                        "else": { "$divide": ["$suma total de insectos", "$numero total de trampas"] }
                    }
                },
                "% hembras por mes": {
                    "$cond": {
                        "if": { "$eq": ["$total de insectos por mes", 0] },
                        "then": 0,
                        "else": { "$multiply": [{ "$divide": ["$suma de hembras por mes", "$total de insectos por mes"] }, 100] }
                    }
                },
                "% machos por mes": {
                    "$cond": {
                        "if": { "$eq": ["$total de insectos por mes", 0] },
                        "then": 0,
                        "else": { "$multiply": [{ "$divide": ["$suma de machos por mes", "$total de insectos por mes"] }, 100] }
                    }
                }
            }
        }
    ]