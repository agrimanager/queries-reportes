db.form_modulodetrampas.aggregate(
    [


        // //======CONDICIONES BASE
        {
            "$match": {
                "Plaga": "Rhynchophorus palmarum"
            }
        },

        //======CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Trampa.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Trampa.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "trampa": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "trampa": "$trampa.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Trampa": 0
                , "Point": 0
                , "Formula": 0
            }
        }


        // //---test
        // {
        //     $group:{
        //       "_id":{
        //           plaga:"$Plaga"
        //       } 
        //     }
        // }



        //=====FECHA

        //---info fechas
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }


        //===== AGRUPACIONES
        //---bloque-lote-trampa-mes
        , {
            "$group": {
                "_id": {
                    "bloque": "$bloque"
                    , "lote": "$lote"
                    , "trampa": "$trampa"

                    , "num_anio": "$num_anio"
                    , "num_mes": "$num_mes"
                }
                , "data": { "$push": "$$ROOT" }
                
                , "trampas_leidas": { "$sum": 1 }

                // , "num_machos": { "$sum": { "$toDouble": "$Número de Machos" } }
                // , "num_hembras": { "$sum": { "$toDouble": "$Número de Hembras" } }
            }
        }


        // , {
        //     "$group": {
        //         "_id": {
        //             "bloque": "$_id.bloque"
        //             , "lote": "$_id.lote"
        //             // , "trampa": "$trampa"

        //             , "num_anio": "$_id.num_anio"
        //             , "num_mes": "$_id.num_mes"
        //         }
        //         , "data": { "$push": "$$ROOT" }

        //         //---datos
        //         , "trampas_leidas": { "$sum": 1 }
        //         , "num_machos": { "$sum": { "$toDouble": "$num_machos" } }
        //         , "num_hembras": { "$sum": { "$toDouble": "$num_hembras" } }
        //     }
        // }
        
        // ,{
        //     "$addFields": {
        //         "num_total_insectos": {"$sum":["$num_machos","$num_hembras"]}
        //     }
        // }


        //===== CALCULOS



        //===== CRUZAR TABLAS




        //----fechas finales de mes
        //     , {
        //     "$addFields": {
        //         "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

        //         , "Mes_Txt": {
        //             "$switch": {
        //                 "branches": [
        //                     { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
        //                     { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
        //                     { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
        //                     { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
        //                     { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
        //                     { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
        //                     { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
        //                     { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
        //                     { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
        //                     { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
        //                     { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
        //                     { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
        //                 ],
        //                 "default": "Mes desconocido"
        //             }
        //         }
        //     }
        // },



    ]


)