[



    {
        "$match": {
            "Plaga": "Rhynchophorus palmarum"
        }
    },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Trampa.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Trampa.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "trampa": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
        }
    },

    {
        "$addFields": {
            "bloque": "$bloque.properties.name",
            "lote": "$lote.properties.name",
            "trampa": "$trampa.properties.name"

        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },



    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0

            , "Trampa": 0
            , "Point": 0
            , "Formula": 0
        }
    }


    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }

    , {
        "$group": {
            "_id": {
                "bloque": "$bloque"
                , "lote": "$lote"
                , "trampa": "$trampa"

                , "num_anio": "$num_anio"
                , "num_mes": "$num_mes"
            }

            , "num_machos": { "$sum": { "$toDouble": "$Número de Machos" } }
            , "num_hembras": { "$sum": { "$toDouble": "$Número de Hembras" } }
        }
    }


    , {
        "$group": {
            "_id": {
                "bloque": "$_id.bloque"
                , "lote": "$_id.lote"


                , "num_anio": "$_id.num_anio"
                , "num_mes": "$_id.num_mes"
            }



            , "trampas_leidas": { "$sum": 1 }
            , "num_machos": { "$sum": { "$toDouble": "$num_machos" } }
            , "num_hembras": { "$sum": { "$toDouble": "$num_hembras" } }
        }
    }

    , {
        "$addFields": {
            "num_total_insectos": { "$sum": ["$num_machos", "$num_hembras"] }
        }
    }




    , {
        "$lookup": {
            "from": "form_pluviometria",
            "as": "info_precipitacion",
            "let": {
                "num_anio": "$_id.num_anio"
                , "num_mes": "$_id.num_mes"
            },
            "pipeline": [

                { "$match": { "Pluviometro.path": { "$ne": "" } } },
                { "$match": { "Pluviometro.features.properties.name": { "$in": ["Pluviometro casa ppal"] } } },


                { "$addFields": { "anio_filtro": { "$year": "$Fecha" } } },
                { "$match": { "anio_filtro": { "$gt": 2000 } } },
                { "$match": { "anio_filtro": { "$lt": 3000 } } },

                { "$addFields": { "mes_filtro": { "$month": "$Fecha" } } },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$anio_filtro", "$$num_anio"] }
                                ,
                                { "$eq": ["$mes_filtro", "$$num_mes"] }
                            ]
                        }
                    }
                },

                {
                    "$project": {
                        "_id": 0,
                        "Precipitacion": 1
                    }
                }
            ]
        }
    }


    , {
        "$addFields": {
            "info_precipitacion": {
                "$reduce": {
                    "input": "$info_precipitacion.Precipitacion",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$value", "$$this"]
                    }
                }
            }
        }
    }





    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$_id.num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$_id.num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$_id.num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$_id.num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$_id.num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$_id.num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$_id.num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$_id.num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$_id.num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$_id.num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$_id.num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$_id.num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    },


    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "trampas_leidas": "$trampas_leidas",
                        "num_machos": "$num_machos",
                        "num_hembras": "$num_hembras",
                        "num_total_insectos": "$num_total_insectos",
                        "info_precipitacion": "$info_precipitacion",
                        "Mes_Txt": "$Mes_Txt"
                    }
                ]
            }
        }
    }


]