[

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
        }
    },




    {
        "$lookup": {
            "from": "cartography_polygon",
            "localField": "Lote._id",
            "foreignField": "_id",
            "as": "info_lote"
        }
    },
    {
        "$addFields": {
            "info_lote": "$info_lote.internal_features"
        }
    },
    { "$unwind": "$info_lote" },

    {
        "$addFields": {
            "plantas_x_lote": {
                "$filter": {
                    "input": "$info_lote",
                    "as": "item",
                    "cond": { "$eq": ["$$item.type_features", "trees"] }
                }
            }
        }
    },
    { "$unwind": "$plantas_x_lote" },
    {
        "$addFields": {
            "plantas_x_lote": "$plantas_x_lote.count_features"
        }
    },




    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Uma": "$Uma.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_lote": 0

            , "Point": 0
            , "Palma": 0

            , "_id": 0
            , "uid": 0
            , "Formula": 0
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$Lote",
                "planta": "$Planta"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "num_planta": { "$split": ["$Planta", "-"] }
        }
    }

    , {
        "$addFields": {
            "num_planta": {
                "$sum": [
                    {
                        "$multiply": [
                            {
                                "$toInt": {
                                    "$substr": [{ "$arrayElemAt": ["$num_planta", 2] }, 1, -1]
                                }
                            },
                            100000000
                        ]
                    },
                    {
                        "$multiply": [
                            { "$toInt": { "$arrayElemAt": ["$num_planta", 3] } },
                            10000
                        ]
                    },
                    { "$toInt": { "$arrayElemAt": ["$num_planta", 4] } }
                ]
            }
        }
    }

]
