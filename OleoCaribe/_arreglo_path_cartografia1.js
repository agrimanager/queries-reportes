db.cartography.aggregate(
    // {
    //     $match: {
    //         "properties.name": {
    //             //$regex: `^,5fda5921c6443216da62d5c0,5fe201cf2c6493865a784e97,5fe201d02c6493865a784ec4,5fe201d22c6493865a785353,` //37-4-U42

    //             $regex: `^37-4-U42`

    //             //37-4-U42
    //             //path: ,5fda5921c6443216da62d5c0,5fe201cf2c6493865a784e97,5fe201d02c6493865a784ec4,
    //             //_id: 5fe201d22c6493865a785353

    //             //linea
    //             //37-4-U42-1
    //             //path_viejo: ,5fda5921c6443216da62d5c0,5fe201d02c6493865a784ec5,5fe201d12c6493865a7851f9,5fe201d22c6493865a785353,
    //             //path_viejo: ,5fda5921c6443216da62d5c0,XXXXXXXBLOQUE_XXXXXXXXXX,YYYYYYYYLOTE_YYYYYYYYYYY,5fe201d22c6493865a785353,
    //             //path_nuevo: ,5fda5921c6443216da62d5c0,5fe201cf2c6493865a784e97,5fe201d02c6493865a784ec4,5fe201d22c6493865a785353,

    //             //arbol
    //             //37-4-U42-1-1
    //             //path_viejo: ,5fda5921c6443216da62d5c0,5fe201d02c6493865a784ec5,5fe201d12c6493865a7851f9,5fe201d22c6493865a785353,600da5b683861f55b5bb5344,
    //             //path_nuevo: ,5fda5921c6443216da62d5c0,5fe201cf2c6493865a784e97,5fe201d02c6493865a784ec4,5fe201d22c6493865a785353,600da5b683861f55b5bb5344,


    //         }
    //     }
    // },



    {
        $match: {
            "path": {
                // $regex:`^,5fda5921c6443216da62d5c0,`//guapinol
                // $regex:`^,611e66746efef010f44113a1,`//Ceiba
                $regex:`^,5d97a53f0d362e2aa2ac1fab,`//Cortés
                // $regex:`^,5d8e867e0b4cc6ad7dfff3b5,`//Almendro
            }
        }
    },

    {
        $match:{
            type:"Feature"
        }
    },


    {
        $project: {
            "name": "$properties.name"
            , "type": "$properties.type"
            , "path": "$path"
        }
    },




    //test
    //=====CARTOGRAFIA

    //--paso1 (cartografia-nombre variable y ids)
    // {
    //     "$addFields": {
    //         "variable_cartografia": "$Arbol" //🚩editar
    //     }
    // },
    // { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    //--paso2 (cartografia-cruzar informacion)
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    // //--paso3 (cartografia-obtener informacion)

    //--finca
    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    //--bloque
    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    //--lote
    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


    //--uma
    {
        "$addFields": {
            "uma": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$uma",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "uma": { "$ifNull": ["$uma.properties.name", "no existe"] } } },

    //--linea
    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    //--arbol
    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0
        }
    }


    //-----ANALISIS
    //1) para lineas y arboles en el path donde ids de bloque y lote estan mal


    //==================== path logica
    , {
        "$addFields": {
            "name_feature": "$linea"
            // "name_feature": "$arbol"
        }
    }

    , {
        "$match": {
            "name_feature": { "$ne": "no existe" }
        }
    }



    , {
        "$addFields": {
            "split_name_feature": { "$split": [{ "$trim": { "input": "$name_feature", "chars": "-" } }, "-"] }
        }
    }

    , {
        "$addFields": {
            "num_bloque": { "$arrayElemAt": ["$split_name_feature", 0] },
            "num_lote": { "$arrayElemAt": ["$split_name_feature", 1] },
            "num_uma": { "$arrayElemAt": ["$split_name_feature", 2] },
            "num_linea": { "$arrayElemAt": ["$split_name_feature", 3] },
            "num_planta": { "$arrayElemAt": ["$split_name_feature", 4] }
        }
    }

    , {
        "$addFields": {
            "num_bloque_lote": { "$concat": ["$num_bloque", "-", "$num_lote"] }
        }
    }


    //---condiciones
    , {
        "$addFields": {
            "condicion_bloque": {
                "$cond": {
                    "if":{"$eq":["$bloque","$num_bloque"]}
                    ,"then":"igual"
                    ,"else":"diferente"
                }
            }

            ,"condicion_bloque_lote": {
                "$cond": {
                    "if":{"$eq":["$lote","$num_bloque_lote"]}
                    ,"then":"igual"
                    ,"else":"diferente"
                }
            }
        }
    }


    , {
        "$match": {
            "condicion_bloque": "diferente"
            ,"condicion_bloque_lote": "diferente"
        }
    }







)
