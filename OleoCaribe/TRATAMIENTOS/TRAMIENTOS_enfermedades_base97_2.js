db.form_tratamientossanidad.aggregate(

    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-18T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        { "$match": { "Point.farm": "5d8e867e0b4cc6ad7dfff3b5" } },

        //--------


        //---Condicionales
        { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        }


        , { "$unwind": "$Arbol.features" }

        ////test arbol
        // , {
        //     $match: {
        //         "Arbol.features.properties.name": "7-5-U9-1-7"
        //     }
        // }

        , {
            "$group": {
                "_id": {
                    "palma": "$Arbol.features.properties.name",
                    "plaga_enfermedad": "$Plaga o enfermedad",

                    //fechas
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
                }
            }
        }




        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    //fechas
                    "fecha_filtro_inicio": "$_id.Busqueda inicio_menos_30_dias",
                    "fecha_filtro_fin": "$_id.Busqueda fin",
                    // "fecha_filtro_fin": "$_id.Busqueda inicio",

                    //agrupacion
                    "plaga_enfermedad": "$_id.plaga_enfermedad",
                    "palma": "$_id.palma"

                },
                "pipeline": [

                    //---Condicionales
                    { "$match": { "Enfermedad": { "$ne": "" } } }


                    //----!!!!!!!!!!!!!!!!!DANGER !!!!!!!
                    //----MUY MUY LENTO, COSTOSO 1 A 1 COMPUTACIONALMENTE

                    // // Fechas
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$and": [
                    //                 { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                    //                 { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                    //             ]
                    //         }
                    //     }
                    // }

                    // // //--group
                    , { "$unwind": "$Arbol.features" }
                    , {
                        "$group": {
                            "_id": {
                                "palma": "$Arbol.features.properties.name",
                                "plaga_enfermedad": "$Enfermedad",
                            }
                            , "data": {
                                //"$push":"$$ROOT"
                                "$push": "$rgDate"
                            }
                        }
                    }

                    // //Cartografia
                    , {
                        "$match": {
                            "$expr": { "$eq": ["$$palma", "$_id.palma"] }
                            // "$expr": { "$in": ["$$palma", "$Arbol.features.properties.name"] }
                        }
                    }

                    // // //Enfermedad
                    // , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] } } }
                    , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$_id.plaga_enfermedad"] } } }





                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            //"formulario_enfermedades": "$formulario_enfermedades"
                            "formulario_enfermedades": { "$ifNull": ["$formulario_enfermedades", ""] }
                        }
                    ]
                }
            }
        }


        //--cruzar fechas
        //-----condicion de 30 dias
        , {
            "$addFields": {
                "planta_enferma_menos_30_dias": {
                    "$filter": {
                        "input": "$formulario_enfermedades.data",
                        "as": "data_fecha_enfermedad",
                        "cond": {
                            //"$ne": ["$$data_fecha_enfermedad", ""]
                            //  "$expr": {
                            "$and": [
                                { "$gte": ["$$data_fecha_enfermedad", "$Busqueda inicio_menos_30_dias"] },
                                { "$lte": ["$$data_fecha_enfermedad", "$Busqueda fin"] }
                            ]
                            // }
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "check_planta_enferma_menos_30_dias": {
                    "$cond": {
                        "if": { "$or": [{ "$eq": ["$planta_enferma_menos_30_dias", null] }, { "$eq": ["$planta_enferma_menos_30_dias", []] }] },
                        "then": "no",
                        "else": "si"
                    }
                }
            }
        }



    ]
)
