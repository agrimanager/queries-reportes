db.form_tratamientossanidad.aggregate(

    [

        //---Cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "UMA": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "UMA": "$UMA.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    "palma": "$Planta",
                    "plaga_enfermedad":"$Plaga o enfermedad",
                    //fechas
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
                                    { "$in": ["$$palma", "$Arbol.features.properties.name"] },
                                    { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] },
                                    //fechas
                                    
                                ]
                            }
                        }
                    }

                    // ,{
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // }
                    // ,{
                    //     "$limit": 1
                    // }

                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }

    ]
)