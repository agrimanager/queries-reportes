db.form_tratamientossanidad.aggregate(

    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-01T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //--------


        //---Condicionales
        { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        }

        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    //fechas
                    "fecha_filtro_inicio": "$Busqueda inicio_menos_30_dias",
                    "fecha_filtro_fin": "$Busqueda fin",

                    //agrupacion
                    // "palma": "$Planta",
                    "plaga_enfermedad": "$Plaga o enfermedad"

                },
                "pipeline": [
                    //Enfermedad
                    { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] } } }

                    //Fechas
                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                                    { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": false
            }
        }




        // //---cruzar con enfermedades
        // , {
        //     "$lookup": {
        //         "from": "form_modulodeenfermedades",
        //         "as": "formulario_enfermedades",
        //         "let": {
        //             //fechas
        //             "fecha_filtro_inicio": "$Busqueda inicio_menos_30_dias",
        //             "fecha_filtro_fin": "$Busqueda fin",

        //             //agrupacion
        //             // "palma": "$Planta",
        //             "plaga_enfermedad": "$Plaga o enfermedad"
        //             // "plaga_enfermedad": "$plaga_enfermedad"

        //         },
        //         "pipeline": [

        //               {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             // { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
        //                             // { "$in": ["$$palma", "$Arbol.features.properties.name"] },
        //                             { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] },
        //                             //fechas

        //                         ]
        //                     }
        //                 }
        //             }
        //             //---Condicionales
        //             //{"$match": {"Enfermedad": {"$ne": ""}}},
        //             //{"$match": {"Enfermedad": {"$eq": "$$plaga_enfermedad"}}},
        //             // {
        //             //     "$match": {
        //             //         "$expr": {
        //             //             "$and": [
        //             //                 //fechas
        //             //                 { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
        //             //                 { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
        //             //             ]
        //             //         }
        //             //     }
        //             // },
        //             // {
        //             //     "$match": {
        //             //         "$expr": {
        //             //             "$and": [
        //             //                 { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] },

        //             //                 // //fechas
        //             //                 // { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
        //             //                 // { "$lte": ["$rgDate", "$$fecha_filtro_fin"] },

        //             //                 // { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
        //             //                 // { "$in": ["$$palma", "$Arbol.features.properties.name"] },



        //             //             ]
        //             //         }
        //             //     }
        //             // }

        //             // ,{
        //             //     "$sort": {
        //             //         "rgDate": -1
        //             //     }
        //             // }
        //             // ,{
        //             //     "$limit": 1
        //             // }

        //         ]
        //     }
        // }
        // , {
        //     "$unwind": {
        //         "path": "$formulario_enfermedades",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // }




    ]
)
