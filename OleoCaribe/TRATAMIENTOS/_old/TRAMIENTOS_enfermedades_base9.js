db.form_tratamientossanidad.aggregate(

    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-01T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //--------


        //---Condicionales
        { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        }


        , { "$unwind": "$Arbol.features" }

        , {
            "$group": {
                "_id": {
                    "palma": "$Arbol.features.properties.name",
                    "plaga_enfermedad": "$Plaga o enfermedad",

                    //fechas
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
                }
            }
        }




        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    // //fechas
                    // "fecha_filtro_inicio": "$Busqueda inicio_menos_30_dias",
                    // // "fecha_filtro_fin": "$Busqueda fin",
                    // "fecha_filtro_fin": "$Busqueda inicio",

                    // //agrupacion
                    // "plaga_enfermedad": "$Plaga o enfermedad",

                    // // "palma": "$Planta",
                    // //"palma": "$Arbol.features._id",
                    // "palma": "$Arbol.features.properties.name"


                    //fechas
                    "fecha_filtro_inicio": "$_id.Busqueda inicio_menos_30_dias",
                    // "fecha_filtro_fin": "$Busqueda fin",
                    "fecha_filtro_fin": "$_id.Busqueda inicio",

                    //agrupacion
                    "plaga_enfermedad": "$_id.plaga_enfermedad",

                    // "palma": "$Planta",
                    //"palma": "$Arbol.features._id",
                    "palma": "$_id.palma"


                },
                "pipeline": [

                    // Fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                                    { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                                ]
                            }
                        }
                    }

                    // //Enfermedad
                    , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] } } }

                    
                    //--group
                    , { "$unwind": "$Arbol.features" }
                    , {
                        "$group": {
                            "_id": {
                                "palma": "$Arbol.features.properties.name",
                                // "plaga_enfermedad": "$Plaga o enfermedad",

                                // //fechas
                                // "Busqueda inicio": "$Busqueda inicio",
                                // "Busqueda fin": "$Busqueda fin",
                                // "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
                            }
                        }
                    }


                    //Cartografia
                    // ,{
                    //     "$match": {
                    //         //"$expr": { "$in": ["$$palma", "$Arbol.features.properties.name"] }
                    //         "$expr": { "$eq": ["$$palma", "$_id.palma"] }
                    //     }
                    // }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }




    ]
)
