db.form_tratamientossanidad.aggregate(

    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-01T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //--------


        //---Condicionales
        {"$match": {"Plaga o enfermedad": {"$ne": ""}}},


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        }



        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    //fechas
                    "fecha_filtro_inicio": "$Busqueda inicio_menos_30_dias",
                    "fecha_filtro_fin": "$Busqueda fin",

                    //agrupacion
                    // "palma": "$Planta",
                    "plaga_enfermedad": "$Plaga o enfermedad"

                },
                "pipeline": [
                    //---Condicionales
                    {"$match": {"Enfermedad": {"$ne": ""}}},
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //fechas
                                    { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                                    { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                                ]
                            }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] },

                                    // //fechas
                                    // { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                                    // { "$lte": ["$rgDate", "$$fecha_filtro_fin"] },

                                    // { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
                                    // { "$in": ["$$palma", "$Arbol.features.properties.name"] },



                                ]
                            }
                        }
                    }

                    // ,{
                    //     "$sort": {
                    //         "rgDate": -1
                    //     }
                    // }
                    // ,{
                    //     "$limit": 1
                    // }

                ]
            }
        }
        // , {
        //     "$unwind": {
        //         "path": "$formulario_enfermedades",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }




        // //---Cartografia
        // ,{
        //     "$addFields": {
        //         "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": {
        //             "$concatArrays": [
        //                 "$split_path_oid",
        //                 "$features_oid"
        //             ]
        //         }
        //     }
        // },
        // {
        //     "$lookup": {
        //         "from": "cartography",
        //         "localField": "split_path_oid",
        //         "foreignField": "_id",
        //         "as": "objetos_del_cultivo"
        //     }
        // },
        // {
        //     "$addFields": {
        //         "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
        //         "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
        //         "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
        //         "UMA": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
        //         "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
        //         "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "Bloque": "$Bloque.properties.name",
        //         "Lote": "$Lote.properties.name",
        //         "UMA": "$UMA.properties.name",
        //         "Linea": "$Linea.properties.name",
        //         "Planta": "$Planta.properties.name"
        //     }
        // },
        // {
        //     "$lookup": {
        //         "from": "farms",
        //         "localField": "Finca._id",
        //         "foreignField": "_id",
        //         "as": "Finca"
        //     }
        // },
        // {
        //     "$addFields": {
        //         "Finca": "$Finca.name"
        //     }
        // },
        // { "$unwind": "$Finca" },
        // {
        //     "$project": {
        //         "split_path": 0,
        //         "split_path_oid": 0,
        //         "objetos_del_cultivo": 0,
        //         "features_oid": 0
        //     }
        // }




    ]
)
