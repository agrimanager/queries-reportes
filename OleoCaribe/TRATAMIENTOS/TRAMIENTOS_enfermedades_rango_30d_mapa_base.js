//--mapa xxxx
db.form_tratamientossanidad.aggregate(
    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-18T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        { "$match": { "Point.farm": "5d8e867e0b4cc6ad7dfff3b5" } },

        //--------

        //---Condicionales
        { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        },



        //---mapa


        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        //---
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Cartography.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        //---
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
                // ,"Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                // ,"Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },


        //---Cartography = lote (geometria a dibujar)
        {
            "$addFields": {
                "Cartography": "$Lote"
            }
        },


        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Uma": "$Uma.properties.name"
                // ,"Linea": "$Linea.properties.n,ame"
                // ,"Planta": "$Planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }



        //-----Agrupacion
        , { "$unwind": "$Arbol.features" }
        , {
            "$group": {
                "_id": {
                    // "palma": "$Planta",
                    "palma": "$Arbol.features.properties.name",
                    "lote": "$Lote",
                    "Cartography":"$Cartography",//---ojo
                    "uma": "$UMA",
                    "plaga_enfermedad": "$Plaga o enfermedad",

                    //fechas
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
                }
            }
        }
        
        
        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    "plaga_enfermedad": "$_id.plaga_enfermedad",
                    "palma": "$_id.palma"

                },
                "pipeline": [

                    //---Condicionales
                    { "$match": { "Enfermedad": { "$ne": "" } } }

                    // // //--group
                    , { "$unwind": "$Arbol.features" }
                    , {
                        "$group": {
                            "_id": {
                                "palma": "$Arbol.features.properties.name",
                                "plaga_enfermedad": "$Enfermedad",
                            }
                            , "data": {
                                //"$push":"$$ROOT"
                                "$push": "$rgDate"
                            }
                        }
                    }

                    // //Cartografia
                    , {
                        "$match": {
                            "$expr": { "$eq": ["$$palma", "$_id.palma"] }
                            // "$expr": { "$in": ["$$palma", "$Arbol.features.properties.name"] }
                        }
                    }

                    // // //Enfermedad
                    // , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] } } }
                    , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$_id.plaga_enfermedad"] } } }

                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            //"formulario_enfermedades": "$formulario_enfermedades"
                            "formulario_enfermedades": { "$ifNull": ["$formulario_enfermedades", ""] }
                        }
                    ]
                }
            }
        }


        //--cruzar fechas
        //-----condicion de 30 dias
        , {
            "$addFields": {
                "planta_enferma_menos_30_dias": {
                    "$filter": {
                        "input": "$formulario_enfermedades.data",
                        "as": "data_fecha_enfermedad",
                        "cond": {
                            //"$ne": ["$$data_fecha_enfermedad", ""]
                            //  "$expr": {
                            "$and": [
                                { "$gte": ["$$data_fecha_enfermedad", "$Busqueda inicio_menos_30_dias"] },
                                { "$lte": ["$$data_fecha_enfermedad", "$Busqueda fin"] }
                            ]
                            // }
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "check_planta_enferma_menos_30_dias": {
                    "$cond": {
                        "if": { "$or": [{ "$eq": ["$planta_enferma_menos_30_dias", null] }, { "$eq": ["$planta_enferma_menos_30_dias", []] }] },
                        "then": 0,
                        "else": 1
                    }
                }
            }
        }



        //-----Agrupacion
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Cartography":"$Cartography",//---ojo
                    // "uma": "$uma",
                    "plaga_enfermedad": "$plaga_enfermedad",
                }
                , "cantidad_tratadas": { "$sum": 1 }
                , "cantidad_enfermas": { "$sum": "$check_planta_enferma_menos_30_dias" }
            }
        }
        
        
        
        
        

        //--------------color mapa
        , {
            "$addFields": {
                // "pct_incidencia": {
                "rango_tratamiento_en_30_dias": {
                    "$multiply": [
                        { "$divide": ["$cantidad_enfermas", "$cantidad_tratadas"] }
                        , 100
                    ]
                }
            }
        }



        // //--leyenda reporte
        //------
        //Rango Tratamiento:#ffffff,(<85%):#rojo,[85%-90%):#Amarillo,[90%-100%]:#verde
        //Rango Tratamiento:#ffffff,(<85%):#ff0000,[85%-90%):#ffff00,[90%-100%]:#00b050

        ,{
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 0] }, { "$lt": ["$rango_tratamiento_en_30_dias", 85] }]
                        },
                        "then": "#ff0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 85] }, { "$lt": ["$rango_tratamiento_en_30_dias",90] }]
                                },
                                "then": "#ffff00",
                                "else": "#00b050"
                            }
                        }
                    }
                },
               "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 0] }, { "$lt": ["$rango_tratamiento_en_30_dias", 85] }]
                        },
                        "then": "A-(<85%)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 85] }, { "$lt": ["$rango_tratamiento_en_30_dias",90] }]
                                },
                                "then": "B-[85%-90%)",
                                "else": "C-[90%-100%]"
                            }
                        }
                    }
                }
            }
        }




        ,{
            "$project": {
                // "_id": {
                //     "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                // },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.lote",
                    "Enfermedad": "$_id.plaga_enfermedad",
                    "Rango": "$rango_tratamiento_en_30_dias",

                    "color": "$color"
                },
                "geometry": 
                // {
                    //"$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
                    // "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                    "$_id.Cartography.geometry"
                // }
            }
        }



    ]

    , { "allowDiskUse": true })