[

    { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


    {
        "$addFields": {
            "Busqueda inicio_menos_30_dias": {
                "$subtract": [
                    "$Busqueda inicio",
                    { "$multiply": [30, 86400000] }
                ]
            }
        }
    },


    {
        "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    },

    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
        }
    },


    {
        "$addFields": {
            "Cartography": "$Uma"
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Uma": "$Uma.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }

    , { "$unwind": "$Arbol.features" }
    , {
        "$group": {
            "_id": {
                "palma": "$Arbol.features.properties.name",
                "lote": "$Lote",
                "uma": "$Uma",
                "Cartography": "$Cartography",
                "plaga_enfermedad": "$Plaga o enfermedad",


                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",
                "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
            }
        }
    }


    , {
        "$lookup": {
            "from": "form_modulodeenfermedades",
            "as": "formulario_enfermedades",
            "let": {
                "plaga_enfermedad": "$_id.plaga_enfermedad",
                "palma": "$_id.palma"

            },
            "pipeline": [
                { "$match": { "Enfermedad": { "$ne": "" } } }

                , { "$unwind": "$Arbol.features" }
                , {
                    "$group": {
                        "_id": {
                            "palma": "$Arbol.features.properties.name",
                            "plaga_enfermedad": "$Enfermedad"
                        }
                        , "data": {
                            "$push": "$rgDate"
                        }
                    }
                }
                , {
                    "$match": {
                        "$expr": { "$eq": ["$$palma", "$_id.palma"] }
                    }
                }
                , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$_id.plaga_enfermedad"] } } }

            ]
        }
    }
    , {
        "$unwind": {
            "path": "$formulario_enfermedades",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "formulario_enfermedades": { "$ifNull": ["$formulario_enfermedades", ""] }
                    }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "planta_enferma_menos_30_dias": {
                "$filter": {
                    "input": "$formulario_enfermedades.data",
                    "as": "data_fecha_enfermedad",
                    "cond": {
                        "$and": [
                            { "$gte": ["$$data_fecha_enfermedad", "$Busqueda inicio_menos_30_dias"] },
                            { "$lte": ["$$data_fecha_enfermedad", "$Busqueda fin"] }
                        ]
                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "check_planta_enferma_menos_30_dias": {
                "$cond": {
                    "if": { "$or": [{ "$eq": ["$planta_enferma_menos_30_dias", null] }, { "$eq": ["$planta_enferma_menos_30_dias", []] }] },
                    "then": 0,
                    "else": 1
                }
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "uma": "$uma",
                "Cartography": "$Cartography",
                "plaga_enfermedad": "$plaga_enfermedad"
            }
            , "cantidad_tratadas": { "$sum": 1 }
            , "cantidad_enfermas": { "$sum": "$check_planta_enferma_menos_30_dias" }
        }
    }


    , {
        "$addFields": {
            "rango_tratamiento_en_30_dias": {
                "$multiply": [
                    { "$divide": ["$cantidad_enfermas", "$cantidad_tratadas"] }
                    , 100
                ]
            }
        }
    }

    , {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 0] }, { "$lt": ["$rango_tratamiento_en_30_dias", 85] }]
                    },
                    "then": "#ff0000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 85] }, { "$lt": ["$rango_tratamiento_en_30_dias", 90] }]
                            },
                            "then": "#ffff00",
                            "else": "#00b050"
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 0] }, { "$lt": ["$rango_tratamiento_en_30_dias", 85] }]
                    },
                    "then": "A-(<85%)",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$rango_tratamiento_en_30_dias", 85] }, { "$lt": ["$rango_tratamiento_en_30_dias", 90] }]
                            },
                            "then": "B-[85%-90%)",
                            "else": "C-[90%-100%]"
                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
"_id":null,
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.lote",
                "Uma": "$_id.uma",
                "Enfermedad": "$_id.plaga_enfermedad",
                "Rango": "$rango",

                "color": "$color"
            },
            "geometry": "$_id.Cartography.geometry"

        }
    }



]