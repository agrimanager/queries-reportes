db.form_tratamientossanidad.aggregate(

    [

        //---test---///filtro de fechas
        { "$addFields": { "Busqueda inicio": ISODate("2020-04-18T19:00:00.000-05:00") } },
        { "$addFields": { "Busqueda fin": new Date } },

        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        { "$match": { "Point.farm": "5d8e867e0b4cc6ad7dfff3b5" } },

        //--------


        //---Condicionales
        { "$match": { "Plaga o enfermedad": { "$ne": "" } } },


        //---Variables auxiliares
        {
            "$addFields": {
                "Busqueda inicio_menos_30_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [30, 86400000] }
                    ]
                }
            }
        },




        //---Cartografia
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "UMA": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },
        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "UMA": "$UMA.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }


        //-----Agrupacion
        , {
            "$group": {
                "_id": {
                    "palma": "$Planta",
                    "lote": "$Lote",
                    "uma": "$UMA",
                    "plaga_enfermedad": "$Plaga o enfermedad",

                    //fechas
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "Busqueda inicio_menos_30_dias": "$Busqueda inicio_menos_30_dias"
                }
            }
        }




        //---cruzar con enfermedades
        , {
            "$lookup": {
                "from": "form_modulodeenfermedades",
                "as": "formulario_enfermedades",
                "let": {
                    //fechas
                    "fecha_filtro_inicio": "$_id.Busqueda inicio_menos_30_dias",
                    "fecha_filtro_fin": "$_id.Busqueda fin",
                    // "fecha_filtro_fin": "$_id.Busqueda inicio",

                    //agrupacion
                    "plaga_enfermedad": "$_id.plaga_enfermedad",
                    "palma": "$_id.palma"

                },
                "pipeline": [

                    //---Condicionales
                    { "$match": { "Enfermedad": { "$ne": "" } } }


                    //----!!!!!!!!!!!!!!!!!DANGER !!!!!!!
                    //----MUY MUY LENTO, COSTOSO 1 A 1 COMPUTACIONALMENTE

                    // // Fechas
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$and": [
                    //                 { "$gte": ["$rgDate", "$$fecha_filtro_inicio"] },
                    //                 { "$lte": ["$rgDate", "$$fecha_filtro_fin"] }
                    //             ]
                    //         }
                    //     }
                    // }

                    // // //--group
                    , { "$unwind": "$Arbol.features" }
                    , {
                        "$group": {
                            "_id": {
                                "palma": "$Arbol.features.properties.name",
                                "plaga_enfermedad": "$Enfermedad",
                            }
                            , "data": {
                                //"$push":"$$ROOT"
                                "$push": "$rgDate"
                            }
                        }
                    }

                    // //Cartografia
                    , {
                        "$match": {
                            "$expr": { "$eq": ["$$palma", "$_id.palma"] }
                            // "$expr": { "$in": ["$$palma", "$Arbol.features.properties.name"] }
                        }
                    }

                    // // //Enfermedad
                    // , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$Enfermedad"] } } }
                    , { "$match": { "$expr": { "$eq": ["$$plaga_enfermedad", "$_id.plaga_enfermedad"] } } }





                ]
            }
        }
        , {
            "$unwind": {
                "path": "$formulario_enfermedades",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            //"formulario_enfermedades": "$formulario_enfermedades"
                            "formulario_enfermedades": { "$ifNull": ["$formulario_enfermedades", ""] }
                        }
                    ]
                }
            }
        }


        //--cruzar fechas
        //-----condicion de 30 dias
        , {
            "$addFields": {
                "planta_enferma_menos_30_dias": {
                    "$filter": {
                        "input": "$formulario_enfermedades.data",
                        "as": "data_fecha_enfermedad",
                        "cond": {
                            //"$ne": ["$$data_fecha_enfermedad", ""]
                            //  "$expr": {
                            "$and": [
                                { "$gte": ["$$data_fecha_enfermedad", "$Busqueda inicio_menos_30_dias"] },
                                { "$lte": ["$$data_fecha_enfermedad", "$Busqueda fin"] }
                            ]
                            // }
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "check_planta_enferma_menos_30_dias": {
                    "$cond": {
                        "if": { "$or": [{ "$eq": ["$planta_enferma_menos_30_dias", null] }, { "$eq": ["$planta_enferma_menos_30_dias", []] }] },
                        "then": 0,
                        "else": 1
                    }
                }
            }
        }



        //-----Agrupacion
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    // "uma": "$uma",
                    "plaga_enfermedad": "$plaga_enfermedad",
                }
                , "cantidad_tratadas": { "$sum": 1 }
                , "cantidad_enfermas": { "$sum": "$check_planta_enferma_menos_30_dias" }
            }
        }


    ]
)