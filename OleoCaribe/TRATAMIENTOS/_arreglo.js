//--1)agregar datos con nombre de maestro desactualizado
//----------------------------------------------------------
var cursor = db.form_tratamientossanidad.aggregate({
    $match: {
        "Enfermedad": {
            $exists: true
        }, //viejo nombre de maestro
        "Plaga o enfermedad": {
            $exists: false
        } //nuevo nombre de maestro
    }
});
// cursor
cursor.forEach(i => {
    db.form_tratamientossanidad.update({
        _id: i._id
    }, {
            $set: {
                "Plaga o enfermedad": i.Enfermedad
            }
        })
});




// //--2)borrar datos con nombre de maestro desactualizado
// //----------------------------------------------------------
// Borrar columna (ejemplo)
// db.form_cosechafrutapalmadeaceite.update( {"Racimos Recolector 3":{$exists:true}}, { $unset: { "Racimos Recolector 1": 1 } }, false, true);
var cursor = db.form_tratamientossanidad.aggregate({
    $match: {
        "Enfermedad": {
            $exists: true
        }, //viejo nombre de maestro
        // "Plaga o enfermedad":{$exists:false}//nuevo nombre de maestro
    }
});
// cursor
cursor.forEach(i => {
    db.form_tratamientossanidad.update({
        _id: i._id
    }, {
            $unset: {
                "Enfermedad": 1
            }
        })
});




// //--3)agregar datos nuevos
// //----------------------------------------------------------
var cursor = db.form_tratamientossanidad.aggregate({
    $match: {
        "Dar de alta": { $exists: true }
    }
});
// cursor
cursor.forEach(i => {
    db.form_tratamientossanidad.update({
        _id: i._id
    }, {
            $set: {
                "Refuerzo Nutricional" : "No"
            }
        })
});



// //--4)borrar datos viejos eliminados
// //----------------------------------------------------------
// Borrar columna (ejemplo)
// db.form_cosechafrutapalmadeaceite.update( {"Racimos Recolector 3":{$exists:true}}, { $unset: { "Racimos Recolector 1": 1 } }, false, true);
var cursor = db.form_tratamientossanidad.aggregate({
    $match: {
        // "Grado Enfermedad": {$exists: true}

        "Dar de alta": { $exists: true }

        // "Erradicar": {$exists: true}
    }
});
// cursor
cursor.forEach(i => {
    db.form_tratamientossanidad.update({
        _id: i._id
    }, {
            $unset: {
                // "Enfermedad": 1

                "Dar de alta": 1,
                "Erradicar": 1,
                "Ultimo tratamiento": 1,
                "Grado Enfermedad": 1,
                "Enfermedad que afecto": 1,
                "Fisiopatia": 1,
                "Observaciones": 1
            }
        })
});


// "Dar de alta"
// "Erradicar"
//  "Ultimo tratamiento" : 
// "Grado Enfermedad"
//  "Enfermedad que afecto" : 
//  "Fisiopatia" : "",
// Observaciones




// //--5)borrar datos innecesarios
//AND
//--donde plaga o enfermedad = ""
//--donde Rezuerfo nutricional = "No"