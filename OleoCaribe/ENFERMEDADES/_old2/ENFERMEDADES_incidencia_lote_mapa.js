//--mapa x lote
db.form_modulodeenfermedades.aggregate(
    [

        // // //-----
        // { $unwind: "$Arbol.features" },
        // {
        //     "$addFields": {
        //         "today": new Date,
        //         "Cartography": "$Arbol.features"
        //     }
        // },
        // // //-----

        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        //---
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Cartography.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        //---
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
                // ,"Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                // ,"Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },




        {
            "$lookup": {
                "from": "cartography_polygon",
                "localField": "Lote._id",
                "foreignField": "_id",
                "as": "info_polygon"
            }
        },
        {
            "$addFields": {
                "info_polygon": "$info_polygon.internal_features"
            }
        },
        { "$unwind": "$info_polygon" },

        {
            "$addFields": {
                "plantas_x_poligono": {
                    "$filter": {
                        "input": "$info_polygon",
                        "as": "item",
                        "cond": { "$eq": ["$$item.type_features", "trees"] }
                    }
                }
            }
        },
        { "$unwind": "$plantas_x_poligono" },
        {
            "$addFields": {
                "plantas_x_poligono": "$plantas_x_poligono.count_features"
            }
        },

        //---Cartography = lote (geometria a dibujar)
        {
            "$addFields": {
                "Cartography": "$Lote"
            }
        },


        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Uma": "$Uma.properties.name"
                // ,"Linea": "$Linea.properties.n,ame"
                // ,"Planta": "$Planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_polygon": 0
            }
        }

        , {
            "$group": {
                "_id": {
                    "Finca": "$Finca",
                    "Lote": "$Lote",
                    "Enfermedad": "$Enfermedad",
                    "plantas_x_poligono": "$plantas_x_poligono",
                    
                    "idform": "$idform"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "num_plantas_dif": { "$sum": 1 }
            }
        }


        , {
            "$addFields": {
                "pct_incidencia": {
                    "$multiply": [
                        { "$divide": ["$num_plantas_dif", "$_id.plantas_x_poligono"] }
                        , 100
                    ]
                }
            }
        }
        
        
        
        //--leyenda reporte
        //[0%-3%]: #05ac50, (3%-6%]: #ffff01, (>6%): #fd0100
        
        ,{
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                        },
                        "then": "#05ac50",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia",6] }]
                                },
                                "then": "#ffff01",
                                "else": "#fd0100"
                            }
                        }
                    }
                },
                 "rango_incidencia": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                        },
                        "then": "[0%-3%]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia",6] }]
                                },
                                "then": "(3%-6%]",
                                "else": "(>6%)"
                            }
                        }
                    }
                }
            }
        }
        
        
        
        
        ,{
            "$project": {
                "_id": {
                    "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.Lote",
                    "Enfermedad": "$_id.Enfermedad",
                    "Rango": "$rango_incidencia",
                    
                    "color": "$color"
                },
                "geometry": {
                    //"$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
                    "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                }
            }
        }



    ]

    , { "allowDiskUse": true })