db.cartography_polygon.aggregate(

    {
        $group: {
            _id: null,
            data1: { $push: "$$ROOT" }
        }
    }

    , {
        "$lookup": {
            "from": "cartography_polygon_Guapinol",
            "as": "data2",
            "let": {},
            "pipeline": []
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data1"
                    , "$data2"
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


    // //---- Insertar en nueva coleccion
    , { $out: "cartography_polygon" }




)
// .count()
