db.form_modulodeenfermedades.aggregate(
    [

        // // //-----
        // { $unwind: "$Arbol.features" },
        // {
        //     "$addFields": {
        //         "today": new Date,
        //         "Cartography": "$Arbol.features"
        //     }
        // },
        // // //-----

        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        //---
        // {
        //     "$addFields": {
        //         "features_oid": { "$map": { "input": "$Cartography.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        //     }
        // },
        //---
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "Uma": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
                "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },




        {
            "$lookup": {
                "from": "cartography_polygon",
                "localField": "Lote._id",
                "foreignField": "_id",
                "as": "info_lote"
            }
        },
        {
            "$addFields": {
                "info_lote": "$info_lote.internal_features"
            }
        },
        { "$unwind": "$info_lote" },

        {
            "$addFields": {
                "plantas_x_lote": {
                    "$filter": {
                        "input": "$info_lote",
                        "as": "item",
                        "cond": { "$eq": ["$$item.type_features", "trees"] }
                    }
                }
            }
        },
        { "$unwind": "$plantas_x_lote" },
        {
            "$addFields": {
                "plantas_x_lote": "$plantas_x_lote.count_features"
            }
        },




        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "Lote": "$Lote.properties.name",
                "Uma": "$Uma.properties.name",
                "Linea": "$Linea.properties.name",
                "Planta": "$Planta.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "info_lote": 0
            }
        }

        , {
            "$group": {
                "_id": {
                    "Finca": "$Finca",
                    "Lote": "$Lote",
                    "Enfermedad": "$Enfermedad",
                    "plantas_x_lote": "$plantas_x_lote",
                },
                "num_plantas_dif": { "$sum": 1 }
            }
        }


        , {
            "$addFields": {
                "pct_incidencia": {
                    "$multiply": [
                        { "$divide": ["$num_plantas_dif", "$_id.plantas_x_lote"] }
                        , 100
                    ]
                }
            }
        }
        
        
        
        //--leyenda reporte
        //[0%-3%]: #05ac50, (3%-6%]: #ffff01, (>6%): #fd0100
        
        
        ,{
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                        },
                        "then": "#05ac50",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia",6] }]
                                },
                                "then": "#ffff01",
                                "else": "#fd0100"
                            }
                        }
                    }
                },
                 "rango_incidencia": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$pct_incidencia", 0] }, { "$lte": ["$pct_incidencia", 3] }]
                        },
                        "then": "[0%-3%]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia", 3] }, { "$lte": ["$pct_incidencia",6] }]
                                },
                                "then": "(3%-6%]",
                                "else": "(>6%)"
                            }
                        }
                    }
                }
            }
        }
        
        
        
        
        // ,{
        //     "$project": {
        //         "_id": {
        //             "$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
        //         },
        //         "idform": "$_id.idform",
        //         "type": "Feature",
        //         "properties": {
        //             "Lote": "$_id.nombre_lote",
        //             "Estado del Ciclo": "$estado de ciclo",
        //             "Dias Ciclo": {
        //                 "$cond": {
        //                     "if": { "$eq": ["$dias de ciclo", -1] },
        //                     "then": "-1",
        //                     "else": {
        //                         "$concat": [
        //                             {"$toString": "$dias de ciclo"},
        //                             " dias"
        //                         ]
        //                     }
        //                 }
        //             },
        //             "color": "$color"
        //         },
        //         "geometry": {
        //             "$arrayElemAt": ["$data.Cartography.features.geometry", { "$subtract": [{ "$size": "$data.Cartography.features.geometry" }, 1] }]
        //         }
        //     }
        // }



    ]

    , { "allowDiskUse": true })