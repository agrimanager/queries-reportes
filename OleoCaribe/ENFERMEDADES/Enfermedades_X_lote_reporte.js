db.form_modulodeenfermedades.aggregate([

    //----test
    {
        $match: {
            // "Point.farm": "5fda5921c6443216da62d5c0"//Guapinol
            // "Point.farm": "5d97a53f0d362e2aa2ac1fab"//Cortés
            "Point.farm": "5d8e867e0b4cc6ad7dfff3b5"//Almendro
        }
    },


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2021-03-01T06:00:00.000-05:00"),
            "Busqueda fin": new Date,
            "today": new Date
        }
    },
    //----------------------------------------------------------------


    //----filtro de fechas
    {
        "$match": {
            "$expr": {
                "$and": [
                    {
                        "$gte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                        ]
                    },

                    {
                        "$lte": [
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                            ,
                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                        ]
                    }
                ]
            }
        }
    },
    //----------------------------------------------------------------




    //=====CARTOGRAFIA

    //--paso1 (cartografia-nombre variable y ids)
    {
        "$addFields": {
            "variable_cartografia": "$Arbol" //🚩editar
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    //--paso2 (cartografia-cruzar informacion)
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    // //--paso3 (cartografia-obtener informacion)

    //--finca
    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    //--bloque
    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    //--lote
    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    //---plantas x poligono
    //======================
    {
        "$lookup": {
            "from": "cartography_polygon",
            "localField": "lote._id",
            "foreignField": "_id",
            "as": "info_lote"
        }
    },
    {
        "$addFields": {
            "info_lote": "$info_lote.internal_features"
        }
    },
    // { "$unwind": "$info_lote" },

    // {
    //     "$addFields": {
    //         "plantas_x_lote": {
    //             "$filter": {
    //                 "input": "$info_lote",
    //                 "as": "item",
    //                 "cond": { "$eq": ["$$item.type_features", "trees"] }
    //             }
    //         }
    //     }
    // },
    // { "$unwind": "$plantas_x_lote" },
    // {
    //     "$addFields": {
    //         "plantas_x_lote": "$plantas_x_lote.count_features"
    //     }
    // },

    // { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    // //--uma
    // {
    //     "$addFields": {
    //         "uma": {
    //             "$filter": {
    //                 "input": "$objetos_del_cultivo",
    //                 "as": "item_cartografia",
    //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
    //             }
    //         }
    //     }
    // },
    // {
    //     "$unwind": {
    //         "path": "$uma",
    //         "preserveNullAndEmptyArrays": true
    //     }
    // },
    // { "$addFields": { "uma": { "$ifNull": ["$uma.properties.name", "no existe"] } } },

    // //--linea
    // {
    //     "$addFields": {
    //         "linea": {
    //             "$filter": {
    //                 "input": "$objetos_del_cultivo",
    //                 "as": "item_cartografia",
    //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
    //             }
    //         }
    //     }
    // },
    // {
    //     "$unwind": {
    //         "path": "$linea",
    //         "preserveNullAndEmptyArrays": true
    //     }
    // },
    // { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    // //--arbol
    // {
    //     "$addFields": {
    //         "arbol": {
    //             "$filter": {
    //                 "input": "$objetos_del_cultivo",
    //                 "as": "item_cartografia",
    //                 "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
    //             }
    //         }
    //     }
    // },
    // {
    //     "$unwind": {
    //         "path": "$arbol",
    //         "preserveNullAndEmptyArrays": true
    //     }
    // },
    // { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    // {
    //     "$project": {
    //         "variable_cartografia": 0,
    //         "split_path_padres": 0,
    //         "split_path_padres_oid": 0,
    //         "variable_cartografia_oid": 0,
    //         "split_path_oid": 0,
    //         "objetos_del_cultivo": 0,
    //         "tiene_variable_cartografia": 0

    //         , "Point": 0
    //         , "Arbol": 0
    //     }
    // }




])
