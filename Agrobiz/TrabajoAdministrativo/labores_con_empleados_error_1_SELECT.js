db.tasks.aggregate(
    {
        $unwind: "$employees"
    }
    , {
        $addFields: {
            num_caracteres_empleado: { "$strLenCP": "$employees" }

        }
    }
    , {
        $match: {
            num_caracteres_empleado: { $ne: 24 }
        }
    }
)
