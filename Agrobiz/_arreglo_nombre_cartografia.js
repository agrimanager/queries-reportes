
var data =db.getCollection("cartography_2022-09-02").aggregate(
    {
        $match: {
            type: "Feature"
        }
    },
    {
        $addFields: {
            nombre: "$properties.name"
        }
    }
)


data.forEach(i=>{
    db.cartography.update(
    {
        _id:i._id
    }, {
        $set:{
            "properties.name":i.nombre
        }
    }
    )
})
