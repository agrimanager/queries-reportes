db.cartography.aggregate(
    {
        $match: {
            type: "Feature"
        }
    }

    //---finca
    , {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "finca"
        }
    },

    {
        "$addFields": {
            "finca": "$finca.name"
        }
    },
    { "$unwind": "$finca" },


    {
        "$project": {
            "Finca": "$finca",
            "Bloque": "$properties.name"
        }
    }

)
