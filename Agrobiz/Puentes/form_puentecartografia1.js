
db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        { "$limit": 1 },

        {
            "$lookup": {
                "from": "form_puentecartografia",
                "as": "data_info",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "fecha_carga": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }
                        }
                    }


                    , {
                        "$project": {
                            "_id": 0,
                            "Point": 0,
                            "uid": 0,
                            "uDate": 0,
                            "capture": 0
                        }
                    }

                    , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data_info", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }






    ]
)