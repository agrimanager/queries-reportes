[
   

    { "$limit": 1 },

    {
        "$lookup": {
            "from": "form_puentelabores",
            "as": "data_info",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio"
                , "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                {
                    "$addFields": {
                        "fecha_carga": {
                            "$dateToString": {
                                "date": "$rgDate",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }
                    }
                }


                , {
                    "$project": {
                        "_id": 0,
                        "Point": 0,
                        "uid": 0,
                        "uDate": 0,
                        "capture": 0
                    }
                }

                , { "$addFields": { "rgDate": "$$filtro_fecha_inicio" } }

            ]

        }
    }


    , {
        "$project": {
            "datos": {
                "$concatArrays": ["$data_info", []]
            }
        }
    }
    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }

]