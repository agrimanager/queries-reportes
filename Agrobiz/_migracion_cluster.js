//----Ojo usar desde db local

// DBs
var db_desde = "cluster";//db1

var db_hacia = "agrobiz"; //db2
var uid_hacia = ObjectId("5ede82b75f8e783d9c7b9bec");
var supervisores_hacia = [
    ObjectId("5f0784a09d4e8f6838f42336"),
    ObjectId("5f0786a09d4e8f6838f42343"),
    ObjectId("5f0788519d4e8f6838f4234f"),
    ObjectId("5f0789879d4e8f6838f42353"),
    ObjectId("5f078a749d4e8f6838f42380"),
    ObjectId("5f078afc9d4e8f6838f42388"),
    ObjectId("5f078cd89d4e8f6838f4238c"),
    ObjectId("5f07a05f9d4e8f6838f4239e"),
    ObjectId("5f07a0b89d4e8f6838f423a1"),
    ObjectId("5f07a14e9d4e8f6838f423a4"),
    ObjectId("5f7c913082575a4c5d21b6af")
];

//============= Maestros

var mestros_desde = db.getSiblingDB(db_desde)
    .getCollection("masters")
    .aggregate(
        {
            $match: {
                _id: ObjectId("6078aa571b347435d1ac1395")
            }
        }
    )
    .allowDiskUse();


//--🔄 data
mestros_desde.forEach(item_maestro => {

    //modificar uid
    item_maestro.uid = uid_hacia;

    //insertar maestro
    db.getSiblingDB(db_hacia)
        .getCollection("masters")
        .insert(item_maestro);
});
// mestros_desde.close();



//============= Formularios

var formularios_desde = db.getSiblingDB(db_desde)
    .getCollection("forms")
    .aggregate(
        {
            $match: {
                "name": {
                    $in: [
                        "Planeacion y Cumplimiento de Actividades", "Registro de Actividades Diarias"
                    ]
                }
            }

        }
    )
    .allowDiskUse();


//--🔄 data
formularios_desde.forEach(item_formulario => {

    //modificar uid
    item_formulario.uid = uid_hacia;
    //modificar supervisors
    item_formulario.supervisors = supervisores_hacia;

    //crear coleccion
    db.getSiblingDB(db_hacia).createCollection(item_formulario.anchor).ok;

    //insertar formulario
    db.getSiblingDB(db_hacia)
        .getCollection("forms")
        .insert(item_formulario);
});
// formularios_desde.close();




// //============= Reportes

// var reportes_desde = db.getSiblingDB(db_desde)
//     .getCollection("reports")
//     .aggregate()
//     .allowDiskUse();


// //--🔄 data
// reportes_desde.forEach(item_reporte => {

//     //modificar uid
//     item_reporte.uid = uid_hacia;


//     //insertar reporte
//     db.getSiblingDB(db_hacia)
//         .getCollection("reports")
//         .insert(item_reporte);
// });
// // reportes_desde.close();
