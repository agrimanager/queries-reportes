db.form_trabajoadministrativo.aggregate(
    [


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_puentecartografia",
                "as": "data",
                "let": {},
                "pipeline": []
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



    ]

)
