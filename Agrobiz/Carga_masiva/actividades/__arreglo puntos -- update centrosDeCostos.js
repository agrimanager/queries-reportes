
//-----------------------------------------
//---------UPDATE PUNTOS


//---data con puntos
var array_data = [
    ObjectId("5f568d7d5239315c7f381812"),
    ObjectId("5f11cf70198d233a0c9df862"),
    ObjectId("5f11cfbd198d233a0c9df8ee"),
    ObjectId("5f568db95239315c7f381814"),
    ObjectId("5f11cf72198d233a0c9df864"),
    ObjectId("5f11cfbe198d233a0c9df8f0"),
    ObjectId("5f568cdb5239315c7f38180d"),
    ObjectId("5f11cf6f198d233a0c9df860"),
    ObjectId("5f11cfbc198d233a0c9df8ec"),
    ObjectId("5f568c885239315c7f38180b"),
    ObjectId("5f11cf6e198d233a0c9df85e"),
    ObjectId("5f11cfbb198d233a0c9df8ea"),
    ObjectId("5f568d5d5239315c7f381811"),
    ObjectId("5f11cf70198d233a0c9df861"),
    ObjectId("5f11cfbd198d233a0c9df8ed"),
    ObjectId("5f568cbe5239315c7f38180c"),
    ObjectId("5f11cf6f198d233a0c9df85f"),
    ObjectId("5f11cfbb198d233a0c9df8eb"),
    ObjectId("5f568d955239315c7f381813"),
    ObjectId("5f11cf71198d233a0c9df863"),
    ObjectId("5f11cfbe198d233a0c9df8ef"),
];


//---buscar data
var cursor = db.costsCenters.aggregate(
    {
        $match: {
            "_id": { $in: array_data }
        }
    }
);

// cursor


cursor.forEach(i => {

    var item_actual = i.name
    var item_nuevo = item_actual.replace('.', '')
    //replaceAll
    // console.log(item_nuevo)

    db.costsCenters.update(
        {
            _id: i._id
        },
        {
            $set: {
                "name": item_nuevo
            }
        }
    )


});

//-------tirar varias veces