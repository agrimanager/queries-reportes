Lote Bloque Kmz;Campaña;Nucleo;Asociacion;Productor;CC NIT;Lote No;Hectareas Verificadas;Yuca
22_OscarDBuelvasB_1M_1,00Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Oscar David Buelvas Bohorquez;1102228104;1;1;Metro
22_OscarDBuelvasB_2R_1,95Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Oscar David Buelvas Bohorquez;1102228104;2;1.95;Regular
22_VictorEMartinezH_1R_6,97Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Victor Emiro Martinez Hernandez;92032311;1;6.97;Regular
22_VictorEMartinezH_2R_2,57Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Victor Emiro Martinez Hernandez;92032311;2;2.57;Regular
22_VictorEMartinezH_3R_5,40Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Victor Emiro Martinez Hernandez;92032311;3;5.4;Regular
22_EugenioRSevericheN_1R_1,40Ha;2022 - 2023;Fernando Orrego;22 23 Independientes FO;Eugenio Rafael Severiche Navarro;1100542748;1;2.4;Regular
22_CarlosAPAchecoB_1R_13,40Ha;2022 - 2023;Jesus Sarmiento;22 23 Independientes JS;Carlos Alberto Pacheco Barros;72195966;1;13.4;Regular
22_ToniEGomezM_1R_5,86Ha;2022 - 2023;Julio Estrada;22 23 Independientes JE;Toni Enrique Gomez Monterroza;92190384;1;5.86;Regular
22_Coosechas_2R_1,34Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;2;1.34;Regular
22_Coosechas_3R_13,20Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;3;13.2;Regular
22_Coosechas_4M_2,49Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;4;2.49;Metro
22_Coosechas_5M_1,42Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;5;1.42;Metro
22_Coosechas_6M_3,41Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;6;3.41;Metro
22_Coosechas_7M_4,81Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;7;4.81;Metro
22_Coosechas_8M_3,32Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;8;3.32;Metro
22_Coosechas_9M_3,75Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;9;3.75;Metro
22_Coosechas_10M_1,99Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;10;1.99;Metro
22_Coosechas_11M_1,90Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Coosechas;901462122-0;11;1.9;Metro
22_IslenisPBertelA_1R_8,26Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Islenis Paola Bertel Aguas;1.102.873.144;1;8.26;Regular
22_KettyMAvilezM_1M_5,75Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Ketty Milena Avilez Macea;1.143.130.597;1;5.75;Metro
22_MarthaLPachecoS_1R_2,90Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Martha Lucia Pacheco Sierra;33.146.870;1;2.9;Regular
22_RichardFLopezE_1R_5,10Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Richard Francisco Lopez Espitia;15.590.365;1;5.1;Regular
22_RobertoGUparelaB_1R_11,20Ha;2022 - 2023;Rafael Molina;22 23 Independientes RM;Roberto Gamaliel Uparela Brid;92.532.541;1;11.2;Regular
22_BlasJMonterrosaT_1R_3,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Blas Jose Monterrosa Tamara;9306618;1;3;Regular
22_DavidMHerazoR_1M_5,50Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;David Manuel Herazo Rivera;8860630;1;5.5;Metro
22_DavidMHerazoR_2R_7,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;David Manuel Herazo Rivera;8860630;2;7;Regular
22_EduarLHerazoH_1R_1,80Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Eduar Luis Herazo Herazo;1103980940;1;1.8;Regular
22_EduarLHerazoH_2R_4,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Eduar Luis Herazo Herazo;1103980940;2;4;Regular
22_JorgeAArrietaP_1R_3,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Jorge Armando Arrieta Palencia;92671642;1;3;Regular
22_JoseLContrerasH_1M_3,70Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Jose Leonardo Contreras Herazo;1100625131;1;3.7;Metro
22_JoseLContrerasH_2M_5,70Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Jose Leonardo Contreras Herazo;1100625131;2;5.7;Metro
22_JoseLContrerasH_3R_1,70Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Jose Leonardo Contreras Herazo;1100625131;3;1.7;Regular
22_JoseLContrerasH_4R_0,80Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Jose Leonardo Contreras Herazo;1100625131;3;0.8;Regular
22_LuisAEspinozaA_1R_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Luis Alberto Espinosa Alvis;1103095882;1;2;Regular
22_LuisCMonterrozaA_1R_1,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Luis Carlos Monterroza Aguas;8860321;1;1;Regular
22_LuisCMonterrozaA_2R_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Luis Carlos Monterroza Aguas;8860321;2;2;Regular
22_LuisFSalgadoJ_1M_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Luis Fernando Salgado Jimenez;1103121960;1;2;Metro
22_MarioJAlvarezR_1M_1,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Mario de Jesus Alvarez Rivera;92550590;1;1;Metro
22_MelissaMFuentesM_1R_6,50Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Melissa Marcela Fuentes Moreno;1103980214;1;6.5;Regular
22_OlgaMOrtegaM_1R_1,50Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Olga Maria Ortega Medina;42206057;1;1.5;Regular
22_OlgaMOrtegaM_2R_3,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Olga Maria Ortega Medina;42206057;2;3;Regular
22_RafaelLBarretoC_1R_1,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Rafael Lucio Barreto Castro;92031817;1;1;Regular
22_RamiroAPerezM_1M_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Ramiro Andres Perez Monterroza;1103951464;1;2;Metro
22_RobertoMCardenasJ_1M_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Roberto Manuel Cardenas Junieles;8860833;1;2;Metro
22_YaledisSOviedoR_1M_2,50Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Yaledis del Socorro Oviedo Roman;32930866;1;2.5;Metro
22_YaledisSOviedoR_2R_2,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoagriam;Yaledis del Socorro Oviedo Roman;32930866;2;2;Regular
22_IsmaelJPerezP_1R_8,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Ismael de Jesus Perez Perez;9308881;1;8;Regular
22_JorgeLHernandezB_1R_5,60Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Jorge Luis Hernandez Baldovino;92095515;1;5.6;Metro
22_MariaASantizD_1R_5,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Maria Alejandra Santiz Diaz;1103121409;1;5;Regular
22_MiguelEOrtegaB_1R_3,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Miguel Eduardo Ortega Baldovino;1131105985;1;3;Regular
22_OmarYSuarezV_1R_4,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Omar Yesid Suarez Vargas;1099992488;1;4;Regular
22_OrlideJPerezL_1R_4,00Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Orlide de Jesus Perez Lopez;92640683;1;4;Regular
22_SaulCVelillaB_1R_1,0Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Saul del Cristo Velilla Barboza;92553665;1;1;Regular
22_YudysCAscencioL_1R_6,50Ha;2022 - 2023;Edwin Uparela;22 23 Asoproccor;Yudys del Carmen Ascencio Lopez;22616061;1;6.5;Regular
22_JorgeAGuzmanP_1R_10,80Ha;2022 - 2023;Edwin Uparela;22 23 Independientes EU;Jorge Alberto Guzman Padilla;1102838860;1;10.8;Regular
22_JorgeLMartinezP_1M_6,00Ha;2022 - 2023;Edwin Uparela;22 23 Independientes EU;Jorge Luis Martinez Paez;1193435545;1;6;Metro
22_ArmandoJVelillasN_1M_0,61Ha;2022 - 2023;Jose Benitez;22 23 Cooagrosanpedro;Armando de Jesus Velillas Navas;92185869;1;0.61;Metro
22_ArmandoJVelillasN_2R_2,39Ha;2022 - 2023;Jose Benitez;22 23 Cooagrosanpedro;Armando de Jesus Velillas Navas;92185869;2;2.39;Regular
22_ArmandoJVelillasN_3R_1,44Ha;2022 - 2023;Jose Benitez;22 23 Cooagrosanpedro;Armando de Jesus Velillas Navas;92185869;3;1.44;Regular
22_ArmandoJVelillasN_4M_3,16Ha;2022 - 2023;Jose Benitez;22 23 Cooagrosanpedro;Armando de Jesus Velillas Navas;92185869;4;3.16;Metro
22_AntoniaPDurangoR_2R_2,10Ha;2022 - 2023;Jose Benitez;22 23 Nuevo Santa Ines;Antonia Patricia Durango Romero;1104010224;2;2.1;Regular
22_AntonioCOspinoC_1R_11,50Ha;2022 - 2023;Manuel Meneses;22 23 Independientes MM;Antonio Carlos Ospino Caliz;17079455;1;11.5;Regular
22_AntonioCOspinoC_2R_12,40Ha;2022 - 2023;Manuel Meneses;22 23 Independientes MM;Antonio Carlos Ospino Caliz;17079455;2;12.4;Regular
