var cursor = db.form_puentecartografia.aggregate(
    {
        $match: {
            "Empleado Identificacion": { $regex: `XXXXX` }
        }
    }
    , { "$addFields": { "Empleado_Identificacion": "$Empleado Identificacion" } }
);

// cursor

cursor.forEach(i => {

    var item_actual = i.Empleado_Identificacion
    var item_nuevo = item_actual.replace('XXXXX', '.')

    db.form_puentecartografia.update(
        {
            _id: i._id
        },
        {
            $set: {
                "Empleado Identificacion": item_nuevo
            }
        }
    )


});

