[


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_puentecartografia",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [


                    {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    },
                    {
                      "$project":{
                        "Point":0
                      }
                    }
                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
