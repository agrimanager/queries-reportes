[

    {
        "$match": {
            "Campaña": { "$exists": false }
        }
    },
    { "$sort": { "rgDate": -1 } },
    { "$addFields": { "tipo_formulario": "Con KMZ" } },

    {
        "$addFields": {
            "variable_cartografia": "$Bloque"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "variable_puente_kmz": "$variable_cartografia.features.properties.name"
        }
    }


    , {
        "$addFields": {
            "Descripcion": "$Descrpcion"
        }
    }

    , {
        "$project": {
            "Point": 0
            , "uid": 0
            , "Formula": 0
            , "Bloque": 0

            , "Descrpcion": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_puentecartografia",
            "as": "info_form_puentecartografia",
            "let": {
                "variable_puente_kmz": "$variable_puente_kmz"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Lote Bloque Kmz", "$$variable_puente_kmz"] }
                            ]
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_form_puentecartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "Campaña": { "$ifNull": ["$info_form_puentecartografia.Campaña", ""] }
            , "Nucleo": { "$ifNull": ["$info_form_puentecartografia.Nucleo", ""] }
            , "Asociacion": { "$ifNull": ["$info_form_puentecartografia.Asociacion", ""] }
            , "Lote No": { "$ifNull": ["$info_form_puentecartografia.Lote No", 0] }
            , "Productor": { "$ifNull": ["$info_form_puentecartografia.Productor", ""] }
            , "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }


            , "Yuca": { "$ifNull": ["$info_form_puentecartografia.Yuca", ""] }
        }
    }


    , {
        "$group": {
            "_id": {
                "tipo_data": "form_diagnosticokmz"
            }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$lookup": {
            "from": "form_diagnostico",
            "as": "data_form_diagnostico",
            "let": {

            },

            "pipeline": [

                {
                    "$match": {
                        "Campaña": { "$exists": true }
                        , "Nucleo": { "$exists": true }
                        , "Asociacion": { "$exists": true }
                        , "Lote No": { "$exists": true }
                    }
                },
                { "$sort": { "rgDate": -1 } },
                { "$addFields": { "tipo_formulario": "Sin KMZ" } },

                {
                    "$addFields": {
                        "nombre_maestro_principal": "Asociacion_"
                    }
                }

                , {
                    "$addFields": {
                        "num_letras_nombre_maestro_principal": {
                            "$strLenCP": "$nombre_maestro_principal"
                        }
                    }
                }



                , {
                    "$addFields": {
                        "valor_mestro_enlazado": {
                            "$filter": {
                                "input": {
                                    "$map": {
                                        "input": { "$objectToArray": "$$ROOT" },
                                        "as": "dataKV",
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [{
                                                        "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                    }, "$nombre_maestro_principal"]
                                                },
                                                "then": "$$dataKV.v",
                                                "else": ""
                                            }
                                        }
                                    }
                                },
                                "as": "item",
                                "cond": { "$ne": ["$$item", ""] }
                            }
                        }
                    }
                }
                , {
                    "$unwind": {
                        "path": "$valor_mestro_enlazado",
                        "preserveNullAndEmptyArrays": true
                    }
                }


                , {
                    "$addFields": {
                        "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                    }
                }

                , {
                    "$project": {
                        "nombre_maestro_principal": 0,
                        "num_letras_nombre_maestro_principal": 0
                    }
                }

                , {
                    "$addFields": {
                        "Productor": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                    }
                }

                , {
                    "$project": {
                        "valor_mestro_enlazado": 0

                        , "Point": 0
                        , "uid": 0
                        , "Formula": 0
                    }
                }


                , {
                    "$match": {
                        "Campaña": { "$ne": "" }
                        , "Nucleo": { "$ne": "" }
                        , "Asociacion": { "$ne": "" }
                        , "Lote No": { "$ne": "" }
                        , "Productor": { "$ne": "" }
                    }
                }




                , {
                    "$lookup": {
                        "from": "form_puentecartografia",
                        "as": "info_form_puentecartografia",
                        "let": {
                            "variable_puente_campana": "$Campaña"
                            , "variable_puente_nucleo": "$Nucleo"
                            , "variable_puente_asociacion": "$Asociacion"
                            , "variable_puente_lote": "$Lote No"
                            , "variable_puente_productor": "$Productor"
                        },
                        "pipeline": [

                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [


                                            { "$eq": ["$Campaña", "$$variable_puente_campana"] }
                                            , { "$eq": ["$Nucleo", "$$variable_puente_nucleo"] }
                                            , { "$eq": ["$Asociacion", "$$variable_puente_asociacion"] }
                                            , { "$eq": ["$Productor", "$$variable_puente_productor"] }
                                            , { "$eq": [{ "$toDouble": "$Lote No" }, { "$toDouble": "$$variable_puente_lote" }] }

                                        ]
                                    }
                                }
                            }
                        ]
                    }
                }

                , {
                    "$unwind": {
                        "path": "$info_form_puentecartografia",
                        "preserveNullAndEmptyArrays": true
                    }
                }

                , {
                    "$addFields": {
                        "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }

                    }
                }


            ]
        }
    }



    , {
        "$project": {

            "data_final": {
                "$concatArrays": [
                    "$data"
                    , "$data_form_diagnostico"
                ]
            }
        }
    }

    , { "$unwind": "$data_final" }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_final",
                    {
                    }
                ]
            }
        }
    }




    , {
        "$addFields": {
            "variable_puente_kmz": { "$ifNull": ["$variable_puente_kmz", ""] }
        }
    }

    , {
        "$project": {
            "info_form_puentecartografia": 0
            , "variable_cartografia": 0
        }
    }


    , {
            "$addFields": {
                "fecha_rgDate": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" }
                }
            }
        }


        , {
            "$project": {
                "supervisor": 1,
                "fecha_rgDate": 1,

                "tipo_formulario": 1,
                "variable_puente_kmz": 1,
                "Campaña": 1,
                "Nucleo": 1,
                "Asociacion": 1,
                "Lote No": 1,
                "Productor": 1,
                "Lote Bloque Kmz": 1,
                "Yuca": 1
            }
        }



]
