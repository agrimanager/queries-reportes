db.form_diagnosticokmz.aggregate(
    [
        //================================================
        //--->>data form_diagnosticokmz (con cartografia)
        //condiciones iniciales
        {
            "$match": {
                "Campaña": { "$exists": false }
            }
        },
        { "$sort": { "rgDate": -1 } },
        { "$addFields": { "tipo_formulario": "Con KMZ" } },

        //cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Bloque"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "variable_puente_kmz": "$variable_cartografia.features.properties.name"
            }
        }


        //----eliminar varibales
        , {
            "$addFields": {
                "Descripcion": "$Descrpcion"
            }
        }

        , {
            "$project": {
                "Point": 0
                , "uid": 0
                , "Formula": 0
                , "Bloque": 0

                , "Descrpcion": 0
            }
        }


        // //seleccionar ultimo registro
        // , {
        //     "$group": {
        //         "_id": {
        //             "variable_puente_kmz": "$variable_puente_kmz"

        //         }
        //         , "data": { "$push": "$$ROOT" }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "data": { "$arrayElemAt": ["$data", 0] }
        //     }
        // }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {}
        //             ]
        //         }
        //     }
        // }

        //---CRUZAR CON PUENTE form_puentecartografia
        , {
            "$lookup": {
                "from": "form_puentecartografia",
                "as": "info_form_puentecartografia",
                "let": {
                    "variable_puente_kmz": "$variable_puente_kmz"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Lote Bloque Kmz", "$$variable_puente_kmz"] }
                                ]
                            }
                        }
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$info_form_puentecartografia",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "Campaña": { "$ifNull": ["$info_form_puentecartografia.Campaña", ""] }
                , "Nucleo": { "$ifNull": ["$info_form_puentecartografia.Nucleo", ""] }
                , "Asociacion": { "$ifNull": ["$info_form_puentecartografia.Asociacion", ""] }
                , "Lote No": { "$ifNull": ["$info_form_puentecartografia.Lote No", 0] }
                , "Productor": { "$ifNull": ["$info_form_puentecartografia.Productor", ""] }
                , "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }
                // ,"Lote Bloque Kmz": { "$ifNull": ["$variable_puente_kmz", ""] }

                , "Yuca": { "$ifNull": ["$info_form_puentecartografia.Yuca", ""] }
            }
        }

        , {
            "$project": {
                "info_form_puentecartografia": 0
            }
        }

        //data_agrupacion
        , {
            "$group": {
                "_id": {
                    "tipo_data": "form_diagnosticokmz"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }


        //================================================
        //--->>data form_diagnostico (sin cartografia)
        , {
            "$lookup": {
                "from": "form_diagnostico",
                "as": "data_form_diagnostico",
                "let": {
                    // "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    // "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2

                    // , "user_timezone": "$user_timezone",         //--filtro_timezone
                },

                "pipeline": [
                    //query
                    //condiciones iniciales
                    {
                        "$match": {
                            "Campaña": { "$exists": true }
                            , "Nucleo": { "$exists": true }
                            , "Asociacion": { "$exists": true }
                            , "Lote No": { "$exists": true }
                        }
                    },
                    { "$sort": { "rgDate": -1 } },
                    { "$addFields": { "tipo_formulario": "Sin KMZ" } },

                    //maestro enlazado
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Asociacion_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0
                        }
                    }


                    //--JERARQUIA ESTRUCTURAL
                    // Campaña ---< Nucleo ---< Asociacion ---< Productor
                    //!!!NOTA:
                    //Maestro principal = Asociacion
                    //Maestros enlazados = Productor

                    , {
                        "$addFields": {
                            "Productor": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                        }
                    }

                    //----eliminar varibales
                    , {
                        "$project": {
                            "valor_mestro_enlazado": 0

                            , "Point": 0
                            , "uid": 0
                            , "Formula": 0
                        }
                    }


                    //condiciones
                    , {
                        "$match": {
                            "Campaña": { "$ne": "" }
                            , "Nucleo": { "$ne": "" }
                            , "Asociacion": { "$ne": "" }
                            , "Lote No": { "$ne": "" }
                            , "Productor": { "$ne": "" }
                        }
                    }


                    // //agrupacion
                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "variable_puente_campana": "$Campaña"
                    //             , "variable_puente_nucleo": "$Nucleo"
                    //             , "variable_puente_asociacion": "$Asociacion"
                    //             , "variable_puente_lote": "$Lote No"
                    //             , "variable_puente_productor": "$Productor"

                    //         }
                    //         , "data": { "$push": "$$ROOT" }
                    //     }
                    // }
                    // //seleccionar ultimo registro
                    // , {
                    //     "$addFields": {
                    //         "data": { "$arrayElemAt": ["$data", 0] }
                    //     }
                    // }

                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data",
                    //                 {}
                    //             ]
                    //         }
                    //     }
                    // }




                    //---CRUZAR CON PUENTE form_puentecartografia
                    , {
                        "$lookup": {
                            "from": "form_puentecartografia",
                            "as": "info_form_puentecartografia",
                            "let": {
                                "variable_puente_campana": "$Campaña"
                                , "variable_puente_nucleo": "$Nucleo"
                                , "variable_puente_asociacion": "$Asociacion"
                                , "variable_puente_lote": "$Lote No"
                                , "variable_puente_productor": "$Productor"
                            },
                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                /*
                                                    "Lote Bloque Kmz" : "21_JoseSalgado_2R_0,59Ha",
                                                    "Campaña" : "2021 - 2022",
                                                    "Nucleo" : "21 22 San Pedro",
                                                    "Asociacion" : "21 22 Independiente SP",
                                                    "Productor" : "Jose Luis Salgado Perez",
                                                    "CC NIT" : "92550616",
                                                    "Lote No" : 2,
                                                */

                                                { "$eq": ["$Campaña", "$$variable_puente_campana"] }
                                                , { "$eq": ["$Nucleo", "$$variable_puente_nucleo"] }
                                                , { "$eq": ["$Asociacion", "$$variable_puente_asociacion"] }
                                                , { "$eq": ["$Productor", "$$variable_puente_productor"] }
                                                , { "$eq": [{ "$toDouble": "$Lote No" }, { "$toDouble": "$$variable_puente_lote" }] }

                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$info_form_puentecartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , {
                        "$addFields": {
                            // "Campaña": { "$ifNull": ["$info_form_puentecartografia.Campaña", ""] }
                            // , "Nucleo": { "$ifNull": ["$info_form_puentecartografia.Nucleo", ""] }
                            // , "Asociacion": { "$ifNull": ["$info_form_puentecartografia.Asociacion", ""] }
                            // , "Lote No": { "$ifNull": ["$info_form_puentecartografia.Lote No", 0] }
                            // , "Productor": { "$ifNull": ["$info_form_puentecartografia.Productor", ""] }
                            "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }
                            // ,"Lote Bloque Kmz": { "$ifNull": ["$variable_puente_kmz", ""] }

                            // , "Yuca": { "$ifNull": ["$info_form_puentecartografia.Yuca", ""] }
                        }
                    }

                    // , {
                    //     "$project": {
                    //         "info_form_puentecartografia": 0
                    //     }
                    // }

                ]
            }
        }



        //=====UNIR DATOS
        , {
            "$project": {

                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data"
                        , "$data_form_diagnostico"
                    ]
                }

                // //----variables bases
                // , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
                // , "Busqueda fin": "$Busqueda fin"               //--filtro_fecha2
                // , "user_timezone": "$user_timezone",         //--filtro_timezone
            }
        }

        //desagregar datos
        , { "$unwind": "$data_final" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final",
                        {
                            // "rgDate": "$Busqueda inicio"
                        }
                    ]
                }
            }
        }



        //============================================================
        //---organizar data de formularios

        , {
            "$addFields": {
                "variable_puente_kmz": { "$ifNull": ["$variable_puente_kmz", ""] }
            }
        }

        , {
            "$project": {
                "info_form_puentecartografia": 0
                , "variable_cartografia": 0
            }
        }


        , {
            "$addFields": {
                "fecha_rgDate": {
                    "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" }
                }
            }
        }


        , {
            "$project": {
                "supervisor": 1,
                "fecha_rgDate": 1,

                "tipo_formulario": 1,
                "variable_puente_kmz": 1,
                "Campaña": 1,
                "Nucleo": 1,
                "Asociacion": 1,
                "Lote No": 1,
                "Productor": 1,
                "Lote Bloque Kmz": 1,
                "Yuca": 1,
            }
        }



    ]
)
    // .sort({ _id: -1 })
