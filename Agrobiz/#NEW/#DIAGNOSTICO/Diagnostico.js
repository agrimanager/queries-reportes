db.form_diagnostico.aggregate(
    [
        //condiciones iniciales
        {
            "$match": {
                "Campaña": { "$exists": true }
                , "Nucleo": { "$exists": true }
                , "Asociacion": { "$exists": true }
                , "Lote No": { "$exists": true }
            }
        },
        { "$sort": { "rgDate": -1 } },
        { "$addFields": { "tipo_formulario": "Sin KMZ" } },

        //maestro enlazado
        {
            "$addFields": {
                "nombre_maestro_principal": "Asociacion_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }



        , {
            "$addFields": {
                "valor_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , {
            "$unwind": {
                "path": "$valor_mestro_enlazado",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0
            }
        }


        //--JERARQUIA ESTRUCTURAL
        // Campaña ---< Nucleo ---< Asociacion ---< Productor
        //!!!NOTA:
        //Maestro principal = Asociacion
        //Maestros enlazados = Productor

        , {
            "$addFields": {
                "Productor": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        //----eliminar varibales
        , {
            "$project": {
                "valor_mestro_enlazado": 0

                , "Point": 0
                , "uid": 0
            }
        }


        //condiciones
        , {
            "$match": {
                "Campaña": { "$ne": "" }
                , "Nucleo": { "$ne": "" }
                , "Asociacion": { "$ne": "" }
                , "Lote No": { "$ne": "" }
                , "Productor": { "$ne": "" }
            }
        }


        //agrupacion
        , {
            "$group": {
                "_id": {
                    "variable_puente_campana": "$Campaña"
                    , "variable_puente_nucleo": "$Nucleo"
                    , "variable_puente_asociacion": "$Asociacion"
                    , "variable_puente_lote": "$Lote No"
                    , "variable_puente_productor": "$Productor"

                }
                , "data": { "$push": "$$ROOT" }
            }
        }
        //seleccionar ultimo registro
        , {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {}
                    ]
                }
            }
        }




        //---CRUZAR CON PUENTE form_puentecartografia
        , {
            "$lookup": {
                "from": "form_puentecartografia",
                "as": "info1_form_puentecartografia",
                "let": {
                    "variable_puente_campana": "$Campaña"
                    , "variable_puente_nucleo": "$Nucleo"
                    , "variable_puente_asociacion": "$Asociacion"
                    , "variable_puente_lote": "$Lote No"
                    , "variable_puente_productor": "$Productor"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    /*
                                    	"Lote Bloque Kmz" : "21_JoseSalgado_2R_0,59Ha",
                                    	"Campaña" : "2021 - 2022",
                                    	"Nucleo" : "21 22 San Pedro",
                                    	"Asociacion" : "21 22 Independiente SP",
                                    	"Productor" : "Jose Luis Salgado Perez",
                                    	"CC NIT" : "92550616",
                                    	"Lote No" : 2,
                                    */

                                    { "$eq": ["$Campaña", "$$variable_puente_campana"] }
                                    , { "$eq": ["$Nucleo", "$$variable_puente_nucleo"] }
                                    , { "$eq": ["$Asociacion", "$$variable_puente_asociacion"] }
                                    , { "$eq": ["$Productor", "$$variable_puente_productor"] }
                                    , { "$eq": [{ "$toDouble": "$Lote No" }, { "$toDouble": "$$variable_puente_lote" }] }

                                ]
                            }
                        }
                    }
                ]
            }
        }

        // , {
        //     "$unwind": {
        //         "path": "$info1_form_puentecartografia",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "num_arboles": { "$ifNull": ["$info_inventario_cultivo.Numero de Arboles", 0] }
        //     }
        // }

        // , {
        //     "$project": {
        //         "info_inventario_cultivo": 0
        //     }
        // }


    ]
)
    .sort({ _id: -1 })
