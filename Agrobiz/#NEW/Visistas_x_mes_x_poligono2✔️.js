[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_diagnosticokmz",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [


                {
                    "$match": {
                        "Campaña": { "$exists": false }
                    }
                },
                { "$sort": { "rgDate": -1 } },

                { "$addFields": { "formulario": "Diagnostico KMZ" } },


                {
                    "$addFields": {
                        "variable_cartografia": "$Bloque"
                    }
                },

                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "variable_puente_kmz": "$variable_cartografia.features.properties.name"
                    }
                }



                , {
                    "$addFields": {
                        "Descripcion": "$Descrpcion"
                    }
                }



                , {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca_point"
                    }
                },
                { "$unwind": "$finca_point" },
                { "$addFields": { "finca_point": { "$ifNull": ["$finca_point.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }

                , {
                    "$project": {
                        "Point": 0
                        , "uid": 0
                        , "Formula": 0
                        , "Bloque": 0

                        , "Descrpcion": 0
                    }
                }


                , {
                    "$lookup": {
                        "from": "form_puentecartografia",
                        "as": "info_form_puentecartografia",
                        "let": {
                            "variable_puente_kmz": "$variable_puente_kmz"
                        },
                        "pipeline": [

                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            { "$eq": ["$Lote Bloque Kmz", "$$variable_puente_kmz"] }
                                        ]
                                    }
                                }
                            }
                        ]
                    }
                }

                , {
                    "$unwind": {
                        "path": "$info_form_puentecartografia",
                        "preserveNullAndEmptyArrays": true
                    }
                }


                , {
                    "$addFields": {
                        "Campaña": { "$ifNull": ["$info_form_puentecartografia.Campaña", ""] }
                        , "Nucleo": { "$ifNull": ["$info_form_puentecartografia.Nucleo", ""] }
                        , "Asociacion": { "$ifNull": ["$info_form_puentecartografia.Asociacion", ""] }
                        , "Lote No": { "$ifNull": ["$info_form_puentecartografia.Lote No", 0] }
                        , "Productor": { "$ifNull": ["$info_form_puentecartografia.Productor", ""] }
                        , "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }
                        , "Yuca": { "$ifNull": ["$info_form_puentecartografia.Yuca", ""] }
                    }
                }

                , {
                    "$project": {
                        "info_form_puentecartografia": 0
                        , "variable_cartografia": 0
                    }
                }




                , {
                    "$group": {
                        "_id": {
                            "filtro_busqueda_inicio": "$$filtro_fecha_inicio"
                            , "filtro_busqueda_fin": "$$filtro_fecha_fin"
                        }
                        , "data1": { "$push": "$$ROOT" }
                    }
                }


                , {
                    "$lookup": {
                        "from": "form_supervision",
                        "as": "data2",
                        "let": {
                            "filtro_finca": "$_id.filtro_finca",
                            "filtro_fecha_inicio": "$_id.filtro_busqueda_inicio",
                            "filtro_fecha_fin": "$_id.filtro_busqueda_fin"
                        },

                        "pipeline": [

                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [

                                            {
                                                "$gte": [
                                                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                                ]
                                            },
                                            {
                                                "$lte": [
                                                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                                ]
                                            }
                                        ]
                                    }
                                }
                            },



                            { "$sort": { "rgDate": -1 } },

                            { "$addFields": { "formulario": "Supervision" } },


                            {
                                "$addFields": {
                                    "variable_cartografia": "$Bloque"
                                }
                            },

                            { "$unwind": "$variable_cartografia.features" },

                            {
                                "$addFields": {
                                    "variable_puente_kmz": "$variable_cartografia.features.properties.name"
                                }
                            }

                            , {
                                "$addFields": {
                                    "point_farm_oid": { "$toObjectId": "$Point.farm" }
                                }
                            },
                            {
                                "$lookup": {
                                    "from": "farms",
                                    "localField": "point_farm_oid",
                                    "foreignField": "_id",
                                    "as": "finca_point"
                                }
                            },
                            { "$unwind": "$finca_point" },
                            { "$addFields": { "finca_point": { "$ifNull": ["$finca_point.name", "no existe"] } } },

                            {
                                "$project": {
                                    "Point": 0
                                    , "point_farm_oid": 0
                                    , "uid": 0
                                }
                            }



                            , {
                                "$project": {
                                    "Point": 0
                                    , "uid": 0
                                    , "Formula": 0
                                    , "Bloque": 0
                                }
                            }


                            , {
                                "$lookup": {
                                    "from": "form_puentecartografia",
                                    "as": "info_form_puentecartografia",
                                    "let": {
                                        "variable_puente_kmz": "$variable_puente_kmz"
                                    },
                                    "pipeline": [

                                        {
                                            "$match": {
                                                "$expr": {
                                                    "$and": [
                                                        { "$eq": ["$Lote Bloque Kmz", "$$variable_puente_kmz"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ]
                                }
                            }

                            , {
                                "$unwind": {
                                    "path": "$info_form_puentecartografia",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }


                            , {
                                "$addFields": {
                                    "Campaña": { "$ifNull": ["$info_form_puentecartografia.Campaña", ""] }
                                    , "Nucleo": { "$ifNull": ["$info_form_puentecartografia.Nucleo", ""] }
                                    , "Asociacion": { "$ifNull": ["$info_form_puentecartografia.Asociacion", ""] }
                                    , "Lote No": { "$ifNull": ["$info_form_puentecartografia.Lote No", 0] }
                                    , "Productor": { "$ifNull": ["$info_form_puentecartografia.Productor", ""] }
                                    , "Lote Bloque Kmz": { "$ifNull": ["$info_form_puentecartografia.Lote Bloque Kmz", ""] }
                                    , "Yuca": { "$ifNull": ["$info_form_puentecartografia.Yuca", ""] }
                                }
                            }

                            , {
                                "$project": {
                                    "info_form_puentecartografia": 0
                                    , "variable_cartografia": 0
                                }
                            }


                        ]
                    }
                }




                , {
                    "$project": {


                        "data_final": {
                            "$concatArrays": [
                                "$data1"
                                , "$data2"
                            ]
                        }
                    }
                }


                , { "$unwind": "$data_final" }

                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data_final",
                                {}
                            ]
                        }
                    }
                }



                , {
                    "$project": {
                        "supervisor": 1,
                        "rgDate": 1,

                        "formulario": 1,
                        "variable_puente_kmz": 1,

                        "Campaña": 1,
                        "Nucleo": 1,
                        "Asociacion": 1,
                        "Lote No": 1,
                        "Productor": 1,
                        "Lote Bloque Kmz": 1,
                        "Yuca": 1
                        , "finca_point": 1
                    }
                }



                , { "$addFields": { "variable_fecha": "$rgDate" } }
                , {
                    "$addFields": {
                        "num_anio": { "$year": { "date": "$variable_fecha" } },
                        "num_mes": { "$month": { "date": "$variable_fecha" } },
                        "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } }
                    }
                }

                , {
                    "$addFields": {
                        "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } }

                        , "Mes_Txt": {
                            "$switch": {
                                "branches": [
                                    { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                                    { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                                    { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                                    { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                                    { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                                    { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                                    { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                                    { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                                    { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                                    { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                                    { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                                    { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                                ],
                                "default": "Mes desconocido"
                            }
                        }

                    }
                },

                { "$project": { "variable_fecha": 0 } }




            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    , {
        "$group": {
            "_id": {
                "supervisor": "$supervisor"
                , "variable_puente_kmz": "$variable_puente_kmz"
                , "fecha_txt": "$Fecha_Txt"
            }
            , "data": { "$first": "$$ROOT" }
        }
    }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {}
                ]
            }
        }
    }




]
