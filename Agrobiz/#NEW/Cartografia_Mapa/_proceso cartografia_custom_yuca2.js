

// //(1)==== Agregar custom_properties en fincas

// //agregar propiedades a farms
// var fincas = db.farms.find({});

// fincas.forEach((item_finca) => {

//     db.farms.update(
//         { _id: item_finca._id },
//         {
//             $push: {
//                 //---blocks
//                 "defaultProperties.blocks": {
//                     $each: [
//                         // {
//                         //     "name": "Yuca",
//                         //     "type": "string"
//                         // },
//                         {
//                             "name": "Yuca Metro",
//                             "type": "bool",
//                             "color": "#008000"
//                         },
//                         {
//                             "name": "Yuca Regular",
//                             "type": "bool",
//                             "color": "#0000FF"
//                         }
//                     ]
//                 }
//             }
//         }
//     )
// });



//(2)==== Buscar propiedades en puente


//----query cartografia y cruce con puente
var cursor = db.cartography.aggregate(
    {
        $match: {
            type: "Feature"
        }
    }

    , {
        "$lookup": {
            "from": "form_puentecartografia",
            "as": "info_form_puentecartografia",
            "let": {
                "variable_puente_kmz": "$properties.name"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Lote Bloque Kmz", "$$variable_puente_kmz"] }
                            ]
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_form_puentecartografia",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        $addFields: {
            tipo_yuca: { $ifNull: ["$info_form_puentecartografia.Yuca", "sin tipo"] }
        }
    }

    , {
        $addFields: {
            is_tipo_regular: {
                $switch: {
                    branches: [
                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$tipo_yuca", "Regular"] }
                                ]
                            }
                            , "then": true
                        }
                    ],
                    default: false
                }
            }
            , is_tipo_metro: {
                $switch: {
                    branches: [
                        {
                            "case": {
                                "$and": [
                                    { "$eq": ["$tipo_yuca", "Metro"] }
                                ]
                            }
                            , "then": true
                        }
                    ],
                    default: false
                }
            }
        }
    }



    //obtener solo los que estan en el puente
    , {
        $match: {
            tipo_yuca: { $ne: "sin tipo" }
        }
    }


    //obtener solo los que falta por actualizar
    , {
        $match: {
            "properties.custom": { $eq: {} }
        }
    }

);


// cursor



//(3)==== Actualizar propiedades de cartografia
cursor.forEach(i => {

    db.cartography.update(
        {
            _id: i._id
        },
        {
            $set: {
                "properties.custom.Yuca": {
                    "type": "string",
                    "value": i.tipo_yuca
                }
                ,"properties.custom.Yuca Metro": {
                    "type": "bool",
                    "value": i.is_tipo_metro
                }
                ,"properties.custom.Yuca Regular": {
                    "type": "bool",
                    "value": i.is_tipo_regular
                }
            }
        }
    )

})
