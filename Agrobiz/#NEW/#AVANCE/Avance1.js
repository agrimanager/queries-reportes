db.form_avance.aggregate(
    [


        //--condicion test
        {
            "$match": {
                "Asociacion": { "$exists": true }
            }
        },



        //======MAESTRO ENLAZADO
        {
            "$project": {
                "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },


        {
            "$addFields": {
                "nombre_maestro_principal": "Asociacion_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }



        , {
            "$addFields": {
                "valor_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , {
            "$unwind": {
                "path": "$valor_mestro_enlazado",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0
            }
        }


        //--JERARQUIA ESTRUCTURAL
        // Campaña ---< Nucleo ---< Asociacion ---< Productor
        //!!!NOTA:
        //Maestro principal = Asociacion
        //Maestros enlazados = Productor

        , {
            "$addFields": {
                "Productor": { "$ifNull": ["$valor_mestro_enlazado", "sin datos"] }
            }
        }

        //----eliminar varibales
        , {
            "$project": {
                "valor_mestro_enlazado": 0

                , "Point": 0
                , "uid": 0
            }
        }




        //======TIPOS DE VARIABLES

        //====== {A} Variables Cualiitativas
        //--bool (===>> 0)

        //--date (===>> 8)

        //texto (4)
        //master_simple (2)
        //selector_simple (3)
        //array (1)
        //--string (TOTAL = 4+2+3+1 ===>> 10)
        //COMO MOSTRAR VARIABLES LA 1RA o la ULTIMA??????-------------------------->>>pregunta


        //====== {B} Variables Cuantitativas
        //--number (===>> 32)
        //QUE VARIABLES SE DEBEN IR ACUMULANDO??????-------------------------->>>pregunta



        //---AGRUPACIONES




    ]
)
