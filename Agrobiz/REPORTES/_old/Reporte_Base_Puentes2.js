db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //----🕙 (FILTRO FECHAS)
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }
                    , { "$unwind": "$user_timezone" }
                    , { "$addFields": { "user_timezone": "$user_timezone.timezone" } }

                    , {
                        "$addFields": {
                            "filtro_fecha": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }


                    //.....⚡ 

                    //--->data cruces
                    //1)Data Labores
                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    }
                    , { "$unwind": "$activity" }
                    , { "$addFields": { "nombre_actividad": "$activity.name" } }


                    //2)Data Cartografia
                    , {
                        "$unwind": {
                            "path": "$cartography.features",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , { "$addFields": { "nombre_bloque": { "$ifNull": ["$cartography.features.properties.name", "-----"] } } }



                    //--->cruces

                    //1)Puente Labores
                    , {
                        "$lookup": {
                            "from": "form_puentelabores",
                            "localField": "nombre_actividad",
                            "foreignField": "Actividad",
                            "as": "puentelabores"
                        }
                    }
                    , { "$unwind": "$puentelabores" }
                    , { "$addFields": { "Labor Agrobiz": "$puentelabores.Labor Agrobiz" } }

                    //2)Puente Cartografia
                    , {
                        "$lookup": {
                            "from": "form_puentecartografia",
                            "localField": "nombre_bloque",
                            "foreignField": "Bloque",
                            "as": "puentecartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$puentecartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    
                    , { "$addFields": { "empleado_identificacion": { "$ifNull": ["$puentecartografia.Empleado Identificacion", "-----"] } } }
                    
                    //  "Bloque" : "Asoyusab_YR_IsidroHernadez1",
                    //  "Empresa" : "ASOYUSAB",
                    //  "Empleado Identificacion" : "92186490",//------------->>EMPLEADO
                    //  "Hectareas Contratadas" : 2,
                    //  "Hectareas Disponibles" : 1.8,
                    //  "Tipo Yuca" : "Yuca Regular",
                    //  "Tipo Predio" : "Arriendo",
                    //  "Departamento" : "Sucre",
                    //  "Municipio" : "San Pedro",
                    //  "Vereda" : "San Mateo",
                    //  "Finca Bloque" : "Rabacan",
                    
                    
                    //2.1)Puente Cartografia -- EMPLEADO
                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_identificacion",
                            "foreignField": "numberID",
                            "as": "info_empleados"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleados",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    
                    //, { "$addFields": { "empleado_identificacion": { "$ifNull": ["$puentecartografia.Empleado Identificacion", "-----"] } } }
                    //cedula : numberID
                    //telefono : cellphone
                    //cantidad_lotes : AGRUPACION(x)



                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$filtro_fecha" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data_labores", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)