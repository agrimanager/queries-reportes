db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //----🕙 (FILTRO FECHAS)
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }
                    , { "$unwind": "$user_timezone" }
                    , { "$addFields": { "user_timezone": "$user_timezone.timezone" } }

                    , {
                        "$addFields": {
                            "filtro_fecha": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }


                    //.....⚡ 


                    //(CONDICION BASE)
                    //---labores finalizadas
                    , {
                        "$match": {
                            "status": "Done"
                        }
                    }


                    //---REGLAS DE NEGOCIO
                    , {
                        "$addFields": {
                            "num_bloques": { "$size": "$cartography.features" }
                        }
                    }
                    , {
                        "$addFields": {
                            "num_empleados": {
                                "$cond": {
                                    "if": { "$eq": ["$productivityReport", null] },
                                    "then": 0,
                                    "else": { "$size": "$productivityReport" }
                                }
                            }
                        }
                    }

                    //---casos PARA FILTRAR

                    , {
                        "$addFields": {
                            "caso": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$eq": ["$num_bloques", 0] }
                                                    , { "$eq": ["$num_empleados", 0] }
                                                ]
                                                //}, "then": "caso_borrar_1"
                                            }, "then": "caso_si_borrar"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$num_bloques", 1] }
                                                    , { "$gt": ["$num_empleados", 1] }
                                                ]
                                                // }, "then": "caso_borrar_2"
                                            }, "then": "caso_si_borrar"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$eq": ["$num_bloques", 0] }
                                                    , { "$gt": ["$num_empleados", 1] }
                                                ]
                                                //}, "then": "caso_borrar_3"
                                            }, "then": "caso_si_borrar"
                                        },
                                    ],
                                    "default": "caso_no_borrar"
                                }
                            }
                        }
                    }

                    , {
                        "$match": {
                            "caso": "caso_no_borrar"
                        }
                    }



                    //-----DATOS PARA PUENTES

                    //----NOTA: ya estan filtros los casos para que esto se cumpla BIEN

                    //nombre_bloque
                    , {
                        "$addFields": {
                            "nombre_bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$num_bloques", 1] },
                                    "then": "$cartography.features.properties.name",
                                    "else": []
                                }
                            }
                        }
                    }

                    //empleado_identificacion
                    , {
                        "$addFields": {
                            "empleado_identificacion_oid": {
                                "$cond": {
                                    //"if": { "$eq": ["$num_empleados", 1] },
                                    "if": {
                                        "$and": [
                                            { "$ne": ["$num_bloques", 1] },
                                            { "$eq": ["$num_empleados", 1] }
                                        ]
                                    },
                                    "then": "$productivityReport.employee",
                                    "else": []
                                }
                            }
                        }
                    }

                    //1)===================== EMPLEADO

                    //---cruce1 con empelado
                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_identificacion_oid",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    , { "$addFields": { "empleado_identificacion_1": { "$ifNull": ["$info_empleado.numberID", "___"] } } }
                    , { "$project": { "info_empleado": 0 } }

                    , {
                        "$lookup": {
                            "from": "form_puentecartografia",
                            "localField": "nombre_bloque",
                            "foreignField": "Bloque",
                            "as": "data_puentecartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$data_puentecartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , { "$addFields": { "empleado_identificacion_2": { "$ifNull": ["$data_puentecartografia.Empleado Identificacion", "___"] } } }
                    , { "$project": { "data_puentecartografia": 0 } }


                    //empleado_identificacion
                    , {
                        "$addFields": {
                            "empleado_identificacion": {
                                "$cond": {
                                    "if": { "$ne": ["$empleado_identificacion_1", "___"] },
                                    "then": "$empleado_identificacion_1",
                                    "else": "$empleado_identificacion_2"
                                }
                            }
                        }
                    }
                    , { "$project": { "empleado_identificacion_1": 0 } }
                    , { "$project": { "empleado_identificacion_2": 0 } }


                    , {
                        "$match": {
                            "empleado_identificacion": { "$ne": "___" }
                        }
                    }

                    //---cruce2 con empelado
                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_identificacion",
                            "foreignField": "numberID",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    // , { "$addFields": { "empleado_identificacion": { "$ifNull": ["$puentecartografia.Empleado Identificacion", "-----"] } } }
                    // //cedula : numberID
                    // //telefono : cellphone
                    // //cantidad_lotes : AGRUPACION(x)



                    //-----SUPERVISOR
                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "supervisor",
                            "foreignField": "_id",
                            "as": "info_supervisor"
                        }
                    }
                    , { "$unwind": "$info_supervisor" }
                    , { "$addFields": { "supervisor_identificacion": { "$ifNull": ["$info_supervisor.numberID", "___"] } } }
                    , { "$addFields": { "supervisor_nombre": { "$ifNull": [{ "$concat": ["$info_supervisor.firstName", " ", "$info_supervisor.lastName"] }, "___"] } } }



                    //2)===================== PUENTE LABORES
                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    }
                    , { "$unwind": "$activity" }
                    , { "$addFields": { "nombre_actividad": "$activity.name" } }

                    , {
                        "$lookup": {
                            "from": "form_puentelabores",
                            "localField": "nombre_actividad",
                            "foreignField": "Actividad",
                            "as": "data_puentelabores"
                        }
                    }
                    , { "$unwind": "$data_puentelabores" }
                    , { "$addFields": { "Labor Agrobiz": "$data_puentelabores.Labor Agrobiz" } }
                    , { "$project": { "data_puentelabores": 0 } }


                    //----CANTIDADES
                    //,{ "$addFields": { "TotalPagoEmpleado": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$productivityAchieved" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] } } }, { "$addFields": { "TOTAL LABOR": { "$sum": [{ "$ifNull": [{ "$toDouble": "$TotalPagoEmpleado" }, 0] }, { "$ifNull": [{ "$toDouble": "$totalSupplies" }, 0] }] } } }
                    , {
                        "$addFields": {
                            "labor_unidad_medida": { "$ifNull": ["$productivityPrice.measure", "SIN UNIDAD"] },
                            "labor_precio": "$productivityPrice.price",
                            "labor_num_productividad": { "$ifNull": [{ "$toDouble": "$productivityAchieved" }, 0] },
                            "labor_costo_mo": { "$multiply": [{ "$ifNull": [{ "$toDouble": "$productivityAchieved" }, 0] }, { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }] },
                            "labor_costo_inv": { "$ifNull": [{ "$toDouble": "$totalSupplies" }, 0] }
                        }
                    }

                    , {
                        "$addFields": {
                            "labor_costo_TOTAL": { "$sum": [{ "$ifNull": [{ "$toDouble": "$labor_costo_mo" }, 0] }, { "$ifNull": [{ "$toDouble": "$labor_costo_inv" }, 0] }] }
                        }
                    }




                    //3)===================== PUENTE CARTOGRAFIA


                    , {
                        "$group": {
                            "_id": {
                                "supervisor_identificacion": "$supervisor_identificacion",
                                "empleado_identificacion": "$empleado_identificacion"
                            }
                            , "data": { "$push": "$$ROOT" }
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_puentecartografia",
                            "localField": "_id.empleado_identificacion",
                            "foreignField": "Empleado Identificacion",
                            "as": "data_puentecartografia"
                        }
                    }
                    
                    , {
                        "$addFields": {
                            "empleado_asociacion": {"$arrayElemAt": ["$data_puentecartografia.Empresa", 0 ]}
                            ,"tipo_yuca": {"$arrayElemAt": ["$data_puentecartografia.Tipo Yuca", 0 ]}
                        }
                    }

                    , {
                        "$addFields": {
                            "cantidad_bloques": { "$size": "$data_puentecartografia" }
                            ,"sum_ha_contratadas": { "$sum": { "$ifNull": ["$data_puentecartografia.Hectareas Contratadas", 0] } }
                            ,"sum_ha_disponibles": { "$sum": { "$ifNull": ["$data_puentecartografia.Hectareas Disponibles", 0] } }
                        }
                    }
                    
                    , { "$project": { "data_puentecartografia": 0 } }


                    // //  "Bloque" : "Asoyusab_YR_IsidroHernadez1",
                    // //-----------o  "Empresa" : "ASOYUSAB",
                    // //  "Empleado Identificacion" : "92186490",//------------->>EMPLEADO
                    // //-----------<  "Hectareas Contratadas" : 2,
                    // //-----------<  "Hectareas Disponibles" : 1.8,
                    // //-----------o  "Tipo Yuca" : "Yuca Regular",
                    // //  "Tipo Predio" : "Arriendo",
                    // //  "Departamento" : "Sucre",
                    // //  "Municipio" : "San Pedro",
                    // //  "Vereda" : "San Mateo",
                    // //  "Finca Bloque" : "Rabacan",






                    //--->operaciones
                    //...cantidades / Ha





                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$filtro_fecha" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data_labores", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //         From Sarah Alzate to Everyone:  03:42 PM
        // Caso 1: No seleccionan lotes, se busca la cedula del empleado y la pdtividad se divide linealmente en todos las áreas del empleado

        // Caso 2: Seleccionan un sólo lote, se hace el comparativo a ese elemento de la carografía

        // Caso 3: Seleccionan varios lotes, se dividen las ha de la productividad de manera líneal en las ha del productor

        // NOTA: Sólo seleccionar lotes de 1 productor y sólo seleccionar 1 empleado



    ]
)