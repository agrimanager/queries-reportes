db.users.aggregate(
    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----🚩 (REPORTE SIN FORMULARIO)
        { "$limit": 1 },

        {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio"
                    , "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    //----🕙 (FILTRO FECHAS)
                    {
                        "$lookup": {
                            "from": "users",
                            "localField": "uid",
                            "foreignField": "_id",
                            "as": "user_timezone"
                        }
                    }
                    , { "$unwind": "$user_timezone" }
                    , { "$addFields": { "user_timezone": "$user_timezone.timezone" } }

                    , {
                        "$addFields": {
                            "filtro_fecha": { "$arrayElemAt": ["$when.start", { "$subtract": [{ "$size": "$when.start" }, 1] }] }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "$user_timezone" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha", "timezone": "$user_timezone" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "$user_timezone" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }


                    //.....⚡ 


                    //(CONDICION BASE)
                    //---labores finalizadas
                    , {
                        "$match": {
                            "status": "Done"
                        }
                    }


                    //---REGLAS DE NEGOCIO
                    , {
                        "$addFields": {
                            "num_bloques": { "$size": "$cartography.features" }
                        }
                    }
                    , {
                        "$addFields": {
                            "num_empleados": {
                                "$cond": {
                                    "if": { "$eq": ["$productivityReport", null] },
                                    "then": 0,
                                    "else": { "$size": "$productivityReport" }
                                }
                            }
                        }
                    }

                    //---casos PARA FILTRAR

                    , {
                        "$addFields": {
                            "caso": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$eq": ["$num_bloques", 0] }
                                                    , { "$eq": ["$num_empleados", 0] }
                                                ]
                                                //}, "then": "caso_borrar_1"
                                            }, "then": "caso_si_borrar"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$num_bloques", 1] }
                                                    , { "$gt": ["$num_empleados", 1] }
                                                ]
                                                // }, "then": "caso_borrar_2"
                                            }, "then": "caso_si_borrar"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$eq": ["$num_bloques", 0] }
                                                    , { "$gt": ["$num_empleados", 1] }
                                                ]
                                                //}, "then": "caso_borrar_3"
                                            }, "then": "caso_si_borrar"
                                        },
                                    ],
                                    "default": "caso_no_borrar"
                                }
                            }
                        }
                    }

                    , {
                        "$match": {
                            "caso": "caso_no_borrar"
                        }
                    }



                    //-----DATOS PARA PUENTES

                    //----NOTA: ya estan filtros los casos para que esto se cumpla BIEN

                    //nombre_bloque
                    , {
                        "$addFields": {
                            "nombre_bloque": {
                                "$cond": {
                                    "if": { "$eq": ["$num_bloques", 1] },
                                    "then": "$cartography.features.properties.name",
                                    "else": []
                                }
                            }
                        }
                    }

                    //empleado_identificacion
                    , {
                        "$addFields": {
                            "empleado_identificacion_oid": {
                                "$cond": {
                                    //"if": { "$eq": ["$num_empleados", 1] },
                                    "if": {
                                        "$and": [
                                            { "$ne": ["$num_bloques", 1] },
                                            { "$eq": ["$num_empleados", 1] }
                                        ]
                                    },
                                    "then": "$productivityReport.employee",
                                    "else": []
                                }
                            }
                        }
                    }



                    //---cruce1 con empelado
                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_identificacion_oid",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    , { "$addFields": { "empleado_identificacion_1": { "$ifNull": ["$info_empleado.numberID", "___"] } } }
                    , { "$project": { "info_empleado": 0 } }


                    // //1)Puente Cartografia
                    , {
                        "$lookup": {
                            "from": "form_puentecartografia",
                            "localField": "nombre_bloque",
                            "foreignField": "Bloque",
                            "as": "data_puentecartografia"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$data_puentecartografia",
                            "preserveNullAndEmptyArrays": true
                        }
                    }

                    , { "$addFields": { "empleado_identificacion_2": { "$ifNull": ["$data_puentecartografia.Empleado Identificacion", "___"] } } }
                    , { "$project": { "data_puentecartografia": 0 } }


                    //empleado_identificacion
                    , {
                        "$addFields": {
                            "empleado_identificacion": {
                                "$cond": {
                                    "if": { "$ne": ["$empleado_identificacion_1", "___"] },
                                    "then": "$empleado_identificacion_1",
                                    "else": "$empleado_identificacion_2"
                                }
                            }
                        }
                    }
                    , { "$project": { "empleado_identificacion_1": 0 } }
                    , { "$project": { "empleado_identificacion_2": 0 } }


                    , {
                        "$match": {
                            "empleado_identificacion": { "$ne": "___" }
                        }
                    }



                    // //  "Bloque" : "Asoyusab_YR_IsidroHernadez1",
                    // //  "Empresa" : "ASOYUSAB",
                    // //  "Empleado Identificacion" : "92186490",//------------->>EMPLEADO
                    // //  "Hectareas Contratadas" : 2,
                    // //  "Hectareas Disponibles" : 1.8,
                    // //  "Tipo Yuca" : "Yuca Regular",
                    // //  "Tipo Predio" : "Arriendo",
                    // //  "Departamento" : "Sucre",
                    // //  "Municipio" : "San Pedro",
                    // //  "Vereda" : "San Mateo",
                    // //  "Finca Bloque" : "Rabacan",


                    // //2.1)Puente Cartografia -- EMPLEADO
                    // , {
                    //     "$lookup": {
                    //         "from": "employees",
                    //         "localField": "empleado_identificacion",
                    //         "foreignField": "numberID",
                    //         "as": "info_empleados"
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$info_empleados",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , { "$addFields": { "empleado_identificacion": { "$ifNull": ["$puentecartografia.Empleado Identificacion", "-----"] } } }
                    // //cedula : numberID
                    // //telefono : cellphone
                    // //cantidad_lotes : AGRUPACION(x)



                    //2)Puente Labores
                    , {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    }
                    , { "$unwind": "$activity" }
                    , { "$addFields": { "nombre_actividad": "$activity.name" } }

                    , {
                        "$lookup": {
                            "from": "form_puentelabores",
                            "localField": "nombre_actividad",
                            "foreignField": "Actividad",
                            "as": "data_puentelabores"
                        }
                    }
                    , { "$unwind": "$data_puentelabores" }
                    , { "$addFields": { "Labor Agrobiz": "$data_puentelabores.Labor Agrobiz" } }
                    , { "$project": { "data_puentelabores": 0 } }



                    //--->operaciones
                    //...cantidades / Ha



                    //----🚩 (REPORTE SIN FORMULARIO)
                    , { "$addFields": { "rgDate": "$filtro_fecha" } }

                ]

            }
        }


        , {
            "$project": {
                "datos": {
                    "$concatArrays": ["$data_labores", []]
                }
            }
        }
        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //         From Sarah Alzate to Everyone:  03:42 PM
        // Caso 1: No seleccionan lotes, se busca la cedula del empleado y la pdtividad se divide linealmente en todos las áreas del empleado

        // Caso 2: Seleccionan un sólo lote, se hace el comparativo a ese elemento de la carografía

        // Caso 3: Seleccionan varios lotes, se dividen las ha de la productividad de manera líneal en las ha del productor

        // NOTA: Sólo seleccionar lotes de 1 productor y sólo seleccionar 1 empleado



    ]
)