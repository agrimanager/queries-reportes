
var cursor = db.activities.aggregate(
    {
        $match: {
            name: {
                $in: [
                    "Transporte interno",
                    "Montaje punto inicial de riego",
                    "Construcción de fuentes de agua",
                    "Construcción y/o adecuación de bodega",
                    "Transporte externo",
                    "Cargue y descargue",
                    "Cosecha manual",
                    "Almacenamiento de semilla",
                    "Selección de semilla",
                    "Control Preemergente de Malezas",
                    "Control químico de enfermedades",
                    "Control químico de plagas",
                    "Control manual de plagas",
                    "Control químico de malezas",
                    "Control manual de malezas",
                    "Deschupone",
                    "Celaduría",
                    "Aplicación de riego",
                    "Instalación de riego",
                    "Canales de drenajes",
                    "Mantenimiento de cercas",
                    "Fertilización foliar",
                    "Fertilización edáfica",
                    "Transporte interno de fertilizantes",
                    "Entrega de insumos",
                    "Resiembra",
                    "Siembra",
                    "Picado de semilla",
                    "Tratamiento de semilla",
                    "Transporte de semilla",
                    "Compra de semilla",
                    "Surcado",
                    "Rastra",
                    "Arada",
                    "Cortamalezas",

                ]
            }
        }
    }

);
//cursor

cursor.forEach(i => {

    var item_actual = i.name
    //var item_nuevo = item_actual.replace('.', '')
    var item_nuevo = item_actual.concat ("-Yesica Yulieth Arrieta Cardeño")
    
    
    //console.log(item_nuevo)

    db.activities.update(
        {
            _id: i._id
        },
        {
            $set: {
                "name": item_nuevo
            }
        }
    )

});

//-----OJO TIRAR VARIAS VECES  PORQUE (replaceAll) no funciona