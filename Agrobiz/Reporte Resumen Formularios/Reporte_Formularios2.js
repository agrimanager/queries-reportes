db.users.aggregate([


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2020-10-08T06:00:00.000-05:00"),
            "Busqueda fin": new Date,
            "today": new Date
        }
    },
    //----------------------------------------------------------------

    { "$limit": 1 }

    //--form1 (form_controldemalezas)
    , {
        "$lookup": {
            "from": "form_controldemalezas",
            "as": "data1",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
            },
            "pipeline": [

                //----variables
                {
                    "$project": {
                        "formulario": "form1",//--editar
                        "supervisor": "$supervisor",
                        "rgDate": "$rgDate",
                        // fecha
                        // hora

                        "farm": "$Point.farm"
                        // *finca
                        // *lote
                    }
                },

                //---filtro fechas
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }



    //--form2 (form_registrodevisitas)
    , {
        "$lookup": {
            "from": "form_registrodevisitas",
            "as": "data2",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
            },
            "pipeline": [

                //----variables
                {
                    "$project": {
                        "formulario": "form2",//--editar
                        "supervisor": "$supervisor",
                        "rgDate": "$rgDate",
                        // fecha
                        // hora

                        "farm": "$Point.farm"
                        // *finca
                        // *lote
                    }
                },

                //---filtro fechas
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }
            ]
        }
    }





    //.... al final concatenar arrays
    , {
        "$project": {
            "datos": {
                "$concatArrays": [
                    "$data1"
                    , "$data2"

                    //data3
                    //.....
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


])
