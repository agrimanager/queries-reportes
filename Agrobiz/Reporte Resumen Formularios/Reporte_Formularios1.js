db.users.aggregate([


    //------------------------------------------------------------------
    //---filtros de fechas
    {
        $addFields: {
            "Busqueda inicio": ISODate("2020-10-08T06:00:00.000-05:00"),
            "Busqueda fin": new Date,
            "today": new Date
        }
    },
    //----------------------------------------------------------------

    { "$limit": 1 },

    //--form1
    {
        "$lookup": {
            //"from": "XXXXXXXXXX___FORM",
            "from": "form_controldemalezas",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
            },
            "pipeline": [

                //----variables
                {
                    "$project": {
                        "formulario": "form1",//--editar
                        "supervisor": "$supervisor",
                        "rgDate": "$rgDate",
                        // fecha
                        // hora

                        "farm": "$Point.farm"
                        // *finca
                        // *lote
                    }
                },

                //---filtro fechas
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "Busqueda inicio": "$$filtro_fecha_inicio",
                        "Busqueda fin": "$$filtro_fecha_fin"
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


])
