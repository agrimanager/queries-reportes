db.form_cosecha.aggregate(
    [
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        // //----FILTRO FECHAS Y FINCA
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [

        //                 {
        //                     "$gte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
        //                     ]
        //                 },
        //                 {
        //                     "$lte": [
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                         { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
        //                     ]
        //                 }
        //                 // , {
        //                 //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
        //                 // }
        //             ]
        //         }
        //     }
        // },
        // //----------------------------------------------------------------
        // //....query reporte



        {
            "$addFields": {
                "busqueda_inicio": {
                    "$subtract": ["$Busqueda fin", 1209600000]
                }
            }
        },

        {
            "$match": {
                "$expr": {
                    "$gte": ["$rgDate", "$busqueda_inicio"]
                }
            }
        },




        {
            "$addFields": {
                "variable_cartografia": "$Programa "
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },


        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "programa": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$programa",
                "preserveNullAndEmptyArrays": true
            }
        },


        { "$addFields": { "plantas_x_programa": { "$ifNull": ["$programa.properties.custom.Plantas.value", 0] } } },

        { "$addFields": { "programa": { "$ifNull": ["$programa.properties.name", "no existe"] } } },



        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Programa ": 0
                , "uid": 0
                , "Formula": 0


            }
        }



        , {
            "$match": {
                "N Tallos ": { "$gt": 0 }
            }
        }



        , {
            "$group": {
                "_id": "$programa",
                "rgDate": { "$max": "$rgDate" }
            }
        }

        , {
            "$addFields": {
                "inicio_ciclo": {
                    "$subtract": [
                        "$rgDate",
                        7257600000
                    ]
                }
            }
        }



        , {
            "$lookup": {
                "from": "form_cosecha",
                "let": {
                    "busquedad_inicio": "$inicio_ciclo",
                    "busquedad_fin": "$rgDate",
                    "programa": "$_id"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$busquedad_inicio"] },
                                    { "$lte": ["$rgDate", "$$busquedad_fin"] }
                                ]
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "variable_cartografia": "$Programa "
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },



                    {
                        "$addFields": {
                            "programa": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$programa",
                            "preserveNullAndEmptyArrays": true
                        }
                    },


                    { "$addFields": { "plantas_x_programa": { "$ifNull": ["$programa.properties.custom.Plantas.value", 0] } } },

                    { "$addFields": { "programa": { "$ifNull": ["$programa.properties.name", "no existe"] } } },



                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "Point": 0
                            , "Programa ": 0
                            , "uid": 0
                            , "Formula": 0


                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$programa", "$$programa"]
                            }
                        }
                    }
                ],
                "as": "data_cosecha"
            }
        }

        , { "$unwind": "$data_cosecha" }

        , {
            "$replaceRoot": {
                "newRoot": "$data_cosecha"
            }
        }







        , {
            "$addFields": {
                "anio": { "$year": { "date": "$rgDate" } },
                "semana": { "$week": { "date": "$rgDate", "timezone": "-0500" } },
                "dia_de_semana": { "$dayOfWeek": { "date": "$rgDate" } }
            }
        },

        {
            "$addFields": {
                "semana": { "$sum": ["$semana", 1] },
                "dia_de_semana": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$dia_de_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$dia_de_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$dia_de_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$dia_de_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$dia_de_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$dia_de_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$dia_de_semana", 1] }, "then": "07-Domingo" }

                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        }

        , {
            "$addFields": {
                "N Tallos ": { "$toDouble": "$N Tallos " }
                , "plantas_x_programa": { "$toDouble": "$plantas_x_programa" }


            }
        }

        , {
            "$addFields": {
                "productividad": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_x_programa", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$N Tallos ",
                                "$plantas_x_programa"]
                        }
                    }
                }
            }
        }






        // , {
        //     "$addFields": {
        //         "variable_label": "$programa"

        //         , "variable_dataset": "Productividad por ciclo"
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$variable_label"
        //             , "dashboard_dataset": "$variable_dataset"
        //         }
        //         , "dashboard_cantidad": { "$sum": "$productividad" }
        //         , "test": { "$sum": "$N Tallos " }

        //     }
        // }



        // , {
        //     "$addFields": {
        //         "dashboard_cantidad": {
        //             "$divide": [{
        //                 "$floor": {
        //                     "$multiply": [
        //                         "$dashboard_cantidad",
        //                         10000
        //                     ]
        //                 }
        //             }, 10000]
        //         }
        //     }
        // }



        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_dataset": "$_id.dashboard_dataset"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }

        //     }
        // }

        // , {
        //     "$sort": {
        //         "_id.dashboard_dataset": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "data_group": { "$push": "$$ROOT" }
        //         , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
        //     }
        // }

        // , { "$unwind": "$data_group" }

        // , { "$unwind": "$data_group.data_group" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data_group.data_group",
        //                 {
        //                     "array_dashboard_dataset": "$array_dashboard_dataset"
        //                 }
        //             ]
        //         }
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$_id.dashboard_label"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }
        //     }
        // }
        // , {
        //     "$sort": {
        //         "_id.dashboard_label": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "dashboard_data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$map": {
        //                 "input": "$dashboard_data",
        //                 "as": "item_dashboard_data",
        //                 "in": {
        //                     "$reduce": {
        //                         "input": "$$item_dashboard_data.data_group",
        //                         "initialValue": [],
        //                         "in": {
        //                             "$concatArrays": [
        //                                 "$$value",
        //                                 [
        //                                     {
        //                                         "dashboard_label": "$$this._id.dashboard_label",
        //                                         "dashboard_dataset": "$$this._id.dashboard_dataset",
        //                                         "dashboard_cantidad": "$$this.dashboard_cantidad"
        //                                     }
        //                                 ]
        //                             ]
        //                         }
        //                     }
        //                 }

        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$reduce": {
        //                 "input": "$datos_dashboard",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$concatArrays": [
        //                         "$$value",
        //                         "$$this"
        //                     ]
        //                 }
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "info_datasets": {
        //             "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "array_colores": [
        //             "#008000",
        //             "#ffff00",
        //             "#0000ff",
        //             "#ff0000",
        //             "#ffa500",
        //             "#ee82ee"
        //         ]
        //     }
        // }




        // , {
        //     "$addFields": {
        //         "DATA_ARRAY_DATASETS": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_info_datasets",
        //                 "in": {
        //                     "label": "$$item_info_datasets",
        //                     "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     "data": {
        //                         "$map": {
        //                             "input": "$DATA_LABELS",
        //                             "as": "item_data_labels",
        //                             "in": {
        //                                 "$reduce": {
        //                                     "input": "$datos_dashboard",
        //                                     "initialValue": 0,
        //                                     "in": {
        //                                         "$cond": {
        //                                             "if": {
        //                                                 "$and": [
        //                                                     { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
        //                                                     , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
        //                                                 ]

        //                                             },
        //                                             "then": "$$this.dashboard_cantidad",
        //                                             "else": "$$value"
        //                                         }
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }




        // , { "$project": { "_id": 0 } }


        // , {
        //     "$project": {

        //         "data": {
        //             "labels": "$DATA_LABELS",
        //             "datasets": "$DATA_ARRAY_DATASETS"
        //         },
        //         "options": {
        //             "title": {
        //                 "display": true,
        //                 "text": "Chart.js Bar Chart"
        //             }
        //         }
        //     }
        // }
    ]

)
