[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "cartography",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [

                {
                    "$match": {
                        "properties.type": "trees"
                    }
                },



                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": "si"
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "poligono_adicional": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$poligono_adicional",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "poligono_adicional": { "$ifNull": ["$poligono_adicional.properties.name", "no existe"] } } },



                {
                    "$addFields": {
                        "poligono_muestreo": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "samplingPolygons"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$poligono_muestreo",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "poligono_muestreo": { "$ifNull": ["$poligono_muestreo.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                {
                    "$project": {
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0
                    }
                },

                {
                    "$project": {
                        "finca": "$finca",
                        "bloque": "$bloque",
                        "lote": "$lote",
                        "programa": "$poligono_adicional",
                        "cama": "$poligono_muestreo",
                        "tercio": "$arbol"
                    }
                },


                {
                    "$addFields": {
                        "split_tercio": { "$split": [{ "$trim": { "input": "$tercio", "chars": "-" } }, "-"] }
                    }
                }


                , {
                    "$addFields": {
                        "num_tercio": { "$arrayElemAt": ["$split_tercio", { "$subtract": [{ "$size": "$split_tercio" }, 1] }] }
                    }
                }

                , {
                    "$project": {
                        "split_tercio": 0
                    }
                }



                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
