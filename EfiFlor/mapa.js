//---query mapa
db.form_cosecha.aggregate(
    [
        //----SIMULAR DESDE FORM
        { $unwind: "$Programa .features" },
        {
            "$addFields": {
                "Cartography": "$Programa .features"
            }
        },



        {
            "$group": {
                "_id": {
                    "idform": "$idform",
                    "Cartography": "$Cartography"
                },
                "Indicador": {
                    "$sum": "$N Tallos "
                }
            }
        },

        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$lt": ["$Indicador", 5000]
                        },
                        "then": "#FF0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$Indicador", 5000] }, { "$lte": ["$Indicador", 10000] }]
                                },
                                "then": "#FFFF00",
                                "else": {
                                    "cond": {
                                        "if": {
                                            "$and": [{ "$gt": ["$Indicador", 10000] }, { "$lte": ["$Indicador", 15000] }]
                                        },
                                        "then": "#2d572c",
                                        "else": "#0000FF"
                                    }
                                }
                            }

                        }
                    }
                }
            }
        },


        {
            "$project": {
                "_id": "$_id.Cartography._id",
                "geometry": "$_id.Cartography.geometry",
                "idform": "$_id.idform",
                "type": "Feature",

                "properties": {
                    "color": "$color",
                    "programa": "$_id.Cartography.properties.name"
                }
            }
        }


    ]
)