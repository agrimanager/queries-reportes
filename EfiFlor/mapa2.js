[
    {
        "$group": {
            "_id": {
                "idform": "$idform",
                "programa": "$Cartography.properties.name"
            },
            "data": {
                "$push": "$$ROOT"
            },
            "Indicador": {
                "$sum": "$N Tallos "
            }
        }
    },

    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$lt": ["$Indicador", 5000]
                    },
                    "then": "#FF0000",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$Indicador", 5000] }, { "$lte": ["$Indicador", 10000] }]
                            },
                            "then": "#FFFF00",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gt": ["$Indicador", 10000] }, { "$lte": ["$Indicador", 15000] }]
                                    },
                                    "then": "#2d572c",
                                    "else": "#0000FF"
                                }
                            }
                        }

                    }
                }
            }
        }
    },

    { "$unwind": "$data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "Indicador":"$Indicador",
                        "color":"$color"
                    }
                ]
            }
        }
    },

    {
        "$group": {
            "_id": {
                "idform": "$idform",
                "Cartography": "$Cartography",
                "Indicador":"$Indicador",
                "color":"$color"
            }
        }
    },


    {
        "$project": {
            "_id": "$_id.Cartography._id",
            "geometry": "$_id.Cartography.geometry",
            "idform": "$_id.idform",
            "type": "Feature",

            "properties": {
                "color": "$_id.color",
                "programa": "$_id.Cartography.properties.name"
            }
        }
    }



]