db.form_cosecha.aggregate(
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Programa .path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Programa .features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "programa": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
            }
        },

        {
            "$addFields": {
                "plantas_x_programa": "$programa.properties.custom.Plantas.value"
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "programa": "$programa.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$addFields": {
                "semana": { "$week": { "date": "$rgDate", "timezone": "-0500" } },
                "dia_de_semana": { "$dayOfWeek": { "date": "$rgDate" } }
            }
        },

        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },

        {
            "$addFields": {
                "semana": { "$sum": ["$semana", 1] },
                "dia_de_semana": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$dia_de_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$dia_de_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$dia_de_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$dia_de_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$dia_de_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$dia_de_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$dia_de_semana", 1] }, "then": "07-Domingo" }

                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        }
    ]

)
