db.form_despachodeflor.aggregate(
    [
        {
            "$addFields": {
                "Super select": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                "$Super select",
                                ""
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$ifNull": [
                                "$Super select",
                                0
                            ]
                        }
                    }
                }
            }
        },
        {
            "$sort": {
                "Super select": -1
            }
        },

        {
            "$addFields": {
                "Txt_fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                "Txt_Semana": { "$isoWeek": "$rgDate" }
            }
        }



    ]

)
