[

    { "$limit": 1 },

    {
        "$lookup": {
            "from": "form_despachodeflor",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [



                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },


                {
                    "$addFields": {
                        "Super select": {
                            "$cond": {
                                "if": {
                                    "$eq": [
                                        "$Super select",
                                        ""
                                    ]
                                },
                                "then": 0,
                                "else": {
                                    "$ifNull": [
                                        "$Super select",
                                        0
                                    ]
                                }
                            }
                        }
                    }
                },
                {
                    "$sort": {
                        "Super select": -1
                    }
                }


                , {
                    "$project": {
                        "Point": 0
                    }
                }



            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    , {
        "$addFields": {
            "Txt_fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
            "num_semana": { "$isoWeek": { "date": "$rgDate" } },
            "num_dia_semana": { "$dayOfWeek": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }


            , "Dia_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                    ],
                    "default": "dia de la semana desconocido"
                }
            }
        }
    },

    { "$project": { "num_dia_semana": 0 } }




]
