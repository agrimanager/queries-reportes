db.form_plagas.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Tercio"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "poligono_adicional": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "additionalPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$poligono_adicional",
                "preserveNullAndEmptyArrays": true
            }
        },
        // {
        //     "$addFields": {
        //         "plantas_x_programa": { "$ifNull": ["$poligono_adicional.properties.custom.Plantas.value", 0] }
        //     }
        // },
        { "$addFields": { "poligono_adicional": { "$ifNull": ["$poligono_adicional.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "poligono_muestreo": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "samplingPolygons"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$poligono_muestreo",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "poligono_muestreo": { "$ifNull": ["$poligono_muestreo.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Tercio": 0

                , "Formula": 0
                , "Plagas": 0
                , "uid": 0
            }
        }

        , {
            "$addFields": {
                "programa": "$poligono_adicional",
                "cama": "$poligono_muestreo",
                "tercio": "$arbol"
            }
        }


        , {
            "$project": {
                "poligono_adicional": 0,
                "poligono_muestreo": 0,
                "arbol": 0
            }
        }


        , {
            "$addFields": {
                "split_tercio": { "$split": [{ "$trim": { "input": "$tercio", "chars": "-" } }, "-"] }
            }
        }


        , {
            "$addFields": {
                "num_tercio": { "$arrayElemAt": ["$split_tercio", { "$subtract": [{ "$size": "$split_tercio" }, 1] }] }
            }
        }

        , {
            "$project": {
                "split_tercio": 0
            }
        }

        , {
            "$addFields": {
                "Txt_fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "-0500" } },
                "num_anio": { "$year": "$rgDate" },
                "Txt_Semana": { "$isoWeek": "$rgDate" }
            }
        }


        , {
            "$addFields": {
                "semana": { "$week": { "date": "$rgDate", "timezone": "-0500" } },
                "dia_de_semana": { "$dayOfWeek": { "date": "$rgDate" } }
            }
        }



        , {
            "$addFields": {
                "semana": { "$sum": ["$semana", 1] },
                "dia_de_semana": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$dia_de_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$dia_de_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$dia_de_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$dia_de_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$dia_de_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$dia_de_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$dia_de_semana", 1] }, "then": "07-Domingo" }

                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        }



        //array_plagas
        , {
            "$addFields": {
                "array_plagas": [

                    // {
                    //     "plaga": "xxxxx",
                    //     "individuos": "$xxxxx"
                    // },

                    // {"plaga": "
                    // xxxxx
                    // ","individuos": "$
                    // xxxxx
                    // "},

                    { "plaga": "Acaro blanco", "individuos": "$Acaro blanco" },
                    { "plaga": "Acaro tetrany", "individuos": "$Acaro tetrany" },
                    { "plaga": "Afidos", "individuos": "$Afidos" },
                    { "plaga": "Alternaria", "individuos": "$Alternaria" },
                    { "plaga": "Botrytis", "individuos": "$Botrytis" },
                    { "plaga": "Chinche", "individuos": "$Chinche" },
                    { "plaga": "Coleopteros", "individuos": "$Coleopteros" },
                    { "plaga": "Lepidoptera", "individuos": "$Lepidoptera" },
                    { "plaga": "Mancha foliar", "individuos": "$Mancha foliar" },
                    { "plaga": "Mildeo", "individuos": "$Mildeo" },
                    { "plaga": "Molusco", "individuos": "$Molusco" },
                    { "plaga": "Mosca blanca", "individuos": "$Mosca blanca" },
                    { "plaga": "Punto rojo", "individuos": "$Punto rojo" },
                    { "plaga": "Trips", "individuos": "$Trips" }


                ]
            }
        }


        , {
            "$project": {
                "Acaro blanco": 0,
                "Acaro tetrany": 0,
                "Afidos": 0,
                "Alternaria": 0,
                "Botrytis": 0,
                "Chinche": 0,
                "Coleopteros": 0,
                "Lepidoptera": 0,
                "Mancha foliar": 0,
                "Mildeo": 0,
                "Molusco": 0,
                "Mosca blanca": 0,
                "Punto rojo": 0,
                "Trips": 0

            }
        }



        // , {
        //     "$addFields": {
        //         "condicion_para_C": {
        //             "$cond": {
        //                 "if": {
        //                     "$and": [
        //                         { "$in": ["$array_plagas.plaga", ["Mancha foliar", "Mildeo", "Alternaria"]] },
        //                         { "$in": ["$num_tercio", ["1", "3"]] }
        //                     ]
        //                 },
        //                 "then": "no incluir",
        //                 "else": "incluir"
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$match": {
        //         "condicion_para_C": "incluir"
        //     }
        // }


        , {
            "$unwind": {
                "path": "$array_plagas",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "array_plagas.individuos": { "$toDouble": "$array_plagas.individuos" }
            }
        }

        , {
            "$addFields": {
                "plaga": "$array_plagas.plaga",
                "individuos": "$array_plagas.individuos"
            }
        }


        , {
            "$project": {
                "array_plagas": 0
            }
        }

        , {
            "$addFields": {
                "tiene_plaga": {
                    "$cond": {
                        "if": { "$gt": ["$individuos", 0] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        }



        //---calculos agrupados


        //A)Num de sitios monitoreados
        //B)Num de sitios con individuos
        //C)Num de individuos capturados


        , {
            "$group": {
                "_id": {
                    "num_anio": "$num_anio",
                    "semana": "$Txt_Semana",
                    "plaga": "$plaga",
                    "tercio": "$tercio"
                }
                , "data": { "$push": "$$ROOT" }

                , "a_sitios_monitoreados": { "$sum": 1 }
                , "b_sitios_con_plaga": { "$sum": "$tiene_plaga" }
                , "c_suma_individuos": { "$sum": "$individuos" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "num_anio": "$_id.num_anio",
                    "semana": "$_id.semana",
                    "plaga": "$_id.plaga"
                    // ,"tercio": "$tercio"
                }
                , "data": { "$push": "$$ROOT" }

                , "total_a_sitios_monitoreados": { "$sum": "$a_sitios_monitoreados" }
                , "total_b_sitios_con_plaga": { "$sum": "$b_sitios_con_plaga" }
                , "total_c_suma_individuos": { "$sum": "$c_suma_individuos" }
            }
        }


        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "a_sitios_monitoreados": "$data.a_sitios_monitoreados"
                            , "b_sitios_con_plaga": "$data.b_sitios_con_plaga"
                            , "c_suma_individuos": "$data.c_suma_individuos"


                            , "total_a_sitios_monitoreados": "$total_a_sitios_monitoreados"
                            , "total_b_sitios_con_plaga": "$total_b_sitios_con_plaga"
                            , "total_c_suma_individuos": "$total_c_suma_individuos"
                        }
                    ]
                }
            }
        }



        //project final
        , {
            "$group": {
                "_id": {
                    "num_anio": "$num_anio",
                    "semana": "$Txt_Semana",
                    "plaga": "$plaga",
                    "cama":"$cama",
                    "tercio": "$tercio"

                    , "num_tercio": "$num_tercio"
                }
                , "data": { "$push": "$$ROOT" }

                // , "a_sitios_monitoreados": { "$sum": 1 }
                // , "b_sitios_con_plaga": { "$sum": "$tiene_plaga" }
                , "c_suma_individuos": { "$sum": "$individuos" }


                , "total_a_sitios_monitoreados": { "$min": "$total_a_sitios_monitoreados" }
                , "total_b_sitios_con_plaga": { "$min": "$total_b_sitios_con_plaga" }
                , "total_c_suma_individuos": { "$min": "$total_c_suma_individuos" }


            }
        }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "c_suma_individuos": "$c_suma_individuos"

                            , "total_a_sitios_monitoreados": "$total_a_sitios_monitoreados"
                            , "total_b_sitios_con_plaga": "$total_b_sitios_con_plaga"
                            , "total_c_suma_individuos": "$total_c_suma_individuos"
                        }
                    ]
                }
            }
        }

        //incidencia y severidad

        , {
            "$addFields": {
                "incidencia": {
                    "$multiply": [
                        {
                            "$divide": ["$total_b_sitios_con_plaga", "$total_a_sitios_monitoreados"]
                        }
                        , 100
                    ]
                }
                , "severidad": { "$divide": ["$total_c_suma_individuos", "$total_a_sitios_monitoreados"] }

            }
        }


        , {
            "$addFields": {
                "incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia", 100] }, { "$mod": [{ "$multiply": ["$incidencia", 100] }, 1] }] }, 100] }
                , "severidad": { "$divide": [{ "$subtract": [{ "$multiply": ["$severidad", 100] }, { "$mod": [{ "$multiply": ["$severidad", 100] }, 1] }] }, 100] }
            }
        }







    ]


)
