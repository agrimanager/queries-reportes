db.form_plagas.aggregate(
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Tercio.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Tercio.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "programa": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "cama": { "$arrayElemAt": ["$objetos_del_cultivo", 4] },
                "tercio": { "$arrayElemAt": ["$objetos_del_cultivo", 5] }
            }
        },


        {
            "$addFields": {
                "plantas_x_programa": { "$ifNull": ["$programa.properties.custom.Plantas.value", 0] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "programa": "$programa.properties.name",
                "cama": "$cama.properties.name",
                "tercio": "$tercio.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$addFields": {
                "semana": { "$week": { "date": "$rgDate", "timezone": "-0500" } },
                "dia_de_semana": { "$dayOfWeek": { "date": "$rgDate" } }
            }
        },

        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0,
                "Tercio": 0,
                "Point": 0
            }
        },


        {
            "$addFields": {
                "semana": { "$sum": ["$semana", 1] },
                "dia_de_semana": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$dia_de_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$dia_de_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$dia_de_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$dia_de_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$dia_de_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$dia_de_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$dia_de_semana", 1] }, "then": "07-Domingo" }

                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        }

        // , { "$unwind": "$Plagas" }

        //---totalA = numeros de registros
        , {
            "$group": {
                "_id": {
                    "finca": "$finca"
                }
                , "data": { "$push": "$$ROOT" }
                , "total_A": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_A": "$total_A",
                        }
                    ]
                }
            }
        }



        //---totalB = tercios x plaga

        , { "$unwind": "$Plagas" }
        , {
            "$group": {
                "_id": {
                    "plaga": "$Plagas",
                    "tercio": "$tercio"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "plaga": "$_id.plaga"
                }
                , "data": { "$push": "$$ROOT" }
                , "total_B": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "total_B": "$total_B",
                        }
                    ]
                }
            }
        }



        //---totalC = tercio 2 x plaga (PARTICULAR)
        // Mancha foliar
        // Mildeo
        // Alternaria

        //----condicion
        , {
            "$addFields": {
                "split_tercio": { "$split": [{ "$trim": { "input": "$tercio", "chars": "-" } }, "-"] }
            }
        }


        , {
            "$addFields": {
                "num_tercio": { "$arrayElemAt": ["$split_tercio", { "$subtract": [{ "$size": "$split_tercio" }, 1] }] }
            }
        }


        , {
            "$addFields": {
                "condicion_para_C": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$in": ["$Plagas", ["Mancha foliar", "Mildeo", "Alternaria"]] },
                                { "$in": ["$num_tercio", ["1", "3"]] }
                            ]
                        },
                        "then": "no incluir",
                        "else": "incluir"
                    }
                }
            }
        }



        , {
            "$match": {
                "condicion_para_C": "incluir"
            }
        }


        //----agrupacion
        , {
            "$group": {
                "_id": {
                    "plaga": "$Plagas",
                    "tercio": "$tercio"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "plaga": "$_id.plaga"
                }
                , "data": { "$push": "$$ROOT" }
                , "total_C": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "total_C": "$total_C",
                        }
                    ]
                }
            }
        }



        //----indicadores finales (incidencia y severidad)

        , {
            "$addFields": {
                "incidencia": {
                    "$multiply": [
                        {
                            "$divide": ["$total_B", "$total_A"]
                        }
                        , 100
                    ]
                },
                "severidad": { "$divide": ["$total_C", "$total_B"] }

            }
        }






    ]


)