
var data = db.form_controldeenfermedades.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-08-23T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------

        {
            $match: {
                "Erradicada": "Si"
            }
        }

        , {
            $unwind: "$Palma.features"
        }

        , {
            $addFields: {
                cartografia_oid: { $toObjectId: "$Palma.features._id" }
            }
        }

    ]

)


// data



data.forEach(i => {

    //====UPDATE -- agregar propiedad
    db.cartography.update(
        { _id: i.cartografia_oid },
        {
            $set: {
                "properties.custom.Erradicada": {
                    "type": "bool",
                    "value": true,
                    "color": "#1B1A1A"
                }
            }
        }
    )

})

/*
	"Erradicada" : {
		"type" : "bool",
		"value" : false,
		"color" : "#1B1A1A",
		"id" : NumberInt(0)
	},
*/
