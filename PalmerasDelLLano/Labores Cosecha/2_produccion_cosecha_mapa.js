
//---NOTA: data de labores pegar a formulario recurrencia en registros
db.form_controldeerradicacin.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("609ea8cb491ced0df50ee0a7"),
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",

            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                        // ,"FincaID": "$FincaID"
                        // ,"idform": "$idform"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //query
        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today"
                },
                //query
                "pipeline": [

                    //cartografia
                    { "$unwind": "$cartography.features" },
                    {
                        "$match": {
                            "cartography.features.properties.type": "lot"
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

                    //actividades de Cosecha
                    {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },

                    {
                        "$match": {
                            "activity.name": {
                                "$regex": "Cosecha"
                            }
                        }
                    },
                    {
                        "$match": {
                            // "activity.productivity.measure" : "kg"
                            "productivityPrice.measure": "kg"
                        }
                    },

                    // //fechas
                    {
                        "$addFields": {
                            "Fecha inicio": { "$max": "$when.start" },
                            "Fecha fin": { "$max": "$when.finish" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$lte": ["$Fecha fin", "$$busqueda_fin"]
                            }
                        }
                    },


                    //cantidad
                    {
                        "$addFields": {
                            "cantidad_kg": { "$max": "$productivityReport.quantity" }
                        }
                    },

                    {
                        "$group": {
                            "_id": {
                                "lote": "$lote"
                            }
                            , "fecha_max": { "$max": "$Fecha fin" }
                            , "sum_cantidad_kg": { "$sum": "$cantidad_kg" }
                            , "data": { "$push": "$$ROOT" }
                        }
                    },

                    { "$unwind": "$data" },

                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data",
                                    {
                                        "fecha_max": "$fecha_max"
                                        , "sum_cantidad_kg": "$sum_cantidad_kg"
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Fecha fin", "$fecha_max"]
                            }
                        }
                    }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_labores",
                "preserveNullAndEmptyArrays": true
            }
        }


        // //----proyeccion final
        // , {
        //     "$project": {
        //         "lote": "$data_labores.lote"
        //         // ,"hoy": "$today"
        //         , "fecha_ultimo_registro": "$data_labores.fecha_max"
        //         , "sum_cantidad_kg": "$data_labores.sum_cantidad_kg"
        //     }
        // }

        //TON
        , {
            "$addFields": {
                "cantidad_TON": { "$divide": ["$data_labores.sum_cantidad_kg",1000] }
            }
        }


        // //================================
        // //-----MAPA

        //--indicador color
        , {
            "$addFields": {
                "indicador": "$cantidad_TON"//🚩EDITAR🚩
            }
        }
        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
            }
        }

        // //--leyenda
        /*
        : #,
        : #,
        : #,
        : #,
        : #,
        : #,
        : #,
        : #,
        : #,
        : #,
        : #
        */


        // // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$indicador", 100] }
                                    ]
                                }
                                , "then": "#dc143c"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 100] }
                                        , { "$lte": ["$indicador", 200] }
                                    ]
                                }
                                , "then": "#bdb76b"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 200] }
                                        , { "$lte": ["$indicador", 300] }
                                    ]
                                }
                                , "then": "#808000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 300] }
                                        , { "$lte": ["$indicador", 400] }
                                    ]
                                }
                                , "then": "#00ff00"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 400] }
                                        , { "$lte": ["$indicador", 500] }
                                    ]
                                }
                                , "then": "#adff2f"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 500] }
                                        , { "$lte": ["$indicador", 600] }
                                    ]
                                }
                                , "then": "#7fff00"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 600] }
                                        , { "$lte": ["$indicador", 700] }
                                    ]
                                }
                                , "then": "#3cb371"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 700] }
                                        , { "$lte": ["$indicador", 800] }
                                    ]
                                }
                                , "then": "#8fbc8f"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 800] }
                                        , { "$lte": ["$indicador", 900] }
                                    ]
                                }
                                , "then": "#9acd32"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 900] }
                                        , { "$lte": ["$indicador", 1000] }
                                    ]
                                }
                                , "then": "#556b2f"
                            },

                            //--ultimo
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 1000] }
                                    ]
                                }
                                , "then": "#006400"
                            }

                        ],
                        "default": "#000000"
                    }
                }
                ,"rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$indicador", 100] }
                                    ]
                                }
                                , "then": "A-[0 - 100] Toneladas"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 100] }
                                        , { "$lte": ["$indicador", 200] }
                                    ]
                                }
                                , "then": "B-(100 - 200] Toneladas"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 200] }
                                        , { "$lte": ["$indicador", 300] }
                                    ]
                                }
                                , "then": "C-(200 - 300] Toneladas"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 300] }
                                        , { "$lte": ["$indicador", 400] }
                                    ]
                                }
                                , "then": "D-(300 - 400] Toneladas"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 400] }
                                        , { "$lte": ["$indicador", 500] }
                                    ]
                                }
                                , "then": "E-(400 - 500] Toneladas"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 500] }
                                        , { "$lte": ["$indicador", 600] }
                                    ]
                                }
                                , "then": "F-(500 - 600] Toneladas"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 600] }
                                        , { "$lte": ["$indicador", 700] }
                                    ]
                                }
                                , "then": "G-(600 - 700] Toneladas"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 700] }
                                        , { "$lte": ["$indicador", 800] }
                                    ]
                                }
                                , "then": "H-(700 - 800] Toneladas"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 800] }
                                        , { "$lte": ["$indicador", 900] }
                                    ]
                                }
                                , "then": "I-(800 - 900] Toneladas"
                            },{
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 900] }
                                        , { "$lte": ["$indicador", 1000] }
                                    ]
                                }
                                , "then": "J-(900 - 1000] Toneladas"
                            },

                            //--ultimo
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 1000] }
                                    ]
                                }
                                , "then": "K-( > 1000] Toneladas"
                            }

                        ],
                        "default": "#000000"
                    }
                }

            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": { "$toObjectId":"$data_labores.cartography.features._id"}
                , "cartography_geometry": "$data_labores.cartography.features.geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {
                    "Lote": "$data_labores.lote",
                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color"

                    , "Toneladas": { "$concat": [{ "$toString": "$indicador" }, " TON"] }//🚩EDITAR🚩
                }

            }
        }







    ]
)
