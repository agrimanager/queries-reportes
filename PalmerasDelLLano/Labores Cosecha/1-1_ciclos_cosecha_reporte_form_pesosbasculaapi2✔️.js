[

        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        , {
            "$lookup": {
                "from": "form_pesosbasculaapi",
                "as": "data",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today"
                },

                "pipeline": [


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "Lote",
                            "foreignField": "properties.name",
                            "as": "info_lote"
                        }
                    },
                    { "$unwind": "$info_lote" }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$Lote"
                                , "today": "$$today"
                            }
                            , "fecha_max": { "$max": "$rgDate" }

                        }
                    },



                    {
                        "$addFields": {
                            "dias de ciclo": {
                                "$floor": {
                                    "$divide": [{ "$subtract": ["$_id.today", "$fecha_max"] }, 86400000]
                                }
                            }
                        }
                    }


                    , {
                        "$project": {
                            "lote": "$_id.lote"
                            , "hoy": "$_id.today"
                            , "fecha_ultimo_registro": "$fecha_max"
                            , "dias_ultimo_ciclo": "$dias de ciclo"
                        }
                    }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }



        , {
            "$project": {
                "lote": "$data.lote"
                ,"hoy": "$today"
                ,"fecha_ultimo_registro": "$data.fecha_ultimo_registro"
                ,"dias_ultimo_ciclo": "$data.dias_ultimo_ciclo"
            }
        }





    ]
