
[

    { "$limit": 1 },

    {
        "$project": {
            "datos": [
                {
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "today": "$today"
                }
            ]
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }

    , {
        "$lookup": {
            "from": "tasks",
            "as": "data_labores",
            "let": {

                "busqueda_inicio": "$Busqueda inicio",
                "busqueda_fin": "$Busqueda fin",
                "today": "$today"
            },
            "pipeline": [

                { "$unwind": "$cartography.features" },
                {
                    "$match": {
                        "cartography.features.properties.type": "lot"
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

                {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                },
                { "$unwind": "$activity" },

                {
                    "$match": {
                        "activity.name": {
                            "$regex": "Cosecha"
                        }
                    }
                },

                {
                    "$addFields": {
                        "Fecha inicio": { "$max": "$when.start" },
                        "Fecha fin": { "$max": "$when.finish" }
                    }
                },

                {
                    "$group": {
                        "_id": {
                            "lote": "$lote"
                        }
                        , "fecha_max": { "$max": "$Fecha fin" }
                        , "data": { "$push": "$$ROOT" }
                    }
                },

                { "$unwind": "$data" },

                {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data",
                                { "fecha_max": "$fecha_max" }
                            ]
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$Fecha fin", "$fecha_max"]
                        }
                    }
                },

                {
                    "$addFields": {
                        "dias de ciclo": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$$today", "$fecha_max"] }, 86400000]
                            }
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_labores",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$project": {
            "lote": "$data_labores.lote"
            ,"hoy": "$today"
            ,"fecha_ultimo_registro": "$data_labores.fecha_max"
            ,"dias_ultimo_ciclo": "$data_labores.dias de ciclo"
        }
    }


]
