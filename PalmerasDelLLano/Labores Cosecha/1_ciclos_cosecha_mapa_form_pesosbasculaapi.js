//reporte
var cursor = db.form_Censodepc.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                // "Busqueda inicio": ISODate("2021-07-16T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-23T00:00:00.000-05:00"),
                // "Busqueda fin": new Date,

                //=================SAN ALEJO (fechas ciclos)

                //Finca: San Alejo
                //desde: ciclo 2
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-06-19T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-28T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 3
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-08-03T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 2
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-06-19T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-28T00:00:00.000-05:00"),

                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 4
                "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                "Busqueda fin": ISODate("2021-11-21T00:00:00.000-05:00"),

                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 1
                // "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-06-18T00:00:00.000-05:00"),


                // //Finca: San Alejo
                // //desde: ciclo 2
                // //hasta: ciclo 2
                // "Busqueda inicio": ISODate("2021-06-19T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-08-02T00:00:00.000-05:00"),


                //Finca: San Alejo
                //desde: ciclo 1
                //hasta: ciclo 2
                // "Busqueda inicio": ISODate("2021-05-24T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-08-02T00:00:00.000-05:00"),



                //=================CAICESA (fechas ciclos)
                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 1
                //hasta: ciclo 3
                // "Busqueda inicio": ISODate("2021-04-12T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-09-21T00:00:00.000-05:00"),

                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 3
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-07-16T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-22T00:00:00.000-05:00"),

                //Finca: Caicesa-Ponce-Pizzati
                //desde: ciclo 1
                //hasta: ciclo 4
                // "Busqueda inicio": ISODate("2021-04-12T00:00:00.000-05:00"),
                // "Busqueda fin": ISODate("2021-10-22T00:00:00.000-05:00"),

                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",
            }
        },

        //FILTRO FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            // "$eq": ["$Point.farm", "5fbe5f972033e75d57a736e6"]//Caicesa-Ponce-Pizzati
                            "$eq": ["$Point.farm", "5fac01ce246347247f068528"]//San Alejo

                        }
                    ]
                }
            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },

        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },

        { "$addFields": { "finca_id": { "$arrayElemAt": ["$split_path_padres_oid", 0] } } },

        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },

        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$lookup": {
                "from": "cartography_polygon",
                "localField": "lote._id",
                "foreignField": "_id",
                "as": "info_lote"
            }
        },

        {
            "$addFields": {
                "info_lote": "$info_lote.internal_features"
            }
        },
        { "$unwind": "$info_lote" },

        {
            "$addFields": {
                "total_plantas_x_lote": {
                    "$filter": {
                        "input": "$info_lote",
                        "as": "item",
                        "cond": { "$eq": ["$$item.type_features", "trees"] }
                    }
                }
            }
        },
        { "$unwind": "$total_plantas_x_lote" },

        {
            "$addFields": {
                "total_plantas_x_lote": "$total_plantas_x_lote.count_features"
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },





        {
            "$lookup": {
                "from": "cartography",
                "as": "data_cartography",
                "let": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "variable_custom": "$properties.custom.Eliminada"
                        }
                    },

                    {
                        "$match": {
                            "variable_custom": { "$exists": true },
                            "variable_custom.value": { "$ne": false }
                        }
                    },



                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },



                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0
                        }
                    },



                    {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "bloque": "$bloque",
                                "lote": "$lote"
                            }
                            , "custom_x_lote_cantidad": { "$sum": 1 }
                        }
                    },


                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$_id.finca"] },
                                    { "$eq": ["$$bloque", "$_id.bloque"] },
                                    { "$eq": ["$$lote", "$_id.lote"] }
                                ]
                            }
                        }
                    }
                ]
            }
        },

        {
            "$unwind": {
                "path": "$data_cartography",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "plantas_eliminadas_x_lote": { "$ifNull": ["$data_cartography.custom_x_lote_cantidad", 0] }
            }
        },

        {
            "$project": {
                "data_cartography": 0
            }
        },



        {
            "$addFields": {
                "plantas_x_lote": {
                    "$subtract": ["$total_plantas_x_lote", "$plantas_eliminadas_x_lote"]
                }
            }
        },




        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "info_lote": 0,
                "Palma": 0,
                "Point": 0,
                "uid": 0,
                "uDate": 0,
                "Formula": 0
            }
        },

        {
            "$addFields": {
                "tiene_enfermedad": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$Secamiento de foliolo rudimentario", "SI"] },
                                { "$eq": ["$Amarillamiento de hojas jovenes", "SI"] },
                                { "$eq": ["$Mordisco en hojas", "SI"] },
                                { "$ne": ["$Severidad de la enfermedad", ""] }
                            ]
                        },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },

        {
            "$match": {
                "tiene_enfermedad": 1
            }
        },




        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    // "linea": "$linea", //ajuste con mapa (porque lote "19D-3 " tenia errores con linea)
                    "arbol": "$arbol"

                }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"

                }
                , "numero_palmas_enfermas": { "$sum": 1 }
                , "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "numero_palmas_enfermas": "$numero_palmas_enfermas" }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "# plantas por lote": "$plantas_x_lote",
                "# plantas enfermas por lote": "$numero_palmas_enfermas",

                "% incidencia de plantas enfermas": {
                    "$cond": {
                        "if": { "$gt": ["$total_plantas_x_lote", 0] },
                        "then": {
                            "$divide": [
                                {
                                    "$floor": {
                                        "$multiply": [
                                            { "$divide": [{ "$multiply": ["$numero_palmas_enfermas", 100] }, "$total_plantas_x_lote"] }
                                            , 100]
                                    }
                                }
                                , 100]
                        },
                        "else": 0
                    }
                }
            }
        },



        {
            "$project": {
                "tiene_enfermedad": 0,
                "plantas_x_lote": 0,
                "numero_palmas_enfermas": 0
            }
        }



        //-----agregar ciclo
        //obtener ciclo de registro
        , {
            "$lookup": {
                "from": "form_ciclos",
                "as": "data_ciclos",
                "let": {
                    "fecha_censo": "$rgDate"
                    , "today": "$today"
                    , "finca_id": "$finca_id"
                },
                "pipeline": [

                    { "$addFields": { "finca": { "$toObjectId": "$Point.farm" } } },
                    { "$match": { "$expr": { "$eq": ["$finca", "$$finca_id"] } } },

                    { "$addFields": { "anio_fecha": { "$year": "$Fin de ciclo" } } },

                    {
                        "$addFields": {
                            "Fin de ciclo": {
                                "$cond": {
                                    "if": {
                                        "$and": [
                                            { "$gt": ["$anio_fecha", 2000] },
                                            { "$lt": ["$anio_fecha", 3000] }
                                        ]
                                    },
                                    "then": "$Fin de ciclo",
                                    "else": "$$today"
                                }
                            },
                            "ciclo_tiene_fin": {
                                "$cond": {
                                    "if": {
                                        "$and": [
                                            { "$gt": ["$anio_fecha", 2000] },
                                            { "$lt": ["$anio_fecha", 3000] }
                                        ]
                                    },
                                    "then": "SI",
                                    "else": "NO"
                                }
                            }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            {
                                                "$dateFromString": {
                                                    "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } }
                                                    , "timezone": "America/Managua"
                                                }
                                            }
                                            ,
                                            {
                                                "$dateFromString": {
                                                    "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Inicio de ciclo", "timezone": "America/Managua" } }
                                                    , "timezone": "America/Managua"
                                                }
                                            }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            {
                                                "$dateFromString": {
                                                    "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha_censo", "timezone": "America/Managua" } }
                                                    , "timezone": "America/Managua"
                                                }
                                            }
                                            ,
                                            {
                                                "$dateFromString": {
                                                    "dateString": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fin de ciclo", "timezone": "America/Managua" } }
                                                    , "timezone": "America/Managua"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        },

        {
            "$unwind": {
                "path": "$data_ciclos",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {
                "ciclo num": { "$ifNull": [{ "$toDouble": "$data_ciclos.Ciclo" }, -1] }
                , "ciclo fecha inicio": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Inicio de ciclo", "timezone": "America/Managua" } }
                , "ciclo fecha fin": {
                    "$cond": {
                        "if": { "$eq": ["$data_ciclos.ciclo_tiene_fin", "SI"] },
                        "then": { "$dateToString": { "format": "%Y-%m-%d", "date": "$data_ciclos.Fin de ciclo", "timezone": "America/Managua" } },
                        "else": "ciclo sin fecha fin"
                    }

                }
            }
        },

        {
            "$project": {
                "data_ciclos": 0
            }
        }


        // //---test
        // , {
        //     $match: {
        //         //"lote": "19D-3 "
        //         // "lote": "3-2B"
        //         "lote": "19D-1"

        //     }
        // }



        // //========= DATA PARA BORRAR
        // , {
        //     $group: {
        //         _id: {

        //             "lote": "$lote"
        //             , "arbol": "$arbol"

        //             // ,"ciclo":"$ciclo num"
        //         }
        //         , "data": { "$push": "$$ROOT" }
        //         , "data_ciclos": { "$push": "$ciclo num" }
        //         , "data_ciclos_set": { "$addToSet": "$ciclo num" }
        //     }
        // }


        // , { "$addFields": { "size_data": { "$size": "$data" } } }
        // , { "$addFields": { "size_data_ciclos": { "$size": "$data_ciclos" } } }
        // , { "$addFields": { "size_data_ciclos_set": { "$size": "$data_ciclos_set" } } }

        // , {
        //     $match: {
        //         "size_data": { $gt: 1 }
        //     }
        // }

        // // , {
        // //     $match: {
        // //         "data_ciclos": [2,2]
        // //     }
        // // }

        // ,{
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //                 {"$eq": ["$size_data", "$size_data_ciclos"]},
        //                 {"$ne": ["$size_data", "$size_data_ciclos_set"]}
        //             ]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         //"data_borrar": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //         "data_borrar": {
        //             $map: {
        //                 input: "$data",
        //                 as: "item_data",
        //                 in: {
        //                     "$cond": {
        //                         "if": { "$eq": ["$$item_data.ciclo num", 2] },
        //                         //"then": "$$item_data",
        //                         "then": "$$item_data._id",
        //                         "else": null
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "data_borrar": {
        //             "$filter": {
        //                 "input": "$data_borrar",
        //                 "as": "item_data",
        //                 "cond": { "$ne": ["$$item_data", null] }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$unwind": {
        //         "path": "$data_borrar",
        //         "preserveNullAndEmptyArrays": false
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "ids": {
        //             "$push": "$data_borrar"
        //         }
        //     }
        // },








    ]
    , { allowDiskUse: true }
)
// .count()




cursor


// cursor.forEach(i => {

//     db.form_Censodepc.remove(
//         {
//             "_id": {
//                 "$in": i.ids
//             }
//         }
//     )

// })
