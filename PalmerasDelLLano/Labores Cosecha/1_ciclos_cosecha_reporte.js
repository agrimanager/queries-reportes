
//---NOTA: data de labores pegar a formulario recurrencia en registros
db.form_controldeerradicacin.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("609ea8cb491ced0df50ee0a7"),
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",

            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                        // ,"FincaID": "$FincaID"
                        // ,"idform": "$idform"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //query
        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today"
                },
                //query
                "pipeline": [

                    //cartografia
                    { "$unwind": "$cartography.features" },
                    {
                        "$match": {
                            "cartography.features.properties.type": "lot"
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

                    //actividades de Cosecha
                    {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },

                    {
                        "$match": {
                            "activity.name": {
                                "$regex": "Cosecha"
                            }
                        }
                    },

                    //fechas
                    {
                        "$addFields": {
                            "Fecha inicio": { "$max": "$when.start" },
                            "Fecha fin": { "$max": "$when.finish" }
                        }
                    },

                    {
                        "$group": {
                            "_id": {
                                "lote": "$lote"
                            }
                            , "fecha_max": { "$max": "$Fecha fin" }
                            , "data": { "$push": "$$ROOT" }
                        }
                    },

                    { "$unwind": "$data" },

                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data",
                                    { "fecha_max": "$fecha_max" }
                                ]
                            }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Fecha fin", "$fecha_max"]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "dias de ciclo": {
                                "$floor": {
                                    "$divide": [{ "$subtract": ["$$today", "$fecha_max"] }, 86400000]
                                }
                            }
                        }
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_labores",
                "preserveNullAndEmptyArrays": true
            }
        }


        //----proyeccion final
        , {
            "$project": {
                "lote": "$data_labores.lote"
                ,"hoy": "$today"
                ,"fecha_ultimo_registro": "$data_labores.fecha_max"
                ,"dias_ultimo_ciclo": "$data_labores.dias de ciclo"
            }
        }






    ]
)
