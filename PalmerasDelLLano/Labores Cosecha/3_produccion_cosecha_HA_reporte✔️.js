[

{ "$limit": 1 },

{
    "$project": {
        "datos": [
            {
                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",
                "today": "$today"
            }
        ]
    }
}

, { "$unwind": "$datos" }
, { "$replaceRoot": { "newRoot": "$datos" } }


, {
    "$lookup": {
        "from": "tasks",
        "as": "data_labores",
        "let": {

            "busqueda_inicio": "$Busqueda inicio",
            "busqueda_fin": "$Busqueda fin",
            "today": "$today"
        },
        "pipeline": [

            { "$unwind": "$cartography.features" },
            {
                "$match": {
                    "cartography.features.properties.type": "lot"
                }
            },
            { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

            {
                "$lookup": {
                    "from": "activities",
                    "localField": "activity",
                    "foreignField": "_id",
                    "as": "activity"
                }
            },
            { "$unwind": "$activity" },

            {
                "$match": {
                    "activity.name": {
                        "$regex": "Cosecha"
                    }
                }
            },
            {
                "$match": {
                    "productivityPrice.measure": "kg"
                }
            },

            {
                "$addFields": {
                    "Fecha inicio": { "$max": "$when.start" },
                    "Fecha fin": { "$max": "$when.finish" }
                }
            },

            {
                "$match": {
                    "$expr": {
                        "$lte": ["$Fecha fin", "$$busqueda_fin"]
                    }
                }
            },

            {
                "$addFields": {
                    "cantidad_kg": { "$max": "$productivityReport.quantity" }
                }
            },

            {
                "$group": {
                    "_id": {
                        "lote": "$lote"
                    }
                    , "fecha_max": { "$max": "$Fecha fin" }
                    , "sum_cantidad_kg": { "$sum": "$cantidad_kg" }
                    , "data": { "$push": "$$ROOT" }
                }
            },

            { "$unwind": "$data" },

            {
                "$replaceRoot": {
                    "newRoot": {
                        "$mergeObjects": [
                            "$data",
                            {
                                "fecha_max": "$fecha_max"
                                , "sum_cantidad_kg": "$sum_cantidad_kg"
                            }
                        ]
                    }
                }
            },

            {
                "$match": {
                    "$expr": {
                        "$eq": ["$Fecha fin", "$fecha_max"]
                    }
                }
            }

        ]
    }
}

, {
    "$unwind": {
        "path": "$data_labores"
    }
}


, {
    "$project": {
        "lote": "$data_labores.lote"
        , "fecha_ultimo_registro": "$data_labores.fecha_max"
        , "sum_cantidad_kg": "$data_labores.sum_cantidad_kg"
    }
}

, {
    "$addFields": {
        "cantidad_TON": { "$divide": ["$sum_cantidad_kg",1000] }
    }
}


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "lote",
            "foreignField": "properties.name",
            "as": "data_lote"
        }
    },
    { "$unwind": "$data_lote" }
    , {
        "$addFields": {
            "lote_ha": "$data_lote.properties.custom.area.value"
        }
    }
    , {
        "$project": {
            "data_lote": 0
        }
    }

    , {
        "$addFields": {
            "cantidad_TON_HA": {
                "$cond": {
                    "if": { "$eq": ["$lote_ha", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$cantidad_TON",
                            "$lote_ha"]
                    }
                }
            }
        }
    }




]
