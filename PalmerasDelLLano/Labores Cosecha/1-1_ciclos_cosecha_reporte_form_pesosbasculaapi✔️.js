[

    {
        "$lookup": {
            "from": "cartography",
            "localField": "Lote",
            "foreignField": "properties.name",
            "as": "info_lote"
        }
    },
    { "$unwind": "$info_lote" }


    , {
        "$group": {
            "_id": {
                "lote": "$Lote"
                ,"today":"$today"
            }
            , "fecha_max": { "$max": "$rgDate" }
        }
    },


    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$fecha_max"] }, 86400000]
                }
            }
        }
    }

    , {
        "$project": {
            "lote": "$_id.lote"
            ,"hoy": "$_id.today"
            ,"fecha_ultimo_registro": "$fecha_max"
            ,"dias_ultimo_ciclo": "$dias de ciclo"
        }
    }





]
