
//---NOTA: data de labores pegar a formulario recurrencia en registros
db.form_pesosbasculaapi.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("609ea8cb491ced0df50ee0a7"),
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",

            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                        // ,"FincaID": "$FincaID"
                        // ,"idform": "$idform"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //query
        , {
            "$lookup": {
                "from": "form_pesosbasculaapi",
                "as": "data",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today"
                },
                //query
                "pipeline": [

                    //cartografia txt
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "Lote",
                            "foreignField": "properties.name",
                            "as": "info_lote"
                        }
                    },
                    { "$unwind": "$info_lote" }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$Lote"
                                , "today": "$$today"
                            }
                            , "fecha_max": { "$max": "$rgDate" }
                            // , "data": { "$push": "$$ROOT" }
                        }
                    },

                    // { "$unwind": "$data" },

                    {
                        "$addFields": {
                            "dias de ciclo": {
                                "$floor": {
                                    "$divide": [{ "$subtract": ["$_id.today", "$fecha_max"] }, 86400000]
                                }
                            }
                        }
                    }

                    //----proyeccion final
                    , {
                        "$project": {
                            "lote": "$_id.lote"
                            , "hoy": "$_id.today"
                            , "fecha_ultimo_registro": "$fecha_max"
                            , "dias_ultimo_ciclo": "$dias de ciclo"
                        }
                    }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }


          //----proyeccion final
        , {
            "$project": {
                "lote": "$data.lote"
                ,"hoy": "$today"
                ,"fecha_ultimo_registro": "$data.fecha_ultimo_registro"
                ,"dias_ultimo_ciclo": "$data.dias_ultimo_ciclo"
            }
        }





    ]
)
