
//---NOTA: data de labores pegar a formulario recurrencia en registros
db.form_pesosbasculaapi.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("609ea8cb491ced0df50ee0a7"),
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",

            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                        // ,"FincaID": "$FincaID"
                        , "idform": "$idform"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //query
        , {
            "$lookup": {
                "from": "form_pesosbasculaapi",
                "as": "data",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today",
                    "idform": "$idform"
                },
                //query
                "pipeline": [

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "Lote",
                            "foreignField": "properties.name",
                            "as": "info_lote"
                        }
                    },
                    { "$unwind": "$info_lote" }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$Lote"
                                , "today": "$$today"
                                , "idform": "$$idform"

                                , "cartography_id": "$info_lote._id"
                                , "cartography_geometry": "$info_lote.geometry"
                            }
                            , "fecha_max": { "$max": "$rgDate" }
                        }
                    },


                    {
                        "$addFields": {
                            "dias de ciclo": {
                                "$floor": {
                                    "$divide": [{ "$subtract": ["$_id.today", "$fecha_max"] }, 86400000]
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "indicador": "$dias de ciclo"
                        }
                    }

                    , {
                        "$addFields": {
                            "color": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$lte": ["$indicador", 1] }
                                                ]
                                            }
                                            , "then": "#e0f3f8"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 2] }
                                                    , { "$lt": ["$indicador", 10] }
                                                ]
                                            }
                                            , "then": "#66ff33"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 10] }
                                                    , { "$lt": ["$indicador", 13] }
                                                ]
                                            }
                                            , "then": "#ffff00"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 13] }
                                                    , { "$lt": ["$indicador", 16] }
                                                ]
                                            }
                                            , "then": "#fdae61"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 16] }
                                                ]
                                            }
                                            , "then": "#d60000"
                                        }

                                    ],
                                    "default": "#000000"
                                }
                            }
                            , "rango": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$lte": ["$indicador", 1] }
                                                ]
                                            }
                                            , "then": "A-[<= 1] Dias"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 2] }
                                                    , { "$lt": ["$indicador", 10] }
                                                ]
                                            }
                                            , "then": "B-[2 - 9] Dias"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 10] }
                                                    , { "$lt": ["$indicador", 13] }
                                                ]
                                            }
                                            , "then": "C-[10 - 12] Dias"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 13] }
                                                    , { "$lt": ["$indicador", 16] }
                                                ]
                                            }
                                            , "then": "D-[13 - 15] Dias"
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gte": ["$indicador", 16] }
                                                ]
                                            }
                                            , "then": "E-( > 15) Dias"
                                        }

                                    ],
                                    "default": "otro"
                                }
                            }


                        }
                    }


                    , {
                        "$project": {
                            "_id": "$_id.cartography_id",
                            "idform": "$_id.idform",
                            "geometry": { "$ifNull": ["$_id.cartography_geometry", {}] },

                            "type": "Feature",

                            "properties": {
                                "Lote": "$_id.lote",
                                "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                                "color": "$color"

                                , "Dias": { "$concat": [{ "$toString": "$indicador" }, " Dias"] }
                            }

                        }
                    }


                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data",
                "preserveNullAndEmptyArrays": true
            }
        }


        // //----proyeccion final
        // , {
        //     "$project": {
        //         "lote": "$data.lote"
        //         , "hoy": "$today"
        //         , "fecha_ultimo_registro": "$data.fecha_ultimo_registro"
        //         , "dias_ultimo_ciclo": "$data.dias_ultimo_ciclo"
        //     }
        // }


        , {
            "$project": {
                "_id": "$data._id",
                "idform": "$data.idform",
                "geometry": { "$ifNull": ["$data.geometry", {}] },

                "type": "Feature",

                "properties": {
                    "Lote": "$data.properties.Lote",
                    "Rango": { "$ifNull": ["$data.properties.rango", "SIN DATOS"] },
                    "color": "$data.properties.color"

                    // , "Dias": { "$concat": [{ "$toString": "$indicador" }, " Dias"] }
                    ,"Dias": "$data.properties.Dias"
                }

            }
        }





    ]
)
