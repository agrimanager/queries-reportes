
//---NOTA: data de labores pegar a formulario recurrencia en registros
db.form_controldeerradicacin.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-10-01T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("609ea8cb491ced0df50ee0a7"),
                //------MAPA_VARIABLE_IDFORM
                // "idform": "123",

            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------


        //----new
        //no mostrar datos de form
        { "$limit": 1 },

        {
            "$project": {
                "datos": [
                    {
                        "Busqueda inicio": "$Busqueda inicio",
                        "Busqueda fin": "$Busqueda fin",
                        "today": "$today"
                        // ,"FincaID": "$FincaID"
                        // ,"idform": "$idform"
                    }
                ]
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        //query
        , {
            "$lookup": {
                "from": "tasks",
                "as": "data_labores",
                "let": {

                    "busqueda_inicio": "$Busqueda inicio",
                    "busqueda_fin": "$Busqueda fin",
                    "today": "$today"
                },
                //query
                "pipeline": [

                    //cartografia
                    { "$unwind": "$cartography.features" },
                    {
                        "$match": {
                            "cartography.features.properties.type": "lot"
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

                    //actividades de Cosecha
                    {
                        "$lookup": {
                            "from": "activities",
                            "localField": "activity",
                            "foreignField": "_id",
                            "as": "activity"
                        }
                    },
                    { "$unwind": "$activity" },

                    {
                        "$match": {
                            "activity.name": {
                                "$regex": "Cosecha"
                            }
                        }
                    },
                    {
                        "$match": {
                            // "activity.productivity.measure" : "kg"
                            "productivityPrice.measure": "kg"
                        }
                    },

                    // //fechas
                    {
                        "$addFields": {
                            "Fecha inicio": { "$max": "$when.start" },
                            "Fecha fin": { "$max": "$when.finish" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$lte": ["$Fecha fin", "$$busqueda_fin"]
                            }
                        }
                    },


                    //cantidad
                    {
                        "$addFields": {
                            "cantidad_kg": { "$max": "$productivityReport.quantity" }
                        }
                    },

                    {
                        "$group": {
                            "_id": {
                                "lote": "$lote"
                            }
                            , "fecha_max": { "$max": "$Fecha fin" }
                            , "sum_cantidad_kg": { "$sum": "$cantidad_kg" }
                            , "data": { "$push": "$$ROOT" }
                        }
                    },

                    { "$unwind": "$data" },

                    {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data",
                                    {
                                        "fecha_max": "$fecha_max"
                                        , "sum_cantidad_kg": "$sum_cantidad_kg"
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$Fecha fin", "$fecha_max"]
                            }
                        }
                    }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_labores",
                "preserveNullAndEmptyArrays": true
            }
        }


        // //----proyeccion final
        // , {
        //     "$project": {
        //         "lote": "$data_labores.lote"
        //         // ,"hoy": "$today"
        //         , "fecha_ultimo_registro": "$data_labores.fecha_max"
        //         , "sum_cantidad_kg": "$data_labores.sum_cantidad_kg"
        //     }
        // }

        //TON
        , {
            "$addFields": {
                "cantidad_TON": { "$divide": ["$data_labores.sum_cantidad_kg",1000] }
            }
        }

        //---obtener area de lote
        , {
            "$lookup": {
                "from": "cartography",
                "localField": "data_labores.lote",
                "foreignField": "properties.name",
                "as": "data_lote"
            }
        },
        { "$unwind": "$data_lote" }
        , {
            "$addFields": {
                "lote_ha": "$data_lote.properties.custom.area.value"
            }
        }
        , {
            "$project": {
                "data_lote": 0
            }
        }


        //--ton x ha
        , {
            "$addFields": {
                "cantidad_TON_HA": {
                    "$cond": {
                        "if": { "$eq": ["$lote_ha", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$cantidad_TON",
                                "$lote_ha"]
                        }
                    }
                }
            }
        }


        // //================================
        // //-----MAPA

        //--indicador color
        , {
            "$addFields": {
                "indicador": "$cantidad_TON_HA"//🚩EDITAR🚩
            }
        }
        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
            }
        }

        // // //--leyenda
        // /*
        // : #,
        // : #,
        // : #,
        // : #,
        // : #
        // */


        // // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$indicador", 5] }
                                    ]
                                }
                                , "then": "#abd9e9"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 5] }
                                        , { "$lte": ["$indicador", 10] }
                                    ]
                                }
                                , "then": "#fde0ef"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 10] }
                                        , { "$lte": ["$indicador", 15] }
                                    ]
                                }
                                , "then": "#de77ae"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 15] }
                                        , { "$lte": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "#8e0152"
                            },

                            //--ultimo
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "#2f75b5"
                            }

                        ],
                        "default": "#000000"
                    }
                }
                ,"rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$lte": ["$indicador", 5] }
                                    ]
                                }
                                , "then": "#A-[0 - 5] Toneladas Ha"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 5] }
                                        , { "$lte": ["$indicador", 10] }
                                    ]
                                }
                                , "then": "B-(5 - 10] Toneladas Ha"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 10] }
                                        , { "$lte": ["$indicador", 15] }
                                    ]
                                }
                                , "then": "C-(10 - 15] Toneladas Ha"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 15] }
                                        , { "$lte": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "D-(15 - 20] Toneladas Ha"
                            },

                            //--ultimo
                            {
                                "case": {
                                    "$and": [
                                        { "$gt": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "E-( > 20] Toneladas Ha"
                            }

                        ],
                        "default": "otro"
                    }
                }

            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": { "$toObjectId":"$data_labores.cartography.features._id"}
                , "cartography_geometry": "$data_labores.cartography.features.geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {
                    "Lote": "$data_labores.lote",
                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color"

                    , "Toneladas Ha": { "$concat": [{ "$toString": "$indicador" }, " TON Ha"] }//🚩EDITAR🚩
                }

            }
        }







    ]
)
