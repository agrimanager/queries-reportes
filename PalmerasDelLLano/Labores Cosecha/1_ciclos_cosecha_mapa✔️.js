[

    { "$limit": 1 },

    {
        "$project": {
            "datos": [
                {
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin",
                    "today": "$today"
                }
            ]
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


    , {
        "$lookup": {
            "from": "tasks",
            "as": "data_labores",
            "let": {

                "busqueda_inicio": "$Busqueda inicio",
                "busqueda_fin": "$Busqueda fin",
                "today": "$today"
            },
            "pipeline": [

                { "$unwind": "$cartography.features" },
                {
                    "$match": {
                        "cartography.features.properties.type": "lot"
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$cartography.features.properties.name", "no existe"] } } },

                {
                    "$lookup": {
                        "from": "activities",
                        "localField": "activity",
                        "foreignField": "_id",
                        "as": "activity"
                    }
                },
                { "$unwind": "$activity" },

                {
                    "$match": {
                        "activity.name": {
                            "$regex": "Cosecha"
                        }
                    }
                },

                {
                    "$addFields": {
                        "Fecha inicio": { "$max": "$when.start" },
                        "Fecha fin": { "$max": "$when.finish" }
                    }
                },

                {
                    "$group": {
                        "_id": {
                            "lote": "$lote"
                        }
                        , "fecha_max": { "$max": "$Fecha fin" }
                        , "data": { "$push": "$$ROOT" }
                    }
                },

                { "$unwind": "$data" },

                {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data",
                                { "fecha_max": "$fecha_max" }
                            ]
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$Fecha fin", "$fecha_max"]
                        }
                    }
                },

                {
                    "$addFields": {
                        "dias de ciclo": {
                            "$floor": {
                                "$divide": [{ "$subtract": ["$$today", "$fecha_max"] }, 86400000]
                            }
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_labores",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "indicador": "$data_labores.dias de ciclo"
        }
    }


    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$lte": ["$indicador", 1] }
                                ]
                            }
                            , "then": "#e0f3f8"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 2] }
                                    , { "$lt": ["$indicador", 10] }
                                ]
                            }
                            , "then": "#66ff33"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 10] }
                                    , { "$lt": ["$indicador", 13] }
                                ]
                            }
                            , "then": "#ffff00"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 13] }
                                    , { "$lt": ["$indicador", 16] }
                                ]
                            }
                            , "then": "#fdae61"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 16] }
                                ]
                            }
                            , "then": "#d60000"
                        }

                    ],
                    "default": "#000000"
                }
            }
            ,"rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$lte": ["$indicador", 1] }
                                ]
                            }
                            , "then": "A-[<= 1] Dias"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 2] }
                                    , { "$lt": ["$indicador", 10] }
                                ]
                            }
                            , "then": "B-[2 - 9] Dias"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 10] }
                                    , { "$lt": ["$indicador", 13] }
                                ]
                            }
                            , "then": "C-[10 - 12] Dias"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 13] }
                                    , { "$lt": ["$indicador", 16] }
                                ]
                            }
                            , "then": "D-[13 - 15] Dias"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 16] }
                                ]
                            }
                            , "then": "E-( > 15) Dias"
                        }

                    ],
                    "default": "otro"
                }
            }


        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": { "$toObjectId":"$data_labores.cartography.features._id"}
            , "cartography_geometry": "$data_labores.cartography.features.geometry"

            , "color": "$color"
            , "rango": "$rango"
        }
    }


    , {
        "$project": {

            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",

            "properties": {
                "Lote": "$data_labores.lote",
                "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                "color": "$color"

                , "Dias": { "$concat": [{ "$toString": "$indicador" }, " Dias"] }
            }

        }
    }








]
