db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "cartography",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": "trees"
                        }
                    },



                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": "si"
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "arbol_erradicado": { "$ifNull": ["$arbol.properties.custom.Erradicada.value", false] } } },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0
                        }
                    },


                    {
                        "$addFields": {
                            "arbol_erradicado": {
                                "$cond": {
                                    "if": { "$eq": ["$arbol_erradicado", true] },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    },

                    {
                        "$project": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "linea": "$linea",
                            "arbol": "$arbol"

                            , "arbol_erradicado": "$arbol_erradicado"
                        }
                    }

                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "rgDate": "$rgDate"

                                , "finca": "$finca"
                                , "bloque": "$bloque"
                                , "lote": "$lote"
                            }
                            , "total_arboles": { "$sum": 1 }
                            , "total_arboles_erradicados": { "$sum": "$arbol_erradicado" }
                        }
                    }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id"
                                    ,
                                    {
                                        "total_arboles": "$total_arboles"
                                        , "total_arboles_erradicados": "$total_arboles_erradicados"
                                    }

                                ]
                            }
                        }
                    }





                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        , {
            "$addFields": {
                "total_arboles_activos": {
                    "$subtract": [
                        "$total_arboles",
                        "$total_arboles_erradicados"
                    ]
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_registrodiariopolinizadorv2",
                "as": "data_polinizacion",
                "let": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                },

                "pipeline": [
                    {
                        "$addFields": {
                            "variable_cartografia": "$Finca"//🚩editar : 0
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "variable_cartografia_oid": { "$toObjectId": "$variable_cartografia.features._id" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "variable_cartografia_oid",
                            "foreignField": "_id",
                            "as": "data_cartografia_actual"
                        }
                    },

                    { "$unwind": "$data_cartografia_actual" },


                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$data_cartografia_actual.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    ["$variable_cartografia_oid"]
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "Point": 0
                            , "Finca": 0 //🚩editar : 0
                            , "data_cartografia_actual": 0
                        }
                    }



                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca"
                                , "bloque": "$bloque"
                                , "lote": "$lote"

                                , "arbol": "$arbol"
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "finca": "$_id.finca"
                                , "bloque": "$_id.bloque"
                                , "lote": "$_id.lote"

                                // ,"arbol":"$arbol"
                            }
                            , "total_arboles_hibridos": { "$sum": 1 }
                        }
                    }



                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$_id.finca"] },
                                    { "$eq": ["$$bloque", "$_id.bloque"] },
                                    { "$eq": ["$$lote", "$_id.lote"] }
                                ]
                            }
                        }
                    }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id"
                                    ,
                                    {
                                        "total_arboles_hibridos": "$total_arboles_hibridos"
                                    }

                                ]
                            }
                        }
                    }


                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_polinizacion",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "total_arboles_hibridos": { "$ifNull": ["$data_polinizacion.total_arboles_hibridos", 0] }
            }
        }

        , {
            "$project": {
                "data_polinizacion": 0
            }
        }



        , {
            "$addFields": {
                "total_arboles_guinenses": {
                    "$subtract": [
                        "$total_arboles_activos",
                        "$total_arboles_hibridos"
                    ]
                }
            }
        }



    ]
)
