db.form_controldeplagas.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },



        {
            "$addFields": {
                "finca_id": {
                    "$arrayElemAt": ["$split_path_padres_oid", 0]
                }
            }
        },



        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0
            }
        },

        { "$match": { "Plagas": { "$nin": ["Palma sin Plaga", ""] } } },


        {
            "$project": {
                "rgDate día": 0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,

                "uDate día": 0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },


        {
            "$addFields": {
                "nombre_maestro_principal": "Plagas_"
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }


        , {
            "$addFields": {
                "nombre_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": {
                                            "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                { "$strLenCP": "$$dataKV.k" }]
                                        },
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "nombre_mestro_enlazado": { "$arrayElemAt": ["$nombre_mestro_enlazado", 0] }
            }
        }
        , {
            "$addFields": {
                "nombre_mestro_enlazado": { "$ifNull": ["$nombre_mestro_enlazado", ""] }
            }
        }




        , {
            "$addFields": {
                "valor_mestro_enlazado": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }
        , {
            "$unwind": {
                "path": "$valor_mestro_enlazado",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "nombre_maestro_principal": 0,
                "num_letras_nombre_maestro_principal": 0,
                "nombre_mestro_enlazado": 0
            }
        }

        , {
            "$addFields": {
                "instar": { "$ifNull": ["$valor_mestro_enlazado", ""] }
            }
        }

        , {
            "$project": {
                "valor_mestro_enlazado": 0
            }
        },




        {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "finca_id": "$finca_id"
                },
                "pipeline": [
                    { "$match": { "properties.type": "lot" } },
                    {
                        "$addFields": {
                            "ruta": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "finca": {
                                "$arrayElemAt": ["$ruta", 0]
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "finca": {
                                "$toObjectId": "$finca"
                            }
                        }
                    },
                    {
                        "$match": {
                            "$expr": {
                                "$eq": ["$finca", "$$finca_id"]
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "lote": "$properties.name",
                                "num_arboles": "$properties.custom.num_arboles.value",
                                "area": "$properties.custom.area.value"
                            }
                        }
                    },
                    {
                        "$group": {
                            "_id": "$_id.finca",
                            "num_arboles": { "$sum": "$_id.num_arboles" },
                            "area_total": { "$sum": "$_id.area" }
                        }
                    },
                    {
                        "$project": {
                            "_id": 0,
                            "num_arboles": 1,
                            "area_total": 1
                        }
                    }
                ],
                "as": "info_finca"
            }
        },

        { "$unwind": "$info_finca" },

        {
            "$addFields": {
                "arboles_x_finca": "$info_finca.num_arboles",
                "area_total": "$info_finca.area_total"
            }
        },

        {
            "$project": {
                "info_finca": 0
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "arbol": "$arbol",
                    "plagas": "$Plagas"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "plagas": "$_id.plagas"
                },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "num_palmas_x_plaga_y_finca": "$count" }
                    ]
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "arbol": "$arbol"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "num_palmas_con_plagas_total_x_finca": "$count" }
                    ]
                }
            }
        },


        //---instar
        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "arbol": "$arbol",
                    "plagas": "$Plagas"

                    ,"instar": "$instar"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "plagas": "$_id.plagas"

                    ,"instar": "$_id.instar"
                },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "num_palmas_x_plagas_x_instar_y_finca": "$count" }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "densidad_x_finca": {
                    "$cond": {
                        "if": { "$gt": ["$area_total", 0] },
                        "then": {
                            "$divide": [
                                "$arboles_x_finca",
                                "$area_total"
                            ]
                        },
                        "else": 0
                    }
                }
            }
        },


        {
            "$addFields": {
                "Incidencia x plaga y finca": {
                    "$cond": {
                        "if": { "$gt": ["$densidad_x_finca", 0] },
                        "then": {
                            "$divide": [
                                "$num_palmas_x_plaga_y_finca",
                                "$densidad_x_finca"
                            ]
                        },
                        "else": 0
                    }
                },

                "Incidencia plaga total x finca": {
                    "$cond": {
                        "if": { "$gt": ["$densidad_x_finca", 0] },
                        "then": {
                            "$divide": [
                                "$num_palmas_con_plagas_total_x_finca",
                                "$densidad_x_finca"
                            ]
                        },
                        "else": 0
                    }
                }

                ,"Incidencia x plaga x instar y finca": {
                    "$cond": {
                        "if": { "$gt": ["$densidad_x_finca", 0] },
                        "then": {
                            "$divide": [
                                "$num_palmas_x_plagas_x_instar_y_finca",
                                "$densidad_x_finca"
                            ]
                        },
                        "else": 0
                    }
                }
            }
        }
    ]

)
