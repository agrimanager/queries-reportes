db.form_registrodiariopolinizadorv2.aggregate(
    [


        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "FincaID": ObjectId("63345e631e87fb4bf7fb5e39"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------

        {
            "$addFields": {
                "variable_cartografia": "$Finca"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote_area": { "$ifNull": ["$lote.properties.custom.area.value", 0] } } },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$addFields": {
                "arbol_arr": {
                    "$split": ["$arbol", "-"]
                }
            }
        },
        {
            "$addFields": {
                "linea": {
                    "$cond": {
                        "if": { "$eq": ["$linea", "no existe"] },
                        "then": {
                            "$concat": [
                                "$bloque",
                                "-",
                                { "$arrayElemAt": ["$arbol_arr", 0] },
                                "-",
                                { "$arrayElemAt": ["$arbol_arr", -2] }
                            ]
                        },
                        "else": "$linea"
                    }
                }
            }
        },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Finca": 0
                , "Primera Aplicacion": 0

                , "Formula": 0
                , "uid": 0
                , "arbol_arr": 0
            }
        }





        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$rgDate" } },
                "num_semana": { "$week": { "date": "$rgDate" } }

            }
        }

        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
                , "Hora": { "$dateToString": { "format": "%H", "date": "$rgDate", "timezone": "America/Bogota" } }
                , "Fecha_Hora_Txt": { "$dateToString": { "format": "%Y-%m-%d:%H", "date": "$rgDate", "timezone": "America/Bogota" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }



        , {
            "$addFields": {
                "Inflorescencias Buenas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Buenas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Buenas", "chars": "," } },
                        "else": "$Inflorescencias Buenas"
                    }
                }
                , "Inflorescencias Pasadas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Pasadas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Pasadas", "chars": "," } },
                        "else": "$Inflorescencias Pasadas"
                    }
                }
            }
        }
        , {
            "$addFields": {
                "Inflorescencias Buenas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Buenas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Buenas", "chars": " " } },
                        "else": "$Inflorescencias Buenas"
                    }
                }
                , "Inflorescencias Pasadas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Pasadas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Pasadas", "chars": " " } },
                        "else": "$Inflorescencias Pasadas"
                    }
                }
            }
        }
        , {
            "$addFields": {
                "Inflorescencias Buenas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Buenas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Buenas", "chars": "_" } },
                        "else": "$Inflorescencias Buenas"
                    }
                }
                , "Inflorescencias Pasadas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Pasadas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Pasadas", "chars": "_" } },
                        "else": "$Inflorescencias Pasadas"
                    }
                }
            }
        }
        , {
            "$addFields": {
                "Inflorescencias Buenas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Buenas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Buenas", "chars": "-" } },
                        "else": "$Inflorescencias Buenas"
                    }
                }
                , "Inflorescencias Pasadas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Pasadas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Pasadas", "chars": "-" } },
                        "else": "$Inflorescencias Pasadas"
                    }
                }
            }
        }
        , {
            "$addFields": {
                "Inflorescencias Buenas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Buenas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Buenas", "chars": "*" } },
                        "else": "$Inflorescencias Buenas"
                    }
                }
                , "Inflorescencias Pasadas": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Inflorescencias Pasadas" }, "string"] },
                        "then": { "$trim": { "input": "$Inflorescencias Pasadas", "chars": "*" } },
                        "else": "$Inflorescencias Pasadas"
                    }
                }
            }
        }



        , {
            "$addFields": {
                "Inflorescencias Buenas": { "$toDouble": { "$ifNull": ["$Inflorescencias Buenas", 0] } }
                , "Inflorescencias Pasadas": { "$toDouble": { "$ifNull": ["$Inflorescencias Pasadas", 0] } }
                , "Segunda Aplicacion": { "$toDouble": { "$ifNull": ["$Segunda Aplicacion", 0] } }
                , "Tercera Aplicacion": { "$toDouble": { "$ifNull": ["$Tercera Aplicacion", 0] } }
            }
        }


        , {
            "$addFields": {
                "TOTAL Flores Polinizadas": {
                    "$sum": [
                        "$Inflorescencias Buenas",
                        "$Inflorescencias Pasadas",
                        "$Segunda Aplicacion",
                        "$Tercera Aplicacion"

                    ]
                }
            }
        }



        //new
        , {
            "$group": {

                "_id": {

                    "supervisor": "$supervisor"
                    , "fecha": "$Fecha_Txt"

                    , "finca": "$finca"

                }
                , "data": { "$push": "$$ROOT" }
                , "min_fecha": { "$min": "$rgDate" }
                , "max_fecha": { "$max": "$rgDate" }


            }
        }


        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "min_fecha": "$min_fecha",
                            "max_fecha": "$max_fecha"
                        }
                    ]
                }
            }
        }

        //---restar dias
        , {
            "$addFields": {
                "horas_supervisor_x_dia": {
                    "$multiply": [
                        {

                            "$divide": [
                                {
                                    "$subtract": [
                                        "$max_fecha",
                                        "$min_fecha"
                                    ]
                                },
                                86400000
                            ]

                        }
                        , 24
                    ]
                }
            }
        }

        //---redondear hacia abajo
        , {
            "$addFields": {
                "horas_supervisor_x_dia": { "$divide": [{ "$subtract": [{ "$multiply": ["$horas_supervisor_x_dia", 100] }, { "$mod": [{ "$multiply": ["$horas_supervisor_x_dia", 100] }, 1] }] }, 100] }
            }
        }





    ]


    , { allowDiskUse: true }
) 
