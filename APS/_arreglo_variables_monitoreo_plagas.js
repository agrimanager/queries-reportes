
db.form_monitoreodeplagas.updateMany(
    {},
    {
        $rename: {
            "NUM INSECTOS": "MARCEÑO NUM INSECTOS",
            "DAÑO FRESCO EN FRUTOS": "MARCEÑO DAÑO FRESCO EN FRUTOS",
            "DAÑO FRESCO EN HOJAS": "MARCEÑO DAÑO FRESCO EN HOJAS",
            "N INDIVIDUOS": "MONALONION N INDIVIDUOS",
            "HUEVOS": "MONALONION HUEVOS",
            "NINFAS": "MONALONION NINFAS",
            "ADULTOS": "MONALONION ADULTOS",
            "DAÑO FRESCO RAMAS": "MONALONION DAÑO FRESCO RAMAS",
            "DAÑO FRESCO FRUTOS": "MONALONION DAÑO FRESCO FRUTOS",
            "RAMAS INFESTADAS": "BOMBACOCUS RAMAS INFESTADAS",
            "INDIVIDUOS": "BOMBACOCUS INDIVIDUOS",
            "FRUTOS AFECTADOS": "CERCOSPORA FRUTOS AFECTADOS",
            "HOJAS AFECTADAS": "CERCOSPORA HOJAS AFECTADAS",
        }
    }
)
