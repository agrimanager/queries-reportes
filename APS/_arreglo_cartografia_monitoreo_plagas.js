
//=====1) cambiar Point.farm

// var cursor = db.form_monitoreodeplagas.aggregate(

//     {
//         $match: {
//             "Arbol.features.path": {
//                 // $regex: `^,6041268be9bff44ca762b43d,607d95d3f577a1fae324154c,` ////----bloque2
//                 $regex: `^,6041268be9bff44ca762b43d,607d95d3f577a1fae3241549,` ////----bloque3
//             }
//         }
//     }

//     , {
//         "$group": {
//             "_id": null,
//             "ids": {
//                 "$push": "$_id"
//             }
//         }
//     }

// )


// // cursor

// cursor.forEach(i => {
//     db.form_monitoreodeplagas.updateMany(
//         {
//             "_id": {
//                 "$in": i.ids
//             }
//         },
//         {
//             $set: {
//                 // "Point.farm":"623dbeba07dea074560da261"////----bloque2
//                 "Point.farm":"623dbedc07dea074560da262"////----bloque3
//             }
//         }
//     )
// })





//=====2) cambiar path Feature
var cursor = db.form_monitoreodeplagas.aggregate(

    {
        $match: {
            //"Arbol.features.path": {
            "Arbol.path": {
                // $regex: `^,6041268be9bff44ca762b43d,607d95d3f577a1fae324154c,` ////----bloque2
                $regex: `^,6041268be9bff44ca762b43d,607d95d3f577a1fae3241549,` ////----bloque3
            }
        }
    }

    , {
        "$addFields": {
            "variable_cartografia": "$Arbol" //🚩editar
        }
    },
    { "$unwind": "$variable_cartografia.features" }

    //test
    // , { $limit: 100 }




    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartography",
            "let": {
                "arbol_nombre": "$variable_cartografia.features.properties.name"
            },
            "pipeline": [

                {
                    $match: {
                        path: {
                            // $regex: `^,623dbeba07dea074560da261,` ////----bloque2
                            $regex: `^,623dbedc07dea074560da262,` ////----bloque3
                        }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$arbol_nombre", "$properties.name"] }
                            ]
                        }
                    }
                }
            ]
        }
    }

    ,{$unwind: "$data_cartography"}


    , {
        "$addFields": {
            "path_nuevo": "$data_cartography.path"
        }
    }


    , {
        "$project": {
            "path_nuevo": 1
        }
    }



)
// .count()

// cursor


cursor.forEach(i=>{
    db.form_monitoreodeplagas.update(
        {
            _id:i._id
        },{
            $set:{
                "Arbol.path":i.path_nuevo
            }
        }
    )
})
