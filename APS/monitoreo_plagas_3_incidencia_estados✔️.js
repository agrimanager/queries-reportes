[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },


    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Arbol": 0
            , "Point": 0
            , "Formula": 0
        }
    }



    , {
        "$addFields": {
            "array_data": [


                {
                    "plaga": "Acaro"
                    , "estado": "Huevos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO HUEVOS" }, 0] }
                },
                {
                    "plaga": "Acaro"
                    , "estado": "Ninfas"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO NINFAS" }, 0] }
                },
                {
                    "plaga": "Acaro"
                    , "estado": "Adultos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO ADULTOS" }, 0] }
                },
                {
                    "plaga": "Acaro"
                    , "estado": "Individuos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO NUMERO INDIVIDUOS" }, 0] }
                },

                {
                    "plaga": "Marceño"
                    , "estado": "Insectos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO NUM INSECTOS" }, 0] }
                },
                {
                    "plaga": "Marceño"
                    , "estado": "Daño Fresco Frutos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO DAÑO FRESCO EN FRUTOS" }, 0] }
                },
                {
                    "plaga": "Marceño"
                    , "estado": "Daño Fresco Hojas"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO DAÑO FRESCO EN HOJAS" }, 0] }
                },

                {
                    "plaga": "Coleopteros"
                    , "estado": "COMPSUS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$COMPSUS" }, 0] }
                },
                {
                    "plaga": "Coleopteros"
                    , "estado": "CHRYSOMELIDOS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$CHRYSOMELIDOS" }, 0] }
                },
                {
                    "plaga": "Coleopteros"
                    , "estado": "PANDELETEIUS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$PANDELETEIUS" }, 0] }
                },

                {
                    "plaga": "Lepidopteros"
                    , "estado": "LEPIDOPTEROS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$LEPIDOPTEROS" }, 0] }
                },


                {
                    "plaga": "Monalonion"
                    , "estado": "Huevos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION HUEVOS" }, 0] }
                },
                {
                    "plaga": "Monalonion"
                    , "estado": "Ninfas"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION NINFAS" }, 0] }
                },
                {
                    "plaga": "Monalonion"
                    , "estado": "Adultos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION ADULTOS" }, 0] }
                },
                {
                    "plaga": "Monalonion"
                    , "estado": "Individuos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION N INDIVIDUOS" }, 0] }
                },


                {
                    "plaga": "Monalonion"
                    , "estado": "Daño Fresco Frutos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION DAÑO FRESCO FRUTOS" }, 0] }
                },
                {
                    "plaga": "Monalonion"
                    , "estado": "Daño Fresco Ramas"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION DAÑO FRESCO RAMAS" }, 0] }
                },


                {
                    "plaga": "Trips"
                    , "estado": "Trips"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$Trips" }, 0] }
                },

                {
                    "plaga": "Escama"
                    , "estado": "ESCAMA"
                    , "cantidad": {
                        "$ifNull": [
                            {
                                "$cond": { "if": { "$eq": ["$ESCAMA", "SI"] }, "then": 1, "else": 0 }
                            }
                            , 0]
                    }
                },


                {
                    "plaga": "Minador"
                    , "estado": "MINADOR minas activas"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$MINADOR minas activas" }, 0] }
                },


                {
                    "plaga": "Mosca del ovario"
                    , "estado": "TOTAL FRUTOS INFLORESCENCIA"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$TOTAL FRUTOS INFLORESCENCIA" }, 0] }
                },
                {
                    "plaga": "Mosca del ovario"
                    , "estado": "TOTAL FRUTOSAFECTADOS POR INFLORESCENCIA"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$TOTAL FRUTOSAFECTADOS POR INFLORESCENCIA" }, 0] }
                },


                {
                    "plaga": "Bombacocus"
                    , "estado": "RAMAS INFESTADAS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$BOMBACOCUS RAMAS INFESTADAS" }, 0] }
                },
                {
                    "plaga": "Bombacocus"
                    , "estado": "Individuos"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$BOMBACOCUS INDIVIDUOS" }, 0] }
                },


                {
                    "plaga": "Plagas Curentenarias"
                    , "estado": "STENOMA"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$STENOMA" }, 0] }
                },
                {
                    "plaga": "Plagas Curentenarias"
                    , "estado": "HEILIPUS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$HEILIPUS" }, 0] }
                },


                {
                    "plaga": "ANTRACNOSIS"
                    , "estado": "ANTRACNOSIS"
                    , "cantidad": {
                        "$ifNull": [
                            {
                                "$cond": { "if": { "$eq": ["$ANTRACNOSIS", "SI"] }, "then": 1, "else": 0 }
                            }
                            , 0]
                    }
                },

                {
                    "plaga": "Chancro"
                    , "estado": "CHANCRO"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$CHANCRO" }, 0] }
                },

                {
                    "plaga": "MARCHITAMIENTO"
                    , "estado": "MARCHITAMIENTO"
                    , "cantidad": {
                        "$ifNull": [
                            {
                                "$cond": { "if": { "$eq": ["$MARCHITAMIENTO", "SI"] }, "then": 1, "else": 0 }
                            }
                            , 0]
                    }
                },

                {
                    "plaga": "Cercospora"
                    , "estado": "FRUTOS AFECTADOS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$CERCOSPORA FRUTOS AFECTADOS" }, 0] }
                },
                {
                    "plaga": "Cercospora"
                    , "estado": "HOJAS AFECTADAS"
                    , "cantidad": { "$ifNull": [{ "$toDouble": "$CERCOSPORA HOJAS AFECTADAS" }, 0] }
                }


            ]
        }
    }


    , {
        "$addFields":
        {
            "array_data": {
                "$filter": {
                    "input": "$array_data",
                    "as": "data_item",
                    "cond": {
                        "$ne": ["$$data_item.cantidad", 0]
                    }
                }
            }
        }
    }


    , {
        "$unwind": {
            "path": "$array_data",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$project": {
            "finca": "$finca"
            , "bloque": "$bloque"
            , "lote": "$lote"
            , "linea": "$linea"
            , "arbol": "$arbol"

            , "plaga": { "$ifNull": ["$array_data.plaga", "_SIN_PLAGA"] }
            , "estado": { "$ifNull": ["$array_data.estado", "_SIN_ESTADO"] }
            , "cantidad": { "$ifNull": ["$array_data.cantidad", 0] }

            , "supervisor": "$supervisor"
            , "capture": "$capture"
            , "fecha txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }



    , {
        "$group": {
            "_id": {
                "lote": "$lote",
                "planta": "$arbol"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }




    , {
        "$group": {
            "_id": {
                "plaga": "$plaga",
                "lote": "$lote",
                "planta": "$arbol"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "plaga": "$_id.plaga",
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote_x_plaga": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"
                    }
                ]
            }
        }
    }




    , {
        "$match": {
            "plaga": { "$ne": "_SIN_PLAGA" }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"

                , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                , "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"

                , "plaga": "$plaga"
                , "estado": "$estado"
            }
            , "sum_cantidad_lote_plaga_estado": { "$sum": "$cantidad" }

        }
    }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "sum_cantidad_lote_plaga_estado": "$sum_cantidad_lote_plaga_estado"
                    }
                ]
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"

                , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                , "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"

                , "plaga": "$plaga"

            }
            , "sum_cantidad_lote_plaga": { "$sum": "$sum_cantidad_lote_plaga_estado" }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , { "$unwind": "$data" }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "sum_cantidad_lote_plaga": "$sum_cantidad_lote_plaga"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {

            "pct_incidencia_lote_plaga": {
                "$cond": {
                    "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{ "$divide": ["$plantas_dif_censadas_x_lote_x_plaga", "$plantas_dif_censadas_x_lote"] }, 100]
                    }
                }
            }

            , "pct_incidencia_lote_plaga_estado": {
                "$cond": {
                    "if": { "$eq": ["$sum_cantidad_lote_plaga", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [{ "$divide": ["$sum_cantidad_lote_plaga_estado", "$sum_cantidad_lote_plaga"] }, 100]
                    }
                }
            }
        }
    }


]
