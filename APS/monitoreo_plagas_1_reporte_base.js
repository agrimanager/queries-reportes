
//----formulario con maestros numericos
db.form_monitoreodeplagas.aggregate(
    [

        // //=======CONIDICIONES DE FORM NUEVO
        // {
        //     "$match": {
        //         "ACARO HUEVOS": { "$exists": true }
        //     }
        // },

        //=====CARTOGRAFIA

        //--paso1 (cartografia-nombre variable y ids)
        {
            "$addFields": {
                "variable_cartografia": "$Arbol" //🚩editar
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        //--paso2 (cartografia-cruzar informacion)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        // //--paso3 (cartografia-obtener informacion)

        //--finca
        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        //--bloque
        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        //--linea
        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        //--arbol
        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Arbol": 0
                , "Point": 0
                , "Formula": 0
            }
        }


        //===========PLANTAS DIF CENSADAS X LOTE
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }



        //=======ESTANDARIZAR DATOS

        , {
            "$addFields": {
                "array_data": [

                    //--obj_estandar
                    //--XXXXXXX
                    // {
                    //     "plaga": "Xxxxxxxx"
                    //     , "estado": "XXXXXXX"
                    //     , "cantidad": { "$ifNull": [{ "$toDouble": "$XXXXXXX" }, 0] }
                    // },

                    //--ACARO
                    {
                        "plaga": "Acaro"
                        , "estado": "Huevos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO HUEVOS" }, 0] }
                    },
                    {
                        "plaga": "Acaro"
                        , "estado": "Ninfas"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO NINFAS" }, 0] }
                    },
                    {
                        "plaga": "Acaro"
                        , "estado": "Adultos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO ADULTOS" }, 0] }
                    },
                    {
                        "plaga": "Acaro"
                        , "estado": "Individuos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$ACARO NUMERO INDIVIDUOS" }, 0] }
                    },

                    //--MARCEÑO
                    {
                        "plaga": "Marceño"
                        , "estado": "Insectos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO NUM INSECTOS" }, 0] }
                    },
                    {
                        "plaga": "Marceño"
                        , "estado": "Daño Fresco Frutos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO DAÑO FRESCO EN FRUTOS" }, 0] }
                    },
                    {
                        "plaga": "Marceño"
                        , "estado": "Daño Fresco Hojas"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MARCEÑO DAÑO FRESCO EN HOJAS" }, 0] }
                    },

                    //--COLEOPTEROS
                    {
                        "plaga": "Coleopteros"
                        , "estado": "COMPSUS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$COMPSUS" }, 0] }
                    },
                    {
                        "plaga": "Coleopteros"
                        , "estado": "CHRYSOMELIDOS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$CHRYSOMELIDOS" }, 0] }
                    },
                    {
                        "plaga": "Coleopteros"
                        , "estado": "PANDELETEIUS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$PANDELETEIUS" }, 0] }
                    },

                    //--LEPIDOPTEROS
                    {
                        "plaga": "Lepidopteros"
                        , "estado": "LEPIDOPTEROS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$LEPIDOPTEROS" }, 0] }
                    },


                    //--Monalonion
                    {
                        "plaga": "Monalonion"
                        , "estado": "Huevos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION HUEVOS" }, 0] }
                    },
                    {
                        "plaga": "Monalonion"
                        , "estado": "Ninfas"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION NINFAS" }, 0] }
                    },
                    {
                        "plaga": "Monalonion"
                        , "estado": "Adultos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION ADULTOS" }, 0] }
                    },
                    {
                        "plaga": "Monalonion"
                        , "estado": "Individuos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION N INDIVIDUOS" }, 0] }
                    },


                    {
                        "plaga": "Monalonion"
                        , "estado": "Daño Fresco Frutos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION DAÑO FRESCO FRUTOS" }, 0] }
                    },
                    {
                        "plaga": "Monalonion"
                        , "estado": "Daño Fresco Ramas"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MONALONION DAÑO FRESCO RAMAS" }, 0] }
                    },


                    //--Trips
                    {
                        "plaga": "Trips"
                        , "estado": "Trips"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$Trips" }, 0] }
                    },

                    //--ESCAMA (desicion SI/NO)⚠️
                    {
                        "plaga": "Escama"
                        , "estado": "ESCAMA"
                        , "cantidad": {
                            "$ifNull": [
                                {
                                    "$cond": { "if": { "$eq": ["$ESCAMA", "SI"] }, "then": 1, "else": 0 }
                                }
                                , 0]
                        }
                    },


                    //--Minador
                    {
                        "plaga": "Minador"
                        , "estado": "MINADOR minas activas"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$MINADOR minas activas" }, 0] }
                    },


                    //--MOSCA DEL OVARIO
                    {
                        "plaga": "Mosca del ovario"
                        , "estado": "TOTAL FRUTOS INFLORESCENCIA"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$TOTAL FRUTOS INFLORESCENCIA" }, 0] }
                    },
                    {
                        "plaga": "Mosca del ovario"
                        , "estado": "TOTAL FRUTOSAFECTADOS POR INFLORESCENCIA"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$TOTAL FRUTOSAFECTADOS POR INFLORESCENCIA" }, 0] }
                    },


                    //--BOMBACOCUS
                    {
                        "plaga": "Bombacocus"
                        , "estado": "RAMAS INFESTADAS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$BOMBACOCUS RAMAS INFESTADAS" }, 0] }
                    },
                    {
                        "plaga": "Bombacocus"
                        , "estado": "Individuos"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$BOMBACOCUS INDIVIDUOS" }, 0] }
                    },


                    //--PLAGAS CUARENTENARIAS
                    {
                        "plaga": "Plagas Curentenarias"
                        , "estado": "STENOMA"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$STENOMA" }, 0] }
                    },
                    {
                        "plaga": "Plagas Curentenarias"
                        , "estado": "HEILIPUS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$HEILIPUS" }, 0] }
                    },


                    //--ANTRACNOSIS (desicion SI/NO)⚠️
                    {
                        "plaga": "ANTRACNOSIS"
                        , "estado": "ANTRACNOSIS"
                        , "cantidad": {
                            "$ifNull": [
                                {
                                    "$cond": { "if": { "$eq": ["$ANTRACNOSIS", "SI"] }, "then": 1, "else": 0 }
                                }
                                , 0]
                        }
                    },

                    //--CHANCRO
                    {
                        "plaga": "Chancro"
                        , "estado": "CHANCRO"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$CHANCRO" }, 0] }
                    },

                    //MARCHITAMIENTO (desicion SI/NO)⚠️
                    {
                        "plaga": "MARCHITAMIENTO"
                        , "estado": "MARCHITAMIENTO"
                        , "cantidad": {
                            "$ifNull": [
                                {
                                    "$cond": { "if": { "$eq": ["$MARCHITAMIENTO", "SI"] }, "then": 1, "else": 0 }
                                }
                                , 0]
                        }
                    },

                    //--CERCOSPORA
                    {
                        "plaga": "Cercospora"
                        , "estado": "FRUTOS AFECTADOS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$CERCOSPORA FRUTOS AFECTADOS" }, 0] }
                    },
                    {
                        "plaga": "Cercospora"
                        , "estado": "HOJAS AFECTADAS"
                        , "cantidad": { "$ifNull": [{ "$toDouble": "$CERCOSPORA HOJAS AFECTADAS" }, 0] }
                    },


                ]
            }
        }


        //---filtrar datos
        , {
            "$addFields":
            {
                "array_data": {
                    "$filter": {
                        "input": "$array_data",
                        "as": "data_item",
                        "cond": {
                            "$ne": ["$$data_item.cantidad", 0]
                        }
                    }
                }
            }
        }

        // // //---test
        // // , {
        // //     $match: {
        // //         array_data: { $ne: [] }
        // //     }
        // // }


        ,{
            "$unwind": {
                "path": "$array_data",
                "preserveNullAndEmptyArrays": true
            }
        }


        ,{
            "$project": {
                "finca": "$finca"
                ,"bloque": "$bloque"
                ,"lote": "$lote"
                ,"linea": "$linea"
                ,"arbol": "$arbol"

                ,"plaga": { "$ifNull": [ "$array_data.plaga", "_SIN_PLAGA"] }
                ,"estado": { "$ifNull": [ "$array_data.estado", "_SIN_ESTADO"] }
                ,"cantidad": { "$ifNull": [ "$array_data.cantidad", 0] }

                ,"supervisor": "$supervisor"
                ,"capture": "$capture"
                ,"fecha txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate","timezone": "America/Bogota" } }
            }
        }





        //---PLANTAS CENSADAS X LOTE X PLAGA





    ]

)
