/*
reporte

--test_filtros_fechas_finca❌
--condicones base
-->>cartografia
--CRUCE DE TABLAS
--ESTANDARIZACION DE DATOS

--fechas
--condicones REGLAS DE NEGOCIO
*/


db.form_monitoreoenfermedades.aggregate(
    [
        //---❌
        //==================== TEST FILTRO DE FECHA
        // {
        //     "$match": {
        //         "$and": [
        //             {"rgDate": {"$gte": ISODate("2020-08-13T11:44:17.117-05:00")}},
        //             {"rgDate": {"$lte": ISODate("2020-09-15T11:44:17.117-05:00")}
        //             }
        //         ]
        //     }
        // },

        //==================== TEST FILTRO DE FINCA
        // {
        //     "$match": {
        //         "Point.farm": "5d26499b64f5b87ffc809ebf"
        //     }
        // },
        //====================
        //---❌


        //===== CONDICIONALES BASE

        // //--condiccion 2  
        // {
        //     "$match": {
        //         "PC": {
        //             "$in": ["Tiene"]
        //         }
        //     }
        // },


        //===== CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },


        // //====== ESTANDARIZACION DE DATOS
        {
            "$addFields": {
                "estado_pc": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$PC", "Tiene"] }, "then": "Con PC" }
                        ],
                        "default": "Sin PC"
                    }
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "arbol": "$arbol",
                    "estado_pc": "$estado_pc"
                }
                //,"cantidad":{"$sum":1}
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$sort": {
                "_id.arbol": 1,
                "_id.estado_pc": 1
            }
        },


        {
            "$group": {
                "_id": {
                    "arbol": "$_id.arbol",
                    // "estado_pc":"$estado_pc"
                }
                //,"cantidad":{"$sum":1}
                , "data": { "$push": "$$ROOT" }
            }
        },


        //---test
        // {
        //     "$match": {
        //         "_id.arbol": "3812-3-30-8"
        //     }
        // },

        {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data.data", 0] }
            }
        },

        {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }



        //====== CRUCES DE TABLAS


        // //condiccion 3

        , {
            "$lookup": {
                "from": "form_controldepc",
                "localField": "arbol",
                "foreignField": "Arbol.features.properties.name",
                "as": "info_lote"
            }
        },



        //==== CONDICIONES
        //--SIN CONTROL
        {
            "$addFields": {
                "estado_control_pc": {
                    "$cond": {
                        "if": { "$eq": ["$info_lote", []] },
                        "then": "Sin Control",
                        "else": "Con Control"
                    }
                }
            }
        },


        {
            "$addFields": {
                "estado_control_pc": {
                    "$cond": {
                        "if": { "$eq": ["$estado_control_pc", "Sin Control"] },
                        "then": "Sin Control",
                        "else": {
                            "$reduce": {
                                "input": "$info_lote.Control de PC",
                                "initialValue": "",
                                "in": "$$this"
                            }
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "estado_control_pc": {
                    "$cond": {
                        //"if": { "$eq": ["$estado_control_pc", "No Aplicada"] },
                        "if": { "$in": ["$estado_control_pc", ["No Aplicada", "Sin Control"]] },
                        "then": "Sin Control",
                        "else": "Con Control"
                    }
                }
            }
        },


        // {
        //     "$addFields": {
        //         "estado_control_pc": {
        //             "$cond": {
        //                 "if": { "$in": ["$estado_control_pc", ["No Aplicada","Sin Control"]] },
        //                 "then": "Sin Control",
        //                 "else": "Con Control"
        //             }
        //         }
        //     }
        // },




        // //---test
        // {
        //     "$match": {
        //         //"arbol": "3017-1-70-14"
        //         "arbol": "3505-14-31-17"

        //     }
        // },
        // //-----




        /*
        opciones:
        
        
        */
        {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$eq": ["$estado_PC", "Con PC"] }, { "$eq": ["$estado_control_PC", "Sin Control"] }]
                        },
                        "then": "opcion1",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$eq": ["$estado_PC", "Con PC"] }, { "$eq": ["$estado_control_PC", "Con Control"] }]
                                },
                                "then": "opcion2-3",
                                "else": "opcion4",
                            }
                        }
                    }
                }

            }
        }




    ]


)