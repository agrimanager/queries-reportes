[




    { "$limit": 1 },


    {
        "$lookup": {
            "from": "cartography_internal",
            "as": "data1",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [


                {
                    "$match": {
                        "type": "lines"
                    }
                }

                , {
                    "$unwind": {
                        "path": "$internal_features",
                        "preserveNullAndEmptyArrays": true
                    }
                }

                , {
                    "$addFields": {
                        "cantidad_arboles": {
                            "$ifNull": ["$internal_features.count_features", 0]
                        }
                    }
                }

                , {
                    "$project": {
                        "internal_features": 0,
                        "type": 0
                    }
                }



                , {
                    "$addFields": {
                        "split_path_oid": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": { "$map": { "input": "$split_path_oid", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },


                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                        "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                        "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                    }
                },


                {
                    "$addFields": {
                        "finca": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.type", "Farm"] },
                                "then": "$feature_1",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.type", "Farm"] },
                                        "then": "$feature_2",
                                        "else": "$feature_3"
                                    }
                                }
                            }
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },

                {
                    "$addFields": {
                        "finca": "$finca.name"
                    }
                },
                { "$unwind": "$finca" },


                {
                    "$addFields": {
                        "bloque": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.properties.type", "blocks"] },
                                "then": "$feature_1.properties.name",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.properties.type", "blocks"] },
                                        "then": "$feature_2.properties.name",
                                        "else": "$feature_3.properties.name"
                                    }
                                }
                            }
                        }
                    }
                },


                {
                    "$addFields": {
                        "lote": {
                            "$cond": {
                                "if": { "$eq": ["$feature_1.properties.type", "lot"] },
                                "then": "$feature_1.properties.name",
                                "else": {
                                    "$cond": {
                                        "if": { "$eq": ["$feature_2.properties.type", "lot"] },
                                        "then": "$feature_2.properties.name",
                                        "else": "$feature_3.properties.name"
                                    }
                                }
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "linea": "$name"
                    }
                }

                , {
                    "$project": {
                        "split_path": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "features_oid": 0,

                        "feature_1": 0,
                        "feature_2": 0,
                        "feature_3": 0,

                        "path": 0,
                        "name": 0,
                        "_id": 0
                    }
                }


                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }



            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data1"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


    , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartography",
            "let": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "variable_custom": "$properties.custom.Erradicada"
                    }
                },

                {
                    "$match": {
                        "properties.type": "trees"

                        , "variable_custom": { "$exists": true }
                        , "variable_custom.value": { "$ne": false }
                    }
                }



                , {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },

                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },

                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },



                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0
                    }
                },



                {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "linea": "$linea"
                        }
                        , "custom_cantidad": { "$sum": 1 }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$finca", "$_id.finca"] },
                                { "$eq": ["$$bloque", "$_id.bloque"] },
                                { "$eq": ["$$lote", "$_id.lote"] },
                                { "$eq": ["$$linea", "$_id.linea"] }
                            ]
                        }
                    }
                }



            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_cartography",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {

            "plantas_erradicadas_x_linea": { "$ifNull": ["$data_cartography.custom_cantidad", 0] }
        }
    }

    , {
        "$project": {
            "data_cartography": 0
        }
    }



    , {
        "$addFields": {
            "total_arboles_activos": {
                "$subtract": ["$cantidad_arboles", "$plantas_erradicadas_x_linea"]
            }
        }
    }

     , {
        "$lookup": {
            "from": "cartography",
            "as": "data_cartography",
            "let": {

                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "variable_custom": "$properties.custom.Resiembra"
                    }
                },

                {
                    "$match": {
                        "properties.type": "trees"

                        , "variable_custom": { "$exists": true }
                        , "variable_custom.value": { "$ne": false }
                    }
                }



                , {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                    }
                },

                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                    }
                },

                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },



                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0
                    }
                },



                {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "linea": "$linea"
                        }
                        , "custom_cantidad": { "$sum": 1 }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$finca", "$_id.finca"] },
                                { "$eq": ["$$bloque", "$_id.bloque"] },
                                { "$eq": ["$$lote", "$_id.lote"] },
                                { "$eq": ["$$linea", "$_id.linea"] }
                            ]
                        }
                    }
                }



            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_cartography",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {

            "plantas_resiembra_x_linea": { "$ifNull": ["$data_cartography.custom_cantidad", 0] }
        }
    }

    , {
        "$project": {
            "data_cartography": 0
        }
    }

    , {
        "$addFields": {
            "improductiva": ""
        }
    }

]
