//mapa
db.form_monitoreoenfermedades.aggregate(
    [

        //======== CARTOGRAFIA


        {
            "$addFields": { "Cartography": "$Arbol", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },


        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },


        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },


        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },


        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }

            }
        },

        {
            "$addFields": {
                "Cartography": "$lote"
            }
        },


        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },




        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0

                , "Arbol": 0
                , "Point": 0
                , "Formula": 0
            }
        },


        //====== ESTANDARIZACION DE DATOS

        //---Parte1 (estado_pc)
        {
            "$addFields": {
                "estado_pc": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$PC", "Tiene"] }, "then": "Con PC" }
                        ],
                        "default": "Sin PC"
                    }
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "arbol": "$arbol",
                    "estado_pc": "$estado_pc"
                }
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$sort": {
                "_id.arbol": 1,
                "_id.estado_pc": 1
            }
        },


        {
            "$group": {
                "_id": {
                    "arbol": "$_id.arbol",
                }
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data.data", 0] }
            }
        },

        {
            "$addFields": {
                "data": { "$arrayElemAt": ["$data", 0] }
            }
        }


        , {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }



        //====== CRUCES DE TABLAS
        //---Parte2 (control_pc)

        // , {
        //     "$lookup": {
        //         "from": "form_controldepc",
        //         "localField": "arbol",
        //         "foreignField": "Arbol.features.properties.name",
        //         "as": "info_control_pc"
        //     }
        // }


        , {
            "$lookup": {
                "from": "form_controldepc",
                "as": "info_control_pc",
                "let": {
                    "nombre_arbol": "$arbol",
                    "fecha_registro": "$rgDate"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_arbol", "$Arbol.features.properties.name"] },
                                ]
                            }
                        }
                    }
                    //---restar dias
                    , {
                        "$addFields": {
                            "total_restar_dias": {
                                "$divide": [
                                    {
                                        "$subtract": [
                                            "$rgDate",
                                            "$$fecha_registro"
                                        ]
                                    },
                                    86400000
                                ]
                            }
                        }
                    }
                    , {
                        "$match": {
                            "$expr": { "$lt": ["$total_restar_dias", 180] },
                        }
                    }
                ]
            }
        }


        // //---test 
        // , {
        //     "$addFields": {
        //         "size_num": { "$size": "$info_control_pc" }
        //     }
        // }


        // , {
        //     "$match": {
        //         "size_num": { $gt: 0 }
        //     }
        // }
        // //-----


        , {
            "$addFields": {
                "num_estado_control_pc": {
                    "$reduce": {
                        "input": "$info_control_pc.Control de PC",
                        "initialValue": {
                            "valor": "",
                            "cantidad": 0
                        },
                        "in": {
                            "valor": "$$this",
                            "cantidad": {
                                "$cond": {
                                    "if": { "$in": ["$$this", ["", "No Aplicada"]] },
                                    "then": { "$add": ["$$value.cantidad", 0] },
                                    "else": { "$add": ["$$value.cantidad", 1] }
                                }
                            }
                        }
                    }
                }
            }
        }




        //----Parte3
        //----Opciones de rango
        , {
            "$addFields": {
                "opcion": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Con PC"] }
                                        , { "$eq": ["$num_estado_control_pc.cantidad", 0] }
                                    ]
                                }
                                , "then": "Opcion 1"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Con PC"] }
                                        , { "$eq": ["$num_estado_control_pc.cantidad", 1] }
                                    ]
                                }
                                , "then": "Opcion 2"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Con PC"] }
                                        , { "$gt": ["$num_estado_control_pc.cantidad", 1] }
                                    ]
                                }
                                , "then": "Opcion 3"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Sin PC"] }
                                        , { "$gt": ["$num_estado_control_pc.cantidad", 0] }
                                    ]
                                }
                                , "then": "Opcion 4"
                            }
                        ],
                        "default": "sin_opcion"
                    }
                }
            }
        },

        {
            "$match": {
                "opcion": {
                    "$in": [
                        "Opcion 1"
                        , "Opcion 2"
                        , "Opcion 3"
                        , "Opcion 4"
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "control_pc": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$opcion", "Opcion 1"] }
                                , "then": "Sin Control PC"
                            }
                        ],
                        "default": "Con Control PC"
                    }
                }
            }
        },
        // //-----

        //=====FECHA

        //---info fechas
        {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                , "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }
        
        
        
        //----leyenda
        





        //=====COLOR
        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$estado_pc", "Con PC"] }
                                , { "$eq": ["$num_estado_control_pc.cantidad", 0] }
                            ]
                        },
                        "then": "#FF0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Con PC"] }
                                        , { "$eq": ["$num_estado_control_pc.cantidad", 1] }
                                    ]
                                },
                                "then": " #ffff00",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [
                                                { "$eq": ["$estado_pc", "Con PC"] }
                                                , { "$gt": ["$num_estado_control_pc.cantidad", 1] }
                                            ]
                                        },
                                        "then": " #008000",
                                        "else": "#1a73e8"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$estado_pc", "Con PC"] }
                                , { "$eq": ["$num_estado_control_pc.cantidad", 0] }
                            ]
                        },
                        "then": "A-(Palmas con PC sin control)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$eq": ["$estado_pc", "Con PC"] }
                                        , { "$eq": ["$num_estado_control_pc.cantidad", 1] }
                                    ]
                                },
                                "then": "B-(Palmas con PC y un control)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [
                                                { "$eq": ["$estado_pc", "Con PC"] }
                                                , { "$gt": ["$num_estado_control_pc.cantidad", 1] }
                                            ]
                                        },
                                        "then": "C-(Palmas con PC y más de un control)",
                                        "else": "D-(Palmas sin PC con controles)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        , {
            "$project": {
                "_id": "$elemnq",
                "idform": "$idform",
                "type": "Feature",
                "properties": {
                    "bloque": "$bloque",
                    "Lote": "$lote",
                    "arbol": "$arbol",
                    "Rango": "$rango",

                    "Mes": "$Mes_txt",
                    "PC": "$estado_pc",
                    "Control": "$control_pc",
                    "Num_Control": "$num_estado_control_pc.cantidad",

                    "color": "$color"
                },
                "geometry": "$Cartography.geometry"
            }
        }




        // // //---test
        // , {
        //     "$match": {
        //         "arbol": {
        //             $in: [
        //                 "3505-14-31-17" //sin ""
        //                 , "3017-1-70-14" //sin
        //                 , "3017-1-79-8"  //con
        //                 , "3812-4-36-15" //sin
        //                 , "2105-1-14I-16"//con2
        //                 , "3017-1-103-25"//sin y con
        //             ]
        //         }
        //     }
        // }
        // // //-----



    ]


)