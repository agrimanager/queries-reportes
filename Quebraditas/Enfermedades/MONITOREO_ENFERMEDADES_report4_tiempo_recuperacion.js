db.form_monitoreoenfermedades.aggregate(

    [
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-07-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                // "Busqueda fin": ISODate("2020-07-05T19:00:00.000-05:00"),
                "today": new Date
            }
        },
        //------------------------------------------------------------------

        //---match fechas
        //rgDate
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },



        //==============query


        {
            "$addFields": {
                "Busqueda inicio_menos_365_dias": {
                    "$subtract": [
                        "$Busqueda inicio",
                        { "$multiply": [365, 86400000] }
                    ]
                }
            }
        },





        //---match RECUPERADAS
        {
            "$match": {
                "Grado": "Recuperada"
            }
        },





        //=====CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0
            }
        },



        //=====CRUZAR CON MISMA TABLA
        {
            "$lookup": {
                "from": "form_monitoreoenfermedades",
                "as": "form_monitoreoenfermedades_AUX",
                "let": {

                    "arbol": "$arbol",

                    // "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_inicio": "$Busqueda inicio_menos_365_dias",
                    "filtro_fecha_fin": "$Busqueda fin"
                    //"filtro_fecha_fin": "$Busqueda inicio"
                },
                "pipeline": [
                    //rgDate
                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    { "$ne": [{ "$type": "$Arbol.features.properties.name" }, "missing"] },
                                    { "$in": ["$$arbol", "$Arbol.features.properties.name"] },

                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                    , {
                        "$sort": { "rgDate": 1 }
                    }
                    , {
                        "$limit": 1
                    }
                    , {
                        "$project": {
                            "_id": 0,
                            "rgDate": 1
                        }
                    }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$form_monitoreoenfermedades_AUX",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "fecha_enfermedad": "$form_monitoreoenfermedades_AUX.rgDate"
            }
        }

        , {
            "$project": {
                "form_monitoreoenfermedades_AUX": 0
            }
        }



        //=====VALORES A MOSTRAR
        , {
            "$project": {


                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",

                "fecha_recuperacion": "$rgDate",
                "fecha_enfermedad": "$fecha_enfermedad"

                // "supervisor": "$supervisor",
                // "capture": "$capture",
                // "Sampling": "$Sampling",
                // "Formula": "$Formula",
                // "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

            }
        }

        //---restar dias
        , {
            "$addFields": {
                "total_dias_enfermedad": {
                    "$divide": [
                        {
                            "$subtract": [
                                "$fecha_recuperacion",
                                "$fecha_enfermedad"
                            ]
                        },
                        86400000
                    ]
                }
            }
        }

        //---redondear hacia abajo
        , {
            "$addFields": {
                "total_dias_enfermedad": { "$floor": "$total_dias_enfermedad" }
            }
        }
        
        
        
        //---fechas txt
        , {
            "$addFields": {
                "fecha_recuperacion": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_recuperacion" } },
                "fecha_enfermedad": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha_enfermedad" } }
            }
        }





        , {
            "$group": {
                "_id": {
                    "finca": "$finca"
                }

                , "total_arboles_recuperados": { "$sum": 1 }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , { "$unwind": "$data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "total_arboles_recuperados": "$total_arboles_recuperados"
                        }
                    ]
                }
            }
        }


        //=====INDICADOR FINAL
        //---restar dias
        , {
            "$addFields": {
                "PROM_TIEMPO_RECUPERACION": {
                    "$divide": [
                        "$total_dias_enfermedad",
                        "$total_arboles_recuperados"
                    ]
                }
            }
        }

    ]

)