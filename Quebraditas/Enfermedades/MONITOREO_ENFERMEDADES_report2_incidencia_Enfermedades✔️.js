[
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0
            }
        },
        
        
        { "$unwind": "$Enfermedad" },
        
        
        {
            "$group": {
                "_id": {
                    "Enfermedad": "$Enfermedad",
                    "Lote": "$lote",
                    "Planta": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "Enfermedad": "$_id.Enfermedad",
                    "Lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_x_enfermedad": { "$sum": 1 }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
         , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote_x_enfermedad": "$plantas_dif_censadas_x_lote_x_enfermedad"
                        }
                    ]
                }
            }
        }
    ]