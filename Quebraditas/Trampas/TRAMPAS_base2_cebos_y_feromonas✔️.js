[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Trampa.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Trampa.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "trampa": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
        }
    },
    {
        "$addFields": {
            "bloque": "$bloque.properties.name",

            "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
            "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
            "lote_oid": "$lote._id",
            "lote": "$lote.properties.name",

            "trampa": "$trampa.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    {
        "$addFields": {
            "finca": "$finca.name"

            , "str_id_finca": "$Point.farm"
            , "path_id_finca": { "$concat": [",", "$Point.farm", ","] }
        }
    },
    { "$unwind": "$finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "Formula": 0
        }
    },




    {
        "$lookup": {
            "from": "cartography_polygon",
            "localField": "lote_oid",
            "foreignField": "_id",
            "as": "info_lote"
        }
    },
    {
        "$addFields": {
            "info_lote": "$info_lote.internal_features"
        }
    },
    { "$unwind": "$info_lote" },

    {
        "$addFields": {
            "trampas_x_lote": {
                "$filter": {
                    "input": "$info_lote",
                    "as": "item",
                    "cond": { "$eq": ["$$item.type_features", "traps"] }
                }
            }
        }
    },
    { "$unwind": "$trampas_x_lote" },
    {
        "$addFields": {
            "trampas_x_lote": "$trampas_x_lote.count_features"
        }
    },

    {
        "$project": {
            "info_lote": 0,
            "lote_oid": 0
        }
    },


    {
        "$lookup": {
            "from": "cartography_polygon",
            "as": "info_trampas",
            "let": {
                "path_id_finca": "$path_id_finca"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$$path_id_finca", "$path"]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "trampas_x_bloque": {
                            "$filter": {
                                "input": "$internal_features",
                                "as": "item",
                                "cond": { "$eq": ["$$item.type_features", "traps"] }
                            }
                        }
                    }
                },

                { "$unwind": "$trampas_x_bloque" },
                {
                    "$addFields": {
                        "trampas_x_bloque": "$trampas_x_bloque.count_features"
                    }
                },


                {
                    "$group": {
                        "_id": {
                            "path": "$path"
                        }

                        , "total_trampas_x_finca": {
                            "$sum": "$trampas_x_bloque"
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$info_trampas",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "total_trampas_x_finca": "$info_trampas.total_trampas_x_finca"
        }
    },

    {
        "$project": {
            "info_trampas": 0,
            "str_id_finca": 0,
            "path_id_finca": 0
        }
    },

    {
        "$addFields": {
            "Suma_Individuos_x_trampa": {
                "$sum": ["$Machos Rhynchophorus", "$Hembras Rhynchophorus"]
            }
        }

    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "tiene_feromonas": "$Cambio de Feromona"
            }

            , "data": {
                "$push": "$$ROOT"
            }


            , "cantidad_feromonas": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$Cambio de Feromona", "Si"] },
                                "then": 1
                            }
                        ],
                        "default": 0
                    }
                }
            }

        }
    }


    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_feromonas": "$cantidad_feromonas"
                    }
                ]
            }
        }
    }



    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "tiene_cebos": "$Cambio de Cebo"
            }

            , "data": {
                "$push": "$$ROOT"
            }


            , "cantidad_cebos": {
                "$sum": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$Cambio de Cebo", "Si"] },
                                "then": 1
                            }
                        ],
                        "default": 0
                    }
                }
            }

        }
    }


    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "cantidad_cebos": "$cantidad_cebos"
                    }
                ]
            }
        }
    }



]