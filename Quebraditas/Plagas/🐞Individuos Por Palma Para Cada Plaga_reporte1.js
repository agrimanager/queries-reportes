db.form_monitoreoplagas.aggregate(
    [
        //=====CARTOGRAFIA
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0
            }
        },
        
        
        
        //=====pla
        {
            "$group": {
                "_id": {
                    "Tiene_Leucothyreus femoratus": "$Leucothyreus femoratus",
                    "Lote": "$lote",
                    "Planta": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "Tiene_Leucothyreus femoratus": "$_id.Tiene_Leucothyreus femoratus",
                    "Lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
            }
        }



        ,
        {
            "$group": {
                "_id": {
                    "Lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "plantas_dif_censadas_x_lote_Total": { "$sum": "$plantas_dif_censadas_x_lote" }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , { "$unwind": "$data.data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data.data",
                        {
                            "plantas_dif_censadas_x_lote_Total": "$plantas_dif_censadas_x_lote_Total",
                            "plantas_dif_censadas_x_lote": "$data.plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "Hispoleptis Subfasciata": { "$toDouble": "$Hispoleptis Subfasciata" },
                "Loxotoma elegans": { "$toDouble": "$Loxotoma elegans" },
                "Delocrania sp": { "$toDouble": "$Delocrania sp" },
                "Retracrus elaeis": { "$toDouble": "$Retracrus elaeis" },
                "Sibine fusca": { "$toDouble": "$Sibine fusca" },
                "Sagalassa Valida W": { "$toDouble": "$Sagalassa Valida W" },
                "Opsiphanes cassina": { "$toDouble": "$Opsiphanes cassina" },
                "Oiquetikus kirbyi": { "$toDouble": "$Oiquetikus kirbyi" },
                "Natada Pucara Doguin": { "$toDouble": "$Natada Pucara Doguin" },
                "Leptopharsa gibbicarina": { "$toDouble": "$Leptopharsa gibbicarina" },
                "Euprosterna elaeasa": { "$toDouble": "$Euprosterna elaeasa" },
                "Euclea diversa": { "$toDouble": "$Euclea diversa" },
                "Durrantia sp": { "$toDouble": "$Durrantia sp" },
                "Dirphia gragatus": { "$toDouble": "$Dirphia gragatus" },
                "Caligo sp": { "$toDouble": "$Caligo sp" },
                "Mesocia pusilla": { "$toDouble": "$Mesocia pusilla" },
                "Brassolis sophorae": { "$toDouble": "$Brassolis sophorae" },
                "Strategus aloeus": { "$toDouble": "$Strategus aloeus" },
                "Automeris sp": { "$toDouble": "$Automeris sp" },
                "Atta": { "$toDouble": "$Atta" },
                "Megalopygue": { "$toDouble": "$Megalopygue" },
                "Alurnus humeralis Rosemberg": { "$toDouble": "$Alurnus humeralis Rosemberg" },
                "Phobetron pithecium": { "$toDouble": "$Phobetron pithecium" },
                "Spaethilla Triste": { "$toDouble": "$Spaethilla Triste" },
                "Acraga ochracea": { "$toDouble": "$Acraga ochracea" }

//  "Spaethilla Triste" : null,
//  "Acraga ochracea" : null



            }
        },

        // {
        //     "$addFields":
        //     {
        //         "Spaethilla Triste": { "$ifNull": ["$Spaethilla Triste", 0] }
        //     }
        // },


        // {
        //     "$addFields":
        //     {
        //         "Acraga ochracea": { "$ifNull": ["$Acraga ochracea", 0] }
        //     }
        // },



        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Hispoleptis Subfasciata": {
        //             "$sum": "$Hispoleptis Subfasciata"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Hispoleptis Subfasciata": "$Sum_Hispoleptis Subfasciata"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Loxotoma elegans": {
        //             "$sum": "$Loxotoma elegans"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Loxotoma elegans": "$Sum_Loxotoma elegans"
        //                 }
        //             ]
        //         }
        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Delocrania sp": {
        //             "$sum": "$Delocrania sp"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Delocrania sp": "$Sum_Delocrania sp"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Retracrus elaeis": {
        //             "$sum": "$Retracrus elaeis"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Retracrus elaeis": "$Sum_Retracrus elaeis"
        //                 }
        //             ]
        //         }
        //     }
        // },






        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Sibine fusca": {
        //             "$sum": "$Sibine fusca"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Sibine fusca": "$Sum_Sibine fusca"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Sagalassa Valida W": {
        //             "$sum": "$Sagalassa Valida W"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Sagalassa Valida W": "$Sum_Sagalassa Valida W"
        //                 }
        //             ]
        //         }
        //     }
        // },




        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Opsiphanes cassina": {
        //             "$sum": "$Opsiphanes cassina"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Opsiphanes cassina": "$Sum_Opsiphanes cassina"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Oiquetikus kirbyi": {
        //             "$sum": "$Oiquetikus kirbyi"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Oiquetikus kirbyi": "$Sum_Oiquetikus kirbyi"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Natada Pucara Doguin": {
        //             "$sum": "$Natada Pucara Doguin"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Natada Pucara Doguin": "$Sum_Natada Pucara Doguin"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Leptopharsa gibbicarina": {
        //             "$sum": "$Leptopharsa gibbicarina"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Leptopharsa gibbicarina": "$Sum_Leptopharsa gibbicarina"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Euprosterna elaeasa": {
        //             "$sum": "$Euprosterna elaeasa"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Euprosterna elaeasa": "$Sum_Euprosterna elaeasa"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Euclea diversa": {
        //             "$sum": "$Euclea diversa"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Euclea diversa": "$Sum_Euclea diversa"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Durrantia sp": {
        //             "$sum": "$Durrantia sp"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Durrantia sp": "$Sum_Durrantia sp"
        //                 }
        //             ]
        //         }
        //     }
        // },



        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Dirphia gragatus": {
        //             "$sum": "$Dirphia gragatus"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Dirphia gragatus": "$Sum_Dirphia gragatus"
        //                 }
        //             ]
        //         }
        //     }
        // },






        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Caligo sp": {
        //             "$sum": "$Caligo sp"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Caligo sp": "$Sum_Caligo sp"
        //                 }
        //             ]
        //         }
        //     }
        // },



        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Mesocia pusilla": {
        //             "$sum": "$Mesocia pusilla"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Mesocia pusilla": "$Sum_Mesocia pusilla"
        //                 }
        //             ]
        //         }
        //     }
        // },









        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Mesocia pusilla": {
        //             "$sum": "$Mesocia pusilla"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Mesocia pusilla": "$Sum_Mesocia pusilla"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Brassolis sophorae": {
        //             "$sum": "$Brassolis sophorae"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Brassolis sophorae": "$Sum_Brassolis sophorae"
        //                 }
        //             ]
        //         }
        //     }
        // },




        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Strategus aloeus": {
        //             "$sum": "$Strategus aloeus"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Sum_Strategus aloeus": "$Sum_Strategus aloeus"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Automeris sp": {
        //             "$sum": "$Automeris sp"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Automeris sp": "$Sum_Automeris sp"
        //                 }
        //             ]
        //         }
        //     }
        // },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Atta": {
        //             "$sum": "$Atta"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Atta": "$Sum_Atta"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Atta": {
        //             "$sum": "$Atta"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Atta": "$Sum_Atta"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Megalopygue": {
        //             "$sum": "$Megalopygue"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Megalopygue": "$Sum_Megalopygue"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Alurnus humeralis Rosemberg": {
        //             "$sum": "$Alurnus humeralis Rosemberg"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Alurnus humeralis Rosemberg": "$Sum_Alurnus humeralis Rosemberg"
        //                 }
        //             ]
        //         }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Phobetron pithecium": {
        //             "$sum": "$Phobetron pithecium"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Phobetron pithecium": "$Sum_Phobetron pithecium"
        //                 }
        //             ]
        //         }
        //     }
        // }




        // , {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Spaethilla Triste": {
        //             "$sum": "$Spaethilla Triste"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Spaethilla Triste": "$Sum_Spaethilla Triste"
        //                 }
        //             ]
        //         }
        //     }
        // },





        // {
        //     "$group": {
        //         "_id": null,
        //         "data": {
        //             "$push": "$$ROOT"
        //         },
        //         "Sum_Acraga ochracea": {
        //             "$sum": "$Acraga ochracea"
        //         }
        //     }
        // },
        // { "$unwind": "$data" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data",
        //                 {
        //                     "Sum_Acraga ochracea": "$Sum_Acraga ochracea"
        //                 }
        //             ]
        //         }
        //     }
        // }

    ]

)

.sort({_id:-1})