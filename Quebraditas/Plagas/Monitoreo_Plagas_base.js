db.form_monitoreoplagas.aggregate(

    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"

            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },


        {
            "$addFields": {
                "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
            }
        },
        
        // //---agrupacion

        // {
        //     "$group": {
        //         "_id": {
        //             "finca": "$finca",
        //             "fecha": "$fecha"
        //         }
                
        //         , "data": { "$push": "$$ROOT" }
        //         , "rgDate": { "$min": "$rgDate" }
        //     }
        // },

        // {
        //     "$group": {
        //         "_id": {
        //             "finca": "$_id.finca",
        //             "lote": "$_id.lote"
        //         }

        //         , "total_dias": { "$sum": 1 }

        //         , "rgDate": { "$min": "$rgDate" }
        //     }
        // },



        // {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$_id",
        //                 {

        //                     "total_dias": "$total_dias"



        //                     , "rgDate": "$rgDate"
        //                 }
        //             ]
        //         }
        //     }
        // },




        // {
        //     "$addFields": {
        //         "hectareas_censadas_diariamente": {
        //             "$cond": {
        //                 "if": { "$eq": ["$total_dias", 0] },
        //                 "then": 0,
        //                 "else": {
        //                     "$divide": ["$total_hectareas", "$total_dias"]
        //                 }
        //             }
        //         }
        //     }
        // }


    ]

)