   [

      
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,
                "Formula": 0
                , "Sampling": 0

                , "Arbol": 0
                , "Point": 0
                , "Plagas": 0
                , "uid": 0
            }
        }


      
        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "planta": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }

       
        , {

            "$addFields": {
                "array_plagas": [
                    {
                        "plaga": "Hispoleptis Subfasciata",
                        "cantidad": { "$toDouble": "$Hispoleptis Subfasciata" }
                    },
                    {
                        "plaga": "Loxotoma elegans",
                        "cantidad": { "$toDouble": "$Loxotoma elegans" }
                    },
                    {
                        "plaga": "Delocrania sp",
                        "cantidad": { "$toDouble": "$Delocrania sp" }
                    },
                    {
                        "plaga": "Retracrus elaeis",
                        "cantidad": { "$toDouble": "$Retracrus elaeis" }
                    },
                    {
                        "plaga": "Sibine fusca",
                        "cantidad": { "$toDouble": "$Sibine fusca" }
                    },
                    {
                        "plaga": "Sagalassa Valida W",
                        "cantidad": { "$toDouble": "$Sagalassa Valida W" }
                    },
                    {
                        "plaga": "Opsiphanes cassina",
                        "cantidad": { "$toDouble": "$Opsiphanes cassina" }
                    },

                    {
                        "plaga": "Oiquetikus kirbyi",
                        "cantidad": { "$toDouble": "$Oiquetikus kirbyi" }
                    },

                    {
                        "plaga": "Natada Pucara Doguin",
                        "cantidad": { "$toDouble": "$Natada Pucara Doguin" }
                    },

                    {
                        "plaga": "Leptopharsa gibbicarina",
                        "cantidad": { "$toDouble": "$Leptopharsa gibbicarina" }
                    },

                    {
                        "plaga": "Euprosterna elaeasa",
                        "cantidad": { "$toDouble": "$Euprosterna elaeasa" }
                    },

                    {
                        "plaga": "Euclea diversa",
                        "cantidad": { "$toDouble": "$Euclea diversa" }
                    },

                    {
                        "plaga": "Durrantia sp",
                        "cantidad": { "$toDouble": "$Durrantia sp" }
                    },

                    {
                        "plaga": "Dirphia gragatus",
                        "cantidad": { "$toDouble": "$Dirphia gragatus" }
                    },

                    {
                        "plaga": "Caligo sp",
                        "cantidad": { "$toDouble": "$Caligo sp" }
                    },


                    {
                        "plaga": "Mesocia pusilla",
                        "cantidad": { "$toDouble": "$Mesocia pusilla" }
                    },


                    {
                        "plaga": "Brassolis sophorae",
                        "cantidad": { "$toDouble": "$Brassolis sophorae" }
                    },

                    {
                        "plaga": "Strategus aloeus",
                        "cantidad": { "$toDouble": "$Strategus aloeus" }
                    },

                    {
                        "plaga": "Automeris sp",
                        "cantidad": { "$toDouble": "$Automeris sp" }
                    },


                    {
                        "plaga": "Atta",
                        "cantidad": { "$toDouble": "$Atta" }
                    },


                    {
                        "plaga": "Megalopygue",
                        "cantidad": { "$toDouble": "$Megalopygue" }
                    },



                    {
                        "plaga": "Alurnus humeralis Rosemberg",
                        "cantidad": { "$toDouble": "$Alurnus humeralis Rosemberg" }
                    },



                    {
                        "plaga": "Phobetron pithecium",
                        "cantidad": { "$toDouble": "$Phobetron pithecium" }
                    },


                    {
                        "plaga": "Spaethilla Triste",
                        "cantidad": { "$toDouble": "$Spaethilla Triste" }
                    },


                    {
                        "plaga": "Acraga ochracea",
                        "cantidad": { "$toDouble": "$Acraga ochracea" }
                    }
                ]
            }
        },


      
        {
            "$addFields": {
                "array_plagas": {
                    "$filter": {
                        "input": "$array_plagas",
                        "as": "censo",
                        "cond": {
                            "$not": {
                                "$in": [
                                    "$$censo.cantidad",
                                    [null, 0]
                                ]
                            }
                        }
                    }
                }
            }
        },

        { "$unwind": "$array_plagas" }

        , {
            "$addFields": {
                "plaga": "$array_plagas.plaga",
                "cantidad": "$array_plagas.cantidad"
            }
        }

        , {
            "$project": {
                "_id": 1,
                "finca": 1,
                "bloque": 1,
                "lote": 1,
                "linea": 1,
                "arbol": 1,
                "Numero Hectareas": 1,
                "Numero de plantas": 1,
                "plantas_dif_censadas_x_lote": 1,
                "plaga": 1,
                "cantidad": 1,
                "Leucothyreus femoratus": 1,
                "supervisor": 1,
                "rgDate": 1,
                "capture": 1
            }
        }


    ]