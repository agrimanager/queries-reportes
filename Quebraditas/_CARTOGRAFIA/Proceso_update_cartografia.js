//#MANUAL
var data = db.form_actualizaciondepalmas.aggregate(


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "oid_variable_cartografia": { $toObjectId: "$variable_cartografia.features._id" }
        }
    },

    {
        "$project": {
            "variable_cartografia": 0
            , "Point": 0
            , "Arbol": 0
        }
    }

    , {
        $group: {
            _id: {
                oid_variable_cartografia: "$oid_variable_cartografia"
            }
            , datos: { $push: "$$ROOT" }
        }
    }




    , {
        "$addFields": {
            "erradicada": {
                "$filter": {
                    "input": "$datos",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Erradicada", "SI"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "erradicada": {
                "$cond": {
                    "if": { "$eq": ["$erradicada", []] },
                    "then": false,
                    "else": true
                }
            }
        }
    }



    , {
        "$addFields": {
            "resiembra": {
                "$filter": {
                    "input": "$datos",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Resiembra", "SI"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "resiembra": {
                "$cond": {
                    "if": { "$eq": ["$resiembra", []] },
                    "then": false,
                    "else": true
                }
            }
        }
    }



    , {
        "$addFields": {
            "supermachos": {
                "$filter": {
                    "input": "$datos",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Supermacho", "SI"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "supermachos": {
                "$cond": {
                    "if": { "$eq": ["$supermachos", []] },
                    "then": false,
                    "else": true
                }
            }
        }
    }


    , {
        "$addFields": {
            "improductivas": {
                "$filter": {
                    "input": "$datos",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Improductivas", "SI"] }
                }
            }
        }
    }
    , {
        "$addFields": {
            "improductivas": {
                "$cond": {
                    "if": { "$eq": ["$improductivas", []] },
                    "then": false,
                    "else": true
                }
            }
        }
    }

    , {
        "$project": {
            "datos": 0
        }
    }



)

// data



data.forEach(i => {

    db.cartography.update(
        {
            _id: i._id.oid_variable_cartografia
        }, {
        $set: {

            "properties.custom.Erradicada": {
                "type": "bool",
                "value": i.erradicada
            },

            "properties.custom.Resiembra": {
                "type": "bool",
                "value": i.resiembra
            },

            "properties.custom.Supermachos": {
                "type": "bool",
                "value": i.supermachos
            },

            "properties.custom.Improductivas": {
                "type": "bool",
                "value": i.improductivas
            },
        }
    }

    )


})
