[


    {
        "$match": {
            "Estado del seguimiento fenologico": { "$exists": true }
        }
    },


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "X": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
            "Y": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] }
        }
    },

    { "$addFields": { "arbol_id": { "$ifNull": ["$arbol._id", ""] } } },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0


            , "Arbol": 0
        }
    }



    , {
        "$addFields": {
            "split_lote": { "$split": ["$lote", " "] }
        }
    }


    , {
        "$addFields": {
            "size_split_lote": { "$size": "$split_lote" }
        }
    }

    , {
        "$addFields": {
            "aux_size_split_lote": {
                "$subtract": ["$size_split_lote", 2]
            }
        }
    }

    , {
        "$addFields": {
            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
        }
    }



    , {
        "$addFields": {

            "UP": {
                "$reduce": {
                    "input": "$split_lote2",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", " ", "$$this"] }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
        }
    }


    , {
        "$addFields": {
            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
        }
    }



    , {
        "$addFields": {
            "Monitor": "$supervisor"
        }
    }



    , {
        "$project": {
            "Point": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0

            , "Sampling": 0
            , "supervisor": 0



            , "split_lote": 0
            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0

        }
    }


    , {
        "$addFields": {
            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
        }
    }


    , {
        "$addFields": {
            "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
            , "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } }
            , "Semana": { "$week": { "date": "$rgDate" } }
            , "Año": { "$year": { "date": "$rgDate" } }
        }
    }



    , {
        "$addFields": {
            "Cuadrante Inferior Norte": { "$ifNull": ["$Cuadrante Inferior Norte", ""] },
            "Cuadrante Superior Norte": { "$ifNull": ["$Cuadrante Superior Norte", ""] },
            "Cuadrante Inferior Oriente": { "$ifNull": ["$Cuadrante Inferior Oriente", ""] },
            "Cuadrante Superior Oriente": { "$ifNull": ["$Cuadrante Superior Oriente", ""] },
            "Cuadrante Inferior Sur": { "$ifNull": ["$Cuadrante Inferior Sur", ""] },
            "Cuadrante Superior Sur": { "$ifNull": ["$Cuadrante Superior Sur", ""] },
            "Cuadrante Inferior Occidente": { "$ifNull": ["$Cuadrante Inferior Occidente", ""] },
            "Cuadrante Superior Occidente": { "$ifNull": ["$Cuadrante Superior Occidente", ""] }
        }
    }

    , {
        "$addFields": {
            "estado_yema_0": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "0"] }
                }
            },
            "estado_yema_1": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "1"] }
                }
            },
            "estado_yema_6": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "6"] }
                }
            },
            "estado_yema_8": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "8"] }
                }
            },
            "estado_yema_11": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "11"] }
                }
            },
            "estado_yema_13": {
                "$filter": {
                    "input": "$Estado predominante de las yemas en el arbol",
                    "as": "item",
                    "cond": { "$eq": ["$$item", "13"] }
                }
            }
        }
    }



    , {
        "$addFields": {
            "array_data": [


                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Inferior Norte",
                    "valor_variable": "$Cuadrante Inferior Norte"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Superior Norte",
                    "valor_variable": "$Cuadrante Superior Norte"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Inferior Oriente",
                    "valor_variable": "$Cuadrante Inferior Oriente"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Superior Oriente",
                    "valor_variable": "$Cuadrante Superior Oriente"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Inferior Sur",
                    "valor_variable": "$Cuadrante Inferior Sur"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Superior Sur",
                    "valor_variable": "$Cuadrante Superior Sur"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Inferior Occidente",
                    "valor_variable": "$Cuadrante Inferior Occidente"
                },
                {
                    "tipo_variable": "Cuadrante",
                    "nombre_variable": "Cuadrante Superior Occidente",
                    "valor_variable": "$Cuadrante Superior Occidente"
                },

                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 0",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_0", 0] }, ""] }
                },
                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 1",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_1", 0] }, ""] }
                },
                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 6",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_6", 0] }, ""] }
                },
                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 8",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_8", 0] }, ""] }
                },
                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 11",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_11", 0] }, ""] }
                },
                {
                    "tipo_variable": "Estado Yema",
                    "nombre_variable": "Estado Yema 13",
                    "valor_variable": { "$ifNull": [{ "$arrayElemAt": ["$estado_yema_13", 0] }, ""] }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "array_data": {
                "$filter": {
                    "input": "$array_data",
                    "as": "item",
                    "cond": { "$ne": ["$$item.valor_variable", ""] }
                }
            }
        }
    }


    , {
        "$unwind": {
            "path": "$array_data",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "arbol_id",
            "foreignField": "_id",
            "as": "info_cartografia"
        }
    }
    , { "$unwind": "$info_cartografia" }
    , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_cartografia.geometry", {}] } } }
    , {
        "$project": {
            "info_cartografia": 0
        }
    }

    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Inferior Norte"] }
                            , "then": "#baf95a"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Superior Norte"] }
                            , "then": "#3cdd5f"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Inferior Oriente"] }
                            , "then": "#79a1dd"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Superior Oriente"] }
                            , "then": "#33cbee"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Inferior Sur"] }
                            , "then": "#fc1a02"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Superior Sur"] }
                            , "then": "#ee6616"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Inferior Occidente"] }
                            , "then": "#b34c3d"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Cuadrante Superior Occidente"] }
                            , "then": "#17776a"
                        },


                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 0"] }
                            , "then": "#ff0000"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 1"] }
                            , "then": "#ffa500"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 6"] }
                            , "then": "#ffff00"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 8"] }
                            , "then": "#ee82ee"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 11"] }
                            , "then": "#0000ff"
                        },
                        {
                            "case": { "$eq": ["$array_data.nombre_variable", "Estado Yema 13"] }
                            , "then": "#008000"
                        }

                    ],
                    "default": "#000000"
                }
            }

        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": "$arbol_id"
            , "cartography_geometry": "$cartography_geometry"

            , "color": "$color"
        }
    }


    , {
        "$project": {
            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",

            "properties": {

                "tipo_variable": "$array_data.tipo_variable",
                "nombre_variable": "$array_data.nombre_variable",

                "Lote": "$Lote",
                "Nombre": "$Nombre",
                "UP": "$UP",
                "Monitor": "$Monitor",
                "Semana": { "$toString": "$Semana" },
                "Fecha": "$Fecha",

                "color": "$color"
            }

        }
    }




]
