db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-02-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_seguimientofenologico",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //FILTRO FECHAS
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$match": {
                            "Estado del seguimiento fenologico": { "$exists": true }
                        }
                    },


                    {
                        "$addFields": {
                            "variable_cartografia": "$Arbol"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    // { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields": {
                            "X": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                            "Y": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] }
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0


                            , "Arbol": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "split_lote": { "$split": ["$lote", " "] }
                        }
                    }


                    , {
                        "$addFields": {
                            "size_split_lote": { "$size": "$split_lote" }
                        }
                    }

                    , {
                        "$addFields": {
                            "aux_size_split_lote": {
                                "$subtract": ["$size_split_lote", 2]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
                        }
                    }



                    , {
                        "$addFields": {

                            "UP": {
                                "$reduce": {
                                    "input": "$split_lote2",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", " ", "$$this"] }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
                        }
                    }


                    , {
                        "$addFields": {
                            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
                        }
                    }



                    , {
                        "$addFields": {
                            "Monitor": "$supervisor"
                        }
                    }



                    , {
                        "$project": {
                            "Point": 0
                            , "Formula": 0
                            , "uid": 0
                            , "uDate": 0
                            // , "capture": 0
                            , "Sampling": 0
                            , "supervisor": 0



                            , "split_lote": 0
                            , "size_split_lote": 0
                            , "aux_size_split_lote": 0
                            , "split_lote2": 0

                        }
                    }


                    , {
                        "$addFields": {
                            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
                        }
                    }


                    , {
                        "$addFields": {
                            "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
                            , "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } }
                            , "Semana": { "$week": { "date": "$rgDate" } }
                            , "Año": { "$year": { "date": "$rgDate" } }
                        }
                    }


                    //  "Cuadrante Inferior Norte" : "1",
                    // 	"Cuadrante Superior Norte" : "0",
                    // 	"Cuadrante Inferior Oriente" : "11",
                    // 	"Cuadrante Superior Oriente" : "11",
                    // 	"Cuadrante Inferior Sur" : "11",
                    // 	"Cuadrante Superior Sur" : "11",
                    // 	"Cuadrante Inferior Occidente" : "11",
                    // 	"Cuadrante Superior Occidente" : "11",


                    , {
                        "$addFields": {
                            "Cuadrante Inferior Norte": { "$ifNull": ["$Cuadrante Inferior Norte", ""] },
                            "Cuadrante Superior Norte": { "$ifNull": ["$Cuadrante Superior Norte", ""] },
                            "Cuadrante Inferior Oriente": { "$ifNull": ["$Cuadrante Inferior Oriente", ""] },
                            "Cuadrante Superior Oriente": { "$ifNull": ["$Cuadrante Superior Oriente", ""] },
                            "Cuadrante Inferior Sur": { "$ifNull": ["$Cuadrante Inferior Sur", ""] },
                            "Cuadrante Superior Sur": { "$ifNull": ["$Cuadrante Superior Sur", ""] },
                            "Cuadrante Inferior Occidente": { "$ifNull": ["$Cuadrante Inferior Occidente", ""] },
                            "Cuadrante Superior Occidente": { "$ifNull": ["$Cuadrante Superior Occidente", ""] }
                        }
                    }


                    //columnas nuevas 2023-06-09
                    , {
                        "$addFields": {
                            "Estado predominante 1": { "$ifNull": [{ "$arrayElemAt": ["$Estado predominante de las yemas en el arbol", 0] }, ""] }
                            ,"Estado predominante 2": { "$ifNull": [{ "$arrayElemAt": ["$Estado predominante de las yemas en el arbol", 1] }, ""] }
                            ,"Estado predominante 3": { "$ifNull": [{ "$arrayElemAt": ["$Estado predominante de las yemas en el arbol", 2] }, ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "Estados predominates del arbol": {
                                "$cond": {
                                    "if": {
                                        "$isArray": ["$Estado predominante de las yemas en el arbol"]
                                    },
                                    "then": {
                                        "$reduce": {
                                            "input": "$Estado predominante de las yemas en el arbol",
                                            "initialValue": "",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$indexOfArray": ["$Estado predominante de las yemas en el arbol", "$$this"]
                                                        }, 0]
                                                    },
                                                    "then": {
                                                        "$concat": ["$$value", "$$this"]
                                                    },
                                                    "else": {
                                                        "$concat": ["$$value", "-", "$$this"]
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "else": ""
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "Presencia de Floracion": { "$ifNull": ["$Presencia de Floracion", ""] }
                            , "Tipo de floracion": { "$ifNull": ["$Presencia de Floracion_SI", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "Presencia de Floracion": { "$ifNull": ["$Porcentaje de cobertura de raices", 0] }
                        }
                    }

                    , {
                        "$addFields": {
                            "Cantidad cuajes marcados": { "$ifNull": ["$Cantidad cuajes marcados", 0] }
                        }
                    }


                    /*
                    "TRAVIESA Longitud del fruto 1      mm" : 0,
                	"TRAVIESA Diametro del fruto 1 mm" : 0,
                	"TRAVIESA  Longitud del fruto 2 mm" : 0,
                	"TRAVIESA  Diametro del fruto 2 mm" : 0,
                	"TRAVIESA  Longitud del fruto 3 mm" : 0,
                	"TRAVIESA  Diametro del fruto 3 mm" : 0,
                	"COSECHA Longitud del fruto 1      mm" : 0,
                	"COSECHA Diametro del fruto 1 mm" : 0,
                	"COSECHA Longitud del fruto 2 mm" : 0,
                	"COSECHA Diametro del fruto 2 mm" : 0,
                	"COSECHA Longitud del fruto 3 mm" : 0,
                	"COSECHA Diametro del fruto 3 mm" : 0,


                	"TRAVIESA Longitud del fruto 1      mm" : "",
                	"TRAVIESA Diametro del fruto 1 mm" : "",
                	"TRAVIESA  Longitud del fruto 2 mm" : "",
                	"TRAVIESA  Diametro del fruto 2 mm" : "",
                	"TRAVIESA  Longitud del fruto 3 mm" : "",
                	"TRAVIESA  Diametro del fruto 3 mm" : "",
                	"COSECHA Longitud del fruto 1      mm" : "",
                	"COSECHA Diametro del fruto 1 mm" : "",
                	"COSECHA Longitud del fruto 2 mm" : "",
                	"COSECHA Diametro del fruto 2 mm" : "",
                	"COSECHA Longitud del fruto 3 mm" : "",
                    */


                    /*
                    //otros old

                    "Longitud del fruto 1      mm" : 0,
                	"Diametro del fruto 1 mm" : 0,
                	"Longitud del fruto 2 mm" : 0,
                	"Diametro del fruto 2 mm" : 0,
                	"Longitud del fruto 3 mm" : 0,
                	"Diametro del fruto 3 mm" : 0,

                    */

                    //cosecha longitud
                    , {
                        "$addFields": {
                            "COSECHA Longitud del fruto 1      mm": { "$ifNull": ["$COSECHA Longitud del fruto 1      mm", ""] },
                            "COSECHA Longitud del fruto 2 mm": { "$ifNull": ["$COSECHA Longitud del fruto 2 mm", ""] },
                            "COSECHA Longitud del fruto 3 mm": { "$ifNull": ["$COSECHA Longitud del fruto 3 mm", ""] }
                        }
                    }

                    //cosecha diametro
                    , {
                        "$addFields": {
                            "COSECHA Diametro del fruto 1 mm": { "$ifNull": ["$COSECHA Diametro del fruto 1 mm", ""] },
                            "COSECHA Diametro del fruto 2 mm": { "$ifNull": ["$COSECHA Diametro del fruto 2 mm", ""] },
                            "COSECHA Diametro del fruto 3 mm": { "$ifNull": ["$COSECHA Diametro del fruto 3 mm", ""] }
                        }
                    }

                    //traviesa longitud
                    , {
                        "$addFields": {
                            "TRAVIESA Longitud del fruto 1      mm": { "$ifNull": ["$TRAVIESA Longitud del fruto 1      mm", ""] },
                            "TRAVIESA  Longitud del fruto 2 mm": { "$ifNull": ["$TRAVIESA  Longitud del fruto 2 mm", ""] },
                            "TRAVIESA  Longitud del fruto 3 mm": { "$ifNull": ["$TRAVIESA  Longitud del fruto 3 mm", ""] }
                        }
                    }

                    //traviesa diametro
                    , {
                        "$addFields": {
                            "TRAVIESA Diametro del fruto 1 mm": { "$ifNull": ["$TRAVIESA Diametro del fruto 1 mm", ""] },
                            "TRAVIESA  Diametro del fruto 2 mm": { "$ifNull": ["$TRAVIESA  Diametro del fruto 2 mm", ""] },
                            "TRAVIESA  Diametro del fruto 3 mm": { "$ifNull": ["$TRAVIESA  Diametro del fruto 3 mm", ""] }
                        }
                    }



                    //----convertir en numeros



                    //cosecha longitud
                    , {
                        "$addFields": {
                            "COSECHA Longitud del fruto 1      mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Longitud del fruto 1      mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Longitud del fruto 1      mm"
                                    }
                                }
                            }
                            , "COSECHA Longitud del fruto 2 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Longitud del fruto 2 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Longitud del fruto 2 mm"
                                    }
                                }
                            }
                            , "COSECHA Longitud del fruto 3 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Longitud del fruto 3 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Longitud del fruto 3 mm"
                                    }
                                }
                            }
                        }
                    }


                    //cosecha diametro
                    , {
                        "$addFields": {
                            "COSECHA Diametro del fruto 1 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Diametro del fruto 1 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Diametro del fruto 1 mm"
                                    }
                                }
                            }
                            , "COSECHA Diametro del fruto 2 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Diametro del fruto 2 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Diametro del fruto 2 mm"
                                    }
                                }
                            }
                            , "COSECHA Diametro del fruto 3 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$COSECHA Diametro del fruto 3 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$COSECHA Diametro del fruto 3 mm"
                                    }
                                }
                            }
                        }
                    }

                    //traviesa longitud
                    , {
                        "$addFields": {
                            "TRAVIESA Longitud del fruto 1      mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA Longitud del fruto 1      mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA Longitud del fruto 1      mm"
                                    }
                                }
                            }
                            , "TRAVIESA  Longitud del fruto 2 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA  Longitud del fruto 2 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA  Longitud del fruto 2 mm"
                                    }
                                }
                            }
                            , "TRAVIESA  Longitud del fruto 3 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA  Longitud del fruto 3 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA  Longitud del fruto 3 mm"
                                    }
                                }
                            }
                        }
                    }

                    //traviesa diametro
                    , {
                        "$addFields": {
                            "TRAVIESA Diametro del fruto 1 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA Diametro del fruto 1 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA Diametro del fruto 1 mm"
                                    }
                                }
                            }
                            , "TRAVIESA  Diametro del fruto 2 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA  Diametro del fruto 2 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA  Diametro del fruto 2 mm"
                                    }
                                }
                            }
                            , "TRAVIESA  Diametro del fruto 3 mm": {
                                "$cond": {
                                    "if": { "$eq": ["$TRAVIESA  Diametro del fruto 3 mm", ""] },
                                    "then": 0,
                                    "else": {
                                        "$toDouble": "$TRAVIESA  Diametro del fruto 3 mm"
                                    }
                                }
                            }
                        }
                    }



                    //---calcular promedios




                    //cosecha longitud
                    , {
                        "$addFields": {
                            "Long prom frutos de cosecha": {
                                "$divide": [
                                    {
                                        "$sum": [
                                            "$COSECHA Longitud del fruto 1      mm",
                                            "$COSECHA Longitud del fruto 2 mm",
                                            "$COSECHA Longitud del fruto 3 mm"
                                        ]
                                    }
                                    , 3]
                            }
                        }
                    }



                    //cosecha diametro
                    , {
                        "$addFields": {
                            "Diam prom frutos de cosecha": {
                                "$divide": [
                                    {
                                        "$sum": [
                                            "$COSECHA Diametro del fruto 1 mm",
                                            "$COSECHA Diametro del fruto 2 mm",
                                            "$COSECHA Diametro del fruto 3 mm"
                                        ]
                                    }
                                    , 3]
                            }
                        }
                    }





                    //traviesa longitud
                    , {
                        "$addFields": {
                            "Long prom frutos de Traviesa": {
                                "$divide": [
                                    {
                                        "$sum": [
                                            "$TRAVIESA Longitud del fruto 1      mm",
                                            "$TRAVIESA  Longitud del fruto 2 mm",
                                            "$TRAVIESA  Longitud del fruto 3 mm"
                                        ]
                                    }
                                    , 3]
                            }
                        }
                    }


                    //traviesa diametro
                    , {
                        "$addFields": {
                            "Diam prom frutos de traviesa": {
                                "$divide": [
                                    {
                                        "$sum": [
                                            "$TRAVIESA Diametro del fruto 1 mm",
                                            "$TRAVIESA  Diametro del fruto 2 mm",
                                            "$TRAVIESA  Diametro del fruto 3 mm"
                                        ]
                                    }
                                    , 3]
                            }
                        }
                    }



                    //---fechas

                    , {
                        "$addFields": {
                            "dias_br1": {
                                "$divide": [
                                    {
                                        "$subtract": [
                                            "$Fecha de maduracion de brote  vegetativo Br1",
                                            "$Fecha de inicio de desarrollo vegetativo Br1"
                                        ]
                                    },
                                    86400000
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "dias_br2": {
                                "$divide": [
                                    {
                                        "$subtract": [
                                            "$Fecha de maduracion de brote vegetativo Br2",
                                            "$Fecha de inicio de desarrollo vegetativo Br2"
                                        ]
                                    },
                                    86400000
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "dias_br3": {
                                "$divide": [
                                    {
                                        "$subtract": [
                                            "$Fecha de maduracion de brote vegetativo Br3",
                                            "$Fecha de inicio de desarrollo vegetativo Br3"
                                        ]
                                    },
                                    86400000
                                ]
                            }
                        }
                    }

                    //prom dias
                    , {
                        "$addFields": {
                            "Promedio de dias necesarios para madurar el brote": {
                                "$divide": [
                                    {
                                        "$sum": [
                                            "$dias_br1",
                                            "$dias_br2",
                                            "$dias_br3"
                                        ]
                                    }
                                    , 3]
                            }
                        }
                    }


                    /*
                             "Fecha de inicio de desarrollo vegetativo Br1" : ISODate("1969-12-31T19:00:00.000-05:00"),
                       "Fecha de maduracion de brote  vegetativo Br1" : ISODate("1969-12-31T19:00:00.000-05:00"),
                       "Fecha de inicio de desarrollo vegetativo Br2" : ISODate("1969-12-31T19:00:00.000-05:00"),
                       "Fecha de maduracion de brote vegetativo Br2" : ISODate("1969-12-31T19:00:00.000-05:00"),
                       "Fecha de inicio de desarrollo vegetativo Br3" : ISODate("1969-12-31T19:00:00.000-05:00"),
                       "Fecha de maduracion de brote vegetativo Br3" : ISODate("1969-12-31T19:00:00.000-05:00"),
                   */




                    //====project final

                    , {
                        "$project": {

                            "UP": "$UP",
                            "Lote": "$Lote",
                            "Nombre": "$Nombre",
                            "Hora": "$Hora",
                            "Fecha": "$Fecha",
                            "Semana": "$Semana",
                            "Año": "$Año",
                            "Monitor": "$Monitor",
                            "X": "$X",
                            "Y": "$Y",
                            "Cuadrante Superior Norte": "$Cuadrante Superior Norte",
                            "Cuadrante Inferior Norte": "$Cuadrante Inferior Norte",
                            "Cuadrante Superior Oriente": "$Cuadrante Superior Oriente",
                            "Cuadrante Inferior Oriente": "$Cuadrante Inferior Oriente",
                            "Cuadrante Superior Sur": "$Cuadrante Superior Sur",
                            "Cuadrante Inferior Sur": "$Cuadrante Inferior Sur",
                            "Cuadrante Superior Occidente": "$Cuadrante Superior Occidente",
                            "Cuadrante Inferior Occidente": "$Cuadrante Inferior Occidente",
                            "Estados predominates del arbol": "$Estados predominates del arbol",
                            "Estado predominante 1":"$Estado predominante 1",
                            "Estado predominante 2":"$Estado predominante 2",
                            "Estado predominante 3":"$Estado predominante 3",
                            "Tipo de floracion": "$Tipo de floracion",
                            "Porcentaje de cobertura de raices": "$Porcentaje de cobertura de raices",
                            "Cantidad de cuajes marcados": "$Cantidad cuajes marcados",
                            "Long prom frutos de cosecha": "$Long prom frutos de cosecha",
                            "Diam prom frutos de cosecha": "$Diam prom frutos de cosecha",
                            "Long prom frutos de Traviesa": "$Long prom frutos de Traviesa",
                            "Diam prom frutos de traviesa": "$Diam prom frutos de traviesa",
                            "Promedio de dias necesarios para madurar el brote": "$Promedio de dias necesarios para madurar el brote"

                            , "rgDate": "$$filtro_fecha_inicio"




                        }


                    }







                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }






    ]

    // , { allowDiskUse: true }


)

    .sort({ "_id": -1 })
