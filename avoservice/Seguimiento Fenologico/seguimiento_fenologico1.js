db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-02-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_seguimientofenologico",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //FILTRO FECHAS
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$match": {
                            "Estado del seguimiento fenologico": { "$exists": true }
                        }
                    },


                    {
                        "$addFields": {
                            "variable_cartografia": "$Arbol"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    // { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    {
                        "$addFields": {
                            "X": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                            "Y": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] }
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0


                            , "Arbol": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "split_lote": { "$split": ["$lote", " "] }
                        }
                    }


                    , {
                        "$addFields": {
                            "size_split_lote": { "$size": "$split_lote" }
                        }
                    }

                    , {
                        "$addFields": {
                            "aux_size_split_lote": {
                                "$subtract": ["$size_split_lote", 2]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
                        }
                    }



                    , {
                        "$addFields": {

                            "UP": {
                                "$reduce": {
                                    "input": "$split_lote2",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", " ", "$$this"] }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
                        }
                    }


                    , {
                        "$addFields": {
                            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
                        }
                    }



                    , {
                        "$addFields": {
                            "Monitor": "$supervisor"
                        }
                    }



                    , {
                        "$project": {
                            "Point": 0
                            , "Formula": 0
                            , "uid": 0
                            , "uDate": 0
                            , "capture": 0
                            , "Sampling": 0
                            , "supervisor": 0



                            , "split_lote": 0
                            , "size_split_lote": 0
                            , "aux_size_split_lote": 0
                            , "split_lote2": 0

                        }
                    }


                    , {
                        "$addFields": {
                            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
                        }
                    }


                     , {
                        "$addFields": {
                            "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
                            ,"Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } }
                            ,"Semana": { "$week": { "date": "$rgDate" } }
                            ,"Año": { "$year": { "date": "$rgDate" } }
                        }
                    }




                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }






    ]

    // , { allowDiskUse: true }


)

    .sort({ "_id": -1 })
