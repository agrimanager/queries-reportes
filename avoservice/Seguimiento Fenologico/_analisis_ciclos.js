db.users.aggregate(
    [


        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-02-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_seguimientofenologico",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //FILTRO FECHAS
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$match": {
                            "Estado del seguimiento fenologico": { "$exists": true }
                        }
                    },


                    {
                        "$addFields": {
                            "variable_cartografia": "$Arbol"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    // { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    // {
                    //     "$addFields": {
                    //         "X": { "$arrayElemAt": ["$arbol.geometry.coordinates", 0] },
                    //         "Y": { "$arrayElemAt": ["$arbol.geometry.coordinates", 1] }
                    //     }
                    // },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0


                            , "Arbol": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "split_lote": { "$split": ["$lote", " "] }
                        }
                    }


                    , {
                        "$addFields": {
                            "size_split_lote": { "$size": "$split_lote" }
                        }
                    }

                    , {
                        "$addFields": {
                            "aux_size_split_lote": {
                                "$subtract": ["$size_split_lote", 2]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
                        }
                    }



                    , {
                        "$addFields": {

                            "UP": {
                                "$reduce": {
                                    "input": "$split_lote2",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", " ", "$$this"] }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
                        }
                    }


                    , {
                        "$addFields": {
                            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
                        }
                    }



                    , {
                        "$addFields": {
                            "Monitor": "$supervisor"
                        }
                    }



                    , {
                        "$project": {
                            "Point": 0
                            , "Formula": 0
                            , "uid": 0
                            , "uDate": 0
                            , "capture": 0
                            , "Sampling": 0
                            , "supervisor": 0



                            , "split_lote": 0
                            , "size_split_lote": 0
                            , "aux_size_split_lote": 0
                            , "split_lote2": 0

                        }
                    }


                    , {
                        "$addFields": {
                            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
                        }
                    }


                    , {
                        "$addFields": {
                            // "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } },
                            "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } }
                            , "Semana": { "$week": { "date": "$rgDate" } }
                            , "Año": { "$year": { "date": "$rgDate" } }
                        }
                    }



                    //====== ciclos


                    , {
                        "$sort": {
                            "rgDate": 1
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "nombre": "$Nombre"
                            },
                            "data": {
                                "$push": "$$ROOT"
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "datos_inicio": {
                                "$filter": {
                                    "input": "$data",
                                    "as": "item",
                                    "cond": { "$eq": ["$$item.Estado del seguimiento fenologico", "Se inicio"] }
                                }
                            }
                        }
                    }
                    // , {
                    //     "$match": {
                    //         "datos_inicio": { "$ne": [] }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "fechas_inicio": {
                    //             "$map": {
                    //                 "input": "$datos_inicio",
                    //                 "as": "item",
                    //                 "in": "$$item.rgDate"
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$addFields": {
                    //         "data_ciclos": {
                    //             "$map": {
                    //                 "input": "$fechas_inicio",
                    //                 "as": "item_fechas_inicio",
                    //                 "in": {
                    //                     "fecha_inicio": "$$item_fechas_inicio",

                    //                     "datos_ciclo": {

                    //                         "$filter": {
                    //                             "input": "$data",
                    //                             "as": "item_data",
                    //                             "cond": {
                    //                                 "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                    //                             }
                    //                         }
                    //                     }
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }




                    // , {
                    //     "$addFields": {
                    //         "data_ciclos": {
                    //             "$map": {
                    //                 "input": "$data_ciclos",
                    //                 "as": "item_data_ciclos",
                    //                 "in": {
                    //                     "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                    //                     "datos_ciclo": {


                    //                         "$reduce": {
                    //                             "input": "$$item_data_ciclos.datos_ciclo",
                    //                             "initialValue": {
                    //                                 "estado_ciclo": "Se inicio"
                    //                                 , "datos": []
                    //                             },

                    //                             "in": {
                    //                                 "estado_ciclo": {
                    //                                     "$cond": {
                    //                                         "if": { "$eq": ["$$value.estado_ciclo", "Se finalizo"] },
                    //                                         "then": "$$value.estado_ciclo",
                    //                                         "else": "$$this.Estado del seguimiento fenologico"
                    //                                     }
                    //                                 }

                    //                                 , "datos": {

                    //                                     "$cond": {
                    //                                         "if": { "$eq": ["$$value.estado_ciclo", "Se finalizo"] },
                    //                                         "then": "$$value.datos",
                    //                                         "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                    //                                     }

                    //                                 }
                    //                             }

                    //                         }

                    //                     }
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }



                    // , {
                    //     "$project": {
                    //         "data": 0
                    //     }
                    // }


                    // , { "$unwind": "$data_ciclos" }


                    // , {
                    //     "$addFields": {
                    //         "fecha_fin": {
                    //             "$reduce": {
                    //                 "input": "$data_ciclos.datos_ciclo.datos",
                    //                 "initialValue": "",
                    //                 "in": "$$this.rgDate"
                    //             }
                    //         }
                    //     }
                    // }


                    // , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data_ciclos.datos_ciclo.datos",
                    //                 {

                    //                     "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                    //                     , "ciclo_fecha_fin": "$fecha_fin"
                    //                     , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }



                    // , {
                    //     "$project": {
                    //         "Cuadrante Inferior Norte": 0,
                    //         "Cuadrante Superior Norte": 0,
                    //         "Cuadrante Inferior Oriente": 0,
                    //         "Cuadrante Superior Oriente": 0,
                    //         "Cuadrante Inferior Sur": 0,
                    //         "Cuadrante Superior Sur": 0,
                    //         "Cuadrante Inferior Occidente": 0,
                    //         "Cuadrante Superior Occidente": 0
                    //     }
                    // }





                    // //======



                    // //plantas censadas
                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$finca",
                    //             "bloque": "$bloque",
                    //             "lote": "$lote",
                    //             "arbol": "$arbol"


                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"

                    //         },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //         },
                    //         "plantas_dif_censadas_x_lote": { "$sum": 1 },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , { "$unwind": "$data" }
                    // , { "$unwind": "$data.data" }


                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data.data",
                    //                 {
                    //                     "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }



                    // , {
                    //     "$addFields": {
                    //         "ciclo_año": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    //                 "then": "",
                    //                 "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                    //             }
                    //         }
                    //         , "ciclo_semana": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    //                 "then": "",
                    //                 "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                    //             }
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
                    //         , "ciclo_fecha_fin": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    //                 "then": "",
                    //                 "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                    //             }
                    //         }
                    //     }
                    // }


                    // //     , {
                    // //         "$addFields": {
                    // //             "Estados predominates del arbol": {
                    // //                 "$cond": {
                    // //                     "if": {
                    // //                         "$isArray": ["$Estado predominante de las yemas en el arbol"]
                    // //                     },
                    // //                     "then": {
                    // //                         "$reduce": {
                    // //                             "input": "$Estado predominante de las yemas en el arbol",
                    // //                             "initialValue": "",
                    // //                             "in": {
                    // //                                 "$cond": {
                    // //                                     "if": {
                    // //                                         "$eq": [{
                    // //                                             "$indexOfArray": ["$Estado predominante de las yemas en el arbol", "$$this"]
                    // //                                         }, 0]
                    // //                                     },
                    // //                                     "then": {
                    // //                                         "$concat": ["$$value", "$$this"]
                    // //                                     },
                    // //                                     "else": {
                    // //                                         "$concat": ["$$value", "-", "$$this"]
                    // //                                     }
                    // //                                 }
                    // //                             }
                    // //                         }
                    // //                     },
                    // //                     "else": ""
                    // //                 }
                    // //             }
                    // //         }
                    // //     }


                    // //     , {
                    // //         "$addFields": {
                    // //             "Presencia de Floracion": { "$ifNull": ["$Presencia de Floracion", ""] }
                    // //             , "Tipo de floracion": { "$ifNull": ["$Presencia de Floracion_SI", ""] }
                    // //         }
                    // //     }


                    // //     , {
                    // //         "$addFields": {
                    // //             "Presencia de Floracion": { "$ifNull": ["$Porcentaje de cobertura de raices", 0] }
                    // //         }
                    // //     }

                    // //     , {
                    // //         "$addFields": {
                    // //             "Cantidad cuajes marcados": { "$ifNull": ["$Cantidad cuajes marcados", 0] }
                    // //         }
                    // //     }



                    // //cosecha longitud
                    // , {
                    //     "$addFields": {
                    //         "COSECHA Longitud del fruto 1      mm": { "$ifNull": ["$COSECHA Longitud del fruto 1      mm", ""] },
                    //         "COSECHA Longitud del fruto 2 mm": { "$ifNull": ["$COSECHA Longitud del fruto 2 mm", ""] },
                    //         "COSECHA Longitud del fruto 3 mm": { "$ifNull": ["$COSECHA Longitud del fruto 3 mm", ""] }
                    //     }
                    // }

                    // //cosecha diametro
                    // , {
                    //     "$addFields": {
                    //         "COSECHA Diametro del fruto 1 mm": { "$ifNull": ["$COSECHA Diametro del fruto 1 mm", ""] },
                    //         "COSECHA Diametro del fruto 2 mm": { "$ifNull": ["$COSECHA Diametro del fruto 2 mm", ""] },
                    //         "COSECHA Diametro del fruto 3 mm": { "$ifNull": ["$COSECHA Diametro del fruto 3 mm", ""] }
                    //     }
                    // }

                    // //traviesa longitud
                    // , {
                    //     "$addFields": {
                    //         "TRAVIESA Longitud del fruto 1      mm": { "$ifNull": ["$TRAVIESA Longitud del fruto 1      mm", ""] },
                    //         "TRAVIESA  Longitud del fruto 2 mm": { "$ifNull": ["$TRAVIESA  Longitud del fruto 2 mm", ""] },
                    //         "TRAVIESA  Longitud del fruto 3 mm": { "$ifNull": ["$TRAVIESA  Longitud del fruto 3 mm", ""] }
                    //     }
                    // }

                    // //traviesa diametro
                    // , {
                    //     "$addFields": {
                    //         "TRAVIESA Diametro del fruto 1 mm": { "$ifNull": ["$TRAVIESA Diametro del fruto 1 mm", ""] },
                    //         "TRAVIESA  Diametro del fruto 2 mm": { "$ifNull": ["$TRAVIESA  Diametro del fruto 2 mm", ""] },
                    //         "TRAVIESA  Diametro del fruto 3 mm": { "$ifNull": ["$TRAVIESA  Diametro del fruto 3 mm", ""] }
                    //     }
                    // }



                    // //----convertir en numeros



                    // //cosecha longitud
                    // , {
                    //     "$addFields": {
                    //         "COSECHA Longitud del fruto 1      mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Longitud del fruto 1      mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Longitud del fruto 1      mm"
                    //                 }
                    //             }
                    //         }
                    //         , "COSECHA Longitud del fruto 2 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Longitud del fruto 2 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Longitud del fruto 2 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "COSECHA Longitud del fruto 3 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Longitud del fruto 3 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Longitud del fruto 3 mm"
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }


                    // //cosecha diametro
                    // , {
                    //     "$addFields": {
                    //         "COSECHA Diametro del fruto 1 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Diametro del fruto 1 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Diametro del fruto 1 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "COSECHA Diametro del fruto 2 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Diametro del fruto 2 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Diametro del fruto 2 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "COSECHA Diametro del fruto 3 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$COSECHA Diametro del fruto 3 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$COSECHA Diametro del fruto 3 mm"
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // //traviesa longitud
                    // , {
                    //     "$addFields": {
                    //         "TRAVIESA Longitud del fruto 1      mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA Longitud del fruto 1      mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA Longitud del fruto 1      mm"
                    //                 }
                    //             }
                    //         }
                    //         , "TRAVIESA  Longitud del fruto 2 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA  Longitud del fruto 2 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA  Longitud del fruto 2 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "TRAVIESA  Longitud del fruto 3 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA  Longitud del fruto 3 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA  Longitud del fruto 3 mm"
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // //traviesa diametro
                    // , {
                    //     "$addFields": {
                    //         "TRAVIESA Diametro del fruto 1 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA Diametro del fruto 1 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA Diametro del fruto 1 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "TRAVIESA  Diametro del fruto 2 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA  Diametro del fruto 2 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA  Diametro del fruto 2 mm"
                    //                 }
                    //             }
                    //         }
                    //         , "TRAVIESA  Diametro del fruto 3 mm": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$TRAVIESA  Diametro del fruto 3 mm", ""] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$toDouble": "$TRAVIESA  Diametro del fruto 3 mm"
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }



                    // //---calcular promedios




                    // //cosecha longitud
                    // , {
                    //     "$addFields": {
                    //         "Long prom frutos de cosecha": {
                    //             "$divide": [
                    //                 {
                    //                     "$sum": [
                    //                         "$COSECHA Longitud del fruto 1      mm",
                    //                         "$COSECHA Longitud del fruto 2 mm",
                    //                         "$COSECHA Longitud del fruto 3 mm"
                    //                     ]
                    //                 }
                    //                 , 3]
                    //         }
                    //     }
                    // }



                    // //cosecha diametro
                    // , {
                    //     "$addFields": {
                    //         "Diam prom frutos de cosecha": {
                    //             "$divide": [
                    //                 {
                    //                     "$sum": [
                    //                         "$COSECHA Diametro del fruto 1 mm",
                    //                         "$COSECHA Diametro del fruto 2 mm",
                    //                         "$COSECHA Diametro del fruto 3 mm"
                    //                     ]
                    //                 }
                    //                 , 3]
                    //         }
                    //     }
                    // }





                    // //traviesa longitud
                    // , {
                    //     "$addFields": {
                    //         "Long prom frutos de Traviesa": {
                    //             "$divide": [
                    //                 {
                    //                     "$sum": [
                    //                         "$TRAVIESA Longitud del fruto 1      mm",
                    //                         "$TRAVIESA  Longitud del fruto 2 mm",
                    //                         "$TRAVIESA  Longitud del fruto 3 mm"
                    //                     ]
                    //                 }
                    //                 , 3]
                    //         }
                    //     }
                    // }


                    // //traviesa diametro
                    // , {
                    //     "$addFields": {
                    //         "Diam prom frutos de traviesa": {
                    //             "$divide": [
                    //                 {
                    //                     "$sum": [
                    //                         "$TRAVIESA Diametro del fruto 1 mm",
                    //                         "$TRAVIESA  Diametro del fruto 2 mm",
                    //                         "$TRAVIESA  Diametro del fruto 3 mm"
                    //                     ]
                    //                 }
                    //                 , 3]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$project": {
                    //         "TRAVIESA Longitud del fruto 1      mm": 0,
                    //         "TRAVIESA Diametro del fruto 1 mm": 0,
                    //         "TRAVIESA  Longitud del fruto 2 mm": 0,
                    //         "TRAVIESA  Diametro del fruto 2 mm": 0,
                    //         "TRAVIESA  Longitud del fruto 3 mm": 0,
                    //         "TRAVIESA  Diametro del fruto 3 mm": 0,
                    //         "COSECHA Longitud del fruto 1      mm": 0,
                    //         "COSECHA Diametro del fruto 1 mm": 0,
                    //         "COSECHA Longitud del fruto 2 mm": 0,
                    //         "COSECHA Diametro del fruto 2 mm": 0,
                    //         "COSECHA Longitud del fruto 3 mm": 0,
                    //         "COSECHA Diametro del fruto 3 mm": 0

                    //         , "Estado del seguimiento fenologico": 0

                    //     }
                    // }






                    // //---fechas


                    // , {
                    //     "$addFields": {
                    //         "dias_br1": {
                    //             "$divide": [
                    //                 {
                    //                     "$subtract": [
                    //                         "$Fecha de maduracion de brote  vegetativo Br1",
                    //                         "$Fecha de inicio de desarrollo vegetativo Br1"
                    //                     ]
                    //                 },
                    //                 86400000
                    //             ]
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "dias_br2": {
                    //             "$divide": [
                    //                 {
                    //                     "$subtract": [
                    //                         "$Fecha de maduracion de brote vegetativo Br2",
                    //                         "$Fecha de inicio de desarrollo vegetativo Br2"
                    //                     ]
                    //                 },
                    //                 86400000
                    //             ]
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "dias_br3": {
                    //             "$divide": [
                    //                 {
                    //                     "$subtract": [
                    //                         "$Fecha de maduracion de brote vegetativo Br3",
                    //                         "$Fecha de inicio de desarrollo vegetativo Br3"
                    //                     ]
                    //                 },
                    //                 86400000
                    //             ]
                    //         }
                    //     }
                    // }

                    // //prom dias
                    // , {
                    //     "$addFields": {
                    //         "Promedio de dias necesarios para madurar el brote": {
                    //             "$divide": [
                    //                 {
                    //                     "$sum": [
                    //                         "$dias_br1",
                    //                         "$dias_br2",
                    //                         "$dias_br3"
                    //                     ]
                    //                 }
                    //                 , 3]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$project": {
                    //         "Fecha de inicio de desarrollo vegetativo Br1": 0,
                    //         "Fecha de maduracion de brote  vegetativo Br1": 0,
                    //         "Fecha de inicio de desarrollo vegetativo Br2": 0,
                    //         "Fecha de maduracion de brote vegetativo Br2": 0,
                    //         "Fecha de inicio de desarrollo vegetativo Br3": 0,
                    //         "Fecha de maduracion de brote vegetativo Br3": 0

                    //         , "dias_br1": 0
                    //         , "dias_br2": 0
                    //         , "dias_br3": 0
                    //     }
                    // }






                    // //Floracion
                    // , {
                    //     "$addFields": {
                    //         "Presencia de Floracion": { "$ifNull": ["$Presencia de Floracion", ""] }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "Presencia de Floracion": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$Presencia de Floracion", ""] },
                    //                 "then": "No",
                    //                 "else": "$Presencia de Floracion"
                    //             }
                    //         }
                    //     }
                    // }




                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$finca",
                    //             "bloque": "$bloque",
                    //             "lote": "$lote",
                    //             "arbol": "$arbol"


                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"

                    //             , "presencia_floracion": "$Presencia de Floracion"

                    //         },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             , "presencia_floracion": "$_id.presencia_floracion"

                    //         },
                    //         "cantidad": { "$sum": 1 },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             // , "presencia_floracion": "$_id.presencia_floracion"

                    //         }
                    //         // ,"cantidad_total": { "$sum": "$cantidad" }
                    //         , "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_floracion_si": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.presencia_floracion", "SI"] }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_floracion_si",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_floracion_si": { "$ifNull": ["$cantidad_arboles_floracion_si.cantidad", 0] }
                    //     }
                    // }



                    // , { "$unwind": "$data" }
                    // , { "$unwind": "$data.data" }
                    // , { "$unwind": "$data.data.data" }


                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data.data.data",
                    //                 {
                    //                     "cantidad_arboles_floracion_si": "$cantidad_arboles_floracion_si"
                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }



                    // //Floracion tipo
                    // , {
                    //     "$addFields": {
                    //         "Presencia de Floracion_SI": { "$ifNull": ["$Presencia de Floracion_SI", "sin_datos"] }
                    //     }
                    // }


                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$finca",
                    //             "bloque": "$bloque",
                    //             "lote": "$lote",
                    //             "arbol": "$arbol"


                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"

                    //             , "presencia_floracion_si": "$Presencia de Floracion_SI"

                    //         },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             , "presencia_floracion_si": "$_id.presencia_floracion_si"

                    //         },
                    //         "cantidad": { "$sum": 1 },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             // , "presencia_floracion": "$_id.presencia_floracion"

                    //         }
                    //         // ,"cantidad_total": { "$sum": "$cantidad" }
                    //         , "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_floracion_indeterminada": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.presencia_floracion_si", "Indeterminada"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_floracion_indeterminada",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_floracion_indeterminada": { "$ifNull": ["$cantidad_arboles_floracion_indeterminada.cantidad", 0] }
                    //     }
                    // }

                    // // //---arreglo de PORCENTAJE
                    // // , {
                    // //     "$addFields": {
                    // //         "cantidad_arboles_floracion_determinada": {
                    // //             "$filter": {
                    // //                 "input": "$data",
                    // //                 "as": "item",
                    // //                 "cond": { "$eq": ["$$item._id.presencia_floracion_si", "Determinada"] }
                    // //             }
                    // //         }
                    // //     }
                    // // }
                    // // , {
                    // //     "$unwind": {
                    // //         "path": "$cantidad_arboles_floracion_determinada",
                    // //         "preserveNullAndEmptyArrays": true
                    // //     }
                    // // }

                    // // , {
                    // //     "$addFields": {
                    // //         "cantidad_arboles_floracion_determinada": { "$ifNull": ["$cantidad_arboles_floracion_determinada.cantidad", 0] }
                    // //     }
                    // // }





                    // , { "$unwind": "$data" }
                    // , { "$unwind": "$data.data" }
                    // , { "$unwind": "$data.data.data" }


                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data.data.data",
                    //                 {
                    //                     "cantidad_arboles_floracion_indeterminada": "$cantidad_arboles_floracion_indeterminada",
                    //                     "cantidad_arboles_floracion_determinada": "$cantidad_arboles_floracion_determinada"
                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$project": {
                    //         "Presencia de Floracion": 0,
                    //         "Presencia de Floracion_SI": 0,
                    //         "Presencia de Floracion_No": 0
                    //     }
                    // }




                    // //---arreglo de PORCENTAJE
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_floracion_determinada": {
                    //             "$subtract": [
                    //                 "$cantidad_arboles_floracion_si",
                    //                 "$cantidad_arboles_floracion_indeterminada"
                    //             ]

                    //         }
                    //     }
                    // }



                    // //---otras variables sum-prom

                    // , {
                    //     "$group": {
                    //         "_id": {


                    //             "up": "$UP",
                    //             "lote": "$Lote",
                    //             "nombre": "$Nombre"


                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"
                    //             , "ciclo_año": "$ciclo_año"
                    //             , "ciclo_semana": "$ciclo_semana"

                    //             // , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"

                    //         }
                    //         , "plantas_dif_censadas_x_lote": { "$max": "$plantas_dif_censadas_x_lote" }
                    //         // , "monitor": { "$max": "$Monitor" }
                    //         , "data": { "$push": "$$ROOT" }


                    //         //NEW
                    //         , "sum_pct_raices": { "$sum": { "$toDouble": "$Porcentaje de cobertura de raices" } },

                    //         "sum_Long prom frutos de cosecha": { "$sum": "$Long prom frutos de cosecha" },
                    //         "sum_Diam prom frutos de cosecha": { "$sum": "$Diam prom frutos de cosecha" },
                    //         "sum_Long prom frutos de Traviesa": { "$sum": "$Long prom frutos de Traviesa" },
                    //         "sum_Diam prom frutos de traviesa": { "$sum": "$Diam prom frutos de traviesa" },
                    //         "sum_Promedio de dias necesarios para madurar el brote": { "$sum": "$Promedio de dias necesarios para madurar el brote" }

                    //     }
                    // }


                    // , { "$unwind": "$data" }


                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data",
                    //                 {
                    //                     "sum_pct_raices": "$sum_pct_raices",

                    //                     "sum_Long prom frutos de cosecha": "$sum_Long prom frutos de cosecha",
                    //                     "sum_Diam prom frutos de cosecha": "$sum_Diam prom frutos de cosecha",
                    //                     "sum_Long prom frutos de Traviesa": "$sum_Long prom frutos de Traviesa",
                    //                     "sum_Diam prom frutos de traviesa": "$sum_Diam prom frutos de traviesa",
                    //                     "sum_Promedio de dias necesarios para madurar el brote": "$sum_Promedio de dias necesarios para madurar el brote"

                    //                     , "max_plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "Long prom frutos de cosecha": "$sum_Long prom frutos de cosecha",
                    //         "Diam prom frutos de cosecha": "$sum_Diam prom frutos de cosecha",
                    //         "Long prom frutos de Traviesa": "$sum_Long prom frutos de Traviesa",
                    //         "Diam prom frutos de traviesa": "$sum_Diam prom frutos de traviesa",
                    //         "Promedio de dias necesarios para madurar el brote": "$sum_Promedio de dias necesarios para madurar el brote"

                    //         , "plantas_dif_censadas_x_lote": "$max_plantas_dif_censadas_x_lote"
                    //     }
                    // }


                    // , {
                    //     "$project": {
                    //         "sum_Long prom frutos de cosecha": 0,
                    //         "sum_Diam prom frutos de cosecha": 0,
                    //         "sum_Long prom frutos de Traviesa": 0,
                    //         "sum_Diam prom frutos de traviesa": 0,
                    //         "sum_Promedio de dias necesarios para madurar el brote": 0

                    //         , "max_plantas_dif_censadas_x_lote": 0
                    //     }
                    // }


                    // //raices
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_PROM_RAICES": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$sum_pct_raices",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_PROM_RAICES": {
                    //             "$multiply": ["$PORCENTAJE_PROM_RAICES", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_PROM_RAICES": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_PROM_RAICES", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_PROM_RAICES", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // , {
                    //     "$project": {
                    //         "Porcentaje de cobertura de raices": 0
                    //     }
                    // }





                    // //--% floracion

                    // //FLORACION
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_floracion_si",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION": {
                    //             "$multiply": ["$PORCENTAJE_FLORACION", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_FLORACION", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_FLORACION", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //FLORACION_INDETERMINADA
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_INDETERMINADA": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$cantidad_arboles_floracion_si", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_floracion_indeterminada",
                    //                         "$cantidad_arboles_floracion_si"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_INDETERMINADA": {
                    //             "$multiply": ["$PORCENTAJE_FLORACION_INDETERMINADA", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_INDETERMINADA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_FLORACION_INDETERMINADA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_FLORACION_INDETERMINADA", 100] }, 1] }] }, 100] }
                    //     }
                    // }


                    // //FLORACION_DETERMINADA
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_DETERMINADA": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$cantidad_arboles_floracion_si", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_floracion_determinada",
                    //                         "$cantidad_arboles_floracion_si"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_DETERMINADA": {
                    //             "$multiply": ["$PORCENTAJE_FLORACION_DETERMINADA", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_FLORACION_DETERMINADA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_FLORACION_DETERMINADA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_FLORACION_DETERMINADA", 100] }, 1] }] }, 100] }
                    //     }
                    // }



                    // //Estado predominante de las yemas en el arbol
                    // , {
                    //     "$unwind": {
                    //         "path": "$Estado predominante de las yemas en el arbol",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , { "$addFields": { "Estado predominante de las yemas en el arbol": { "$ifNull": ["$Estado predominante de las yemas en el arbol", ""] } } }



                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$finca",
                    //             "bloque": "$bloque",
                    //             "lote": "$lote",
                    //             "arbol": "$arbol"


                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"

                    //             , "estado_yemas": "$Estado predominante de las yemas en el arbol"

                    //         },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }



                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             , "estado_yemas": "$_id.estado_yemas"

                    //         },
                    //         "cantidad": { "$sum": 1 },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {
                    //             "finca": "$_id.finca",
                    //             "bloque": "$_id.bloque",
                    //             "lote": "$_id.lote"


                    //             , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                    //             , "ciclo_estado": "$_id.ciclo_estado"

                    //             // , "estado_yemas": "$_id.estado_yemas"

                    //         },
                    //         // "cantidad": { "$sum": 1 },
                    //         "data": {
                    //             "$push": "$$ROOT"
                    //         }
                    //     }
                    // }

                    // // //estado_yema_xxx
                    // // , {
                    // //     "$addFields": {
                    // //         "cantidad_arboles_estado_xx": {
                    // //             "$filter": {
                    // //                 "input": "$data",
                    // //                 "as": "item",
                    // //                 "cond": { "$eq": ["$$item._id.estado_yemas", "xxx"] }
                    // //             }
                    // //         }
                    // //     }
                    // // }
                    // // , {
                    // //     "$unwind": {
                    // //         "path": "$cantidad_arboles_estado_xx",
                    // //         "preserveNullAndEmptyArrays": true
                    // //     }
                    // // }

                    // // , {
                    // //     "$addFields": {
                    // //         "cantidad_arboles_estado_xx": { "$ifNull": ["$cantidad_arboles_estado_xx.cantidad", 0] }
                    // //     }
                    // // }


                    // //estado_yema_0
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_0": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "0"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_0",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_0": { "$ifNull": ["$cantidad_arboles_estado_0.cantidad", 0] }
                    //     }
                    // }

                    // //estado_yema_1
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_1": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "1"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_1",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_1": { "$ifNull": ["$cantidad_arboles_estado_1.cantidad", 0] }
                    //     }
                    // }

                    // //estado_yema_6
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_6": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "6"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_6",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_6": { "$ifNull": ["$cantidad_arboles_estado_6.cantidad", 0] }
                    //     }
                    // }

                    // //estado_yema_8
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_8": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "8"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_8",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_8": { "$ifNull": ["$cantidad_arboles_estado_8.cantidad", 0] }
                    //     }
                    // }

                    // //estado_yema_11
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_11": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "11"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_11",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_11": { "$ifNull": ["$cantidad_arboles_estado_11.cantidad", 0] }
                    //     }
                    // }

                    // //estado_yema_13
                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_13": {
                    //             "$filter": {
                    //                 "input": "$data",
                    //                 "as": "item",
                    //                 "cond": { "$eq": ["$$item._id.estado_yemas", "13"] }
                    //             }
                    //         }
                    //     }
                    // }
                    // , {
                    //     "$unwind": {
                    //         "path": "$cantidad_arboles_estado_13",
                    //         "preserveNullAndEmptyArrays": true
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "cantidad_arboles_estado_13": { "$ifNull": ["$cantidad_arboles_estado_13.cantidad", 0] }
                    //     }
                    // }



                    // //  "cantidad_arboles_estado_0" : 1,
                    // // 	"cantidad_arboles_estado_1" : 1,
                    // // 	"cantidad_arboles_estado_6" : 0,
                    // // 	"cantidad_arboles_estado_8" : 6,
                    // // 	"cantidad_arboles_estado_11" : 1,
                    // // 	"cantidad_arboles_estado_13" : 2


                    // , { "$unwind": "$data" }
                    // , { "$unwind": "$data.data" }
                    // , { "$unwind": "$data.data.data" }


                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$data.data.data",
                    //                 {
                    //                     "cantidad_arboles_estado_0": "$cantidad_arboles_estado_0",
                    //                     "cantidad_arboles_estado_1": "$cantidad_arboles_estado_1",
                    //                     "cantidad_arboles_estado_6": "$cantidad_arboles_estado_6",
                    //                     "cantidad_arboles_estado_8": "$cantidad_arboles_estado_8",
                    //                     "cantidad_arboles_estado_11": "$cantidad_arboles_estado_11",
                    //                     "cantidad_arboles_estado_13": "$cantidad_arboles_estado_13"

                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }



                    // //% tipos

                    // // //sxx
                    // // , {
                    // //     "$addFields": {
                    // //         "PORCENTAJE_Sxx": {
                    // //             "$cond": {
                    // //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    // //                 "then": 0,
                    // //                 "else": {
                    // //                     "$divide": ["$cantidad_arboles_estado_xx",
                    // //                         "$plantas_dif_censadas_x_lote"]
                    // //                 }
                    // //             }
                    // //         }
                    // //     }
                    // // }

                    // // , {
                    // //     "$addFields": {
                    // //         "PORCENTAJE_Sxx": {
                    // //             "$multiply": ["$PORCENTAJE_Sxx", 100]
                    // //         }
                    // //     }
                    // // }

                    // // , {
                    // //     "$addFields": {
                    // //         "PORCENTAJE_Sxx": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_Sxx", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_Sxx", 100] }, 1] }] }, 100] }
                    // //     }
                    // // }

                    // //s0
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S0": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_0",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S0": {
                    //             "$multiply": ["$PORCENTAJE_S0", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S0": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S0", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S0", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //s1
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S1": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_1",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S1": {
                    //             "$multiply": ["$PORCENTAJE_S1", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S1": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S1", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S1", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //s6
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S6": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_6",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S6": {
                    //             "$multiply": ["$PORCENTAJE_S6", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S6": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S6", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S6", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //s8
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S8": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_8",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S8": {
                    //             "$multiply": ["$PORCENTAJE_S8", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S8": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S8", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S8", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //s11
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S11": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_11",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S11": {
                    //             "$multiply": ["$PORCENTAJE_S11", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S11": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S11", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S11", 100] }, 1] }] }, 100] }
                    //     }
                    // }

                    // //s13
                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S13": {
                    //             "$cond": {
                    //                 "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    //                 "then": 0,
                    //                 "else": {
                    //                     "$divide": ["$cantidad_arboles_estado_13",
                    //                         "$plantas_dif_censadas_x_lote"]
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S13": {
                    //             "$multiply": ["$PORCENTAJE_S13", 100]
                    //         }
                    //     }
                    // }

                    // , {
                    //     "$addFields": {
                    //         "PORCENTAJE_S13": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_S13", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_S13", 100] }, 1] }] }, 100] }
                    //     }
                    // }


                    // , {
                    //     "$project": {
                    //         "cantidad_arboles_estado_0": 0,
                    //         "cantidad_arboles_estado_1": 0,
                    //         "cantidad_arboles_estado_6": 0,
                    //         "cantidad_arboles_estado_8": 0,
                    //         "cantidad_arboles_estado_11": 0,
                    //         "cantidad_arboles_estado_13": 0
                    //     }
                    // }

                    // , {
                    //     "$group": {
                    //         "_id": {


                    //             "up": "$UP",
                    //             "lote": "$Lote",
                    //             "nombre": "$Nombre"

                    //             , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                    //             , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                    //             , "ciclo_estado": "$ciclo_estado"
                    //             , "ciclo_año": "$ciclo_año"
                    //             , "ciclo_semana": "$ciclo_semana"



                    //         }
                    //         , "plantas_dif_censadas_x_lote": { "$max": "$plantas_dif_censadas_x_lote" }
                    //         , "monitor": { "$max": "$Monitor" }
                    //         , "data": { "$push": "$$ROOT" }
                    //     }
                    // }




                    // , {
                    //     "$addFields": {
                    //         "data": { "$arrayElemAt": ["$data", 0] }
                    //     }
                    // }




                    // , {
                    //     "$addFields": {
                    //         "dias_ciclo": {
                    //             "$divide": [
                    //                 {
                    //                     "$subtract": [
                    //                         { "$dateFromString": { "dateString": "$_id.ciclo_fecha_fin", "format": "%d/%m/%Y" } },
                    //                         { "$dateFromString": { "dateString": "$_id.ciclo_fecha_inicio", "format": "%d/%m/%Y" } }
                    //                     ]
                    //                 },
                    //                 86400000
                    //             ]
                    //         }
                    //     }
                    // }


                    // , {
                    //     "$addFields": {
                    //         "dias_ciclo": { "$floor": "$dias_ciclo" }
                    //     }
                    // }



                    // //======data final

                    // , {
                    //     "$replaceRoot": {
                    //         "newRoot": {
                    //             "$mergeObjects": [
                    //                 "$_id",
                    //                 {
                    //                     "dias_ciclo": "$dias_ciclo",
                    //                     "Arboles Monitoreados": "$plantas_dif_censadas_x_lote",
                    //                     "Monitor": "$monitor",


                    //                     //......
                    //                     "% de  presencia de S-0": "$data.PORCENTAJE_S0",
                    //                     "% de  presencia de S-1": "$data.PORCENTAJE_S1",
                    //                     "% de  presencia de S-6": "$data.PORCENTAJE_S6",
                    //                     "% de  presencia de S-8": "$data.PORCENTAJE_S8",
                    //                     "% de  presencia de S-11": "$data.PORCENTAJE_S11",
                    //                     "% de  presencia de S-13": "$data.PORCENTAJE_S13",
                    //                     "% promedio de raices": "$data.PORCENTAJE_PROM_RAICES",
                    //                     "Porcentaje del lote en floración": "$data.PORCENTAJE_FLORACION",
                    //                     "Porcentaje de floracion determinada": "$data.PORCENTAJE_FLORACION_DETERMINADA",
                    //                     "Porcentaje de floracion indeterminada": "$data.PORCENTAJE_FLORACION_INDETERMINADA",
                    //                     "Long prom frutos de cosecha": "$data.Long prom frutos de cosecha",
                    //                     "Diam prom frutos de cosecha": "$data.Diam prom frutos de cosecha",
                    //                     "Long prom frutos de Traviesa": "$data.Long prom frutos de Traviesa",
                    //                     "Diam prom frutos de traviesa": "$data.Diam prom frutos de traviesa",
                    //                     "Promedio de dias necesarios para madurar el brote": "$data.Promedio de dias necesarios para madurar el brote"


                    //                     , "rgDate": "$$filtro_fecha_inicio"

                    //                 }
                    //             ]
                    //         }
                    //     }
                    // }







                ]

            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }






    ]

    // , { allowDiskUse: true }


)

    // .sort({ "_id": -1 })
