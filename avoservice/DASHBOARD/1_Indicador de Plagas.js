db.form_monitoreodeplagasyenfermedades.aggregate(


    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-05-18T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },



        {
            "$match": {
                "Estado del monitoreo": { "$exists": true }
            }
        },

        {
            "$match": {
                "$expr": {
                    "$gte": [
                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                        ,
                        { "$toDate": { "$dateFromString": { "format": "%Y-%m-%d", "dateString": "2023-01-20" } } }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0


                , "Arbol": 0
            }
        }



        , {
            "$addFields": {
                "split_lote": { "$split": ["$lote", " "] }
            }
        }


        , {
            "$addFields": {
                "size_split_lote": { "$size": "$split_lote" }
            }
        }

        , {
            "$addFields": {
                "aux_size_split_lote": {
                    "$subtract": ["$size_split_lote", 2]
                }
            }
        }

        , {
            "$addFields": {
                "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
            }
        }



        , {
            "$addFields": {

                "UP": {
                    "$reduce": {
                        "input": "$split_lote2",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", " ", "$$this"] }
                            }
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Lote": { "$arrayElemAt": ["$split_lote", -1] }
            }
        }


        , {
            "$addFields": {
                "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
            }
        }

        , {
            "$addFields": {
                "Monitor": "$supervisor"
            }
        }



        , {
            "$project": {
                "Point": 0
                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
                , "Sampling": 0
                , "supervisor": 0



                , "split_lote": 0
                , "size_split_lote": 0
                , "aux_size_split_lote": 0
                , "split_lote2": 0

                , "Observaciones": 0

            }
        }



        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
                "Año": { "$year": { "date": "$rgDate" } }
                , "Semana": { "$week": { "date": "$rgDate" } }
            }
        }


        , {
            "$addFields": {
                "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
            }
        }






        , {
            "$group": {
                "_id": {
                    "up": "$UP",
                    "nombre": "$Nombre",
                    "arbol": "$arbol"


                    , "semana": "$Semana"

                }
                , "data": { "$push": "$$ROOT" }
            }
        }


        , {
            "$group": {
                "_id": {
                    "up": "$_id.up",
                    "nombre": "$_id.nombre"
                    // "arbol": "$arbol"


                    , "semana": "$_id.semana"

                }
                , "plantas_dif_censadas_x_lote": { "$sum": 1 }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "up": "$_id.up"
                    // "nombre": "$_id.nombre"
                    // "arbol": "$arbol"


                    , "semana": "$_id.semana"

                }
                , "plantas_dif_censadas_x_up_x_semana": { "$sum": "$plantas_dif_censadas_x_lote" }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , { "$unwind": "$data.data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data.data",
                        {
                            "plantas_dif_censadas_x_up_x_semana": "$plantas_dif_censadas_x_up_x_semana"
                        }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "array_plagas_enfermedades": [



                    {
                        "nombre": "ACAROH",
                        "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                    },
                    {
                        "nombre": "ACARON",
                        "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                    },
                    {
                        "nombre": "ACAROA",
                        "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                    },

                    // {
                    //     "nombre": "MOSCAB",
                    //     "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                    // },

                    // {
                    //     "nombre": "ESCAMA",
                    //     "valor": { "$ifNull": ["$Escama", 0] }
                    // },


                    {
                        "nombre": "MONALONION",
                        "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                    },


                    // {
                    //     "nombre": "BARRENADOR",
                    //     "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                    // },
                    // {
                    //     "nombre": "MARCENO",
                    //     "valor": { "$ifNull": ["$Marceño", 0] }
                    // },
                    {
                        "nombre": "TRIPS",
                        "valor": { "$ifNull": ["$Trips", 0] }
                    },


                    // {
                    //     "nombre": "MOSCAO",
                    //     "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                    // },


                    // {
                    //     "nombre": "XILEBORUS",
                    //     "valor": { "$ifNull": ["$Xyleborus", "No"] }
                    // },


                    // {
                    //     "nombre": "STENOMA",
                    //     "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                    // },
                    // {
                    //     "nombre": "HEILIPUS",
                    //     "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                    // },
                    // {
                    //     "nombre": "COMPSUS",
                    //     "valor": { "$ifNull": ["$Compsus", 0] }
                    // },


                    // {
                    //     "nombre": "ACARTONAMIENTO",
                    //     "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                    // },



                    // {
                    //     "nombre": "CLOROSIS",
                    //     "valor": { "$ifNull": ["$Clorosis", "No"] }
                    // },



                    // {
                    //     "nombre": "CHANCRO",
                    //     "valor": { "$ifNull": ["$Chancro", "No"] }
                    // }



                    // , {
                    //     "nombre": "ONCIDERES",
                    //     "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                    // }

                    // , {
                    //     "nombre": "VERTICILIUM",
                    //     "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                    // }
                    // , {
                    //     "nombre": "FUMAGINA",
                    //     "valor": { "$ifNull": ["$Fumagina", "No"] }
                    // }
                    // , {
                    //     "nombre": "ANTRACNOSIS",
                    //     "valor": { "$ifNull": ["$Antracnosis", "No"] }
                    // }
                    // , {
                    //     "nombre": "LASIODIPLODIA",
                    //     "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                    // }




                ]

                , "array_campos_DEFOLIADORES": [

                    {
                        "nombre": "Marceño",
                        "valor": { "$ifNull": ["$Marceño", 0] }
                    },
                    {
                        "nombre": "Compsus",
                        "valor": { "$ifNull": ["$Compsus", 0] }
                    },
                    {
                        "nombre": "Pandeleteius",
                        "valor": { "$ifNull": ["$Pandeleteius", 0] }
                    },
                    {
                        "nombre": "Mimografus",
                        "valor": { "$ifNull": ["$Mimografus", 0] }
                    },
                    {
                        "nombre": "Epitrix",
                        "valor": { "$ifNull": ["$Epitrix", 0] }
                    },
                    {
                        "nombre": "Daños",
                        "valor": { "$ifNull": ["$Daños", 0] }
                    }

                ]
            }
        }


        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", ""] }
                                            , { "$eq": ["$$item.valor", "no"] }
                                            , { "$eq": ["$$item.valor", "No"] }
                                            , { "$eq": ["$$item.valor", "NO"] }


                                        ]
                                    },
                                    "then": 0,
                                    "else": "$$item.valor"
                                }


                            }

                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", "si"] }
                                            , { "$eq": ["$$item.valor", "Si"] }
                                            , { "$eq": ["$$item.valor", "SI"] }
                                        ]
                                    },
                                    "then": 1,
                                    "else": "$$item.valor"
                                }
                            }

                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                    "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                    "else": { "$toDouble": "$$item.valor" }
                                }
                            }

                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "array_campos_DEFOLIADORES": {
                    "$map": {
                        "input": "$array_campos_DEFOLIADORES",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",

                            "valor": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                    "then": "$$item.valor",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [
                                                    { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                    { "$eq": ["$$item.valor", ""] }
                                                ]
                                            },
                                            "then": 0,
                                            "else": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "DEFOLIADORES_variable_agrupada":
                {
                    "nombre": "DEFOLIADORES",
                    "valor": {
                        "$reduce": {
                            "input": "$array_campos_DEFOLIADORES.valor",
                            "initialValue": 0,
                            "in": { "$add": ["$$value", "$$this"] }
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$concatArrays": [
                        "$array_plagas_enfermedades",
                        ["$DEFOLIADORES_variable_agrupada"]
                    ]
                }
            }
        }


        , {
            "$project": {
                "Point": 0
                , "DEFOLIADORES_variable_agrupada": 0
                , "array_campos_DEFOLIADORES": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
                , "split_lote": 0
                , "Sampling": 0
                , "supervisor": 0


                , "size_split_lote": 0
                , "aux_size_split_lote": 0
                , "split_lote2": 0




                , "Acaro Huevo": 0
                , "Acaro Ninfa": 0
                , "Acaro Adulto": 0
                , "Escama": 0
                , "Tipos de Escamas": 0
                , "Marceño": 0
                , "Compsus": 0
                , "Pandeleteius": 0
                , "Mimografus": 0
                , "Epitrix": 0
                , "Larvas": 0
                , "Daños": 0
                , "Mosca Blanca": 0
                , "Trips": 0
                , "Frutos afectados por Mosca del Ovario": 0
                , "Insectos Beneficos": 0
                , "Ninfa": 0
                , "Adulto": 0
                , "Cantidad de Larvas o daños de Stenoma": 0
                , "cantidad de individuos o daños de Heilipus": 0
                , "Fumagina": 0
                , "Muerte descendente  Posible verticilium": 0
                , "Clorosis": 0
                , "Cantidad de ramas afectadas por barrenador": 0
                , "Cantidad de ramas afectadas por Oncideres": 0
                , "Xyleborus": 0
                , "Chancro": 0
                , "Antracnosis": 0
                , "Lasiodiplodia": 0
                , "Acaro": 0
                , "Defoliadores": 0
                , "Monalonion": 0
                , "Observaciones": 0
                , "Arboles afectados por Monalonion": 0
                , "Huevo": 0
                , "Acartonamiento": 0

                , "rgDate": 0


                , "Busqueda inicio": 0
                , "Busqueda fin": 0
                , "today": 0
                , "idform": 0



            }
        }






        , {
            "$group": {
                "_id": {
                    "up": "$UP"
                    , "semana": "$Semana"

                    , "plagas": "$array_plagas_enfermedades.nombre"
                }
                , "plantas_dif_censadas_x_up_x_semana": { "$max": "$plantas_dif_censadas_x_up_x_semana" }
                , "data": { "$push": "$$ROOT" }

            }
        }



        , {
            "$addFields": {
                "data_plagas": {
                    "$map": {
                        "input": "$_id.plagas",
                        "as": "item_plagas",
                        "in": {
                            "nombre": "$$item_plagas"

                            , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                                "$map": {
                                    "input": "$data",
                                    "as": "item_data",
                                    "in": {
                                        "$reduce": {
                                            "input": "$$item_data.array_plagas_enfermedades",
                                            "initialValue": 0,
                                            "in": {
                                                "$cond": {
                                                    "if": { "$eq": ["$$this.nombre", "$$item_plagas"] },
                                                    "then": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$$this.valor", 0] },
                                                            "then": { "$add": ["$$value", 0] },
                                                            "else": { "$add": ["$$value", 1] }
                                                        }
                                                    },
                                                    "else": { "$add": ["$$value", 0] }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "data_plagas2": {
                    "$map": {
                        "input": "$data_plagas",
                        "as": "item_data_plagas",
                        "in": {
                            "nombre": "$$item_data_plagas.nombre"

                            , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                                "$reduce": {
                                    "input": "$$item_data_plagas.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                                    "initialValue": 0,
                                    "in": { "$add": ["$$value", "$$this"] }
                                }
                            }
                        }
                    }
                }
            }
        }


        , {
            "$project": {
                "data": 0,
                "data_plagas": 0

                , "_id.plagas": 0
            }
        }


        , {
            "$unwind": {
                "path": "$data_plagas2",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": {
                    "$cond": {
                        "if": { "$eq": ["$plantas_dif_censadas_x_up_x_semana", 0] },
                        "then": 0,
                        "else": {
                            "$divide": ["$data_plagas2.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                                "$plantas_dif_censadas_x_up_x_semana"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": {
                    "$multiply": ["$PORCENTAJE_INIDENCIA", 100]
                }
            }
        }

        , {
            "$addFields": {
                "PORCENTAJE_INIDENCIA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, 1] }] }, 100] }
            }
        }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "plantas_dif_censadas_x_up_x_semana": "$plantas_dif_censadas_x_up_x_semana"
                            , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": "$data_plagas2.plantas_dif_censadas_x_plaga_enfermedad_x_lote"
                            , "plaga": "$data_plagas2.nombre"
                            , "PORCENTAJE_INIDENCIA": "$PORCENTAJE_INIDENCIA"
                        }
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": null
                , "max_semana": { "$max": "$semana" }
                , "data": { "$push": "$$ROOT" }

            }
        }

        , { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "max_semana": "$max_semana"
                        }
                    ]
                }
            }
        }

        , {
            "$match": {
                "$expr": {
                    "$eq": ["$semana", "$max_semana"]
                }
            }
        }


        // //----dashboard

        /*

                {
                    "nombre": "ACAROH",
                    "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                },
                {
                    "nombre": "ACARON",
                    "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                },
                {
                    "nombre": "ACAROA",
                    "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                },



                {
                    "nombre": "MONALONION",
                    "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                },



                {
                    "nombre": "TRIPS",
                    "valor": { "$ifNull": ["$Trips", 0] }
                }

                //DEFOLIADORES

        */

        , {
            "$addFields": {
                "Nivel_INCIDENCIA": {
                    "$switch": {
                        "branches": [


                            {
                                "case": { "$eq": ["$plaga", "ACAROA"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            },
                            {
                                "case": { "$eq": ["$plaga", "ACAROH"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            },
                            {
                                "case": { "$eq": ["$plaga", "ACARON"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            },

                            {
                                "case": { "$eq": ["$plaga", "MONALONION"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            },
                            {
                                "case": { "$eq": ["$plaga", "TRIPS"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            },

                            {
                                "case": { "$eq": ["$plaga", "DEFOLIADORES"] },
                                "then": {
                                    "$switch": {
                                        "branches": [
                                            {
                                                "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    ]
                                                },
                                                "then": 1
                                            },
                                            {
                                                "case": {
                                                    "$and": [
                                                        { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                        , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                    ]
                                                },
                                                "then": 2
                                            },
                                            {
                                                "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                "then": 3
                                            }

                                        ], "default": null
                                    }

                                }
                            }



                        ], "default": null
                    }
                }
            }
        }



        // , {
        //     "$addFields": {
        //         "variable_label": "$up"

        //         , "variable_dataset": "$plaga"
        //     }
        // }



        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$variable_label"
        //             , "dashboard_dataset": "$variable_dataset"
        //         }
        //         , "dashboard_cantidad": { "$sum": "$PORCENTAJE_INIDENCIA" }

        //     }
        // }

        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_dataset": "$_id.dashboard_dataset"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }

        //     }
        // }

        // , {
        //     "$sort": {
        //         "_id.dashboard_dataset": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "data_group": { "$push": "$$ROOT" }
        //         , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
        //     }
        // }

        // , { "$unwind": "$data_group" }

        // , { "$unwind": "$data_group.data_group" }

        // , {
        //     "$replaceRoot": {
        //         "newRoot": {
        //             "$mergeObjects": [
        //                 "$data_group.data_group",
        //                 {
        //                     "array_dashboard_dataset": "$array_dashboard_dataset"
        //                 }
        //             ]
        //         }
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": {
        //             "dashboard_label": "$_id.dashboard_label"
        //         }
        //         , "data_group": { "$push": "$$ROOT" }
        //     }
        // }
        // , {
        //     "$sort": {
        //         "_id.dashboard_label": 1
        //     }
        // }


        // , {
        //     "$group": {
        //         "_id": null
        //         , "dashboard_data": {
        //             "$push": "$$ROOT"
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$map": {
        //                 "input": "$dashboard_data",
        //                 "as": "item_dashboard_data",
        //                 "in": {
        //                     "$reduce": {
        //                         "input": "$$item_dashboard_data.data_group",
        //                         "initialValue": [],
        //                         "in": {
        //                             "$concatArrays": [
        //                                 "$$value",
        //                                 [
        //                                     {
        //                                         "dashboard_label": "$$this._id.dashboard_label",
        //                                         "dashboard_dataset": "$$this._id.dashboard_dataset",
        //                                         "dashboard_cantidad": "$$this.dashboard_cantidad"
        //                                     }
        //                                 ]
        //                             ]
        //                         }
        //                     }
        //                 }

        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "datos_dashboard": {
        //             "$reduce": {
        //                 "input": "$datos_dashboard",
        //                 "initialValue": [],
        //                 "in": {
        //                     "$concatArrays": [
        //                         "$$value",
        //                         "$$this"
        //                     ]
        //                 }
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "info_datasets": {
        //             "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "array_colores": [

        //             "#773e91",
        //             "#61c3ac",
        //             "#fed330",
        //             "#fc5c65",
        //             "#45aaf2",
        //             "#d1d8e0",
        //             "#635f6d",

        //             "#474787",
        //             "#218c74",
        //             "#26de81",
        //             "#b33939",
        //             "#218c74",
        //             "#227093",
        //             "#cd6133",
        //             "#84817a",
        //             "#40407a",
        //             "#33d9b2",
        //             "#ffda79",
        //             "#2c2c54",



        //             "#a55eea",
        //             "#2bcbba",
        //             "#fd9644",
        //             "#4b7bec",
        //             "#20bf6b",
        //             "#4b6584",
        //             "#ffb142",


        //             "#fc5c65",
        //             "#20bf6b",
        //             "#3867d6",
        //             "#fed330",
        //             "#a55eea",
        //             "#45aaf2",
        //             "#fa8231",
        //             "#eb3b5a",
        //             "#0fb9b1",
        //             "#d1d8e0",

        //             "#8854d0",


        //             "#63b598",
        //             "#ce7d78",
        //             "#ea9e70",
        //             "#a48a9e",
        //             "#c6e1e8",
        //             "#648177",
        //             "#0d5ac1",
        //             "#f205e6",
        //             "#1c0365",
        //             "#14a9ad",
        //             "#4ca2f9",
        //             "#a4e43f",
        //             "#d298e2",
        //             "#6119d0",
        //             "#d2737d",
        //             "#c0a43c",
        //             "#f2510e",
        //             "#651be6",
        //             "#79806e",
        //             "#61da5e",
        //             "#cd2f00",
        //             "#9348af",
        //             "#01ac53",
        //             "#c5a4fb",
        //             "#996635",
        //             "#b11573",
        //             "#4bb473",
        //             "#75d89e",
        //             "#2f3f94",
        //             "#2f7b99",
        //             "#da967d",
        //             "#34891f",
        //             "#b0d87b",
        //             "#ca4751",
        //             "#7e50a8",
        //             "#c4d647",
        //             "#e0eeb8",
        //             "#11dec1",
        //             "#289812",
        //             "#566ca0",
        //             "#ffdbe1",
        //             "#2f1179",
        //             "#935b6d",
        //             "#916988",
        //             "#513d98",
        //             "#aead3a",
        //             "#9e6d71",
        //             "#4b5bdc",
        //             "#0cd36d",
        //             "#250662",
        //             "#cb5bea",
        //             "#228916",
        //             "#ac3e1b",
        //             "#df514a",
        //             "#539397",
        //             "#880977",
        //             "#f697c1",
        //             "#ba96ce",
        //             "#679c9d",
        //             "#c6c42c",
        //             "#5d2c52",
        //             "#48b41b",
        //             "#e1cf3b",
        //             "#5be4f0",
        //             "#57c4d8",
        //             "#a4d17a",
        //             "#be608b",
        //             "#96b00c",
        //             "#088baf",
        //             "#f158bf",
        //             "#e145ba",
        //             "#ee91e3",
        //             "#05d371",
        //             "#5426e0",
        //             "#4834d0",
        //             "#802234",
        //             "#6749e8",
        //             "#0971f0",
        //             "#8fb413",
        //             "#b2b4f0",
        //             "#c3c89d",
        //             "#c9a941",
        //             "#41d158",
        //             "#fb21a3",
        //             "#51aed9",
        //             "#5bb32d",
        //             "#21538e",
        //             "#89d534",
        //             "#d36647",
        //             "#7fb411",
        //             "#0023b8",
        //             "#3b8c2a",
        //             "#986b53",
        //             "#f50422",
        //             "#983f7a",
        //             "#ea24a3",
        //             "#79352c",
        //             "#521250",
        //             "#c79ed2",
        //             "#d6dd92",
        //             "#e33e52",
        //             "#b2be57",
        //             "#fa06ec",
        //             "#1bb699",
        //             "#6b2e5f",
        //             "#64820f",
        //             "#21538e",
        //             "#89d534",
        //             "#d36647",
        //             "#7fb411",
        //             "#0023b8",
        //             "#3b8c2a",
        //             "#986b53",
        //             "#f50422",
        //             "#983f7a",
        //             "#ea24a3",
        //             "#79352c",
        //             "#521250",
        //             "#c79ed2",
        //             "#d6dd92",
        //             "#e33e52",
        //             "#b2be57",
        //             "#fa06ec",
        //             "#1bb699",
        //             "#6b2e5f",
        //             "#64820f",
        //             "#9cb64a",
        //             "#996c48",
        //             "#9ab9b7",
        //             "#06e052",
        //             "#e3a481",
        //             "#0eb621",
        //             "#fc458e",
        //             "#b2db15",
        //             "#aa226d",
        //             "#792ed8",
        //             "#73872a",
        //             "#520d3a",
        //             "#cefcb8",
        //             "#a5b3d9",
        //             "#7d1d85",
        //             "#c4fd57",
        //             "#f1ae16",
        //             "#8fe22a",
        //             "#ef6e3c",
        //             "#243eeb",
        //             "#dd93fd",
        //             "#3f8473",
        //             "#e7dbce",
        //             "#421f79",
        //             "#7a3d93",

        //             "#93f2d7",
        //             "#9b5c2a",
        //             "#15b9ee",
        //             "#0f5997",
        //             "#409188",
        //             "#911e20",
        //             "#1350ce",
        //             "#10e5b1",
        //             "#fff4d7",
        //             "#cb2582",
        //             "#ce00be",
        //             "#32d5d6",
        //             "#608572",
        //             "#c79bc2",
        //             "#00f87c",
        //             "#77772a",
        //             "#6995ba",
        //             "#fc6b57",
        //             "#f07815",
        //             "#8fd883",
        //             "#060e27",
        //             "#96e591",
        //             "#21d52e",
        //             "#d00043",
        //             "#b47162",
        //             "#1ec227",
        //             "#4f0f6f",
        //             "#1d1d58",
        //             "#947002",
        //             "#bde052",
        //             "#e08c56",
        //             "#28fcfd",
        //             "#36486a",
        //             "#d02e29",
        //             "#1ae6db",
        //             "#3e464c",
        //             "#a84a8f",
        //             "#911e7e",
        //             "#3f16d9",
        //             "#0f525f",
        //             "#ac7c0a",
        //             "#b4c086",
        //             "#c9d730",
        //             "#30cc49",
        //             "#3d6751",
        //             "#fb4c03",
        //             "#640fc1",
        //             "#62c03e",
        //             "#d3493a",
        //             "#88aa0b",
        //             "#406df9",
        //             "#615af0",
        //             "#2a3434",
        //             "#4a543f",
        //             "#79bca0",
        //             "#a8b8d4",
        //             "#00efd4",
        //             "#7ad236",
        //             "#7260d8",
        //             "#1deaa7",
        //             "#06f43a",
        //             "#823c59",
        //             "#e3d94c",
        //             "#dc1c06",
        //             "#f53b2a",
        //             "#b46238",
        //             "#2dfff6",
        //             "#a82b89",
        //             "#1a8011",
        //             "#436a9f",
        //             "#1a806a",
        //             "#4cf09d",
        //             "#c188a2",
        //             "#67eb4b",
        //             "#b308d3",
        //             "#fc7e41",
        //             "#af3101",
        //             "#71b1f4",
        //             "#a2f8a5",
        //             "#e23dd0",
        //             "#d3486d",
        //             "#00f7f9",
        //             "#474893",
        //             "#3cec35",
        //             "#1c65cb",
        //             "#5d1d0c",
        //             "#2d7d2a",
        //             "#ff3420",
        //             "#5cdd87",
        //             "#a259a4",
        //             "#e4ac44",
        //             "#1bede6",
        //             "#8798a4",
        //             "#d7790f",
        //             "#b2c24f",
        //             "#de73c2",
        //             "#d70a9c",
        //             "#88e9b8",
        //             "#c2b0e2",
        //             "#86e98f",
        //             "#ae90e2",
        //             "#1a806b",
        //             "#436a9e",
        //             "#0ec0ff"

        //         ]
        //     }
        // }




        // , {
        //     "$addFields": {
        //         "DATA_ARRAY_DATASETS": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_info_datasets",
        //                 "in": {
        //                     "label": "$$item_info_datasets",
        //                     "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
        //                     "borderColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
        //                     "borderWidth": 0,
        //                     "data": {
        //                         "$map": {
        //                             "input": "$DATA_LABELS",
        //                             "as": "item_data_labels",
        //                             "in": {
        //                                 "$reduce": {
        //                                     "input": "$datos_dashboard",
        //                                     "initialValue": 0,
        //                                     "in": {
        //                                         "$cond": {
        //                                             "if": {
        //                                                 "$and": [
        //                                                     { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
        //                                                     , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
        //                                                 ]

        //                                             },
        //                                             "then": "$$this.dashboard_cantidad",
        //                                             "else": "$$value"
        //                                         }
        //                                     }
        //                                 }
        //                             }

        //                         }

        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }




        // , { "$project": { "_id": 0 } }


        // , {
        //     "$project": {

        //         "data": {
        //             "labels": "$DATA_LABELS",
        //             "datasets": "$DATA_ARRAY_DATASETS"
        //         },
        //         "options": {
        //             "title": {
        //                 "display": true,
        //                 "text": "Chart.js Bar Chart"
        //             }
        //         }

        //     }
        // }





    ]


)
