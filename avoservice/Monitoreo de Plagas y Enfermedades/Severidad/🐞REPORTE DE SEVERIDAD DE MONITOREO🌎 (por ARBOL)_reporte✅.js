[


    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0


            , "Arbol": 0
        }
    }



    , {
        "$addFields": {
            "split_lote": { "$split": ["$lote", " "] }
        }
    }


    , {
        "$addFields": {
            "size_split_lote": { "$size": "$split_lote" }
        }
    }

    , {
        "$addFields": {
            "aux_size_split_lote": {
                "$subtract": ["$size_split_lote", 2]
            }
        }
    }

    , {
        "$addFields": {
            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
        }
    }



    , {
        "$addFields": {

            "UP": {
                "$reduce": {
                    "input": "$split_lote2",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", " ", "$$this"] }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
        }
    }


    , {
        "$addFields": {
            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
        }
    }


    , {
        "$addFields": {
            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
        }
    }

    , {
        "$addFields": {
            "Monitor": "$supervisor"
        }
    }




    , {
        "$addFields": {
            "X": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
            "Y": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
        }
    }



    , {
        "$project": {
            "Point": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "Sampling": 0
            , "supervisor": 0



            , "split_lote": 0
            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0

        }
    }



    , {
        "$addFields": {
            "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
            "Año": { "$year": { "date": "$rgDate" } },
            "Semana": { "$week": { "date": "$rgDate" } }
            , "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }



    , {
        "$addFields": {
            "array_plagas_enfermedades": [



                {
                    "nombre": "ACAROH",
                    "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                },
                {
                    "nombre": "ACARON",
                    "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                },
                {
                    "nombre": "ACAROA",
                    "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                },

                {
                    "nombre": "MOSCAB",
                    "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                },

                {
                    "nombre": "ESCAMA",
                    "valor": { "$ifNull": ["$Escama", 0] }
                },


                {
                    "nombre": "MONALONION",
                    "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                },


                {
                    "nombre": "BARRENADOR",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                },
                {
                    "nombre": "MARCENO",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "TRIPS",
                    "valor": { "$ifNull": ["$Trips", 0] }
                },


                {
                    "nombre": "MOSCAO",
                    "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                },


                {
                    "nombre": "XILEBORUS",
                    "valor": { "$ifNull": ["$Xyleborus", "No"] }
                },


                {
                    "nombre": "STENOMA",
                    "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                },
                {
                    "nombre": "HEILIPUS",
                    "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                },
                {
                    "nombre": "COMPSUS",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },


                {
                    "nombre": "ACARTONAMIENTO",
                    "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                },



                {
                    "nombre": "CLOROSIS",
                    "valor": { "$ifNull": ["$Clorosis", "No"] }
                },



                {
                    "nombre": "CHANCRO",
                    "valor": { "$ifNull": ["$Chancro", "No"] }
                }



                , {
                    "nombre": "ONCIDERES",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                }

                , {
                    "nombre": "VERTICILIUM",
                    "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                }
                , {
                    "nombre": "FUMAGINA",
                    "valor": { "$ifNull": ["$Fumagina", "No"] }
                }
                , {
                    "nombre": "ANTRACNOSIS",
                    "valor": { "$ifNull": ["$Antracnosis", "No"] }
                }
                , {
                    "nombre": "LASIODIPLODIA",
                    "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                }




            ]

            , "array_campos_DEFOLIADORES": [

                {
                    "nombre": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },
                {
                    "nombre": "Pandeleteius",
                    "valor": { "$ifNull": ["$Pandeleteius", 0] }
                },
                {
                    "nombre": "Mimografus",
                    "valor": { "$ifNull": ["$Mimografus", 0] }
                },
                {
                    "nombre": "Epitrix",
                    "valor": { "$ifNull": ["$Epitrix", 0] }
                },
                {
                    "nombre": "Daños",
                    "valor": { "$ifNull": ["$Daños", 0] }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                        , { "$eq": ["$$item.valor", "no"] }
                                        , { "$eq": ["$$item.valor", "No"] }
                                        , { "$eq": ["$$item.valor", "NO"] }


                                    ]
                                },
                                "then": 0,
                                "else": "$$item.valor"
                            }


                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", "si"] }
                                        , { "$eq": ["$$item.valor", "Si"] }
                                        , { "$eq": ["$$item.valor", "SI"] }
                                    ]
                                },
                                "then": 1,
                                "else": "$$item.valor"
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                "else": { "$toDouble": "$$item.valor" }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_campos_DEFOLIADORES": {
                "$map": {
                    "input": "$array_campos_DEFOLIADORES",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",

                        "valor": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                "then": "$$item.valor",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [
                                                { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                { "$eq": ["$$item.valor", ""] }
                                            ]
                                        },
                                        "then": 0,
                                        "else": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "DEFOLIADORES_variable_agrupada":
            {
                "nombre": "DEFOLIADORES",
                "valor": {
                    "$reduce": {
                        "input": "$array_campos_DEFOLIADORES.valor",
                        "initialValue": 0,
                        "in": { "$add": ["$$value", "$$this"] }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$concatArrays": [
                    "$array_plagas_enfermedades",
                    ["$DEFOLIADORES_variable_agrupada"]
                ]
            }
        }
    }


    , {
        "$project": {
            "Point": 0
            , "DEFOLIADORES_variable_agrupada": 0
            , "array_campos_DEFOLIADORES": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "split_lote": 0
            , "Sampling": 0
            , "supervisor": 0


            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0




            , "Acaro Huevo": 0
            , "Acaro Ninfa": 0
            , "Acaro Adulto": 0
            , "Escama": 0
            , "Tipos de Escamas": 0
            , "Marceño": 0
            , "Compsus": 0
            , "Pandeleteius": 0
            , "Mimografus": 0
            , "Epitrix": 0
            , "Larvas": 0
            , "Daños": 0
            , "Mosca Blanca": 0
            , "Trips": 0
            , "Frutos afectados por Mosca del Ovario": 0
            , "Insectos Beneficos": 0
            , "Ninfa": 0
            , "Adulto": 0
            , "Cantidad de Larvas o daños de Stenoma": 0
            , "cantidad de individuos o daños de Heilipus": 0
            , "Fumagina": 0
            , "Muerte descendente  Posible verticilium": 0
            , "Clorosis": 0
            , "Cantidad de ramas afectadas por barrenador": 0
            , "Cantidad de ramas afectadas por Oncideres": 0
            , "Xyleborus": 0
            , "Chancro": 0
            , "Antracnosis": 0
            , "Lasiodiplodia": 0
            , "Acaro": 0
            , "Defoliadores": 0
            , "Monalonion": 0
            , "Observaciones": 0
            , "Arboles afectados por Monalonion": 0
            , "Huevo": 0
            , "Acartonamiento": 0

            , "rgDate": 0


            , "Busqueda inicio": 0
            , "Busqueda fin": 0
            , "today": 0
            , "idform": 0



        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {
                            "$cond": {
                                "if": {
                                    "$in": ["$$item.nombre", ["ACAROH", "ACARON", "ACAROA", "MOSCAB"]]
                                },
                                "then": {
                                    "$divide": ["$$item.valor", 4]
                                },
                                "else": "$$item.valor"
                            }
                        }

                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": "$$item.valor",
                        "nivel": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ACAROA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ACAROH"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ACARON"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ACARTONAMIENTO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "COMPSUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 10] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 10] }
                                                                , { "$lte": ["$$item.valor", 20] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 20] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "DEFOLIADORES"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ESCAMA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "MARCENO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "MOSCAB"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 10] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 10] }
                                                                , { "$lte": ["$$item.valor", 20] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 20] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "MOSCAO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 5] }
                                                                , { "$lte": ["$$item.valor", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "TRIPS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 15] }
                                                                , { "$lte": ["$$item.valor", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },



                                    {
                                        "case": { "$eq": ["$$item.nombre", "BARRENADOR"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "CHANCRO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "CLOROSIS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "HEILIPUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "MONALONION"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "STENOMA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$$item.nombre", "XILEBORUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }



                                    ,
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ONCIDERES"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    ,
                                    {
                                        "case": { "$eq": ["$$item.nombre", "VERTICILIUM"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    ,
                                    {
                                        "case": { "$eq": ["$$item.nombre", "FUMAGINA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    ,
                                    {
                                        "case": { "$eq": ["$$item.nombre", "ANTRACNOSIS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    ,
                                    {
                                        "case": { "$eq": ["$$item.nombre", "LASIODIPLODIA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$$item.valor", 0] },
                                                        "then": 0
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 0] }
                                                                , { "$lte": ["$$item.valor", 1] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$$item.valor", 1] }
                                                                , { "$lte": ["$$item.valor", 5] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$$item.valor", 5] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }


                                ], "default": null
                            }


                        }

                    }
                }
            }
        }
    }


    , {
        "$project": {
            "finca": 0
            , "bloque": 0
            , "lote": 0
            , "linea": 0
        }
    }




    , { "$addFields": { "ACAROA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACAROA"] } } } } }, { "$unwind": "$ACAROA" }, { "$addFields": { "ACAROA": "$ACAROA.valor" } }
    , { "$addFields": { "SEV_ACAROA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACAROA"] } } } } }, { "$unwind": "$SEV_ACAROA" }, { "$addFields": { "SEV_ACAROA": "$SEV_ACAROA.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ACAROA"] } } } } }

    , { "$addFields": { "ACAROH": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACAROH"] } } } } }, { "$unwind": "$ACAROH" }, { "$addFields": { "ACAROH": "$ACAROH.valor" } }
    , { "$addFields": { "SEV_ACAROH": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACAROH"] } } } } }, { "$unwind": "$SEV_ACAROH" }, { "$addFields": { "SEV_ACAROH": "$SEV_ACAROH.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ACAROH"] } } } } }

    , { "$addFields": { "ACARON": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACARON"] } } } } }, { "$unwind": "$ACARON" }, { "$addFields": { "ACARON": "$ACARON.valor" } }
    , { "$addFields": { "SEV_ACARON": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACARON"] } } } } }, { "$unwind": "$SEV_ACARON" }, { "$addFields": { "SEV_ACARON": "$SEV_ACARON.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ACARON"] } } } } }


    , { "$addFields": { "ACARTONAMIENTO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$ACARTONAMIENTO" }, { "$addFields": { "ACARTONAMIENTO": "$ACARTONAMIENTO.valor" } }
    , { "$addFields": { "SEV_ACARTONAMIENTO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$SEV_ACARTONAMIENTO" }, { "$addFields": { "SEV_ACARTONAMIENTO": "$SEV_ACARTONAMIENTO.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ACARTONAMIENTO"] } } } } }


    , { "$addFields": { "BARRENADOR": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "BARRENADOR"] } } } } }, { "$unwind": "$BARRENADOR" }, { "$addFields": { "BARRENADOR": "$BARRENADOR.valor" } }
    , { "$addFields": { "SEV_BARRENADOR": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "BARRENADOR"] } } } } }, { "$unwind": "$SEV_BARRENADOR" }, { "$addFields": { "SEV_BARRENADOR": "$SEV_BARRENADOR.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "BARRENADOR"] } } } } }


    , { "$addFields": { "CHANCRO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "CHANCRO"] } } } } }, { "$unwind": "$CHANCRO" }, { "$addFields": { "CHANCRO": "$CHANCRO.valor" } }
    , { "$addFields": { "SEV_CHANCRO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "CHANCRO"] } } } } }, { "$unwind": "$SEV_CHANCRO" }, { "$addFields": { "SEV_CHANCRO": "$SEV_CHANCRO.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "CHANCRO"] } } } } }


    , { "$addFields": { "CLOROSIS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "CLOROSIS"] } } } } }, { "$unwind": "$CLOROSIS" }, { "$addFields": { "CLOROSIS": "$CLOROSIS.valor" } }
    , { "$addFields": { "SEV_CLOROSIS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "CLOROSIS"] } } } } }, { "$unwind": "$SEV_CLOROSIS" }, { "$addFields": { "SEV_CLOROSIS": "$SEV_CLOROSIS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "CLOROSIS"] } } } } }



    , { "$addFields": { "COMPSUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "COMPSUS"] } } } } }, { "$unwind": "$COMPSUS" }, { "$addFields": { "COMPSUS": "$COMPSUS.valor" } }
    , { "$addFields": { "SEV_COMPSUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "COMPSUS"] } } } } }, { "$unwind": "$SEV_COMPSUS" }, { "$addFields": { "SEV_COMPSUS": "$SEV_COMPSUS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "COMPSUS"] } } } } }


    , { "$addFields": { "DEFOLIADORES": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "DEFOLIADORES"] } } } } }, { "$unwind": "$DEFOLIADORES" }, { "$addFields": { "DEFOLIADORES": "$DEFOLIADORES.valor" } }
    , { "$addFields": { "SEV_DEFOLIADORES": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "DEFOLIADORES"] } } } } }, { "$unwind": "$SEV_DEFOLIADORES" }, { "$addFields": { "SEV_DEFOLIADORES": "$SEV_DEFOLIADORES.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "DEFOLIADORES"] } } } } }


    , { "$addFields": { "ESCAMA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ESCAMA"] } } } } }, { "$unwind": "$ESCAMA" }, { "$addFields": { "ESCAMA": "$ESCAMA.valor" } }
    , { "$addFields": { "SEV_ESCAMA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ESCAMA"] } } } } }, { "$unwind": "$SEV_ESCAMA" }, { "$addFields": { "SEV_ESCAMA": "$SEV_ESCAMA.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ESCAMA"] } } } } }


    , { "$addFields": { "HEILIPUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "HEILIPUS"] } } } } }, { "$unwind": "$HEILIPUS" }, { "$addFields": { "HEILIPUS": "$HEILIPUS.valor" } }
    , { "$addFields": { "SEV_HEILIPUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "HEILIPUS"] } } } } }, { "$unwind": "$SEV_HEILIPUS" }, { "$addFields": { "SEV_HEILIPUS": "$SEV_HEILIPUS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "HEILIPUS"] } } } } }


    , { "$addFields": { "MARCENO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MARCENO"] } } } } }, { "$unwind": "$MARCENO" }, { "$addFields": { "MARCENO": "$MARCENO.valor" } }
    , { "$addFields": { "SEV_MARCENO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MARCENO"] } } } } }, { "$unwind": "$SEV_MARCENO" }, { "$addFields": { "SEV_MARCENO": "$SEV_MARCENO.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "MARCENO"] } } } } }


    , { "$addFields": { "MONALONION": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MONALONION"] } } } } }, { "$unwind": "$MONALONION" }, { "$addFields": { "MONALONION": "$MONALONION.valor" } }
    , { "$addFields": { "SEV_MONALONION": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MONALONION"] } } } } }, { "$unwind": "$SEV_MONALONION" }, { "$addFields": { "SEV_MONALONION": "$SEV_MONALONION.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "MONALONION"] } } } } }


    , { "$addFields": { "MOSCAB": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MOSCAB"] } } } } }, { "$unwind": "$MOSCAB" }, { "$addFields": { "MOSCAB": "$MOSCAB.valor" } }
    , { "$addFields": { "SEV_MOSCAB": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MOSCAB"] } } } } }, { "$unwind": "$SEV_MOSCAB" }, { "$addFields": { "SEV_MOSCAB": "$SEV_MOSCAB.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "MOSCAB"] } } } } }


    , { "$addFields": { "MOSCAO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MOSCAO"] } } } } }, { "$unwind": "$MOSCAO" }, { "$addFields": { "MOSCAO": "$MOSCAO.valor" } }
    , { "$addFields": { "SEV_MOSCAO": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "MOSCAO"] } } } } }, { "$unwind": "$SEV_MOSCAO" }, { "$addFields": { "SEV_MOSCAO": "$SEV_MOSCAO.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "MOSCAO"] } } } } }


    , { "$addFields": { "STENOMA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "STENOMA"] } } } } }, { "$unwind": "$STENOMA" }, { "$addFields": { "STENOMA": "$STENOMA.valor" } }
    , { "$addFields": { "SEV_STENOMA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "STENOMA"] } } } } }, { "$unwind": "$SEV_STENOMA" }, { "$addFields": { "SEV_STENOMA": "$SEV_STENOMA.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "STENOMA"] } } } } }


    , { "$addFields": { "TRIPS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "TRIPS"] } } } } }, { "$unwind": "$TRIPS" }, { "$addFields": { "TRIPS": "$TRIPS.valor" } }
    , { "$addFields": { "SEV_TRIPS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "TRIPS"] } } } } }, { "$unwind": "$SEV_TRIPS" }, { "$addFields": { "SEV_TRIPS": "$SEV_TRIPS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "TRIPS"] } } } } }


    , { "$addFields": { "XILEBORUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "XILEBORUS"] } } } } }, { "$unwind": "$XILEBORUS" }, { "$addFields": { "XILEBORUS": "$XILEBORUS.valor" } }
    , { "$addFields": { "SEV_XILEBORUS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "XILEBORUS"] } } } } }, { "$unwind": "$SEV_XILEBORUS" }, { "$addFields": { "SEV_XILEBORUS": "$SEV_XILEBORUS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "XILEBORUS"] } } } } }



    , { "$addFields": { "ONCIDERES": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ONCIDERES"] } } } } }, { "$unwind": "$ONCIDERES" }, { "$addFields": { "ONCIDERES": "$ONCIDERES.valor" } }
    , { "$addFields": { "SEV_ONCIDERES": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ONCIDERES"] } } } } }, { "$unwind": "$SEV_ONCIDERES" }, { "$addFields": { "SEV_ONCIDERES": "$SEV_ONCIDERES.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ONCIDERES"] } } } } }


    , { "$addFields": { "VERTICILIUM": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "VERTICILIUM"] } } } } }, { "$unwind": "$VERTICILIUM" }, { "$addFields": { "VERTICILIUM": "$VERTICILIUM.valor" } }
    , { "$addFields": { "SEV_VERTICILIUM": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "VERTICILIUM"] } } } } }, { "$unwind": "$SEV_VERTICILIUM" }, { "$addFields": { "SEV_VERTICILIUM": "$SEV_VERTICILIUM.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "VERTICILIUM"] } } } } }


    , { "$addFields": { "FUMAGINA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "FUMAGINA"] } } } } }, { "$unwind": "$FUMAGINA" }, { "$addFields": { "FUMAGINA": "$FUMAGINA.valor" } }
    , { "$addFields": { "SEV_FUMAGINA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "FUMAGINA"] } } } } }, { "$unwind": "$SEV_FUMAGINA" }, { "$addFields": { "SEV_FUMAGINA": "$SEV_FUMAGINA.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "FUMAGINA"] } } } } }


    , { "$addFields": { "ANTRACNOSIS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ANTRACNOSIS"] } } } } }, { "$unwind": "$ANTRACNOSIS" }, { "$addFields": { "ANTRACNOSIS": "$ANTRACNOSIS.valor" } }
    , { "$addFields": { "SEV_ANTRACNOSIS": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "ANTRACNOSIS"] } } } } }, { "$unwind": "$SEV_ANTRACNOSIS" }, { "$addFields": { "SEV_ANTRACNOSIS": "$SEV_ANTRACNOSIS.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "ANTRACNOSIS"] } } } } }


    , { "$addFields": { "LASIODIPLODIA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "LASIODIPLODIA"] } } } } }, { "$unwind": "$LASIODIPLODIA" }, { "$addFields": { "LASIODIPLODIA": "$LASIODIPLODIA.valor" } }
    , { "$addFields": { "SEV_LASIODIPLODIA": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$eq": ["$$item.nombre", "LASIODIPLODIA"] } } } } }, { "$unwind": "$SEV_LASIODIPLODIA" }, { "$addFields": { "SEV_LASIODIPLODIA": "$SEV_LASIODIPLODIA.nivel" } }
    , { "$addFields": { "array_plagas_enfermedades": { "$filter": { "input": "$array_plagas_enfermedades", "as": "item", "cond": { "$ne": ["$$item.nombre", "LASIODIPLODIA"] } } } } }




    , {
        "$project": {
            "array_plagas_enfermedades": 0
        }
    }




]
