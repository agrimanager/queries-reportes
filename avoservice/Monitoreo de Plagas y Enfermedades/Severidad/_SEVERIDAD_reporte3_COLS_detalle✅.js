[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0


            , "Arbol": 0
        }
    }




    , {
        "$addFields": {
            "split_lote": { "$split": ["$lote", " "] }
        }
    }

    , {
        "$addFields": {
            "UP": { "$arrayElemAt": ["$split_lote", 1] },
            "Lote": { "$arrayElemAt": ["$split_lote", 2] }
        }
    }
    , {
        "$addFields": {
            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }

        }
    }



    , {
        "$addFields": {
            "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
            "Año": { "$year": { "date": "$rgDate" } },
            "Semana": { "$week": { "date": "$rgDate" } }
            , "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }
    , {
        "$addFields": {
            "Monitor": "$supervisor"
        }
    }

    , {
        "$addFields": {
            "X": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
            "Y": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
        }
    }



    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"
            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "array_plagas_enfermedades": [



                {
                    "nombre": "ACAROH",
                    "tipo": "Acaro",
                    "variable": "Acaro Huevo",
                    "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                },
                {
                    "nombre": "ACARON",
                    "tipo": "Acaro",
                    "variable": "Acaro Ninfa",
                    "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                },
                {
                    "nombre": "ACAROA",
                    "tipo": "Acaro",
                    "variable": "Acaro Adulto",
                    "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                },

                {
                    "nombre": "MOSCAB",
                    "tipo": "Mosca",
                    "variable": "Mosca Blanca",
                    "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                },

                {
                    "nombre": "ESCAMA",
                    "tipo": "Escama",
                    "variable": "Escama",
                    "valor": { "$ifNull": ["$Escama", 0] }
                },


                {
                    "nombre": "MONALONION",
                    "tipo": "otro",
                    "variable": "Arboles afectados por Monalonion",
                    "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                },


                {
                    "nombre": "BARRENADOR",
                    "tipo": "otro",
                    "variable": "Cantidad de ramas afectadas por barrenador",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                },
                {
                    "nombre": "MARCENO",
                    "tipo": "otro",
                    "variable": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "TRIPS",
                    "tipo": "otro",
                    "variable": "Trips",
                    "valor": { "$ifNull": ["$Trips", 0] }
                },


                {
                    "nombre": "MOSCAO",
                    "tipo": "Mosca",
                    "variable": "Frutos afectados por Mosca del Ovario",
                    "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                },


                {
                    "nombre": "XILEBORUS",
                    "tipo": "otro",
                    "variable": "Xyleborus",
                    "valor": { "$ifNull": ["$Xyleborus", "No"] }
                },


                {
                    "nombre": "STENOMA",
                    "tipo": "otro",
                    "variable": "Cantidad de Larvas o daños de Stenoma",
                    "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                },
                {
                    "nombre": "HEILIPUS",
                    "tipo": "otro",
                    "variable": "cantidad de individuos o daños de Heilipus",
                    "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                },
                {
                    "nombre": "COMPSUS",
                    "tipo": "otro",
                    "variable": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },


                {
                    "nombre": "ACARTONAMIENTO",
                    "tipo": "otro",
                    "variable": "Acartonamiento",
                    "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                },



                {
                    "nombre": "CLOROSIS",
                    "tipo": "otro",
                    "variable": "Clorosis",
                    "valor": { "$ifNull": ["$Clorosis", "No"] }
                },



                {
                    "nombre": "CHANCRO",
                    "tipo": "otro",
                    "variable": "Chancro",
                    "valor": { "$ifNull": ["$Chancro", "No"] }
                }




            ]

            , "array_campos_DEFOLIADORES": [

                {
                    "nombre": "Marceño",
                    "tipo": "DEFOLIADORES",
                    "variable": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "Compsus",
                    "tipo": "DEFOLIADORES",
                    "variable": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },
                {
                    "nombre": "Pandeleteius",
                    "tipo": "DEFOLIADORES",
                    "variable": "Pandeleteius",
                    "valor": { "$ifNull": ["$Pandeleteius", 0] }
                },
                {
                    "nombre": "Mimografus",
                    "tipo": "DEFOLIADORES",
                    "variable": "Mimografus",
                    "valor": { "$ifNull": ["$Mimografus", 0] }
                },
                {
                    "nombre": "Epitrix",
                    "tipo": "DEFOLIADORES",
                    "variable": "Epitrix",
                    "valor": { "$ifNull": ["$Epitrix", 0] }
                },
                {
                    "nombre": "Daños",
                    "tipo": "DEFOLIADORES",
                    "variable": "Daños",
                    "valor": { "$ifNull": ["$Daños", 0] }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                        , { "$eq": ["$$item.valor", "no"] }
                                        , { "$eq": ["$$item.valor", "No"] }
                                        , { "$eq": ["$$item.valor", "NO"] }


                                    ]
                                },
                                "then": 0,
                                "else": "$$item.valor"
                            }


                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", "si"] }
                                        , { "$eq": ["$$item.valor", "Si"] }
                                        , { "$eq": ["$$item.valor", "SI"] }
                                    ]
                                },
                                "then": 1,
                                "else": "$$item.valor"
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": { "$toDouble": "$$item.valor" }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_campos_DEFOLIADORES": {
                "$map": {
                    "input": "$array_campos_DEFOLIADORES",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {
                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                    ]
                                },
                                "then": 0,
                                "else": { "$toDouble": "$$item.valor" }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "DEFOLIADORES_variable_agrupada":
            {
                "nombre": "DEFOLIADORES",
                "tipo": "DEFOLIADORES",
                "variable": "Marceño-Compsus-Pandeleteius-Mimografus-Epitrix-Daños",
                "valor": {
                    "$reduce": {
                        "input": "$array_campos_DEFOLIADORES.valor",
                        "initialValue": 0,
                        "in": { "$add": ["$$value", "$$this"] }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$concatArrays": [
                    "$array_plagas_enfermedades",
                    ["$DEFOLIADORES_variable_agrupada"]
                ]
            }
        }
    }


    , {
        "$project": {
            "Point": 0
            , "DEFOLIADORES_variable_agrupada": 0
            , "array_campos_DEFOLIADORES": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "split_lote": 0
            , "Sampling": 0
            , "supervisor": 0



            , "Acaro Huevo": 0
            , "Acaro Ninfa": 0
            , "Acaro Adulto": 0
            , "Escama": 0
            , "Tipos de Escamas": 0
            , "Marceño": 0
            , "Compsus": 0
            , "Pandeleteius": 0
            , "Mimografus": 0
            , "Epitrix": 0
            , "Larvas": 0
            , "Daños": 0
            , "Mosca Blanca": 0
            , "Trips": 0
            , "Frutos afectados por Mosca del Ovario": 0
            , "Insectos Beneficos": 0
            , "Ninfa": 0
            , "Adulto": 0
            , "Cantidad de Larvas o daños de Stenoma": 0
            , "cantidad de individuos o daños de Heilipus": 0
            , "Fumagina": 0
            , "Muerte descendente  Posible verticilium": 0
            , "Clorosis": 0
            , "Cantidad de ramas afectadas por barrenador": 0
            , "Cantidad de ramas afectadas por Oncideres": 0
            , "Xyleborus": 0
            , "Chancro": 0
            , "Antracnosis": 0
            , "Lasiodiplodia": 0
            , "Acaro": 0
            , "Defoliadores": 0
            , "Monalonion": 0
            , "Observaciones": 0
            , "Arboles afectados por Monalonion": 0
            , "Huevo": 0
            , "Acartonamiento": 0



        }
    }


    , { "$unwind": "$array_plagas_enfermedades" }

    , {
        "$addFields": {
            "tiene_plaga_enfermedad": {
                "$cond": {
                    "if": {
                        "$gt": ["$array_plagas_enfermedades.valor", 0]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    }



    , {
        "$addFields": {
            "valor_promedio": {
                "$cond": {
                    "if": {
                        "$in": ["$array_plagas_enfermedades.nombre", ["ACAROH", "ACARON", "ACAROA", "MOSCAB"]]
                    },
                    "then": {
                        "$divide": ["$array_plagas_enfermedades.valor", 4]
                    },
                    "else": "$array_plagas_enfermedades.valor"
                }
            }
        }
    }


    , {
        "$addFields": {
            "Nivel_SEVERIDAD": {
                "$switch": {
                    "branches": [


                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "ACAROA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "ACAROH"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "ACARON"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "ACARTONAMIENTO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "COMPSUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 10] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 10] }
                                                    , { "$lte": ["$valor_promedio", 20] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 20] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "DEFOLIADORES"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "ESCAMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "MARCENO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "MOSCAB"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 10] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 10] }
                                                    , { "$lte": ["$valor_promedio", 20] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 20] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "MOSCAO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 5] }
                                                    , { "$lte": ["$valor_promedio", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "TRIPS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 15] }
                                                    , { "$lte": ["$valor_promedio", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },



                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "BARRENADOR"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "CHANCRO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "CLOROSIS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "HEILIPUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "MONALONION"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "STENOMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$array_plagas_enfermedades.nombre", "XILEBORUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$valor_promedio", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 0] }
                                                    , { "$lte": ["$valor_promedio", 1] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$valor_promedio", 1] }
                                                    , { "$lte": ["$valor_promedio", 5] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$valor_promedio", 5] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }


                    ], "default": null
                }
            }
        }
    }


    , {
        "$addFields": {
            "valor_promedio": { "$divide": [{ "$subtract": [{ "$multiply": ["$valor_promedio", 100] }, { "$mod": [{ "$multiply": ["$valor_promedio", 100] }, 1] }] }, 100] }
        }
    }



    ,
    {
        "$project": {
            "UP": "$UP"
            , "Lote": "$Lote"
            , "Nombre": "$Nombre"
            , "Fecha": "$Fecha"
            , "Año": "$Año"
            , "Semana": "$Semana"
            , "Hora": "$Hora"
            , "Monitor": "$Monitor"

            , "X": "$X"
            , "Y": "$Y"

            , "arbol": "$arbol"
            , "Plaga": "$array_plagas_enfermedades.nombre"

            , "SEVERIDAD": "$valor_promedio"
            , "Nivel SEVERIDAD": "$Nivel_SEVERIDAD"


        }
    }



    , {
        "$group": {
            "_id": {
                "_id": "$_id",

                "up": "$UP",
                "lote": "$Lote",
                "nombre": "$Nombre"

                , "arbol": "$arbol"

                , "hora": "$Hora"
                , "fecha": "$Fecha"

                , "año": "$Año"
                , "semana": "$Semana"
                , "monitor": "$Monitor"

                , "x": "$X"
                , "y": "$Y"


            }
            , "data": { "$push": "$$ROOT" }
        }
    }


    , { "$addFields": { "ACAROA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACAROA"] } } } } }, { "$unwind": "$ACAROA" }, { "$addFields": { "ACAROA": "$ACAROA.SEVERIDAD" } }
    , { "$addFields": { "ACAROH": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACAROH"] } } } } }, { "$unwind": "$ACAROH" }, { "$addFields": { "ACAROH": "$ACAROH.SEVERIDAD" } }
    , { "$addFields": { "ACARON": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACARON"] } } } } }, { "$unwind": "$ACARON" }, { "$addFields": { "ACARON": "$ACARON.SEVERIDAD" } }
    , { "$addFields": { "ACARTONAMIENTO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$ACARTONAMIENTO" }, { "$addFields": { "ACARTONAMIENTO": "$ACARTONAMIENTO.SEVERIDAD" } }
    , { "$addFields": { "BARRENADOR": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "BARRENADOR"] } } } } }, { "$unwind": "$BARRENADOR" }, { "$addFields": { "BARRENADOR": "$BARRENADOR.SEVERIDAD" } }
    , { "$addFields": { "CHANCRO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "CHANCRO"] } } } } }, { "$unwind": "$CHANCRO" }, { "$addFields": { "CHANCRO": "$CHANCRO.SEVERIDAD" } }
    , { "$addFields": { "CLOROSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "CLOROSIS"] } } } } }, { "$unwind": "$CLOROSIS" }, { "$addFields": { "CLOROSIS": "$CLOROSIS.SEVERIDAD" } }
    , { "$addFields": { "COMPSUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "COMPSUS"] } } } } }, { "$unwind": "$COMPSUS" }, { "$addFields": { "COMPSUS": "$COMPSUS.SEVERIDAD" } }
    , { "$addFields": { "DEFOLIADORES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "DEFOLIADORES"] } } } } }, { "$unwind": "$DEFOLIADORES" }, { "$addFields": { "DEFOLIADORES": "$DEFOLIADORES.SEVERIDAD" } }
    , { "$addFields": { "ESCAMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ESCAMA"] } } } } }, { "$unwind": "$ESCAMA" }, { "$addFields": { "ESCAMA": "$ESCAMA.SEVERIDAD" } }
    , { "$addFields": { "HEILIPUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "HEILIPUS"] } } } } }, { "$unwind": "$HEILIPUS" }, { "$addFields": { "HEILIPUS": "$HEILIPUS.SEVERIDAD" } }
    , { "$addFields": { "MARCENO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MARCENO"] } } } } }, { "$unwind": "$MARCENO" }, { "$addFields": { "MARCENO": "$MARCENO.SEVERIDAD" } }
    , { "$addFields": { "MONALONION": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MONALONION"] } } } } }, { "$unwind": "$MONALONION" }, { "$addFields": { "MONALONION": "$MONALONION.SEVERIDAD" } }
    , { "$addFields": { "MOSCAB": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MOSCAB"] } } } } }, { "$unwind": "$MOSCAB" }, { "$addFields": { "MOSCAB": "$MOSCAB.SEVERIDAD" } }
    , { "$addFields": { "MOSCAO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MOSCAO"] } } } } }, { "$unwind": "$MOSCAO" }, { "$addFields": { "MOSCAO": "$MOSCAO.SEVERIDAD" } }
    , { "$addFields": { "STENOMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "STENOMA"] } } } } }, { "$unwind": "$STENOMA" }, { "$addFields": { "STENOMA": "$STENOMA.SEVERIDAD" } }
    , { "$addFields": { "TRIPS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "TRIPS"] } } } } }, { "$unwind": "$TRIPS" }, { "$addFields": { "TRIPS": "$TRIPS.SEVERIDAD" } }
    , { "$addFields": { "XILEBORUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "XILEBORUS"] } } } } }, { "$unwind": "$XILEBORUS" }, { "$addFields": { "XILEBORUS": "$XILEBORUS.SEVERIDAD" } }



    , { "$addFields": { "SEV_ACAROA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACAROA"] } } } } }, { "$unwind": "$SEV_ACAROA" }, { "$addFields": { "SEV_ACAROA": "$SEV_ACAROA.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_ACAROH": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACAROH"] } } } } }, { "$unwind": "$SEV_ACAROH" }, { "$addFields": { "SEV_ACAROH": "$SEV_ACAROH.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_ACARON": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACARON"] } } } } }, { "$unwind": "$SEV_ACARON" }, { "$addFields": { "SEV_ACARON": "$SEV_ACARON.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_ACARTONAMIENTO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$SEV_ACARTONAMIENTO" }, { "$addFields": { "SEV_ACARTONAMIENTO": "$SEV_ACARTONAMIENTO.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_BARRENADOR": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "BARRENADOR"] } } } } }, { "$unwind": "$SEV_BARRENADOR" }, { "$addFields": { "SEV_BARRENADOR": "$SEV_BARRENADOR.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_CHANCRO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "CHANCRO"] } } } } }, { "$unwind": "$SEV_CHANCRO" }, { "$addFields": { "SEV_CHANCRO": "$SEV_CHANCRO.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_CLOROSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "CLOROSIS"] } } } } }, { "$unwind": "$SEV_CLOROSIS" }, { "$addFields": { "SEV_CLOROSIS": "$SEV_CLOROSIS.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_COMPSUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "COMPSUS"] } } } } }, { "$unwind": "$SEV_COMPSUS" }, { "$addFields": { "SEV_COMPSUS": "$SEV_COMPSUS.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_DEFOLIADORES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "DEFOLIADORES"] } } } } }, { "$unwind": "$SEV_DEFOLIADORES" }, { "$addFields": { "SEV_DEFOLIADORES": "$SEV_DEFOLIADORES.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_ESCAMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "ESCAMA"] } } } } }, { "$unwind": "$SEV_ESCAMA" }, { "$addFields": { "SEV_ESCAMA": "$SEV_ESCAMA.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_HEILIPUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "HEILIPUS"] } } } } }, { "$unwind": "$SEV_HEILIPUS" }, { "$addFields": { "SEV_HEILIPUS": "$SEV_HEILIPUS.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_MARCENO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MARCENO"] } } } } }, { "$unwind": "$SEV_MARCENO" }, { "$addFields": { "SEV_MARCENO": "$SEV_MARCENO.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_MONALONION": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MONALONION"] } } } } }, { "$unwind": "$SEV_MONALONION" }, { "$addFields": { "SEV_MONALONION": "$SEV_MONALONION.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_MOSCAB": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MOSCAB"] } } } } }, { "$unwind": "$SEV_MOSCAB" }, { "$addFields": { "SEV_MOSCAB": "$SEV_MOSCAB.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_MOSCAO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "MOSCAO"] } } } } }, { "$unwind": "$SEV_MOSCAO" }, { "$addFields": { "SEV_MOSCAO": "$SEV_MOSCAO.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_STENOMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "STENOMA"] } } } } }, { "$unwind": "$SEV_STENOMA" }, { "$addFields": { "SEV_STENOMA": "$SEV_STENOMA.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_TRIPS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "TRIPS"] } } } } }, { "$unwind": "$SEV_TRIPS" }, { "$addFields": { "SEV_TRIPS": "$SEV_TRIPS.Nivel SEVERIDAD" } }
    , { "$addFields": { "SEV_XILEBORUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.Plaga", "XILEBORUS"] } } } } }, { "$unwind": "$SEV_XILEBORUS" }, { "$addFields": { "SEV_XILEBORUS": "$SEV_XILEBORUS.Nivel SEVERIDAD" } }





    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {


                        "ACAROA": "$ACAROA",
                        "ACAROH": "$ACAROH",
                        "ACARON": "$ACARON",
                        "ACARTONAMIENTO": "$ACARTONAMIENTO",
                        "BARRENADOR": "$BARRENADOR",
                        "CHANCRO": "$CHANCRO",
                        "CLOROSIS": "$CLOROSIS",
                        "COMPSUS": "$COMPSUS",
                        "DEFOLIADORES": "$DEFOLIADORES",
                        "ESCAMA": "$ESCAMA",
                        "HEILIPUS": "$HEILIPUS",
                        "MARCENO": "$MARCENO",
                        "MONALONION": "$MONALONION",
                        "MOSCAB": "$MOSCAB",
                        "MOSCAO": "$MOSCAO",
                        "STENOMA": "$STENOMA",
                        "TRIPS": "$TRIPS",
                        "XILEBORUS": "$XILEBORUS",





                        "SEV_ACAROA": "$SEV_ACAROA",
                        "SEV_ACAROH": "$SEV_ACAROH",
                        "SEV_ACARON": "$SEV_ACARON",
                        "SEV_ACARTONAMIENTO": "$SEV_ACARTONAMIENTO",
                        "SEV_BARRENADOR": "$SEV_BARRENADOR",
                        "SEV_CHANCRO": "$SEV_CHANCRO",
                        "SEV_CLOROSIS": "$SEV_CLOROSIS",
                        "SEV_COMPSUS": "$SEV_COMPSUS",
                        "SEV_DEFOLIADORES": "$SEV_DEFOLIADORES",
                        "SEV_ESCAMA": "$SEV_ESCAMA",
                        "SEV_HEILIPUS": "$SEV_HEILIPUS",
                        "SEV_MARCENO": "$SEV_MARCENO",
                        "SEV_MONALONION": "$SEV_MONALONION",
                        "SEV_MOSCAB": "$SEV_MOSCAB",
                        "SEV_MOSCAO": "$SEV_MOSCAO",
                        "SEV_STENOMA": "$SEV_STENOMA",
                        "SEV_TRIPS": "$SEV_TRIPS",
                        "SEV_XILEBORUS": "$SEV_XILEBORUS"



                    }
                ]
            }
        }
    }




]
