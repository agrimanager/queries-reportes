db.form_monitoreodeplagasyenfermedades.aggregate(

    [


        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-07-10T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        // {
        //     $sort: {
        //         rgDate: -1
        //     }
        // },
        //----------------------------------


        //================================
        //-----QUERY REPORTE

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "cartography_id": { "$ifNull": ["$arbol._id", 0] }
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0


                , "Arbol": 0

                , "Estado del monitoreo": 0
            }
        }



        , {
            "$addFields": {
                "split_lote": { "$split": ["$lote", " "] }
            }
        }


        , {
            "$addFields": {
                "size_split_lote": { "$size": "$split_lote" }
            }
        }

        , {
            "$addFields": {
                "aux_size_split_lote": {
                    "$subtract": ["$size_split_lote", 2]
                }
            }
        }

        , {
            "$addFields": {
                "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
            }
        }



        , {
            "$addFields": {

                "UP": {
                    "$reduce": {
                        "input": "$split_lote2",
                        "initialValue": "",
                        "in": {
                            "$cond": {
                                "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                "then": "$$this",
                                "else": { "$concat": ["$$value", " ", "$$this"] }
                            }
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Lote": { "$arrayElemAt": ["$split_lote", -1] }
            }
        }


        , {
            "$addFields": {
                "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
            }
        }


        , {
            "$addFields": {
                "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
            }
        }

        , {
            "$addFields": {
                "Monitor": "$supervisor"
            }
        }

        //---


        // , {
        //     "$addFields": {
        //         "X": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
        //         "Y": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
        //     }
        // }



        , {
            "$project": {
                "Point": 0
                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
                , "Sampling": 0
                , "supervisor": 0


                //aux
                , "split_lote": 0
                , "size_split_lote": 0
                , "aux_size_split_lote": 0
                , "split_lote2": 0

            }
        }



        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
                "Año": { "$year": { "date": "$rgDate" } },
                "Semana": { "$week": { "date": "$rgDate" } }
                // , "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }



        , {
            "$addFields": {
                "array_plagas_enfermedades": [



                    {
                        "nombre": "ACAROH",
                        "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                    },
                    {
                        "nombre": "ACARON",
                        "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                    },
                    {
                        "nombre": "ACAROA",
                        "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                    },

                    {
                        "nombre": "MOSCAB",

                        "valor": { "$ifNull": ["$Mosca Blanca", { "$ifNull": ["$Numero de hojas afectadas por Mosca Blanca", 0] }] }

                    },

                    {
                        "nombre": "ESCAMA",
                        "valor": { "$ifNull": ["$Escama", { "$ifNull": ["$Numero de hojas afectadas por escama", 0] }] }
                    },


                    {
                        "nombre": "MONALONION",
                        "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                    },


                    {
                        "nombre": "BARRENADOR",
                        "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                    },
                    {
                        "nombre": "MARCENO",
                        "valor": { "$ifNull": ["$Marceño", 0] }
                    },
                    {
                        "nombre": "TRIPS",
                        "valor": { "$ifNull": ["$Trips", { "$ifNull": ["$Numero de Trips por panicula", 0] }] }
                    },


                    {
                        "nombre": "MOSCAO",
                        "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", { "$ifNull": ["$Paniculas afectadas por Mosca del Ovario", 0] }] }
                    },


                    {
                        "nombre": "XILEBORUS",
                        "valor": { "$ifNull": ["$Xyleborus", "No"] }
                    },


                    {
                        "nombre": "STENOMA",
                        "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                    },
                    {
                        "nombre": "HEILIPUS",
                        "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                    },
                    {
                        "nombre": "COMPSUS",
                        "valor": { "$ifNull": ["$Compsus", 0] }
                    },


                    {
                        "nombre": "ACARTONAMIENTO",
                        "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                    },



                    {
                        "nombre": "CLOROSIS",
                        "valor": { "$ifNull": ["$Clorosis", "No"] }
                    },



                    {
                        "nombre": "CHANCRO",
                        "valor": { "$ifNull": ["$Chancro", "No"] }
                    }



                    , {
                        "nombre": "ONCIDERES",
                        "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                    }

                    , {
                        "nombre": "VERTICILIUM",
                        "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                    }
                    , {
                        "nombre": "FUMAGINA",
                        "valor": { "$ifNull": ["$Fumagina", "No"] }
                    }
                    , {
                        "nombre": "ANTRACNOSIS",
                        "valor": { "$ifNull": ["$Antracnosis", "No"] }
                    }
                    , {
                        "nombre": "LASIODIPLODIA",
                        "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                    }


                    , {
                        "nombre": "MIRIDAE",
                        "valor": { "$ifNull": ["$Individuos de Chinche Miridae", 0] }
                    }
                    , {
                        "nombre": "PEGA PEGA",
                        "valor": { "$ifNull": ["$Pega pega", "No"] }
                    }
                    , {
                        "nombre": "COPTURUMIMUS",
                        "valor": { "$ifNull": ["$Copturumimus", "NO"] }
                    }
                    , {
                        "nombre": "ABEJAS",
                        "valor": { "$ifNull": ["$Presencia de Abejas", "No"] }
                    }





                ]

                , "array_campos_DEFOLIADORES": [

                    {
                        "nombre": "Marceño",
                        "valor": { "$ifNull": ["$Marceño", 0] }
                    },
                    {
                        "nombre": "Compsus",
                        "valor": { "$ifNull": ["$Compsus", 0] }
                    },
                    {
                        "nombre": "Pandeleteius",
                        "valor": { "$ifNull": ["$Pandeleteius", 0] }
                    },
                    {
                        "nombre": "Mimografus",
                        "valor": { "$ifNull": ["$Mimografus", 0] }
                    },
                    {
                        "nombre": "Epitrix",
                        "valor": { "$ifNull": ["$Epitrix", 0] }
                    },
                    {
                        "nombre": "Daños",
                        "valor": { "$ifNull": ["$Daños", 0] }
                    }

                    ,
                    {
                        "nombre": "Larvas",
                        "valor": { "$ifNull": ["$Larvas", 0] }
                    }

                ]
            }
        }


        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", ""] }
                                            , { "$eq": ["$$item.valor", "no"] }
                                            , { "$eq": ["$$item.valor", "No"] }
                                            , { "$eq": ["$$item.valor", "NO"] }


                                        ]
                                    },
                                    "then": 0,
                                    "else": "$$item.valor"
                                }


                            }

                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", "si"] }
                                            , { "$eq": ["$$item.valor", "Si"] }
                                            , { "$eq": ["$$item.valor", "SI"] }
                                        ]
                                    },
                                    "then": 1,
                                    "else": "$$item.valor"
                                }
                            }

                        }
                    }
                }
            }
        }

        , {
                 "$addFields": {
                     "array_plagas_enfermedades": {
                         "$map": {
                             "input": "$array_plagas_enfermedades",
                             "as": "item",
                             "in": {
                                 "nombre": "$$item.nombre",
                                 "valor": {
                                     "$cond": {
                                         "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                         "then": "$$item.valor",
                                         "else": {
                                             "$cond": {
                                                 "if": {
                                                     "$and": [
                                                         { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                         {
                                                             "$or": [
                                                                 { "$eq": ["$$item.valor", ""] }
                                                                 , { "$eq": ["$$item.valor", " "] }
                                                                 , { "$eq": ["$$item.valor", "  "] }

                                                                 , { "$eq": ["$$item.valor", "."] }
                                                                 , { "$eq": ["$$item.valor", ","] }
                                                                 , { "$eq": ["$$item.valor", ";"] }

                                                                 , { "$eq": ["$$item.valor", "-"] }
                                                                 , { "$eq": ["$$item.valor", "+"] }
                                                                 , { "$eq": ["$$item.valor", "/"] }
                                                                 , { "$eq": ["$$item.valor", "*"] }
                                                             ]
                                                         }
                                                     ]
                                                 },
                                                 "then": 0,
                                                 "else": {
                                                     "$cond": {
                                                         "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                         "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                                         "else": { "$toDouble": "$$item.valor" }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }

                             }
                         }
                     }
                 }
             }

        , {
                 "$addFields": {
                     "array_campos_DEFOLIADORES": {
                         "$map": {
                             "input": "$array_campos_DEFOLIADORES",
                             "as": "item",
                             "in": {
                                 "nombre": "$$item.nombre",
                                 "valor": {
                                     "$cond": {
                                         "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                         "then": "$$item.valor",
                                         "else": {
                                             "$cond": {
                                                 "if": {
                                                     "$and": [
                                                         { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                         {
                                                             "$or": [
                                                                 { "$eq": ["$$item.valor", ""] }
                                                                 , { "$eq": ["$$item.valor", " "] }
                                                                 , { "$eq": ["$$item.valor", "  "] }

                                                                 , { "$eq": ["$$item.valor", "."] }
                                                                 , { "$eq": ["$$item.valor", ","] }
                                                                 , { "$eq": ["$$item.valor", ";"] }

                                                                 , { "$eq": ["$$item.valor", "-"] }
                                                                 , { "$eq": ["$$item.valor", "+"] }
                                                                 , { "$eq": ["$$item.valor", "/"] }
                                                                 , { "$eq": ["$$item.valor", "*"] }
                                                             ]
                                                         }
                                                     ]
                                                 },
                                                 "then": 0,
                                                 "else": {
                                                     "$cond": {
                                                         "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                         "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                                         "else": { "$toDouble": "$$item.valor" }
                                                     }
                                                 }
                                             }
                                         }
                                     }
                                 }

                             }
                         }
                     }
                 }
             }

        , {
            "$addFields": {
                "DEFOLIADORES_variable_agrupada":
                {
                    "nombre": "DEFOLIADORES",
                    "valor": {
                        "$reduce": {
                            "input": "$array_campos_DEFOLIADORES.valor",
                            "initialValue": 0,
                            "in": { "$add": ["$$value", "$$this"] }
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$concatArrays": [
                        "$array_plagas_enfermedades",
                        ["$DEFOLIADORES_variable_agrupada"]
                    ]
                }
            }
        }


        , {
            "$project": {
                "Point": 0
                , "DEFOLIADORES_variable_agrupada": 0
                , "array_campos_DEFOLIADORES": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
                , "split_lote": 0
                , "Sampling": 0
                , "supervisor": 0


                , "size_split_lote": 0
                , "aux_size_split_lote": 0
                , "split_lote2": 0




                , "Acaro Huevo": 0
                , "Acaro Ninfa": 0
                , "Acaro Adulto": 0
                , "Escama": 0
                , "Tipos de Escamas": 0
                , "Marceño": 0
                , "Compsus": 0
                , "Pandeleteius": 0
                , "Mimografus": 0
                , "Epitrix": 0
                , "Larvas": 0
                , "Daños": 0
                , "Mosca Blanca": 0
                , "Trips": 0
                , "Frutos afectados por Mosca del Ovario": 0
                , "Insectos Beneficos": 0
                , "Ninfa": 0
                , "Adulto": 0
                , "Cantidad de Larvas o daños de Stenoma": 0
                , "cantidad de individuos o daños de Heilipus": 0
                , "Fumagina": 0
                , "Muerte descendente  Posible verticilium": 0
                , "Clorosis": 0
                , "Cantidad de ramas afectadas por barrenador": 0
                , "Cantidad de ramas afectadas por Oncideres": 0
                , "Xyleborus": 0
                , "Chancro": 0
                , "Antracnosis": 0
                , "Lasiodiplodia": 0
                , "Acaro": 0
                , "Defoliadores": 0
                , "Monalonion": 0
                , "Observaciones": 0
                , "Arboles afectados por Monalonion": 0
                , "Huevo": 0
                , "Acartonamiento": 0

                , "rgDate": 0


                , "Busqueda inicio": 0
                , "Busqueda fin": 0
                , "today": 0
                // , "idform": 0

                , "Keys": 0
                , "user": 0
                , "FincaID": 0

                , "uDate día": 0
                , "uDate mes": 0
                , "uDate año": 0
                , "uDate hora": 0




                , "Numero de hojas afectadas por escama": 0
                , "Numero de hojas afectadas por Mosca Blanca": 0
                , "Paniculas afectadas por Mosca del Ovario": 0
                , "Individuos de Chinche Miridae": 0
                , "Pega pega": 0
                , "Numero de Trips por panicula": 0
                , "Presencia de Abejas": 0
                , "Copturumimus": 0



            }
        }





        // //======SEVERIDAD
        // //----new

        //valor promedio
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": {
                                "$cond": {
                                    "if": {
                                        // "$in": ["$$item.nombre", ["ACAROH", "ACARON", "ACAROA", "MOSCAB"]]
                                        "$in": ["$$item.nombre", ["ACAROH", "ACARON", "ACAROA", "MOSCAB", "ESCAMA", "MARCENO", "TRIPS", "DEFOLIADORES", "COMPSUS"]]
                                    },
                                    "then": {
                                        "$divide": ["$$item.valor", 4]
                                    },
                                    "else": "$$item.valor"
                                }
                            }

                        }
                    }
                }
            }
        }


        //nivel severidad
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "valor": "$$item.valor",
                            "nivel": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ACAROA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ACAROH"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ACARON"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ACARTONAMIENTO"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "COMPSUS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 10] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 10] }
                                                                    , { "$lte": ["$$item.valor", 20] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 20] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "DEFOLIADORES"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ESCAMA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "MARCENO"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "MOSCAB"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 10] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 10] }
                                                                    , { "$lte": ["$$item.valor", 20] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 20] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "MOSCAO"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 5] }
                                                                    , { "$lte": ["$$item.valor", 10] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 10] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "TRIPS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 15] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 15] }
                                                                    , { "$lte": ["$$item.valor", 30] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 30] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },



                                        {
                                            "case": { "$eq": ["$$item.nombre", "BARRENADOR"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "CHANCRO"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "CLOROSIS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "HEILIPUS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "MONALONION"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "STENOMA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        },
                                        {
                                            "case": { "$eq": ["$$item.nombre", "XILEBORUS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }



                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ONCIDERES"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "VERTICILIUM"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "FUMAGINA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ANTRACNOSIS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "LASIODIPLODIA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }


                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "MIRIDAE"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }


                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "PEGA PEGA"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "COPTURUMIMUS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }
                                        ,
                                        {
                                            "case": { "$eq": ["$$item.nombre", "ABEJAS"] },
                                            "then": {
                                                "$switch": {
                                                    "branches": [
                                                        {
                                                            "case": { "$eq": ["$$item.valor", 0] },
                                                            "then": 0
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 0] }
                                                                    , { "$lte": ["$$item.valor", 1] }
                                                                ]
                                                            },
                                                            "then": 1
                                                        },
                                                        {
                                                            "case": {
                                                                "$and": [
                                                                    { "$gt": ["$$item.valor", 1] }
                                                                    , { "$lte": ["$$item.valor", 5] }
                                                                ]
                                                            },
                                                            "then": 2
                                                        },
                                                        {
                                                            "case": { "$gt": ["$$item.valor", 5] },
                                                            "then": 3
                                                        }

                                                    ], "default": null
                                                }

                                            }
                                        }


                                    ], "default": null
                                }


                            }

                        }
                    }
                }
            }
        }


        , {
            "$project": {
                "finca": 0
                , "bloque": 0
                , "lote": 0
                , "linea": 0

                , "_id": 0
            }
        }


        //================================
        //-----MAPA




        , {
            "$lookup": {
                "from": "cartography",
                "localField": "cartography_id",
                "foreignField": "_id",
                "as": "info_arbol"
            }
        }
        , { "$unwind": "$info_arbol" }
        , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_arbol.geometry", {}] } } }
        , {
            "$project": {
                "info_arbol": 0
            }
        }


        , { "$unwind": "$array_plagas_enfermedades" }




        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 0] }
                                , "then": "#808080"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 1] }
                                , "then": "#008000"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 2] }
                                , "then": "#ffff00"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 3] }
                                , "then": "#ff0000"
                            }

                        ],
                        "default": "#808080"
                    }
                }

                , "rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 0] }
                                , "then": "Nivel 0"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 1] }
                                , "then": "Nivel 1"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 2] }
                                , "then": "Nivel 2"
                            },
                            {
                                "case": { "$eq": ["$array_plagas_enfermedades.nivel", 3] }
                                , "then": "Nivel 3"
                            }

                        ],
                        "default": "Nivel 0"
                    }
                }

            }
        }



        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }


        , {
            "$project": {

                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",


                "properties": {



                    "UP": "$UP",
                    "Lote": "$Lote",
                    "Nombre": "$Nombre",
                    "arbol": "$arbol",

                    "Monitor": "$Monitor",
                    "Fecha": "$Fecha",
                    "Año": { "$toString": "$Año" },
                    "Semana": { "$toString": "$Semana" }


                    , "Plaga": "$array_plagas_enfermedades.nombre"

                    , "Nivel": { "$ifNull": ["$rango", "SIN DATOS"] }
                    , "color": "$color"

                }

            }
        }






    ]

    // , { allowDiskUse: true }
)

    // .count()
