[





    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_monitoreodeplagasyenfermedades",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [



                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },



                {
                    "$match": {
                        "Estado del monitoreo": { "$exists": true }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateFromString": { "format": "%Y-%m-%d", "dateString": "2023-01-20" } } }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia": "$Arbol"
                    }
                },
                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": {
                                    "$eq": [
                                        { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                        , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0


                        , "Arbol": 0
                    }
                }



                , {
                    "$addFields": {
                        "split_lote": { "$split": ["$lote", " "] }
                    }
                }


                , {
                    "$addFields": {
                        "size_split_lote": { "$size": "$split_lote" }
                    }
                }

                , {
                    "$addFields": {
                        "aux_size_split_lote": {
                            "$subtract": ["$size_split_lote", 2]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
                    }
                }



                , {
                    "$addFields": {

                        "UP": {
                            "$reduce": {
                                "input": "$split_lote2",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                        "then": "$$this",
                                        "else": { "$concat": ["$$value", " ", "$$this"] }
                                    }
                                }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "Lote": { "$arrayElemAt": ["$split_lote", -1] }
                    }
                }


                , {
                    "$addFields": {
                        "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
                    }
                }

                , {
                    "$addFields": {
                        "Monitor": "$supervisor"
                    }
                }



                , {
                    "$project": {
                        "Point": 0
                        , "Formula": 0
                        , "uid": 0
                        , "uDate": 0
                        , "capture": 0
                        , "Sampling": 0
                        , "supervisor": 0



                        , "split_lote": 0
                        , "size_split_lote": 0
                        , "aux_size_split_lote": 0
                        , "split_lote2": 0

                        , "Observaciones": 0

                    }
                }



                , {
                    "$addFields": {
                        "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
                        "Año": { "$year": { "date": "$rgDate" } }
                    }
                }






                , {
                    "$sort": {
                        "rgDate": 1
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "nombre": "$Nombre"
                        },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }





                , {
                    "$addFields": {
                        "datos_inicio": {
                            "$filter": {
                                "input": "$data",
                                "as": "item",
                                "cond": { "$eq": ["$$item.Estado del monitoreo", "Inicio"] }
                            }
                        }
                    }
                }
                , {
                    "$match": {
                        "datos_inicio": { "$ne": [] }
                    }
                }

                , {
                    "$addFields": {
                        "fechas_inicio": {
                            "$map": {
                                "input": "$datos_inicio",
                                "as": "item",
                                "in": "$$item.rgDate"
                            }
                        }
                    }
                }
                , {
                    "$addFields": {
                        "data_ciclos": {
                            "$map": {
                                "input": "$fechas_inicio",
                                "as": "item_fechas_inicio",
                                "in": {
                                    "fecha_inicio": "$$item_fechas_inicio",

                                    "datos_ciclo": {

                                        "$filter": {
                                            "input": "$data",
                                            "as": "item_data",
                                            "cond": {
                                                "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "data_ciclos": {
                            "$map": {
                                "input": "$data_ciclos",
                                "as": "item_data_ciclos",
                                "in": {
                                    "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                                    "datos_ciclo": {


                                        "$reduce": {
                                            "input": "$$item_data_ciclos.datos_ciclo",
                                            "initialValue": {
                                                "estado_ciclo": "Inicio"
                                                , "datos": []
                                            },

                                            "in": {
                                                "estado_ciclo": {
                                                    "$cond": {
                                                        "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                                        "then": "$$value.estado_ciclo",
                                                        "else": "$$this.Estado del monitoreo"
                                                    }
                                                }

                                                , "datos": {

                                                    "$cond": {
                                                        "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                                        "then": "$$value.datos",
                                                        "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                                                    }

                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }
                    }
                }



                , {
                    "$project": {
                        "data": 0
                    }
                }


                , { "$unwind": "$data_ciclos" }


                , {
                    "$addFields": {
                        "fecha_fin": {
                            "$reduce": {
                                "input": "$data_ciclos.datos_ciclo.datos",
                                "initialValue": "",
                                "in": "$$this.rgDate"
                            }
                        }
                    }
                }


                , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data_ciclos.datos_ciclo.datos",
                                {

                                    "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                                    , "ciclo_fecha_fin": "$fecha_fin"
                                    , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                                }
                            ]
                        }
                    }
                }








                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "arbol": "$arbol"


                            , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                            , "ciclo_estado": "$ciclo_estado"

                        },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$_id.finca",
                            "bloque": "$_id.bloque",
                            "lote": "$_id.lote"


                            , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                            , "ciclo_estado": "$_id.ciclo_estado"

                        },
                        "plantas_dif_censadas_x_lote": { "$sum": 1 },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }

                , { "$unwind": "$data" }
                , { "$unwind": "$data.data" }


                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data.data",
                                {
                                    "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                }
                            ]
                        }
                    }
                }



                , {
                    "$addFields": {
                        "array_plagas_enfermedades": [



                            {
                                "nombre": "ACAROH",
                                "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                            },
                            {
                                "nombre": "ACARON",
                                "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                            },
                            {
                                "nombre": "ACAROA",
                                "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                            },

                            {
                                "nombre": "MOSCAB",

                                "valor": { "$ifNull": ["$Mosca Blanca", { "$ifNull": ["$Numero de hojas afectadas por Mosca Blanca", 0] }] }

                            },

                            {
                                "nombre": "ESCAMA",
                                "valor": { "$ifNull": ["$Escama", { "$ifNull": ["$Numero de hojas afectadas por escama", 0] }] }
                            },


                            {
                                "nombre": "MONALONION",
                                "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                            },


                            {
                                "nombre": "BARRENADOR",
                                "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                            },
                            {
                                "nombre": "MARCENO",
                                "valor": { "$ifNull": ["$Marceño", 0] }
                            },
                            {
                                "nombre": "TRIPS",
                                 "valor": { "$ifNull": ["$Trips", { "$ifNull": ["$Numero de Trips por panicula", 0] }] }
                            },


                            {
                                "nombre": "MOSCAO",
                                "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", { "$ifNull": ["$Paniculas afectadas por Mosca del Ovario", 0] }] }
                            },


                            {
                                "nombre": "XILEBORUS",
                                "valor": { "$ifNull": ["$Xyleborus", "No"] }
                            },


                            {
                                "nombre": "STENOMA",
                                "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                            },
                            {
                                "nombre": "HEILIPUS",
                                "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                            },
                            {
                                "nombre": "COMPSUS",
                                "valor": { "$ifNull": ["$Compsus", 0] }
                            },


                            {
                                "nombre": "ACARTONAMIENTO",
                                "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                            },



                            {
                                "nombre": "CLOROSIS",
                                "valor": { "$ifNull": ["$Clorosis", "No"] }
                            },



                            {
                                "nombre": "CHANCRO",
                                "valor": { "$ifNull": ["$Chancro", "No"] }
                            }



                            , {
                                "nombre": "ONCIDERES",
                                "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                            }

                            , {
                                "nombre": "VERTICILIUM",
                                "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                            }
                            , {
                                "nombre": "FUMAGINA",
                                "valor": { "$ifNull": ["$Fumagina", "No"] }
                            }
                            , {
                                "nombre": "ANTRACNOSIS",
                                "valor": { "$ifNull": ["$Antracnosis", "No"] }
                            }
                            , {
                                "nombre": "LASIODIPLODIA",
                                "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                            }


                            , {
                                "nombre": "MIRIDAE",
                                "valor": { "$ifNull": ["$Individuos de Chinche Miridae", 0] }
                            }
                            , {
                                "nombre": "PEGA PEGA",
                                "valor": { "$ifNull": ["$Pega pega", "No"] }
                            }
                            , {
                                "nombre": "COPTURUMIMUS",
                                "valor": { "$ifNull": ["$Copturumimus", "NO"] }
                            }
                            , {
                                "nombre": "ABEJAS",
                                "valor": { "$ifNull": ["$Presencia de Abejas", "No"] }
                            }





                        ]

                        , "array_campos_DEFOLIADORES": [

                            {
                                "nombre": "Marceño",
                                "valor": { "$ifNull": ["$Marceño", 0] }
                            },
                            {
                                "nombre": "Compsus",
                                "valor": { "$ifNull": ["$Compsus", 0] }
                            },
                            {
                                "nombre": "Pandeleteius",
                                "valor": { "$ifNull": ["$Pandeleteius", 0] }
                            },
                            {
                                "nombre": "Mimografus",
                                "valor": { "$ifNull": ["$Mimografus", 0] }
                            },
                            {
                                "nombre": "Epitrix",
                                "valor": { "$ifNull": ["$Epitrix", 0] }
                            },
                            {
                                "nombre": "Daños",
                                "valor": { "$ifNull": ["$Daños", 0] }
                            }

                            ,
                            {
                                "nombre": "Larvas",
                                "valor": { "$ifNull": ["$Larvas", 0] }
                            }

                        ]
                    }
                }


                , {
                    "$addFields": {
                        "array_plagas_enfermedades": {
                            "$map": {
                                "input": "$array_plagas_enfermedades",
                                "as": "item",
                                "in": {
                                    "nombre": "$$item.nombre",
                                    "valor": {

                                        "$cond": {
                                            "if": {
                                                "$or": [
                                                    { "$eq": ["$$item.valor", ""] }
                                                    , { "$eq": ["$$item.valor", "no"] }
                                                    , { "$eq": ["$$item.valor", "No"] }
                                                    , { "$eq": ["$$item.valor", "NO"] }


                                                ]
                                            },
                                            "then": 0,
                                            "else": "$$item.valor"
                                        }


                                    }

                                }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "array_plagas_enfermedades": {
                            "$map": {
                                "input": "$array_plagas_enfermedades",
                                "as": "item",
                                "in": {
                                    "nombre": "$$item.nombre",
                                    "valor": {

                                        "$cond": {
                                            "if": {
                                                "$or": [
                                                    { "$eq": ["$$item.valor", "si"] }
                                                    , { "$eq": ["$$item.valor", "Si"] }
                                                    , { "$eq": ["$$item.valor", "SI"] }
                                                ]
                                            },
                                            "then": 1,
                                            "else": "$$item.valor"
                                        }
                                    }

                                }
                            }
                        }
                    }
                }




                , {
                    "$addFields": {
                        "array_plagas_enfermedades": {
                            "$map": {
                                "input": "$array_plagas_enfermedades",
                                "as": "item",
                                "in": {
                                    "nombre": "$$item.nombre",
                                    "valor": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                            "then": "$$item.valor",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [
                                                            { "$eq": [{ "$type": "$$item.valor" }, "string"] },

                                                            {
                                                                "$or": [
                                                                    { "$eq": ["$$item.valor", ""] }
                                                                    , { "$eq": ["$$item.valor", " "] }
                                                                    , { "$eq": ["$$item.valor", "  "] }

                                                                    , { "$eq": ["$$item.valor", "."] }
                                                                    , { "$eq": ["$$item.valor", ","] }
                                                                    , { "$eq": ["$$item.valor", ";"] }

                                                                    , { "$eq": ["$$item.valor", "-"] }
                                                                    , { "$eq": ["$$item.valor", "+"] }
                                                                    , { "$eq": ["$$item.valor", "/"] }
                                                                    , { "$eq": ["$$item.valor", "*"] }
                                                                ]
                                                            }
                                                        ]
                                                    },
                                                    "then": 0,
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                            "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                                            "else": { "$toDouble": "$$item.valor" }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "array_campos_DEFOLIADORES": {
                            "$map": {
                                "input": "$array_campos_DEFOLIADORES",
                                "as": "item",
                                "in": {
                                    "nombre": "$$item.nombre",
                                    "valor": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                            "then": "$$item.valor",
                                            "else": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [
                                                            { "$eq": [{ "$type": "$$item.valor" }, "string"] },

                                                            {
                                                                "$or": [
                                                                    { "$eq": ["$$item.valor", ""] }
                                                                    , { "$eq": ["$$item.valor", " "] }
                                                                    , { "$eq": ["$$item.valor", "  "] }

                                                                    , { "$eq": ["$$item.valor", "."] }
                                                                    , { "$eq": ["$$item.valor", ","] }
                                                                    , { "$eq": ["$$item.valor", ";"] }

                                                                    , { "$eq": ["$$item.valor", "-"] }
                                                                    , { "$eq": ["$$item.valor", "+"] }
                                                                    , { "$eq": ["$$item.valor", "/"] }
                                                                    , { "$eq": ["$$item.valor", "*"] }
                                                                ]
                                                            }
                                                        ]
                                                    },
                                                    "then": 0,
                                                    "else": {
                                                        "$cond": {
                                                            "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                            "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                                            "else": { "$toDouble": "$$item.valor" }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "DEFOLIADORES_variable_agrupada":
                        {
                            "nombre": "DEFOLIADORES",
                            "valor": {
                                "$reduce": {
                                    "input": "$array_campos_DEFOLIADORES.valor",
                                    "initialValue": 0,
                                    "in": { "$add": ["$$value", "$$this"] }
                                }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "array_plagas_enfermedades": {
                            "$concatArrays": [
                                "$array_plagas_enfermedades",
                                ["$DEFOLIADORES_variable_agrupada"]
                            ]
                        }
                    }
                }


                , {
                    "$project": {
                        "Point": 0
                        , "DEFOLIADORES_variable_agrupada": 0
                        , "array_campos_DEFOLIADORES": 0

                        , "Formula": 0
                        , "uid": 0
                        , "uDate": 0
                        , "capture": 0
                        , "split_lote": 0
                        , "Sampling": 0
                        , "supervisor": 0


                        , "size_split_lote": 0
                        , "aux_size_split_lote": 0
                        , "split_lote2": 0




                        , "Acaro Huevo": 0
                        , "Acaro Ninfa": 0
                        , "Acaro Adulto": 0
                        , "Escama": 0
                        , "Tipos de Escamas": 0
                        , "Marceño": 0
                        , "Compsus": 0
                        , "Pandeleteius": 0
                        , "Mimografus": 0
                        , "Epitrix": 0
                        , "Larvas": 0
                        , "Daños": 0
                        , "Mosca Blanca": 0
                        , "Trips": 0
                        , "Frutos afectados por Mosca del Ovario": 0
                        , "Insectos Beneficos": 0
                        , "Ninfa": 0
                        , "Adulto": 0
                        , "Cantidad de Larvas o daños de Stenoma": 0
                        , "cantidad de individuos o daños de Heilipus": 0
                        , "Fumagina": 0
                        , "Muerte descendente  Posible verticilium": 0
                        , "Clorosis": 0
                        , "Cantidad de ramas afectadas por barrenador": 0
                        , "Cantidad de ramas afectadas por Oncideres": 0
                        , "Xyleborus": 0
                        , "Chancro": 0
                        , "Antracnosis": 0
                        , "Lasiodiplodia": 0
                        , "Acaro": 0
                        , "Defoliadores": 0
                        , "Monalonion": 0
                        , "Observaciones": 0
                        , "Arboles afectados por Monalonion": 0
                        , "Huevo": 0
                        , "Acartonamiento": 0

                        , "rgDate": 0


                        , "Busqueda inicio": 0
                        , "Busqueda fin": 0
                        , "today": 0
                        , "idform": 0

                        , "Keys": 0
                        , "user": 0
                        , "FincaID": 0

                        , "uDate día": 0
                        , "uDate mes": 0
                        , "uDate año": 0
                        , "uDate hora": 0




                        , "Numero de hojas afectadas por escama": 0
                        , "Numero de hojas afectadas por Mosca Blanca": 0
                        , "Paniculas afectadas por Mosca del Ovario": 0
                        , "Individuos de Chinche Miridae": 0
                        , "Pega pega": 0
                        , "Numero de Trips por panicula": 0
                        , "Presencia de Abejas": 0
                        , "Copturumimus": 0



                    }
                }





                , {
                    "$addFields": {
                        "ciclo_año": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                            }
                        }
                        , "ciclo_semana": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
                        , "ciclo_fecha_fin": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                            }
                        }
                    }
                }



                , {
                    "$group": {
                        "_id": {


                            "up": "$UP",
                            "lote": "$Lote",
                            "nombre": "$Nombre"

                            , "plagas": "$array_plagas_enfermedades.nombre"

                            , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                            , "ciclo_estado": "$ciclo_estado"
                            , "ciclo_año": "$ciclo_año"
                            , "ciclo_semana": "$ciclo_semana"



                        }
                        , "plantas_dif_censadas_x_lote": { "$max": "$plantas_dif_censadas_x_lote" }
                        , "monitor": { "$max": "$Monitor" }
                        , "data": { "$push": "$$ROOT" }
                    }
                }



                , {
                    "$addFields": {
                        "data_plagas": {
                            "$map": {
                                "input": "$_id.plagas",
                                "as": "item_plagas",
                                "in": {
                                    "nombre": "$$item_plagas"

                                    , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                                        "$map": {
                                            "input": "$data",
                                            "as": "item_data",
                                            "in": {
                                                "$reduce": {
                                                    "input": "$$item_data.array_plagas_enfermedades",
                                                    "initialValue": 0,
                                                    "in": {
                                                        "$cond": {
                                                            "if": { "$eq": ["$$this.nombre", "$$item_plagas"] },
                                                            "then": {
                                                                "$cond": {
                                                                    "if": { "$eq": ["$$this.valor", 0] },
                                                                    "then": { "$add": ["$$value", 0] },
                                                                    "else": { "$add": ["$$value", 1] }
                                                                }
                                                            },
                                                            "else": { "$add": ["$$value", 0] }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }



                , {
                    "$addFields": {
                        "data_plagas2": {
                            "$map": {
                                "input": "$data_plagas",
                                "as": "item_data_plagas",
                                "in": {
                                    "nombre": "$$item_data_plagas.nombre"

                                    , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                                        "$reduce": {
                                            "input": "$$item_data_plagas.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                                            "initialValue": 0,
                                            "in": { "$add": ["$$value", "$$this"] }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                , {
                    "$project": {
                        "data": 0,
                        "data_plagas": 0

                        , "_id.plagas": 0
                    }
                }


                , {
                    "$unwind": {
                        "path": "$data_plagas2",
                        "preserveNullAndEmptyArrays": true
                    }
                }


                , {
                    "$addFields": {
                        "PORCENTAJE_INIDENCIA": {
                            "$cond": {
                                "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                                "then": 0,
                                "else": {
                                    "$divide": ["$data_plagas2.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                                        "$plantas_dif_censadas_x_lote"]
                                }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "PORCENTAJE_INIDENCIA": {
                            "$multiply": ["$PORCENTAJE_INIDENCIA", 100]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "PORCENTAJE_INIDENCIA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, 1] }] }, 100] }
                    }
                }




                , {
                    "$addFields": {
                        "Nivel_INCIDENCIA": {
                            "$switch": {
                                "branches": [


                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ACAROA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ACAROH"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ACARON"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ACARTONAMIENTO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "COMPSUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "DEFOLIADORES"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ESCAMA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "MARCENO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "MOSCAB"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "MOSCAO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "TRIPS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },



                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "BARRENADOR"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "CHANCRO"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "CLOROSIS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "HEILIPUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "MONALONION"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "STENOMA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$data_plagas2.nombre", "XILEBORUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }


                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ONCIDERES"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "VERTICILIUM"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "FUMAGINA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ANTRACNOSIS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }
                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "LASIODIPLODIA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }


                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "MIRIDAE"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }

                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "ABEJAS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }

                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "COPTURUMIMUS"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }

                                    , {
                                        "case": { "$eq": ["$data_plagas2.nombre", "PEGA PEGA"] },
                                        "then": {
                                            "$switch": {
                                                "branches": [
                                                    {
                                                        "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                            ]
                                                        },
                                                        "then": 1
                                                    },
                                                    {
                                                        "case": {
                                                            "$and": [
                                                                { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                                , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                            ]
                                                        },
                                                        "then": 2
                                                    },
                                                    {
                                                        "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                                        "then": 3
                                                    }

                                                ], "default": null
                                            }

                                        }
                                    }





                                ], "default": null
                            }
                        }
                    }
                }



                , {
                    "$group": {
                        "_id": {

                            "up": "$_id.up",
                            "lote": "$_id.lote",
                            "nombre": "$_id.nombre"


                            , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                            , "ciclo_estado": "$_id.ciclo_estado"
                            , "ciclo_año": "$_id.ciclo_año"
                            , "ciclo_semana": "$_id.ciclo_semana"



                        }
                        , "data": { "$push": "$$ROOT" }
                        , "plantas_dif_censadas_x_lote": { "$max": "$plantas_dif_censadas_x_lote" }
                        , "monitor": { "$max": "$monitor" }
                    }
                }


                , {
                    "$addFields": {
                        "dias_ciclo": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        { "$dateFromString": { "dateString": "$_id.ciclo_fecha_fin", "format": "%d/%m/%Y" } },
                                        { "$dateFromString": { "dateString": "$_id.ciclo_fecha_inicio", "format": "%d/%m/%Y" } }
                                    ]
                                },
                                86400000
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "dias_ciclo": { "$floor": "$dias_ciclo" }
                    }
                }




                , { "$addFields": { "INC_ACAROA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACAROA"] } } } } }, { "$unwind": "$INC_ACAROA" }, { "$addFields": { "INC_ACAROA": "$INC_ACAROA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ACAROA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACAROA"] } } } } }, { "$unwind": "$I_ACAROA" }, { "$addFields": { "I_ACAROA": "$I_ACAROA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ACAROA"] } } } } }

                , { "$addFields": { "INC_ACAROH": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACAROH"] } } } } }, { "$unwind": "$INC_ACAROH" }, { "$addFields": { "INC_ACAROH": "$INC_ACAROH.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ACAROH": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACAROH"] } } } } }, { "$unwind": "$I_ACAROH" }, { "$addFields": { "I_ACAROH": "$I_ACAROH.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ACAROH"] } } } } }

                , { "$addFields": { "INC_ACARON": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACARON"] } } } } }, { "$unwind": "$INC_ACARON" }, { "$addFields": { "INC_ACARON": "$INC_ACARON.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ACARON": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACARON"] } } } } }, { "$unwind": "$I_ACARON" }, { "$addFields": { "I_ACARON": "$I_ACARON.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ACARON"] } } } } }


                , { "$addFields": { "INC_ACARTONAMIENTO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$INC_ACARTONAMIENTO" }, { "$addFields": { "INC_ACARTONAMIENTO": "$INC_ACARTONAMIENTO.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ACARTONAMIENTO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ACARTONAMIENTO"] } } } } }, { "$unwind": "$I_ACARTONAMIENTO" }, { "$addFields": { "I_ACARTONAMIENTO": "$I_ACARTONAMIENTO.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ACARTONAMIENTO"] } } } } }


                , { "$addFields": { "INC_BARRENADOR": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "BARRENADOR"] } } } } }, { "$unwind": "$INC_BARRENADOR" }, { "$addFields": { "INC_BARRENADOR": "$INC_BARRENADOR.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_BARRENADOR": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "BARRENADOR"] } } } } }, { "$unwind": "$I_BARRENADOR" }, { "$addFields": { "I_BARRENADOR": "$I_BARRENADOR.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "BARRENADOR"] } } } } }


                , { "$addFields": { "INC_CHANCRO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "CHANCRO"] } } } } }, { "$unwind": "$INC_CHANCRO" }, { "$addFields": { "INC_CHANCRO": "$INC_CHANCRO.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_CHANCRO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "CHANCRO"] } } } } }, { "$unwind": "$I_CHANCRO" }, { "$addFields": { "I_CHANCRO": "$I_CHANCRO.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "CHANCRO"] } } } } }


                , { "$addFields": { "INC_CLOROSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "CLOROSIS"] } } } } }, { "$unwind": "$INC_CLOROSIS" }, { "$addFields": { "INC_CLOROSIS": "$INC_CLOROSIS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_CLOROSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "CLOROSIS"] } } } } }, { "$unwind": "$I_CLOROSIS" }, { "$addFields": { "I_CLOROSIS": "$I_CLOROSIS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "CLOROSIS"] } } } } }



                , { "$addFields": { "INC_COMPSUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "COMPSUS"] } } } } }, { "$unwind": "$INC_COMPSUS" }, { "$addFields": { "INC_COMPSUS": "$INC_COMPSUS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_COMPSUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "COMPSUS"] } } } } }, { "$unwind": "$I_COMPSUS" }, { "$addFields": { "I_COMPSUS": "$I_COMPSUS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "COMPSUS"] } } } } }


                , { "$addFields": { "INC_DEFOLIADORES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "DEFOLIADORES"] } } } } }, { "$unwind": "$INC_DEFOLIADORES" }, { "$addFields": { "INC_DEFOLIADORES": "$INC_DEFOLIADORES.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_DEFOLIADORES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "DEFOLIADORES"] } } } } }, { "$unwind": "$I_DEFOLIADORES" }, { "$addFields": { "I_DEFOLIADORES": "$I_DEFOLIADORES.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "DEFOLIADORES"] } } } } }


                , { "$addFields": { "INC_ESCAMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ESCAMA"] } } } } }, { "$unwind": "$INC_ESCAMA" }, { "$addFields": { "INC_ESCAMA": "$INC_ESCAMA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ESCAMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ESCAMA"] } } } } }, { "$unwind": "$I_ESCAMA" }, { "$addFields": { "I_ESCAMA": "$I_ESCAMA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ESCAMA"] } } } } }


                , { "$addFields": { "INC_HEILIPUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "HEILIPUS"] } } } } }, { "$unwind": "$INC_HEILIPUS" }, { "$addFields": { "INC_HEILIPUS": "$INC_HEILIPUS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_HEILIPUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "HEILIPUS"] } } } } }, { "$unwind": "$I_HEILIPUS" }, { "$addFields": { "I_HEILIPUS": "$I_HEILIPUS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "HEILIPUS"] } } } } }


                , { "$addFields": { "INC_MARCENO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MARCENO"] } } } } }, { "$unwind": "$INC_MARCENO" }, { "$addFields": { "INC_MARCENO": "$INC_MARCENO.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_MARCENO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MARCENO"] } } } } }, { "$unwind": "$I_MARCENO" }, { "$addFields": { "I_MARCENO": "$I_MARCENO.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "MARCENO"] } } } } }


                , { "$addFields": { "INC_MONALONION": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MONALONION"] } } } } }, { "$unwind": "$INC_MONALONION" }, { "$addFields": { "INC_MONALONION": "$INC_MONALONION.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_MONALONION": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MONALONION"] } } } } }, { "$unwind": "$I_MONALONION" }, { "$addFields": { "I_MONALONION": "$I_MONALONION.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "MONALONION"] } } } } }


                , { "$addFields": { "INC_MOSCAB": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MOSCAB"] } } } } }, { "$unwind": "$INC_MOSCAB" }, { "$addFields": { "INC_MOSCAB": "$INC_MOSCAB.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_MOSCAB": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MOSCAB"] } } } } }, { "$unwind": "$I_MOSCAB" }, { "$addFields": { "I_MOSCAB": "$I_MOSCAB.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "MOSCAB"] } } } } }


                , { "$addFields": { "INC_MOSCAO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MOSCAO"] } } } } }, { "$unwind": "$INC_MOSCAO" }, { "$addFields": { "INC_MOSCAO": "$INC_MOSCAO.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_MOSCAO": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MOSCAO"] } } } } }, { "$unwind": "$I_MOSCAO" }, { "$addFields": { "I_MOSCAO": "$I_MOSCAO.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "MOSCAO"] } } } } }


                , { "$addFields": { "INC_STENOMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "STENOMA"] } } } } }, { "$unwind": "$INC_STENOMA" }, { "$addFields": { "INC_STENOMA": "$INC_STENOMA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_STENOMA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "STENOMA"] } } } } }, { "$unwind": "$I_STENOMA" }, { "$addFields": { "I_STENOMA": "$I_STENOMA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "STENOMA"] } } } } }


                , { "$addFields": { "INC_TRIPS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "TRIPS"] } } } } }, { "$unwind": "$INC_TRIPS" }, { "$addFields": { "INC_TRIPS": "$INC_TRIPS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_TRIPS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "TRIPS"] } } } } }, { "$unwind": "$I_TRIPS" }, { "$addFields": { "I_TRIPS": "$I_TRIPS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "TRIPS"] } } } } }


                , { "$addFields": { "INC_XILEBORUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "XILEBORUS"] } } } } }, { "$unwind": "$INC_XILEBORUS" }, { "$addFields": { "INC_XILEBORUS": "$INC_XILEBORUS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_XILEBORUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "XILEBORUS"] } } } } }, { "$unwind": "$I_XILEBORUS" }, { "$addFields": { "I_XILEBORUS": "$I_XILEBORUS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "XILEBORUS"] } } } } }



                , { "$addFields": { "INC_ONCIDERES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ONCIDERES"] } } } } }, { "$unwind": "$INC_ONCIDERES" }, { "$addFields": { "INC_ONCIDERES": "$INC_ONCIDERES.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ONCIDERES": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ONCIDERES"] } } } } }, { "$unwind": "$I_ONCIDERES" }, { "$addFields": { "I_ONCIDERES": "$I_ONCIDERES.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ONCIDERES"] } } } } }


                , { "$addFields": { "INC_VERTICILIUM": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "VERTICILIUM"] } } } } }, { "$unwind": "$INC_VERTICILIUM" }, { "$addFields": { "INC_VERTICILIUM": "$INC_VERTICILIUM.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_VERTICILIUM": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "VERTICILIUM"] } } } } }, { "$unwind": "$I_VERTICILIUM" }, { "$addFields": { "I_VERTICILIUM": "$I_VERTICILIUM.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "VERTICILIUM"] } } } } }


                , { "$addFields": { "INC_FUMAGINA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "FUMAGINA"] } } } } }, { "$unwind": "$INC_FUMAGINA" }, { "$addFields": { "INC_FUMAGINA": "$INC_FUMAGINA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_FUMAGINA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "FUMAGINA"] } } } } }, { "$unwind": "$I_FUMAGINA" }, { "$addFields": { "I_FUMAGINA": "$I_FUMAGINA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "FUMAGINA"] } } } } }


                , { "$addFields": { "INC_ANTRACNOSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ANTRACNOSIS"] } } } } }, { "$unwind": "$INC_ANTRACNOSIS" }, { "$addFields": { "INC_ANTRACNOSIS": "$INC_ANTRACNOSIS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ANTRACNOSIS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ANTRACNOSIS"] } } } } }, { "$unwind": "$I_ANTRACNOSIS" }, { "$addFields": { "I_ANTRACNOSIS": "$I_ANTRACNOSIS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ANTRACNOSIS"] } } } } }


                , { "$addFields": { "INC_LASIODIPLODIA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "LASIODIPLODIA"] } } } } }, { "$unwind": "$INC_LASIODIPLODIA" }, { "$addFields": { "INC_LASIODIPLODIA": "$INC_LASIODIPLODIA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_LASIODIPLODIA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "LASIODIPLODIA"] } } } } }, { "$unwind": "$I_LASIODIPLODIA" }, { "$addFields": { "I_LASIODIPLODIA": "$I_LASIODIPLODIA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "LASIODIPLODIA"] } } } } }

                , { "$addFields": { "INC_ABEJAS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ABEJAS"] } } } } }, { "$unwind": "$INC_ABEJAS" }, { "$addFields": { "INC_ABEJAS": "$INC_ABEJAS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_ABEJAS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "ABEJAS"] } } } } }, { "$unwind": "$I_ABEJAS" }, { "$addFields": { "I_ABEJAS": "$I_ABEJAS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "ABEJAS"] } } } } }

                , { "$addFields": { "INC_COPTURUMIMUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "COPTURUMIMUS"] } } } } }, { "$unwind": "$INC_COPTURUMIMUS" }, { "$addFields": { "INC_COPTURUMIMUS": "$INC_COPTURUMIMUS.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_COPTURUMIMUS": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "COPTURUMIMUS"] } } } } }, { "$unwind": "$I_COPTURUMIMUS" }, { "$addFields": { "I_COPTURUMIMUS": "$I_COPTURUMIMUS.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "COPTURUMIMUS"] } } } } }

                , { "$addFields": { "INC_MIRIDAE": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MIRIDAE"] } } } } }, { "$unwind": "$INC_MIRIDAE" }, { "$addFields": { "INC_MIRIDAE": "$INC_MIRIDAE.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_MIRIDAE": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "MIRIDAE"] } } } } }, { "$unwind": "$I_MIRIDAE" }, { "$addFields": { "I_MIRIDAE": "$I_MIRIDAE.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "MIRIDAE"] } } } } }

                , { "$addFields": { "INC_PEGA PEGA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "PEGA PEGA"] } } } } }, { "$unwind": "$INC_PEGA PEGA" }, { "$addFields": { "INC_PEGA PEGA": "$INC_PEGA PEGA.PORCENTAJE_INIDENCIA" } }
                , { "$addFields": { "I_PEGA PEGA": { "$filter": { "input": "$data", "as": "item", "cond": { "$eq": ["$$item.data_plagas2.nombre", "PEGA PEGA"] } } } } }, { "$unwind": "$I_PEGA PEGA" }, { "$addFields": { "I_PEGA PEGA": "$I_PEGA PEGA.Nivel_INCIDENCIA" } }
                , { "$addFields": { "data": { "$filter": { "input": "$data", "as": "item", "cond": { "$ne": ["$$item.data_plagas2.nombre", "PEGA PEGA"] } } } } }





                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$_id",
                                {
                                    "dias_ciclo": "$dias_ciclo",
                                    "Arboles Monitoreados": "$plantas_dif_censadas_x_lote",
                                    "Monitor": "$monitor",


                                    "INC_ACAROA": "$INC_ACAROA",
                                    "INC_ACAROH": "$INC_ACAROH",
                                    "INC_ACARON": "$INC_ACARON",
                                    "INC_ACARTONAMIENTO": "$INC_ACARTONAMIENTO",
                                    "INC_BARRENADOR": "$INC_BARRENADOR",
                                    "INC_CHANCRO": "$INC_CHANCRO",
                                    "INC_CLOROSIS": "$INC_CLOROSIS",
                                    "INC_COMPSUS": "$INC_COMPSUS",
                                    "INC_DEFOLIADORES": "$INC_DEFOLIADORES",
                                    "INC_ESCAMA": "$INC_ESCAMA",
                                    "INC_HEILIPUS": "$INC_HEILIPUS",
                                    "INC_MARCENO": "$INC_MARCENO",
                                    "INC_MONALONION": "$INC_MONALONION",
                                    "INC_MOSCAB": "$INC_MOSCAB",
                                    "INC_MOSCAO": "$INC_MOSCAO",
                                    "INC_STENOMA": "$INC_STENOMA",
                                    "INC_TRIPS": "$INC_TRIPS",
                                    "INC_XILEBORUS": "$INC_XILEBORUS",

                                    "INC_ONCIDERES": "$INC_ONCIDERES",
                                    "INC_VERTICILIUM": "$INC_VERTICILIUM",
                                    "INC_FUMAGINA": "$INC_FUMAGINA",
                                    "INC_ANTRACNOSIS": "$INC_ANTRACNOSIS",
                                    "INC_LASIODIPLODIA": "$INC_LASIODIPLODIA",

                                    "INC_ABEJAS": "$INC_ABEJAS",
                                    "INC_COPTURUMIMUS": "$INC_COPTURUMIMUS",
                                    "INC_MIRIDAE": "$INC_MIRIDAE",
                                    "INC_PEGA PEGA": "$INC_PEGA PEGA",





                                    "I_ACAROA": "$I_ACAROA",
                                    "I_ACAROH": "$I_ACAROH",
                                    "I_ACARON": "$I_ACARON",
                                    "I_ACARTONAMIENTO": "$I_ACARTONAMIENTO",
                                    "I_BARRENADOR": "$I_BARRENADOR",
                                    "I_CHANCRO": "$I_CHANCRO",
                                    "I_CLOROSIS": "$I_CLOROSIS",
                                    "I_COMPSUS": "$I_COMPSUS",
                                    "I_DEFOLIADORES": "$I_DEFOLIADORES",
                                    "I_ESCAMA": "$I_ESCAMA",
                                    "I_HEILIPUS": "$I_HEILIPUS",
                                    "I_MARCENO": "$I_MARCENO",
                                    "I_MONALONION": "$I_MONALONION",
                                    "I_MOSCAB": "$I_MOSCAB",
                                    "I_MOSCAO": "$I_MOSCAO",
                                    "I_STENOMA": "$I_STENOMA",
                                    "I_TRIPS": "$I_TRIPS",
                                    "I_XILEBORUS": "$I_XILEBORUS",

                                    "I_ONCIDERES": "$I_ONCIDERES",
                                    "I_VERTICILIUM": "$I_VERTICILIUM",
                                    "I_FUMAGINA": "$I_FUMAGINA",
                                    "I_ANTRACNOSIS": "$I_ANTRACNOSIS",
                                    "I_LASIODIPLODIA": "$I_LASIODIPLODIA"

                                    , "I_ABEJAS": "$I_ABEJAS"
                                    , "I_COPTURUMIMUS": "$I_COPTURUMIMUS"
                                    , "I_MIRIDAE": "$I_MIRIDAE"
                                    , "I_PEGA PEGA": "$I_PEGA PEGA"


                                    , "rgDate": "$$filtro_fecha_inicio"



                                }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "up": { "$toUpper": { "$trim": { "input": "$up" } } }
                        , "nombre": { "$toUpper": { "$trim": { "input": "$nombre" } } }
                    }
                }




            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }






]
