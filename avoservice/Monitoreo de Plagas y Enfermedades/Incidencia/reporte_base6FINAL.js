db.form_monitoreodeplagasyenfermedades.aggregate(

    [

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                // , "Point": 0
                , "Arbol": 0
            }
        }


        //----variables reporte

        , {
            "$addFields": {
                "split_lote": { "$split": ["$lote", " "] }
            }
        }

        , {
            "$addFields": {
                "UP": { "$arrayElemAt": ["$split_lote", 1] },
                "Lote": { "$arrayElemAt": ["$split_lote", 2] }
            }
        }
        , {
            "$addFields": {
                "Nombre": { "$concat": ["$UP", " ", "$Lote"] }

            }
        }


        //---info fechas
        , {
            "$addFields": {
                "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
                "Año": { "$year": { "date": "$rgDate" } },
                "Semana": { "$week": { "date": "$rgDate" } }
                , "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
            }
        }
        , {
            "$addFields": {
                "Monitor": "$supervisor"
            }
        }

        , {
            "$addFields": {
                // lon: { $arrayElemAt: ["$geometry.coordinates", 0] },
                // lat: { $arrayElemAt: ["$geometry.coordinates", 1] },

                "X": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
                "Y": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
            }
        }


        //---plantas dif censadas
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }


        //------plagas y enfermedades
        , {
            "$addFields": {
                "array_plagas_enfermedades": [

                    // {
                    //     "nombre": "xxxxx",
                    //     "tipo": "otro",
                    //     "variable": "xxxxx",
                    //     "valor": { "$ifNull": [ "$xxxxx" , 0] }
                    // },

                    {
                        "nombre": "ACAROH",
                        "tipo": "Acaro",
                        "variable": "Acaro Huevo",
                        "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                    },
                    {
                        "nombre": "ACARON",
                        "tipo": "Acaro",
                        "variable": "Acaro Ninfa",
                        "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                    },
                    {
                        "nombre": "ACAROA",
                        "tipo": "Acaro",
                        "variable": "Acaro Adulto",
                        "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                    },

                    {
                        "nombre": "MOSCAB",
                        "tipo": "Mosca",
                        "variable": "Mosca Blanca",
                        "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                    },

                    {
                        "nombre": "ESCAMA",
                        "tipo": "Escama",
                        "variable": "Escama",
                        "valor": { "$ifNull": ["$Escama", 0] }
                    },


                    {
                        "nombre": "MONALONION",
                        "tipo": "otro",
                        "variable": "Arboles afectados por Monalonion",
                        "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                    },


                    {
                        "nombre": "BARRENADOR",
                        "tipo": "otro",
                        "variable": "Cantidad de ramas afectadas por barrenador",
                        "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                    },
                    {
                        "nombre": "MARCENO",
                        "tipo": "otro",
                        "variable": "Marceño",
                        "valor": { "$ifNull": ["$Marceño", 0] }
                    },
                    {
                        "nombre": "TRIPS",
                        "tipo": "otro",
                        "variable": "Trips",
                        "valor": { "$ifNull": ["$Trips", 0] }
                    },

                    //!!!falta DEFOLIADORES
                    //......(sumatoria)
                    // Marceño
                    // Compsus
                    // Pandeleteius
                    // Mimografus
                    // Epitrix
                    // Daños

                    {
                        "nombre": "MOSCAO",
                        "tipo": "Mosca",
                        "variable": "Frutos afectados por Mosca del Ovario",
                        "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                    },

                    //!!!XILEBORUS (si-no)
                    {
                        "nombre": "XILEBORUS",
                        "tipo": "otro",
                        "variable": "Xyleborus",
                        "valor": { "$ifNull": ["$Xyleborus", "No"] }
                    },


                    {
                        "nombre": "STENOMA",
                        "tipo": "otro",
                        "variable": "Cantidad de Larvas o daños de Stenoma",
                        "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                    },
                    {
                        "nombre": "HEILIPUS",
                        "tipo": "otro",
                        "variable": "cantidad de individuos o daños de Heilipus",
                        "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                    },
                    {
                        "nombre": "COMPSUS",
                        "tipo": "otro",
                        "variable": "Compsus",
                        "valor": { "$ifNull": ["$Compsus", 0] }
                    },

                    //!!! ACARTONAMIENTO (si-no)
                    {
                        "nombre": "ACARTONAMIENTO",
                        "tipo": "otro",
                        "variable": "Acartonamiento",
                        "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                    },


                    //!!! CLOROSIS (si-no)
                    {
                        "nombre": "CLOROSIS",
                        "tipo": "otro",
                        "variable": "Clorosis",
                        "valor": { "$ifNull": ["$Clorosis", "No"] }
                    },


                    //!!! CHANCRO (si-no)
                    {
                        "nombre": "CHANCRO",
                        "tipo": "otro",
                        "variable": "Chancro",
                        "valor": { "$ifNull": ["$Chancro", "No"] }
                    }




                ]

                , "array_campos_DEFOLIADORES": [

                    //!!!falta DEFOLIADORES
                    //......(sumatoria)
                    // Marceño
                    // Compsus
                    // Pandeleteius
                    // Mimografus
                    // Epitrix
                    // Daños

                    {
                        "nombre": "Marceño",
                        "tipo": "DEFOLIADORES",
                        "variable": "Marceño",
                        "valor": { "$ifNull": ["$Marceño", 0] }
                    },
                    {
                        "nombre": "Compsus",
                        "tipo": "DEFOLIADORES",
                        "variable": "Compsus",
                        "valor": { "$ifNull": ["$Compsus", 0] }
                    },
                    {
                        "nombre": "Pandeleteius",
                        "tipo": "DEFOLIADORES",
                        "variable": "Pandeleteius",
                        "valor": { "$ifNull": ["$Pandeleteius", 0] }
                    },
                    {
                        "nombre": "Mimografus",
                        "tipo": "DEFOLIADORES",
                        "variable": "Mimografus",
                        "valor": { "$ifNull": ["$Mimografus", 0] }
                    },
                    {
                        "nombre": "Epitrix",
                        "tipo": "DEFOLIADORES",
                        "variable": "Epitrix",
                        "valor": { "$ifNull": ["$Epitrix", 0] }
                    },
                    {
                        "nombre": "Daños",
                        "tipo": "DEFOLIADORES",
                        "variable": "Daños",
                        "valor": { "$ifNull": ["$Daños", 0] }
                    },

                ]
            }
        }


        // //-------------
        // //---TEST
        // ,{$unwind: "$array_plagas_enfermedades"}

        // ,{
        //     $group:{
        //         _id:"$array_plagas_enfermedades.valor"
        //         ,cant:{$sum:1}
        //     }
        // }

        // //---------------



        //----arreglo de valores

        //arreglo1- valores "","no","No","NO"
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "tipo": "$$item.tipo",
                            "variable": "$$item.variable",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", ""] }
                                            , { "$eq": ["$$item.valor", "no"] }
                                            , { "$eq": ["$$item.valor", "No"] }
                                            , { "$eq": ["$$item.valor", "NO"] }


                                        ]
                                    },
                                    "then": 0,
                                    "else": "$$item.valor"
                                }


                            }

                        }
                    }
                }
            }
        }

        //arreglo2- valores "si","Si","SI"
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "tipo": "$$item.tipo",
                            "variable": "$$item.variable",
                            "valor": {

                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", "si"] }
                                            , { "$eq": ["$$item.valor", "Si"] }
                                            , { "$eq": ["$$item.valor", "SI"] }
                                        ]
                                    },
                                    "then": 1,
                                    "else": "$$item.valor"
                                }
                            }

                        }
                    }
                }
            }
        }

        //arreglo3- valores convertir en NUMEROS
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$map": {
                        "input": "$array_plagas_enfermedades",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "tipo": "$$item.tipo",
                            "variable": "$$item.variable",
                            "valor": { "$toDouble": "$$item.valor" }
                        }
                    }
                }
            }
        }


        // //-------------
        // //---TEST
        // ,{$unwind: "$array_plagas_enfermedades"}

        // ,{
        //     $group:{
        //         _id:"$array_plagas_enfermedades.valor"
        //         ,cant:{$sum:1}
        //     }
        // }

        // //---------------


        //arreglo4- DEFOLIADORES valores "" y convertir en NUMERO
        , {
            "$addFields": {
                "array_campos_DEFOLIADORES": {
                    "$map": {
                        "input": "$array_campos_DEFOLIADORES",
                        "as": "item",
                        "in": {
                            "nombre": "$$item.nombre",
                            "tipo": "$$item.tipo",
                            "variable": "$$item.variable",
                            "valor": {
                                "$cond": {
                                    "if": {
                                        "$or": [
                                            { "$eq": ["$$item.valor", ""] }
                                        ]
                                    },
                                    "then": 0,
                                    "else": { "$toDouble": "$$item.valor" }
                                }
                            }

                        }
                    }
                }
            }
        }



        // //-------------
        // //---TEST2
        // ,{$unwind: "$array_campos_DEFOLIADORES"}

        // ,{
        //     $group:{
        //         _id:"$array_campos_DEFOLIADORES.valor"
        //         ,cant:{$sum:1}
        //     }
        // }

        // //---------------


        //calcular campo DEFOLIADORES
        , {
            "$addFields": {
                "DEFOLIADORES_variable_agrupada":
                {
                    "nombre": "DEFOLIADORES",
                    "tipo": "DEFOLIADORES",
                    "variable": "Marceño-Compsus-Pandeleteius-Mimografus-Epitrix-Daños",
                    "valor": {
                        "$reduce": {
                            "input": "$array_campos_DEFOLIADORES.valor",
                            "initialValue": 0,
                            "in": { "$add": ["$$value", "$$this"] }
                        }
                    }
                }
            }
        }

        // //-------------
        // //---TEST3
        // ,{
        //  $match:{
        //      "DEFOLIADORES_variable_agrupada.valor":{$ne:0}
        //  }
        // }

        // //---------------


        //agregar DEFOLIADORES al array
        , {
            "$addFields": {
                "array_plagas_enfermedades": {
                    "$concatArrays": [
                        "$array_plagas_enfermedades",
                        ["$DEFOLIADORES_variable_agrupada"]
                    ]
                }
            }
        }


        , {
            "$project": {
                "Point": 0
                , "DEFOLIADORES_variable_agrupada": 0
                , "array_campos_DEFOLIADORES": 0

                , "Formula": 0
                , "uid": 0
                , "uDate": 0
                , "capture": 0
                , "split_lote": 0
                , "Sampling": 0
                , "supervisor": 0
                // , "XXXXXX": 0


            }
        }


        // //filtrar valor en 0
        // , {
        //     "$addFields": {
        //         "array_plagas_enfermedades": {
        //             "$filter": {
        //                 "input": "$array_plagas_enfermedades",
        //                 "as": "item",
        //                 "cond": { "$ne": ["$$item.valor", 0] }
        //             }
        //         }
        //     }
        // }

        // , {
        //     "$match": {
        //         "array_plagas_enfermedades": { "$ne": [] }
        //     }

        // }





        //=============AGRUPAMIENTO FINAL
        , { "$unwind": "$array_plagas_enfermedades" }

        , {
            "$addFields": {
                "tiene_plaga_enfermedad": {
                    "$cond": {
                        "if": {
                            "$gt": ["$array_plagas_enfermedades.valor",0]
                        },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"

                    , "plaga_enfermedad": "$array_plagas_enfermedades.nombre"

                }
                , "plantas_dif_censadas_x_lote": { "$min": "$plantas_dif_censadas_x_lote" }
                , "sum_valor": { "$sum": "$array_plagas_enfermedades.valor" }
                // , "plantas_dif_censadas_x_plaga_enfermedad": { "$sum": 1 } //!!!danger
                , "plantas_dif_censadas_x_plaga_enfermedad": { "$sum": "$tiene_plaga_enfermedad" } //!!!danger
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"

                    , "plaga_enfermedad": "$_id.plaga_enfermedad"
                }
                , "plantas_dif_censadas_x_lote": { "$min": "$plantas_dif_censadas_x_lote" }
                , "sum_sum_valor": { "$sum": "$sum_valor" }
                , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": { "$sum": "$plantas_dif_censadas_x_plaga_enfermedad" }//!!!danger
                , "data": { "$push": "$$ROOT" }
            }
        }

        // // , { "$unwind": "$data" }
        // // , { "$unwind": "$data.data" }


        // // , {
        // //     "$replaceRoot": {
        // //         "newRoot": {
        // //             "$mergeObjects": [
        // //                 "$data.data",
        // //                 {
        // //                     "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
        // //                 }
        // //             ]
        // //         }
        // //     }
        // // }




    ]
    , { allowDiskUse: true }
)

    // .count()


/*
	"" : "0",
	"" : "0",
	"" : "0",
	"" : 0,
	"Tipos de Escamas" : [ ],
	"" : "0",
	"" : "0",
	"" : "0",
	"" : "0",
	"" : "0",
	"Larvas" : "0",
	"" : "0",
	"" : 0,
	"" : 0,
	"" : 0,
	"" : 0,
	"Insectos Beneficos" : [ ],
	"" : 0,
	"" : 0,
	"Fumagina" : "No",
	"Muerte descendente  Posible verticilium" : "No",
	"" : "No",
	"Point" : --------
	"" : 0,
	"Cantidad de ramas afectadas por Oncideres" : 0,
	"" : "No",
	"" : "No",
	"Antracnosis" : "No",
	"Lasiodiplodia" : "No",
	"Observaciones" : "Sano ",
	"Acaro" : "",
	"Defoliadores" : "",
	"" : "",
*/
