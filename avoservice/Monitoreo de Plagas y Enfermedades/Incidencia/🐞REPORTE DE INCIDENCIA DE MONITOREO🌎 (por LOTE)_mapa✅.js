[



    {
        "$match": {
            "Estado del monitoreo": { "$exists": true }
        }
    },

    {
        "$match": {
            "$expr": {
                "$gte": [
                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                    ,
                    { "$toDate": { "$dateFromString": { "format": "%Y-%m-%d", "dateString": "2023-01-20" } } }
                ]
            }
        }
    },

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },



    {
        "$addFields": {
            "lote_id": { "$ifNull": ["$lote._id", null] }
        }
    },

    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "cartography_id": { "$ifNull": ["$arbol._id", null] }
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0


            , "Arbol": 0
        }
    }



    , {
        "$addFields": {
            "split_lote": { "$split": ["$lote", " "] }
        }
    }


    , {
        "$addFields": {
            "size_split_lote": { "$size": "$split_lote" }
        }
    }

    , {
        "$addFields": {
            "aux_size_split_lote": {
                "$subtract": ["$size_split_lote", 2]
            }
        }
    }

    , {
        "$addFields": {
            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
        }
    }



    , {
        "$addFields": {

            "UP": {
                "$reduce": {
                    "input": "$split_lote2",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", " ", "$$this"] }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
        }
    }


    , {
        "$addFields": {
            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
        }
    }

    , {
        "$addFields": {
            "Monitor": "$supervisor"
        }
    }


    , {
        "$addFields": {
            "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
            , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
        }
    }




    , {
        "$project": {
            "Point": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "Sampling": 0
            , "supervisor": 0



            , "split_lote": 0
            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0

            , "Observaciones": 0

        }
    }




    , {
        "$addFields": {
            "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
            "Año": { "$year": { "date": "$rgDate" } }
        }
    }










    , {
        "$sort": {
            "rgDate": 1
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "nombre": "$Nombre"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }





    , {
        "$addFields": {
            "datos_inicio": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Estado del monitoreo", "Inicio"] }
                }
            }
        }
    }
    , {
        "$match": {
            "datos_inicio": { "$ne": [] }
        }
    }

    , {
        "$addFields": {
            "fechas_inicio": {
                "$map": {
                    "input": "$datos_inicio",
                    "as": "item",
                    "in": "$$item.rgDate"
                }
            }
        }
    }
    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$fechas_inicio",
                    "as": "item_fechas_inicio",
                    "in": {
                        "fecha_inicio": "$$item_fechas_inicio",

                        "datos_ciclo": {

                            "$filter": {
                                "input": "$data",
                                "as": "item_data",
                                "cond": {
                                    "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$data_ciclos",
                    "as": "item_data_ciclos",
                    "in": {
                        "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                        "datos_ciclo": {


                            "$reduce": {
                                "input": "$$item_data_ciclos.datos_ciclo",
                                "initialValue": {
                                    "estado_ciclo": "Inicio"
                                    , "datos": []
                                },

                                "in": {
                                    "estado_ciclo": {
                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                            "then": "$$value.estado_ciclo",
                                            "else": "$$this.Estado del monitoreo"
                                        }
                                    }

                                    , "datos": {

                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                            "then": "$$value.datos",
                                            "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
    }



    , {
        "$project": {
            "data": 0
        }
    }


    , { "$unwind": "$data_ciclos" }


    , {
        "$addFields": {
            "fecha_fin": {
                "$reduce": {
                    "input": "$data_ciclos.datos_ciclo.datos",
                    "initialValue": "",
                    "in": "$$this.rgDate"
                }
            }
        }
    }


    , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_ciclos.datos_ciclo.datos",
                    {

                        "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                        , "ciclo_fecha_fin": "$fecha_fin"
                        , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                    }
                ]
            }
        }
    }








    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"


                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"


                , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                , "ciclo_estado": "$_id.ciclo_estado"

            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": [



                {
                    "nombre": "ACAROH",
                    "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                },
                {
                    "nombre": "ACARON",
                    "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                },
                {
                    "nombre": "ACAROA",
                    "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                },

                {
                    "nombre": "MOSCAB",
                    "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                },

                {
                    "nombre": "ESCAMA",
                    "valor": { "$ifNull": ["$Escama", 0] }
                },


                {
                    "nombre": "MONALONION",
                    "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                },


                {
                    "nombre": "BARRENADOR",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                },
                {
                    "nombre": "MARCENO",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "TRIPS",
                    "valor": { "$ifNull": ["$Trips", 0] }
                },


                {
                    "nombre": "MOSCAO",
                    "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                },


                {
                    "nombre": "XILEBORUS",
                    "valor": { "$ifNull": ["$Xyleborus", "No"] }
                },


                {
                    "nombre": "STENOMA",
                    "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                },
                {
                    "nombre": "HEILIPUS",
                    "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                },
                {
                    "nombre": "COMPSUS",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },


                {
                    "nombre": "ACARTONAMIENTO",
                    "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                },



                {
                    "nombre": "CLOROSIS",
                    "valor": { "$ifNull": ["$Clorosis", "No"] }
                },



                {
                    "nombre": "CHANCRO",
                    "valor": { "$ifNull": ["$Chancro", "No"] }
                }



                , {
                    "nombre": "ONCIDERES",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                }

                , {
                    "nombre": "VERTICILIUM",
                    "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                }
                , {
                    "nombre": "FUMAGINA",
                    "valor": { "$ifNull": ["$Fumagina", "No"] }
                }
                , {
                    "nombre": "ANTRACNOSIS",
                    "valor": { "$ifNull": ["$Antracnosis", "No"] }
                }
                , {
                    "nombre": "LASIODIPLODIA",
                    "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                }




            ]

            , "array_campos_DEFOLIADORES": [

                {
                    "nombre": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },
                {
                    "nombre": "Pandeleteius",
                    "valor": { "$ifNull": ["$Pandeleteius", 0] }
                },
                {
                    "nombre": "Mimografus",
                    "valor": { "$ifNull": ["$Mimografus", 0] }
                },
                {
                    "nombre": "Epitrix",
                    "valor": { "$ifNull": ["$Epitrix", 0] }
                },
                {
                    "nombre": "Daños",
                    "valor": { "$ifNull": ["$Daños", 0] }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                        , { "$eq": ["$$item.valor", "no"] }
                                        , { "$eq": ["$$item.valor", "No"] }
                                        , { "$eq": ["$$item.valor", "NO"] }


                                    ]
                                },
                                "then": 0,
                                "else": "$$item.valor"
                            }


                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", "si"] }
                                        , { "$eq": ["$$item.valor", "Si"] }
                                        , { "$eq": ["$$item.valor", "SI"] }
                                    ]
                                },
                                "then": 1,
                                "else": "$$item.valor"
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "valor": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                "then": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } },
                                "else": { "$toDouble": "$$item.valor" }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_campos_DEFOLIADORES": {
                "$map": {
                    "input": "$array_campos_DEFOLIADORES",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",

                        "valor": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$$item.valor" }, "double"] },
                                "then": "$$item.valor",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [
                                                { "$eq": [{ "$type": "$$item.valor" }, "string"] },
                                                { "$eq": ["$$item.valor", ""] }
                                            ]
                                        },
                                        "then": 0,
                                        "else": { "$toDouble": { "$trim": { "input": { "$trim": { "input": "$$item.valor" } } } } }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "DEFOLIADORES_variable_agrupada":
            {
                "nombre": "DEFOLIADORES",
                "valor": {
                    "$reduce": {
                        "input": "$array_campos_DEFOLIADORES.valor",
                        "initialValue": 0,
                        "in": { "$add": ["$$value", "$$this"] }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$concatArrays": [
                    "$array_plagas_enfermedades",
                    ["$DEFOLIADORES_variable_agrupada"]
                ]
            }
        }
    }


    , {
        "$project": {
            "Point": 0
            , "DEFOLIADORES_variable_agrupada": 0
            , "array_campos_DEFOLIADORES": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "split_lote": 0
            , "Sampling": 0
            , "supervisor": 0


            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0




            , "Acaro Huevo": 0
            , "Acaro Ninfa": 0
            , "Acaro Adulto": 0
            , "Escama": 0
            , "Tipos de Escamas": 0
            , "Marceño": 0
            , "Compsus": 0
            , "Pandeleteius": 0
            , "Mimografus": 0
            , "Epitrix": 0
            , "Larvas": 0
            , "Daños": 0
            , "Mosca Blanca": 0
            , "Trips": 0
            , "Frutos afectados por Mosca del Ovario": 0
            , "Insectos Beneficos": 0
            , "Ninfa": 0
            , "Adulto": 0
            , "Cantidad de Larvas o daños de Stenoma": 0
            , "cantidad de individuos o daños de Heilipus": 0
            , "Fumagina": 0
            , "Muerte descendente  Posible verticilium": 0
            , "Clorosis": 0
            , "Cantidad de ramas afectadas por barrenador": 0
            , "Cantidad de ramas afectadas por Oncideres": 0
            , "Xyleborus": 0
            , "Chancro": 0
            , "Antracnosis": 0
            , "Lasiodiplodia": 0
            , "Acaro": 0
            , "Defoliadores": 0
            , "Monalonion": 0
            , "Observaciones": 0
            , "Arboles afectados por Monalonion": 0
            , "Huevo": 0
            , "Acartonamiento": 0

            , "rgDate": 0


            , "Busqueda inicio": 0
            , "Busqueda fin": 0
            , "today": 0




        }
    }



    , {
        "$addFields": {
            "ciclo_año": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                }
            }
            , "ciclo_semana": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }


    , {
        "$addFields": {
            "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
            , "ciclo_fecha_fin": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }



    , {
        "$group": {
            "_id": {


                "up": "$UP",
                "lote": "$Lote",
                "nombre": "$Nombre"

                , "plagas": "$array_plagas_enfermedades.nombre"

                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"
                , "ciclo_año": "$ciclo_año"
                , "ciclo_semana": "$ciclo_semana"


                ,"lote_id":"$lote_id"



            }
            , "plantas_dif_censadas_x_lote": { "$max": "$plantas_dif_censadas_x_lote" }
            , "monitor": { "$max": "$Monitor" }
            , "data": { "$push": "$$ROOT" }
        }
    }



    , {
        "$addFields": {
            "data_plagas": {
                "$map": {
                    "input": "$_id.plagas",
                    "as": "item_plagas",
                    "in": {
                        "nombre": "$$item_plagas"

                        , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                            "$map": {
                                "input": "$data",
                                "as": "item_data",
                                "in": {
                                    "$reduce": {
                                        "input": "$$item_data.array_plagas_enfermedades",
                                        "initialValue": 0,
                                        "in": {
                                            "$cond": {
                                                "if": { "$eq": ["$$this.nombre", "$$item_plagas"] },
                                                "then": {
                                                    "$cond": {
                                                        "if": { "$eq": ["$$this.valor", 0] },
                                                        "then": { "$add": ["$$value", 0] },
                                                        "else": { "$add": ["$$value", 1] }
                                                    }
                                                },
                                                "else": { "$add": ["$$value", 0] }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    , {
        "$addFields": {
            "data_plagas2": {
                "$map": {
                    "input": "$data_plagas",
                    "as": "item_data_plagas",
                    "in": {
                        "nombre": "$$item_data_plagas.nombre"

                        , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": {
                            "$reduce": {
                                "input": "$$item_data_plagas.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                                "initialValue": 0,
                                "in": { "$add": ["$$value", "$$this"] }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$project": {
            "data": 0,
            "data_plagas": 0

            , "_id.plagas": 0
        }
    }


    , {
        "$unwind": {
            "path": "$data_plagas2",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": {
                "$cond": {
                    "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$data_plagas2.plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                            "$plantas_dif_censadas_x_lote"]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": {
                "$multiply": ["$PORCENTAJE_INIDENCIA", 100]
            }
        }
    }

    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, 1] }] }, 100] }
        }
    }

    , {
        "$addFields": {
            "Nivel_INCIDENCIA": {
                "$switch": {
                    "branches": [


                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "ACAROA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "ACAROH"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "ACARON"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "ACARTONAMIENTO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "COMPSUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "DEFOLIADORES"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "ESCAMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "MARCENO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "MOSCAB"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "MOSCAO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "TRIPS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },



                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "BARRENADOR"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "CHANCRO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "CLOROSIS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "HEILIPUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "MONALONION"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "STENOMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$data_plagas2.nombre", "XILEBORUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }


                        , {
                            "case": { "$eq": ["$data_plagas2.nombre", "ONCIDERES"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$data_plagas2.nombre", "VERTICILIUM"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$data_plagas2.nombre", "FUMAGINA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$data_plagas2.nombre", "ANTRACNOSIS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$data_plagas2.nombre", "LASIODIPLODIA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }


                    ], "default": null
                }
            }
        }
    }



    , {
        "$lookup": {
            "from": "cartography",
            "localField": "_id.lote_id",
            "foreignField": "_id",
            "as": "info_lote"
        }
    }
    , { "$unwind": "$info_lote" }
    , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
    , {
        "$project": {
            "info_lote": 0
        }
    }

    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 0] }
                            , "then": "#808080"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 1] }
                            , "then": "#008000"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 2] }
                            , "then": "#ffff00"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 3] }
                            , "then": "#ff0000"
                        }

                    ],
                    "default": "#808080"
                }
            }

            , "rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 0] }
                            , "then": "Nivel 0"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 1] }
                            , "then": "Nivel 1"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 2] }
                            , "then": "Nivel 2"
                        },
                        {
                            "case": { "$eq": ["$Nivel_INCIDENCIA", 3] }
                            , "then": "Nivel 3"
                        }

                    ],
                    "default": "Nivel 0"
                }
            }

        }
    }



    , {
        "$project": {

            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",


            "properties": {
                "UP": "$_id.up",
                "Lote": "$_id.lote",
                "Nombre": "$_id.nombre",

                "Año": { "$toString": "$_id.ciclo_año" },
                "Fecha Inicio": "$_id.ciclo_fecha_inicio",
                "Fecha Fin": "$_id.ciclo_fecha_fin",
                "Estado Ciclo": "$_id.ciclo_estado",
                "Semana": { "$toString": "$_id.ciclo_semana" },


                "Nivel": { "$ifNull": ["$rango", "SIN DATOS"] },
                "color": "$color"

                , "Plaga": "$data_plagas2.nombre"
                , "% Incidencia": { "$concat": [{ "$toString": "$PORCENTAJE_INIDENCIA" }, " %"] }
            }

        }
    }





]
