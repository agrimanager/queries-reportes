[


    {
        "$match": {
            "Estado del monitoreo": { "$exists": true }
        }
    },

    {
        "$match": {
            "$expr": {
                "$gte": [
                    { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                    ,
                    { "$toDate": { "$dateFromString": { "format": "%Y-%m-%d", "dateString": "2023-01-20" } } }
                ]
            }
        }
    },

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },



    {
        "$addFields": {
            "lote_id": { "$ifNull": ["$lote._id", null] }
        }
    },
    { "$addFields": { "num_arboles_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields": {
            "cartography_id": { "$ifNull": ["$arbol._id", null] }
        }
    },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0


            , "Arbol": 0
        }
    }



    , {
        "$addFields": {
            "split_lote": { "$split": ["$lote", " "] }
        }
    }


    , {
        "$addFields": {
            "size_split_lote": { "$size": "$split_lote" }
        }
    }

    , {
        "$addFields": {
            "aux_size_split_lote": {
                "$subtract": ["$size_split_lote", 2]
            }
        }
    }

    , {
        "$addFields": {
            "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
        }
    }



    , {
        "$addFields": {

            "UP": {
                "$reduce": {
                    "input": "$split_lote2",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", " ", "$$this"] }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Lote": { "$arrayElemAt": ["$split_lote", -1] }
        }
    }


    , {
        "$addFields": {
            "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
        }
    }

    , {
        "$addFields": {
            "X": { "$arrayElemAt": ["$Point.geometry.coordinates", 0] },
            "Y": { "$arrayElemAt": ["$Point.geometry.coordinates", 1] }
        }
    }

    , {
        "$addFields": {
            "Monitor": "$supervisor"
        }
    }



    , {
        "$project": {
            "Point": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "Sampling": 0
            , "supervisor": 0



            , "split_lote": 0
            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0

        }
    }



    , {
        "$addFields": {
            "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
            "Año": { "$year": { "date": "$rgDate" } },
            "Semana": { "$week": { "date": "$rgDate" } }
            , "Hora": { "$dateToString": { "format": "%H:%M:%S", "date": "$rgDate", "timezone": "America/Bogota" } }
        }
    }






    , {
        "$sort": {
            "rgDate": 1
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "nombre": "$Nombre"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }





    , {
        "$addFields": {
            "datos_inicio": {
                "$filter": {
                    "input": "$data",
                    "as": "item",
                    "cond": { "$eq": ["$$item.Estado del monitoreo", "Inicio"] }
                }
            }
        }
    }
    , {
        "$match": {
            "datos_inicio": { "$ne": [] }
        }
    }

    , {
        "$addFields": {
            "fechas_inicio": {
                "$map": {
                    "input": "$datos_inicio",
                    "as": "item",
                    "in": "$$item.rgDate"
                }
            }
        }
    }



    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$fechas_inicio",
                    "as": "item_fechas_inicio",
                    "in": {
                        "fecha_inicio": "$$item_fechas_inicio",

                        "datos_ciclo": {

                            "$filter": {
                                "input": "$data",
                                "as": "item_data",
                                "cond": {
                                    "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "data_ciclos": {
                "$map": {
                    "input": "$data_ciclos",
                    "as": "item_data_ciclos",
                    "in": {
                        "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                        "datos_ciclo": {


                            "$reduce": {
                                "input": "$$item_data_ciclos.datos_ciclo",
                                "initialValue": {
                                    "estado_ciclo": "Inicio"
                                    , "datos": []
                                },

                                "in": {
                                    "estado_ciclo": {
                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                            "then": "$$value.estado_ciclo",
                                            "else": "$$this.Estado del monitoreo"
                                        }
                                    }

                                    , "datos": {

                                        "$cond": {
                                            "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                            "then": "$$value.datos",
                                            "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
            }
        }
    }




    , {
        "$project": {
            "data": 0
        }
    }


    , { "$unwind": "$data_ciclos" }


    , {
        "$addFields": {
            "fecha_fin": {
                "$reduce": {
                    "input": "$data_ciclos.datos_ciclo.datos",
                    "initialValue": "",
                    "in": "$$this.rgDate"
                }
            }
        }
    }


    , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_ciclos.datos_ciclo.datos",
                    {

                        "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                        , "ciclo_fecha_fin": "$fecha_fin"
                        , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                    }
                ]
            }
        }
    }






    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"


                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"

            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"


                , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                , "ciclo_estado": "$_id.ciclo_estado"

            },
            "plantas_dif_censadas_x_lote": { "$sum": 1 },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "array_plagas_enfermedades": [



                {
                    "nombre": "ACAROH",
                    "tipo": "Acaro",
                    "variable": "Acaro Huevo",
                    "valor": { "$ifNull": ["$Acaro Huevo", 0] }
                },
                {
                    "nombre": "ACARON",
                    "tipo": "Acaro",
                    "variable": "Acaro Ninfa",
                    "valor": { "$ifNull": ["$Acaro Ninfa", 0] }
                },
                {
                    "nombre": "ACAROA",
                    "tipo": "Acaro",
                    "variable": "Acaro Adulto",
                    "valor": { "$ifNull": ["$Acaro Adulto", 0] }
                },

                {
                    "nombre": "MOSCAB",
                    "tipo": "Mosca",
                    "variable": "Mosca Blanca",
                    "valor": { "$ifNull": ["$Mosca Blanca", 0] }
                },

                {
                    "nombre": "ESCAMA",
                    "tipo": "Escama",
                    "variable": "Escama",
                    "valor": { "$ifNull": ["$Escama", 0] }
                },


                {
                    "nombre": "MONALONION",
                    "tipo": "otro",
                    "variable": "Arboles afectados por Monalonion",
                    "valor": { "$ifNull": ["$Arboles afectados por Monalonion", 0] }
                },


                {
                    "nombre": "BARRENADOR",
                    "tipo": "otro",
                    "variable": "Cantidad de ramas afectadas por barrenador",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por barrenador", 0] }
                },
                {
                    "nombre": "MARCENO",
                    "tipo": "otro",
                    "variable": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "TRIPS",
                    "tipo": "otro",
                    "variable": "Trips",
                    "valor": { "$ifNull": ["$Trips", 0] }
                },


                {
                    "nombre": "MOSCAO",
                    "tipo": "Mosca",
                    "variable": "Frutos afectados por Mosca del Ovario",
                    "valor": { "$ifNull": ["$Frutos afectados por Mosca del Ovario", 0] }
                },


                {
                    "nombre": "XILEBORUS",
                    "tipo": "otro",
                    "variable": "Xyleborus",
                    "valor": { "$ifNull": ["$Xyleborus", "No"] }
                },


                {
                    "nombre": "STENOMA",
                    "tipo": "otro",
                    "variable": "Cantidad de Larvas o daños de Stenoma",
                    "valor": { "$ifNull": ["$Cantidad de Larvas o daños de Stenoma", 0] }
                },
                {
                    "nombre": "HEILIPUS",
                    "tipo": "otro",
                    "variable": "cantidad de individuos o daños de Heilipus",
                    "valor": { "$ifNull": ["$cantidad de individuos o daños de Heilipus", 0] }
                },
                {
                    "nombre": "COMPSUS",
                    "tipo": "otro",
                    "variable": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },


                {
                    "nombre": "ACARTONAMIENTO",
                    "tipo": "otro",
                    "variable": "Acartonamiento",
                    "valor": { "$ifNull": ["$Acartonamiento", "NO"] }
                },



                {
                    "nombre": "CLOROSIS",
                    "tipo": "otro",
                    "variable": "Clorosis",
                    "valor": { "$ifNull": ["$Clorosis", "No"] }
                },



                {
                    "nombre": "CHANCRO",
                    "tipo": "otro",
                    "variable": "Chancro",
                    "valor": { "$ifNull": ["$Chancro", "No"] }
                }




                , {
                    "nombre": "ONCIDERES",
                    "tipo": "otro",
                    "variable": "Cantidad de ramas afectadas por Oncideres",
                    "valor": { "$ifNull": ["$Cantidad de ramas afectadas por Oncideres", 0] }
                }

                , {
                    "nombre": "VERTICILIUM",
                    "tipo": "otro",
                    "variable": "Muerte descendente  Posible verticilium",
                    "valor": { "$ifNull": ["$Muerte descendente  Posible verticilium", "No"] }
                }
                , {
                    "nombre": "FUMAGINA",
                    "tipo": "otro",
                    "variable": "Fumagina",
                    "valor": { "$ifNull": ["$Fumagina", "No"] }
                }
                , {
                    "nombre": "ANTRACNOSIS",
                    "tipo": "otro",
                    "variable": "Antracnosis",
                    "valor": { "$ifNull": ["$Antracnosis", "No"] }
                }
                , {
                    "nombre": "LASIODIPLODIA",
                    "tipo": "otro",
                    "variable": "Lasiodiplodia",
                    "valor": { "$ifNull": ["$Lasiodiplodia", "No"] }
                }




            ]

            , "array_campos_DEFOLIADORES": [

                {
                    "nombre": "Marceño",
                    "tipo": "DEFOLIADORES",
                    "variable": "Marceño",
                    "valor": { "$ifNull": ["$Marceño", 0] }
                },
                {
                    "nombre": "Compsus",
                    "tipo": "DEFOLIADORES",
                    "variable": "Compsus",
                    "valor": { "$ifNull": ["$Compsus", 0] }
                },
                {
                    "nombre": "Pandeleteius",
                    "tipo": "DEFOLIADORES",
                    "variable": "Pandeleteius",
                    "valor": { "$ifNull": ["$Pandeleteius", 0] }
                },
                {
                    "nombre": "Mimografus",
                    "tipo": "DEFOLIADORES",
                    "variable": "Mimografus",
                    "valor": { "$ifNull": ["$Mimografus", 0] }
                },
                {
                    "nombre": "Epitrix",
                    "tipo": "DEFOLIADORES",
                    "variable": "Epitrix",
                    "valor": { "$ifNull": ["$Epitrix", 0] }
                },
                {
                    "nombre": "Daños",
                    "tipo": "DEFOLIADORES",
                    "variable": "Daños",
                    "valor": { "$ifNull": ["$Daños", 0] }
                }

            ]
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                        , { "$eq": ["$$item.valor", "no"] }
                                        , { "$eq": ["$$item.valor", "No"] }
                                        , { "$eq": ["$$item.valor", "NO"] }


                                    ]
                                },
                                "then": 0,
                                "else": "$$item.valor"
                            }


                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {

                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", "si"] }
                                        , { "$eq": ["$$item.valor", "Si"] }
                                        , { "$eq": ["$$item.valor", "SI"] }
                                    ]
                                },
                                "then": 1,
                                "else": "$$item.valor"
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$map": {
                    "input": "$array_plagas_enfermedades",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": { "$toDouble": "$$item.valor" }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "array_campos_DEFOLIADORES": {
                "$map": {
                    "input": "$array_campos_DEFOLIADORES",
                    "as": "item",
                    "in": {
                        "nombre": "$$item.nombre",
                        "tipo": "$$item.tipo",
                        "variable": "$$item.variable",
                        "valor": {
                            "$cond": {
                                "if": {
                                    "$or": [
                                        { "$eq": ["$$item.valor", ""] }
                                    ]
                                },
                                "then": 0,
                                "else": { "$toDouble": "$$item.valor" }
                            }
                        }

                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "DEFOLIADORES_variable_agrupada":
            {
                "nombre": "DEFOLIADORES",
                "tipo": "DEFOLIADORES",
                "variable": "Marceño-Compsus-Pandeleteius-Mimografus-Epitrix-Daños",
                "valor": {
                    "$reduce": {
                        "input": "$array_campos_DEFOLIADORES.valor",
                        "initialValue": 0,
                        "in": { "$add": ["$$value", "$$this"] }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "array_plagas_enfermedades": {
                "$concatArrays": [
                    "$array_plagas_enfermedades",
                    ["$DEFOLIADORES_variable_agrupada"]
                ]
            }
        }
    }



    , {
        "$project": {
            "Point": 0
            , "DEFOLIADORES_variable_agrupada": 0
            , "array_campos_DEFOLIADORES": 0

            , "Formula": 0
            , "uid": 0
            , "uDate": 0
            , "capture": 0
            , "split_lote": 0
            , "Sampling": 0
            , "supervisor": 0


            , "size_split_lote": 0
            , "aux_size_split_lote": 0
            , "split_lote2": 0




            , "Acaro Huevo": 0
            , "Acaro Ninfa": 0
            , "Acaro Adulto": 0
            , "Escama": 0
            , "Tipos de Escamas": 0
            , "Marceño": 0
            , "Compsus": 0
            , "Pandeleteius": 0
            , "Mimografus": 0
            , "Epitrix": 0
            , "Larvas": 0
            , "Daños": 0
            , "Mosca Blanca": 0
            , "Trips": 0
            , "Frutos afectados por Mosca del Ovario": 0
            , "Insectos Beneficos": 0
            , "Ninfa": 0
            , "Adulto": 0
            , "Cantidad de Larvas o daños de Stenoma": 0
            , "cantidad de individuos o daños de Heilipus": 0
            , "Fumagina": 0
            , "Muerte descendente  Posible verticilium": 0
            , "Clorosis": 0
            , "Cantidad de ramas afectadas por barrenador": 0
            , "Cantidad de ramas afectadas por Oncideres": 0
            , "Xyleborus": 0
            , "Chancro": 0
            , "Antracnosis": 0
            , "Lasiodiplodia": 0
            , "Acaro": 0
            , "Defoliadores": 0
            , "Monalonion": 0
            , "Observaciones": 0
            , "Arboles afectados por Monalonion": 0
            , "Huevo": 0
            , "Acartonamiento": 0

            , "rgDate": 0


            , "Busqueda inicio": 0
            , "Busqueda fin": 0
            , "today": 0




        }
    }

    , { "$unwind": "$array_plagas_enfermedades" }

    , {
        "$addFields": {
            "tiene_plaga_enfermedad": {
                "$cond": {
                    "if": {
                        "$gt": ["$array_plagas_enfermedades.valor", 0]
                    },
                    "then": 1,
                    "else": 0
                }
            }
        }
    }




    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol"

                , "plaga_enfermedad": "$array_plagas_enfermedades.nombre"


                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"

            }
            , "plantas_dif_censadas_x_lote": { "$min": "$plantas_dif_censadas_x_lote" }
            , "sum_valor_arbol": { "$sum": "$array_plagas_enfermedades.valor" }

            , "plantas_dif_censadas_x_plaga_enfermedad": { "$sum": "$tiene_plaga_enfermedad" }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

                , "plaga_enfermedad": "$_id.plaga_enfermedad"



                , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                , "ciclo_estado": "$_id.ciclo_estado"

            }
            , "plantas_dif_censadas_x_lote": { "$min": "$plantas_dif_censadas_x_lote" }
            , "sum_valor_lote": { "$sum": "$sum_valor_arbol" }
            , "plantas_dif_censadas_x_plaga_enfermedad_x_lote": { "$sum": "$plantas_dif_censadas_x_plaga_enfermedad" }
            , "data": { "$push": "$$ROOT" }
        }
    }


    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": {
                "$cond": {
                    "if": { "$eq": ["$plantas_dif_censadas_x_lote", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                            "$plantas_dif_censadas_x_lote"]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": {
                "$multiply": ["$PORCENTAJE_INIDENCIA", 100]
            }
        }
    }

    , {
        "$addFields": {
            "PORCENTAJE_INIDENCIA": { "$divide": [{ "$subtract": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, { "$mod": [{ "$multiply": ["$PORCENTAJE_INIDENCIA", 100] }, 1] }] }, 100] }
        }
    }


    , {
        "$addFields": {
            "Nivel_INCIDENCIA": {
                "$switch": {
                    "branches": [


                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ACAROA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ACAROH"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ACARON"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ACARTONAMIENTO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "COMPSUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "DEFOLIADORES"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ESCAMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "MARCENO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "MOSCAB"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "MOSCAO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "TRIPS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 15] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 15] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 30] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 30] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },



                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "BARRENADOR"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "CHANCRO"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "CLOROSIS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "HEILIPUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "MONALONION"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "STENOMA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        },
                        {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "XILEBORUS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }



                        , {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ONCIDERES"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "VERTICILIUM"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "FUMAGINA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "ANTRACNOSIS"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }
                        , {
                            "case": { "$eq": ["$_id.plaga_enfermedad", "LASIODIPLODIA"] },
                            "then": {
                                "$switch": {
                                    "branches": [
                                        {
                                            "case": { "$eq": ["$PORCENTAJE_INIDENCIA", 0] },
                                            "then": 0
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 0] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 5] }
                                                ]
                                            },
                                            "then": 1
                                        },
                                        {
                                            "case": {
                                                "$and": [
                                                    { "$gt": ["$PORCENTAJE_INIDENCIA", 5] }
                                                    , { "$lte": ["$PORCENTAJE_INIDENCIA", 10] }
                                                ]
                                            },
                                            "then": 2
                                        },
                                        {
                                            "case": { "$gt": ["$PORCENTAJE_INIDENCIA", 10] },
                                            "then": 3
                                        }

                                    ], "default": null
                                }

                            }
                        }


                    ], "default": null
                }
            }
        }
    }




    , { "$unwind": "$data" }
    , { "$unwind": "$data.data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {
                        "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote",
                        "sum_valor_lote": "$sum_valor_lote",
                        "plantas_dif_censadas_x_plaga_enfermedad_x_lote": "$plantas_dif_censadas_x_plaga_enfermedad_x_lote",
                        "PORCENTAJE_INIDENCIA": "$PORCENTAJE_INIDENCIA",
                        "Nivel_INCIDENCIA": "$Nivel_INCIDENCIA"
                    }
                ]
            }
        }
    }



    , {
        "$addFields": {
            "Plaga": "$array_plagas_enfermedades.nombre"
            , "Valor": "$array_plagas_enfermedades.valor"
        }
    }
    ,
    {
        "$project": {
            "array_plagas_enfermedades": 0
        }
    }



    , {
        "$addFields": {
            "ciclo_año": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                }
            }
            , "ciclo_semana": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }




    , {
        "$addFields": {
            "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
            , "ciclo_fecha_fin": {
                "$cond": {
                    "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                    "then": "",
                    "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                }
            }
        }
    }




    , {
        "$addFields": {
            "indicador": "$PORCENTAJE_INIDENCIA"
        }
    }
    , {
        "$addFields": {
            "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
        }
    }




    , {
        "$addFields": {
            "elemento_agrupacion": "$Plaga"
        }
    }
    , {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote"
                , "arbol": "$arbol"


                , "lote_id": "$lote_id"
                , "lote_nombre": "$Nombre"
                , "nivel_incidencia": "$Nivel_INCIDENCIA"

                , "up": "$UP"
                , "lote_num": "$Lote"


                , "idform": "$idform"
                , "elemento_agrupacion": "$elemento_agrupacion"



                , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                , "ciclo_estado": "$ciclo_estado"
                , "ciclo_año": "$ciclo_año"
                , "ciclo_semana": "$ciclo_semana"

            }
            , "indicador": { "$min": "$indicador" }
            , "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote"

                , "lote_id": "$_id.lote_id"
                , "lote_nombre": "$_id.lote_nombre"
                , "nivel_incidencia": "$_id.nivel_incidencia"


                , "up": "$_id.up"
                , "elemento_agrupacion": "$_id.elemento_agrupacion"

                , "idform": "$_id.idform"
                , "lote_num": "$_id.lote_num"


                , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                , "ciclo_estado": "$_id.ciclo_estado"
                , "ciclo_año": "$_id.ciclo_año"
                , "ciclo_semana": "$_id.ciclo_semana"
            }
            , "indicador": { "$min": "$indicador" }
            , "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "indicador": "$indicador"

                        , "cartography_id": {
                            "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.cartography_id", 0] }, 0]
                        }
                    }
                ]
            }
        }
    }


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "lote_id",
            "foreignField": "_id",
            "as": "info_lote"
        }
    }
    , { "$unwind": "$info_lote" }
    , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
    , {
        "$project": {
            "info_lote": 0
        }
    }

    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$nivel_incidencia", 0] }
                            , "then": "#808080"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 1] }
                            , "then": "#008000"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 2] }
                            , "then": "#ffff00"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 3] }
                            , "then": "#ff0000"
                        }

                    ],
                    "default": "#808080"
                }
            }

            , "rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$nivel_incidencia", 0] }
                            , "then": "Nivel 0"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 1] }
                            , "then": "Nivel 1"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 2] }
                            , "then": "Nivel 2"
                        },
                        {
                            "case": { "$eq": ["$nivel_incidencia", 3] }
                            , "then": "Nivel 3"
                        }

                    ],
                    "default": "Nivel 0"
                }
            }

        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": "$cartography_id"
            , "cartography_geometry": "$cartography_geometry"

            , "color": "$color"
            , "rango": "$rango"
        }
    }


    , {
        "$project": {

            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",


            "properties": {

                "UP": "$up",
                "Lote": "$lote_num",
                "Nombre": "$lote_nombre",

                "Año": { "$toString": "$ciclo_año" },
                "Fecha Inicio": "$ciclo_fecha_inicio",
                "Fecha Fin": "$ciclo_fecha_fin",
                "Estado Ciclo": "$ciclo_estado",
                "Semana": { "$toString": "$ciclo_semana" },


                "Nivel": { "$ifNull": ["$rango", "SIN DATOS"] },
                "color": "$color"

                , "Plaga": "$elemento_agrupacion"
                , "% Incidencia": { "$concat": [{ "$toString": "$indicador" }, " %"] }
            }

        }
    }



]
