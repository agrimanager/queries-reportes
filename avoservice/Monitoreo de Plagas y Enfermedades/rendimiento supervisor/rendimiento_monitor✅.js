[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_monitoreodeplagasyenfermedades",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [



                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },



                {
                    "$match": {
                        "Estado del monitoreo": { "$exists": true }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateFromString": { "format": "%Y-%m-%d", "dateString": "2023-01-20" } } }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia": "$Arbol"
                    }
                },
                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": {
                                    "$eq": [
                                        { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                        , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                {
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0


                        , "Arbol": 0
                    }
                }



                , {
                    "$addFields": {
                        "split_lote": { "$split": ["$lote", " "] }
                    }
                }


                , {
                    "$addFields": {
                        "size_split_lote": { "$size": "$split_lote" }
                    }
                }

                , {
                    "$addFields": {
                        "aux_size_split_lote": {
                            "$subtract": ["$size_split_lote", 2]
                        }
                    }
                }

                , {
                    "$addFields": {
                        "split_lote2": { "$slice": ["$split_lote", 1, "$aux_size_split_lote"] }
                    }
                }



                , {
                    "$addFields": {

                        "UP": {
                            "$reduce": {
                                "input": "$split_lote2",
                                "initialValue": "",
                                "in": {
                                    "$cond": {
                                        "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                        "then": "$$this",
                                        "else": { "$concat": ["$$value", " ", "$$this"] }
                                    }
                                }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "Lote": { "$arrayElemAt": ["$split_lote", -1] }
                    }
                }


                , {
                    "$addFields": {
                        "Nombre": { "$concat": ["$UP", " ", "$Lote"] }
                    }
                }



                , {
                    "$addFields": {
                        "Monitor": "$supervisor"
                    }
                }



                , {
                    "$project": {
                        "Point": 0
                        , "Formula": 0
                        , "uid": 0
                        , "uDate": 0
                        , "capture": 0
                        , "Sampling": 0
                        , "supervisor": 0



                        , "split_lote": 0
                        , "size_split_lote": 0
                        , "aux_size_split_lote": 0
                        , "split_lote2": 0

                        , "Observaciones": 0

                    }
                }

                , {
                    "$project": {
                        "Point": 0
                        , "DEFOLIADORES_variable_agrupada": 0
                        , "array_campos_DEFOLIADORES": 0

                        , "Formula": 0
                        , "uid": 0
                        , "uDate": 0
                        , "capture": 0
                        , "split_lote": 0
                        , "Sampling": 0
                        , "supervisor": 0


                        , "size_split_lote": 0
                        , "aux_size_split_lote": 0
                        , "split_lote2": 0




                        , "Acaro Huevo": 0
                        , "Acaro Ninfa": 0
                        , "Acaro Adulto": 0
                        , "Escama": 0
                        , "Tipos de Escamas": 0
                        , "Marceño": 0
                        , "Compsus": 0
                        , "Pandeleteius": 0
                        , "Mimografus": 0
                        , "Epitrix": 0
                        , "Larvas": 0
                        , "Daños": 0
                        , "Mosca Blanca": 0
                        , "Trips": 0
                        , "Frutos afectados por Mosca del Ovario": 0
                        , "Insectos Beneficos": 0
                        , "Ninfa": 0
                        , "Adulto": 0
                        , "Cantidad de Larvas o daños de Stenoma": 0
                        , "cantidad de individuos o daños de Heilipus": 0
                        , "Fumagina": 0
                        , "Muerte descendente  Posible verticilium": 0
                        , "Clorosis": 0
                        , "Cantidad de ramas afectadas por barrenador": 0
                        , "Cantidad de ramas afectadas por Oncideres": 0
                        , "Xyleborus": 0
                        , "Chancro": 0
                        , "Antracnosis": 0
                        , "Lasiodiplodia": 0
                        , "Acaro": 0
                        , "Defoliadores": 0
                        , "Monalonion": 0
                        , "Observaciones": 0
                        , "Arboles afectados por Monalonion": 0
                        , "Huevo": 0
                        , "Acartonamiento": 0




                        , "Busqueda inicio": 0
                        , "Busqueda fin": 0
                        , "today": 0
                        , "idform": 0



                    }
                }



                , {
                    "$addFields": {
                        "Fecha": { "$dateToString": { "format": "%d/%m/%Y", "date": "$rgDate" } },
                        "Año": { "$year": { "date": "$rgDate" } }
                    }
                }






                , {
                    "$sort": {
                        "rgDate": 1
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "nombre": "$Nombre"
                        },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }





                , {
                    "$addFields": {
                        "datos_inicio": {
                            "$filter": {
                                "input": "$data",
                                "as": "item",
                                "cond": { "$eq": ["$$item.Estado del monitoreo", "Inicio"] }
                            }
                        }
                    }
                }
                , {
                    "$match": {
                        "datos_inicio": { "$ne": [] }
                    }
                }

                , {
                    "$addFields": {
                        "fechas_inicio": {
                            "$map": {
                                "input": "$datos_inicio",
                                "as": "item",
                                "in": "$$item.rgDate"
                            }
                        }
                    }
                }
                , {
                    "$addFields": {
                        "data_ciclos": {
                            "$map": {
                                "input": "$fechas_inicio",
                                "as": "item_fechas_inicio",
                                "in": {
                                    "fecha_inicio": "$$item_fechas_inicio",

                                    "datos_ciclo": {

                                        "$filter": {
                                            "input": "$data",
                                            "as": "item_data",
                                            "cond": {
                                                "$gte": ["$$item_data.rgDate", "$$item_fechas_inicio"]
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "data_ciclos": {
                            "$map": {
                                "input": "$data_ciclos",
                                "as": "item_data_ciclos",
                                "in": {
                                    "fecha_inicio": "$$item_data_ciclos.fecha_inicio",

                                    "datos_ciclo": {


                                        "$reduce": {
                                            "input": "$$item_data_ciclos.datos_ciclo",
                                            "initialValue": {
                                                "estado_ciclo": "Inicio"
                                                , "datos": []
                                            },

                                            "in": {
                                                "estado_ciclo": {
                                                    "$cond": {
                                                        "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                                        "then": "$$value.estado_ciclo",
                                                        "else": "$$this.Estado del monitoreo"
                                                    }
                                                }

                                                , "datos": {

                                                    "$cond": {
                                                        "if": { "$eq": ["$$value.estado_ciclo", "Finalizo"] },
                                                        "then": "$$value.datos",
                                                        "else": { "$concatArrays": ["$$value.datos", ["$$this"]] }
                                                    }

                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }
                    }
                }



                , {
                    "$project": {
                        "data": 0
                    }
                }


                , { "$unwind": "$data_ciclos" }


                , {
                    "$addFields": {
                        "fecha_fin": {
                            "$reduce": {
                                "input": "$data_ciclos.datos_ciclo.datos",
                                "initialValue": "",
                                "in": "$$this.rgDate"
                            }
                        }
                    }
                }


                , { "$unwind": "$data_ciclos.datos_ciclo.datos" }



                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data_ciclos.datos_ciclo.datos",
                                {

                                    "ciclo_fecha_inicio": "$data_ciclos.fecha_inicio"
                                    , "ciclo_fecha_fin": "$fecha_fin"
                                    , "ciclo_estado": "$data_ciclos.datos_ciclo.estado_ciclo"
                                }
                            ]
                        }
                    }
                }



                , {
                    "$addFields": {
                        "ciclo_año": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$year": { "date": "$ciclo_fecha_fin" } }
                            }
                        }
                        , "ciclo_semana": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$week": { "date": "$ciclo_fecha_fin" } }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "ciclo_fecha_inicio": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_inicio" } }
                        , "ciclo_fecha_fin": {
                            "$cond": {
                                "if": { "$eq": ["$ciclo_fecha_fin", ""] },
                                "then": "",
                                "else": { "$dateToString": { "format": "%d/%m/%Y", "date": "$ciclo_fecha_fin" } }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "dias_ciclo": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        { "$dateFromString": { "dateString": "$ciclo_fecha_fin", "format": "%d/%m/%Y" } },
                                        { "$dateFromString": { "dateString": "$ciclo_fecha_inicio", "format": "%d/%m/%Y" } }
                                    ]
                                },
                                86400000
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "dias_ciclo": { "$floor": "$dias_ciclo" }
                    }
                }





                , {
                    "$group": {
                        "_id": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "arbol": "$arbol"


                            , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                            , "ciclo_estado": "$ciclo_estado"

                        },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }

                , {
                    "$group": {
                        "_id": {
                            "finca": "$_id.finca",
                            "bloque": "$_id.bloque",
                            "lote": "$_id.lote"


                            , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                            , "ciclo_estado": "$_id.ciclo_estado"

                        },
                        "plantas_dif_censadas_x_lote": { "$sum": 1 },
                        "data": {
                            "$push": "$$ROOT"
                        }
                    }
                }

                , { "$unwind": "$data" }
                , { "$unwind": "$data.data" }


                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$data.data",
                                {
                                    "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                                }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "UP": { "$toUpper": { "$trim": { "input": "$UP" } } }
                        , "Nombre": { "$toUpper": { "$trim": { "input": "$Nombre" } } }
                    }
                }




                , {
                    "$group": {
                        "_id": {


                            "up": "$UP",
                            "lote": "$Lote",
                            "nombre": "$Nombre"

                            , "arbol": "$arbol"

                            , "monitor": "$Monitor"
                            , "fecha": "$Fecha"

                            , "ciclo_fecha_inicio": "$ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$ciclo_fecha_fin"
                            , "ciclo_estado": "$ciclo_estado"
                            , "ciclo_año": "$ciclo_año"
                            , "ciclo_semana": "$ciclo_semana"

                            , "dias_ciclo": "$dias_ciclo"
                            , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"



                        }

                    }
                }


                , {
                    "$group": {
                        "_id": {

                            "up": "$_id.up",
                            "lote": "$_id.lote",
                            "nombre": "$_id.nombre"



                            , "monitor": "$_id.monitor"
                            , "fecha": "$_id.fecha"


                            , "ciclo_fecha_inicio": "$_id.ciclo_fecha_inicio"
                            , "ciclo_fecha_fin": "$_id.ciclo_fecha_fin"
                            , "ciclo_estado": "$_id.ciclo_estado"
                            , "ciclo_año": "$_id.ciclo_año"
                            , "ciclo_semana": "$_id.ciclo_semana"

                            , "dias_ciclo": "$_id.dias_ciclo"
                            , "plantas_dif_censadas_x_lote": "$_id.plantas_dif_censadas_x_lote"


                        }

                        , "plantas_dif_censadas_x_lote_x_monitor": { "$sum": 1 }
                    }
                }



                , {
                    "$replaceRoot": {
                        "newRoot": {
                            "$mergeObjects": [
                                "$_id",
                                {

                                    "plantas_dif_censadas_x_lote_x_monitor": "$plantas_dif_censadas_x_lote_x_monitor"
                                }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "rgDate": "$$filtro_fecha_inicio"
                    }
                }


            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }






]
