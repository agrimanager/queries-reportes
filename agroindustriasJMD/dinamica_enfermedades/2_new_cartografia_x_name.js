db.form_dinamicadeenfermedades.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-07-01T06:00:00.000-05:00"),
                "Busqueda fin": ISODate("2021-07-31T06:00:00.000-05:00"),
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        //----filtro de fechas
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },


        //-----new x name
        {
            "$lookup": {
                "from": "cartography",
                "localField": "variable_cartografia.features.properties.name",
                "foreignField": "properties.name",
                "as": "variable_cartografia_x_name"
            }
        },

        { "$unwind": "$variable_cartografia_x_name" },

        {
            "$addFields": {
                // "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia_x_name.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                //"variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia_x_name._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        //-----old x id
        // {
        //     "$addFields": {
        //         "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        //     }
        // },
        // {
        //     "$addFields": {
        //         "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        //     }
        // },
        // {
        //     "$addFields": {
        //         "split_path_oid": {
        //             "$concatArrays": [
        //                 "$split_path_padres_oid",
        //                 "$variable_cartografia_oid"
        //             ]
        //         }
        //     }
        // },


        //----split_path_oid (bien formado)
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },


        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        //{ "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.Numero de plantas.value", "sin datos"] } } },
        { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", "sin datos"] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "erradicada": { "$ifNull": ["$arbol.properties.custom.Erradicada.value", false] } } },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0,
                "uid": 0,
                "capture": 0

                , "variable_cartografia_x_name": 0
            }
        },




        { "$sort": { "rgDate": -1 } },
        {
            "$addFields": {
                "numero_pc_actual": {
                    "$cond": {
                        "if": {
                            "$or": [
                                {
                                    "$and": [
                                        { "$eq": [{ "$type": "$Enfermedad_Pudricion de cogollo" }, "string"] },
                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                    ]
                                },
                                { "$ne": ["$Sintomas determinantes de PC", []] }
                            ]
                        },
                        "then": 1,
                        "else": 0
                    }
                },
                "numero_pc_anterior": {
                    "$cond": {
                        "if": { "$ne": ["$Estado palma PC anterior", []] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "numero_pc_acumulado": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": ["$numero_pc_actual", 1] },
                                { "$eq": ["$numero_pc_anterior", 1] }
                            ]
                        },
                        "then": 1,
                        "else": 0
                    }
                },


                "numero_recuperadas": {
                    "$cond": {
                        "if": { "$eq": ["$Palma se da de alta", "Si"] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "numero_erradicadas": {
                    "$cond": {
                        "if": { "$eq": ["$erradicada", true] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol"
                },
                "numero_erradicadas": { "$first": "$numero_erradicadas" },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "count_erradicadas": { "$sum": "$numero_erradicadas" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "numero_erradicadas_lote": "$count_erradicadas"
                        }
                    ]
                }
            }
        },



        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol",
                    "numero_pc_acumulado": "$numero_pc_acumulado",
                    "numero_recuperadas": "$numero_recuperadas"

                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "count_pc_acumulado": { "$sum": "$_id.numero_pc_acumulado" },
                "count_recuperadas_acumulado": { "$sum": "$_id.numero_recuperadas" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "numero_pc_acumulado_lote": "$count_pc_acumulado",
                            "numero_recuperadas_acumulado_lote": "$count_recuperadas_acumulado"
                        }
                    ]
                }
            }
        },



        {
            "$unwind": {
                "path": "$Sintomas determinantes de PC",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado palma PC anterior",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado de Anillo rojo",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado Marchitez sorpresiva",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion basal humedad",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion basal seca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion alta de estipite",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Ubicacioin de pestalotiopsis",
                "preserveNullAndEmptyArrays": true
            }
        },





        {
            "$addFields": {
                "pc_actual_en_campo_x_lote": {
                    "$subtract": [
                        "$numero_pc_acumulado_lote",
                        { "$add": ["$numero_recuperadas_acumulado_lote", "$numero_erradicadas_lote"] }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "incidencia_acumulada": {
                    "$cond": {
                        "if": { "$and": [{ "$gt": ["$platas_x_lote", 0] }, { "$ne": [{ "$type": "$platas_x_lote" }, "string"] }] },
                        "then": {
                            "$multiply": [
                                { "$divide": ["$numero_pc_acumulado_lote", "$platas_x_lote"] },
                                100
                            ]
                        },
                        "else": 0
                    }
                },

                "incidencia_real": {
                    "$cond": {
                        "if": { "$and": [{ "$gt": ["$platas_x_lote", 0] }, { "$ne": [{ "$type": "$platas_x_lote" }, "string"] }] },
                        "then": {
                            "$multiply": [
                                { "$divide": ["$pc_actual_en_campo_x_lote", "$platas_x_lote"] },
                                100
                            ]
                        },
                        "else": 0
                    }
                }
            }
        },





        {
            "$project": {
                "_id": 1,
                "supervisor": 1,
                "rgDate": 1,
                "uDate": 1,
                "finca": 1,
                "bloque": 1,
                "lote": 1,
                "linea": 1,
                "arbol": 1,
                "Enfermedad": 1,

                "Palmas enfermas nuevas PC Clasica": {
                    "$ifNull": ["$Enfermedad_Pudricion de cogollo", ""]
                },

                "Estado palmas enfermas con PC anteriormente Clorotica": {
                    "$ifNull": ["$Estado palma PC anterior", ""]
                },
                "Sintomas determinantes de PC": { "$ifNull": ["$Sintomas determinantes de PC", ""] },
                "Numero total de emisiones": 1,
                "Numero emisiones sanas": 1,
                "Numero total emisiones Enfermas": 1,
                "N hojas sanas y tamaño normal": 1,
                "Palma continua en observacion con tratamiento": 1,
                "Palma se da de alta": 1,


                "Anillo Rojo AR": { "$ifNull": ["$Estado de Anillo rojo", ""] },
                "Marchitez Sorpresiva MS": { "$ifNull": ["$Estado Marchitez sorpresiva", ""] },
                "Pudrición Basal Húmeda PBH": { "$ifNull": ["$Estado pudricion basal humedad", ""] },
                "Pudrición Basal Seca PBS": { "$ifNull": ["$Estado pudricion basal seca", ""] },
                "Pudrición Alta de Estípite PAE": { "$ifNull": ["$Estado pudricion alta de estipite", ""] },
                "Gonoderma": { "$ifNull": ["$Ubicacion Ganoderma", ""] },
                "Pestalotiopsis": { "$ifNull": ["$Ubicacioin de pestalotiopsis", ""] },

                "N plantas por lote": "$platas_x_lote",
                "N pc acumulado por lote": "$numero_pc_acumulado_lote",
                "N repueradas acumulado por lote": "$numero_recuperadas_acumulado_lote",

                "N erradicadas censadas por lote": "$numero_erradicadas_lote",
                "PC Actual en campo por lote": "$pc_actual_en_campo_x_lote",

                "Incidencia acumulada": { "$divide": [{ "$floor": { "$multiply": ["$incidencia_acumulada", 100] } }, 100] },
                "Incidencia real": { "$divide": [{ "$floor": { "$multiply": ["$incidencia_real", 100] } }, 100] }
            }
        }
    ]
)