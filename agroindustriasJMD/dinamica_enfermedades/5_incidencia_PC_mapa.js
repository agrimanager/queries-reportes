db.form_dinamicadeenfermedades.aggregate(
    [
            //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-12-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5d13d8c6091c0677116d6fe5"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // , {
                        //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        // }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            "$addFields": { "Cartography": "$Palma" }
        },
        { "$unwind": "$Cartography.features" },
        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },



        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "erradicada": { "$ifNull": ["$arbol.properties.custom.Erradicada.value", false] } } },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0,
                "uid": 0,
                "capture": 0
            }
        },


        // {
        //     "$group": {
        //         "_id": null,
        //         "data": { "$push": "$$ROOT" }
        //     }
        // },
        // { "$unwind": "$data" },
        // {
        //     "$replaceRoot": {
        //         "newRoot": "$data"
        //     }
        // },













        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },



        {
            "$addFields": {
                "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } }
            }
        },

        {
            "$match": {
                "$expr": {
                    "$or": [
                        {
                            "$and": [
                                {
                                    "$eq": [
                                        { "$type": "$Enfermedad_Pudricion de cogollo" },
                                        "string"
                                    ]
                                },
                                { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                            ]
                        },
                        {
                            "$and": [
                                {
                                    "$eq": [
                                        { "$type": "$Sintomas determinantes de PC" },
                                        "array"
                                    ]
                                },
                                { "$ne": ["$Sintomas determinantes de PC", []] }
                            ]
                        },
                        {
                            "$and": [
                                { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                { "$ne": ["$Sintomas determinantes de PCHC", []] }
                            ]
                        },

                        { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                    ]
                }
            }
        },

        {
            "$project": {
                "_id": 0,
                "finca_obj_id": 1,
                "lote_obj_id": 1,
                "Palma": 1,
                "type": "nuevo"
            }
        },





        {
            "$group": {
                "_id": {
                    "finca": "$finca_obj_id",
                    "lote": "$lote_obj_id"
                },
                "data": { "$push": "$$ROOT" }
            }
        },



        {
            "$lookup": {
                "from": "form_dinamicadeenfermedades",
                "let": {
                    "finca_id": "$_id.finca",
                    "lote_id": "$_id.lote"
                },
                "pipeline": [
                    { "$unwind": "$Palma.features" },
                    {
                        "$addFields": {
                            "split_path_padres": {
                                "$split": [
                                    { "$trim": { "input": "$Palma.path", "chars": "," } },
                                    ","
                                ]
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                            "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } },
                            "arbol_obj_id": { "$toObjectId": "$Palma.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$finca_obj_id", "$$finca_id"] },
                                    { "$eq": ["$lote_obj_id", "$$lote_id"] }
                                ]

                            }
                        }
                    },




                    {
                        "$facet": {
                            "pc_acumulado": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$or": [
                                                {
                                                    "$and": [
                                                        {
                                                            "$eq": [
                                                                { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                                "string"
                                                            ]
                                                        },
                                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                                    ]
                                                },
                                                {
                                                    "$and": [
                                                        {
                                                            "$eq": [
                                                                { "$type": "$Sintomas determinantes de PC" },
                                                                "array"
                                                            ]
                                                        },
                                                        { "$ne": ["$Sintomas determinantes de PC", []] }
                                                    ]
                                                },
                                                {
                                                    "$and": [
                                                        { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                                        { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                                    ]
                                                },

                                                { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                                            ]
                                        }
                                    }
                                },

                                {
                                    "$group": {
                                        "_id": "$Palma"
                                    }
                                },

                                {
                                    "$project": {
                                        "_id": 0,
                                        "Palma": "$_id",
                                        "type": "acumulado"
                                    }
                                }
                            ],



                            "recuperadas_acumulado": [
                                {
                                    "$addFields": {

                                        "esta_recuperada": {
                                            "$cond": {
                                                "if": { "$eq": ["$Palma se da de alta", "Si"] },
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    "$match": {
                                        "$expr": {
                                            "$eq": ["$esta_recuperada", true]
                                        }
                                    }
                                },
                                {
                                    "$group": {
                                        "_id": "$Palma"
                                    }
                                },

                                {
                                    "$project": {
                                        "_id": 0,
                                        "Palma": "$_id",
                                        "type": "recuperada"
                                    }
                                }
                            ]
                        }
                    }
                ],
                "as": "data_enf"
            }
        },



        {
            "$addFields": {
                "data_enf": {
                    "$arrayElemAt": ["$data_enf.pc_acumulado", 0]
                },
                "data_recuperada": {
                    "$arrayElemAt": ["$data_enf.recuperadas_acumulado", 0]
                }
            }
        },

        {
            "$addFields": {
                "datos": {
                    "$concatArrays": [
                        "$data",
                        "$data_enf",
                        "$data_recuperada"]
                }
            }
        },
        { "$unwind": "$datos" },

        {
            "$replaceRoot": {
                "newRoot": "$datos"
            }
        },


        {
            "$addFields": { "Cartography": "$Palma" }
        },
        { "$unwind": "$Cartography.features" },
        {
            "$addFields": { "elemnq": { "$toObjectId": "$Cartography.features._id" } }
        },





        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },


        {
            "$addFields": {
                "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } }
            }
        },



        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "erradicada": { "$ifNull": ["$arbol.properties.custom.Erradicada por PC.value", false] } } },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


        {
            "$project": {
                "finca_obj_id": 0,
                "lote_obj_id": 0,
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "bloque_id": 0
            }
        },




        {
            "$group": {
                "_id": {
                    "elemnq": "$elemnq",
                    "lote": "$lote",
                    "arbol": "$arbol",
                    "erradicada": "$erradicada"
                },
                "type": { "$addToSet": "$type" },
                "geometry": { "$first": "$Cartography.features.geometry" }
            }
        },

        {
            "$addFields": {
                "recuperada": {
                    "$filter": {
                        "input": "$type",
                        "as": "item",
                        "cond": { "$eq": ["$$item", "recuperda"] }
                    }
                }
            }
        },
        {
            "$addFields": {
                "recuperada": {
                    "$cond": {
                        "if": { "$gt": [{ "$size": "$recuperada" }, 0] },
                        "then": true,
                        "else": false
                    }
                }
            }
        },

        {
            "$addFields": {
                "type": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$eq": ["$_id.erradicada", false] },
                                { "$eq": ["$recuperada", false] }
                            ]
                        },
                        "then": {
                            "$concatArrays": [
                                "$type",
                                ["actual"]
                            ]
                        },
                        "else": "$type"
                    }
                }
            }
        },



        { "$unwind": "$type" },



        {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": ["$type", "nuevo"] },
                                "then": "#ffc0cb"
                            },
                            {
                                "case": { "$eq": ["$type", "acumulado"] },
                                "then": "#ff0000"
                            },
                            {
                                "case": { "$eq": ["$type", "recuperada"] },
                                "then": "#87ceeb"
                            },
                            {
                                "case": { "$eq": ["$type", "actual"] },
                                "then": "#691a7a"
                            }
                        ],

                        "default": null
                    }
                }
            }
        },

        { "$match": { "color": { "$ne": null } } },





        {
            "$project": {
                "_id": "$_id.elemnq",
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "lote": "$_id.lote",
                    "arbol": "$_id.arbol",
                    "caso": "$type",
                    "color": "$color"
                },
                "geometry": "$geometry"
            }
        }


    ]
)
