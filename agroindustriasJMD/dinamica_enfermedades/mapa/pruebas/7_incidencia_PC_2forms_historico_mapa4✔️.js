[


    {
        "$addFields": {
            "tiene_pc": {
                "$cond": {
                    "if": {
                        "$or": [
                            {
                                "$and": [
                                    {
                                        "$eq": [
                                            { "$type": "$Enfermedad_Pudricion de cogollo" },
                                            "string"
                                        ]
                                    },
                                    { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                ]
                            },
                            {
                                "$and": [
                                    {
                                        "$eq": [
                                            { "$type": "$Sintomas determinantes de PC" },
                                            "array"
                                        ]
                                    },
                                    { "$ne": ["$Sintomas determinantes de PC", []] }
                                ]
                            },
                            {
                                "$and": [
                                    { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                    { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                ]
                            },

                            { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                        ]
                    },
                    "then": true,
                    "else": false
                }
            }
        }
    },

    { "$match": { "tiene_pc": true } },

    {
        "$addFields": {
            "esta_recuperada": {
                "$cond": {
                    "if": { "$eq": ["$Palma se da de alta", "Si"] },
                    "then": 1,
                    "else": 0
                }
            }
        }
    },

    {
        "$addFields": {
            "variable_cartografia": "$Palma"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },



    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [
                                      { "$size":{"$ifNull":[ "$split_path_oid",[]]} }
                                      , { "$size": {"$ifNull":[ "$objetos_del_cultivo" ,[]]}}]
                                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },


    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
    { "$addFields": { "cartography_geometry": { "$ifNull": ["$arbol.geometry", {}] } } },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


    { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },
    { "$addFields": { "formulario": "dinamica_de_enfermedades" } },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Palma": 0,
            "Point": 0,
            "bloque_id": 0,
            "Formula": 0,
            "uid": 0,
            "capture": 0
        }
    },
    {
        "$project": {
            "_id": 0
        }
    }

    , {
        "$group": {
            "_id": "$lote",
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    {
        "$replaceRoot": {
            "newRoot": "$data"
        }
    }


    ,{
        "$group": {
            "_id": {
                "point_farm": "$point_farm",

                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "data_form1": { "$push": "$$ROOT" }
        }
    },

    {
        "$lookup": {
            "from": "form_formatodeseguimientopalmasconpchc",

            "let": {
                "point_farm": "$_id.point_farm",

                "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                "filtro_fecha_fin": "$_id.filtro_fecha_fin"
            },
            "as": "data_form2",
            "pipeline": [


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": ["$Point.farm", "$$point_farm"]
                                },
                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "America/Bogota" } } }
                                    ]
                                }
                            ]
                        }
                    }
                },


                { "$match": { "SINTOMA": { "$ne": "" } } },

                {
                    "$addFields": {
                        "variable_cartografia": "$Cartografia"
                    }
                },
                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                    }
                },
                {
                    "$addFields": {
                        "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                    }
                },
                {
                    "$addFields": {
                        "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                    }
                },
                {
                    "$addFields": {
                        "split_path_oid": {
                            "$concatArrays": [
                                "$split_path_padres_oid",
                                "$variable_cartografia_oid"
                            ]
                        }
                    }
                },

                {
                    "$lookup": {
                        "from": "cartography",
                        "localField": "split_path_oid",
                        "foreignField": "_id",
                        "as": "objetos_del_cultivo"
                    }
                },

                {
                    "$addFields": {
                        "tiene_variable_cartografia": {
                            "$cond": {
                                "if": { "$eq": [
                                      { "$size":{"$ifNull":[ "$split_path_oid",[]]} }
                                      , { "$size": {"$ifNull":[ "$objetos_del_cultivo" ,[]]}}]
                                    },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                },

                {
                    "$addFields": {
                        "objetos_del_cultivo": {
                            "$cond": {
                                "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                "then": "$objetos_del_cultivo",
                                "else": {
                                    "$concatArrays": [
                                        "$objetos_del_cultivo",
                                        ["$variable_cartografia.features"]
                                    ]
                                }
                            }
                        }
                    }
                },



                {
                    "$addFields": {
                        "finca": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$finca",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "finca._id",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },

                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                {
                    "$addFields": {
                        "bloque": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$bloque",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "lote": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$lote",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                {
                    "$addFields": {
                        "linea": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$linea",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                {
                    "$addFields": {
                        "arbol": {
                            "$filter": {
                                "input": "$objetos_del_cultivo",
                                "as": "item_cartografia",
                                "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                            }
                        }
                    }
                },
                {
                    "$unwind": {
                        "path": "$arbol",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
                { "$addFields": { "cartography_geometry": { "$ifNull": ["$arbol.geometry", {}] } } },

                { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }





                , { "$addFields": { "esta_recuperada": 0 } }

                ,{ "$addFields": { "formulario": "formato_de_seguimiento_palmas_con_pchc" } }
                ,{ "$addFields": { "idform": "61f2e81dc39b29501e8361a9" } }


                ,{
                    "$project": {
                        "variable_cartografia": 0,
                        "split_path_padres": 0,
                        "split_path_padres_oid": 0,
                        "variable_cartografia_oid": 0,
                        "split_path_oid": 0,
                        "objetos_del_cultivo": 0,
                        "tiene_variable_cartografia": 0,
                        "Palma": 0,
                        "Point": 0,
                        "bloque_id": 0,
                        "Formula": 0,
                        "uid": 0,
                        "capture": 0
                    }
                },
                {
                    "$project": {
                        "_id": 0
                    }
                }

            ]

        }
    },


    {
        "$project": {

            "data_final": {
                "$concatArrays": [
                    "$data_form1"
                    , "$data_form2"
                ]
            }

        }
    }

    , { "$unwind": "$data_final" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_final"
                    , {}
                ]
            }
        }
    }


    ,{
        "$group": {
            "_id": "$lote",
            "data": { "$push": "$$ROOT" }
        }
    },
    { "$unwind": "$data" },
    {
        "$replaceRoot": {
            "newRoot": "$data"
        }
    }


    , {
        "$addFields": {
            "num_anio": { "$year": { "date": "$rgDate" } },
            "num_mes": { "$month": { "date": "$rgDate" } }
        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }
        }
    }


    , {
        "$addFields": {
            "idform": "$idform"
            , "cartography_id": "$cartography_id"
            , "cartography_geometry": "$cartography_geometry"

            , "color": "#39AE37"
        }
    }



    , {
        "$project": {
            "_id": "$cartography_id",
            "idform": "$idform",
            "geometry": { "$ifNull": ["$cartography_geometry", {}] },

            "type": "Feature",

            "properties": {

                "Año": { "$toString": "$num_anio" },
                "Mes": "$Mes_Txt",

                "Formulario": "$formulario",

                "Lote": "$lote",
                "Palma": "$arbol",

                "color": "$color"

            }

        }
    }


]
