db.form_dinamicadeenfermedades.aggregate(
    [

        //=======================PROCESOS INICIALES

        {
            "$match": {
                "$and": [
                    {
                        "rgDate": {
                            "$gte": ISODate("2021-10-10T22:21:57.000Z")
                        }
                    },
                    {
                        "rgDate": {
                            "$lte": ISODate("2022-02-17T22:21:57.000Z")
                        }
                    }
                ]
            }
        },

        {
            "$unwind": {
                "path": "$Sintomas determinantes de PCHC",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado palma PC anterior",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado de Anillo rojo",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado Marchitez sorpresiva",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion basal humedad",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion basal seca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado pudricion alta de estipite",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Ubicacioin de pestalotiopsis",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Colapso Cogollo y Hojas",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Cogollo y Hoja Partida",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Palma con PC para Erradicar",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$match": {
                "Point.farm": "5d13d8c6091c0677116d6fe5"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "uid",
                "foreignField": "_id",
                "as": "user"
            }
        },
        {
            "$addFields": {
                "user": {
                    "$arrayElemAt": [
                        "$user.timezone",
                        0
                    ]
                },
                "today": {
                    "$toDate": "2022-02-17T22:23:06.290Z"
                },
                "idform": "5f972c9d92240c46d530ca4f",
                // "Busqueda inicio": "2000-02-10T22:21:57.000Z",
                // "Busqueda fin": "2022-02-17T22:21:57.000Z",
                "Busqueda inicio": ISODate("2021-10-10T22:21:57.000Z"),
                "Busqueda fin": ISODate("2022-02-17T22:21:57.000Z"),
                "FincaID": "5d13d8c6091c0677116d6fe5"
            }
        },
        {
            "$addFields": {
                "Cartography": "$Palma.features",
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Pudricion de cogollo"
                                    ]
                                },
                                "then": "#FFFB02"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Anillo rojo"
                                    ]
                                },
                                "then": "#882424"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Marchitez sorpresiva"
                                    ]
                                },
                                "then": "#77FF00"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Pudricion basal H"
                                    ]
                                },
                                "then": "#8000FF"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Pudricion basal S"
                                    ]
                                },
                                "then": "#8F3813"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Pudricion alta Estipite"
                                    ]
                                },
                                "then": "#220B63"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Ganoderma"
                                    ]
                                },
                                "then": "#FF0095"
                            },
                            {
                                "case": {
                                    "$eq": [
                                        "$Enfermedad",
                                        "Pestalotiopsis"
                                    ]
                                },
                                "then": "#00FF95"
                            }
                        ],
                        "default": "#cccccc"
                    }
                }
            }
        },

        //   //----------------------------------------------------------------
        //   //---VARIABLES INYECTADAS
        //   {
        //       $addFields: {
        //           //"Busqueda inicio": ISODate("2018-12-01T06:00:00.000-05:00"),
        //           "Busqueda inicio": ISODate("2021-10-10T06:00:00.000-05:00"),
        //           "Busqueda fin": new Date,
        //           "today": new Date,
        //           "FincaID": ObjectId("5d13d8c6091c0677116d6fe5"),
        //           "idform": "123"
        //           //user,FincaNombre
        //           //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
        //           //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
        //       }
        //   },
        //   //----FILTRO FECHAS Y FINCA
        //   {
        //       "$match": {
        //           "$expr": {
        //               "$and": [

        //                   {
        //                       "$gte": [
        //                           { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                           { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
        //                       ]
        //                   },
        //                   {
        //                       "$lte": [
        //                           { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
        //                           { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
        //                       ]
        //                   }
        //                   , {
        //                       "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
        //                   }
        //               ]
        //           }
        //       }
        //   },
        //   //----------------------------------------------------------------
        //   //....query reporte

        // //----old
        // {
        //     "$addFields": {
        //         "tiene_pc": {
        //             "$cond": {
        //                 "if": {
        //                     "$or": [
        //                         {
        //                             "$and": [
        //                                 {
        //                                     "$eq": [
        //                                         { "$type": "$Enfermedad_Pudricion de cogollo" },
        //                                         "string"
        //                                     ]
        //                                 },
        //                                 { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
        //                             ]
        //                         },
        //                         {
        //                             "$and": [
        //                                 {
        //                                     "$eq": [
        //                                         { "$type": "$Sintomas determinantes de PC" },
        //                                         "array"
        //                                     ]
        //                                 },
        //                                 { "$ne": ["$Sintomas determinantes de PC", []] }
        //                             ]
        //                         },
        //                         {
        //                             "$and": [
        //                                 { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
        //                                 { "$ne": ["$Sintomas determinantes de PCHC", []] }
        //                             ]
        //                         },

        //                         { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
        //                     ]
        //                 },
        //                 "then": true,
        //                 "else": false
        //             }
        //         }
        //     }
        // },

        //----new


        //---agrupacion despues de unwind de campos multiples
        {
            "$group": {
                "_id": "$_id",
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$addFields": {
                "data": {"$arrayElemAt": ["$data", 0]}
            }
        },
        {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        },



        //---condiciones despues de unwind de campos multiples
        {
            "$addFields": {
                "tiene_pc": {
                    "$cond": {
                        "if": {
                            "$or": [

                                { "$eq": ["$Enfermedad", "Pudricion de cogollo"] },

                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Sintomas determinantes de PC" },
                                                "array"
                                            ]
                                        },
                                        { "$ne": ["$Sintomas determinantes de PC", []] }
                                    ]
                                },



                                // {
                                //     "$and": [
                                //         { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                //         { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                //     ]
                                // },

                                //----condicion nueva
                                {
                                    "$and": [
                                        { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "string"] },
                                        { "$ne": ["$Sintomas determinantes de PCHC", ""] }
                                    ]
                                },

                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                "string"
                                            ]
                                        },
                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                    ]
                                },



                            ]
                        },
                        "then": true,
                        "else": false
                    }
                }
            }
        },

        { "$match": { "tiene_pc": true } },

        {
            "$addFields": {
                "esta_recuperada": {
                    "$cond": {
                        "if": { "$eq": ["$Palma se da de alta", "Si"] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },

        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },



        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },


        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
        { "$addFields": { "cartography_geometry": { "$ifNull": ["$arbol.geometry", {}] } } },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },


        { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },
        { "$addFields": { "formulario": "dinamica_de_enfermedades" } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0,
                "uid": 0,
                "capture": 0
            }
        },
        {
            "$project": {
                "_id": 0
            }
        }

        , {
            "$group": {
                "_id": "$lote",
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }


        , {
            "$group": {
                "_id": {
                    "point_farm": "$point_farm",

                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "data_form1": { "$push": "$$ROOT" }
            }
        },

        {
            "$lookup": {
                "from": "form_formatodeseguimientopalmasconpchc",

                "let": {
                    "point_farm": "$_id.point_farm",

                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data_form2",
                "pipeline": [


                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    { "$match": { "SINTOMA": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Cartografia"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },
                    { "$addFields": { "cartography_geometry": { "$ifNull": ["$arbol.geometry", {}] } } },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }





                    , { "$addFields": { "esta_recuperada": 0 } }

                    , { "$addFields": { "formulario": "formato_de_seguimiento_palmas_con_pchc" } }
                    , { "$addFields": { "idform": "61f2e81dc39b29501e8361a9" } }


                    , {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0,
                            "Palma": 0,
                            "Point": 0,
                            "bloque_id": 0,
                            "Formula": 0,
                            "uid": 0,
                            "capture": 0
                        }
                    },
                    {
                        "$project": {
                            "_id": 0
                        }
                    }

                ]

            }
        },


        {
            "$project": {

                "data_final": {
                    "$concatArrays": [
                        "$data_form1"
                        , "$data_form2"
                    ]
                }

            }
        }

        , { "$unwind": "$data_final" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final"
                        , {}
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": "$lote",
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": "$data"
            }
        }


        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                // , "color": "#39AE37"
                , "color": {
                    "$cond": {
                        "if": { "$eq": ["$formulario", "dinamica_de_enfermedades"] },
                        "then": "#39AE37",
                        "else": "#10700e"
                    }
                }
            }
        }



        , {
            "$project": {
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                "properties": {

                    "Año": { "$toString": "$num_anio" },
                    "Mes": "$Mes_Txt",

                    "Formulario": "$formulario",

                    "Lote": "$lote",
                    "Palma": "$arbol",

                    "color": "$color"

                }

            }
        }


    ]


)
