db.form_dinamicadeenfermedades.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2022-01-01T06:08:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5d13d8c6091c0677116d6fe5"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte




        //------------------------PROCESOS
        //1) Obtener ACTUALES (FECHAS FILTRADAS)
        //2) Obtener ACUM_Historico (FECHAS ANTERIORES), (TODOS LOS LOTES)


        //Inicio================================================================
        //======================================================================
        //2) Obtener ACUM_Historico (FECHAS ANTERIORES)=========================
        //======================================================================


        //Inicio================================================================
        //======================================================================
        //PROCESO 1) Obtener ACTUALES (FECHAS FILTRADAS)========================
        //======================================================================



        //condicion
        {
            "$addFields": {
                "tiene_pc": {
                    "$cond": {
                        "if": {
                            "$or": [
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                "string"
                                            ]
                                        },
                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                    ]
                                },
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Sintomas determinantes de PC" },
                                                "array"
                                            ]
                                        },
                                        { "$ne": ["$Sintomas determinantes de PC", []] }
                                    ]
                                },
                                {
                                    "$and": [
                                        { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                        { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                    ]
                                },

                                { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }
                }
            }
        },

        { "$match": { "tiene_pc": true } },

        {
            "$addFields": {
                "esta_recuperada": {
                    "$cond": {
                        "if": { "$eq": ["$Palma se da de alta", "Si"] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },


        //cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },


        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },


        //mostrar minimas variables
        {
            "$project": {
                "arbol": "$arbol",
                "esta_recuperada": "$esta_recuperada",
                "lote": "$lote",
                "platas_x_lote": "$platas_x_lote",
                "rgDate": "$rgDate",
                "formulario": "dinamica_de_enfermedades",
                "point_farm": "$point_farm",

                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            }
        },
        {
            "$project": {
                "_id": 0
            }
        },

        //agrupar data
        {
            "$group": {
                "_id": {
                    "point_farm": "$point_farm",
                    "filtro_fecha_inicio": "$filtro_fecha_inicio",
                    "filtro_fecha_fin": "$filtro_fecha_fin"
                },
                "data_form1": { "$push": "$$ROOT" }
            }
        },

        {
            "$lookup": {
                "from": "form_formatodeseguimientopalmasconpchc",

                "let": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data_form2",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    { "$match": { "SINTOMA": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Cartografia"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }

                    , { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } }



                    , { "$addFields": { "esta_recuperada": 0 } }

                    , {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "formato_de_seguimiento_palmas_con_pchc",

                            "point_farm": "$$point_farm",

                            "filtro_fecha_inicio": "$$filtro_fecha_inicio",
                            "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                ]

            }
        },


        {
            "$project": {

                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data_form1"
                        , "$data_form2"
                    ]
                }

                // //----variables bases
                // , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
                // , "Busqueda fin": "$Busqueda fin"               //--filtro_fecha2
                // , "user_timezone": "$user_timezone",         //--filtro_timezone
            }
        }

        , { "$unwind": "$data_final" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final"
                        , {}
                    ]
                }
            }
        }

        , {
            "$project": {
                "arbol": "$arbol",
                "esta_recuperada": "$esta_recuperada",
                "lote": "$lote",
                "platas_x_lote": "$platas_x_lote",
                "rgDate": "$rgDate",
                "formulario": "$formulario"

                , "point_farm": "$point_farm"

                , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                , "filtro_fecha_fin": "$filtro_fecha_fin"
            }
        }


        //Fin===================================================================
        //======================================================================



        //-----AGRUPACION
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"

                    , "formulario": "$formulario"

                    , "point_farm": "$point_farm"
                    , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                    , "filtro_fecha_fin": "$filtro_fecha_fin"
                }
                , "platas_x_lote": { "$min": "$platas_x_lote" }
                , "num_pc_nuevo": { "$sum": 1 }
                , "num_pc_acumulado": { "$sum": 0 }
                , "num_recuperadas": { "$sum": "$esta_recuperada" }

                // , "data": { "$push": "$$ROOT" }
            }
        }


        //Inicio================================================================
        //======================================================================
        //2) Obtener ACUM_Historico (FECHAS ANTERIORES)=========================
        //======================================================================

        , {
            "$group": {
                "_id": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "data1": { "$push": "$$ROOT" }
            }
        }


        , {
            "$lookup": {
                "from": "form_dinamicadeenfermedades",

                "let": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data2_form1",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$lt": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "tiene_pc": {
                                "$cond": {
                                    "if": {
                                        "$or": [
                                            {
                                                "$and": [
                                                    {
                                                        "$eq": [
                                                            { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                            "string"
                                                        ]
                                                    },
                                                    { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                                ]
                                            },
                                            {
                                                "$and": [
                                                    {
                                                        "$eq": [
                                                            { "$type": "$Sintomas determinantes de PC" },
                                                            "array"
                                                        ]
                                                    },
                                                    { "$ne": ["$Sintomas determinantes de PC", []] }
                                                ]
                                            },
                                            {
                                                "$and": [
                                                    { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                                    { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                                ]
                                            },

                                            { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                                        ]
                                    },
                                    "then": true,
                                    "else": false
                                }
                            }
                        }
                    },

                    { "$match": { "tiene_pc": true } },

                    {
                        "$addFields": {
                            "esta_recuperada": {
                                "$cond": {
                                    "if": { "$eq": ["$Palma se da de alta", "Si"] },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Palma"
                        }
                    },

                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },


                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },

                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

                    { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },


                    //mostrar minimas variables
                    {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "dinamica_de_enfermedades",

                            "point_farm": "$$point_farm",

                            "filtro_fecha_inicio": "$$filtro_fecha_inicio",
                            "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    },
                    {
                        "$project": {
                            "_id": 0
                        }
                    }


                    //-----AGRUPACION
                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote"

                                , "formulario": "$formulario"

                                , "point_farm": "$point_farm"
                                , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                                , "filtro_fecha_fin": "$filtro_fecha_fin"
                            }
                            , "platas_x_lote": { "$min": "$platas_x_lote" }
                            , "num_pc_nuevo": { "$sum": 0 }
                            , "num_pc_acumulado": { "$sum": 1 }
                            , "num_recuperadas": { "$sum": "$esta_recuperada" }

                            // , "data": { "$push": "$$ROOT" }
                        }
                    }


                ]

            }
        }


        , {
            "$lookup": {
                "from": "form_formatodeseguimientopalmasconpchc",

                "let": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data2_form2",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$lt": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    { "$match": { "SINTOMA": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Cartografia"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }

                    , { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } }



                    , { "$addFields": { "esta_recuperada": 0 } }

                    , {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "formato_de_seguimiento_palmas_con_pchc",

                            "point_farm": "$$point_farm",

                            "filtro_fecha_inicio": "$$filtro_fecha_inicio",
                            "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    //-----AGRUPACION
                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote"

                                , "formulario": "$formulario"

                                , "point_farm": "$point_farm"
                                , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                                , "filtro_fecha_fin": "$filtro_fecha_fin"
                            }
                            , "platas_x_lote": { "$min": "$platas_x_lote" }
                            , "num_pc_nuevo": { "$sum": 0 }
                            , "num_pc_acumulado": { "$sum": 1 }
                            , "num_recuperadas": { "$sum": "$esta_recuperada" }

                            // , "data": { "$push": "$$ROOT" }
                        }
                    }


                ]

            }
        }



        //---unir data2 f1_y_f2
        , {
            "$addFields": {
                "data2": {
                    "$concatArrays": [
                        "$data2_form1"
                        , "$data2_form2"
                    ]
                }
            }
        }

        , {
            "$project": {
                "data2_form1": 0
                , "data2_form2": 0
            }
        }


        //---unir data1 y data2
        , {
            "$addFields": {
                "data_final": {
                    "$concatArrays": [
                        "$data1"
                        , "$data2"
                    ]
                }
            }
        }

        , {
            "$project": {
                "data1": 0
                , "data2": 0
            }
        }


        , { "$unwind": "$data_final" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final"
                        , {}
                    ]
                }
            }
        }



        //Fin===================================================================
        //======================================================================


        //aducuar datos finales
        , {
            "$replaceRoot": {
                "newRoot": {
                    "lote":"$_id.lote"

                    ,"formulario":"$_id.formulario"

                    ,"platas_x_lote":"$platas_x_lote"
                    ,"num_pc_nuevo":"$num_pc_nuevo"
                    ,"num_pc_acumulado":"$num_pc_acumulado"
                    ,"num_recuperadas":"$num_recuperadas"
                }
            }
        }



        //erradicada por pc???
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                },
                "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "lote": "$_id.lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees",
                            "properties.custom.Erradicada por PC.value": true
                        }
                    },


                    //=====CARTOGRAFIA
                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    //--paso2 (cartografia-cruzar informacion)
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    // //--paso3 (cartografia-obtener informacion)

                    //--finca
                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    //--bloque
                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    //--lote
                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    //--linea
                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    //--arbol
                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0
                        }
                    }




                    ,{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$lote", "$$lote"] }
                                ]
                            }
                        }
                    },

                    { "$count": "count" }
                ],
                "as": "data_erradicadas"
            }
        },


        {
            "$addFields": {
                "erradicadas_pc_lote": {"$ifNull": [{ "$arrayElemAt": ["$data_erradicadas.count", 0] },0]}
            }
        },


        { "$unwind": "$data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "erradicadas_pc_lote": "$erradicadas_pc_lote"
                        }
                    ]
                }
            }
        }


    ]
    , { allowDiskUse: true }
)
