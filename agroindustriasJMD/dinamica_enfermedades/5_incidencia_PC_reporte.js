db.form_dinamicadeenfermedades.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2000-12-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5d13d8c6091c0677116d6fe5"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // , {
                        //     "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        // }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte

        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },



        {
            "$addFields": {
                "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } }
            }
        },



        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", "sin datos"] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Palma": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0,
                "uid": 0,
                "capture": 0
            }
        },

        {
            "$addFields": {
                "tiene_pc": {
                    "$cond": {
                        "if": {
                            "$or": [
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                "string"
                                            ]
                                        },
                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                    ]
                                },
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Sintomas determinantes de PC" },
                                                "array"
                                            ]
                                        },
                                        { "$ne": ["$Sintomas determinantes de PC", []] }
                                    ]
                                },
                                {
                                    "$and": [
                                        { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                        { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                    ]
                                },

                                { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }
                }
            }
        },

        { "$match": { "tiene_pc": true } },






        {
            "$group": {
                "_id": {
                    "arbol": "$arbol",
                    "finca": "$finca_obj_id",
                    "lote": "$lote_obj_id"
                },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "lote": "$_id.lote"
                },
                "count_pc_nuevo": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },




        {
            "$lookup": {
                "from": "form_dinamicadeenfermedades",

                "let": {
                    "finca_id": "$_id.finca",
                    "lote_id": "$_id.lote"
                },
                "pipeline": [

                    { "$unwind": "$Palma.features" },
                    {
                        "$addFields": {
                            "split_path_padres": {
                                "$split": [
                                    { "$trim": { "input": "$Palma.path", "chars": "," } },
                                    ","
                                ]
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                            "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } },
                            "arbol_obj_id": { "$toObjectId": "$Palma.features._id" }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$finca_obj_id", "$$finca_id"] },
                                    { "$eq": ["$lote_obj_id", "$$lote_id"] }
                                ]

                            }
                        }
                    },



                    {
                        "$facet": {
                            "pc_acumulado": [
                                {
                                    "$addFields": {
                                        "tiene_pc": {
                                            "$cond": {
                                                "if": {
                                                    "$or": [
                                                        {
                                                            "$and": [
                                                                {
                                                                    "$eq": [
                                                                        { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                                        "string"
                                                                    ]
                                                                },
                                                                { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                                            ]
                                                        },
                                                        {
                                                            "$and": [
                                                                {
                                                                    "$eq": [
                                                                        { "$type": "$Sintomas determinantes de PC" },
                                                                        "array"
                                                                    ]
                                                                },
                                                                { "$ne": ["$Sintomas determinantes de PC", []] }
                                                            ]
                                                        },
                                                        {
                                                            "$and": [
                                                                { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                                                { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                                            ]
                                                        },

                                                        { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                                                    ]
                                                },
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    "$match": {
                                        "$expr": {
                                            "$eq": ["$tiene_pc", true]
                                        }
                                    }
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            "finca": "$finca_obj_id",
                                            "lote": "$lote_obj_id",
                                            "arbol": "$arbol_obj_id"
                                        }
                                    }
                                },
                                { "$count": "count" }
                            ],



                            "recuperadas_acumulado": [
                                {
                                    "$addFields": {

                                        "esta_recuperada": {
                                            "$cond": {
                                                "if": { "$eq": ["$Palma se da de alta", "Si"] },
                                                "then": true,
                                                "else": false
                                            }
                                        }
                                    }
                                },
                                {
                                    "$match": {
                                        "$expr": {
                                            "$eq": ["$esta_recuperada", true]
                                        }
                                    }
                                },
                                {
                                    "$group": {
                                        "_id": {
                                            "finca": "$finca_obj_id",
                                            "lote": "$lote_obj_id",
                                            "arbol": "$arbol_obj_id"
                                        }
                                    }
                                },
                                { "$count": "count" }
                            ]
                        }
                    }
                ],
                "as": "data_enf"
            }
        },

        { "$unwind": "$data_enf" },

        {
            "$addFields": {
                "pc_acumulado": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_enf.pc_acumulado.count", 0] },
                        0
                    ]
                },
                "recuperadas_acumulado": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_enf.recuperadas_acumulado.count", 0] },
                        0
                    ]
                }
            }
        },



        {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "finca_id": "$_id.finca",
                    "lote_id": "$_id.lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees",
                            "properties.custom.Erradicada por PC.value": true
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "finca_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 0] } },
                            "lote_obj_id": { "$toObjectId": { "$arrayElemAt": ["$split_path_padres", 2] } }
                        }
                    },

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$finca_obj_id", "$$finca_id"] },
                                    { "$eq": ["$lote_obj_id", "$$lote_id"] }
                                ]
                            }
                        }
                    },

                    { "$count": "count" }
                ],
                "as": "data_erra"
            }
        },

        {
            "$addFields": {
                "erradicadas_pc_acumulado": {
                    "$ifNull": [
                        { "$arrayElemAt": ["$data_erra.count", 0] },
                        0
                    ]
                }
            }
        },


        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "pc_nuevo_x_lote": "$count_pc_nuevo",
                            "pc_acumulado": "$pc_acumulado",
                            "recuperadas_acumulado": "$recuperadas_acumulado",
                            "erradicadas_pc_acumulado": "$erradicadas_pc_acumulado"
                        }
                    ]
                }
            }
        },








        {
            "$unwind": {
                "path": "$Sintomas determinantes de PC",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Sintomas determinantes de PCHC",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$unwind": {
                "path": "$Estado palma PC anterior",
                "preserveNullAndEmptyArrays": true
            }
        },






        {
            "$addFields": {
                "pc_actual_en_campo_x_lote": {
                    "$subtract": [
                        "$pc_acumulado",
                        { "$add": ["$recuperadas_acumulado", "$erradicadas_pc_acumulado"] }
                    ]
                }
            }
        },


        {
            "$addFields": {
                "incidencia_acumulada": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$gt": ["$platas_x_lote", 0] },
                                { "$ne": [{ "$type": "$platas_x_lote" }, "string"] }
                            ]
                        },
                        "then": {
                            "$multiply": [
                                { "$divide": ["$pc_acumulado", "$platas_x_lote"] },
                                100
                            ]
                        },
                        "else": 0
                    }
                },

                "incidencia_real": {
                    "$cond": {
                        "if": {
                            "$and": [
                                { "$gt": ["$platas_x_lote", 0] },
                                { "$ne": [{ "$type": "$platas_x_lote" }, "string"] }
                            ]
                        },
                        "then": {
                            "$multiply": [
                                { "$divide": ["$pc_actual_en_campo_x_lote", "$platas_x_lote"] },
                                100
                            ]
                        },
                        "else": 0
                    }
                }
            }
        },





        {
            "$project": {
                "_id": 1,
                "supervisor": 1,
                "rgDate": 1,
                "uDate": 1,
                "finca": 1,
                "bloque": 1,
                "lote": 1,
                "linea": 1,
                "arbol": 1,
                "Enfermedad": 1,

                "Palmas enfermas nuevas PC Clasica": {
                    "$ifNull": ["$Enfermedad_Pudricion de cogollo", ""]
                },

                "Estado palmas enfermas con PC anteriormente Clorotica": {
                    "$ifNull": ["$Estado palma PC anterior", ""]
                },
                "Sintomas determinantes de PC": { "$ifNull": ["$Sintomas determinantes de PC", "$Sintomas determinantes de PCHC"] },
                "Numero total de emisiones": 1,
                "Numero emisiones sanas": 1,
                "Numero total emisiones Enfermas": 1,
                "N hojas sanas y tamaño normal": 1,
                "Palma continua en observacion con tratamiento": 1,
                "Palma se da de alta": 1,



                "N plantas por lote": "$platas_x_lote",
                "N pc nuevo por lote": "$pc_nuevo_x_lote",
                "N pc acumulado por lote": "$pc_acumulado",
                "N repueradas acumulado por lote": "$recuperadas_acumulado",

                "N erradicadas acumulado por lote": "$erradicadas_pc_acumulado",
                "PC actual en campo por lote": "$pc_actual_en_campo_x_lote",

                "Incidencia acumulada": "$incidencia_acumulada",
                "Incidencia real": "$incidencia_real"
            }
        },



        {
            "$group": {
                "_id": "$lote",
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },
        { "$unwind": "$data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        { "count_id_lote": "$count" }
                    ]
                }
            }
        },

        {
            "$match": {
                "N plantas por lote": { "$ne": "sin datos" }
            }
        },


        {
            "$addFields": {
                "plantas_x_arbol_x_lote": {
                    "$divide": [
                        "$N plantas por lote",
                        "$count_id_lote"
                    ]
                },
                "pc_nuevo_x_arbol_x_lote": {
                    "$divide": [
                        "$N pc nuevo por lote",
                        "$count_id_lote"
                    ]
                },
                "pc_acumulado_x_arbol_x_lote": {
                    "$divide": [
                        "$N pc acumulado por lote",
                        "$count_id_lote"
                    ]
                },
                "pc_erradicadas_x_arbol_x_lote": {
                    "$divide": [
                        "$N erradicadas acumulado por lote",
                        "$count_id_lote"
                    ]
                },
                "pc_recuperadas_x_arbol_x_lote": {
                    "$divide": [
                        "$N repueradas acumulado por lote",
                        "$count_id_lote"
                    ]
                },
                "pc_actual_x_arbol_x_lote": {
                    "$divide": [
                        "$PC actual en campo por lote",
                        "$count_id_lote"
                    ]
                }
            }
        },


        {
            "$addFields": {
                "fecha": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%d-%m-%Y"
                    }
                },

                "hora": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%H:%M:%S"
                    }

                }
            }
        }
    ]
)
