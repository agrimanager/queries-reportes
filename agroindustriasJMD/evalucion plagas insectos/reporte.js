db.form_evaluaciondeinsectosplagas.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Arbol": 0
            }
        }

        //plantas_dif_censadas
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "lote": "$lote",
                    "arbol": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                },
                "plantas_dif_censadas_x_lote": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                        }
                    ]
                }
            }
        }

        //plantas_dif_censadas
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "lote": "$lote",
                    "arbol": "$arbol"

                    , "plaga": "$Insectos y Acaros Plagas"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"

                    , "plaga": "$_id.plaga"
                },
                "plantas_dif_censadas_x_lote_x_plaga": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"
                        }
                    ]
                }
            }
        }




        //plantas_dif_censadas
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "arbol": "$arbol"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                },
                "plantas_dif_censadas_x_finca": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_finca": "$plantas_dif_censadas_x_finca"
                        }
                    ]
                }
            }
        }



        //plantas_dif_censadas
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "arbol": "$arbol"

                    , "plaga": "$Insectos y Acaros Plagas"
                },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                    , "plaga": "$_id.plaga"
                },
                "plantas_dif_censadas_x_finca_x_plaga": { "$sum": 1 },
                "data": {
                    "$push": "$$ROOT"
                }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "plantas_dif_censadas_x_finca_x_plaga": "$plantas_dif_censadas_x_finca_x_plaga"
                        }
                    ]
                }
            }
        }



        //array_plagas
        , {
            "$addFields": {
                "array_estado_plaga": [

                    {
                        "estado": "Huevo"
                        , "valor": { "$toDouble": { "$ifNull": ["$Huevo", 0] } }
                    },
                    {
                        "estado": "Larva"
                        , "valor": { "$toDouble": { "$ifNull": ["$Larva", 0] } }
                    },
                    {
                        "estado": "Pupa"
                        , "valor": { "$toDouble": { "$ifNull": ["$Pupa", 0] } }
                    },
                    {
                        "estado": "Ninfa"
                        , "valor": { "$toDouble": { "$ifNull": ["$Ninfa", 0] } }
                    },
                    {
                        "estado": "Adulto"
                        , "valor": { "$toDouble": { "$ifNull": ["$Adulto", 0] } }
                    }

                ]
            }
        }

        , {
            "$addFields": {
                "array_estado_plaga": {
                    "$filter": {
                        "input": "$array_estado_plaga",
                        "as": "item",
                        "cond": { "$ne": ["$$item.valor", 0] }
                    }
                }
            }
        }


        , { "$unwind": "$array_estado_plaga" }



        , {
            "$project": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol"

                , "Insectos y Acaros Plagas": "$Insectos y Acaros Plagas"
                , "estado_plaga": "$array_estado_plaga.estado"
                , "estado_valor": "$array_estado_plaga.valor"

                , "supervisor": "$supervisor"
                , "fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                , "plantas_dif_censadas_x_lote": "$plantas_dif_censadas_x_lote"
                , "plantas_dif_censadas_x_lote_x_plaga": "$plantas_dif_censadas_x_lote_x_plaga"
                , "plantas_dif_censadas_x_finca": "$plantas_dif_censadas_x_finca"
                , "plantas_dif_censadas_x_finca_x_plaga": "$plantas_dif_censadas_x_finca_x_plaga"

            }
        }




    ]
)
