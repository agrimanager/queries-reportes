db.form_dinamicadeenfermedades.aggregate(

    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2022-02-01T06:08:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5d13d8c6091c0677116d6fe5"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        , {
                            "$eq": ["$Point.farm", { "$toString": "$FincaID" }]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte



        {
            "$addFields": {
                "tiene_pc": {
                    "$cond": {
                        "if": {
                            "$or": [
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                "string"
                                            ]
                                        },
                                        { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                    ]
                                },
                                {
                                    "$and": [
                                        {
                                            "$eq": [
                                                { "$type": "$Sintomas determinantes de PC" },
                                                "array"
                                            ]
                                        },
                                        { "$ne": ["$Sintomas determinantes de PC", []] }
                                    ]
                                },
                                {
                                    "$and": [
                                        { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                        { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                    ]
                                },

                                { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                            ]
                        },
                        "then": true,
                        "else": false
                    }
                }
            }
        },

        { "$match": { "tiene_pc": true } },

        {
            "$addFields": {
                "esta_recuperada": {
                    "$cond": {
                        "if": { "$eq": ["$Palma se da de alta", "Si"] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        },


        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },


        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },

        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

        { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },


        {
            "$project": {
                "arbol": "$arbol",
                "esta_recuperada": "$esta_recuperada",
                "lote": "$lote",
                "platas_x_lote": "$platas_x_lote",
                "rgDate": "$rgDate",
                "formulario": "dinamica_de_enfermedades",
                "point_farm": "$point_farm",

                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            }
        },
        {
            "$project": {
                "_id": 0
            }
        },

        // //test
        // {
        //     "$group": {
        //         "_id": {
        //             "arbol": "$arbol",
        //             "point_farm": "$point_farm",
        //             "filtro_fecha_inicio": "$filtro_fecha_inicio",
        //             "filtro_fecha_fin": "$filtro_fecha_fin"
        //         },
        //         // "data_form1": { "$push": "$$ROOT" }
        //     }
        // },


        //data1 = actuales

        {
            "$group": {
                "_id": {
                    "point_farm": "$point_farm",
                    "filtro_fecha_inicio": "$filtro_fecha_inicio",
                    "filtro_fecha_fin": "$filtro_fecha_fin"
                },
                "data_form1": { "$push": "$$ROOT" }
            }
        },

        {
            "$lookup": {
                "from": "form_formatodeseguimientopalmasconpchc",

                "let": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data_form2",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    { "$match": { "SINTOMA": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Cartografia"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }

                    , { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } }



                    , { "$addFields": { "esta_recuperada": 0 } }

                    , {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "formato_de_seguimiento_palmas_con_pchc",

                            "point_farm": "$$point_farm",

                            "filtro_fecha_inicio": "$$filtro_fecha_inicio",
                            "filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                ]

            }
        },


        {
            "$project": {

                "data_final": {
                    "$concatArrays": [
                        "$data_form1"
                        , "$data_form2"
                    ]
                }

            }
        }

        , { "$unwind": "$data_final" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final"
                        , {}
                    ]
                }
            }
        }

        , {
            "$project": {
                "arbol": "$arbol",
                "esta_recuperada": "$esta_recuperada",
                "lote": "$lote",
                "platas_x_lote": "$platas_x_lote",
                "rgDate": "$rgDate",
                "formulario": "$formulario"

                , "tipo": "actual"//new

                , "point_farm": "$point_farm"

                , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                , "filtro_fecha_fin": "$filtro_fecha_fin"
            }
        }



        //------OLD
        // , {
        //     "$group": {
        //         "_id": {
        //             "lote": "$lote"

        //             , "formulario": "$formulario"

        //             , "point_farm": "$point_farm"
        //             , "filtro_fecha_inicio": "$filtro_fecha_inicio"
        //             , "filtro_fecha_fin": "$filtro_fecha_fin"
        //         }
        //         , "platas_x_lote": { "$min": "$platas_x_lote" }
        //         , "num_pc_nuevo": { "$sum": 1 }
        //         , "num_pc_acumulado": { "$sum": 0 }
        //         , "num_recuperadas": { "$sum": "$esta_recuperada" }


        //     }
        // }

        //------NEW
        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "arbol": "$arbol"//new

                    , "formulario": "$formulario"

                    , "tipo": "$tipo"

                    , "point_farm": "$point_farm"
                    , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                    , "filtro_fecha_fin": "$filtro_fecha_fin"
                }
                , "platas_x_lote": { "$min": "$platas_x_lote" }
                //, "num_pc_nuevo": { "$sum": 1 }//old
                // , "num_pc_nuevo": { "$sum": 0 }
                // , "num_pc_acumulado": { "$sum": 0 }
                , "num_recuperadas": { "$sum": "$esta_recuperada" }


            }
        }


        , {
            "$group": {
                "_id": {
                    // "arbol": "$_id.arbol",//new

                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "data1": { "$push": "$$ROOT" }
            }
        }


        //data2 = anteriores


        , {
            "$lookup": {
                "from": "form_dinamicadeenfermedades",

                "let": {
                    // "arbol": "$_id.arbol",//new

                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data2_form1",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$lt": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "tiene_pc": {
                                "$cond": {
                                    "if": {
                                        "$or": [
                                            {
                                                "$and": [
                                                    {
                                                        "$eq": [
                                                            { "$type": "$Enfermedad_Pudricion de cogollo" },
                                                            "string"
                                                        ]
                                                    },
                                                    { "$ne": ["$Enfermedad_Pudricion de cogollo", ""] }
                                                ]
                                            },
                                            {
                                                "$and": [
                                                    {
                                                        "$eq": [
                                                            { "$type": "$Sintomas determinantes de PC" },
                                                            "array"
                                                        ]
                                                    },
                                                    { "$ne": ["$Sintomas determinantes de PC", []] }
                                                ]
                                            },
                                            {
                                                "$and": [
                                                    { "$eq": [{ "$type": "$Sintomas determinantes de PCHC" }, "array"] },
                                                    { "$ne": ["$Sintomas determinantes de PCHC", []] }
                                                ]
                                            },

                                            { "$eq": ["$Enfermedad", "Pudricion de cogollo"] }
                                        ]
                                    },
                                    "then": true,
                                    "else": false
                                }
                            }
                        }
                    },

                    { "$match": { "tiene_pc": true } },

                    {
                        "$addFields": {
                            "esta_recuperada": {
                                "$cond": {
                                    "if": { "$eq": ["$Palma se da de alta", "Si"] },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Palma"
                        }
                    },

                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },


                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },

                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },

                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },

                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },

                    { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } },


                    // //test
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$and": [
                    //                 {"$eq": ["$arbol", "$$arbol"]}
                    //             ]
                    //         }
                    //     }
                    // },




                    {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "dinamica_de_enfermedades"

                            , "tipo": "anterior"//new

                            // ,"point_farm": "$$point_farm"

                            // ,"filtro_fecha_inicio": "$$filtro_fecha_inicio"
                            // ,"filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    },
                    {
                        "$project": {
                            "_id": 0
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote"
                                , "arbol": "$arbol"//new

                                , "formulario": "$formulario"

                                , "tipo": "$tipo"//new

                                // , "point_farm": "$point_farm"
                                // , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                                // , "filtro_fecha_fin": "$filtro_fecha_fin"
                            }
                            , "platas_x_lote": { "$min": "$platas_x_lote" }
                            // , "num_pc_nuevo": { "$sum": 0 }
                            // , "num_pc_acumulado": { "$sum": 1 }//old
                            // , "num_pc_acumulado": { "$sum": 0 }
                            , "num_recuperadas": { "$sum": "$esta_recuperada" }

                        }
                    }


                ]

            }
        }


        , {
            "$lookup": {
                "from": "form_formatodeseguimientopalmasconpchc",

                "let": {
                    "point_farm": "$_id.point_farm",
                    "filtro_fecha_inicio": "$_id.filtro_fecha_inicio",
                    "filtro_fecha_fin": "$_id.filtro_fecha_fin"
                },
                "as": "data2_form2",
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$Point.farm", "$$point_farm"]
                                    },
                                    {
                                        "$lt": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate", "timezone": "America/Bogota" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio", "timezone": "America/Bogota" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    { "$match": { "SINTOMA": { "$ne": "" } } },

                    {
                        "$addFields": {
                            "variable_cartografia": "$Cartografia"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "platas_x_lote": { "$ifNull": ["$lote.properties.custom.num_arboles.value", 0] } } },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } }

                    , { "$addFields": { "point_farm": { "$ifNull": ["$Point.farm", null] } } }



                    , { "$addFields": { "esta_recuperada": 0 } }

                    , {
                        "$project": {
                            "arbol": "$arbol",
                            "esta_recuperada": "$esta_recuperada",
                            "lote": "$lote",
                            "platas_x_lote": "$platas_x_lote",
                            "rgDate": "$rgDate",
                            "formulario": "formato_de_seguimiento_palmas_con_pchc"

                            , "tipo": "anterior"//new

                            // ,"point_farm": "$$point_farm"

                            // ,"filtro_fecha_inicio": "$$filtro_fecha_inicio"
                            // ,"filtro_fecha_fin": "$$filtro_fecha_fin"
                        }
                    }

                    , {
                        "$project": {
                            "_id": 0
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$lote"
                                , "arbol": "$arbol"//new

                                , "formulario": "$formulario"

                                , "tipo": "$tipo"//new

                                // , "point_farm": "$point_farm"
                                // , "filtro_fecha_inicio": "$filtro_fecha_inicio"
                                // , "filtro_fecha_fin": "$filtro_fecha_fin"
                            }
                            , "platas_x_lote": { "$min": "$platas_x_lote" }
                            // , "num_pc_nuevo": { "$sum": 0 }
                            // , "num_pc_acumulado": { "$sum": 1 }//old
                            // , "num_pc_acumulado": { "$sum": 0 }
                            , "num_recuperadas": { "$sum": "$esta_recuperada" }

                        }
                    }


                ]

            }
        }



        , {
            "$addFields": {
                "data2": {
                    "$concatArrays": [
                        "$data2_form1"
                        , "$data2_form2"
                    ]
                }
            }
        }

        , {
            "$project": {
                "data2_form1": 0
                , "data2_form2": 0
            }
        }


        //----------------------------------------------------------
        //---NOTA: data1 (datos actuales) y data2 (datos anteriores)
        //----------------------------------------------------------

        , {
            "$addFields": {
                "data_final": {
                    "$concatArrays": [
                        "$data1"
                        , "$data2"
                    ]
                }
            }
        }

        , {
            "$project": {
                "data1": 0
                , "data2": 0
            }
        }


        , { "$unwind": "$data_final" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final"
                        , {}
                    ]
                }
            }
        }


        //========LOGICA DE NUEVOS Y ACUMULADOS



        , {
            "$replaceRoot": {
                "newRoot": {
                    "lote": "$_id.lote"

                    , "arbol": "$_id.arbol"//NEW

                    , "formulario": "$_id.formulario"

                    , "tipo": "$_id.tipo"//NEW

                    , "platas_x_lote": "$platas_x_lote"
                    // , "num_pc_nuevo": "$num_pc_nuevo"
                    // , "num_pc_acumulado": "$num_pc_acumulado"
                    , "num_recuperadas": "$num_recuperadas"
                }
            }
        }

        // //test
        // , {
        //     $match: {
        //         arbol: "1-3_95_B-58-4"
        //         // num_recuperadas:{$ne:0}
        //     }
        // }



        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "arbol": "$arbol"//NEW
                    // ,"tipo": "$tipo"//NEW
                },
                "data": { "$push": "$$ROOT" }

                , "platas_x_lote": { "$min": "$platas_x_lote" }
                , "num_recuperadas": { "$sum": "$num_recuperadas" }
            }
        }



        //-----es actual o anterior?
        , {
            "$addFields": {
                "data_actual": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.tipo", "actual"] }
                    }
                }
                , "data_anterior": {
                    "$filter": {
                        "input": "$data",
                        "as": "item",
                        "cond": { "$eq": ["$$item.tipo", "anterior"] }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "condicion_data": {
                    "$cond": {
                        "if": {
                            "$gt": [
                                { "$size": { "$ifNull": ["$data_anterior", []] } }
                                , 0
                            ]
                        },
                        "then": "anterior",
                        "else": "actual"
                    }
                }
            }
        }


        , {
            "$addFields": {
                "num_pc_nuevo": {
                    "$cond": {
                        "if": { "$eq": ["$condicion_data", "actual"] },
                        "then": 1,
                        "else": 0
                    }
                }
                , "num_pc_acumulado": {
                    "$cond": {
                        "if": { "$eq": ["$condicion_data", "anterior"] },
                        "then": 1,
                        "else": 0
                    }
                }
            }
        }


        //agrupacion lote
        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                }
                , "platas_x_lote": { "$min": "$platas_x_lote" }

                , "num_pc_nuevo": { "$sum": "$num_pc_nuevo" }
                , "num_pc_acumulado": { "$sum": "$num_pc_acumulado" }

                , "num_recuperadas": { "$sum": "$num_recuperadas" }

                // ,"data": { "$push": "$$ROOT" }//test
            }
        }

        // //test
        // , {
        //     $match: {
        //         "_id.lote": "1-3_95_B"
        //     }
        // }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "lote": "$_id.lote"

                    , "platas_x_lote": "$platas_x_lote"
                    , "num_pc_nuevo": "$num_pc_nuevo"
                    , "num_pc_acumulado": "$num_pc_acumulado"
                    , "num_recuperadas": "$num_recuperadas"
                }
            }
        }




        , {
            "$lookup": {
                "from": "cartography",
                "let": {
                    "lote": "$lote"
                },
                "pipeline": [
                    {
                        "$match": {
                            "properties.type": "trees",
                            "properties.custom.Erradicada por PC.value": true
                        }
                    },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "linea": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$linea",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "arbol": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$arbol",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0
                        }
                    }




                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$lote", "$$lote"] }
                                ]
                            }
                        }
                    },

                    { "$count": "count" }
                ],
                "as": "data_erradicadas"
            }
        },


        {
            "$addFields": {
                "erradicadas_pc_lote": { "$ifNull": [{ "$arrayElemAt": ["$data_erradicadas.count", 0] }, 0] }
            }
        },


        {
            "$addFields": {
                "formulario": "dinamica_de_enfermedades y formato_de_seguimiento_palmas_con_pchc"
            }
        },


        {
            "$project": {
                "data_erradicadas": 0
            }
        }









    ]
)
