db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "cartography",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$match": {
                            "properties.type": { "$in": ["blocks", "lot"] }
                        }
                    },


                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$_id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },


                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0
                        }
                    }



                    , {
                        "$project": {
                            "finca": "$finca"

                            , "cartografia_nombre": "$properties.name"
                            , "cartografia_tipo": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.type", "lot"] },
                                    "then": "Lote",
                                    "else": {
                                        "$cond": {
                                            "if": { "$eq": ["$properties.type", "blocks"] },
                                            "then": "Bloque",
                                            "else": "otro"
                                        }
                                    }
                                }
                            }
                            , "cartografia_production": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.production", true] },
                                    "then": "SI",
                                    "else": "NO"
                                }
                            }
                            , "cartografia_status": {
                                "$cond": {
                                    "if": { "$eq": ["$properties.status", true] },
                                    "then": "SI",
                                    "else": "NO"
                                }
                            }



                            , "num_lotes": {
                                "$ifNull": ["$properties.custom.num_lotes.value", 0]
                            }

                            , "num_lineas": {
                                "$ifNull": ["$properties.custom.num_lineas.value", 0]
                            }

                            , "num_arboles": {
                                "$ifNull": ["$properties.custom.num_arboles.value", 0]
                            }

                        }
                    }



                    , {
                        "$addFields": {
                            "rgDate": "$$filtro_fecha_inicio"
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]


)