

//---parametros
var db_name = "agroindustriasjmd";
var coleccion_name = "form_dinamicadeenfermedades";


//---query
var query_pipeline1 = [
    {
        "$addFields": {
            "variable_cartografia": "$Palma"//EDITAR
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },


    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },


    //---obtener solo los que no existen (tienen id viejo)
    {
        $match: {
            "tiene_variable_cartografia": "no"
        }
    }

    , {
        $project: {
            _id: "$_id"
            , cartografia_nombre: "$variable_cartografia.features.properties.name"
            , cartografia_path_old: "$variable_cartografia.features.path"
            , cartografia_id_old: "$variable_cartografia.features._id"
            //, cartografia_id_old: { "$toObjectId": "$variable_cartografia.features._id" }


        }
    }


    //test
    // , { $limit: 1 }

];


//--ejecutar query
var data_query = db.getSiblingDB(db_name)
    .getCollection(coleccion_name)
    .aggregate(query_pipeline1)
    .allowDiskUse();


// data_query


data_query.forEach(item_form => {

    //--obtener cartografia new
    var data_cartografia = db.getSiblingDB(db_name)
        .getCollection("cartography")
        .aggregate([
            {
                $match: {
                    "properties.name": item_form.cartografia_nombre
                }
            }
            , {
                $addFields: {
                    _id_str: { $toString: "$_id" }
                }
            }
        ])
        .allowDiskUse();

    data_cartografia.forEach(item_cartografia => {
        // console.log(item_form);
        // console.log(item_cartografia);
        // console.log("----------");


        //---UPDATE
        var data_query = db.getSiblingDB(db_name)
            .getCollection(coleccion_name)
            .update(
                {
                    "_id": item_form._id
                },
                {
                    $set: {
                        // "Palma.features._id": item_cartografia._id_str,
                        "Palma.features.$[]._id": item_cartografia._id_str,
                        "Palma.path": item_cartografia.path
                    }
                }
            );
    });
    data_cartografia.close();

});

//...aveces hay tirarlo varias veces por que se pausa con un error (.noCursorTimeout();)


//....existen problemas con los cursores y la session
