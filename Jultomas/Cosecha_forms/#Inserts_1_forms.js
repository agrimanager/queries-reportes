//======info de usuario
var user_name = "Jultomas";
var uid = ObjectId("5d6fd9bcfa5f0c67511de242");
var supervisor_id = ObjectId("5d6fe199fa5f0c67511de26f");

//======info de FORMULARIOS
var formulario1 = "form_informaciondelotes";
var formulario2 = "form_recoleccioncosecha";
var formulario3 = "form_despachodefruta";
//---aux
var fecha_rgDate = new Date;


//======INSERTAR FORMULARIOS
db.getCollection("forms").insert(
    [

        //--info lotes
        {
            //"uid": ObjectId("5ec4043570d49671746b8e42"),
            "uid": uid,
            "name": "Informacion de lotes",
            "fstructure": [
                "{\"name\":\"Lote\",\"type\":\"Cartography\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Peso Promedio\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Año de Siembra\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Area Total\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Numero de Plantas\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Variedad\",\"type\":\"Text\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Tarifa Kg por siembra\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":true}",
                "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
            ],
            "icon": "002",
            "anchor": "form_informaciondelotes",
            //"uname": "Hacienda La Gloria",
            "uname": user_name,
            "tracing": false,
            "timeTracing": "",
            "supervisors": [
                //ObjectId("5eda98e2b9c9a03e246e2b2e")
                supervisor_id
            ],
            "consecutives": null,
            "projectColor": "",
            "queryMap": "",
            "principalMaster": "",
            "followField": "",
            "backOption": "fixed",
            "lastData": false,
            //"rgDate": ISODate("2020-06-05T14:49:20.179-05:00"),
            //"uDate": ISODate("2020-06-24T14:07:53.658-05:00")
            "rgDate": fecha_rgDate,
            "uDate": fecha_rgDate
        },

        //--recoleccion
        {
            "uid": uid,
            "name": "Recoleccion cosecha",
            "fstructure": [
                "{\"name\":\"Lote\",\"type\":\"Cartography\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Empleado\",\"type\":\"Numeric employees\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[{\"id\":0,\"label\":\"Cortador\"},{\"id\":1,\"label\":\"Bufalero\"}],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Fecha corte\",\"type\":\"Date\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Numero de envio\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
            ],
            "icon": "117",
            "anchor": "form_recoleccioncosecha",
            "uname": user_name,
            "tracing": false,
            "timeTracing": "",
            "supervisors": [
                supervisor_id
            ],
            "consecutives": [
                "lot"
            ],
            "projectColor": "",
            "queryMap": "",
            "principalMaster": "",
            "followField": "",
            "backOption": "map",
            "lastData": false,
            //"rgDate": ISODate("2020-06-05T14:49:20.179-05:00"),
            //"uDate": ISODate("2020-06-24T14:07:53.658-05:00")
            "rgDate": fecha_rgDate,
            "uDate": fecha_rgDate
        },

        //---despacho
        {
            "uid": uid,
            "name": "Despacho de Fruta",
            "fstructure": [
                "{\"name\":\"Numero de envio\",\"type\":\"Number\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Fecha inicio de corte\",\"type\":\"Date\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Fecha fin de corte\",\"type\":\"Date\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"N ticket extractora\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Peso segun Extractora\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Fecha de despacho\",\"type\":\"Date\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Racimos totales\",\"type\":\"Number\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Numero de remision\",\"type\":\"Number\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
                "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
            ],
            "icon": "106",
            "anchor": "form_despachodefruta",
            "uname": user_name,
            "tracing": false,
            "timeTracing": "",
            "supervisors": [
                supervisor_id
            ],
            "consecutives": null,
            "projectColor": "",
            "queryMap": "",
            "principalMaster": "",
            "followField": "",
            "backOption": "fixed",
            "lastData": false,
            //"rgDate": ISODate("2020-06-05T14:49:20.179-05:00"),
            //"uDate": ISODate("2020-06-24T14:07:53.658-05:00")
            "rgDate": fecha_rgDate,
            "uDate": fecha_rgDate
        }

    ]

);



//----crear collecciones
db.createCollection(formulario1);
db.createCollection(formulario2);
db.createCollection(formulario3);



