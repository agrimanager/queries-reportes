db.getCollection("forms").insert(
{
    "uid": ObjectId("5ec4043570d49671746b8e42"),
    "name": "Informacion de lotes",
    "fstructure": [
        "{\"name\":\"Lote\",\"type\":\"Lots\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Año de Siembra\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Peso Promedio\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Variedad\",\"type\":\"Text\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Tarifa Kg por siembra\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":true}",
        "{\"name\":\"Area Total\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Numero de Plantas\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Fecha Siembra\",\"type\":\"Date\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
    ],
    "icon": "018",
    "anchor": "form_informaciondelotes",
    "uname": "Hacienda La Gloria",
    "tracing": false,
    "timeTracing": "",
    "supervisors": [
        ObjectId("5eda98e2b9c9a03e246e2b2e")
    ],
    "consecutives": null,
    "projectColor": "",
    "queryMap": "",
    "principalMaster": "",
    "followField": "",
    "backOption": "fixed",
    "lastData": false,
    "rgDate": ISODate("2020-06-05T14:49:20.179-05:00"),
    "uDate": ISODate("2020-06-24T14:07:53.658-05:00")
}
)