db.getCollection("forms").insert(
{
    "uid": ObjectId("5ec4043570d49671746b8e42"),
    "name": "Despacho de Fruta",
    "fstructure": [
        "{\"name\":\"Lotes a recoger\",\"type\":\"Smo\",\"grid\":24,\"master\":\"\",\"options\":[\"H1\",\"H2\",\"H3\",\"H4\",\"H5\",\"H6\",\"H7\",\"H8\",\"H9\",\"H10\",\"H11\",\"H12\",\"Z1\",\"Z2\",\"Z3\",\"Z4\",\"Z5\",\"Z6\",\"Z7\",\"Z8\",\"C1\",\"C2\",\"C3\",\"C4\",\"C5\",\"C6\"],\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Fecha de despacho\",\"type\":\"Date\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Fecha inicio de corte\",\"type\":\"Date\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Fecha fin de corte\",\"type\":\"Date\",\"grid\":8,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Racimos totales\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Numero de remision\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"N ticket extractora\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Peso segun Extractora\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Numero de envio\",\"type\":\"Number\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
    ],
    "icon": "106",
    "anchor": "form_despachodefruta",
    "uname": "Hacienda La Gloria",
    "tracing": false,
    "timeTracing": "",
    "supervisors": [
        ObjectId("5eda98e2b9c9a03e246e2b2e"),
        ObjectId("5f19bd5a6c3c832a33330707"),
        ObjectId("5f19bd5a6c3c832a33330709"),
        ObjectId("5f19bd5a6c3c832a3333070b"),
        ObjectId("5f19bd5a6c3c832a3333070d"),
        ObjectId("5f19bd5b6c3c832a3333070f"),
        ObjectId("5f19bd5a6c3c832a33330705")
    ],
    "consecutives": null,
    "projectColor": "",
    "queryMap": "",
    "principalMaster": "",
    "followField": "",
    "backOption": "fixed",
    "lastData": false,
    "rgDate": ISODate("2020-06-05T15:02:57.387-05:00"),
    "uDate": ISODate("2020-08-05T08:49:43.071-05:00")
}
)