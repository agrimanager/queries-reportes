db.getCollection("forms").insert(
{
    "uid": ObjectId("5ec4043570d49671746b8e42"),
    "name": "Recoleccion cosecha",
    "fstructure": [
        "{\"name\":\"Lote\",\"type\":\"Cartography\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Empleado\",\"type\":\"Numeric employees\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[{\"id\":0,\"label\":\"Cortador\"},{\"id\":1,\"label\":\"Bufalero\"}],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Fecha corte\",\"type\":\"Date\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Siembra\",\"type\":\"Number\",\"grid\":12,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Point\",\"type\":\"Point\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}",
        "{\"name\":\"Numero de envio\",\"type\":\"Number\",\"grid\":24,\"master\":\"\",\"options\":\"\",\"employees\":[],\"options_employees\":[],\"isCurrencyField\":\"No\"}"
    ],
    "icon": "117",
    "anchor": "form_recoleccioncosecha",
    "uname": "Hacienda La Gloria",
    "tracing": false,
    "timeTracing": "",
    "supervisors": [
        ObjectId("5eda98e2b9c9a03e246e2b2e"),
        ObjectId("5ef387cedff5cb11fa17a8f1"),
        ObjectId("5f19bd5a6c3c832a33330707"),
        ObjectId("5f19bd5a6c3c832a33330709"),
        ObjectId("5f19bd5a6c3c832a3333070b"),
        ObjectId("5f19bd5a6c3c832a3333070d"),
        ObjectId("5f19bd5b6c3c832a3333070f"),
        ObjectId("5f19bd5a6c3c832a33330705")
    ],
    "consecutives": [
        "lot"
    ],
    "projectColor": "",
    "queryMap": "",
    "principalMaster": "",
    "followField": "",
    "backOption": "map",
    "lastData": false,
    "rgDate": ISODate("2020-06-05T14:46:51.101-05:00"),
    "uDate": ISODate("2020-08-05T08:49:55.881-05:00")
}
)