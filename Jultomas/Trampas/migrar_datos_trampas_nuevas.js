// db.form_modulodetrampas.find({})
//    .projection({})
//    .sort({_id:-1})
//




var trampas = [

    // {
    //     trampa_desde: "123",
    //     trampa_hacia:"123"
    // },


    // {trampa_desde: "
    // 123
    // ",trampa_hacia:"
    // 123
    // "},


    { trampa_desde: "Triangulo-", trampa_hacia: "T048" },
    { trampa_desde: "t-9-1-31", trampa_hacia: "T047" },
    { trampa_desde: "t-9-1-29", trampa_hacia: "T051" },
    { trampa_desde: "t-9-1-28", trampa_hacia: "T051" },
    { trampa_desde: "t-8-1-23", trampa_hacia: "T037" },
    { trampa_desde: "t-7-3-3", trampa_hacia: "T01" },
    { trampa_desde: "t-7-3-03", trampa_hacia: "T01" },
    { trampa_desde: "t-7-2-2", trampa_hacia: "T02" },
    { trampa_desde: "t-7-1-1", trampa_hacia: "TR1" },
    { trampa_desde: "t-5-5A-19", trampa_hacia: "T034" },
    { trampa_desde: "t-5-3-20", trampa_hacia: "T036" },
    { trampa_desde: "t-5-1-21", trampa_hacia: "T038" },
    { trampa_desde: "t-4-2-18", trampa_hacia: "T030" },
    { trampa_desde: "t-4-2-17", trampa_hacia: "T028" },
    { trampa_desde: "t-4-1-18", trampa_hacia: "T031" },
    { trampa_desde: "t-3-2-6", trampa_hacia: "T08" },
    { trampa_desde: "t-3-2-06", trampa_hacia: "T08" },
    { trampa_desde: "t-3-1-5", trampa_hacia: "T07" },
    { trampa_desde: "t-3-1-4", trampa_hacia: "T05" },
    { trampa_desde: "t-2-4-11", trampa_hacia: "T018" },
    { trampa_desde: "t-2-3-10", trampa_hacia: "T016" },
    { trampa_desde: "t-2-1B-7", trampa_hacia: "T010" },
    { trampa_desde: "t-1-2-15", trampa_hacia: "T021" },
    { trampa_desde: "t-1-2-14", trampa_hacia: "T021" },
    { trampa_desde: "t-1-2-13", trampa_hacia: "T019" },
    { trampa_desde: "t-1-2-12", trampa_hacia: "T019" },
    { trampa_desde: "t-11-1-30", trampa_hacia: "T058" },
    { trampa_desde: "t-11-1-11", trampa_hacia: "T060" },
    { trampa_desde: "t-10-4-25", trampa_hacia: "T043" },
    { trampa_desde: "t-10-4-24", trampa_hacia: "T043" },
    { trampa_desde: "t-10-4-024", trampa_hacia: "T043" },
    { trampa_desde: "T-#33", trampa_hacia: "T052" },
    { trampa_desde: "T-#20", trampa_hacia: "T034" },
    { trampa_desde: "T-#2", trampa_hacia: "T02" },
    { trampa_desde: "T #38", trampa_hacia: "T020" },
    { trampa_desde: "T -#1", trampa_hacia: "T02" },
    { trampa_desde: "Rincon 3-", trampa_hacia: "T01" },
    { trampa_desde: "Quinta B- #24", trampa_hacia: "T040" },
    // { trampa_desde: "Parrilla 4- trampa N# "16"", trampa_hacia: "T022" },
    { trampa_desde: 'Parrilla 4- trampa N# "16"', trampa_hacia: "T022" },
    { trampa_desde: "Parrilla 2-N#17", trampa_hacia: "T029" },
    { trampa_desde: "Isla- T- 30", trampa_hacia: "T046" },
    { trampa_desde: "Isla- T -#29", trampa_hacia: "T044" },
    { trampa_desde: "Isla- T- #25", trampa_hacia: "T042" },
    { trampa_desde: "Isla 4-# 27", trampa_hacia: "T043" },
    { trampa_desde: "Isla 2- N #26-", trampa_hacia: "T041" },
    { trampa_desde: "Huila 2-", trampa_hacia: "T057" },
    { trampa_desde: "Huila 1- N# 28", trampa_hacia: "T051" },
    { trampa_desde: "Huila 1-", trampa_hacia: "T049" },
    { trampa_desde: "70-6-#-10", trampa_hacia: "T014" },
    { trampa_desde: "70-6-", trampa_hacia: "T014" },
    { trampa_desde: "70-4-", trampa_hacia: "T017" },
    { trampa_desde: "70-1B- T -#7", trampa_hacia: "T010" },
    { trampa_desde: "70-1B- #-8", trampa_hacia: "T012" },
    { trampa_desde: "70-1B-", trampa_hacia: "T011" },
    { trampa_desde: "70- T -#10", trampa_hacia: "T014" },
    { trampa_desde: "30-3-", trampa_hacia: "T09" },
    { trampa_desde: "30-2-", trampa_hacia: "T08" },
    { trampa_desde: "30-1-nueva06", trampa_hacia: "T06" },
    { trampa_desde: "30-1-4-2-2", trampa_hacia: "T04" },
    { trampa_desde: "30-1- nueva 03", trampa_hacia: "T03" },
    { trampa_desde: "16- 2  T- #14", trampa_hacia: "T021" },
    { trampa_desde: "100-4- N#21", trampa_hacia: "T035" },
    { trampa_desde: "100-1- #22", trampa_hacia: "T039" },


]



//====== Resultado
var result_info = [];

trampas.forEach(i => {


    var data_query = db.form_modulodetrampas.aggregate(

        {
            $match: {
                "Trampa.features.properties.name": {
                    // $in: trampas.trampa_desde
                    $in: [i.trampa_desde]
                }
            }
        }

        , {
            "$group": {
                "_id": null,
                "ids": {
                    "$push": "$_id"
                }
            }
        },
        {
            "$project": {
                "_id": 0,
                "ids": 1
            }
        }

    )


    data_query.forEach(item_query => {

        result_info.push({
            trampa_desde: i.trampa_desde,
            trampa_hacia: i.trampa_hacia,

            "ids_censos": item_query.ids
        })
    })





})




// result_info



//====== Resultado2
var result_info2 = [];

result_info.forEach(item_result => {

    var data_cartografia_trampa_hacia = db.cartography.aggregate(

        {
            $match: {
                "properties.name": item_result.trampa_hacia
            }
        }

        , {
            $addFields: {
                "_id_str": { $toString: "$_id" }
            }
        }

    )



    data_cartografia_trampa_hacia.forEach(item_cartografia => {

        result_info2.push({
            trampa_desde: item_result.trampa_desde,
            trampa_hacia: item_result.trampa_hacia,

            "ids_censos": item_result.ids_censos

            , "data_cartografia_trampa_hacia": item_cartografia
        })

    })




})


// result_info2


result_info2.forEach(item_final => {

    var trampa_path = item_final.data_cartografia_trampa_hacia.path;

    var trampa_features = [

        {
            "_id": item_final.data_cartografia_trampa_hacia._id_str,
            "type": "Feature",
            "path": item_final.data_cartografia_trampa_hacia.path,
            "properties": {
                "type": "traps",
                "name": item_final.data_cartografia_trampa_hacia.properties.name,
                "production": item_final.data_cartografia_trampa_hacia.properties.production,
                "status": item_final.data_cartografia_trampa_hacia.properties.status,
                "custom": item_final.data_cartografia_trampa_hacia.properties.custom,
            },
            "geometry": item_final.data_cartografia_trampa_hacia.geometry
        }


    ]


    db.form_modulodetrampas.updateMany(
        {
            _id: {
                "$in": item_final.ids_censos
            }
        }
        , {
            $set: {
                "Trampa.features": trampa_features,
                "Trampa.path": trampa_path,
            }

        }
    )




})



/*
// collection: cartography
{
	"_id" : ObjectId("64b947b094e279c9f4413184"),
	"type" : "Feature",
	"geometry" : {
		"type" : "Point",
		"coordinates" : [
			-74.2658462524414,
			10.576716423034668
		]
	},
	"properties" : {
		"name" : "T035",
		"type" : "traps",
		"production" : false,
		"status" : true,
		"custom" : {
			"color" : {
				"type" : "color",
				"value" : "#03A9F4"
			}
		}
	},
	"path" : ",5d6fdddffa5f0c67511de265,5f176789785bcfa3f07cf8f4,5f176789785bcfa3f07cf918,"
}
*/




/*

// collection: form_modulodetrampas
{
	"_id" : ObjectId("6283f845d150ac7bb7214949"),
	"Formula" : "",
	"Número de Insectos" : 0,
	"Número de Machos" : 0,
	"Número de Hembras" : 0,
	"Cambio de feromona" : "No",
	"Cambio de cebo" : "No",
	"Cambio de lamina" : "No",
	"Trampa" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5f17679b785bcfa3f07ddc75",
				"type" : "Feature",
				"path" : ",5d6fdddffa5f0c67511de265,5f176789785bcfa3f07cf8f3,5f176789785bcfa3f07cf902,",
				"properties" : {
					"type" : "traps",
					"name" : "t-4-1-20",
					"production" : false,
					"status" : true,
					"order" : 20,
					"custom" : {
						"Tipo de trampa" : {
							"type" : "string",
							"value" : ""
						},
						"color" : {
							"type" : "color",
							"value" : "#706363"
						}
					}
				},
				"geometry" : {
					"type" : "Point",
					"coordinates" : [
						-74.268912859261,
						10.5784449894835
					]
				}
			}
		],
		"path" : ",5d6fdddffa5f0c67511de265,5f176789785bcfa3f07cf8f3,5f176789785bcfa3f07cf902,"
	},
	"Point" : {
		"farm" : "5d6fdddffa5f0c67511de265",
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				-74.2690569,
				10.5784673
			]
		}
	},
	"Plaga" : "",
	"Observaciones" : "",
	"uid" : ObjectId("5d6fd9bcfa5f0c67511de242"),
	"supervisor" : "Luis Carlos",
	"rgDate" : ISODate("2022-05-16T10:29:38.000-05:00"),
	"uDate" : ISODate("2022-05-16T10:29:38.000-05:00"),
	"capture" : "M"
}

*/
