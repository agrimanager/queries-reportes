https://www.eniun.com/lista-codigos-de-colores-html-css/
//============== COLORES
//---bases
#ffffff : blanco
#000000 : negro
#808080 : gris
#c65911 : cafe
//---primarios
#ffff00 : amarillo
#0000ff : azul
#ff0000 : rojo
//---secundarios
#008000 : verde
#ffa500 : naranja
#ee82ee : morado o violeta



Blanco:#FFFFFF
Gris:#9c9c9c
Verde Claro:#00FF00
Verde Oscuro:   #008000}
Amarillo claro: #ffff00
Amarillo Oscuro:#e2ba1f
Naranja:#ff8000
Cafe:#7B3F32
Rojo:#ff0000
Azul:#1a73e8
Morado-Azul oscuro:#3f3b69
Vino Tinto:#722f37



--ejemplo
[0]:#FFFFFF,(0-5]:#008000,(5-10]:#FFFF00,(10-15]:#FF8000,(15-20]:#7B3F32,(>20):#FF0000

[0]--Blanco:#FFFFFF
(0 - 0.5]--Gris:#9c9c9c
(0.5 - 10]--Verde Claro:#00FF00
(10 - 15]--Verde Oscuro:   #008000
(15 - 20]--Amarillo claro: #ffff00
(20 - 25]--Amarillo Oscuro:#e2ba1f
(25 - 30]--Naranja:#ff8000
(30 - 35]--Cafe:#7B3F32
(>35)--Rojo:#ff0000


--FINAL
[0]:#FFFFFF,(0 - 0.5]:#9c9c9c,(0.5 - 10]:#00FF00,(10 - 15]: #008000,(15 - 20]: #ffff00,(20 - 25]:#e2ba1f,(25 - 30]:#ff8000,(30 - 35]:#7B3F32,(>35):#ff0000


...como el gris no se ve en el mapa por que los elemntos


https://htmlcolorcodes.com/es/

White	#FFFFFF
Silver	#C0C0C0
Gray	#808080
Black	#000000
Red	#FF0000
Maroon	#800000
Yellow	#FFFF00
Olive	#808000
Lime	#00FF00
Green	#008000
Aqua	#00FFFF
Teal	#008080
Blue	#0000FF
Navy	#000080
Fuchsia	#FF00FF
Purple	#800080



//---
Green	#008000
Yellow	#FFFF00
Red	#FF0000
