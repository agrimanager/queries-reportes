[

    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_pedidodecafe",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0

                        , "uDate": 0
                        , "Formula": 0
                    }
                }


                , {
                    "$addFields": {
                        "Fecha_rgDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                        , "Fecha de Solicitud": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Solicitud" } }
                        , "Fecha esperada de entrega": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha esperada de entrega" } }
                    }
                }


                , {
                    "$addFields": {
                        "Solicitante": "$supervisor"
                    }
                }
                , {
                    "$project": {
                        "supervisor": 0
                    }
                }


                , {
                    "$addFields": {
                        "Cantidad solicitada": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": ["$Presentacion de empaque", ""] }
                                        , "then": 0
                                    },
                                    {
                                        "case": { "$eq": ["$Presentacion de empaque", "283g"] }
                                        , "then": {
                                            "$multiply": ["$Cantidad Solicitada Paquetes", 283]
                                        }
                                    },
                                    {
                                        "case": { "$eq": ["$Presentacion de empaque", "340g"] }
                                        , "then": {
                                            "$multiply": ["$Cantidad Solicitada Paquetes", 340]
                                        }
                                    }

                                ],
                                "default": 0
                            }
                        }

                    }
                }


                , {
                    "$lookup": {
                        "from": "form_pedidodecafe",
                        "let": {
                            "id": "$_id"
                        },
                        "pipeline": [
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            { "$lte": ["$_id", "$$id"] }
                                        ]
                                    }
                                }
                            },

                            { "$count": "count" },
                            { "$project": { "count": { "$toString": "$count" } } }
                        ],
                        "as": "data_consecutivo"
                    }
                },

                {
                    "$addFields": {
                        "consecutivo": {
                            "$arrayElemAt": [
                                "$data_consecutivo.count",
                                0
                            ]
                        }
                    }
                },


                {
                    "$addFields": {
                        "consecutivo": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                                        "then": { "$concat": ["000", "$consecutivo"] }
                                    },
                                    {
                                        "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                                        "then": { "$concat": ["00", "$consecutivo"] }
                                    },
                                    {
                                        "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                                        "then": { "$concat": ["0", "$consecutivo"] }
                                    }
                                ],
                                "default": "$consecutivo"
                            }
                        }
                    }
                },


                {
                    "$project": {
                        "data_consecutivo": 0
                    }
                }


                , {
                    "$addFields": {
                        "NUMERO PEDIDO": "$consecutivo"
                    }
                }
                , {
                    "$project": {
                        "consecutivo": 0
                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
