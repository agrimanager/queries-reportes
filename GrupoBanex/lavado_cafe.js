//------LAVADO
var data = db.form_lavadodecafe.aggregate(
    [
        {
            $lookup: {
                from: "form_iniciodelavadodecafe",
                localField: "_id",
                foreignField: "_id",
                as: "data_inicio"
            }
        },

        {
            $lookup: {
                from: "form_finaldelavadodecafe",
                localField: "_id",
                foreignField: "_id",
                as: "data_fin"
            }
        },

        //restringir que ya esten insertados en los formularios de inicio y fin
        {
            $match: {
                data_inicio: [],
                data_fin: []
            }
        }
    ]
)


// data


data.forEach(item_data => {


    //-------- form_lavadodecafe
    // {
    // 	"_id" : ObjectId("6351449a734d2443f4de15fc"),
    // 	"Bache" : "C1401.0012",
    // 	"Formula" : "",
    // 	"Fecha inicial de lavado" : ISODate("2022-10-17T12:00:00.000-05:00"),
    // 	"Fecha final de lavado" : ISODate("2022-10-17T12:04:00.000-05:00"),
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.7397419,
    // 				10.740763
    // 			]
    // 		}
    // 	},
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-20T07:52:02.000-05:00"),
    // 	"uDate" : ISODate("2022-10-20T07:52:02.000-05:00"),
    // 	"capture" : "M"
    // }




    //==================================================================
    //-------- form_iniciodelavadodecafe
    // {
    // 	"_id" : ObjectId("635953d6734d2443f4de1888"),
    // 	"Bache" : "C0701.0019",
    // 	"Formula" : "",
    // 	"Fecha inicial" : ISODate("2022-10-26T10:34:00.000-05:00"),
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.0906152,
    // 				11.098564
    // 			]
    // 		}
    // 	},
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-26T10:34:24.000-05:00"),
    // 	"uDate" : ISODate("2022-10-26T10:34:24.000-05:00"),
    // 	"capture" : "M"
    // }

    var data_inicio = {

        "_id": item_data["_id"],

        // datos dinamicos
        "Bache": item_data["Bache"],
        "Fecha inicial": item_data["Fecha inicial de lavado"],


        //datos fijos
        "Point": item_data["Point"],
        "uid": item_data["uid"],
        "supervisor": item_data["supervisor"],
        "rgDate": item_data["rgDate"],
        "uDate": item_data["uDate"],
        "capture": item_data["capture"]

    }


    //==================================================================
    //-------- form_finaldelavadodecafe
    // {
    // 	"_id" : ObjectId("635953d6734d2443f4de188b"),
    // 	"Bache" : "C0701.0019",
    // 	"Formula" : "",
    // 	"Fecha final" : ISODate("2022-10-26T10:34:00.000-05:00"),
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.0906142,
    // 				11.0985614
    // 			]
    // 		}
    // 	},
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-26T10:34:57.000-05:00"),
    // 	"uDate" : ISODate("2022-10-26T10:34:57.000-05:00"),
    // 	"capture" : "M"
    // }

    var data_fin = {

        "_id": item_data["_id"],

        // datos dinamicos
        "Bache": item_data["Bache"],
        "Fecha final": item_data["Fecha final de lavado"],


        //datos fijos
        "Point": item_data["Point"],
        "uid": item_data["uid"],
        "supervisor": item_data["supervisor"],
        "rgDate": item_data["rgDate"],
        "uDate": item_data["uDate"],
        "capture": item_data["capture"]

    }


    // //test
    // console.log("-----")
    // console.log(data_inicio)
    // console.log("******")
    // console.log(data_fin)
    // console.log("-----")




    //----insertar datos
    db.form_iniciodelavadodecafe.insert(
        data_inicio
    )

    db.form_finaldelavadodecafe.insert(
        data_fin
    )


})
