/*
//-----------info

//---grupobanex (ticket#1429)

--Formularios:

Recoleccion de cafe en campo
Generacion de bache de Cafe
Despulpado de cafe

Inicio de fermentacion de cafe
Final de fermentacion de cafe

Inicio de lavado de cafe
Final de lavado de cafe

Inicio de secado de cafe
Final de secado de cafe

Precinto de Cafe
Analisis de cafe



--Proceso:
recoleccion (lote)
generacion_bache (lote) ---> (bache)
despulpado (bache)
fermentacion (bache)
lavado (bache)
secado (bache)
precinto (bache)
analisis (bache)

*/

db.form_recolecciondecafeencampo.aggregate(
    [


        //--puentes
        { "$unwind": "$Lote.features" }

        //--puente1
        , {
            "$lookup": {
                "from": "form_puentevariedadvscolorcafe",
                "as": "data_form_puentevariedadvscolorcafe",
                "let": {
                    "lote_id": "$Lote.features._id"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //{ "$eq": ["$Lote.features._id", "$$lote_id"] }
                                    { "$in": ["$$lote_id", "$Lote.features._id"] }
                                ]
                            }
                        }
                    }

                    //duda!!! (lote: 06. El Escondido tiene 2 variedades)
                    , { "$limit": 1 }


                ]
            }
        }

        //variedad y color
        , { "$unwind": "$data_form_puentevariedadvscolorcafe" }

        , {
            "$addFields": {
                "lote_variedad_nombre": "$data_form_puentevariedadvscolorcafe.Variedad"
                , "lote_variedad_color": "$data_form_puentevariedadvscolorcafe.Color"
            }
        }


        , {
            "$project": {
                "data_form_puentevariedadvscolorcafe": 0
            }
        }

        //--puente2
        , {
            "$lookup": {
                "from": "form_puenteidxvariedad",
                "as": "data_form_puenteidxvariedad",
                "let": {
                    "lote_variedad_nombre": "$lote_variedad_nombre"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Variedad", "$$lote_variedad_nombre"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }


                ]
            }
        }

        //Id variedad
        , { "$unwind": "$data_form_puenteidxvariedad" }

        , {
            "$addFields": {
                "lote_variedad_id": "$data_form_puenteidxvariedad.Id variedad"
            }
        }


        , {
            "$project": {
                "data_form_puenteidxvariedad": 0
            }
        }






    ]

)
