/*
//-----------info

//---grupobanex (ticket#1429)

--Formularios:

Recoleccion de cafe en campo (no se toma en cuenta)

Generacion de bache de Cafe

Despulpado de cafe

Inicio de fermentacion de cafe
Final de fermentacion de cafe

Inicio de lavado de cafe
Final de lavado de cafe

Inicio de secado de cafe
Final de secado de cafe

Precinto de Cafe
Analisis de cafe


--Proceso:
recoleccion (lote) (no se toma en cuenta)

generacion_bache (lote) ---> (bache)

despulpado (bache)
fermentacion (bache)
lavado (bache)
secado (bache)
precinto (bache)
analisis (bache)

*/



db.form_generaciondebachede.aggregate(
    [
        {
            "$lookup": {
                "from": "form_puenteidxvariedad",
                "localField": "Variedad",
                "foreignField": "Variedad",
                "as": "variedad_id"
            }
        },

        { "$addFields": { "variedad_color": { "$arrayElemAt": ["$variedad_id.Color", -1] } } },
        { "$addFields": { "variedad_id": { "$arrayElemAt": ["$variedad_id.Id variedad", -1] } } },


        {
            "$addFields": {
                "fecha": {
                    "$dateToString": {
                        "date": "$Fecha",
                        "format": "%d/%m/%Y"
                    }
                },

                "num_lote": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Lote" }, "string"] },
                        "then": {
                            "$arrayElemAt": [
                                { "$split": ["$Lote", "."] },
                                0
                            ]
                        },
                        "else": {
                            "$cond": {
                                "if": { "$lte": ["$Lote", 9] },
                                "then": { "$concat": ["0", { "$toString": "$Lote" }] },
                                "else": { "$toString": "$Lote" }
                            }
                        }
                    }
                }
            }
        },


        {
            "$lookup": {
                "from": "form_generaciondebachede",
                "let": {
                    "id": "$_id",
                    "finca_id": "$Point.farm"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$lte": ["$_id", "$$id"] },
                                    { "$eq": ["$Point.farm", "$$finca_id"] }
                                ]
                            }
                        }
                    },
                    { "$count": "count" },
                    { "$project": { "count": { "$toString": "$count" } } }
                ],
                "as": "data_consecutivo"
            }
        },

        {
            "$addFields": {
                "consecutivo": {
                    "$arrayElemAt": [
                        "$data_consecutivo.count",
                        0
                    ]
                }
            }
        },


        {
            "$addFields": {
                "consecutivo": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                                "then": { "$concat": ["000", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                                "then": { "$concat": ["00", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                                "then": { "$concat": ["0", "$consecutivo"] }
                            }
                        ],
                        "default": "$consecutivo"
                    }
                }
            }
        },


        {
            "$addFields": {
                "CODIGO_BACHE": {
                    "$concat": [
                        "C",
                        "$num_lote",
                        "$variedad_id",
                        ".",
                        "$consecutivo"
                    ]
                }
            }
        },


        {
            "$project": {
                // "num_lote": 0,
                // "variedad_id": 0,
                // "consecutivo": 0,
                // "Fecha": 0,
                "Point": 0,
                "capture": 0,
                "uid": 0,
                "Formula": 0,
                "data_consecutivo": 0
            }
        }



        //---recoleccion
        // , {
        //     "$lookup": {
        //         "from": "form_recolecciondecafeencampo",
        //         "as": "data_recoleccion",
        //         "let": {
        //             "lote_bache": "$Lote",
        //             "fehca_bache": "$fecha"
        //         },

        //         "pipeline": [


        //             {
        //                 "$addFields": {
        //                     "fecha": {
        //                         "$dateToString": {
        //                             "date": "$Fecha de recoleccion",
        //                             "format": "%d/%m/%Y"
        //                         }
        //                     }
        //                 }
        //             }

        //             , {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$in": ["$$lote_bache", "$Lote.features.properties.name"] }
        //                             , { "$eq": ["$fecha", "$$fehca_bache"] }
        //                         ]
        //                     }
        //                 }
        //             }


        //             // //duda!!! (lote: 06. El Escondido tiene 2 variedades)
        //             // , { "$limit": 1 }


        //         ]
        //     }
        // }


        // //---puente
        // //--puente1
        // , {
        //     "$lookup": {
        //         "from": "form_puentevariedadvscolorcafe",
        //         "as": "data_form_puentevariedadvscolorcafe",
        //         "let": {
        //             "lote_bache": "$Lote"
        //         },

        //         "pipeline": [

        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             //{ "$eq": ["$Lote.features._id", "$$lote_id"] }
        //                             { "$in": ["$$lote_bache", "$Lote.features.properties.name"] }
        //                         ]
        //                     }
        //                 }
        //             }

        //             //duda!!! (lote: 06. El Escondido tiene 2 variedades)
        //             , { "$limit": 1 }


        //         ]
        //     }
        // }

        // //variedad y color
        // , {
        //     "$unwind": {
        //         "path": "$data_form_puentevariedadvscolorcafe",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "lote_variedad_nombre": { "$ifNull": ["$data_form_puentevariedadvscolorcafe.Variedad", "sin_variedad"] }
        //         , "lote_variedad_color": { "$ifNull": ["$data_form_puentevariedadvscolorcafe.Color", "sin_color"] }
        //     }
        // }

        // , {
        //     "$project": {
        //         "data_form_puentevariedadvscolorcafe": 0
        //     }
        // }


        ////===CRUCE (xxxxx)
        // , {
        //     "$lookup": {
        //         "from": "xxxxxx",
        //         "as": "data_xxxx",
        //         "let": {
        //             "codigo_bache": "$CODIGO_BACHE"
        //         },

        //         "pipeline": [

        //             {
        //                 "$match": {
        //                     "$expr": {
        //                         "$and": [
        //                             { "$eq": ["$Bache", "$$codigo_bache"] }
        //                         ]
        //                     }
        //                 }
        //             }
        //             , { "$limit": 1 }
        //         ]
        //     }
        // }

        // , {
        //     "$unwind": {
        //         "path": "$data_xxxx",
        //         "preserveNullAndEmptyArrays": true
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "yyyyyyy": { "$ifNull": ["$data_xxxx.yyyyyyy", "sin_datos"] }
        //     }
        // }

        // , {
        //     "$project": {
        //         "data_xxxx": 0
        //     }
        // }


        //===CRUCE (Despulpado de cafe)
        , {
            "$lookup": {
                "from": "form_despulpadodecafe",
                "as": "data_form_despulpadodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_despulpadodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "despulpado_fecha": { "$ifNull": ["$data_form_despulpadodecafe.Fecha de despulpado", "sin_datos"] }
                , "despulpado_maquina": { "$ifNull": ["$data_form_despulpadodecafe.Maquina de despulpado", "sin_datos"] }
            }
        }

        , {
            "$project": {
                "data_form_despulpadodecafe": 0
            }
        }


        //===CRUCE (Inicio de fermentacion de cafe)
        , {
            "$lookup": {
                "from": "form_iniciodefermentaciondecafe",
                "as": "data_form_iniciodefermentaciondecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_iniciodefermentaciondecafe",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "fermentacion_inicio_tanque": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Tanque de fermentacion", "sin_datos"] }
                , "fermentacion_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Fecha inicial", "sin_datos"] }
                , "fermentacion_inicio_ph_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.PH inicial", 0] }
                , "fermentacion_inicio_grados_brix_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Grados Brix iniciales", 0] }

            }
        }

        , {
            "$project": {
                "data_form_iniciodefermentaciondecafe": 0
            }
        }


        //===CRUCE (Final de fermentacion de cafe)
        , {
            "$lookup": {
                "from": "form_finaldefermentaciondecafe",
                "as": "data_form_finaldefermentaciondecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_finaldefermentaciondecafe",
                "preserveNullAndEmptyArrays": true
            }
        }


        , {
            "$addFields": {
                "fermentacion_fin_tanque": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Tanque de fermentacion", "sin_datos"] }
                , "fermentacion_fin_fecha_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Fecha final", "sin_datos"] }
                , "fermentacion_fin_ph_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.PH Final", 0] }
                , "fermentacion_fin_grados_brix_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Grados Brix finales", 0] }
                , "fermentacion_fin_tipo_preparacion": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Tipo de preparacion", "sin_datos"] }

            }
        }

        , {
            "$project": {
                "data_form_finaldefermentaciondecafe": 0
            }
        }


        //===CRUCE (Inicio de lavado de cafe)
        , {
            "$lookup": {
                "from": "form_iniciodelavadodecafe",
                "as": "data_form_iniciodelavadodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_iniciodelavadodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "lavado_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodelavadodecafe.Fecha inicial", "sin_datos"] }
            }
        }

        , {
            "$project": {
                "data_form_iniciodelavadodecafe": 0
            }
        }


        //===CRUCE (Final de lavado de cafe)
        , {
            "$lookup": {
                "from": "form_finaldelavadodecafe",
                "as": "data_form_finaldelavadodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_finaldelavadodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        , {
            "$addFields": {
                "lavado_fin_fecha_final": { "$ifNull": ["$data_form_finaldelavadodecafe.Fecha final", "sin_datos"] }
            }
        }

        , {
            "$project": {
                "data_form_finaldelavadodecafe": 0
            }
        }


        //===CRUCE (Inicio de secado de cafe)
        , {
            "$lookup": {
                "from": "form_iniciodesecadodecafe",
                "as": "data_form_iniciodesecadodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_iniciodesecadodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        //         "Modulo de secado" : "Tunel Parab Solar",
        // 	"Fecha inicial" : ISODate("2022-10-31T13:00:00.000-05:00"),

        , {
            "$addFields": {
                "secado_inicio_modulo": { "$ifNull": ["$data_form_iniciodesecadodecafe.Modulo de secado", "sin_datos"] }
                , "secado_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodesecadodecafe.Fecha inicial", "sin_datos"] }
            }
        }

        , {
            "$project": {
                "data_form_iniciodesecadodecafe": 0
            }
        }


        //===CRUCE (Final de secado de cafe)
        , {
            "$lookup": {
                "from": "form_finaldesecadodecafe",
                "as": "data_form_finaldesecadodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_finaldesecadodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        //         	"Modulo de secado" : "Tunel Parab Solar",
        // 	"Fecha final" : ISODate("2022-11-02T13:00:00.000-05:00"),
        // 	"Humedad final" : 10,

        , {
            "$addFields": {
                "secado_fin_modulo": { "$ifNull": ["$data_form_finaldesecadodecafe.Modulo de secado", "sin_datos"] }
                , "secado_fin_fecha_final": { "$ifNull": ["$data_form_finaldesecadodecafe.Fecha final", "sin_datos"] }
                , "secado_fin_humedad": { "$ifNull": ["$data_form_finaldesecadodecafe.Humedad final", 0] }
            }
        }

        , {
            "$project": {
                "data_form_finaldesecadodecafe": 0
            }
        }




        //===CRUCE (Precinto de Cafe)
        , {
            "$lookup": {
                "from": "form_precintodecafe",
                "as": "data_form_precintodecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_precintodecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        //         	"Primer Precinto" : 19,
        // 	"Ultimo Precinto" : 19,
        // 	"Peso unitario ultimo precinto del bache" : 40,
        // 	"Kg Cafe pergamino seco de segunda" : 0,

        , {
            "$addFields": {
                "precinto_primer": { "$ifNull": ["$data_form_precintodecafe.Primer Precinto", 0] }
                , "precinto_ultimo": { "$ifNull": ["$data_form_precintodecafe.Ultimo Precinto", 0] }
                , "precinto_peso_unitario": { "$ifNull": ["$data_form_precintodecafe.Peso unitario ultimo precinto del bache", 0] }
                , "precinto_kg_seco_segunda": { "$ifNull": ["$data_form_precintodecafe.Kg Cafe pergamino seco de segunda", 0] }
            }
        }

        , {
            "$project": {
                "data_form_precintodecafe": 0
            }
        }


        //===CRUCE (Analisis de cafe)
        , {
            "$lookup": {
                "from": "form_analisisdecafe",
                "as": "data_form_analisisdecafe",
                "let": {
                    "codigo_bache": "$CODIGO_BACHE"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$Bache", "$$codigo_bache"] }
                                ]
                            }
                        }
                    }
                    , { "$limit": 1 }
                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_form_analisisdecafe",
                "preserveNullAndEmptyArrays": true
            }
        }

        //         	"Gr Muestra fisica" : 999,
        // 	"Gr CAV restante muestra" : 888,
        // 	"Gr CAVE restante muestra" : 777,
        // 	"Observaciones" : "xxxxx",

        , {
            "$addFields": {
                "analisis_gr_muestra_fisica": { "$ifNull": ["$data_form_analisisdecafe.Gr Muestra fisica", 0] }
                , "analisis_gr_cav_restante": { "$ifNull": ["$data_form_analisisdecafe.Gr CAV restante muestra", 0] }
                , "analisis_gr_cave_restante": { "$ifNull": ["$data_form_analisisdecafe.Gr CAVE restante muestra", 0] }
                , "analisis_observaciones": { "$ifNull": ["$data_form_analisisdecafe.Observaciones", ""] }
            }
        }

        , {
            "$project": {
                "data_form_analisisdecafe": 0
            }
        }

        //         	"fermentacion_inicio_fecha_inicial" : ISODate("2022-10-04T12:00:00.000-05:00"),
        // 	"fermentacion_fin_fecha_final" : ISODate("2022-10-08T12:00:00.000-05:00"),



        //------variables calculadas
        // //---restar fechas
        // , {
        //     "$addFields": {

        //         "total_dias_xxx": {
        //             "$cond": {
        //                 "if": {
        //                     "$or": [
        //                         { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
        //                         , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
        //                     ]
        //                 },
        //                 "then": 0,
        //                 "else": {
        //                     "$divide": [
        //                         {
        //                             "$subtract": [
        //                                 "$fermentacion_fin_fecha_final",
        //                                 "$fermentacion_inicio_fecha_inicial"
        //                             ]
        //                         },
        //                         86400000
        //                     ]
        //                 }
        //             }
        //         }
        //         , "total_horas_xxx": {
        //             "$cond": {
        //                 "if": {
        //                     "$or": [
        //                         { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
        //                         , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
        //                     ]
        //                 },
        //                 "then": 0,
        //                 "else": {
        //                     "$divide": [
        //                         {
        //                             "$subtract": [
        //                                 "$fermentacion_fin_fecha_final",
        //                                 "$fermentacion_inicio_fecha_inicial"
        //                             ]
        //                         },
        //                         3600000
        //                     ]
        //                 }
        //             }
        //         }


        //     }
        // }

        // , {
        //     "$addFields": {
        //         "total_dias_xxx": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_xxx", 100] }, { "$mod": [{ "$multiply": ["$total_dias_xxx", 100] }, 1] }] }, 100] }
        //         , "total_horas_xxx": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_xxx", 100] }, { "$mod": [{ "$multiply": ["$total_horas_xxx", 100] }, 1] }] }, 100] }
        //     }
        // }

        //---restar fechas (fermentacion)
        , {
            "$addFields": {

                "total_dias_fermentacion": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$fermentacion_fin_fecha_final",
                                        "$fermentacion_inicio_fecha_inicial"
                                    ]
                                },
                                86400000
                            ]
                        }
                    }
                }
                , "total_horas_fermentacion": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$fermentacion_fin_fecha_final",
                                        "$fermentacion_inicio_fecha_inicial"
                                    ]
                                },
                                3600000
                            ]
                        }
                    }
                }


            }
        }

        , {
            "$addFields": {
                "total_dias_fermentacion": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_fermentacion", 100] }, { "$mod": [{ "$multiply": ["$total_dias_fermentacion", 100] }, 1] }] }, 100] }
                , "total_horas_fermentacion": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_fermentacion", 100] }, { "$mod": [{ "$multiply": ["$total_horas_fermentacion", 100] }, 1] }] }, 100] }
            }
        }


        // //---restar fechas (lavado)
        , {
            "$addFields": {

                "total_dias_lavado": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$lavado_fin_fecha_final",
                                        "$lavado_inicio_fecha_inicial"
                                    ]
                                },
                                86400000
                            ]
                        }
                    }
                }
                , "total_horas_lavado": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$lavado_fin_fecha_final",
                                        "$lavado_inicio_fecha_inicial"
                                    ]
                                },
                                3600000
                            ]
                        }
                    }
                }


            }
        }

        , {
            "$addFields": {
                "total_dias_lavado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_lavado", 100] }, { "$mod": [{ "$multiply": ["$total_dias_lavado", 100] }, 1] }] }, 100] }
                , "total_horas_lavado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_lavado", 100] }, { "$mod": [{ "$multiply": ["$total_horas_lavado", 100] }, 1] }] }, 100] }
            }
        }


        //---restar fechas (secado)
        , {
            "$addFields": {

                "total_dias_secado": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$secado_fin_fecha_final",
                                        "$secado_inicio_fecha_inicial"
                                    ]
                                },
                                86400000
                            ]
                        }
                    }
                }
                , "total_horas_secado": {
                    "$cond": {
                        "if": {
                            "$or": [
                                { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] }
                                , { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] }
                            ]
                        },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$subtract": [
                                        "$secado_fin_fecha_final",
                                        "$secado_inicio_fecha_inicial"
                                    ]
                                },
                                3600000
                            ]
                        }
                    }
                }


            }
        }

        , {
            "$addFields": {
                "total_dias_secado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_secado", 100] }, { "$mod": [{ "$multiply": ["$total_dias_secado", 100] }, 1] }] }, 100] }
                , "total_horas_secado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_secado", 100] }, { "$mod": [{ "$multiply": ["$total_horas_secado", 100] }, 1] }] }, 100] }
            }
        }


        //---otros
        , {
            "$addFields": {

                "kilos_cps_bruto": {
                    "$add": [
                        {
                            "$multiply": [
                                {
                                    "$subtract": [
                                        { "$toDouble": "$precinto_ultimo" }
                                        , { "$toDouble": "$precinto_primer" }

                                    ]
                                },
                                40
                            ]
                        }
                        , { "$toDouble": "$precinto_peso_unitario" }
                        , 1
                    ]
                }

                , "cantidad_precintos": {
                    "$add": [
                        {
                            "$subtract": [
                                { "$toDouble": "$precinto_ultimo" }
                                , { "$toDouble": "$precinto_primer" }

                            ]
                        }
                        , 1
                    ]
                }

                , "kilos_cps_netos": {
                    "$add": [
                        {
                            "$multiply": [
                                {
                                    "$subtract": [
                                        { "$toDouble": "$precinto_ultimo" }
                                        , { "$toDouble": "$precinto_primer" }

                                    ]
                                },
                                40
                            ]
                        }
                        , { "$toDouble": "$precinto_peso_unitario" }
                    ]
                }


            }
        }

        //         	"precinto_primer" : 13,
        // 	"precinto_ultimo" : 13,
        // 	"precinto_peso_unitario" : 27,
        // 	"precinto_kg_seco_segunda" : 0,


        , {
            "$addFields": {
                "pct_rendimiento": {
                    "$cond": {
                        "if": { "$eq": [{ "$toDouble": "$Kg del bache" }, 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                {

                                    "$divide": [{ "$toDouble": "$kilos_cps_bruto" },
                                    { "$toDouble": "$Kg del bache" }]

                                }, 100
                            ]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {

                "kg_muestra": {
                    "$subtract": [
                        { "$toDouble": "$kilos_cps_bruto" }
                        , { "$toDouble": "$kilos_cps_netos" }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "factor_conversion": {
                    "$cond": {
                        "if": { "$eq": [{ "$toDouble": "$kilos_cps_bruto" }, 0] },
                        "then": 0,
                        "else": {

                            "$divide": [{ "$toDouble": "$Kg del bache" },
                            { "$toDouble": "$kilos_cps_bruto" }]

                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "kilos_cps_bruto": { "$divide": [{ "$subtract": [{ "$multiply": ["$kilos_cps_bruto", 100] }, { "$mod": [{ "$multiply": ["$kilos_cps_bruto", 100] }, 1] }] }, 100] }
                , "cantidad_precintos": { "$divide": [{ "$subtract": [{ "$multiply": ["$cantidad_precintos", 100] }, { "$mod": [{ "$multiply": ["$cantidad_precintos", 100] }, 1] }] }, 100] }
                , "kilos_cps_netos": { "$divide": [{ "$subtract": [{ "$multiply": ["$kilos_cps_netos", 100] }, { "$mod": [{ "$multiply": ["$kilos_cps_netos", 100] }, 1] }] }, 100] }
                , "pct_rendimiento": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_rendimiento", 100] }, { "$mod": [{ "$multiply": ["$pct_rendimiento", 100] }, 1] }] }, 100] }
                , "kg_muestra": { "$divide": [{ "$subtract": [{ "$multiply": ["$kg_muestra", 100] }, { "$mod": [{ "$multiply": ["$kg_muestra", 100] }, 1] }] }, 100] }
                , "factor_conversion": { "$divide": [{ "$subtract": [{ "$multiply": ["$factor_conversion", 100] }, { "$mod": [{ "$multiply": ["$factor_conversion", 100] }, 1] }] }, 100] }
            }
        }


        //         	"analisis_gr_muestra_fisica" : 0,
        // 	"analisis_gr_cav_restante" : 0,
        // 	"analisis_gr_cave_restante" : 0,
        // 	"analisis_observaciones" : "",


        , {
            "$addFields": {

                "gr_merma": {
                    "$subtract": [
                        { "$toDouble": "$analisis_gr_muestra_fisica" }
                        , { "$toDouble": "$analisis_gr_cav_restante" }
                    ]
                }
            }
        }

        , {
            "$addFields": {
                "pct_merma": {
                    "$cond": {
                        "if": { "$eq": [{ "$toDouble": "$analisis_gr_muestra_fisica" }, 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                {

                                    "$divide": [{ "$toDouble": "$gr_merma" },
                                    { "$toDouble": "$analisis_gr_muestra_fisica" }]

                                }, 100
                            ]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "pct_cave": {
                    "$cond": {
                        "if": { "$eq": [{ "$toDouble": "$analisis_gr_muestra_fisica" }, 0] },
                        "then": 0,
                        "else": {
                            "$multiply": [
                                {

                                    "$divide": [{ "$toDouble": "$analisis_gr_cave_restante" },
                                    { "$toDouble": "$analisis_gr_muestra_fisica" }]

                                }, 100
                            ]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "factor_trilla": {
                    "$cond": {
                        "if": { "$eq": [{ "$toDouble": "$pct_cave" }, 0] },
                        "then": 0,
                        "else": {
                            "$divide": [7000,
                                { "$toDouble": "$pct_cave" }]
                        }
                    }
                }
            }
        }


        , {
            "$addFields": {
                "gr_merma": { "$divide": [{ "$subtract": [{ "$multiply": ["$gr_merma", 100] }, { "$mod": [{ "$multiply": ["$gr_merma", 100] }, 1] }] }, 100] }
                , "pct_merma": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_merma", 100] }, { "$mod": [{ "$multiply": ["$pct_merma", 100] }, 1] }] }, 100] }
                , "pct_cave": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_cave", 100] }, { "$mod": [{ "$multiply": ["$pct_cave", 100] }, 1] }] }, 100] }
                , "factor_trilla": { "$divide": [{ "$subtract": [{ "$multiply": ["$factor_trilla", 100] }, { "$mod": [{ "$multiply": ["$factor_trilla", 100] }, 1] }] }, 100] }
            }
        }


        //--organizar formato de fechas


        ////ejemplo
        // , {
        //     "$addFields": {
        //         "fecha_txt_xxx": {
        //             "$cond": {
        //                 "if": { "$eq": [{ "$type": "$Fecha" }, "string"] },
        //                 "then": "sin fecha",
        //                 "else": {
        //                     "$dateToString": {
        //                         "date": "$Fecha",
        //                         "format": "%d/%m/%Y %H:%M"
        //                         ,"timezone": "America/Bogota"
        //                     }
        //                 }
        //             }

        //         }
        //     }
        // }


        // 	"Fecha" : ISODate("2022-10-03T16:00:00.000-05:00"),
        // 	"despulpado_fecha" : ISODate("2022-10-04T10:00:00.000-05:00"),
        // 	"fermentacion_inicio_fecha_inicial" : ISODate("2022-10-04T12:00:00.000-05:00"),
        // 	"fermentacion_fin_fecha_final" : ISODate("2022-10-08T12:00:00.000-05:00"),
        // 	"lavado_inicio_fecha_inicial" : ISODate("2022-10-08T12:00:00.000-05:00"),
        // 	"lavado_fin_fecha_final" : ISODate("2022-10-08T12:00:00.000-05:00"),
        // 	"secado_inicio_fecha_inicial" : ISODate("2022-10-08T12:05:00.000-05:00"),
        // 	"secado_fin_fecha_final" : ISODate("2022-10-27T14:19:00.000-05:00"),



        , {
            "$addFields": {

                "despulpado_fecha": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$despulpado_fecha" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$despulpado_fecha",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }

                , "fermentacion_inicio_fecha_inicial": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$fermentacion_inicio_fecha_inicial",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }

                , "fermentacion_fin_fecha_final": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$fermentacion_fin_fecha_final",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }

                , "lavado_inicio_fecha_inicial": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$lavado_inicio_fecha_inicial",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }

                , "lavado_fin_fecha_final": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$lavado_fin_fecha_final",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }
                , "secado_inicio_fecha_inicial": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$secado_inicio_fecha_inicial",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }
                , "secado_fin_fecha_final": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] },
                        "then": "sin fecha",
                        "else": {
                            "$dateToString": {
                                "date": "$secado_fin_fecha_final",
                                "format": "%d/%m/%Y %H:%M"
                                , "timezone": "America/Bogota"
                            }
                        }
                    }
                }

            }
        }


        //===========proyeccion final

        , {
            "$project": {

                // "z":"$Fecha"
                // ,"a":"$CODIGO_BACHE"

                // "CÓDIGO": "$CODIGO_BACHE",
                // "ID_ Lote": "$num_lote",
                // "Mezcla ": "$xxxxxx",
                // "Lote": "$Lote",
                // "ID_ Var": "$variedad_id",
                // "Variedad": "$lote_variedad_nombre",
                // "IDTR": "$xxxxxx",
                // "T_ Rec": "$Tipo de recoleccion",
                // "Fecha de recoleccion ": "$fecha",
                // "Kilos de cereza bache": "$Kg del bache",
                // "ID_ Desp_": "$xxxxxx",
                // "Modulo de despulpado": "$despulpado_maquina",
                // "Fecha de despulpado": "$despulpado_fecha",
                // "ID_ Tanque": "$xxxxxx",
                // "Tanque de Fermentacion": "$fermentacion_inicio_tanque",
                // "Fecha inicio de fermentacion ": "$fermentacion_inicio_fecha_inicial",
                // "pH I_ Fermentación": "$fermentacion_inicio_ph_inicial",
                // "Grados Brix Iniciales": "$fermentacion_inicio_grados_brix_inicial",
                // "Fecha final de fermentacion": "$fermentacion_fin_fecha_final",
                // "pH F_ Fermentación": "$fermentacion_fin_ph_final",
                // "Grados Brix Finales": "$fermentacion_fin_grados_brix_final",
                // "Dias de fermentacion": "$total_dias_fermentacion",
                // "Horas de fermentacion": "$total_horas_fermentacion",
                // "Inicio Lavado": "$lavado_inicio_fecha_inicial",
                // "Final Lavado": "$lavado_fin_fecha_final",
                // "Horas Lavado": "$total_horas_lavado",
                // "ID_ Mod_": "$xxxxxx",
                // "Modulo Secado": "$secado_inicio_modulo",
                // "Inicio de secado ": "$secado_inicio_fecha_inicial",
                // "Final de secado": "$secado_fin_fecha_final",
                // "Dias secado": "$total_dias_secado",
                // "Horas secado": "$total_horas_secado",
                // "Humedad Final": "$secado_fin_humedad",
                // "Primer Precinto": "$precinto_primer",
                // "Ultimo Prec_": "$precinto_ultimo",
                // "Peso U_ Prec_ (sin muestra)": "$precinto_peso_unitario",
                // "Color Precinto": "$lote_variedad_color",
                // "Kilos CPS Bruto": "$kilos_cps_bruto",
                // "Cant_ Precintos": "$cantidad_precintos",
                // "Kg CPS Netos (- muestra)": "$kilos_cps_netos",
                // "Kg CPS Segunda": "$precinto_kg_seco_segunda",
                // "Contable": "$xxxxxx",
                // "Rend/to": "$pct_rendimiento",
                // "Kg/muestra": "$kg_muestra",
                // "F_ Convers_": "$factor_conversion",
                // "ID_ Prep_": "$xxxxxx",
                // "Tipo Preparacion": "$fermentacion_fin_tipo_preparacion",
                // "M_ Fisica gr_": "$analisis_gr_muestra_fisica",
                // "gr_CAV RES_ M": "$analisis_gr_cav_restante",
                // "gr_MERMA": "$gr_merma",
                // "Merma %": "$pct_merma",
                // "gr_CAVE RES M_": "$analisis_gr_cave_restante",
                // "%CAVE": "$pct_cave",
                // "Factor de Trilla": "$factor_trilla",
                // "Kg TRILLADOS CAVE FACTOR 88": "$xxxxxx",
                // "Alarma de Trilla": "$xxxxxx",
                // "OBSERVACIONES": "$analisis_observaciones",

                "CÓDIGO": "$CODIGO_BACHE",
                "ID_ Lote": "$num_lote",
                "Lote": "$Lote",
                // "ID_ Var": "$variedad_id",
                //"Variedad": "$lote_variedad_nombre",
                "Variedad": "$Variedad",
                "T_ Rec": "$Tipo de recoleccion",
                "Fecha de recoleccion ": "$fecha",
                "Kilos de cereza bache": "$Kg del bache",
                "Modulo de despulpado": "$despulpado_maquina",
                "Fecha de despulpado": "$despulpado_fecha",
                "Tanque de Fermentacion": "$fermentacion_inicio_tanque",
                "Fecha inicio de fermentacion ": "$fermentacion_inicio_fecha_inicial",
                "pH I_ Fermentación": "$fermentacion_inicio_ph_inicial",
                "Grados Brix Iniciales": "$fermentacion_inicio_grados_brix_inicial",
                "Fecha final de fermentacion": "$fermentacion_fin_fecha_final",
                "pH F_ Fermentación": "$fermentacion_fin_ph_final",
                "Grados Brix Finales": "$fermentacion_fin_grados_brix_final",
                "Dias de fermentacion": "$total_dias_fermentacion",
                "Horas de fermentacion": "$total_horas_fermentacion",
                "Inicio Lavado": "$lavado_inicio_fecha_inicial",
                "Final Lavado": "$lavado_fin_fecha_final",
                "Horas Lavado": "$total_horas_lavado",
                "Modulo Secado": "$secado_inicio_modulo",
                "Inicio de secado ": "$secado_inicio_fecha_inicial",
                "Final de secado": "$secado_fin_fecha_final",
                "Dias secado": "$total_dias_secado",
                "Horas secado": "$total_horas_secado",
                "Humedad Final": "$secado_fin_humedad",
                "Primer Precinto": "$precinto_primer",
                "Ultimo Prec_": "$precinto_ultimo",
                "Peso U_ Prec_ (sin muestra)": "$precinto_peso_unitario",
                // "Color Precinto": "$lote_variedad_color",
                "Color Precinto": "$variedad_color",
                "Kilos CPS Bruto": "$kilos_cps_bruto",
                "Cant_ Precintos": "$cantidad_precintos",
                "Kg CPS Netos(- muestra)": "$kilos_cps_netos",
                "Kg CPS Segunda": "$precinto_kg_seco_segunda",
                "Rend/to": "$pct_rendimiento",
                "Kg/muestra": "$kg_muestra",
                "F_ Convers_": "$factor_conversion",
                "Tipo Preparacion": "$fermentacion_fin_tipo_preparacion",
                "M_ Fisica gr_": "$analisis_gr_muestra_fisica",
                "gr_CAV RES_ M": "$analisis_gr_cav_restante",
                "gr_MERMA": "$gr_merma",
                "Merma %": "$pct_merma",
                "gr_CAVE RES M_": "$analisis_gr_cave_restante",
                "%CAVE": "$pct_cave",
                "Factor de Trilla": "$factor_trilla",
                "OBSERVACIONES": "$analisis_observaciones"





            }

        }






    ]
)
