db.form_generaciondebachede.aggregate(
    [
        {
            "$lookup": {
                "from": "form_puenteidxvariedad",
                "localField": "Variedad",
                "foreignField": "Variedad",
                "as": "variedad_id"
            }
        },
        { "$addFields": { "variedad_id": { "$arrayElemAt": ["$variedad_id.Id variedad", -1] } } },


        {
            "$addFields": {
                "fecha": {
                    "$dateToString": {
                        "date": "$Fecha",
                        "format": "%d/%m/%Y"
                    }
                },

                "num_lote": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Lote" }, "string"] },
                        "then": {
                            "$arrayElemAt": [
                                { "$split": ["$Lote", "."] },
                                0
                            ]
                        },
                        "else": {
                            "$cond": {
                                "if": { "$lte": ["$Lote", 9] },
                                "then": { "$concat": ["0", { "$toString": "$Lote" }] },
                                "else": { "$toString": "$Lote" }
                            }
                        }
                    }
                }
            }
        },


        {
            "$lookup": {
                "from": "form_generaciondebachede",
                "let": {
                    "id": "$_id",
                    "finca_id": "$Point.farm"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$lte": ["$_id", "$$id"] },
                                    { "$eq": ["$Point.farm", "$$finca_id"] }
                                ]
                            }
                        }
                    },
                    { "$count": "count" },
                    { "$project": { "count": { "$toString": "$count" } } }
                ],
                "as": "data_consecutivo"
            }
        },

        {
            "$addFields": {
                "consecutivo": {
                    "$arrayElemAt": [
                        "$data_consecutivo.count",
                        0
                    ]
                }
            }
        },


        {
            "$addFields": {
                "consecutivo": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                                "then": { "$concat": ["000", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                                "then": { "$concat": ["00", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                                "then": { "$concat": ["0", "$consecutivo"] }
                            }
                        ],
                        "default": "$consecutivo"
                    }
                }
            }
        },


        {
            "$addFields": {
                "codigo": {
                    "$concat": [
                        "C",
                        "$num_lote",
                        "$variedad_id",
                        ".",
                        "$consecutivo"
                    ]
                }
            }
        },


        {
            "$project": {
                "num_lote": 0,
                "variedad_id": 0,
                "consecutivo": 0,
                "Fecha": 0,
                "Point": 0,
                "capture": 0,
                "uid": 0,
                "data_consecutivo": 0
            }
        }
    ]
)
