/*
//-----------info

//---grupobanex (ticket#1429)

--Formularios:

Recoleccion de cafe en campo
Generacion de bache de Cafe
Despulpado de cafe

Inicio de fermentacion de cafe
Final de fermentacion de cafe

Inicio de lavado de cafe
Final de lavado de cafe

Inicio de secado de cafe
Final de secado de cafe

Precinto de Cafe
Analisis de cafe



--Proceso:
recoleccion (lote)
generacion_bache (lote) ---> (bache)
despulpado (bache)
fermentacion (bache)
lavado (bache)
secado (bache)
precinto (bache)
analisis (bache)

*/

// db.form_recolecciondecafeencampo.aggregate(
//     [


//         //--puentes
//         { "$unwind": "$Lote.features" }

//         //--puente1
//         , {
//             "$lookup": {
//                 "from": "form_puentevariedadvscolorcafe",
//                 "as": "data_form_puentevariedadvscolorcafe",
//                 "let": {
//                     "lote_id": "$Lote.features._id"
//                 },

//                 "pipeline": [

//                     {
//                         "$match": {
//                             "$expr": {
//                                 "$and": [
//                                     //{ "$eq": ["$Lote.features._id", "$$lote_id"] }
//                                     { "$in": ["$$lote_id", "$Lote.features._id"] }
//                                 ]
//                             }
//                         }
//                     }

//                     //duda!!! (lote: 06. El Escondido tiene 2 variedades)
//                     , { "$limit": 1 }


//                 ]
//             }
//         }

//         //variedad y color
//         , { "$unwind": "$data_form_puentevariedadvscolorcafe" }

//         , {
//             "$addFields": {
//                 "lote_variedad_nombre": "$data_form_puentevariedadvscolorcafe.Variedad"
//                 , "lote_variedad_color": "$data_form_puentevariedadvscolorcafe.Color"
//             }
//         }


//         , {
//             "$project": {
//                 "data_form_puentevariedadvscolorcafe": 0
//             }
//         }

//         //--puente2
//         , {
//             "$lookup": {
//                 "from": "form_puenteidxvariedad",
//                 "as": "data_form_puenteidxvariedad",
//                 "let": {
//                     "lote_variedad_nombre": "$lote_variedad_nombre"
//                 },

//                 "pipeline": [

//                     {
//                         "$match": {
//                             "$expr": {
//                                 "$and": [
//                                     { "$eq": ["$Variedad", "$$lote_variedad_nombre"] }
//                                 ]
//                             }
//                         }
//                     }
//                     , { "$limit": 1 }


//                 ]
//             }
//         }

//         //Id variedad
//         , { "$unwind": "$data_form_puenteidxvariedad" }

//         , {
//             "$addFields": {
//                 "lote_variedad_id": "$data_form_puenteidxvariedad.Id variedad"
//             }
//         }


//         , {
//             "$project": {
//                 "data_form_puenteidxvariedad": 0
//             }
//         }






//     ]

// )


db.form_generaciondebachede.aggregate(
    [
        {
            "$lookup": {
                "from": "form_puenteidxvariedad",
                "localField": "Variedad",
                "foreignField": "Variedad",
                "as": "variedad_id"
            }
        },
        { "$addFields": { "variedad_id": { "$arrayElemAt": ["$variedad_id.Id variedad", -1] } } },


        {
            "$addFields": {
                "fecha": {
                    "$dateToString": {
                        "date": "$Fecha",
                        "format": "%d/%m/%Y"
                    }
                },

                "num_lote": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Lote" }, "string"] },
                        "then": {
                            "$arrayElemAt": [
                                { "$split": ["$Lote", "."] },
                                0
                            ]
                        },
                        "else": {
                            "$cond": {
                                "if": { "$lte": ["$Lote", 9] },
                                "then": { "$concat": ["0", { "$toString": "$Lote" }] },
                                "else": { "$toString": "$Lote" }
                            }
                        }
                    }
                }
            }
        },


        {
            "$lookup": {
                "from": "form_generaciondebachede",
                "let": {
                    "id": "$_id",
                    "finca_id": "$Point.farm"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$lte": ["$_id", "$$id"] },
                                    { "$eq": ["$Point.farm", "$$finca_id"] }
                                ]
                            }
                        }
                    },
                    { "$count": "count" },
                    { "$project": { "count": { "$toString": "$count" } } }
                ],
                "as": "data_consecutivo"
            }
        },

        {
            "$addFields": {
                "consecutivo": {
                    "$arrayElemAt": [
                        "$data_consecutivo.count",
                        0
                    ]
                }
            }
        },


        {
            "$addFields": {
                "consecutivo": {
                    "$switch": {
                        "branches": [
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                                "then": { "$concat": ["000", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                                "then": { "$concat": ["00", "$consecutivo"] }
                            },
                            {
                                "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                                "then": { "$concat": ["0", "$consecutivo"] }
                            }
                        ],
                        "default": "$consecutivo"
                    }
                }
            }
        },


        {
            "$addFields": {
                "codigo": {
                    "$concat": [
                        "C",
                        "$num_lote",
                        "$variedad_id",
                        ".",
                        "$consecutivo"
                    ]
                }
            }
        },


        {
            "$project": {
                // "num_lote": 0,
                // "variedad_id": 0,
                // "consecutivo": 0,
                // "Fecha": 0,
                "Point": 0,
                "capture": 0,
                "uid": 0,
                "Formula": 0,
                "data_consecutivo": 0
            }
        }



        //---recoleccion
        , {
            "$lookup": {
                "from": "form_recolecciondecafeencampo",
                "as": "data_recoleccion",
                "let": {
                    "lote_bache": "$Lote",
                    "fehca_bache": "$fecha"
                },

                "pipeline": [


                    {
                        "$addFields": {
                            "fecha": {
                                "$dateToString": {
                                    "date": "$Fecha de recoleccion",
                                    "format": "%d/%m/%Y"
                                }
                            }
                        }
                    }

                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$in": ["$$lote_bache", "$Lote.features.properties.name"] }
                                    , { "$eq": ["$fecha", "$$fehca_bache"] }
                                ]
                            }
                        }
                    }


                    // //duda!!! (lote: 06. El Escondido tiene 2 variedades)
                    // , { "$limit": 1 }


                ]
            }
        }



    ]
)
