[
    {
        "$lookup": {
            "from": "form_puenteidxvariedad",
            "localField": "Variedad",
            "foreignField": "Variedad",
            "as": "variedad_id"
        }
    },
    { "$addFields": { "variedad_color": { "$arrayElemAt": ["$variedad_id.Color", -1] } } },
    { "$addFields": { "variedad_id": { "$arrayElemAt": ["$variedad_id.Id variedad", -1] } } },


    {
        "$addFields": {
            "fecha": {
                "$dateToString": {
                    "date": "$Fecha",
                    "format": "%d/%m/%Y"
                }
            },

            "num_lote": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$Lote" }, "string"] },
                    "then": {
                        "$arrayElemAt": [
                            { "$split": ["$Lote", "."] },
                            0
                        ]
                    },
                    "else": {
                        "$cond": {
                            "if": { "$lte": ["$Lote", 9] },
                            "then": { "$concat": ["0", { "$toString": "$Lote" }] },
                            "else": { "$toString": "$Lote" }
                        }
                    }
                }
            }
        }
    },


    {
        "$lookup": {
            "from": "form_generaciondebachede",
            "let": {
                "id": "$_id",
                "finca_id": "$Point.farm"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$lte": ["$_id", "$$id"] },
                                { "$eq": ["$Point.farm", "$$finca_id"] }
                            ]
                        }
                    }
                },
                { "$count": "count" },
                { "$project": { "count": { "$toString": "$count" } } }
            ],
            "as": "data_consecutivo"
        }
    },

    {
        "$addFields": {
            "consecutivo": {
                "$arrayElemAt": [
                    "$data_consecutivo.count",
                    0
                ]
            }
        }
    },


    {
        "$addFields": {
            "consecutivo": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 1] },
                            "then": { "$concat": ["000", "$consecutivo"] }
                        },
                        {
                            "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 2] },
                            "then": { "$concat": ["00", "$consecutivo"] }
                        },
                        {
                            "case": { "$eq": [{ "$strLenCP": "$consecutivo" }, 3] },
                            "then": { "$concat": ["0", "$consecutivo"] }
                        }
                    ],
                    "default": "$consecutivo"
                }
            }
        }
    },


    {
        "$addFields": {
            "CODIGO_BACHE": {
                "$concat": [
                    "C",
                    "$num_lote",
                    "$variedad_id",
                    ".",
                    "$consecutivo"
                ]
            }
        }
    },


    {
        "$project": {
            "Point": 0,
            "capture": 0,
            "uid": 0,
            "Formula": 0,
            "data_consecutivo": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_despulpadodecafe",
            "as": "data_form_despulpadodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_despulpadodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "despulpado_fecha": { "$ifNull": ["$data_form_despulpadodecafe.Fecha de despulpado", "sin_datos"] }
            , "despulpado_maquina": { "$ifNull": ["$data_form_despulpadodecafe.Maquina de despulpado", "sin_datos"] }
        }
    }

    , {
        "$project": {
            "data_form_despulpadodecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_iniciodefermentaciondecafe",
            "as": "data_form_iniciodefermentaciondecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_iniciodefermentaciondecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "fermentacion_inicio_tanque": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Tanque de fermentacion", "sin_datos"] }
            , "fermentacion_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Fecha inicial", "sin_datos"] }
            , "fermentacion_inicio_ph_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.PH inicial", 0] }
            , "fermentacion_inicio_grados_brix_inicial": { "$ifNull": ["$data_form_iniciodefermentaciondecafe.Grados Brix iniciales", 0] }

        }
    }

    , {
        "$project": {
            "data_form_iniciodefermentaciondecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_finaldefermentaciondecafe",
            "as": "data_form_finaldefermentaciondecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_finaldefermentaciondecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "fermentacion_fin_tanque": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Tanque de fermentacion", "sin_datos"] }
            , "fermentacion_fin_fecha_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Fecha final", "sin_datos"] }
            , "fermentacion_fin_ph_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.PH Final", 0] }
            , "fermentacion_fin_grados_brix_final": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Grados Brix finales", 0] }
            , "fermentacion_fin_tipo_preparacion": { "$ifNull": ["$data_form_finaldefermentaciondecafe.Tipo de preparacion", "sin_datos"] }

        }
    }

    , {
        "$project": {
            "data_form_finaldefermentaciondecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_iniciodelavadodecafe",
            "as": "data_form_iniciodelavadodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_iniciodelavadodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "lavado_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodelavadodecafe.Fecha inicial", "sin_datos"] }
        }
    }

    , {
        "$project": {
            "data_form_iniciodelavadodecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_finaldelavadodecafe",
            "as": "data_form_finaldelavadodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_finaldelavadodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "lavado_fin_fecha_final": { "$ifNull": ["$data_form_finaldelavadodecafe.Fecha final", "sin_datos"] }
        }
    }

    , {
        "$project": {
            "data_form_finaldelavadodecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_iniciodesecadodecafe",
            "as": "data_form_iniciodesecadodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_iniciodesecadodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "secado_inicio_modulo": { "$ifNull": ["$data_form_iniciodesecadodecafe.Modulo de secado", "sin_datos"] }
            , "secado_inicio_fecha_inicial": { "$ifNull": ["$data_form_iniciodesecadodecafe.Fecha inicial", "sin_datos"] }
        }
    }

    , {
        "$project": {
            "data_form_iniciodesecadodecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_finaldesecadodecafe",
            "as": "data_form_finaldesecadodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_finaldesecadodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "secado_fin_modulo": { "$ifNull": ["$data_form_finaldesecadodecafe.Modulo de secado", "sin_datos"] }
            , "secado_fin_fecha_final": { "$ifNull": ["$data_form_finaldesecadodecafe.Fecha final", "sin_datos"] }
            , "secado_fin_humedad": { "$ifNull": ["$data_form_finaldesecadodecafe.Humedad final", 0] }
        }
    }

    , {
        "$project": {
            "data_form_finaldesecadodecafe": 0
        }
    }





    , {
        "$lookup": {
            "from": "form_precintodecafe",
            "as": "data_form_precintodecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_precintodecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "precinto_primer": { "$ifNull": ["$data_form_precintodecafe.Primer Precinto", 0] }
            , "precinto_ultimo": { "$ifNull": ["$data_form_precintodecafe.Ultimo Precinto", 0] }
            , "precinto_peso_unitario": { "$ifNull": ["$data_form_precintodecafe.Peso unitario ultimo precinto del bache", 0] }
            , "precinto_kg_seco_segunda": { "$ifNull": ["$data_form_precintodecafe.Kg Cafe pergamino seco de segunda", 0] }
        }
    }

    , {
        "$project": {
            "data_form_precintodecafe": 0
        }
    }



    , {
        "$lookup": {
            "from": "form_analisisdecafe",
            "as": "data_form_analisisdecafe",
            "let": {
                "codigo_bache": "$CODIGO_BACHE"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Bache", "$$codigo_bache"] }
                            ]
                        }
                    }
                }
                , { "$limit": 1 }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_form_analisisdecafe",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "analisis_gr_muestra_fisica": { "$ifNull": ["$data_form_analisisdecafe.Gr Muestra fisica", 0] }
            , "analisis_gr_cav_restante": { "$ifNull": ["$data_form_analisisdecafe.Gr CAV restante muestra", 0] }
            , "analisis_gr_cave_restante": { "$ifNull": ["$data_form_analisisdecafe.Gr CAVE restante muestra", 0] }
            , "analisis_observaciones": { "$ifNull": ["$data_form_analisisdecafe.Observaciones", ""] }
        }
    }

    , {
        "$project": {
            "data_form_analisisdecafe": 0
        }
    }


    , {
        "$addFields": {

            "total_dias_fermentacion": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$fermentacion_fin_fecha_final",
                                    "$fermentacion_inicio_fecha_inicial"
                                ]
                            },
                            86400000
                        ]
                    }
                }
            }
            , "total_horas_fermentacion": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$fermentacion_fin_fecha_final",
                                    "$fermentacion_inicio_fecha_inicial"
                                ]
                            },
                            3600000
                        ]
                    }
                }
            }


        }
    }

    , {
        "$addFields": {
            "total_dias_fermentacion": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_fermentacion", 100] }, { "$mod": [{ "$multiply": ["$total_dias_fermentacion", 100] }, 1] }] }, 100] }
            , "total_horas_fermentacion": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_fermentacion", 100] }, { "$mod": [{ "$multiply": ["$total_horas_fermentacion", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {

            "total_dias_lavado": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$lavado_fin_fecha_final",
                                    "$lavado_inicio_fecha_inicial"
                                ]
                            },
                            86400000
                        ]
                    }
                }
            }
            , "total_horas_lavado": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$lavado_fin_fecha_final",
                                    "$lavado_inicio_fecha_inicial"
                                ]
                            },
                            3600000
                        ]
                    }
                }
            }


        }
    }

    , {
        "$addFields": {
            "total_dias_lavado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_lavado", 100] }, { "$mod": [{ "$multiply": ["$total_dias_lavado", 100] }, 1] }] }, 100] }
            , "total_horas_lavado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_lavado", 100] }, { "$mod": [{ "$multiply": ["$total_horas_lavado", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {

            "total_dias_secado": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$secado_fin_fecha_final",
                                    "$secado_inicio_fecha_inicial"
                                ]
                            },
                            86400000
                        ]
                    }
                }
            }
            , "total_horas_secado": {
                "$cond": {
                    "if": {
                        "$or": [
                            { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] }
                            , { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] }
                        ]
                    },
                    "then": 0,
                    "else": {
                        "$divide": [
                            {
                                "$subtract": [
                                    "$secado_fin_fecha_final",
                                    "$secado_inicio_fecha_inicial"
                                ]
                            },
                            3600000
                        ]
                    }
                }
            }


        }
    }

    , {
        "$addFields": {
            "total_dias_secado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_dias_secado", 100] }, { "$mod": [{ "$multiply": ["$total_dias_secado", 100] }, 1] }] }, 100] }
            , "total_horas_secado": { "$divide": [{ "$subtract": [{ "$multiply": ["$total_horas_secado", 100] }, { "$mod": [{ "$multiply": ["$total_horas_secado", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {

            "kilos_cps_bruto": {
                "$add": [
                    {
                        "$multiply": [
                            {
                                "$subtract": [
                                    { "$toDouble": "$precinto_ultimo" }
                                    , { "$toDouble": "$precinto_primer" }

                                ]
                            },
                            40
                        ]
                    }
                    , { "$toDouble": "$precinto_peso_unitario" }
                    , 1
                ]
            }

            , "cantidad_precintos": {
                "$add": [
                    {
                        "$subtract": [
                            { "$toDouble": "$precinto_ultimo" }
                            , { "$toDouble": "$precinto_primer" }

                        ]
                    }
                    , 1
                ]
            }

            , "kilos_cps_netos": {
                "$add": [
                    {
                        "$multiply": [
                            {
                                "$subtract": [
                                    { "$toDouble": "$precinto_ultimo" }
                                    , { "$toDouble": "$precinto_primer" }

                                ]
                            },
                            40
                        ]
                    }
                    , { "$toDouble": "$precinto_peso_unitario" }
                ]
            }


        }
    }


    , {
        "$addFields": {
            "pct_rendimiento": {
                "$cond": {
                    "if": { "$eq": [{ "$toDouble": "$Kg del bache" }, 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {

                                "$divide": [{ "$toDouble": "$kilos_cps_bruto" },
                                { "$toDouble": "$Kg del bache" }]

                            }, 100
                        ]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {

            "kg_muestra": {
                "$subtract": [
                    { "$toDouble": "$kilos_cps_bruto" }
                    , { "$toDouble": "$kilos_cps_netos" }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "factor_conversion": {
                "$cond": {
                    "if": { "$eq": [{ "$toDouble": "$kilos_cps_bruto" }, 0] },
                    "then": 0,
                    "else": {

                        "$divide": [{ "$toDouble": "$Kg del bache" },
                        { "$toDouble": "$kilos_cps_bruto" }]

                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "kilos_cps_bruto": { "$divide": [{ "$subtract": [{ "$multiply": ["$kilos_cps_bruto", 100] }, { "$mod": [{ "$multiply": ["$kilos_cps_bruto", 100] }, 1] }] }, 100] }
            , "cantidad_precintos": { "$divide": [{ "$subtract": [{ "$multiply": ["$cantidad_precintos", 100] }, { "$mod": [{ "$multiply": ["$cantidad_precintos", 100] }, 1] }] }, 100] }
            , "kilos_cps_netos": { "$divide": [{ "$subtract": [{ "$multiply": ["$kilos_cps_netos", 100] }, { "$mod": [{ "$multiply": ["$kilos_cps_netos", 100] }, 1] }] }, 100] }
            , "pct_rendimiento": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_rendimiento", 100] }, { "$mod": [{ "$multiply": ["$pct_rendimiento", 100] }, 1] }] }, 100] }
            , "kg_muestra": { "$divide": [{ "$subtract": [{ "$multiply": ["$kg_muestra", 100] }, { "$mod": [{ "$multiply": ["$kg_muestra", 100] }, 1] }] }, 100] }
            , "factor_conversion": { "$divide": [{ "$subtract": [{ "$multiply": ["$factor_conversion", 100] }, { "$mod": [{ "$multiply": ["$factor_conversion", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {

            "gr_merma": {
                "$subtract": [
                    { "$toDouble": "$analisis_gr_muestra_fisica" }
                    , { "$toDouble": "$analisis_gr_cav_restante" }
                ]
            }
        }
    }

    , {
        "$addFields": {
            "pct_merma": {
                "$cond": {
                    "if": { "$eq": [{ "$toDouble": "$analisis_gr_muestra_fisica" }, 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {

                                "$divide": [{ "$toDouble": "$gr_merma" },
                                { "$toDouble": "$analisis_gr_muestra_fisica" }]

                            }, 100
                        ]
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "pct_cave": {
                "$cond": {
                    "if": { "$eq": [{ "$toDouble": "$analisis_gr_muestra_fisica" }, 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {

                                "$divide": [{ "$toDouble": "$analisis_gr_cave_restante" },
                                { "$toDouble": "$analisis_gr_muestra_fisica" }]

                            }, 100
                        ]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "factor_trilla": {
                "$cond": {
                    "if": { "$eq": [{ "$toDouble": "$pct_cave" }, 0] },
                    "then": 0,
                    "else": {
                        "$divide": [7000,
                            { "$toDouble": "$pct_cave" }]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "gr_merma": { "$divide": [{ "$subtract": [{ "$multiply": ["$gr_merma", 100] }, { "$mod": [{ "$multiply": ["$gr_merma", 100] }, 1] }] }, 100] }
            , "pct_merma": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_merma", 100] }, { "$mod": [{ "$multiply": ["$pct_merma", 100] }, 1] }] }, 100] }
            , "pct_cave": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_cave", 100] }, { "$mod": [{ "$multiply": ["$pct_cave", 100] }, 1] }] }, 100] }
            , "factor_trilla": { "$divide": [{ "$subtract": [{ "$multiply": ["$factor_trilla", 100] }, { "$mod": [{ "$multiply": ["$factor_trilla", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {

            "despulpado_fecha": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$despulpado_fecha" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$despulpado_fecha",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }

            , "fermentacion_inicio_fecha_inicial": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$fermentacion_inicio_fecha_inicial" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$fermentacion_inicio_fecha_inicial",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }

            , "fermentacion_fin_fecha_final": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$fermentacion_fin_fecha_final" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$fermentacion_fin_fecha_final",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }

            , "lavado_inicio_fecha_inicial": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$lavado_inicio_fecha_inicial" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$lavado_inicio_fecha_inicial",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }

            , "lavado_fin_fecha_final": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$lavado_fin_fecha_final" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$lavado_fin_fecha_final",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }
            , "secado_inicio_fecha_inicial": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$secado_inicio_fecha_inicial" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$secado_inicio_fecha_inicial",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }
            , "secado_fin_fecha_final": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$secado_fin_fecha_final" }, "string"] },
                    "then": "sin fecha",
                    "else": {
                        "$dateToString": {
                            "date": "$secado_fin_fecha_final",
                            "format": "%d/%m/%Y %H:%M"
                            , "timezone": "America/Bogota"
                        }
                    }
                }
            }

        }
    }




    , {
        "$project": {


          "CÓDIGO": "$CODIGO_BACHE",
          "ID_ Lote": "$num_lote",
          "Lote": "$Lote",
          "Variedad": "$Variedad",
          "T_ Rec": "$Tipo de recoleccion",
          "Fecha de recoleccion ": "$fecha",
          "Kilos de cereza bache": "$Kg del bache",
          "Modulo de despulpado": "$despulpado_maquina",
          "Fecha de despulpado": "$despulpado_fecha",
          "Tanque de Fermentacion": "$fermentacion_inicio_tanque",
          "Fecha inicio de fermentacion ": "$fermentacion_inicio_fecha_inicial",
          "pH I_ Fermentación": "$fermentacion_inicio_ph_inicial",
          "Grados Brix Iniciales": "$fermentacion_inicio_grados_brix_inicial",
          "Fecha final de fermentacion": "$fermentacion_fin_fecha_final",
          "pH F_ Fermentación": "$fermentacion_fin_ph_final",
          "Grados Brix Finales": "$fermentacion_fin_grados_brix_final",
          "Dias de fermentacion": "$total_dias_fermentacion",
          "Horas de fermentacion": "$total_horas_fermentacion",
          "Inicio Lavado": "$lavado_inicio_fecha_inicial",
          "Final Lavado": "$lavado_fin_fecha_final",
          "Horas Lavado": "$total_horas_lavado",
          "Modulo Secado": "$secado_inicio_modulo",
          "Inicio de secado ": "$secado_inicio_fecha_inicial",
          "Final de secado": "$secado_fin_fecha_final",
          "Dias secado": "$total_dias_secado",
          "Horas secado": "$total_horas_secado",
          "Humedad Final": "$secado_fin_humedad",
          "Primer Precinto": "$precinto_primer",
          "Ultimo Prec_": "$precinto_ultimo",
          "Peso U_ Prec_ (sin muestra)": "$precinto_peso_unitario",
          "Color Precinto": "$variedad_color",
          "Kilos CPS Bruto": "$kilos_cps_bruto",
          "Cant_ Precintos": "$cantidad_precintos",
          "Kg CPS Netos(- muestra)": "$kilos_cps_netos",
          "Kg CPS Segunda": "$precinto_kg_seco_segunda",
          "Rend/to": "$pct_rendimiento",
          "Kg/muestra": "$kg_muestra",
          "F_ Convers_": "$factor_conversion",
          "Tipo Preparacion": "$fermentacion_fin_tipo_preparacion",
          "M_ Fisica gr_": "$analisis_gr_muestra_fisica",
          "gr_CAV RES_ M": "$analisis_gr_cav_restante",
          "gr_MERMA": "$gr_merma",
          "Merma %": "$pct_merma",
          "gr_CAVE RES M_": "$analisis_gr_cave_restante",
          "%CAVE": "$pct_cave",
          "Factor de Trilla": "$factor_trilla",
          "OBSERVACIONES": "$analisis_observaciones"



        }

    }






]
