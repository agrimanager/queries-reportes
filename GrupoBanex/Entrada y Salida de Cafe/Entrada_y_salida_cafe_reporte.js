db.users.aggregate(
    [




        //=====BASE0 ---- inyeccion de variables (❌ BORRAR)
        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-01-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        //============================================
        //============================================
        //=====BASE1  ---- variables principal_concat
        { "$limit": 1 },
        {
            "$addFields": {
                "data_final": []
            }
        }

        //============================================
        //=====BASE2_i  ---- data_form

        , {
            "$lookup": {
                "from": "form_entradadecafe",
                "as": "data_form",
                "let": {},
                //query
                "pipeline": [


                    //--Finca
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0

                            , "uDate": 0
                            , "Formula": 0
                        }
                    }


                    //---fechas
                    , {
                        "$addFields": {
                            "Fecha_rgDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                            , "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
                        }
                    }


                    , {
                        "$project": {
                            "Fecha": "$Fecha"
                            , "Kilos": "$Kilos de cafe"
                            , "Estado": "$Estado"
                            , "Tipo": "Entrada"//----entrada o salida -------
                            , "Origen o Destino": "$Origen"//----origen o destino -------

                            , "Precinto en Bodega": { "$ifNull": ["$Precinto en bodega", ""] }
                            , "Observaciones": { "$ifNull": ["$Observaciones", ""] }


                            //otros
                            , "finca": "$finca"
                            , "supervisor": "$supervisor"
                            , "Fecha_rgDate": "$Fecha_rgDate"
                            , "rgDate": "$rgDate"//REPORTE SIN FORMULARIO

                        }
                    }

                ]
            }
        }


        , {
            "$project": {
                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data_final"
                        , "$data_form"
                    ]
                }


                , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
            }
        }

        // , { "$unwind": "$datos" }
        // , { "$replaceRoot": { "newRoot": "$datos" } }


        //============================================
        //=====BASE2_i  ---- data_form

        /*
        // collection: form_salidadecafe
{
	"_id" : ObjectId("63b627aaaf8bab0b919842dc"),
	"" : ISODate("2023-01-12T00:00:00.000-05:00"),
	"" : 999,
	"" : "CAVE",
	"" : "Trilladora",
	"" : "998 pepe",
	"Point" : {
		"type" : "Feature",
		"geometry" : {
			"type" : "Point",
			"coordinates" : [
				10.83291,
				-74.16778
			]
		},
		"farm" : "5e15ce3b2da22433970dfed5"
	},
	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
	"supervisor" : "Support team",
	"rgDate" : ISODate("2023-01-04T20:28:10.993-05:00"),
	"uDate" : ISODate("2023-01-04T20:28:10.993-05:00"),
	"capture" : "W"
}
        */

        , {
            "$lookup": {
                "from": "form_salidadecafe",
                "as": "data_form",
                "let": {},
                //query
                "pipeline": [


                    //--Finca
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0

                            , "uDate": 0
                            , "Formula": 0
                        }
                    }


                    //---fechas
                    , {
                        "$addFields": {
                            "Fecha_rgDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                            , "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
                        }
                    }


                    , {
                        "$project": {
                            "Fecha": "$Fecha"
                            //, "Kilos": "$Kilos de cafe"

                            , "Kilos": {
                                "$multiply": ["$Kilos de cafe", -1]
                            }

                            , "Estado": "$Estado"
                            , "Tipo": "Salida"//----entrada o salida -------
                            , "Origen o Destino": "$Destino"//----origen o destino -------

                            , "Precinto en Bodega": { "$ifNull": ["$Precinto en bodega", ""] }
                            , "Observaciones": { "$ifNull": ["$Observaciones", ""] }


                            //otros
                            , "finca": "$finca"
                            , "supervisor": "$supervisor"
                            , "Fecha_rgDate": "$Fecha_rgDate"
                            , "rgDate": "$rgDate"//REPORTE SIN FORMULARIO

                        }
                    }

                ]
            }
        }


        , {
            "$project": {
                //----matriz de datos
                "data_final": {
                    "$concatArrays": [
                        "$data_final"
                        , "$data_form"
                    ]
                }

                , "Busqueda inicio": "$Busqueda inicio"         //--filtro_fecha1
            }
        }


        //============================================
        //============================================
        //=====BASE3  ---- PROYECCIONFINAL FINAL

        //desagregar datos
        , { "$unwind": "$data_final" }

        //mostrar datos con variables de CONDICIONALES DE FECHA
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_final",
                        {
                            "rgDate": "$Busqueda inicio"
                        }
                    ]
                }
            }
        }



    ]
)
