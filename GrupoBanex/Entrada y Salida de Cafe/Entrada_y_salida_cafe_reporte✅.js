[



    { "$limit": 1 },
    {
        "$addFields": {
            "data_final": []
        }
    }


    , {
        "$lookup": {
            "from": "form_entradadecafe",
            "as": "data_form",
            "let": {},
            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0

                        , "uDate": 0
                        , "Formula": 0
                    }
                }


                , {
                    "$addFields": {
                        "Fecha_rgDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                        , "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
                    }
                }


                , {
                    "$project": {
                        "Fecha": "$Fecha"
                        , "Kilos": "$Kilos de cafe"
                        , "Estado": "$Estado"
                        , "Tipo": "Entrada"
                        , "Origen o Destino": "$Origen"

                        , "Precinto en Bodega": { "$ifNull": ["$Precinto en bodega", ""] }
                        , "Observaciones": { "$ifNull": ["$Observaciones", ""] }

                        , "finca": "$finca"
                        , "supervisor": "$supervisor"
                        , "Fecha_rgDate": "$Fecha_rgDate"
                        , "rgDate": "$rgDate"

                    }
                }

            ]
        }
    }


    , {
        "$project": {

            "data_final": {
                "$concatArrays": [
                    "$data_final"
                    , "$data_form"
                ]
            }

            , "Busqueda inicio": "$Busqueda inicio"
        }
    }


    , {
        "$lookup": {
            "from": "form_salidadecafe",
            "as": "data_form",
            "let": {},

            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0

                        , "uDate": 0
                        , "Formula": 0
                    }
                }


                , {
                    "$addFields": {
                        "Fecha_rgDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }

                        , "Fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha" } }
                    }
                }


                , {
                    "$project": {
                        "Fecha": "$Fecha"

                        , "Kilos": {
                            "$multiply": ["$Kilos de cafe", -1]
                        }

                        , "Estado": "$Estado"
                        , "Tipo": "Salida"
                        , "Origen o Destino": "$Destino"

                        , "Precinto en Bodega": { "$ifNull": ["$Precinto en bodega", ""] }
                        , "Observaciones": { "$ifNull": ["$Observaciones", ""] }


                        , "finca": "$finca"
                        , "supervisor": "$supervisor"
                        , "Fecha_rgDate": "$Fecha_rgDate"
                        , "rgDate": "$rgDate"

                    }
                }

            ]
        }
    }


    , {
        "$project": {

            "data_final": {
                "$concatArrays": [
                    "$data_final"
                    , "$data_form"
                ]
            }

            , "Busqueda inicio": "$Busqueda inicio"
        }
    }


    , { "$unwind": "$data_final" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data_final",
                    {
                      "rgDate": "$Busqueda inicio"
                    }
                ]
            }
        }
    }



]
