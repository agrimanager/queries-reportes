//------FERMENTACION
var data = db.form_fermentaciondecafe.aggregate(
    [
        {
            $lookup: {
                from: "form_iniciodefermentaciondecafe",
                localField: "_id",
                foreignField: "_id",
                as: "data_inicio"
            }
        },

        {
            $lookup: {
                from: "form_finaldefermentaciondecafe",
                localField: "_id",
                foreignField: "_id",
                as: "data_fin"
            }
        },

        //restringir que ya esten insertados en los formularios de inicio y fin
        {
            $match: {
                data_inicio: [],
                data_fin: []
            }
        }
    ]
)


// data


data.forEach(item_data => {

    // console.log(item_data["Point"]["farm"])
    //console.log(item_data["Tanque de fermentacion"])


    //-------- form_fermentaciondecafe
    // {
    // 	"_id" : ObjectId("6351449a734d2443f4de15f8"),
    // 	"Bache" : "C1401.0012",
    // 	"Formula" : "",
    // 	"Tanque de fermentacion" : "Tanque 09",
    // 	"Fecha inicial de fermentacion" : ISODate("2022-10-15T10:00:00.000-05:00"),
    // 	"Grados Brix iniciales" : 6.5,
    // 	"Fecha final de fermentacion" : ISODate("2022-10-17T12:00:00.000-05:00"),
    // 	"Grados Brix Finales" : 7,
    // 	"Tipo de Preparacion" : "SuaveLavado",
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.7397419,
    // 				10.740763
    // 			]
    // 		}
    // 	},
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-20T07:50:06.000-05:00"),
    // 	"uDate" : ISODate("2022-10-20T07:50:06.000-05:00"),
    // 	"capture" : "M"
    // }




    //==================================================================
    //-------- form_iniciodefermentaciondecafe
    // {
    // 	"_id" : ObjectId("635c42a48a0390259e0fa34d"),
    // 	"Bache" : "C0404.0023",
    // 	"Formula" : "",
    // 	"Tanque de fermentacion" : "Tanque 02",
    // 	"Fecha inicial" : ISODate("2022-10-28T15:58:00.000-05:00"),
    // 	"PH inicial" : 4.5,
    // 	"Grados Brix iniciales" : 4.2,
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.0908807,
    // 				11.0987559
    // 			]
    // 		}
    // 	},
    // 	"Tipo de preparacion" : "SuaveLavado",
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-28T15:58:57.000-05:00"),
    // 	"uDate" : ISODate("2022-10-28T15:58:57.000-05:00"),
    // 	"capture" : "M"
    // }

    var data_inicio = {

        "_id": item_data["_id"],

        // datos dinamicos
        "Bache": item_data["Bache"],
        "Tanque de fermentacion": item_data["Tanque de fermentacion"],
        "Fecha inicial": item_data["Fecha inicial de fermentacion"],
        //"PH inicial": item_data["XXXXXX"],//------------------------------------------->>>OJO
        "PH inicial": 0,//------------------------------------------->>>OJO
        "Grados Brix iniciales": item_data["Grados Brix iniciales"],
        "Tipo de preparacion": item_data["Tipo de Preparacion"],


        //datos fijos
        "Point": item_data["Point"],
        "uid": item_data["uid"],
        "supervisor": item_data["supervisor"],
        "rgDate": item_data["rgDate"],
        "uDate": item_data["uDate"],
        "capture": item_data["capture"]

    }


    //==================================================================
    //-------- form_finaldefermentaciondecafe
    // {
    // 	"_id" : ObjectId("635953d6734d2443f4de188a"),
    // 	"Bache" : "C0701.0019",
    // 	"Formula" : "",
    // 	"Tanque de fermentacion" : "Tanque 01",
    // 	"Fecha final" : ISODate("2022-10-26T10:33:00.000-05:00"),
    // 	"Grados Brix finales" : 0.5,
    // 	"Tipo de preparacion" : "SuaveLavado",
    // 	"Point" : {
    // 		"farm" : "61d5e8159dcc6155d1a7c831",
    // 		"type" : "Feature",
    // 		"geometry" : {
    // 			"type" : "Point",
    // 			"coordinates" : [
    // 				-74.0905662,
    // 				11.0985964
    // 			]
    // 		}
    // 	},
    // 	"uid" : ObjectId("5dd4681aba1d54556c2e033b"),
    // 	"supervisor" : "4 Edwin Navarro Porras",
    // 	"rgDate" : ISODate("2022-10-26T10:33:48.000-05:00"),
    // 	"uDate" : ISODate("2022-10-26T10:33:48.000-05:00"),
    // 	"capture" : "M"
    // }

    var data_fin = {

        "_id": item_data["_id"],

        // datos dinamicos
        "Bache": item_data["Bache"],
        "Tanque de fermentacion": item_data["Tanque de fermentacion"],
        "Fecha final": item_data["Fecha final de fermentacion"],
        //"PH Final": item_data["XXXXXX"],//------------------------------------------->>>OJO
        "PH Final": 0,//------------------------------------------->>>OJO
        "Grados Brix finales": item_data["Grados Brix Finales"],
        "Tipo de preparacion": item_data["Tipo de Preparacion"],

        //datos fijos
        "Point": item_data["Point"],
        "uid": item_data["uid"],
        "supervisor": item_data["supervisor"],
        "rgDate": item_data["rgDate"],
        "uDate": item_data["uDate"],
        "capture": item_data["capture"]

    }


    // //test
    // console.log("-----")
    // console.log(data_inicio)
    // console.log("******")
    // console.log(data_fin)
    // console.log("-----")




    //----insertar datos
    db.form_iniciodefermentaciondecafe.insert(
        data_inicio
    )

    db.form_finaldefermentaciondecafe.insert(
        data_fin
    )


})
