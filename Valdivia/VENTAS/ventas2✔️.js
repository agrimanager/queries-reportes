[
  
     {
        "$addFields": {
            "Variedad": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, 9]
                                        }, "PRODUCTO_"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , {
        "$unwind": {
            "path": "$Variedad",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "TOTAL": {
                "$multiply": ["$CANTIDAD", "$PRECIO"]
            }
        }
    }


    , {
        "$project": {
            "PRODUCTO": "$PRODUCTO",
            "VARIEDAD": { "$ifNull": ["$Variedad", "--sin variedad---"] },

            "PRECIO": "$PRECIO",
            "CANTIDAD": "$CANTIDAD",
            "UNIDAD": "$UNIDAD",
            "TOTAL PRECIO": "$TOTAL",

            "SEMANA": "$SEMANA",
            "REMISION": "$REMISION",

            "CLIENTE": "$CLIENTE",

            "FECHA": "$FECHA",
            "CALIDAD": "$CALIDAD",
            "LOTE": "$LOTE ",

            "FINCA": { "$ifNull": ["$FINCA", ""] },

            "supervisor": "$supervisor",
            "rgDate": "$rgDate",
            "capture": "$capture"
        }
    }
]