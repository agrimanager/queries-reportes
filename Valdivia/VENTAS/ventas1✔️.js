[
    {
        "$addFields": {
            "nombre_maestro_principal": "PRODUCTO_"
        }
    }

    , {
        "$addFields": {
            "num_letras_nombre_maestro_principal": {
                "$strLenCP": "$nombre_maestro_principal"
            }
        }
    }



    , {
        "$addFields": {

            "Tipo_variedad": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": {
                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                            { "$strLenCP": "$$dataKV.k" }]
                                    },
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Tipo_variedad": { "$arrayElemAt": ["$Tipo_variedad", 0] }
        }
    }

    , {
        "$addFields": {
            "Variedad": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$eq": [{
                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                        }, "$nombre_maestro_principal"]
                                    },
                                    "then": "$$dataKV.v",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    , {
        "$unwind": {
            "path": "$Variedad",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$addFields": {
            "TOTAL": {
                "$multiply": ["$CANTIDAD", "$PRECIO"]
            }
        }
    }


    , {
        "$project": {
            "PRODUCTO": "$PRODUCTO",
            "TIPO VARIEDAD": { "$ifNull": ["$Tipo_variedad", "--sin tipo---"] },
            "VARIEDAD": { "$ifNull": ["$Variedad", "--sin variedad---"] },

            "PRECIO": "$PRECIO",
            "CANTIDAD": "CANTIDAD",
            "UNIDAD": "$UNIDAD",
            "TOTAL PRECIO": "$TOTAL",

            "SEMANA": "$SEMANA",
            "REMISION": "$REMISION",

            "CLIENTE": "$CLIENTE",

            "FECHA": "$FECHA",
            "CALIDAD": "$CALIDAD",
            "LOTE ": "$LOTE ",

            "FINCA": { "$ifNull": ["$FINCA", ""] },

            "supervisor": "$supervisor",
            "rgDate": "$rgDate",
            "capture": "$capture"
        }
    }
]