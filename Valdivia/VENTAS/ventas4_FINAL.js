db.form_ventas.aggregate(
    [

        //-----!!!DANGER (desproyectar fechas)
        //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
        {
            "$project": {
                "rgDate día":0,
                "rgDate mes": 0,
                "rgDate año": 0,
                "rgDate hora": 0,
                
                "uDate día":0,
                "uDate mes": 0,
                "uDate año": 0,
                "uDate hora": 0
            }
        },




        //=====================================================
        //--nombre_maestro : aaaaa
        //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
        //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
        //--valor_mestro_enlazado : ccc cc c
        //=====================================================

        //--Maestro principal
        {
            "$addFields": {
                "nombre_maestro_principal": "PRODUCTO_"//---editar aca
            }
        }

        , {
            "$addFields": {
                "num_letras_nombre_maestro_principal": {
                    "$strLenCP": "$nombre_maestro_principal"
                }
            }
        }


        //--Mestro enlazado
        , {
            "$addFields": {
                // "nombre_mestro_enlazado": {
                "Tipo_variedad": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        //"then": "$$dataKV.k",
                                        "then": {
                                            "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                { "$strLenCP": "$$dataKV.k" }]
                                        },
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

        /*
        , {
            "$unwind": {
                // "path": "$nombre_mestro_enlazado",
                "path": "$Grupo_nombre",
                "preserveNullAndEmptyArrays": true
            }
        }
        */

        //----FALLA EN LA WEB
        //---//obtener el primero
        , {
            "$addFields": {
                "Tipo_variedad": { "$arrayElemAt": ["$Tipo_variedad", 0] }
            }
        }

        //--Valor Mestro enlazado
        , {
            "$addFields": {
                // "valor_mestro_enlazado": {
                "Variedad": {
                    "$filter": {
                        "input": {
                            "$map": {
                                "input": { "$objectToArray": "$$ROOT" },
                                "as": "dataKV",
                                "in": {
                                    "$cond": {
                                        "if": {
                                            "$eq": [{
                                                "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                            }, "$nombre_maestro_principal"]
                                        },
                                        "then": "$$dataKV.v",
                                        "else": ""
                                    }
                                }
                            }
                        },
                        "as": "item",
                        "cond": { "$ne": ["$$item", ""] }
                    }
                }
            }
        }

        , {
            "$unwind": {
                // "path": "$valor_mestro_enlazado",
                "path": "$Variedad",
                "preserveNullAndEmptyArrays": true
            }
        }


        //---operamos

        , {
            "$addFields": {
                "TOTAL": {
                    "$multiply": ["$CANTIDAD", "$PRECIO"]
                }
            }
        }



        //---proyectar
        , {
            "$project": {
                "PRODUCTO": "$PRODUCTO",
                // "TIPO VARIEDAD":"$XXXX",
                "TIPO VARIEDAD": { "$ifNull": ["$Tipo_variedad", "--sin tipo---"] },
                "VARIEDAD": { "$ifNull": ["$Variedad", "--sin variedad---"] },

                "PRECIO": "$PRECIO",
                "CANTIDAD": "$CANTIDAD",
                "UNIDAD": "$UNIDAD",
                "TOTAL PRECIO": "$TOTAL",

                "SEMANA": "$SEMANA",
                "REMISION": "$REMISION",

                "CLIENTE": "$CLIENTE",

                "FECHA": "$FECHA",
                "CALIDAD": "$CALIDAD",
                "LOTE ": "$LOTE ",

                "FINCA": { "$ifNull": ["$FINCA", ""] },

                "supervisor": "$supervisor",
                "rgDate": "$rgDate",
                "capture": "$capture",
            }
        }
    ]
)
