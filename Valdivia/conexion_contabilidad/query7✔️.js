[


    { "$limit": 1 },

    {
        "$lookup": {
            "from": "tasks",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
                , "id_user": "$_id"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$gte": [{ "$arrayElemAt": ["$when.start", -1] }, "$$filtro_fecha_inicio"] },
                                { "$lte": [{ "$arrayElemAt": ["$when.start", -1] }, "$$filtro_fecha_fin"] }

                                , { "$eq": ["$uid", "$$id_user"] }
                            ]
                        }
                    }
                }



            ]

        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }





    , {
        "$addFields": {
            "crop": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$crop" }, "object"] },
                    "then": [],
                    "else": "$crop"
                }
            },
            "lots": {
                "$cond": {
                    "if": { "$eq": ["$lots", {}] },
                    "then": [],
                    "else": "$lots"
                }
            }
        }
    }

    ,{
        "$match": {
            "active": true
        }
    }

    , {
        "$lookup": {
            "from": "activities",
            "localField": "activity",
            "foreignField": "_id",
            "as": "actividad"
        }
    },
    { "$unwind": "$actividad" },


    {
        "$lookup": {
            "from": "costsCenters",
            "localField": "ccid",
            "foreignField": "_id",
            "as": "costsCenter"
        }
    },
    { "$unwind": "$costsCenter" },

    {
        "$lookup": {
            "from": "companies",
            "localField": "costsCenter.cid",
            "foreignField": "_id",
            "as": "companies"
        }
    },
    { "$unwind": "$companies" },

    {
        "$lookup": {
            "from": "farms",
            "localField": "farm",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },


    { "$addFields": { "nombre_finca": "$finca.name" } },


    {
        "$addFields": {
            "nombre_cultivo": { "$arrayElemAt": ["$crop", 0] }
        }
    },

    {

        "$addFields": {
            "nombre_cultivo": {
                "$switch": {
                    "branches": [
                        {
                            "case": { "$eq": ["$nombre_cultivo", "avocado"] },
                            "then": "AGUACATE"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "banana"] },
                            "then": "BANANO"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "coffee"] },
                            "then": "CAFE"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "lemon"] },
                            "then": "LIMON"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "tangerine"] },
                            "then": "MANDARINA"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "orange"] },
                            "then": "NARANJA"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "pasilla"] },
                            "then": "PASILLA"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "platano"] },
                            "then": "PLATANO"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "yucca"] },
                            "then": "YUCA"
                        },
                        {
                            "case": { "$eq": ["$nombre_cultivo", "Admon"] },
                            "then": "ADMINISTRATIVO"
                        }
                    ], "default": "otro"
                }
            }
        }

    },

    {
        "$addFields": {
            "lots": {
                "$cond": {
                    "if": { "$eq": [{ "$type": "$lots" }, "array"] },
                    "then": "$lots",
                    "else": { "$map": { "input": { "$objectToArray": "$lots" }, "as": "lotKV", "in": "$$lotKV.v" } }
                }
            }
        }
    },


    {
        "$addFields": {
            "nombre_estado": {
                "$arrayElemAt": [
                    {
                        "$split": [
                            {
                                "$arrayElemAt": ["$lots", 0]
                            },
                            ", "
                        ]
                    },
                    1
                ]
            }
        }
    },





    {
        "$lookup": {
            "from": "form_configuraciondecentrosdecostos",
            "as": "configuracion_centros_de_costos",
            "let": {
                "nombre_estado": "$nombre_estado",
                "nombre_cultivo": "$nombre_cultivo",
                "nombre_finca": "$nombre_finca"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$nombre_finca", "$Finca"] },
                                { "$eq": ["$$nombre_cultivo", "$Cultivo"] },
                                { "$eq": ["$$nombre_estado", "$Estado"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$configuracion_centros_de_costos",
            "preserveNullAndEmptyArrays": true
        }
    }


    , {
        "$unwind": {
            "path": "$productivityReport",
            "preserveNullAndEmptyArrays": true
        }
    }

    , { "$addFields": { "employees": { "$toObjectId": "$productivityReport.employee" } } }

    , {
        "$lookup": {
            "from": "employees",
            "localField": "employees",
            "foreignField": "_id",
            "as": "Empleado"
        }
    }, {
        "$unwind": {
            "path": "$Empleado",
            "preserveNullAndEmptyArrays": true
        }
    }



    , {
        "$addFields": {
            "TotalPagoEmpleado": {
                "$multiply": [
                    { "$ifNull": [{ "$toDouble": "$productivityReport.quantity" }, 0] }
                    , { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }
                ]
            }

        }

    }

    , {
        "$addFields":
        {
            "TotalPagoEmpleado": {
                "$divide": [
                    {
                        "$subtract": [
                            { "$multiply": ["$TotalPagoEmpleado", 100] },
                            { "$mod": [{ "$multiply": ["$TotalPagoEmpleado", 100] }, 1] }
                        ]
                    },
                    100
                ]
            }
        }
    }



    , {
        "$project": {
            "Cuenta": "$actividad.cuc",
            "Comprobante": "",
            "Fecha": { "$arrayElemAt": ["$when.start", -1] },
            "Documento": "",
            "Documento Ref": "$cod",

            "NIT": { "$ifNull": ["$Empleado.numberID", "--"] },
            "Detalle": "$actividad.name",
            "Tipo": "1",
            "Valor": "$TotalPagoEmpleado",
            "Base": "",
            "Centro de Costos": { "$ifNull": ["$configuracion_centros_de_costos.Codigo Centro de Costos", "sin centro"] },
            "Trans Ext": "",
            "Plazo": ""

            , "Finca": "$nombre_finca"
            , "rgDate": { "$arrayElemAt": ["$when.start", -1] }
        }
    }

]
