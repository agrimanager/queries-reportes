db.users.aggregate(

    [
        {
            "$lookup": {
                "from": "tasks",
                "localField": "_id",
                "foreignField": "uid",
                "as": "labores"
            }
        }
        , {
            "$unwind": {
                "path": "$labores"
            }
        },




        {
            "$replaceRoot": {
                "newRoot": "$labores"
            }
        }


        , {
            "$match": {
                "lots": { "$exists": true },
                "crop": { "$exists": true }
            }
        }
        , {
            "$match": {
                "lots": { "$ne": "[]" },
                "crop": { "$ne": null }
            }
        }


        //---test
        ,{
            $match:{
                cod:"PA1PB"
            }
        }

        , {
            "$lookup": {
                "from": "activities",
                "localField": "activity",
                "foreignField": "_id",
                "as": "actividad"
            }
        },
        { "$unwind": "$actividad" },


        {
            "$lookup": {
                "from": "costsCenters",
                "localField": "ccid",
                "foreignField": "_id",
                "as": "costsCenter"
            }
        },
        { "$unwind": "$costsCenter" },

        {
            "$lookup": {
                "from": "companies",
                "localField": "costsCenter.cid",
                "foreignField": "_id",
                "as": "companies"
            }
        },
        { "$unwind": "$companies" },

        {
            "$lookup": {
                "from": "farms",
                "localField": "farm",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },


        { "$addFields": { "nombre_finca": "$finca.name" } },


        {
            "$addFields": {
                "nombre_cultivo": { "$arrayElemAt": ["$crop", 0] }
            }
        },


        /*
        ADMINISTRATIVO
        AGUACATE
        BANANO
        CAFE
        GUAYABA
        LIMON
        MANDARINA
        NARANJA
        PASILLA
        PLATANO
        YUCA
        */

        {

            "$addFields": {
                "nombre_cultivo": {
                    "$switch": {
                        "branches": [

                            {
                                "case": { "$eq": ["$nombre_cultivo", "avocado"] },
                                "then": "AGUACATE"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "banana"] },
                                "then": "BANANO"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "coffee"] },
                                "then": "CAFE"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "lemon"] },
                                "then": "LIMON"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "tangerine"] },
                                "then": "MANDARINA"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "orange"] },
                                "then": "NARANJA"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "pasilla"] },
                                "then": "PASILLA"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "platano"] },
                                "then": "PLATANO"
                            },
                            {
                                "case": { "$eq": ["$nombre_cultivo", "yucca"] },
                                "then": "YUCA"
                            }

                        ], "default": "otro"
                    }
                }
            }

        },

        {
            "$addFields": {
                "lots": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$lots" }, "array"] },
                        "then": "$lots",
                        "else": { "$map": { "input": { "$objectToArray": "$lots" }, "as": "lotKV", "in": "$$lotKV.v" } }
                    }
                }
            }
        },


        {
            "$addFields": {
                "nombre_estado": {
                    "$arrayElemAt": [
                        {
                            "$split": [
                                {
                                    "$arrayElemAt": ["$lots", 0]
                                },
                                ", "
                            ]
                        },
                        1
                    ]
                }
            }
        },




        {
            "$addFields":
            {
                "TotalPagoEmpleado": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$productivityAchieved" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$productivityPrice.price" }, 0] }
                    ]
                }
            }
        },

        {
            "$addFields":
            {
                "TotalPagoEmpleado": {
                    "$divide": [
                        {
                            "$subtract": [
                                { "$multiply": ["$TotalPagoEmpleado", 100] },
                                { "$mod": [{ "$multiply": ["$TotalPagoEmpleado", 100] }, 1] }
                            ]
                        },
                        100
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "form_configuraciondecentrosdecostos",
                "as": "configuracion_centros_de_costos",
                "let": {
                    "nombre_estado": "$nombre_estado",
                    "nombre_cultivo": "$nombre_cultivo",
                    "nombre_finca": "$nombre_finca"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$nombre_finca", "$Finca"] },
                                    { "$eq": ["$$nombre_cultivo", "$Cultivo"] },
                                    { "$eq": ["$$nombre_estado", "$Estado"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$configuracion_centros_de_costos",
                "preserveNullAndEmptyArrays": true
            }
        },



        {
            "$project": {
                "Cuenta": "$actividad.cuc",
                "Comprobante": "",
                "Fecha": { "$min": "$when.start" },
                "Documento": "",
                "Documento Ref": "$cod",
                "NIT": "$companies.nit",
                "Detalle": "$actividad.name",
                "Tipo": "1",
                "Valor": "$TotalPagoEmpleado",
                "Base": "",
                "Centro de Costos": "$configuracion_centros_de_costos.Codigo Centro de Costos",
                "Trans Ext": "",
                "Plazo": ""

                , "Point": "$Point"
                , "rgDate": "$rgDate"
            }
        }

    ]

)
// .count()
//35962

//52571 //--total
//51258 //crop lot
//35962
