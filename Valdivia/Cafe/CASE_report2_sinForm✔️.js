[




    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_cafe",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",  
                "filtro_fecha_fin": "$Busqueda fin"         
            },

            "pipeline": [
                {
                    "$addFields": {
                        "tasa": { "$multiply": ["$Pergamino kg", 0.08] }
                    }
                },

                {
                    "$addFields": {

                        "factor_finca": { "$divide": ["$Cereza kg", "$tasa"] }
                    }
                },
                {
                    "$addFields": {
                        "venta_total": { "$multiply": ["$Pergamino kg", "$Precio kg"] }
                    }
                },
                {
                    "$addFields": {
                        "Precio tasa": { "$divide": ["$venta_total", "$tasa"] }
                    }
                },


                {
                    "$addFields": {
                        "Txt_fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                        "Txt_Semana": { "$isoWeek": "$rgDate" }
                    }
                }

                ,{
                    "$project":{
                        "Point":0,
                        "uid":0,
                        "uDate":0
                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]