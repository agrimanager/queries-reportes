db.form_cafe.aggregate(
    [
        {
            "$addFields": {
                "@": { "$multiply": ["$Pergamino kg", 0.08] }
            }
        },

        {
            "$addFields": {

                "factor_finca": { "$divide": ["$Cereza kg", "$@"] }
            }
        },
        {
            "$addFields": {
                "venta_total": { "$multiply": ["$Pergamino kg", "$Precio kg"] }
            }
        },
        {
            "$addFields": {
                "Precio @": { "$divide": ["$venta_total", "$@"] }
            }
        },


        {
            "$addFields": {
                "Txt_fecha": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } },
                "Txt_Semana": { "$isoWeek": "$rgDate" }
            }
        }


    ]
)