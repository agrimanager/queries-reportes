db.users.aggregate(
    [



        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_controlcanastilla",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                "pipeline": [

                    //==============QUERY=====================

                    //--VALIDAR FECHA BUENA
                    { "$addFields": { "fecha_filtro": "$Fecha" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    //--fechas
                    //---OJO RG_DATE
                    { "$addFields": { "rgDate": "$Fecha" } },

                    //.... año,mes,semana.....
                    {
                        "$addFields": {
                            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
                        }
                    },




                    {
                        "$group": {
                            // "_id": {
                            //     "fecha": "$Fecha_Txt"
                            // }
                            "_id": "$Fecha_Txt"

                            , "sum_entradas": { "$sum": "$Entrada" }
                            , "sum_salidas": { "$sum": "$Salida" }
                        }
                    },

                    { "$sort": { "_id": 1 } },

                    {
                        "$addFields": {
                            "saldo_parcial": {
                                "$subtract": ["$sum_entradas", "$sum_salidas"]
                            }
                        }
                    },



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //------cruce2
        , {
            "$lookup": {
                "from": "form_controlcanastilla",
                "as": "data",
                "let": {
                    // "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    // "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                    "fecha_txt": "$_id"
                },
                "pipeline": [

                    //==============QUERY=====================

                    //--VALIDAR FECHA BUENA
                    { "$addFields": { "fecha_filtro": "$Fecha" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    //--fechas
                    //---OJO RG_DATE
                    { "$addFields": { "rgDate": "$Fecha" } },

                    //.... año,mes,semana.....
                    {
                        "$addFields": {
                            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
                        }
                    },




                    {
                        "$group": {
                            // "_id": {
                            //     "fecha": "$Fecha_Txt"
                            // }
                            "_id": "$Fecha_Txt"

                            , "sum_entradas": { "$sum": "$Entrada" }
                            , "sum_salidas": { "$sum": "$Salida" }
                        }
                    },

                    { "$sort": { "_id": 1 } },

                    {
                        "$addFields": {
                            "saldo_parcial": {
                                "$subtract": ["$sum_entradas", "$sum_salidas"]
                            }
                        }
                    },



                    //---condicion de fechas
                    {
                        "$match": {
                            "$expr": {
                                "$lte": [
                                    { "$toDate": "$_id" }
                                    ,
                                    { "$toDate": "$$fecha_txt" }
                                ]
                            }
                        }
                    },

                ]
            }
        }


        //-----SALDO
        , {
            "$addFields": {
                "Saldo": {
                    "$reduce": {
                        "input": "$data.saldo_parcial",
                        "initialValue": 0,
                        "in": {
                            "$sum": [
                                "$$value",
                                "$$this"
                            ]
                        }
                    }
                }
            }
        }


        , {
            "$project": {
                "data": 0
            }
        }







    ]
)