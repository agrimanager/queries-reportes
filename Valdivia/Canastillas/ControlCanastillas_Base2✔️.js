[

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_controlcanastilla",
                "as": "data",
                "let": {

                },
                "pipeline": [


                    { "$addFields": { "fecha_filtro": "$Fecha" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    { "$addFields": { "rgDate": "$Fecha" } },

                    {
                        "$addFields": {
                            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
                        }
                    },




                    {
                        "$group": {
                            "_id": "$Fecha_Txt"

                            , "rgDate": { "$min": "$rgDate" }
                            , "total_entradas": { "$sum": "$Entrada" }
                            , "total_salidas": { "$sum": "$Salida" }

                        }
                    },

                    { "$sort": { "_id": 1 } },

                    {
                        "$addFields": {
                            "saldo_parcial": {
                                "$subtract": ["$total_entradas", "$total_salidas"]
                            }
                        }
                    }



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


        , { "$addFields": { "fecha": "$_id" } }
        , { "$project": { "_id": 0 } }




        , {
            "$lookup": {
                "from": "form_controlcanastilla",
                "as": "data",
                "let": {
                    "fecha_txt": "$fecha"
                },
                "pipeline": [

                    { "$addFields": { "fecha_filtro": "$Fecha" } },
                    { "$addFields": { "anio_filtro": { "$year": "$fecha_filtro" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    { "$addFields": { "rgDate": "$Fecha" } },

                    {
                        "$addFields": {
                            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } }
                        }
                    },


                    {
                        "$group": {

                            "_id": "$Fecha_Txt"

                            , "total_entradas": { "$sum": "$Entrada" }
                            , "total_salidas": { "$sum": "$Salida" }
                        }
                    },

                    { "$sort": { "_id": 1 } },

                    {
                        "$addFields": {
                            "saldo_parcial": {
                                "$subtract": ["$total_entradas", "$total_salidas"]
                            }
                        }
                    },


                    {
                        "$match": {
                            "$expr": {
                                "$lte": [
                                    { "$toDate": "$_id" }
                                    ,
                                    { "$toDate": "$$fecha_txt" }
                                ]
                            }
                        }
                    }

                ]
            }
        }

        , {
            "$addFields": {
                "SALDO TOTAL ACUMULADO": {
                    "$reduce": {
                        "input": "$data.saldo_parcial",
                        "initialValue": 0,
                        "in": {
                            "$sum": [
                                "$$value",
                                "$$this"
                            ]
                        }
                    }
                }
            }
        }


        , {
            "$project": {
                "data": 0
            }
        }


    ]