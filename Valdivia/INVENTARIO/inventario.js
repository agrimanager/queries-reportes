//--reset cantidad de productos
db.supplies.updateMany(
    {}
    ,
    {$set:{quantity:0}}
);


//--borrar TODAS las transacciones
db.suppliesTimeline.deleteMany({});


//--query para ingresos
db.suppliesTimeline_new0.aggregate(
    {
        $match:{
            quantity:{$gt:0},
            deleted:false,
            lbid : ""
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    {$unwind:"$productos"}
    ,{
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    {$unwind:"$bodegas"}
    ,{
        "$project": {
            "wid": "$bodegas.name",
            "tax":{
                $cond: {
                    if: {
                        $eq: ["$tax", ""]
                    },
                    then: 0,
                    else: "$tax"
                }
            },
            "name": "$productos.name",
            "type":"Ingresos",
            "quantity": "$productEquivalence",
            "invoiceNumber":"$invoiceNumber",
            "inf":"$information",
            "invoiceDate": {
                $cond: {
                    if: {
                        $eq: ["$invoiceDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$invoiceDate"}}
                }
            },
            "dueDate":{
                $cond: {
                    if: {
                        $eq: ["$dueDate", ""]
                    },
                    then: "",
                    else: {$dateToString: {format: "%Y-%m-%d", date: "$dueDate"}}
                }
            },
            "inputmeasure": "$inputmeasure",
            "rgDate": {$dateToString: {format: "%Y-%m-%d", date: "$rgDate"}},
            "subtotal":"$subtotal",
            "lbid":"",
            "lbdoce":""
        }
    }
);


//....importar datos en la web


//....cambiar typeSupply=Ingresos de transaccion de ingreso
db.suppliesTimeline.aggregate([
    {
        $match: {
            typeSupply: "Ingresos"
        }
    },
    {
        $project: {
            sid: 1
        }
    },
    {
        $lookup: {
            from: "supplies",
            as: "supply",
            localField: "sid",
            foreignField: "_id"
        }
    },
    {
        $unwind: "$supply"
    },
    {
        $project: {
            supplyType: "$supply.type"
        }
    }
]).forEach(cambio => {
    db.suppliesTimeline.update({_id: cambio._id}, {$set: {typeSupply: cambio.supplyType}})
});





//--query para egresos manuales
db.suppliesTimeline.aggregate(
    {
        $match:{
            quantity:{$lt:0},
            deleted:false,
            lbid:{$eq:""}
        }
    },
    {
        "$lookup": {
            "from": "supplies",
            "localField": "sid",
            "foreignField": "_id",
            "as": "productos"
        }
    },
    {$unwind:"$productos"}
    ,{
        "$lookup": {
            "from": "warehouses",
            "localField": "wid",
            "foreignField": "_id",
            "as": "bodegas"
        }
    },
    {$unwind:"$bodegas"}
    ,{
        "$project": {
            "wid": "$bodegas.name",
            //"tax":"",
            "name": "$productos.name",
            "type":"Egresos",
            "quantity": "$productEquivalence",
            //"invoiceNumber":"",
            "inf":"$information",
            //"invoiceDate":"",
            //"dueDate":"",
            "inputmeasure": "$inputmeasure",
            "rgDate": {$dateToString: {format: "%Y-%m-%d", date: "$rgDate"}},
            "subtotal":"",
            "lbid":"",
            "lbdoce":""
        }
    }
);


//....importar datos en la web







//----otros
//--labores finalizadas que usaron inventario
db.tasks.aggregate(
    {
        $match:{
            status:"Done",
            supplies: {$ne: []}
        }
    }
);

//...obtener ids de labores a cerrar con inventario


//--editar estado de labores cerradas
db.tasks.updateMany(
    {
        status:"Done"
    },
    {
        $set:{
            status:"Doing"
        }
    }

);



//....subir archivos de labores a cerrar


//....obtener labores que no se cerraron
db.tasks.aggregate(

    {
        $match:{
            $or:[
                {_id:ObjectId("5da0b3bb2a0963238d325b17")},
                {_id:ObjectId("5da11d7c9cb5e0776ad99ca1")},
                {_id:ObjectId("5d978f90f9e80633393b442e")},
                {_id:ObjectId("5d979c3df9e80633393b458f")},
                {_id:ObjectId("5da12d861c4a577d5176db5c")},
                {_id:ObjectId("5d97a09df9e80633393b460a")},
                {_id:ObjectId("5d9cdf3451d9af4e0e4b8247")},
                {_id:ObjectId("5da118809cb5e0776ad99c62")},
            ]
        }
    },

    {$unwind:"$supplies"}

    ,{
        "$lookup": {
            "from": "supplies",
            "localField": "supplies._id",
            "foreignField": "_id",
            "as": "producto"
        }
    }
    ,{$unwind:"$producto"}

    ,{
        $project:{
            _id:1,
            cod:1,
            producto:"$producto.name"
        }
    }

)
