
//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
//var bases_de_datos = ["invcamaru"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    // "capacitacion",
    // "finca",
    // "lukeragricola",
    // "invcamaru_testingNoBorrar",

]




//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener formularios
    var data_users = db.getSiblingDB(db_name_aux).users.aggregate();


    data_users.forEach(item_data_users => {


        result_info.push({
            database: db_name_aux,
            user_name: item_data_users.user,
            user_PIN: item_data_users.PIN,
        })

    })


});

//--imprimir resultado
result_info
