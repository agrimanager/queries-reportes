[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },


    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },


    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "info_lote": 0
        }
    }


    , {
        "$lookup": {
            "from": "form_datoslotes",
            "localField": "lote",
            "foreignField": "Lote.features.properties.name",
            "as": "info_lote"
        }
    }

    , { "$unwind": "$info_lote" }

    , {
        "$addFields": {
            "Peso promedio Racimo": "$info_lote.Peso promedio Racimo",
            "Area": "$info_lote.Area",
            "Ratio": { "$divide": ["$info_lote.Peso promedio Racimo", 19] },
            "Merma": "$info_lote.Merma",
            "Clase suelo": { "$concat": ["Clase ", { "$toString": "$info_lote.Clase" }] }
        }
    }

    , {
        "$project": {
            "info_lote": 0
        }
    }



    , {
        "$addFields": {
            "Semana_n12": { "$sum": ["$Semana", 12] },

            "Total merma": {
                "$multiply": [
                    "$Bacotas", { "$divide": ["$Merma", 100] }
                ]
            }
        }
    }


    , {
        "$addFields": {
            "Total racimos cosecha esperada": {
                "$subtract": ["$Bacotas", "$Total merma"]
            }
        }
    }


    , {
        "$addFields": {
            "Total racimos HA": {
                "$divide": ["$Total racimos cosecha esperada", "$Area"]
            }
        }
    }


    , {
        "$addFields": {
            "Kg esperados HA": {
                "$multiply": ["$Total racimos HA", "$Peso promedio Racimo"]
            }
        }
    }


    , {
        "$addFields": {
            "Cajas esperadas": {
                "$multiply": ["$Kg esperados HA", "$Ratio"]
            }
        }
    }

    , {
        "$addFields": {
            "Cajas esperadas HA": {
                "$divide": ["$Cajas esperadas", "$Area"]
            }
        }
    }




]