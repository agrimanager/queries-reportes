//mapa
db.form_embolesebanano.aggregate(

    [

        {
            "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
        },
        {
            "$unwind": "$Cartography.features"
        },

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Cartography.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": [{ "$toObjectId": "$Cartography.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
            }
        },


        {
            "$addFields": {
                //"Cartography": "$Lote"
                "Cartography": "$lote"
            }
        },


        {
            "$addFields": {
                "Bloque": "$Bloque.properties.name",
                "lote": "$lote.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },

        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        }

        //--cruzar con info lote
        , {
            "$lookup": {
                "from": "form_datoslotes",
                "localField": "lote",
                "foreignField": "Lote.features.properties.name",
                "as": "info_lote"
            }
        }

        , { "$unwind": "$info_lote" }

        , {
            "$addFields": {
                "Peso promedio Racimo": "$info_lote.Peso promedio Racimo",
                "Area": "$info_lote.Area",
                "Ratio": { "$divide": ["$info_lote.Peso promedio Racimo", 19] },
                "Merma": "$info_lote.Merma",
                "Clase suelo": { "$concat": ["Clase ", { "$toString": "$info_lote.Clase" }] }
            }
        }

        , {
            "$project": {
                "info_lote": 0
            }
        }


        , {
            "$addFields": {
                "Semana_n12": { "$sum": ["$Semana", 12] },

                "Total merma": {
                    "$multiply": [
                        "$Bacotas", { "$divide": ["$Merma", 100] }
                    ]
                }
            }
        }


        , {
            "$addFields": {
                "Total racimos cosecha esperada": {
                    "$subtract": ["$Bacotas", "$Total merma"]
                }
            }
        }


        , {
            "$addFields": {
                "Total racimos HA": {
                    "$divide": ["$Total racimos cosecha esperada", "$Area"]
                }
            }
        }


        , {
            "$addFields": {
                "Kg esperados HA": {
                    "$multiply": ["$Total racimos HA", "$Peso promedio Racimo"]
                }
            }
        }


        , {
            "$addFields": {
                "Cajas esperadas": {
                    "$multiply": ["$Kg esperados HA", "$Ratio"]
                }
            }
        }

        , {
            "$addFields": {
                "Cajas esperadas HA": {
                    "$divide": ["$Cajas esperadas", "$Area"]
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Semana_n12": "$Semana_n12",
                    "Cartography": "$Cartography"
                    
                    ,"idform": "$idform"
                }
                , "cajas_ha": { "$sum": "$Cajas esperadas HA" }
                // ,"data": {"$push": "$$ROOT"}
                
            }
        }

        //---leyenda
        //Cajas esperadas por Ha:#blanco,[0 - 45):#rojo,[45 - 90):#naranja,[90 - 135):#amarillo,[135 - 180):#Verde Claro,(>=180):#Verde Oscuro
        //Cajas esperadas por Ha:#FFFFFF,[0 - 45):#ff0000,[45 - 90):#ff8000,[90 - 135):#e2ba1f,[135 - 180):#00FF00,(>=180):#008000

        , {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$cajas_ha", 0] }, { "$lt": ["$cajas_ha", 45] }]
                        },
                        "then": "#ff0000",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$cajas_ha", 45] }, { "$lt": ["$cajas_ha", 90] }]
                                },
                                "then": "#ff8000",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$cajas_ha", 90] }, { "$lt": ["$cajas_ha", 135] }]
                                        },
                                        "then": "#e2ba1f",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$cajas_ha", 135] }, { "$lt": ["$cajas_ha", 180] }]
                                                },
                                                "then": "#00FF00",
                                                "else": "#008000"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "rango": {
                    "$cond": {
                        "if": {
                            "$and": [{ "$gte": ["$cajas_ha", 0] }, { "$lt": ["$cajas_ha", 45] }]
                        },
                        "then": "A-[0 - 45)",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gte": ["$cajas_ha", 45] }, { "$lt": ["$cajas_ha", 90] }]
                                },
                                "then": "B-[45 - 90)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gte": ["$cajas_ha", 90] }, { "$lt": ["$cajas_ha", 135] }]
                                        },
                                        "then": "C-[90 - 135)",
                                        "else": {
                                            "$cond": {
                                                "if": {
                                                    "$and": [{ "$gte": ["$cajas_ha", 135] }, { "$lt": ["$cajas_ha", 180] }]
                                                },
                                                "then": "D-[135 - 180)",
                                                "else": "E-(>=180)"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }




        , {
            "$project": {
                "_id": null,
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.lote",
                    //"Semana_n12": "$_id.Semana_n12",
                    "Semana_n12": { "$concat": ["Semana_n12= ", { "$toString": "$_id.Semana_n12" }] },
                    "Rango": "$rango",

                    "color": "$color"
                },
                "geometry": "$_id.Cartography.geometry"

            }
        }



    ]


)