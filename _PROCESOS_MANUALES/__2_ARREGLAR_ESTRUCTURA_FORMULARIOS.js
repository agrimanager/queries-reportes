//ARREGLO ESTRUCTURA DE FORMULARIOS

//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["invcamaru"];

var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar"

    , "localhost"
    , "localhost3"
    , "localhost:3001"

    , "sanjose_2023-07-12"

]



//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);

    //--obtener formularios con la estructura en objeto y no en array
    var data = db.getSiblingDB(db_name_aux)
        .forms
        .aggregate([

            {
                $addFields: {
                    tipo_variable_estructura: { "$type": "$fstructure" }

                }
            },

            {
                $match: {
                    tipo_variable_estructura: "object"
                }
            },

            {
                $addFields: {
                    array_variable_estructura: { "$objectToArray": "$fstructure" }

                }
            }


        ]).allowDiskUse();

    //--🔄 ciclar formularios
    data.forEach(item_data => {

        var array_variable_estructura_new = []

        item_data.array_variable_estructura.forEach(item => {
            array_variable_estructura_new.push(item.v)
        })


        db.getSiblingDB(db_name_aux).forms.update(
            {
                _id: item_data._id
            },
            {
                $set: {
                    "fstructure": array_variable_estructura_new
                }
            }
        )


        result_info.push({
            database: db_name_aux,
            formulario: item_data.name,
            seguimiento: item_data.tracing

        })

    })

    data.close();


});

//--imprimir resultado
result_info
