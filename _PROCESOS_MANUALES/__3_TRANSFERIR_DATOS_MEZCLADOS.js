//----TRANSALADAR DATOS MEZCLADOS ENTRE BASES_DE_DATOS

//----Ojo usar desde db local

//--obtener DBs
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["acacias", "agricolaocoa", "agricolapersea"];

var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar"

    , "localhost"
    , "localhost3"
    , "localhost:3001"

    , "sanjose_2023-07-12"

]

var colecciones_lista_negra = [
    "system.views",

    "tokens",
    "notifications",
    "dbBackups",
    "logs",
    "reports",
    "supplies",
    "suppliesTimeline",
    "syncdata",
    "tasks",
    "tracks"

]

//====== Resultado FInal (para imprimir)
var data_imprimir = [];


//====== Resultado
var db_usuarios = [];

//--🔄 ciclar DBs
bases_de_datos.forEach(db_name => {

    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name))
        return;

    console.log(db_name);

    var datos_usuario = db.getSiblingDB(db_name).users.aggregate(
        [

            {
                $addFields: {
                    _id_str: { $toString: "$_id" }
                }
            },
            {
                $group: {
                    _id: null
                    , _ids: { $push: "$_id" }
                    , _ids_str: { $push: "$_id_str" }
                }
            }

        ]
    );


    datos_usuario.forEach(item => {

        db_usuarios.push({
            database: db_name,
            _ids: item._ids,
            _ids_str: item._ids_str
        })

    });



});

//--imprimir resultado
// db_usuarios

console.log("-------------------------------");

//DB_COLECCIONES
//===============================================================
// console.log(bases_de_datos)//TEST

//====== Resultado
var data_usuarios_mezclados = [];

db_usuarios.forEach(item_usuario => {

    console.log("---->");
    console.log(item_usuario.database);

    //--obtener nombres de colecciones
    var colecciones = db.getSiblingDB(item_usuario.database).getCollectionInfos();

    colecciones.forEach(item_coleccion => {

        if (colecciones_lista_negra.includes(item_coleccion.name))
            return;

        console.log(item_coleccion.name);

        if (item_coleccion.name === "syncdata") {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [

                        {
                            $match: {
                                "syncInfo.uid": {
                                    $not: {
                                        $in: item_usuario._ids_str
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                //_id: null
                                //_id: { "$toObjectId": "$syncInfo.uid" }
                                _id: "$syncInfo.uid"
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        } else {

            var datos_coleccion = db.getSiblingDB(item_usuario.database)
                .getCollection(item_coleccion.name)
                .aggregate(
                    [

                        {
                            $match: {
                                uid: { $exists: true }
                            }
                        },

                        {
                            $match: {
                                uid: {
                                    $not: {
                                        $in: item_usuario._ids
                                    }
                                }
                            }
                        }


                        , {
                            $group: {
                                // _id: null
                                // _id: "$uid"
                                _id: { "$toString": "$uid" }
                                , cantidad: { $sum: 1 }
                                , max_fecha: { $max: "$rgDate" }
                                , min_fecha: { $min: "$rgDate" }
                            }
                        }

                    ]
                );

        }





        datos_coleccion.forEach(item_datos_coleccion => {

            data_usuarios_mezclados.push({
                database: item_usuario.database,
                coleccion: item_coleccion.name,
                uid: item_datos_coleccion._id,
                cantidad: item_datos_coleccion.cantidad,
                max_fecha: item_datos_coleccion.max_fecha,
                min_fecha: item_datos_coleccion.min_fecha
            })

        });


    });

})



//DATOS MEZCLADOS ENTRE DBS Y COLECCIONES
//===============================================================
//====== Resultado
var data_usuarios_mezclados_uids = [];

data_usuarios_mezclados.forEach(item_data_usuarios_mezclados => {

    var database_uid = null

    db_usuarios.forEach(item_usuario => {

        if (item_usuario._ids_str.includes(item_data_usuarios_mezclados.uid)) {
            database_uid = item_usuario.database
            return;
        }


    })

    data_usuarios_mezclados_uids.push({

        coleccion: item_data_usuarios_mezclados.coleccion,
        db_origen: item_data_usuarios_mezclados.database,
        db_destino: database_uid,
        uid_destino: item_data_usuarios_mezclados.uid,
        cantidad_datos: item_data_usuarios_mezclados.cantidad,
        max_fecha: item_data_usuarios_mezclados.max_fecha,
        min_fecha: item_data_usuarios_mezclados.min_fecha

    })


})


// data_usuarios_mezclados_uids




//UPDATE FINAL
//===============================================================


data_usuarios_mezclados_uids.forEach(item_data_usuarios_mezclados_uids => {

    if (item_data_usuarios_mezclados_uids.db_destino === null)
        return


    //==========obtener datos
    var datos_coleccion_mezclados = db.getSiblingDB(item_data_usuarios_mezclados_uids.db_origen)
        .getCollection(item_data_usuarios_mezclados_uids.coleccion)
        .aggregate(
            [

                {
                    $match: {
                        uid: { $exists: true }
                    }
                }


                , {
                    $addFields: {
                        uid_str: { $toString: "$uid" }
                    }
                }



                , {
                    $match: {
                        uid_str: item_data_usuarios_mezclados_uids.uid_destino
                    }
                }

                , {
                    $project: {
                        uid_str: 0
                    }
                }


            ]
        )
        .allowDiskUse();



    //==========insertar datos

    //-----ERROR ----TRY

    //BULK INSERT
    //var bulk_insert = db._instacrops_historico_data2.initializeUnorderedBulkOp();
    var bulk_insert = db.getSiblingDB(item_data_usuarios_mezclados_uids.db_destino)
        .getCollection(item_data_usuarios_mezclados_uids.coleccion)
        .initializeUnorderedBulkOp();

    var datos_coleccion_mezclados_aux1 = datos_coleccion_mezclados.clone()
    datos_coleccion_mezclados_aux1.forEach(i => {

        var filas = db.getSiblingDB(item_data_usuarios_mezclados_uids.db_destino)
            .getCollection(item_data_usuarios_mezclados_uids.coleccion)
            .aggregate(
                [
                    {
                        $match: {
                            "_id": i._id
                        }
                    }
                ]
            )
            .count()

        if (filas === 0) {
            // console.log("insertar")
            bulk_insert.insert(i);

            data_imprimir.push({
                coleccion: item_data_usuarios_mezclados_uids.coleccion,
                db_origen: item_data_usuarios_mezclados_uids.db_origen,
                db_destino: item_data_usuarios_mezclados_uids.db_destino,
                cantidad_datos: filas
            })

        }


    })
    datos_coleccion_mezclados_aux1.close()
    bulk_insert.execute();


    //==========borrar datos
    var datos_coleccion_mezclados_aux2 = datos_coleccion_mezclados.clone()
    datos_coleccion_mezclados_aux2.forEach(i => {
        // console.log("eliminar")
        db.getSiblingDB(item_data_usuarios_mezclados_uids.db_origen)
            .getCollection(item_data_usuarios_mezclados_uids.coleccion)
            .remove(
                {
                    _id: i._id

                }
            )
    })
    datos_coleccion_mezclados_aux2.close()


})


data_imprimir
