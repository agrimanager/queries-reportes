db.form_modulodecosechapalmas.aggregate(
    [

        {
            "$addFields": {
                "variable_cartografia": "$Lotes"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },


        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Lotes": 0
            }
        }




        , { "$addFields": { "fecha": "$Fecha de recoleccion" } },
        { "$addFields": { "anio": { "$year": "$fecha" } } },
        { "$match": { "anio": { "$gt": 2000 } } },
        { "$match": { "anio": { "$lt": 3000 } } },
        { "$addFields": { "mes": { "$month": "$fecha" } } },
        { "$addFields": { "dia": { "$dayOfMonth": "$fecha" } } },

         {
            "$addFields": {
                "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$fecha" } },

                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


        , {
            "$lookup": {
                "from": "form_formulariopuenteparacosecha",
                "as": "info_lote2",
                "let": {
                    "nombre_lote": "$lote"
                    , "anio": "$anio"
                    , "mes": "$mes"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$ne": [{ "$type": "$Lote.features.properties.name" }, "missing"] },
                                    { "$in": ["$$nombre_lote", "$Lote.features.properties.name"] }

                                    , { "$eq": [{ "$toDouble": "$Año" }, "$$anio"] }
                                    , { "$eq": [{ "$toDouble": "$Mes" }, "$$mes"] }
                                ]
                            }
                        }
                    },
                    {
                        "$sort": {
                            "rgDate": -1
                        }
                    },
                    {
                        "$limit": 1
                    }
                ]
            }
        },
        {
            "$unwind": {
                "path": "$info_lote2",
                "preserveNullAndEmptyArrays": true
            }
        },


        {
            "$addFields": {
                "lote_peso_promedio": { "$ifNull": ["$info_lote2.Peso Promedio", 0] }

            }
        }

        , {
            "$project": {
                "info_lote2": 0
            }
        }


        , {
            "$addFields": {
                "num_racimos_enviados": {
                    "$ifNull": ["$Racimos (#)", 0]
                }
            }
        }

        , {
            "$addFields": {
                "peso_promedio_racimos_enviados": {
                    "$multiply": [
                        { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] },
                        { "$ifNull": [{ "$toDouble": "$lote_peso_promedio" }, 0] }
                    ]
                }
            }
        }




        , {
            "$lookup": {
                "from": "form_despachoviajesaplantaextractora",
                "as": "info_despacho",
                "let": {
                    "recoleccion_fecha": "$fecha"
                    , "recoleccion_vagon": "$Numero de Vagon"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    {
                                        "$gte": [
                                            {
                                                "$toDate": {
                                                    "$dateToString": {
                                                        "format": "%Y-%m-%d",
                                                        "date": "$$recoleccion_fecha"
                                                    }
                                                }
                                            }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Inicio Releccion" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$recoleccion_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de Fin de Recoleccion" } } }
                                        ]
                                    }



                                    , { "$eq": [{ "$trim": { "input": { "$toString": "$Vagon" } } }, { "$trim": { "input": { "$toString": "$$recoleccion_vagon" } } }] }
                                ]
                            }
                        }
                    }
                ]
            }
        },
        {
            "$unwind": {

                "path": "$info_despacho",
                "preserveNullAndEmptyArrays": false
            }
        },

        {
            "$addFields": {


                "despacho_peso_kg": { "$ifNull": ["$info_despacho.Peso Vagon Kg", 0] },
                "despacho_numTicket": { "$ifNull": ["$info_despacho.Tiquete", 0] },

                "despacho_numRemision": { "$ifNull": ["$info_despacho.Remision", 0] },
                "despacho_observaciones": { "$ifNull": ["$info_despacho.Observaciones", ""] },
                "despacho_despachador": { "$ifNull": ["$info_despacho.Despachador", ""] },
                "despacho_vehiculo": { "$ifNull": ["$info_despacho.Vehiculo Transportador", ""] }

            }
        }

        , {
            "$project": {
                "info_despacho": 0
            }
        }



        , {
            "$group": {
                "_id": {
                    "lote": "$lote"
                    , "recoleccion_vagon": "$Numero de Vagon"
                    , "ticket": "$despacho_numTicket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_racimos_alzados_lote_viaje": {
                    "$sum": { "$ifNull": [{ "$toDouble": "$num_racimos_enviados" }, 0] }
                }
                ,
                "total_peso_racimos_alzados_lote_viaje": {
                    "$sum": "$peso_promedio_racimos_enviados"
                }
            }
        },

        {
            "$group": {
                "_id": {

                    "recoleccion_vagon": "$_id.recoleccion_vagon"
                    , "ticket": "$_id.ticket"
                },
                "data": {
                    "$push": "$$ROOT"
                },
                "total_peso_racimos_alzados_viaje": {
                    "$sum": "$total_peso_racimos_alzados_lote_viaje"
                }

            }
        },
        {
            "$unwind": "$data"
        },

        {
            "$addFields":
            {
                "total_peso_promedio_racimos_alzados_lote": "$data.total_peso_racimos_alzados_lote_viaje",
                "total_alzados_lote": "$data.total_racimos_alzados_lote_viaje"
            }
        },

        {
            "$unwind": "$data.data"
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Total Peso aproximado Alzados": "$total_peso_racimos_alzados_viaje",
                            "total_alzados_lote": "$total_alzados_lote"
                            , "ticket": "$_id.ticket"
                            , "pct_Alzados x lote": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$total_peso_racimos_alzados_viaje", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$total_peso_promedio_racimos_alzados_lote", "$total_peso_racimos_alzados_viaje"]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }

        , {
            "$addFields":
            {
                "Peso REAL Alzados": {
                    "$multiply": [{ "$ifNull": [{ "$toDouble": "$despacho_peso_kg" }, 0] }, "$pct_Alzados x lote"]
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL lote": {
                    "$cond": {
                        "if": { "$eq": ["$Total Alzados", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [{ "$ifNull": [{ "$toDouble": "$Peso REAL Alzados" }, 0] }, "$total_alzados_lote"]
                        }
                    }
                }
            }
        }

        , {
            "$addFields": {
                "Peso REAL cosecha x registro": {
                    "$multiply": ["$num_racimos_enviados", "$Peso REAL lote"]
                }
            }
        }


        , {
            "$addFields": {
                "pct_Alzados x lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_Alzados x lote", 10000] }, { "$mod": [{ "$multiply": ["$pct_Alzados x lote", 10000] }, 1] }] }, 10000] }
            }
        }
        , {
            "$addFields": {
                "Peso REAL Alzados": { "$divide": [{ "$subtract": [{ "$multiply": ["$Peso REAL Alzados", 100] }, { "$mod": [{ "$multiply": ["$Peso REAL Alzados", 100] }, 1] }] }, 100] }
            }
        }
        , {
            "$addFields": {
                "Peso REAL lote": { "$divide": [{ "$subtract": [{ "$multiply": ["$Peso REAL lote", 100] }, { "$mod": [{ "$multiply": ["$Peso REAL lote", 100] }, 1] }] }, 100] }
            }
        }
        , {
            "$addFields": {
                "Peso REAL cosecha x registro": { "$divide": [{ "$subtract": [{ "$multiply": ["$Peso REAL cosecha x registro", 100] }, { "$mod": [{ "$multiply": ["$Peso REAL cosecha x registro", 100] }, 1] }] }, 100] }
            }
        }




    ]

)
