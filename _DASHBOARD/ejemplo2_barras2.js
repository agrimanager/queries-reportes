//agricolapersea


db.form_monitoreodeplagas.aggregate(
    [

        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2022-03-19T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                        // ,{"$eq": ["$Point.farm", { "$toString": "$FincaID" }]}
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte



        {
            "$addFields": {
                "variable_cartografia": "$ARBOL"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "ARBOL": 0
            }
        }




        //....DASHBOARD


        //----DASHBOARD_Agrupacion

        , {
            "$addFields": {
                //variable1 cualitativa
                "variable_label": "$lote" //🚩editar

                //variable2 cualitativa
                , "variable_dataset": "$PLAGA" //🚩editar
            }
        }


        //paso1
        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$variable_label"
                    , "dashboard_dataset": "$variable_dataset"
                }
                , "dashboard_cantidad": { "$sum": 1 }

                // , "array_dashboard_dataset": { "$addToSet": "$variable_dataset" }
            }
        }

        , {
            "$group": {
                "_id": {
                    //"dashboard_label": "$_id.dashboard_label"
                    "dashboard_dataset": "$_id.dashboard_dataset"
                }
                , "data_group": { "$push": "$$ROOT" }

                // , "array_dashboard_dataset": { "$addToSet": "$_id.dashboard_dataset" }
            }
        }

        , {
            "$sort": {
                "_id.dashboard_dataset": 1
            }
        }


        , {
            "$group": {
                "_id": null
                , "data_group": { "$push": "$$ROOT" }
                //, "array_dashboard_label": { "$push": "$data_group._id.dashboard_label" }
                // , "array_dashboard_label": { "$addToSet": "$data_group._id.dashboard_label" }
                , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
            }
        }

        , { "$unwind": "$data_group" }

        , { "$unwind": "$data_group.data_group" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_group.data_group",
                        {
                            "array_dashboard_dataset": "$array_dashboard_dataset"
                        }
                    ]
                }
            }
        }



        // //paso2
        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$_id.dashboard_label"
                }
                , "data_group": { "$push": "$$ROOT" }

                // , "array_dashboard_labels": { "$push": "$_id.dashboard_label" }
            }
        }
        , {
            "$sort": {
                "_id.dashboard_label": 1
            }
        }

        //----DASHBOARD_Transformacion
        , {
            "$group": {
                "_id": null
                , "dashboard_data": {
                    "$push": "$$ROOT"
                }
            }
        }


        , {
            "$addFields": {
                "data_labels": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
                // , "data_cantidades": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item.dashboard_cantidad" } }
            }
        }


        , {
            "$addFields": {
                "info_datasets": {
                    "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
                }
            }
        }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": "$$item_dataset"
        //             }
        //         }
        //     }
        // }

        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 //"in": "$$item_dataset"
        //                 "in": {
        //                     // "label": "Dataset 1",
        //                     // "backgroundColor": "green",
        //                     // "borderColor": "green",
        //                     // "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]

        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //                 }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 //"in": "$$item_dataset"
        //                 "in": {
        //                     // "label": "Dataset 1",
        //                     // "backgroundColor": "green",
        //                     // "borderColor": "green",
        //                     // "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]

        //                     // "label": "$$item_dataset",
        //                     // "backgroundColor": "green", //editar
        //                     // "borderColor": "#000000",
        //                     // "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]

        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //                     "data": {
        //                         "$map": {
        //                             "input": "$info_datasets",
        //                             "as": "item_dataset",
        //                             "in": {}
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": {
        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //                     "data": {
        //                         "$map": {
        //                             "input": "$dashboard_data",
        //                             "as": "item_dashboard_data",
        //                             //"in": "$$item_dashboard_data.data_group.dashboard_cantidad"
        //                             "in": {
        //                                 "$cond": {
        //                                     "if": {
        //                                         "$eq": [
        //                                             "$$item_dataset"
        //                                             , "$$item_dashboard_data.data_group._id.dashboard_dataset"
        //                                         ]
        //                                     },
        //                                     "then": "si",
        //                                     "else": "no"
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": {
        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data",
        //                     //         "as": "item_dashboard_data",
        //                     //         //"in": "$$item_dashboard_data.data_group.dashboard_cantidad"
        //                     //         // "in": {
        //                     //         //     "$cond": {
        //                     //         //         "if": {
        //                     //         //             "$in": [
        //                     //         //                 "$$item_dataset"
        //                     //         //                 , "$$item_dashboard_data.data_group._id.dashboard_dataset"
        //                     //         //             ]
        //                     //         //         },
        //                     //         //         "then": "$$item_dashboard_data.data_group.dashboard_cantidad",
        //                     //         //         "else": 0
        //                     //         //     }
        //                     //         // }

        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$eq": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$item_dashboard_data.data_group._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.data_group.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }



        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$in": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     // "data": {
        //                     //     "reduce": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "initialValue": "",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$in": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$this._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$this.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     "data": {
        //                         "reduce": {
        //                             "input": "$dashboard_data.data_group",
        //                             "initialValue": [],
        //                             // "in": {
        //                             //     "$cond": {
        //                             //         "if": {
        //                             //             "$eq": [
        //                             //                 "$$item_dataset"
        //                             //                 , "$$value._id.dashboard_dataset"
        //                             //             ]
        //                             //         },
        //                             //         "then": 1,
        //                             //         "else": 0
        //                             //     }
        //                             // }
        //                             "in": "$$this"
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": {
        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data",
        //                     //         "as": "item_dashboard_data",
        //                     //         //"in": "$$item_dashboard_data.data_group.dashboard_cantidad"
        //                     //         // "in": {
        //                     //         //     "$cond": {
        //                     //         //         "if": {
        //                     //         //             "$in": [
        //                     //         //                 "$$item_dataset"
        //                     //         //                 , "$$item_dashboard_data.data_group._id.dashboard_dataset"
        //                     //         //             ]
        //                     //         //         },
        //                     //         //         "then": "$$item_dashboard_data.data_group.dashboard_cantidad",
        //                     //         //         "else": 0
        //                     //         //     }
        //                     //         // }

        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$eq": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$item_dashboard_data.data_group._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.data_group.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }



        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$in": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     // "data": {
        //                     //     "reduce": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "initialValue": "",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$in": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$this._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$this.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     // "data": {
        //                     //     "reduce": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "initialValue": [],
        //                     //         // "in": {
        //                     //         //     "$cond": {
        //                     //         //         "if": {
        //                     //         //             "$eq": [
        //                     //         //                 "$$item_dataset"
        //                     //         //                 , "$$value._id.dashboard_dataset"
        //                     //         //             ]
        //                     //         //         },
        //                     //         //         "then": 1,
        //                     //         //         "else": 0
        //                     //         //     }
        //                     //         // }
        //                     //         "in": "$$this"
        //                     //     }
        //                     // }


        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$eq": [
        //                     //                         "$$item_dataset"
        //                     //                         , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     "data": {
        //                         "$map": {
        //                             "input": "$dashboard_data.data_group",
        //                             "as": "item_dashboard_data",
        //                             "in": {
        //                                 "$cond": {
        //                                     "if": {
        //                                         "$eq": [
        //                                             "$$item_dataset"
        //                                             // , "$$item_dashboard_data._id.dashboard_dataset"
        //                                             , {

        //                                                 "$map": {
        //                                                     "input": "$$item_dashboard_data",
        //                                                     "as": "item_dashboard_data_item",
        //                                                     "in": "pepe"
        //                                                     // "in": {
        //                                                     //     "$cond": {
        //                                                     //         "if": {
        //                                                     //             "$eq": [
        //                                                     //                 "$$item_dataset"
        //                                                     //                 , "$$item_dashboard_data._id.dashboard_dataset"
        //                                                     //             ]
        //                                                     //         },
        //                                                     //         "then": "$$item_dashboard_data.dashboard_cantidad",
        //                                                     //         "else": 0
        //                                                     //     }
        //                                                     // }
        //                                                 }

        //                                             }
        //                                         ]
        //                                     },
        //                                     "then": "$$item_dashboard_data.dashboard_cantidad",
        //                                     "else": 0
        //                                 }
        //                             }
        //                         }
        //                     }




        //                     //--
        //                 }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": {
        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     // "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]

        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$eq": [
        //                     //                         "$$item_dataset"
        //                     //                         // , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                         , {

        //                     //                             "$map": {
        //                     //                                 "input": "$$item_dashboard_data",
        //                     //                                 "as": "item_dashboard_data_item",
        //                     //                                 "in": "pepe"
        //                     //                                 // "in": {
        //                     //                                 //     "$cond": {
        //                     //                                 //         "if": {
        //                     //                                 //             "$eq": [
        //                     //                                 //                 "$$item_dataset"
        //                     //                                 //                 , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                                 //             ]
        //                     //                                 //         },
        //                     //                                 //         "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                                 //         "else": 0
        //                     //                                 //     }
        //                     //                                 // }
        //                     //                             }

        //                     //                         }
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }

        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$eq": [
        //                     //                         "$$item_dataset"
        //                     //                         // , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                         , {

        //                     //                             "$map": {
        //                     //                                 "input": "$$item_dashboard_data",
        //                     //                                 "as": "item_dashboard_data_item",
        //                     //                                 // "in": "pepe"
        //                     //                                 // "in": {
        //                     //                                 //     "$cond": {
        //                     //                                 //         "if": {
        //                     //                                 //             "$eq": [
        //                     //                                 //                 "$$item_dataset"
        //                     //                                 //                 , "item_dashboard_data_item._id.dashboard_dataset"
        //                     //                                 //             ]
        //                     //                                 //         },
        //                     //                                 //         "then": "item_dashboard_data_item.dashboard_cantidad",
        //                     //                                 //         "else": 0
        //                     //                                 //     }
        //                     //                                 // }
        //                     //                                 //"in": "$$item_dashboard_data_item._id.dashboard_dataset"
        //                     //                                 "in": "$$item_dashboard_data_item.dashboard_dataset"
        //                     //                             }

        //                     //                         }
        //                     //                     ]
        //                     //                 },
        //                     //                 "then": "$$item_dashboard_data.dashboard_cantidad",
        //                     //                 "else": 0
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     // "data": {
        //                     //     "$map": {
        //                     //         "input": "$dashboard_data.data_group",
        //                     //         "as": "item_dashboard_data",
        //                     //         "in": {
        //                     //             "$cond": {
        //                     //                 "if": {
        //                     //                     "$not":
        //                     //                     {
        //                     //                         "$in": [
        //                     //                             "$$item_dataset"
        //                     //                             , "$$item_dashboard_data._id.dashboard_dataset"
        //                     //                         ]
        //                     //                     }
        //                     //                 },
        //                     //                 "then": 0,
        //                     //                 "else": {
        //                     //                     "$map": {
        //                     //                         "input": "$$item_dashboard_data",
        //                     //                         "as": "item_dashboard_data_item",
        //                     //                         "in": "$$item_dashboard_data_item.dashboard_cantidad"
        //                     //                     }
        //                     //                 }
        //                     //             }
        //                     //         }
        //                     //     }
        //                     // }


        //                     "data": {
        //                         "$map": {
        //                             "input": "$dashboard_data.data_group",
        //                             "as": "item_dashboard_data",
        //                             "in": {
        //                                 "$cond": {
        //                                     "if": {
        //                                         "$not":
        //                                         {
        //                                             "$in": [
        //                                                 "$$item_dataset"
        //                                                 , "$$item_dashboard_data._id.dashboard_dataset"
        //                                             ]
        //                                         }
        //                                     },
        //                                     "then": 0,
        //                                     "else": {
        //                                         "$map": {
        //                                             "input": "$$item_dashboard_data",
        //                                             "as": "item_dashboard_data_item",
        //                                             "in": "$$item_dashboard_data_item.dashboard_cantidad"
        //                                         }
        //                                     }
        //                                 }
        //                             }
        //                         }
        //                     }






        //                     //--
        //                 }
        //             }
        //         }
        //     }
        // }



        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_dataset",
        //                 "in": {
        //                     "label": "$$item_dataset",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,


        //                     "data": {
        //                         "$map": {
        //                             "input": "$data_labels",
        //                             "as": "item_data_labels",
        //                             // "in": {
        //                             //     "$cond": {
        //                             //         "if": {
        //                             //             "$not":
        //                             //             {
        //                             //                 "$in": [
        //                             //                     "$$item_dataset"
        //                             //                     , "$$item_dashboard_data._id.dashboard_dataset"
        //                             //                 ]
        //                             //             }
        //                             //         },
        //                             //         "then": 0,
        //                             //         "else": {
        //                             //             "$map": {
        //                             //                 "input": "$$item_dashboard_data",
        //                             //                 "as": "item_dashboard_data_item",
        //                             //                 "in": "$$item_dashboard_data_item.dashboard_cantidad"
        //                             //             }
        //                             //         }
        //                             //     }
        //                             // }

        //                             // "in": {
        //                             //     "$cond": {
        //                             //         "if": {
        //                             //             "$and": [
        //                             //                 {


        //                             //                 }
        //                             //             ]
        //                             //         },
        //                             //         "then": 1,
        //                             //         "else": 0
        //                             //     }
        //                             // }


        //                             "in": {
        //                                 "$map": {
        //                                     "input": "$data_labels",
        //                                     "as": "item_data_labels",
        //                                 }


        //                             }
        //                         }






        //                         //--
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }


        // , {
        //     "$addFields": {
        //         "array_data_dataset": {
        //             ///map1 (plagas)
        //             "$map": {
        //                 "input": "$info_datasets",
        //                 "as": "item_info_datasets",
        //                 "in": {
        //                     "label": "$$item_info_datasets",
        //                     "backgroundColor": "green", //editar
        //                     "borderColor": "#000000",
        //                     "borderWidth": 1,
        //                     "data": {
        //                         ///map2 (lotes)
        //                         "$map": {
        //                             "input": "$data_labels",
        //                             "as": "item_data_labels",
        //                             // "in": 123
        //                             "in": {
        //                                 ///map3 (data)
        //                                 "$map": {
        //                                     "input": "$dashboard_data",
        //                                     "as": "item_dashboard_data",
        //                                     "in": {
        //                                         "$and": [
        //                                             { "$eq": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }

        //                                         ]
        //                                     }
        //                                 }
        //                             }
        //                         }






        //                         //--
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }


        , {
            "$addFields": {
                "array_data_dataset": {
                    ///map1 (plagas)
                    "$map": {
                        "input": "$info_datasets",
                        "as": "item_info_datasets",
                        "in": {
                            "label": "$$item_info_datasets",
                            "backgroundColor": "green", //editar
                            "borderColor": "#000000",
                            "borderWidth": 1,
                            "data": {
                                ///map2 (lotes)
                                "$map": {
                                    "input": "$data_labels",
                                    "as": "item_data_labels",
                                    // "in": 123
                                    // "in": {
                                    //     ///map3 (data)
                                    //     "$map": {
                                    //         "input": "$dashboard_data",
                                    //         "as": "item_dashboard_data",

                                    //         // "in": {
                                    //         //     "$and": [
                                    //         //         { "$eq": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }

                                    //         //     ]
                                    //         // }
                                    //         "in": {
                                    //             "$cond": {
                                    //                 "if": {
                                    //                     "$and": [
                                    //                         //{ "$eq": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }
                                    //                         { "$in": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }
                                    //                     ]
                                    //                 },
                                    //                 "then": 1,
                                    //                 "else": 0
                                    //             }
                                    //         }
                                    //     }
                                    // }

                                    // "in": {
                                    //     ///map3 (data)
                                    //     "$map": {
                                    //         "input": "$dashboard_data.data_group",
                                    //         "as": "item_dashboard_data",
                                    //         "in": {
                                    //             "$cond": {
                                    //                 "if": {
                                    //                     "$and": [
                                    //                         //{ "$eq": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }
                                    //                         // { "$eq": ["$$item_info_datasets", "$$item_dashboard_data._id.dashboard_dataset"] }

                                    //                         { "$in": ["$$item_info_datasets", "$$item_dashboard_data._id.dashboard_dataset"] }
                                    //                         ,{ "$in": ["$$item_data_labels", "$$item_dashboard_data._id.dashboard_label"] }
                                    //                     ]
                                    //                 },
                                    //                 "then": "$$item_dashboard_data.dashboard_cantidad",
                                    //                 "else": 0
                                    //             }
                                    //         }
                                    //     }
                                    // }

                                    "in": {
                                        ///map3 (data)
                                        "$map": {
                                            "input": "$dashboard_data.data_group",
                                            "as": "item_dashboard_data",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [
                                                            //{ "$eq": ["$$item_info_datasets", "$$item_dashboard_data.data_group._id.dashboard_dataset"] }
                                                            // { "$eq": ["$$item_info_datasets", "$$item_dashboard_data._id.dashboard_dataset"] }

                                                            // { "$in": ["$$item_info_datasets", "$$item_dashboard_data._id.dashboard_dataset"] }
                                                            // ,{ "$in": ["$$item_data_labels", "$$item_dashboard_data._id.dashboard_label"] }

                                                            { "$eq": ["$$item_info_datasets", "$$item_dashboard_data._id.dashboard_dataset"] }
                                                            ,{ "$eq": ["$$item_data_labels", "$$item_dashboard_data._id.dashboard_label"] }
                                                        ]
                                                    },
                                                    "then": "$$item_dashboard_data.dashboard_cantidad",
                                                    "else": 0
                                                }
                                            }
                                        }
                                    }

                                }






                                //--
                            }
                        }
                    }
                }
            }
        }





        //---old
        // //torta1 (ejemplo3)
        // , {
        //     "$project": {
        //         "data": {
        //             "labels": "$data_labels",
        //             "datasets": [
        //                 {
        //                     "label": "Dataset XXXX",
        //                     "data": "$data_cantidades",
        //                     //"backgroundColor": []
        //                     "backgroundColor": "$array_colores"
        //                 }
        //             ]
        //         }


        //         , "options": {
        //             "responsive": true,
        //             "legend": {
        //                 "position": "top"
        //             },
        //             "title": {
        //                 "display": true,
        //                 "text": "Chart.js Doughnut Chart"
        //             },
        //             "animation": {
        //                 "animateScale": true,
        //                 "animateRotate": true
        //             }
        //         }
        //     }
        // }

        // , { "$project": { "_id": 0 } }









        // //barras (ejemplo1)
        // {
        //   "data": {
        //     "labels": [
        //       "January",
        //       "February",
        //       "March",
        //       "April",
        //       "May",
        //       "June",
        //       "July"
        //     ],
        //     "datasets": [
        //       {
        //         "label": "Dataset 1",
        //         "backgroundColor": "green",
        //         "borderColor": "green",
        //         "borderWidth": 1,
        //         "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //       },
        //       {
        //         "label": "Dataset 2",
        //         "backgroundColor": "blue",
        //         "borderColor": "blue",
        //         "borderWidth": 1,
        //         "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
        //       }
        //     ]
        //   },
        //   "options": {
        //     "title": {
        //       "display": true,
        //       "text": "Chart.js Bar Chart"
        //     }
        //   }
        // }


        // //barras (ejemplo2)
        // {
        //   "data": {
        //      "labels": "$data_labels",
        //     "datasets": [
        //       {
        //         "label": "Dataset 1",
        //         "backgroundColor": "green",
        //         "borderColor": "green",
        //         "borderWidth": 1,
        //         "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
        //       },
        //       {
        //         "label": "Dataset 2",
        //         "backgroundColor": "blue",
        //         "borderColor": "blue",
        //         "borderWidth": 1,
        //         "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
        //       }
        //     ]
        //   },
        //   "options": {
        //     "title": {
        //       "display": true,
        //       "text": "Chart.js Bar Chart"
        //     }
        //   }
        // }







    ]
)
