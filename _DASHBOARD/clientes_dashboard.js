
//----Ojo usar desde db local

//--obtener bases de datos
var bases_de_datos = db.getMongo().getDBNames();
// var bases_de_datos = ["sanjose", "jaremar"];


var bases_de_datos_lista_negra = [
    "admin",
    "config",
    "local",

    "capacitacion",
    "finca",
    "lukeragricola",
    "invcamaru_testingNoBorrar",

]

//====== Resultado
var result_info = [];


//--🔄 ciclar bases de datos
bases_de_datos.forEach(db_name_aux => {


    //condicion de no star en lista negra (bases_de_datos_lista_negra)
    if (bases_de_datos_lista_negra.includes(db_name_aux))
        return;

    console.log(db_name_aux);



    //---Ejecutar queries

    //===================query1
    var data_query = db.getSiblingDB(db_name_aux)
        .getCollection("dashboard")
        .aggregate()
        .count();


    result_info.push({
        database: db_name_aux,
        cantidad_widgets: data_query

    })



});

//--imprimir resultado
result_info
