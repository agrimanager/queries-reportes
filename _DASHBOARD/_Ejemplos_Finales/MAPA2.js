db.form_monitoreodeplagas.aggregate(
    [

        //==================================== QUERY

        //---condiciones
        {
            "$match": {
                "Point.farm": "5d27d22e793a4867b305012c"
            }
        },


        //---cartografia
        {
            "$addFields": {
                "variable_cartografia": "$Palma"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        //🚩editar
        { "$addFields": { "cartografia_id": { "$ifNull": ["$lote._id", "no existe"] } } },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "Point": 0
                , "Palma": 0

                //cartografia_id
            }
        }


        //---color y rango
        , {

            "$group": {
                "_id": {
                    "cartografia_id": "$cartografia_id"
                }
                , "cantidad": { "$sum": 1 }
            }
        }


        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 0] }
                                        , { "$lt": ["$cantidad", 200] }
                                    ]
                                }
                                , "then": "#008000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 200] }
                                        , { "$lt": ["$cantidad", 400] }
                                    ]
                                }
                                , "then": "#ffff00"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 400] }
                                        , { "$lt": ["$cantidad", 600] }
                                    ]
                                }
                                , "then": "#ffa500"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 600] }
                                        , { "$lt": ["$cantidad", 800] }
                                    ]
                                }
                                , "then": "#ff0000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 800] }
                                        , { "$lt": ["$cantidad", 1000] }
                                    ]
                                }
                                , "then": "#ee82ee"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 1000] }
                                    ]
                                }
                                , "then": "#0000ff"
                            }

                        ],
                        "default": "#000000"
                    }
                }

                , "rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 0] }
                                        , { "$lt": ["$cantidad", 200] }
                                    ]
                                }
                                , "then": "0-200"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 200] }
                                        , { "$lt": ["$cantidad", 400] }
                                    ]
                                }
                                , "then": "200-400"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 400] }
                                        , { "$lt": ["$cantidad", 600] }
                                    ]
                                }
                                , "then": "400-600"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 600] }
                                        , { "$lt": ["$cantidad", 800] }
                                    ]
                                }
                                , "then": "600-800"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 800] }
                                        , { "$lt": ["$cantidad", 1000] }
                                    ]
                                }
                                , "then": "800-1000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$cantidad", 1000] }
                                    ]
                                }
                                , "then": ">1000"
                            }

                        ],
                        "default": "otro"
                    }
                }


            }
        }


        //---geometria
        , {
            "$lookup": {
                "from": "cartography",
                "as": "cartografia_data",
                "let": {
                    "cartografia_id": "$_id.cartografia_id",
                    "rango": "$rango",
                    "color": "$color"
                    , "cantidad": "$cantidad"
                },

                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$_id", "$$cartografia_id"] }
                                ]
                            }
                        }
                    }

                    //inyectar variables a propiedades
                    , {
                        "$addFields": {
                            "properties.rango": "$$rango"
                            , "properties.color": "$$color"
                            , "properties.cantidad": "$$cantidad"



                            //legendInteraction
                            //, "properties.aux_subtitleKey": {"$concat": ["Rango: ", "$$rango", "  Cantidad: ",{"$toString":"$$cantidad"}]}
                            , "properties.aux_subtitleKey": { "$concat": ["Cantidad: ", { "$toString": "$$cantidad" }, " ----- Rango: ", "$$rango"] }
                            , "properties.aux_valueKey": { "$concat": ["Lote: ", "$properties.name"] }
                        }
                    }

                    , {
                        "$project": {
                            // "_id": 0,
                            "path": 0,
                            "children": 0
                        }
                    }
                ]
            }
        }

        , { "$unwind": "$cartografia_data" }



        //==================================== MAPA DASHBOARD


        //=======1) sources
        , { "$replaceRoot": { "newRoot": "$cartografia_data" } }

        , {

            "$group": {
                "_id": null
                , "data_sources_features": { "$push": "$$ROOT" }
            }
        }


        , {
            "$addFields": {

                "DATA_MAPA_sources": [
                    {
                        "id": "source-data",
                        "data": {
                            "type": "geojson",
                            "data": {
                                "type": "FeatureCollection",
                                "features": "$data_sources_features" //🚩features
                            }
                        }
                    }
                ]


            }
        }

        , {
            "$project": {
                "_id": 0,
                "data_sources_features": 0
            }
        }


        //=======2) layers
        , {
            "$addFields": {

                "DATA_MAPA_layers": [
                    {
                        "id": "layer-color",
                        "type": "fill",
                        "source": "source-data",
                        "paint": {
                            // "fill-color": [
                            //     "case",
                            //     [
                            //         "==",
                            //         [
                            //             "get",
                            //             "range"
                            //         ],
                            //         "uno"
                            //     ],
                            //     "#81cfbd",
                            //     [
                            //         "==",
                            //         [
                            //             "get",
                            //             "range"
                            //         ],
                            //         "dos"
                            //     ],
                            //     "#fdc06f",
                            //     "#d71122"
                            // ],
                            "fill-color": [
                                "get",
                                "color"
                            ],
                            "fill-opacity": 0.4
                        }
                    },
                    {
                        "id": "layer-outline",
                        "type": "line",
                        "source": "source-data",
                        "paint": {
                            "line-color": "rgba(255, 0, 0, 1)",
                            "line-width": 2
                        }
                    },
                    {
                        "id": "layer-labels",
                        "type": "symbol",
                        "source": "source-data",
                        "layout": {
                            "text-field": [
                                "to-string",
                                [
                                    "get",
                                    "name"
                                ]
                            ],
                            "text-font": [
                                "DIN Offc Pro Medium",
                                "Arial Unicode MS Regular"
                            ],
                            "text-size": 24,
                            "text-anchor": "bottom",
                            "text-offset": [
                                0,
                                -1
                            ]
                        },
                        "paint": {
                            "text-color": "#303030",
                            "text-halo-color": "#ffffff",
                            "text-halo-width": 0.75
                        }
                    }
                ]


            }
        }



        //=======3) filters
        , {
            "$addFields": {

                "DATA_MAPA_filters": [
                    {
                        "id": "layer-color",
                        "filter": [
                            "==",
                            "type",
                            "Polygon"
                        ]
                    }
                ]


            }
        }


        //=======4) legend 🚩EDITAR
        , {
            "$addFields": {

                "DATA_MAPA_legend": [

                    //titulo
                    {
                        "color": "#ffffff",
                        //"value": "TITULO__LEYENDA__XXXX"
                        "value": "TITULO_RANGOS_COLORES"
                    }

                    //colores y rangos
                    , {
                        "color": "#008000",
                        "value": "0-200"
                    }
                    , {
                        "color": "#ffff00",
                        "value": "200-400"
                    }
                    , {
                        "color": "#ffa500",
                        "value": "400-600"
                    }
                    , {
                        "color": "#ff0000",
                        "value": "600-800"
                    }
                    , {
                        "color": "#ee82ee",
                        "value": "800-1000"
                    }
                    , {
                        "color": "#0000ff",
                        "value": ">1000"
                    }
                ]


            }
        }




        //=======5) legendInteraction
        , {
            "$addFields": {

                "DATA_MAPA_legendInteraction": {
                    "layers": [
                        "layer-color"
                    ],
                    //"title": "TITULO__PANEL_INFORMACION__XXXX",
                    "title": "TITULO__PANEL_INFORMACION__XXXX",
                    // "subtitleKey": "name",
                    // "valueKey": "rango",
                    "subtitleKey": "aux_subtitleKey",
                    "valueKey": "aux_valueKey",
                    "placeholder": "Haz hover sobre un lote para mostrar su informacion."
                }


            }
        }



        //-------------------
        //---Proyeccion final mapa_dashboard
        , {
            "$project": {
                "sources": "$DATA_MAPA_sources",
                "layers": "$DATA_MAPA_layers",
                "filters": "$DATA_MAPA_filters",
                "legend": "$DATA_MAPA_legend",
                "legendInteraction": "$DATA_MAPA_legendInteraction"
            }
        }


        , { "$project": { "_id": 0 } }









    ]
)
