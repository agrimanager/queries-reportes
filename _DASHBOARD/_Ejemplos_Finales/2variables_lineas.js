[

    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": {
                "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","]
            }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": {
                "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } }
            }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote_id": { "$toObjectId": "$variable_cartografia.features._id" }
        }
    },



    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Lote": 0
        }
    },



    {
        "$addFields": {
            "total_kg_cosecha": {
                "$sum": [
                    "$Kg cacao premium",
                    "$Kg cacao germinado",
                    "$Kg cacao segunda"
                ]
            }
        }
    },



    {
        "$addFields": {
            "variable_label": "$lote"
        }
    }

    , {
        "$group": {
            "_id": {
                "dashboard_label": "$variable_label",
                "lote_id": "$lote_id"
            },

            "dashboard_cantidad": { "$sum": "$total_kg_cosecha" },
            "Busqueda inicio": { "$min": "$Busqueda inicio" },
            "Busqueda fin": { "$max": "$Busqueda fin" }
        }
    }

    , {
        "$sort": {
            "_id.dashboard_label": 1
        }
    }



    , {
        "$lookup": {
            "from": "form_formulariodesecado",
            "let": {
                "lote": "$_id.lote_id",
                "busqueda_inicio": "$Busqueda inicio",
                "busqueda_fin": "$Busqueda fin"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [

                                {
                                    "$gte": [
                                        "$rgDate",
                                        "$$busqueda_inicio"
                                    ]
                                },
                                {
                                    "$lte": [
                                        "$rgDate",
                                        "$$busqueda_fin"
                                    ]
                                }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "variable_cartografia": "$Lote"
                    }
                },

                { "$unwind": "$variable_cartografia.features" },

                {
                    "$addFields": {
                        "lote_id": { "$toObjectId": "$variable_cartografia.features._id" }
                    }
                },

                {
                    "$match": {
                        "$expr": {
                            "$eq": ["$$lote", "$lote_id"]
                        }
                    }
                },

                {
                    "$project": {
                        "lote_id": "$lote_id",
                        "total_kg_secado": {
                            "$sum": [
                                "$Kg cacao premium",
                                "$Kg cacao germinado",
                                "$Kg cacao segunda"
                            ]
                        }
                    }
                },

                {
                    "$group": {
                        "_id": "$lote_id",
                        "sum_kg": { "$sum": "$total_kg_secado" }
                    }
                }

            ],
            "as": "data_secado"
        }
    },

    {
        "$addFields": {
            "dashboard_cantidad_2": {
                "$ifNull": [
                    { "$arrayElemAt": ["$data_secado.sum_kg", 0] },
                    0
                ]
            }
        }
    }



    , {
        "$group": {
            "_id": null
            , "dashboard_data": {
                "$push": "$$ROOT"
            }
        }
    }


    , {
        "$addFields": {
            "data_labels": {
                "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" }
            }
            , "data_cantidades": {
                "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item.dashboard_cantidad" }
            }
            , "data_cantidades_2": {
                "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item.dashboard_cantidad_2" }
            }
        }
    }



    , {
        "$project": {
            "data": {
                "labels": "$data_labels",
                "datasets": [
                    {
                        "label": "Kg Cosecha x Lote",
                        "stepped": true,
                        "data": "$data_cantidades",
                        "borderColor": "green",
                        "fill": false
                    },
                    {
                        "label": "Kg Secado x Lote",
                        "stepped": true,
                        "data": "$data_cantidades_2",
                        "borderColor": "yellow",
                        "fill": false
                    }
                ]
            },
            "options": {
                "plugins": {
                    "title": {
                        "display": true,
                        "text": "Kg x Lote"
                    }
                }
            }
        }
    }

    , { "$project": { "_id": 0 } }
]
