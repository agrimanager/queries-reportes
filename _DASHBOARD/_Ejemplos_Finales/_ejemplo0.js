[


  {
    "$project":{
      
    "data": {
      "labels": ["green", "red", "orange", "yellow"],
      "datasets": [
        {
          "label": "Dataset 1",
          "data": [57, 36, 76, 40],
          "backgroundColor": ["green", "red", "orange", "yellow"]
        }
      ]
    },
    "options": {
      "responsive": true,
      "legend": {
        "position": "top"
      },
      "title": {
        "display": true,
        "text": "Chart.js Doughnut Chart"
      },
      "animation": {
        "animateScale": true,
        "animateRotate": true
      }
    }



    }
  }

  ,
  {
    "$project":{
      "_id":0
    }
  }


]
