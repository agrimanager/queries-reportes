

/*

el dashboard falla cuando no viene un nuemro sino un texto como:
"-",
"/",
"+",
"*",
"_",
",",
".",
" ",

*/


db.form_plagasyenfermedades.aggregate(

    {
        "$addFields": {
            "data_error": {
                "$filter": {
                    "input": {
                        "$map": {
                            "input": { "$objectToArray": "$$ROOT" },
                            "as": "dataKV",
                            "in": {
                                "$cond": {
                                    "if": {
                                        "$in": [
                                            "$$dataKV.v",
                                            [
                                                "-",
                                                "/",
                                                "+",
                                                "*",
                                                "_",
                                                ",",
                                                ".",
                                                " ",
                                            ]
                                        ]
                                    },
                                    "then": "$$dataKV",
                                    "else": ""
                                }
                            }
                        }
                    },
                    "as": "item",
                    "cond": { "$ne": ["$$item", ""] }
                }
            }
        }
    }

    ,{
        $match:{
            "data_error":{$ne:[]}
        }

    }



    // //---mostrar ids
    // ,{
    //     $project: {
    //         _id: 1
    //     }
    // }

    // , {
    //     "$group": {
    //         "_id": null,
    //         "ids": {
    //             "$push": "$_id"
    //         }
    //     }
    // },
    // {
    //     "$project": {
    //         "_id": 0,
    //         "ids": 1
    //     }
    // }


)
