//*************************** RESUMEN **********
//==============================================

//array de labels,datasets

//**********************************************



//=========================== TORTAS
//==============================================

//torta1
{
   "data":{
      "labels":[
         "green",
         "red",
         "orange",
         "yellow"
      ],
      "datasets":[
         {
            "label":"Dataset 1",
            "data":[
               57,
               36,
               76,
               40
            ],
            "backgroundColor":[
               "green",
               "red",
               "orange",
               "yellow"
            ]
         }
      ]
   },
   "options":{

   }
}


//torta2
{
   "data":{
      "labels":[
         "green",
         "red",
         "orange",
         "yellow"
      ],
      "datasets":[
         {
            "label":"Dataset 1",
            "data":[
               57,
               36,
               76,
               40
            ],
            "backgroundColor":[
               "green",
               "red",
               "orange",
               "yellow"
            ]
         }
      ]
   },
   "options":{
      "responsive":true,
      "legend":{
         "position":"top"
      },
      "title":{
         "display":true,
         "text":"Chart.js Doughnut Chart"
      },
      "animation":{
         "animateScale":true,
         "animateRotate":true
      }
   }
}

//=========================== BARRAS
//==============================================


//barras_vertical
{
  "data": {
    "labels": [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July"
    ],
    "datasets": [
      {
        "label": "Dataset 1",
        "backgroundColor": "green",
        "borderColor": "green",
        "borderWidth": 1,
        "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
      },
      {
        "label": "Dataset 2",
        "backgroundColor": "blue",
        "borderColor": "blue",
        "borderWidth": 1,
        "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
      }
    ]
  },
  "options": {
    "title": {
      "display": true,
      "text": "Chart.js Bar Chart"
    }
  }
}

//barras_horizontal
{
   "data":{
      "labels":[
         "January",
         "February",
         "March",
         "April",
         "May",
         "June",
         "July"
      ],
      "datasets":[
         {
            "label":"Dataset 1",
            "backgroundColor":"green",
            "borderColor":"green",
            "borderWidth":1,
            "data":[
               74,
               23,
               56,
               35,
               38,
               -66,
               96,
               47,
               33,
               -60,
               -67,
               38
            ]
         },
         {
            "label":"Dataset 2",
            "backgroundColor":"blue",
            "borderColor":"blue",
            "borderWidth":1,
            "data":[
               50,
               -20,
               31,
               -90,
               71,
               96,
               2,
               89,
               -85,
               10,
               -82,
               -26
            ]
         }
      ]
   },
   "options":{
      "title":{
         "display":true,
         "text":"Chart.js Bar Chart"
      }
   }
}

//mapa de calor
{
   "data":{
      "datasets":[
         {
            "label":"Sample with labels",
            "tree":[
               {
                  "value":100,
                  "title":"Alpha"
               },
               {
                  "value":20,
                  "title":"Beta"
               },
               {
                  "value":5,
                  "title":"Gamma"
               }
            ],
            "key":"value",
            "groups":[
               "title"
            ],
            "fontColor":"white",
            "fontFamily":"Work Sans",
            "fontSize":16,
            "backgroundColor":"#800080",
            "spacing":0.1,
            "borderWidth":2,
            "borderColor":"rgba(180,180,180, 0.15)"
         }
      ]
   },
   "options":{
      "maintainAspectRatio":false,
      "title":{
         "display":true,
         "text":"Basic treemap sample with labels"
      },
      "legend":{
         "display":false
      },
      "tooltips":{

      }
   }
}


//=========================== LINEAS
//==============================================

//lineas_simple
{
   "data":{
      "xLabels":[
         "January",
         "February",
         "March",
         "April",
         "May",
         "June",
         "July"
      ],
      "yLabels":[
         "",
         "Request Added",
         "Request Viewed",
         "Request Accepted",
         "Request Solved",
         "Solving Confirmed"
      ],
      "datasets":[
         {
            "label":"My First dataset",
            "data":[
               "",
               "Request Added",
               "Request Added",
               "Request Added",
               "Request Viewed",
               "Request Viewed",
               "Request Viewed"
            ],
            "fill":false,
            "borderColor":"green",
            "backgroundColor":"green"
         }
      ]
   },
   "options":{
      "scales":{
         "xAxes":[
            {
               "display":true,
               "scaleLabel":{
                  "display":true,
                  "labelString":"Month"
               }
            }
         ],
         "yAxes":[
            {
               "type":"category",
               "position":"left",
               "display":true,
               "scaleLabel":{
                  "display":true,
                  "labelString":"Request State"
               },
               "ticks":{
                  "reverse":true
               }
            }
         ]
      }
   }
}

//lineas_dobles
{
   "data":{
      "labels":[
         "January",
         "February",
         "March",
         "April",
         "May",
         "June",
         "July"
      ],
      "datasets":[
         {
            "label":"My First dataset",
            "borderColor":"green",
            "backgroundColor":"green",
            "fill":false,
            "data":[
               91,
               57,
               -7,
               65,
               99,
               90,
               49,
               -20,
               -13,
               -72,
               -18,
               -33
            ],
            "yAxisID":"y"
         },
         {
            "label":"My Second dataset",
            "borderColor":"blue",
            "backgroundColor":"blue",
            "fill":false,
            "data":[
               52,
               -55,
               -7,
               43,
               -19,
               -29,
               40,
               -18,
               9,
               -76,
               33,
               -8
            ],
            "yAxisID":"y1"
         }
      ]
   },
   "options":{
      "interaction":{
         "mode":"index"
      },
      "plugins":{
         "title":{
            "display":true,
            "text":"Chart.js Line Chart - Multi Axis"
         }
      },
      "scales":{
         "yAxes":[
            {
               "type":"linear",
               "display":true,
               "id":"y",
               "position":"left"
            },
            {
               "id":"y1",
               "type":"linear",
               "display":true,
               "position":"right",
               "gridLines":{
                  "drawOnChartArea":false
               }
            }
         ]
      }
   }
}






//----OLD
@@ -1,76 +0,0 @@
// dashboard 1 variable
//TORTA
{
 "data": {
   "labels": ["green", "red", "orange", "yellow"],
   "datasets": [
     {
       "label": "Dataset 1",
       "data": [57, 36, 76, 40],
       "backgroundColor": ["green", "red", "orange", "yellow"]
     }
   ]
 },
 "options": {}
}



//-----------------------------
// dashboard >=2 variable
//histograma multiple

{
 "data": {
   "labels": [
     "January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July"
     ,"x1"
     ,"x2"
     ,"x3"
     ,"x4"
     ,"x5"
     ,"x6"//---pasa el limite
   ],
   "datasets": [
     {
       "label": "Dataset 1",
       "backgroundColor": "green",
       "borderColor": "green",
       "borderWidth": 1,
       "data": [74, 23, 56, 35, 38, -66, 96, 47, 33, -60, -67, 38]
     },
     {
       "label": "Dataset 2",
       "backgroundColor": "blue",
       "borderColor": "blue",
       "borderWidth": 1,
       "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
     }
     ,
     {
       "label": "Dataset 3",
       "backgroundColor": "#0044FF",
       "borderColor": "004455",
       "borderWidth": 1,
       "data": [50, -20, 31, -90, 71, 96, 2, 89, -85, 10, -82, -26]
     }
   ]
 },
 "options": {
   "title": {
     "display": true,
     "text": "Chart.js Bar Chart"
   }
 }
}




//--------------------------------
