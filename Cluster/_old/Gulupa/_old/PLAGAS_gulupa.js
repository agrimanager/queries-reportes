db.form_monitoreoplagasgulupa2.aggregate(
    [

        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        //---aveces el bloque queda como lote y viceversa
        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                , "feature_4": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
                , "feature_5": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        //--finca
        {
            "$addFields": {
                "finca": "$feature_1"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },

        //--bloque
        {
            "$addFields": {
                "bloque": "$feature_2.properties.name"
            }
        },

        //--lote
        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_3.properties.type", "lot"] },
                        "then": "$feature_3.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_4.properties.type", "lot"] },
                                "then": "$feature_4.properties.name",
                                "else": "--sin lote--"
                            }
                        }
                    }
                }
            }
        },

        //--linea
        {
            "$addFields": {
                "linea": {
                    "$cond": {
                        "if": { "$eq": ["$feature_3.properties.type", "lines"] },
                        "then": "$feature_3.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_4.properties.type", "lines"] },
                                "then": "$feature_4.properties.name",
                                "else": "--sin linea--"
                            }
                        }
                    }
                }
            }
        },


        //--arbol
        {
            "$addFields": {
                "arbol": {
                    "$cond": {
                        "if": { "$eq": ["$feature_4.properties.type", "trees"] },
                        "then": "$feature_4.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_5.properties.type", "trees"] },
                                "then": "$feature_5.properties.name",
                                "else": "--sin arbol--"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0,
                "feature_4": 0,
                "feature_5": 0
            }
        }



        //--ELEMENTO (FEATURE X EL CUAL AGRUPAR)
        , {
            "$addFields": {
                "ELEMENTO_CARTOGRAFICO": {
                    "$cond": {
                        "if": { "$eq": ["$arbol", "--sin arbol--"] },
                        "then": "$linea",
                        "else": "$arbol"
                    }
                }
            }
        }

        
        
        //---agrupacion

        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "plaga": "$Plaga Evaluada"

                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque_x_plaga": { "$sum": 1 }
            }
        }
        
        ,{
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque"
                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque": { "$sum": "$elementos_censados_x_bloque_x_plaga" }
            }
        }



        ,{ "$unwind": "$data" }
        ,{ "$unwind": "$data.data" }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "elementos_censados_x_bloque_x_plaga": "$data.elementos_censados_x_bloque_x_plaga",
                            "elementos_censados_x_bloque": "$elementos_censados_x_bloque"
                        }
                    ]
                }
            }
        }






    ]

)