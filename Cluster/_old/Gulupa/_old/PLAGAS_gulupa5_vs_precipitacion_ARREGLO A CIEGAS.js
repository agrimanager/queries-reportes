     [
      
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

      
        {
            "$addFields": {
                "feature_1": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "feature_2": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "feature_3": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
                , "feature_4": { "$arrayElemAt": ["$objetos_del_cultivo", 3] }
                , "feature_5": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

    
        {
            "$addFields": {
                "finca": "$feature_1"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },

    
        {
            "$addFields": {
                "bloque": "$feature_2.properties.name"
            }
        },

    
        {
            "$addFields": {
                "lote": {
                    "$cond": {
                        "if": { "$eq": ["$feature_3.properties.type", "lot"] },
                        "then": "$feature_3.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_4.properties.type", "lot"] },
                                "then": "$feature_4.properties.name",
                                "else": "--sin lote--"
                            }
                        }
                    }
                }
            }
        },

    
        {
            "$addFields": {
                "linea": {
                    "$cond": {
                        "if": { "$eq": ["$feature_3.properties.type", "lines"] },
                        "then": "$feature_3.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_4.properties.type", "lines"] },
                                "then": "$feature_4.properties.name",
                                "else": "--sin linea--"
                            }
                        }
                    }
                }
            }
        },


    
        {
            "$addFields": {
                "arbol": {
                    "$cond": {
                        "if": { "$eq": ["$feature_4.properties.type", "trees"] },
                        "then": "$feature_4.properties.name",
                        "else": {
                            "$cond": {
                                "if": { "$eq": ["$feature_5.properties.type", "trees"] },
                                "then": "$feature_5.properties.name",
                                "else": "--sin arbol--"
                            }
                        }
                    }
                }
            }
        },


        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0,

                "feature_1": 0,
                "feature_2": 0,
                "feature_3": 0,
                "feature_4": 0,
                "feature_5": 0
            }
        }



   
        , {
            "$addFields": {
                "ELEMENTO_CARTOGRAFICO": {
                    "$cond": {
                        "if": { "$eq": ["$arbol", "--sin arbol--"] },
                        "then": "$linea",
                        "else": "$arbol"
                    }
                }
            }
        }



        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "enfermedad": "$Plaga Evaluada"

                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque_x_plaga": { "$sum": 1 }
            }
        }
        
        ,{
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque"
                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque": { "$sum": "$elementos_censados_x_bloque_x_plaga" }
            }
        }



        ,{ "$unwind": "$data" }
        ,{ "$unwind": "$data.data" }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "elementos_censados_x_bloque_x_plaga": "$data.elementos_censados_x_bloque_x_plaga",
                            "elementos_censados_x_bloque": "$elementos_censados_x_bloque"
                        }
                    ]
                }
            }
        },
        
        
        
         {
            "$addFields": {
                "PTC_incidencia": {
                    "$multiply": [
                        { "$divide": ["$elementos_censados_x_bloque_x_plaga", "$elementos_censados_x_bloque"] }
                        , 100]
                }
            }
        }
        
        
        
        
         ,{
            "$project": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",

            

                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",
                "rgDate": "$rgDate",
                "tipo": "Enfermedad",
                "nombre_sanidad": "$Plaga Evaluada",
                "pct_incidencia": "$PTC_incidencia"

            }
        }


       
        , {
            "$group": {
                "_id": {
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin"
                }
                , "data1": { "$push": "$$ROOT" }
            }
        },
        
        
           
        
        {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data1"
                        
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        },
        
        
        
        
         
        {
            "$lookup": {
                "from": "form_registrodeprecipitacion",
                "as": "data2",
                "let": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "num_mes": "$num_mes",
                    "num_anio": "$num_anio"
                },
                "pipeline": [
                    { "$match": { "Fecha Medicion": { "$ne": "" } } },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha Medicion" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Finca.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Finca.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },
                    {
                        "$addFields": {
                            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "BLOQUE": { "$arrayElemAt": ["$objetos_del_cultivo", 1] }
                        }
                    },
                    {
                        "$addFields": {
                            "BLOQUE": "$BLOQUE.properties.name"
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },
                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$Fecha Medicion" } },
                            "num_mes": { "$month": { "date": "$Fecha Medicion" } }
                        }
                    }


                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$finca"] },
                                    { "$eq": ["$$bloque", "$BLOQUE"] }
                                    
                                    ,{ "$eq": ["$$num_anio", "$num_anio"] }
                                    ,{ "$eq": ["$$num_mes", "$num_mes"] }

                                ]
                            }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "bloque": "$BLOQUE"
                                
                                ,"num_anio":"$num_anio"
                                ,"num_mes":"$num_mes"
                            }
                            , "Precipitacion_Acomulada": { "$sum": "$Precipitacion" }

                        }

                    }



                ]

            }
        },
        { "$unwind": "$data2" },

        {
            "$addFields": {
                "Precipitacion_Acomulada": "$data2.Precipitacion_Acomulada"
            }
        }

        , {
            "$project": {
                "data2": 0
            }
        },

        {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }


    ]