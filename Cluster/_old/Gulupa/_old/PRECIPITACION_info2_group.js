db.form_registrodeprecipitacion.aggregate(

    {
        "$project": {
            "Fecha Medicion": "$Fecha Medicion",
            "Precipitacion": "$Precipitacion",
            "Finca": "$Finca"
        }
    },


    //======FECHAS

    { "$match": { "Fecha Medicion": { "$ne": "" } } },

    //---fechas variables
    {
        "$addFields": {
            "num_anio": { "$year": { "date": "$Fecha Medicion" } },
            "num_mes": { "$month": { "date": "$Fecha Medicion" } }
        }
    },
    { "$match": { "num_anio": { "$gt": 2000 } } },
    { "$match": { "num_anio": { "$lt": 3000 } } },

    //---fechas condiciones
    // {
    //     "$match": {
    //         "$expr": {
    //             "$and": [
    //                 { "$eq": ["$$num_mes", "$num_mes"] },
    //                 { "$eq": ["$$num_anio", "$num_anio"] }

    //             ]
    //         }
    //     }
    // },




    //=====CARTOGRAFIA
    //---query nuevo de cartografia

    //--paso1 (cartografia-nombre variable y ids)
    {
        "$addFields": {
            "variable_cartografia": "$Finca" //🚩editar
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    //---borrar variable
    {
        "$project": {
            "Finca": 0
        }
    },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    //--paso2 (cartografia-cruzar informacion)
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    // //--paso3 (cartografia-obtener informacion)

    //--finca
    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    //--bloque
    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0
        }
    }



    // //----condiciones de lookup
    // , {
    //     "$match": {
    //         "$expr": {
    //             "$and": [
    //  "      "         { "$eq": ["$$finca", "$finca"] },
    //                 { "$eq": ["$$bloque", "$bloque"] }

    //             ]
    //         }
    //     }
    // }

    //----TEST
    , {
        $match: {
            "bloque": { "$ne": "no existe" }
        }
    }

    // , {
    //     "$group": {
    //         "_id": {
    //             "finca": "$finca",
    //             "bloque": "$bloque",

    //             "num_anio": "$num_anio",
    //             "num_mes": "$num_mes"
    //         }
    //         , "Precipitacion_Acomulada": { "$sum": "$Precipitacion" }

    //     }

    // }


)