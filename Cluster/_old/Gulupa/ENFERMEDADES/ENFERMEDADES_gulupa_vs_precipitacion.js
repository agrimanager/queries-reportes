db.form_monitoreoenfermedadesgulupa.aggregate(
    [


        //======CARTOGRAFIA
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        //--ID
        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },



        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0
            }
        }


        //======INCIDENCIA
        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "bloque_id": "$bloque_id",
                    "num_anio": "$num_anio",
                    "num_mes": "$num_mes",
                    "enfermedad": "$Enfermedad Evaluada Gulupa"

                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque_x_enfermedad": { "$sum": 1 }
            }
        }

        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "bloque_id": "$_id.bloque_id",
                    "num_anio": "$_id.num_anio",
                    "num_mes": "$_id.num_mes"
                    // "enfermedad": "$Enfermedad Evaluada Gulupa"
                }
                , "data": { "$push": "$$ROOT" }
                , "elementos_censados_x_bloque": { "$sum": "$elementos_censados_x_bloque_x_enfermedad" }
            }
        }



        , { "$unwind": "$data" }
        // , { "$unwind": "$data.data" }



        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data._id",
                        {
                            "elementos_censados_x_bloque_x_enfermedad": "$data.elementos_censados_x_bloque_x_enfermedad",
                            "elementos_censados_x_bloque": "$elementos_censados_x_bloque"
                        }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "PTC_incidencia": {
                    "$multiply": [
                        { "$divide": ["$elementos_censados_x_bloque_x_enfermedad", "$elementos_censados_x_bloque"] }
                        , 100]
                }
            }
        }



        //======PRECIPITACION
        , {
            "$lookup": {
                "from": "form_registrodeprecipitacion",
                "as": "precipitacion",
                "let": {
                    // "finca": "$finca",
                    // "bloque": "$bloque",
                    "bloque_id": "$bloque_id",
                    "num_mes": "$num_mes",
                    "num_anio": "$num_anio"
                },
                "pipeline": [

                    {
                        "$project": {
                            "Fecha Medicion": "$Fecha Medicion",
                            "Precipitacion": "$Precipitacion",
                            "Finca": "$Finca"
                        }
                    },

                    //cartografia
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$in": ["$$bloque_id", "$Finca.features._id"] }
                                ]
                            }
                        }
                    },


                    //fechas
                    { "$match": { "Fecha Medicion": { "$ne": "" } } },


                    {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$Fecha Medicion" } },
                            "num_mes": { "$month": { "date": "$Fecha Medicion" } }
                        }
                    },
                    { "$match": { "num_anio": { "$gt": 2000 } } },
                    { "$match": { "num_anio": { "$lt": 3000 } } },


                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$num_mes", "$num_mes"] },
                                    { "$eq": ["$$num_anio", "$num_anio"] }

                                ]
                            }
                        }
                    }



                    , {
                        "$group": {
                            "_id": {
                                "bloque_id": "$$bloque_id"
                            }
                            , "Precipitacion_Acomulada": { "$sum": "$Precipitacion" }
                        }

                    }

                ]

            }
        },
        // { "$unwind": "$data2" }
        {
            "$unwind": {
                "path": "$precipitacion",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "precipitacion": { "$ifNull": ["$precipitacion.Precipitacion_Acomulada", 0] } } },


    ]
)