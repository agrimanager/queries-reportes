db.form_monitoreoenfermedadesaguacate.aggregate(
    [
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },
        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },
        {
            "$group": {
                "_id": {
                    "Enfermedades Aguacate": "$Enfermedades Aguacate",
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }
        , {
            "$group": {
                "_id": {
                    "Enfermedades Aguacate": "$_id.Enfermedades Aguacate",
                    "Lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_afectados_x_lote_x_enfermedad": { "$sum": 1 }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_afectados_x_lote_x_enfermedad": "$Arboles_afectados_x_lote_x_enfermedad"
                        }
                    ]
                }
            }
        }
        , {
            "$group": {
                "_id": {
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }
        , {
            "$group": {
                "_id": {
                    "lote": "$_id.Lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_evaluados_x_lote": { "$sum": 1 }
            }
        }
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_evaluados_x_lote": "$Arboles_evaluados_x_lote"
                        }
                    ]
                }
            }
        }
        , {
            "$addFields": {
                "PTC_incidencia": {
                    "$multiply": [
                        { "$divide": ["$Arboles_afectados_x_lote_x_enfermedad", "$Arboles_evaluados_x_lote"] }
                        , 100]
                }
            }
        }
    ]

)
