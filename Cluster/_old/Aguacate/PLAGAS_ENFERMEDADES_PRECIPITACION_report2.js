db.form_monitoreoenfermedadesaguacate.aggregate(

    [


        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------


        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"
            }
        },

        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },

        {
            "$addFields": {
                "finca": "$finca.name"
            }
        },
        { "$unwind": "$finca" },



        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },
        {
            "$group": {
                "_id": {
                    "Enfermedades Aguacate": "$Enfermedades Aguacate",
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Enfermedades Aguacate": "$_id.Enfermedades Aguacate",
                    "Lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_afectados": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_afectados": "$Arboles_afectados"
                        }
                    ]
                }
            }
        }

        , {
            "$group": {
                "_id": {
                    "Lote": "$lote",
                    "Arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "lote": "$_id.lote"
                }
                , "data": { "$push": "$$ROOT" }
                , "Arboles_evaluados_x_lote": { "$sum": 1 }
            }
        }

        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        {
                            "Arboles_evaluados_x_lote": "$Arboles_evaluados_x_lote"
                        }
                    ]
                }
            }
        }



        , {
            "$addFields": {
                "PTC_incidencia": {
                    "$multiply": [
                        { "$divide": ["$Arboles_afectados", "$Arboles_evaluados_x_lote"] }
                        , 100]
                }
            }
        },




        {
            "$project": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "linea": "$linea",
                "arbol": "$arbol",

                // "filtro_fecha_inicio": "$$filtro_fecha_inicio",
                // "filtro_fecha_fin": "$$filtro_fecha_fin",

                "Busqueda inicio": "$Busqueda inicio",
                "Busqueda fin": "$Busqueda fin",
                "rgDate": "$rgDate",
                "tipo": "Enfermedad",
                "nombre_sanidad": "$Enfermedades Aguacate",
                "pct_incidencia": "$PTC_incidencia"

            }
        }


        //---group
        , {
            "$group": {
                "_id": {
                    "Busqueda inicio": "$Busqueda inicio",
                    "Busqueda fin": "$Busqueda fin"
                }
                , "data1": { "$push": "$$ROOT" }
            }
        }




        , {
            "$lookup": {
                "from": "form_monitoreoplagasaguacate3",
                "as": "data2",
                "let": {
                    "filtro_fecha_inicio": "$_id.Busqueda inicio",
                    "filtro_fecha_fin": "$_id.Busqueda fin"
                },
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                            "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                            "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
                        }
                    },

                    {
                        "$addFields": {
                            "bloque": "$bloque.properties.name",
                            "lote": "$lote.properties.name",
                            "linea": "$linea.properties.name",
                            "arbol": "$arbol.properties.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },

                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },


                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    },
                    {
                        "$addFields": {
                            "Fruto": { "$toDouble": "$Fruto" },
                            "Hoja": { "$toDouble": "$Hoja" },
                            "Flores": { "$toDouble": "$Flores" },
                            "Rama Floral": { "$toDouble": "$Rama Floral" },
                            "Rama": { "$toDouble": "$Rama" },
                            "Suelo": { "$toDouble": "$Suelo" }

                        }
                    },

                    {
                        "$group": {
                            "_id": {
                                "Plaga Evaluada Aguacate": "$Plaga Evaluada Aguacate",
                                "Lote": "$lote",
                                "Arbol": "$arbol"
                            }
                            , "data": { "$push": "$$ROOT" }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "Plaga Evaluada Aguacate": "$_id.Plaga Evaluada Aguacate",
                                "Lote": "$_id.lote"
                            }
                            , "data": { "$push": "$$ROOT" }
                            , "Arboles_Afectactos_x_Plaga": { "$sum": 1 }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "Arboles_Afectactos_x_Plaga": "$Arboles_Afectactos_x_Plaga"
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "Lote": "$lote",
                                "Arbol": "$arbol"
                            }
                            , "data": { "$push": "$$ROOT" }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "lote": "$_id.lote"
                            }
                            , "data": { "$push": "$$ROOT" }
                            , "Total_Arboles_Censados_X_Lote": { "$sum": 1 }
                        }
                    }

                    , { "$unwind": "$data" }
                    , { "$unwind": "$data.data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data.data",
                                    {
                                        "Total_Arboles_Censados_X_Lote": "$Total_Arboles_Censados_X_Lote"
                                    }
                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "PTC_incidencia_plagas": {
                                "$multiply": [
                                    { "$divide": ["$Arboles_Afectactos_x_Plaga", "$Total_Arboles_Censados_X_Lote"] }
                                    , 100]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "suma_Individuos_x_Plaga": {
                                "$sum": ["$Fruto", "$Hoja", "$Flores", "$Rama Floral", "$Rama", "$Suelo"]
                            }
                        }

                    },

                    {
                        "$addFields": {
                            "Total_Severidad": {
                                "$divide": ["$suma_Individuos_x_Plaga", "$Arboles_Afectactos_x_Plaga"]
                            }
                        }

                    },



                    {
                        "$project": {
                            "finca": "$finca",
                            "bloque": "$bloque",
                            "lote": "$lote",
                            "linea": "$linea",
                            "arbol": "$arbol",


                            "Busqueda inicio": "$$filtro_fecha_inicio",
                            "Busqueda fin": "$$filtro_fecha_fin",

                            "rgDate": "$rgDate",
                            "tipo": "Plagas",
                            "nombre_sanidad": "$Plaga Evaluada Aguacate",
                            "pct_incidencia": "$PTC_incidencia_plagas"

                        }
                    }


                ]

            }
        },




        {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data1",
                        "$data2"
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

        , {
            "$addFields": {
                "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$rgDate" } }
            }
        }



        , {
            "$lookup": {
                "from": "form_registrodeprecipitacion",
                "as": "data3",
                "let": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [
                    { "$match": { "Fecha Medicion": { "$ne": "" } } },
                    { "$addFields": { "anio_filtro": { "$year": "$Fecha Medicion" } } },
                    { "$match": { "anio_filtro": { "$gt": 2000 } } },
                    { "$match": { "anio_filtro": { "$lt": 3000 } } },


                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Medicion" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Medicion" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "split_path": { "$split": [{ "$trim": { "input": "$Finca.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "features_oid": { "$map": { "input": "$Finca.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_oid",
                                    "$features_oid"
                                ]
                            }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },
                    {
                        "$addFields": {
                            "finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                            "BLOQUE": { "$arrayElemAt": ["$objetos_del_cultivo", 1] }
                        }
                    },
                    {
                        "$addFields": {
                            "BLOQUE": "$BLOQUE.properties.name"
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    {
                        "$addFields": {
                            "finca": "$finca.name"
                        }
                    },
                    { "$unwind": "$finca" },
                    {
                        "$project": {
                            "split_path": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "features_oid": 0
                        }
                    }



                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$Fecha Medicion" } },
                            "num_mes": { "$month": { "date": "$Fecha Medicion" } }
                        }
                    }


                    , {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$eq": ["$$finca", "$finca"] },
                                    { "$eq": ["$$bloque", "$BLOQUE"] }

                                ]
                            }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca",
                                "bloque": "$BLOQUE"
                            }
                            , "Precipitacion_Acomulada": { "$sum": "$Precipitacion" }

                        }

                    }



                ]

            }
        },
        { "$unwind": "$data3" },

        {
            "$addFields": {
                "Precipitacion_Acomulada": "$data3.Precipitacion_Acomulada"
            }
        }

        , {
            "$project": {
                "data3": 0
            }
        },

        {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }
            }
        }











    ]

)