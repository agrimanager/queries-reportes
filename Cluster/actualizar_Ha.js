

var cursor = [
    // //aurora
    // { _id: ObjectId("61fc0001113e381bd233cb77"), Ha: 101.15 },
    // { _id: ObjectId("61fc0002113e381bd233cb7b"), Ha: 1.47 },
    // { _id: ObjectId("61fc0002113e381bd233cb8d"), Ha: 4.19 },
    // { _id: ObjectId("61fc0002113e381bd233cb79"), Ha: 3.58 },
    // { _id: ObjectId("61fc0002113e381bd233cb8a"), Ha: 2.52 },
    // { _id: ObjectId("61fc0002113e381bd233cb7e"), Ha: 3.83 },
    // { _id: ObjectId("61fc0002113e381bd233cb88"), Ha: 1.51 },
    // { _id: ObjectId("61fc0002113e381bd233cb80"), Ha: 0.70 },
    // { _id: ObjectId("61fc0002113e381bd233cb87"), Ha: 0.92 },
    // { _id: ObjectId("61fc0002113e381bd233cb89"), Ha: 2.00 },
    // { _id: ObjectId("61fc0002113e381bd233cb81"), Ha: 2.08 },
    // { _id: ObjectId("61fc0002113e381bd233cb7a"), Ha: 1.72 },
    // { _id: ObjectId("61fc0002113e381bd233cb7c"), Ha: 2.40 },
    // { _id: ObjectId("61fc0002113e381bd233cb7d"), Ha: 6.57 },
    // { _id: ObjectId("61fc0002113e381bd233cb78"), Ha: 2.03 },
    // { _id: ObjectId("61fc0002113e381bd233cb86"), Ha: 4.29 },
    // { _id: ObjectId("61fc0002113e381bd233cb8b"), Ha: 3.07 },
    // { _id: ObjectId("61fc0002113e381bd233cb7f"), Ha: 6.68 },
    // { _id: ObjectId("61fc0009113e381bd233d47e"), Ha: 2.03 },
    // { _id: ObjectId("61fc0009113e381bd233d47d"), Ha: 5.97 },
    // { _id: ObjectId("61fc0009113e381bd233d480"), Ha: 7.14 },
    // { _id: ObjectId("61fc0011113e381bd233e08d"), Ha: 1.82 },
    // { _id: ObjectId("61fc0018113e381bd233ea7b"), Ha: 1.07 },


    // //la raya
    // { _id: ObjectId("61cc93d39b357fa4472b1ea8"), Ha: 19.21 },
    // { _id: ObjectId("61cc95d39b357fa4472b1eab"), Ha: 3.57 },
    // { _id: ObjectId("61cc944b9b357fa4472b1ea9"), Ha: 4.36 },
    // { _id: ObjectId("61cc95149b357fa4472b1eaa"), Ha: 7.65 },

    //porvenir
    { _id: ObjectId("5df9057ca1ac6e18367ecb7a"), Ha: 36.62 },
    { _id: ObjectId("5e7e3772c1d28fdafe636df5"), Ha: 0.85 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e06"), Ha: 2.16 },
    { _id: ObjectId("5e821e91c1d28fdafe636e0b"), Ha: 2.09 },
    { _id: ObjectId("5e821e91c1d28fdafe636e82"), Ha: 0.79 },
    { _id: ObjectId("5e7e3773c1d28fdafe636df6"), Ha: 0.84 },
    { _id: ObjectId("5e821e93c1d28fdafe637398"), Ha: 0.51 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e03"), Ha: 1.71 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e07"), Ha: 1.33 },
    { _id: ObjectId("5e821e91c1d28fdafe636e0c"), Ha: 2.31 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e04"), Ha: 3.12 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e02"), Ha: 4.08 },
    { _id: ObjectId("5e7e3773c1d28fdafe636df7"), Ha: 16.2 },
    { _id: ObjectId("5e821e8fc1d28fdafe636e05"), Ha: 1.04 },
    { _id: ObjectId("5e7e3773c1d28fdafe636df8"), Ha: 9.77 },
    { _id: ObjectId("5e821e91c1d28fdafe636e0a"), Ha: 2.42 },
    { _id: ObjectId("5e821e91c1d28fdafe636e09"), Ha: 4.01 },
    { _id: ObjectId("5e7e3772c1d28fdafe636df4"), Ha: 20.3 },
    { _id: ObjectId("5e821e91c1d28fdafe636e83"), Ha: 2.30 },
    { _id: ObjectId("5ef39d5a726b1fe6af57dc45"), Ha: 1.92 },
    { _id: ObjectId("5ef4ff10726b1fe6af57dd09"), Ha: 1.70 },
    { _id: ObjectId("5f621721647b17ea60aef158"), Ha: 1.29 },





]

cursor.forEach(i => {
    db.cartography.update(
        {
            _id: i._id
        },
        {
            $set: {
                "properties.custom.Ha": {
                    "type": "number",
                    "value": i.Ha
                }
            }
        }
    )
})
