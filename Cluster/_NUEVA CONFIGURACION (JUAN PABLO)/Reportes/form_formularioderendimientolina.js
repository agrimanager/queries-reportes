db.form_formularioderendimientolina.aggregate(
    [

        //---empleado numerico


        //========EMPLEADOS
        //----NOTA: tener en cuenta que eligen mas de [20] empleados  y tambien registros sin empleados = "" !!!!
        {
            "$addFields": {
                "Empleados": {
                    "$cond": {
                        "if": { "$eq": [{ "$type": "$Nombre Empleado" }, "array"] },
                        "then": "$Nombre Empleado",
                        "else": { "$map": { "input": { "$objectToArray": "$Nombre Empleado" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                    }
                }
            }
        }

        , {
            "$unwind": {
                "path": "$Empleados",
                // "includeArrayIndex": "arrayIndex",
                "preserveNullAndEmptyArrays": false
            }
        }


        //--info empleado
        , {
            "$addFields": {
                "empleado_oid": { "$toObjectId": "$Empleados._id" }
            }
        }

        , {
            "$lookup": {
                "from": "employees",
                "localField": "empleado_oid",
                "foreignField": "_id",
                "as": "info_empleado"
            }
        }
        , {
            "$unwind": {
                "path": "$info_empleado",
                "preserveNullAndEmptyArrays": false
            }
        }


        , {
            "$addFields": {
                "empleado seleccion": "$Empleado.name"

                , "empleado nombre y apellidos": {
                    "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                }

                , "empleado codigo": "$info_empleado.code"
            }
        }

        , {
            "$project": {
                "info_empleado": 0
                , "Empleados": 0

                , "Nombre Empleado": 0
                , "Point": 0
                , "Formula": 0
                , "uid": 0

                // , "empleado_oid": 0 //necesario para cruce simple
            }
        }



    ]


)
