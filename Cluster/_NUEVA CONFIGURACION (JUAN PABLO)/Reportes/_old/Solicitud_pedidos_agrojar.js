db.users.aggregate(
    [

        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_solicitudpedidosagrojar",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },

                "pipeline": [

                    //=============filtro fechas
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                    { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                                ]
                            }
                        }
                    },

                    //=============finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                        }
                    }


                    // //=============productos (armar array)



                    //---array de productos
                    , {
                        "$addFields": {
                            "array_productos": [
                                {
                                    "producto": { "$ifNull": ["$Producto 1", ""] },
                                    "cantidad": "$Cantidad Producto 1",
                                    "observacion": "$Observacion Producto 1"

                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 1", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 1", 0] }
                                },

                                {
                                    "producto": { "$ifNull": ["$Producto 2", ""] },
                                    "cantidad": "$Cantidad Producto 2",
                                    "observacion": "$Observacion Producto 2"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 2", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 2", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 3", ""] },
                                    "cantidad": "$Cantidad Producto 3",
                                    "observacion": "$Observacion Producto 3"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 3", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 3", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 4", ""] },
                                    "cantidad": "$Cantidad Producto 4",
                                    "observacion": "$Observacion Producto 4"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 4", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 4", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 5", ""] },
                                    "cantidad": "$Cantidad Producto 5",
                                    "observacion": "$Observacion Producto 5"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 5", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 5", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 6", ""] },
                                    "unidad": "$Unidad Producto 6",
                                    "cantidad": "$Cantidad Producto 6",
                                    "observacion": "$Observacion Producto 6"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 6", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 6", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 7", ""] },
                                    "cantidad": "$Cantidad Producto 7",
                                    "observacion": "$Observacion Producto 7"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 7", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 7", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 8", ""] },
                                    "cantidad": "$Cantidad Producto 8",
                                    "observacion": "$Observacion Producto 8"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 8", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 8", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 9", ""] },
                                    "cantidad": "$Cantidad Producto 9",
                                    "observacion": "$Observacion Producto 9"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 9", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 9", 0] }
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto 10", ""] },
                                    "cantidad": "$Cantidad Producto 10",
                                    "observacion": "$Observacion Producto 10"
                                    ,"despacho_producto": { "$ifNull": ["$Despacho Producto 10", "viejo"] }
                                    ,"cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 10", 0] }

                                }
                            ]
                        }
                    }



                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_productos": {
                                "$filter": {
                                    "input": "$array_productos",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.producto", ""]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$array_productos",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    //----OBTENER INFO PRODUCTO
                    //...
                    ,{
                        "$lookup": {
                            "from": "supplies",
                            "localField": "array_productos.producto",
                            "foreignField": "name",
                            "as": "info_producto"
                        }
                    },
                    { "$unwind": "$info_producto" },
                    { "$addFields": { "producto_sku": { "$ifNull": ["$info_producto.sku", ""] } } },



                    //fecha
                    {
                        "$addFields": {
                            "Fecha de solicitud": {
                                "$dateToString": {
                                    "date": "$Fecha de solicitud",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }
                        }
                    }


                    //=============proyeccion final

                    , {
                        "$project": {
                            "finca_point": "$finca"
                            ,"Finca": "$Finca"
                            ,"Fecha de solicitud": "$Fecha de solicitud"


                            //---info producto
                            , "Producto": "$array_productos.producto"
                            , "Producto SKU": "$producto_sku"
                            , "Cantidad": "$array_productos.cantidad"
                            , "Observacion": "$array_productos.observacion"

                            , "Despacho": "$array_productos.despacho_producto"
                            , "Cantidad enviada": "$array_productos.cantidad_enviada"

                            , "supervisor": "$supervisor"
                            , "rgDate": "$rgDate"

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
