[



    {
        "$addFields": {
            "variable_cartografia": "$Lote"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "Point": 0
            , "Lote": 0
            , "Formula": 0
            , "uid": 0
            , "uDate": 0
        }
    }


    , {
        "$addFields": {
            "Estado Fenologico": {
                "$cond": {
                    "if": { "$in": [{ "$type": "$Estado Fenologico" }, ["object", "string"]] },
                    "then": [],
                    "else": "$Estado Fenologico"
                }
            }
        }
    }
    , {
        "$addFields": {
            "Estado Fenologico": {
                "$reduce": {
                    "input": "$Estado Fenologico",
                    "initialValue": "",
                    "in": {
                        "$cond": {
                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                            "then": "$$this",
                            "else": { "$concat": ["$$value", " -- ", "$$this"] }
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "Fecha": {
                "$dateToString": {
                    "date": "$rgDate",
                    "format": "%Y-%m-%d",
                    "timezone": "America/Bogota"
                }
            }
        }
    }



    , {
        "$addFields": {

            "array_info_sanidad": [



                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga1", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo1" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro1" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto1" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto1" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja1" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula1" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama1" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo1" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo1" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos1" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion1", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion1", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado1", ""] },
                    "Observacion": { "$ifNull": ["$Observacion1", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga2", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo2" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro2" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto2" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto2" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja2" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula2" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama2" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo2" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo2" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos2" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion2", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion2", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado2", ""] },
                    "Observacion": { "$ifNull": ["$Observacion2", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga3", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo3" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro3" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto3" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto3" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja3" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula3" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama3" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo3" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo3" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos3" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion3", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion3", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado3", ""] },
                    "Observacion": { "$ifNull": ["$Observacion3", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga4", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo4" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro4" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto4" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto4" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja4" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula4" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama4" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo4" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo4" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos4" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion4", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion4", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado4", ""] },
                    "Observacion": { "$ifNull": ["$Observacion4", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga5", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo5" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro5" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto5" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto5" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja5" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula5" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama5" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo5" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo5" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos5" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion5", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion5", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado5", ""] },
                    "Observacion": { "$ifNull": ["$Observacion5", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga6", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo6" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro6" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto6" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto6" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja6" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula6" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama6" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo6" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo6" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos6" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion6", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion6", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado6", ""] },
                    "Observacion": { "$ifNull": ["$Observacion6", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga7", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo7" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro7" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto7" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto7" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja7" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula7" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama7" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo7" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo7" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos7" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion7", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion7", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado7", ""] },
                    "Observacion": { "$ifNull": ["$Observacion7", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga8", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo8" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro8" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto8" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto8" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja8" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula8" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama8" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo8" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo8" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos8" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion8", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion8", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado8", ""] },
                    "Observacion": { "$ifNull": ["$Observacion8", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga9", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo9" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro9" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto9" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto9" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja9" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula9" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama9" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo9" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo9" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos9" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion9", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion9", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado9", ""] },
                    "Observacion": { "$ifNull": ["$Observacion9", ""] }
                },
                {
                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga10", ""] },
                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo10" }, 0] },
                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro10" }, 0] },
                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto10" }, 0] },
                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto10" }, 0] },
                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja10" }, 0] },
                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula10" }, 0] },
                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama10" }, 0] },
                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo10" }, 0] },
                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo10" }, 0] },
                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos10" }, 0] },
                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion10", ""] },
                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion10", ""] },
                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado10", ""] },
                    "Observacion": { "$ifNull": ["$Observacion10", ""] }
                }




            ]

        }
    }


    , {
        "$addFields": {
            "array_info_sanidad": {
                "$filter": {
                    "input": "$array_info_sanidad",
                    "as": "item",
                    "cond": {
                        "$ne": ["$$item.Enfermedad o Plaga", ""]
                    }
                }
            }
        }
    }


    , {
        "$match": {
            "array_info_sanidad": { "$ne": [] }
        }
    }

    , {
        "$group": {
            "_id": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"

            }
            , "num_censos_x_lote": { "$sum": 1 }
            , "data": { "$push": "$$ROOT" }
        }
    }

    , { "$unwind": "$data" }


    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data",
                    {
                        "num_censos_x_lote": "$num_censos_x_lote"
                    }
                ]
            }
        }
    }


    , {
        "$unwind": {
            "path": "$array_info_sanidad",
            "preserveNullAndEmptyArrays": false
        }
    }




    , {
        "$project": {
            "Fecha": "$Fecha"

            , "finca": "$finca"
            , "bloque": "$bloque"
            , "lote": "$lote"
            , "Estado Fenologico": "$Estado Fenologico"

            , "Enfermedad o Plaga": "$array_info_sanidad.Enfermedad o Plaga"
            , "Huevo": "$array_info_sanidad.Huevo"
            , "Inmaduro": "$array_info_sanidad.Inmaduro"
            , "Adulto": "$array_info_sanidad.Adulto"
            , "Fruto": "$array_info_sanidad.Fruto"
            , "Hoja": "$array_info_sanidad.Hoja"
            , "Panicula": "$array_info_sanidad.Panicula"
            , "Rama": "$array_info_sanidad.Rama"
            , "Tallo": "$array_info_sanidad.Tallo"
            , "Suelo": "$array_info_sanidad.Suelo"
            , "Individuos": "$array_info_sanidad.Individuos"
            , "Grado de afectacion": "$array_info_sanidad.Grado de afectacion"
            , "Porcentaje de afectacion": "$array_info_sanidad.Porcentaje de afectacion"
            , "Tercio Afectado": "$array_info_sanidad.Tercio Afectado"
            , "Observacion": "$array_info_sanidad.Observacion"


            , "supervisor": "$supervisor"
            , "capture": "$capture"
            , "rgDate": "$rgDate"


            , "num_censos_x_lote": "$num_censos_x_lote"

        }
    }





    , {
        "$addFields": {
            "suma_campos_evaluacion": {
                "$switch": {
                    "branches": [


                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Atta spp / Hormiga arriera"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Hoja"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Bombacoccus aguacatae / Protopulvinaria pyriformis / Pulvinaria psidii / Escamas blandas"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Hoja",
                                    "$Rama",
                                    "$Tallo"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Bruggmanniella perseae / Mosca del ovario"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Colletotrichum gloeosporioides / Antracnosis"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Panicula"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Compsus sp / Vaquita"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Copturomimus hustachei / Perforador del tallo "
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                    ,{

                                        "$cond": {
                                            "if": {
                                                "$ne": [
                                                    "$Grado de afectacion",
                                                    ""
                                                ]
                                            },
                                            "then": {"$toDouble": "$Grado de afectacion"},
                                            "else": 0
                                        }

                                    }
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Crisomelidos"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Rama"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Enemigos naturales"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Falso medidor"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Frankliniella gardeniae / Trips"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Panicula"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Heilipus lauri / Barrenador de la semilla de aguacate"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Huevo",
                                    "$Inmaduro",
                                    "$Adulto"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Heilipus Trifasciatus / Barrenador de la semilla de aguacate"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Huevo",
                                    "$Inmaduro",
                                    "$Adulto"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Hemiberlesia cyanophylli / Pseudoparlatoria parlatorioides / Chrysomphalus dictyospermi /Escamas portegidas"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja",
                                    "$Rama",
                                    "$Tallo"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Larvas Coleópteros / Chizas "
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Monalonion velezangueli / Chinche del aguacate"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Rama"
                                    , "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Myzus persicae / Pulgones"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Hoja"
                                    , "$Rama"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Oligonychus yothersi / Acaro rojo"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Huevo",
                                    "$Inmaduro",
                                    "$Adulto"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Pandeleteius viticollis / Picudo del follaje"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Hoja",
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Paraleyrodes sp / Mosca blanca"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Hoja"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Phyllophaga Obsoleta /Cucarrones marceños"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja",
                                    "$Rama"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Platinota spp / Pega pega "
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja"
                                    , "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Polinizadores"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Polyphagotarsonemus latus / Acaro Blanco"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Individuos"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Pseudococcus jackbeardsleyi / Ferrisia Kondoi / Cochinillas harinosas"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja",
                                    "$Rama"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Stenoma catenifer / Polilla de la semilla de aguacate"
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Rama"
                                ]
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "tatuador "
                                ]
                            },
                            "then": {
                                "$sum": [
                                    "$Fruto",
                                    "$Hoja"
                                ]
                            }
                        },

                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Cercospora purpurea/Pseudocercospora purpurea / Mancha angular"
                                ]
                            },
                            "then": {
                                "$cond": {
                                    "if": {
                                        "$ne": [
                                            "$Porcentaje de afectacion",
                                            ""
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Nectria galligena / Cancro bacteriano"
                                ]
                            },
                            "then": {
                                "$cond": {
                                    "if": {
                                        "$ne": [
                                            "$Porcentaje de afectacion",
                                            ""
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        },

                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Cylindrocarpon destructans / Cylindrocladium sp / Pudricion negra de la raiz "
                                ]
                            },
                            "then": {
                                "$cond": {
                                    "if": {
                                        "$ne": [
                                            "$Grado de afectacion",
                                            ""
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Phytophthora cinnamomi /Pudricion de raices"
                                ]
                            },
                            "then": {
                                "$cond": {
                                    "if": {
                                        "$ne": [
                                            "$Grado de afectacion",
                                            ""
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        },
                        {
                            "case": {
                                "$eq": [
                                    "$Enfermedad o Plaga",
                                    "Verticillum Nees / Marchitez por Verticillium"
                                ]
                            },
                            "then": {
                                "$cond": {
                                    "if": {
                                        "$ne": [
                                            "$Grado de afectacion",
                                            ""
                                        ]
                                    },
                                    "then": 1,
                                    "else": 0
                                }
                            }
                        }
                    ],
                    "default": 0
                }
            }
        }
    },
    {
        "$addFields": {
            "presencia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            "$suma_campos_evaluacion",
                            0
                        ]
                    },
                    "then": 0,
                    "else": 1
                }
            }
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"

                , "enfermedad_o_plaga": "$Enfermedad o Plaga"
            }
            , "num_censos_x_lote": { "$min": "$num_censos_x_lote" }
            , "suma_campos_evaluacion": { "$sum": "$suma_campos_evaluacion" }
            , "presencia": { "$sum": "$presencia" }

        }
    }



    , {
        "$addFields": {
            "incidencia": {
                "$cond": {
                    "if": { "$eq": ["$num_censos_x_lote", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$presencia", "$num_censos_x_lote"]
                            }
                            , 100]
                    }
                }
            }
        }
    }
    , {
        "$addFields": {
            "severidad": {
                "$cond": {
                    "if": { "$eq": ["$presencia", 0] },
                    "then": 0,
                    "else": {
                        "$divide": ["$suma_campos_evaluacion", "$presencia"]
                    }
                }
            }
        }
    }

    ,{
      "$addFields":{
        "incidencia": {"$divide": [{"$subtract": [{"$multiply": ["$incidencia",100]},{"$mod": [{"$multiply": ["$incidencia",100]},1]}]},100]}
        ,"severidad": {"$divide": [{"$subtract": [{"$multiply": ["$severidad",100]},{"$mod": [{"$multiply": ["$severidad",100]},1]}]},100]}
      }
    }


    , {
        "$lookup": {
            "from": "form_puentesanidadaguacatesemaforo",
            "localField": "_id.enfermedad_o_plaga",
            "foreignField": "Enfermedad o Plaga",
            "as": "data_semaforo"
        }
    }

    , {
        "$unwind": {
            "path": "$data_semaforo",
            "preserveNullAndEmptyArrays": false
        }
    }


    , {
        "$addFields": {
            "data_semaforo_tipo": "$data_semaforo.tipo"
            , "data_semaforo_umbral_min": "$data_semaforo.Umbral min"
            , "data_semaforo_umbral_max": "$data_semaforo.Umbral max"
        }
    }
    , {
        "$project": {
            "data_semaforo": 0
        }
    }



    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$_id",
                    {
                        "tipo": "$data_semaforo_tipo",
                        "num_censos_x_lote": "$num_censos_x_lote",
                        "suma_campos_evaluacion": "$suma_campos_evaluacion",
                        "presencia": "$presencia",
                        "incidencia": "$incidencia",
                        "severidad": "$severidad",


                        "umbral_accion_min": "$data_semaforo_umbral_min",
                        "umbral_accion_max": "$data_semaforo_umbral_max"
                    }
                ]
            }
        }

    }

    , {
        "$addFields": {
            "umbral_incidencia": {
                "$cond": {
                    "if": { "$lt": ["$incidencia", "$umbral_accion_min"] },
                    "then": "umbral_1",
                    "else": {
                        "$cond": {
                            "if": { "$gte": ["$incidencia", "$umbral_accion_max"] },
                            "then": "umbral_3",
                            "else": "umbral_2"
                        }
                    }
                }
            }
        }
    }

    , {
        "$addFields": {
            "umbral_tipo": {
                "$cond": {
                    "if": { "$lt": ["$incidencia", "$umbral_accion_min"] },
                    "then": "umbral 1 (minimo)",
                    "else": {
                        "$cond": {
                            "if": { "$gte": ["$incidencia", "$umbral_accion_max"] },
                            "then": "umbral 3 (daño)",
                            "else": "umbral 2 (accion)"
                        }
                    }

                }
            }
        }
    }

    ,{
        "$sort":{
            "enfermedad_o_plaga":1
        }
    }



]
