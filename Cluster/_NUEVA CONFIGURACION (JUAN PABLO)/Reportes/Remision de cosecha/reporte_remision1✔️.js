[

    {
        "$match": {
            "FECHA Y HORA DE DESPACHO": { "$exists": true }
        }
    }


    , {
        "$addFields": {
            "farm_id": { "$toObjectId": "$Point.farm" }
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "farm_id",
            "foreignField": "_id",
            "as": "farm_data"
        }
    },
    { "$unwind": "$farm_data" },
    {
        "$addFields": {
            "Finca": "$farm_data.name"
        }
    }

    , {
        "$project": {
            "farm_id": 0,
            "farm_data": 0,


            "Formula": 0,
            "uid": 0,
            "uDate": 0,
            "Point": 0

        }
    }

    , {
        "$addFields": {
            "_id_date": { "$toDate": "$rgDate" }
        }
    }
    

    , {
        "$addFields": {
            "date_unix": { "$toLong": "$_id_date" }
        }
    }

    , {
        "$addFields": {
            "date_unix_min": {
                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
            }
        }
    }

    , {
        "$addFields": {
            "CONSECUTIVO_UNICO": "$date_unix_min"
        }
    }

    , {
        "$project": {
            "_id_date": 0,
            "date_unix": 0,
            "date_unix_min": 0

        }
    }



    , {
        "$addFields": {

            "fecha_rgDate": { "$dateToString": { "format": "%d-%m-%Y %H:%M", "date": "$rgDate", "timezone": "America/Bogota" } },

            "despacho_fecha": { "$dateToString": { "format": "%d-%m-%Y", "date": "$FECHA Y HORA DE DESPACHO", "timezone": "America/Bogota" } },
            "despacho_hora": { "$dateToString": { "format": "%H:%M", "date": "$FECHA Y HORA DE DESPACHO", "timezone": "America/Bogota" } },

            "cosecha_fecha": { "$dateToString": { "format": "%d-%m-%Y", "date": "$Fecha y Hora de Cosecha", "timezone": "America/Bogota" } },
            "cosecha_hora": { "$dateToString": { "format": "%H:%M", "date": "$Fecha y Hora de Cosecha", "timezone": "America/Bogota" } }


        }
    }


    , {
        "$addFields": {
            "Tipo de Fruta": { "$ifNull": ["$Tipo de Fruta", ""] }
        }
    }
    , {
        "$lookup": {
            "from": "form_puentefincaremisionfruta",
            "as": "data",
            "let": {
                "finca": "$Finca"
                , "tipo_fruta": "$Tipo de Fruta"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$Predio", "$$finca"] }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "caso_especial": {
                            "$cond": {
                                "if": {
                                    "$and": [
                                        { "$eq": ["$$finca", "Encanto"] }
                                        , { "$ne": ["$Tipo de fruta", "$$tipo_fruta"] }
                                    ]
                                },
                                "then": "si",
                                "else": "no"
                            }
                        }
                    }
                }

                , {
                    "$match": {
                        "caso_especial": "no"
                    }
                }


            ]
        }
    }


    , {
        "$unwind": {
            "path": "$data",
            "preserveNullAndEmptyArrays": true
        }
    }

    , {
        "$addFields": {
            "Predio": { "$ifNull": ["$data.Predio", "sin datos"] },
            "Codigo ICA": { "$ifNull": ["$data.Codigo ICA", "sin datos"] },
            "Codigo RF": { "$ifNull": ["$data.Codigo RF", "sin datos"] },
            "Codigo GAP": { "$ifNull": ["$data.Codigo GAP", "sin datos"] },
            "Municipio": { "$ifNull": ["$data.Municipio", "sin datos"] },
            "Nombre proveedor": { "$ifNull": ["$data.Nombre proveedor", "sin datos"] },
            "Cedula NIT": { "$ifNull": ["$data.Cedula NIT", "sin datos"] },
            "Tipo de fruta": { "$ifNull": ["$data.Tipo de fruta", "sin datos"] }
        }
    }

    , {
        "$project": {
            "data": 0

            , "Tipo de Fruta": 0
        }
    }



]