db.users.aggregate(
    [
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-04-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_actividadesdiariasaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"

                    , "user_timezone": "$timezone"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                    { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                                ]
                            }
                        }
                    },


                    { "$unwind": "$Empleado" },

                    {
                        "$addFields": {
                            "employee_id": { "$toObjectId": "$Empleado._id" },
                            "farm_id": { "$toObjectId": "$Point.farm" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm_id",
                            "foreignField": "_id",
                            "as": "farm_data"
                        }
                    },
                    { "$unwind": "$farm_data" },
                    {
                        "$addFields": {
                            "Finca": "$farm_data.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "employees",
                            "localField": "employee_id",
                            "foreignField": "_id",
                            "as": "employee_data"
                        }
                    },
                    { "$unwind": "$employee_data" },

                    {
                        "$addFields": {
                            "numero de empleado": "$employee_data.code",

                            "empleado": {
                                "$concat": [
                                    "$employee_data.firstName",
                                    " ",
                                    "$employee_data.lastName"
                                ]
                            },

                            "referencia": "$Empleado.reference"
                        }
                    },


                    //---!!!DANGER....OJO CON rgDate y uDate
                    //---llegan rgDate dia, rgDate mes, rgDate año.....
                    //-----!!!DANGER (desproyectar fechas)
                    //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
                    {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    //=====================================================
                    //--nombre_maestro : aaaaa
                    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
                    //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
                    //--valor_mestro_enlazado : ccc cc c
                    //=====================================================

                    //--Maestro principal
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Labor_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }


                    //--Mestro enlazado
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    //"then": "$$dataKV.k",
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    //----FALLA EN LA WEB
                    /*
                    , {
                        "$unwind": {
                            "path": "$nombre_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    */

                    //---//obtener el primero (aveces falla)
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$arrayElemAt": ["$nombre_mestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$ifNull": ["$nombre_mestro_enlazado", ""] }
                        }
                    }




                    //--Valor Mestro enlazado
                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0
                        }
                    }




                    , {
                        "$project": {
                            "_id": "$_id",
                            "Finca": "$Finca",
                            "Fecha": "$Fecha",
                            "Labor": "$Labor",
                            "Labor maestro": "$valor_mestro_enlazado",
                            "Cantidad": "$Cantidad",
                            "Horas efectivas": "$Horas efectivas",
                            "uid": "$uid",
                            "empleado": "$empleado",
                            "numero de empleado": "$numero de empleado",
                            "referencia": "$referencia",
                            "supervisor": "$supervisor",
                            "rgDate": "$rgDate",
                            "uDate": "$uDate",
                            "capture": "$capture"

                        }
                    }




                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

    ]

)
