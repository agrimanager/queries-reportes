[


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_actividadesdiariasaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"

                    , "user_timezone": "$timezone"
                },
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                    { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                                ]
                            }
                        }
                    },


                    { "$unwind": "$Empleado" },

                    {
                        "$addFields": {
                            "employee_id": { "$toObjectId": "$Empleado._id" },
                            "farm_id": { "$toObjectId": "$Point.farm" }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "farm_id",
                            "foreignField": "_id",
                            "as": "farm_data"
                        }
                    },
                    { "$unwind": "$farm_data" },
                    {
                        "$addFields": {
                            "Finca": "$farm_data.name"
                        }
                    },

                    {
                        "$lookup": {
                            "from": "employees",
                            "localField": "employee_id",
                            "foreignField": "_id",
                            "as": "employee_data"
                        }
                    },
                    { "$unwind": "$employee_data" },

                    {
                        "$addFields": {
                            "numero de empleado": "$employee_data.code",

                            "empleado": {
                                "$concat": [
                                    "$employee_data.firstName",
                                    " ",
                                    "$employee_data.lastName"
                                ]
                            },

                            "referencia": "$Empleado.reference"
                        }
                    },


                    {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Labor_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },

                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$arrayElemAt": ["$nombre_mestro_enlazado", 0] }
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$ifNull": ["$nombre_mestro_enlazado", ""] }
                        }
                    }




                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0
                        }
                    }




                    , {
                        "$project": {
                            "_id": "$_id",
                            "Finca": "$Finca",
                            "Fecha": "$Fecha",
                            "Labor": "$Labor",
                            "Labor maestro": "$valor_mestro_enlazado",
                            "Cantidad": "$Cantidad",
                            "Horas efectivas": "$Horas efectivas",
                            "uid": "$uid",
                            "empleado": "$empleado",
                            "numero de empleado": "$numero de empleado",
                            "referencia": "$referencia",
                            "supervisor": "$supervisor",
                            "rgDate": "$rgDate",
                            "uDate": "$uDate",
                            "capture": "$capture"

                        }
                    }




                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }

    ]
