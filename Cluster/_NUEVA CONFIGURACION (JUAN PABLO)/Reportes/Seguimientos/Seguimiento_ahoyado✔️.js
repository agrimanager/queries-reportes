[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_seguimientoahoyado",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"

                , "user_timezone": "$timezone"
            },
            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                        , "Formula": 0
                    }
                },




                {
                    "$addFields": {
                        "variable_empleado_numerico": "$Colaborador"
                    }
                },
                {
                    "$project": {
                        "Colaborador": 0
                    }
                },

                {
                    "$match": {
                        "variable_empleado_numerico": { "$ne": "" }
                    }
                },

                { "$unwind": "$variable_empleado_numerico" }

                , {
                    "$addFields": {
                        "empleado_oid": { "$toObjectId": "$variable_empleado_numerico._id" }
                    }
                }

                , {
                    "$lookup": {
                        "from": "employees",
                        "localField": "empleado_oid",
                        "foreignField": "_id",
                        "as": "info_empleado"
                    }
                }
                , {
                    "$unwind": {
                        "path": "$info_empleado",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$addFields": {
                        "empleado seleccion": "$variable_empleado_numerico.name"
                        , "empleado nombre y apellidos": {
                            "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                        }
                        , "empleado codigo": "$info_empleado.code"

                        , "empleado cantidad": { "$toDouble": "$variable_empleado_numerico.value" }
                        , "empleado referencia": {
                            "$cond": {
                                "if": { "$eq": ["$variable_empleado_numerico.reference", ""] },
                                "then": "sin selección",
                                "else": "$variable_empleado_numerico.reference"
                            }
                        }
                    }
                }

                , {
                    "$project": {
                        "info_empleado": 0
                        , "variable_empleado_numerico": 0
                        , "empleado_oid": 0
                    }
                }


                , {
                    "$addFields": {
                        "Fecha_txt": {
                            "$dateToString": {
                                "date": "$Fecha",
                                "format": "%Y-%m-%d %H:%M",
                                "timezone": "$$user_timezone"
                            }
                        }
                    }
                }





            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }





]
