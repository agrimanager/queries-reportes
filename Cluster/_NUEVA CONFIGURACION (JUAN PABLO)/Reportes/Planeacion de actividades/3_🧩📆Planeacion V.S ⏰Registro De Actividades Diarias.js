[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_planeacionycumplimientodeactividades",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "_id_date": { "$toDate": "$rgDate" }
                    }
                }

                , {
                    "$addFields": {
                        "date_unix": { "$toLong": "$_id_date" }
                    }
                }


                , {
                    "$addFields": {
                        "date_unix_min": {
                            "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                        }
                    }
                }



                , {
                    "$addFields": {
                        "CODIGO_PLANEACION": "$date_unix_min"
                    }
                }




                , {
                    "$addFields": {
                        "array_actividades": [
                            {
                                "actividad": "$Actividad1",
                                "jornales": "$Jornales Actividad1",
                                "codigo": "1"
                            },
                            {
                                "actividad": "$Actividad2",
                                "jornales": "$Jornales Actividad2",
                                "codigo": "2"
                            },
                            {
                                "actividad": "$Actividad3",
                                "jornales": "$Jornales Actividad3",
                                "codigo": "3"
                            },
                            {
                                "actividad": "$Actividad4",
                                "jornales": "$Jornales Actividad4",
                                "codigo": "4"
                            },
                            {
                                "actividad": "$Actividad5",
                                "jornales": "$Jornales Actividad5",
                                "codigo": "5"
                            },

                            {
                                "actividad": "$Actividad6",
                                "jornales": "$Jornales Actividad6",
                                "codigo": "6"
                            },
                            {
                                "actividad": "$Actividad7",
                                "jornales": "$Jornales Actividad7",
                                "codigo": "7"
                            },
                            {
                                "actividad": "$Actividad8",
                                "jornales": "$Jornales Actividad8",
                                "codigo": "8"
                            },
                            {
                                "actividad": "$Actividad9",
                                "jornales": "$Jornales Actividad9",
                                "codigo": "9"
                            },
                            {
                                "actividad": "$Actividad10",
                                "jornales": "$Jornales Actividad10",
                                "codigo": "10"
                            },

                            {
                                "actividad": "$Actividad11",
                                "jornales": "$Jornales Actividad11",
                                "codigo": "11"
                            },
                            {
                                "actividad": "$Actividad12",
                                "jornales": "$Jornales Actividad12",
                                "codigo": "12"
                            },
                            {
                                "actividad": "$Actividad13",
                                "jornales": "$Jornales Actividad13",
                                "codigo": "13"
                            },
                            {
                                "actividad": "$Actividad14",
                                "jornales": "$Jornales Actividad14",
                                "codigo": "14"
                            },
                            {
                                "actividad": "$Actividad15",
                                "jornales": "$Jornales Actividad15",
                                "codigo": "15"
                            }

                        ]
                    }
                }
                , {
                    "$addFields": {
                        "array_actividades": {
                            "$filter": {
                                "input": "$array_actividades",
                                "as": "item",
                                "cond": {
                                    "$ne": ["$$item.actividad", ""]
                                }
                            }
                        }
                    }
                }



                , {
                    "$project": {
                        "uid": 0,
                        "uDate": 0,
                        "Formula": 0,
                        "Point": 0

                        , "Actividad1": 0
                        , "Jornales Actividad1": 0

                        , "Actividad2": 0
                        , "Jornales Actividad2": 0

                        , "Actividad3": 0
                        , "Jornales Actividad3": 0

                        , "Actividad4": 0
                        , "Jornales Actividad4": 0

                        , "Actividad5": 0
                        , "Jornales Actividad5": 0

                        , "Actividad6": 0
                        , "Jornales Actividad6": 0

                        , "Actividad7": 0
                        , "Jornales Actividad7": 0

                        , "Actividad8": 0
                        , "Jornales Actividad8": 0

                        , "Actividad9": 0
                        , "Jornales Actividad9": 0

                        , "Actividad10": 0
                        , "Jornales Actividad10": 0

                        , "Actividad11": 0
                        , "Jornales Actividad11": 0

                        , "Actividad12": 0
                        , "Jornales Actividad12": 0

                        , "Actividad13": 0
                        , "Jornales Actividad13": 0

                        , "Actividad14": 0
                        , "Jornales Actividad14": 0

                        , "Actividad15": 0
                        , "Jornales Actividad15": 0


                    }
                }



                , { "$unwind": "$array_actividades" }

                , {
                    "$addFields": {
                        "CODIGO_PLANEACION_ACTIVIDAD": {
                            "$concat": ["$CODIGO_PLANEACION", "$array_actividades.codigo"]
                        }
                    }
                }

                , {
                    "$project": {
                        "CODIGO_PLANEACION": "$CODIGO_PLANEACION",
                        "Finca": "$Finca",
                        "Lotes": "$Lotes",

                        "Actividad": "$array_actividades.actividad",
                        "Jornales": "$array_actividades.jornales",
                        "CODIGO_PLANEACION_ACTIVIDAD": "$CODIGO_PLANEACION_ACTIVIDAD",


                        "Fecha inicial": "$Fecha inicial",
                        "Fecha final": "$Fecha final",

                        "supervisor": "$supervisor",
                        "capture": "$capture",
                        "rgDate": "$rgDate"

                        , "Nota": "El CODIGO_PLANEACION_ACTIVIDAD se necesita para la Ejecucion"

                    }
                }

                , {
                    "$match": {
                        "Actividad": { "$exists": true }
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }





    , {
        "$lookup": {
            "from": "form_registrodeactividadesdiarias",
            "as": "data",
            "let": {
                "codigo": "$CODIGO_PLANEACION_ACTIVIDAD"
            },

            "pipeline": [


                {
                    "$match": {
                        "Actividad": { "$ne": "" }
                        , "Empleados": { "$ne": "" }
                    }
                },



                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                {
                                    "$eq": [
                                        { "$toString": "$$codigo" }
                                        , { "$toString": "$CODIGO PLANEACION LABOR" }
                                    ]
                                }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "Empleados": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Empleados" }, "array"] },
                                "then": "$Empleados",
                                "else": { "$map": { "input": { "$objectToArray": "$Empleados" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                            }
                        }
                    }
                }


                , {
                    "$addFields": {
                        "cantidad_empleados": { "$size": "$Empleados" }
                    }
                }


                , {
                    "$project": {
                        "cantidad_empleados": 1
                    }
                }
            ]
        }
    }



    , {
        "$addFields": {
            "Total_jornales_registrados": {
                "$reduce": {
                    "input": "$data.cantidad_empleados",
                    "initialValue": 0,
                    "in": {
                        "$sum": ["$$this", "$$value"]
                    }
                }
            }
        }
    }

    , {
        "$project": {
            "data": 0
        }
    }


    , {
        "$addFields": {
            "pct_vs": {
                "$cond": {
                    "if": { "$eq": ["$Jornales", 0] },
                    "then": 0,
                    "else": {
                        "$multiply": [
                            {
                                "$divide": ["$Total_jornales_registrados",
                                    "$Jornales"]
                            }
                            , 100
                        ]
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "pct_vs": { "$divide": [{ "$subtract": [{ "$multiply": ["$pct_vs", 100] }, { "$mod": [{ "$multiply": ["$pct_vs", 100] }, 1] }] }, 100] }
        }
    }





]
