[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_registrodeactividadesdiarias",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [


                {
                    "$match": {
                        "Actividad": { "$ne": "" }
                        , "Empleados": { "$ne": "" }
                    }
                },



                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                        , "Formula": 0
                    }
                }


                , {
                    "$addFields": {
                        "Empleados": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Empleados" }, "array"] },
                                "then": "$Empleados",
                                "else": { "$map": { "input": { "$objectToArray": "$Empleados" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                            }
                        }
                    }
                }

                , {
                    "$unwind": {
                        "path": "$Empleados",
                        "preserveNullAndEmptyArrays": false
                    }
                }



                , {
                    "$addFields": {
                        "empleado_oid": { "$toObjectId": "$Empleados._id" }
                    }
                }

                , {
                    "$lookup": {
                        "from": "employees",
                        "localField": "empleado_oid",
                        "foreignField": "_id",
                        "as": "info_empleado"
                    }
                }
                , {
                    "$unwind": {
                        "path": "$info_empleado",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$addFields": {
                        "empleado seleccion": "$Empleado.name"

                        , "empleado nombre y apellidos": {
                            "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                        }

                        , "empleado codigo": "$info_empleado.code"
                    }
                }

                , {
                    "$project": {
                        "info_empleado": 0
                        , "Empleado": 0
                    }
                }




                , {
                    "$project": {
                        "Finca": "$finca"


                        , "Actividad": "$Actividad"
                        , "CODIGO PLANEACION LABOR": { "$toString": "$CODIGO PLANEACION LABOR" }


                        , "Empleado": "$Empleados.name"
                        , "Empleado Nombre": "$empleado nombre y apellidos"
                        , "Empleado Codigo": "$empleado codigo"
                        , "Empleado referencia": "$Empleados.reference"
                        , "Empleado valor": "$Empleados.value"


                        , "Observacion": { "$ifNull": ["$Observacion", ""] }
                        , "capture": "$capture"



                        , "Fecha": {
                            "$dateToString": {
                                "date": "$Fecha",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }

                        , "Fecha rgDate": {
                            "$dateToString": {
                                "date": "$rgDate",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }

                        , "rgDate": "$rgDate"



                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
