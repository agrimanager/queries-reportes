db.form_planeacionycumplimientodeactividades.aggregate(
    [


        //========= GENERAR CODIGO UNICO POR REGISTRO
        {
            "$addFields": {
                //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
                "_id_date": { "$toDate": "$rgDate" }
            }
        }
        //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

        , {
            "$addFields": {
                "date_unix": { "$toLong": "$_id_date" }
            }
        }
        //"date_unix" : 1618521716000,  // 13 numeros



        //----Codigo a generar
        //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
        // 1618521716000 = XX18521716XXX = 18521716
        , {
            "$addFields": {
                "date_unix_min": {
                    "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                }
            }
        }



        //---------ASIGNAR CODIGO DE PLANEACION DE ACTIVIDADES
        , {
            "$addFields": {
                "CODIGO_PLANEACION": "$date_unix_min"
            }
        }






        //========= ESTANDARIZAR DATOS
        //---array de actividades planeadas
        , {
            "$addFields": {
                "array_actividades": [
                    {
                        "actividad": "$Actividad1",
                        "jornales": "$Jornales Actividad1",
                        "codigo": "-1"
                    },
                    {
                        "actividad": "$Actividad2",
                        "jornales": "$Jornales Actividad2",
                        "codigo": "-2"
                    },
                    {
                        "actividad": "$Actividad3",
                        "jornales": "$Jornales Actividad3",
                        "codigo": "-3"
                    },
                    {
                        "actividad": "$Actividad4",
                        "jornales": "$Jornales Actividad4",
                        "codigo": "-4"
                    },
                    {
                        "actividad": "$Actividad5",
                        "jornales": "$Jornales Actividad5",
                        "codigo": "-5"
                    }
                ]
            }
        }

        //--filtrar vacias
        , {
            "$addFields": {
                "array_actividades": {
                    "$filter": {
                        "input": "$array_actividades",
                        "as": "item",
                        "cond": {
                            "$ne": ["$$item.actividad", ""]
                        }
                    }
                }
            }
        }



        //---no mostrar datos
        , {
            "$project": {
                "uid": 0,
                "uDate": 0,
                "Formula": 0,
                "Point": 0

                //---variables despues de estandarizar datos
                , "Actividad1": 0
                , "Jornales Actividad1": 0

                , "Actividad2": 0
                , "Jornales Actividad2": 0

                , "Actividad3": 0
                , "Jornales Actividad3": 0

                , "Actividad4": 0
                , "Jornales Actividad4": 0

                , "Actividad5": 0
                , "Jornales Actividad5": 0


            }
        }



        //============ GENERAR CODIGOS POR ACTIVIDAD

        //--filtrar actividades vacias
        , { "$unwind": "$array_actividades" }

        //---concatenar codigos de planeacion con actividad (agregar -n al condigo)
        , {
            "$addFields": {
                "CODIGO_PLANEACION_ACTIVIDAD": {
                    "$concat": ["$CODIGO_PLANEACION", "$array_actividades.codigo"]
                }
            }
        }



        //============ VALORES FINALES DE REPORTE
        , {
            "$project": {

                //---variables base
                "CODIGO_PLANEACION": "$CODIGO_PLANEACION", //general
                "Finca": "$Finca",
                "Lotes": "$Lotes",

                "Actividad": "$array_actividades.actividad",
                "Jornales": "$array_actividades.jornales",
                "CODIGO_PLANEACION_ACTIVIDAD": "$CODIGO_PLANEACION_ACTIVIDAD", //unico-especifico


                //---variables otras
                "Fecha inicial": "$Fecha inicial",
                "Fecha final": "$Fecha final",

                "supervisor": "$supervisor",
                "capture": "$capture",
                "rgDate": "$rgDate"

                //---variables extras
                , "Nota": "El CODIGO_PLANEACION_ACTIVIDAD se necesita para la Ejecucion"

            }
        }







    ]

)
