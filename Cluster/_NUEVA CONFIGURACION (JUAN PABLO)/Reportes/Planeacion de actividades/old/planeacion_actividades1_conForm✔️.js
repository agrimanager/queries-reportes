[

    {
        "$addFields": {
            "_id_date": { "$toDate": "$rgDate" }
        }
    }

    , {
        "$addFields": {
            "date_unix": { "$toLong": "$_id_date" }
        }
    }


    , {
        "$addFields": {
            "date_unix_min": {
                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
            }
        }
    }



    , {
        "$addFields": {
            "CODIGO_PLANEACION": "$date_unix_min"
        }
    }




    , {
        "$addFields": {
            "array_actividades": [
                {
                    "actividad": "$Actividad1",
                    "jornales": "$Jornales Actividad1",
                    "codigo": "-1"
                },
                {
                    "actividad": "$Actividad2",
                    "jornales": "$Jornales Actividad2",
                    "codigo": "-2"
                },
                {
                    "actividad": "$Actividad3",
                    "jornales": "$Jornales Actividad3",
                    "codigo": "-3"
                },
                {
                    "actividad": "$Actividad4",
                    "jornales": "$Jornales Actividad4",
                    "codigo": "-4"
                },
                {
                    "actividad": "$Actividad5",
                    "jornales": "$Jornales Actividad5",
                    "codigo": "-5"
                }
            ]
        }
    }
    , {
        "$addFields": {
            "array_actividades": {
                "$filter": {
                    "input": "$array_actividades",
                    "as": "item",
                    "cond": {
                        "$ne": ["$$item.actividad", ""]
                    }
                }
            }
        }
    }



    , {
        "$project": {
            "uid": 0,
            "uDate": 0,
            "Formula": 0,
            "Point": 0

            , "Actividad1": 0
            , "Jornales Actividad1": 0

            , "Actividad2": 0
            , "Jornales Actividad2": 0

            , "Actividad3": 0
            , "Jornales Actividad3": 0

            , "Actividad4": 0
            , "Jornales Actividad4": 0

            , "Actividad5": 0
            , "Jornales Actividad5": 0


        }
    }



    , { "$unwind": "$array_actividades" }

    , {
        "$addFields": {
            "CODIGO_PLANEACION_ACTIVIDAD": {
                "$concat": ["$CODIGO_PLANEACION", "$array_actividades.codigo"]
            }
        }
    }

    , {
        "$project": {
            "CODIGO_PLANEACION": "$CODIGO_PLANEACION",
            "Finca": "$Finca",
            "Lotes": "$Lotes",

            "Actividad": "$array_actividades.actividad",
            "Jornales": "$array_actividades.jornales",
            "CODIGO_PLANEACION_ACTIVIDAD": "$CODIGO_PLANEACION_ACTIVIDAD",


            "Fecha inicial": "$Fecha inicial",
            "Fecha final": "$Fecha final",

            "supervisor": "$supervisor",
            "capture": "$capture",
            "rgDate": "$rgDate"

            , "Nota": "El CODIGO_PLANEACION_ACTIVIDAD se necesita para la Ejecucion"

        }
    }

]
