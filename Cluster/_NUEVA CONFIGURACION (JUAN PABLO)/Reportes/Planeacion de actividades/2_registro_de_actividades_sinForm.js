db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registrodeactividadesdiarias",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [

                    //========CONDICIONES BASES
                    {
                        "$match": {
                            "Actividad": { "$ne": "" }
                            , "Empleados": { "$ne": "" }
                        }
                    },


                    //========finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                            , "Formula": 0
                        }
                    }


                    //========EMPLEADOS
                    //----NOTA: tener en cuenta que eligen mas de [20] empleados  y tambien registros sin empleados = "" !!!!
                    , {
                        "$addFields": {
                            "Empleados": {
                                "$cond": {
                                    "if": { "$eq": [{ "$type": "$Empleados" }, "array"] },
                                    "then": "$Empleados",
                                    "else": { "$map": { "input": { "$objectToArray": "$Empleados" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                                }
                            }
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$Empleados",
                            // "includeArrayIndex": "arrayIndex",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    //--info empleado
                    , {
                        "$addFields": {
                            "empleado_oid": { "$toObjectId": "$Empleados._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_oid",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , {
                        "$addFields": {
                            "empleado seleccion": "$Empleado.name"

                            , "empleado nombre y apellidos": {
                                "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                            }

                            , "empleado codigo": "$info_empleado.code"
                        }
                    }

                    , {
                        "$project": {
                            "info_empleado": 0
                            , "Empleado": 0

                            // , "empleado_oid": 0 //necesario para cruce simple
                        }
                    }





                    // //=============PROYECCION FINAL
                    , {
                        "$project": {
                            "Finca": "$finca"

                            //---info base
                            , "Actividad": "$Actividad"
                            , "CODIGO PLANEACION LABOR": { "$toString": "$CODIGO PLANEACION LABOR" }

                            //---info Empleado
                            , "Empleado": "$Empleados.name"
                            , "Empleado Nombre": "$empleado nombre y apellidos"
                            , "Empleado Codigo": "$empleado codigo"
                            , "Empleado referencia": "$Empleados.reference"
                            , "Empleado valor": "$Empleados.value"

                            //---info otros
                            , "Observacion": { "$ifNull": ["$Observacion", ""] }
                            , "capture": "$capture"


                            //---info fechas
                            , "Fecha": {
                                "$dateToString": {
                                    // "date": "$rgDate",
                                    "date": "$Fecha",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }

                            , "Fecha rgDate": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }

                            , "rgDate": "$rgDate"



                        }
                    }


                    //----CODIGO DE SOLICITUD
                    //--cruzar



                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
