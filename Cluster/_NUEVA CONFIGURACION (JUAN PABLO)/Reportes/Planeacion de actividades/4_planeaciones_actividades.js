db.users.aggregate(
    [


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_planeacionactividadesaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },

                "pipeline": [

                    {
                        "$addFields": {
                            "_id_date": { "$toDate": "$rgDate" }
                        }
                    }

                    , {
                        "$addFields": {
                            "date_unix": { "$toLong": "$_id_date" }
                        }
                    }


                    , {
                        "$addFields": {
                            "date_unix_min": {
                                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                            }
                        }
                    }



                    , {
                        "$addFields": {
                            "CODIGO_PLANEACION": "$date_unix_min"
                        }
                    }


                    //---!!!DANGER....OJO CON rgDate y uDate
                    //---llegan rgDate dia, rgDate mes, rgDate año.....
                    //-----!!!DANGER (desproyectar fechas)
                    //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
                    , {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    //=====================================================
                    //--nombre_maestro : aaaaa
                    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
                    //--nombre_maestro_enlazado : aaaaa_bbb bbb bbb
                    //--valor_maestro_enlazado : ccc cc c
                    //=====================================================

                    //----Actividad1🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad1_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad1_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad1_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad2🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad2_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad2_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad2_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad3🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad3_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad3_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad3_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad4🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad4_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad4_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad4_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad5🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad5_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad5_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad5_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad6🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad6_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad6_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad6_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad7🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad7_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad7_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad7_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad8🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad8_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad8_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad8_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad9🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad9_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad9_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad9_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad10🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad10_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad10_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad10_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad11🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad11_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad11_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad11_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad12🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad12_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad12_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad12_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad13🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad13_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad13_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad13_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },

                    //----Actividad14🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad14_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad14_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad14_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },


                    //----Actividad15🎯
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Actividad15_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$arrayElemAt": ["$nombre_maestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_maestro_enlazado": { "$ifNull": ["$nombre_maestro_enlazado", ""] }
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_maestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_maestro_enlazado": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }


                    //🎯
                    , {
                        "$addFields": {
                            "Actividad15_maestro_enlazado_nombre": { "$ifNull": ["$nombre_maestro_enlazado", ""] },
                            "Actividad15_maestro_enlazado_valor": { "$ifNull": ["$valor_maestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0

                            , "nombre_maestro_enlazado": 0
                            , "valor_maestro_enlazado": 0
                        }
                    },



                    //==========ARRAY



                    {
                        "$addFields": {
                            "array_actividades": [
                                {
                                    "actividad": "$Actividad1",
                                    "jornales": "$Jornales Actividad1",
                                    "codigo": "1"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad1_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad1_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad2",
                                    "jornales": "$Jornales Actividad2",
                                    "codigo": "2"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad2_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad2_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad3",
                                    "jornales": "$Jornales Actividad3",
                                    "codigo": "3"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad3_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad3_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad4",
                                    "jornales": "$Jornales Actividad4",
                                    "codigo": "4"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad4_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad4_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad5",
                                    "jornales": "$Jornales Actividad5",
                                    "codigo": "5"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad5_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad5_maestro_enlazado_valor"
                                },

                                {
                                    "actividad": "$Actividad6",
                                    "jornales": "$Jornales Actividad6",
                                    "codigo": "6"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad6_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad6_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad7",
                                    "jornales": "$Jornales Actividad7",
                                    "codigo": "7"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad7_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad7_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad8",
                                    "jornales": "$Jornales Actividad8",
                                    "codigo": "8"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad8_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad8_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad9",
                                    "jornales": "$Jornales Actividad9",
                                    "codigo": "9"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad9_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad9_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad10",
                                    "jornales": "$Jornales Actividad10",
                                    "codigo": "10"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad10_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad10_maestro_enlazado_valor"
                                },

                                {
                                    "actividad": "$Actividad11",
                                    "jornales": "$Jornales Actividad11",
                                    "codigo": "11"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad11_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad11_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad12",
                                    "jornales": "$Jornales Actividad12",
                                    "codigo": "12"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad12_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad12_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad13",
                                    "jornales": "$Jornales Actividad13",
                                    "codigo": "13"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad13_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad13_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad14",
                                    "jornales": "$Jornales Actividad14",
                                    "codigo": "14"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad14_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad14_maestro_enlazado_valor"
                                },
                                {
                                    "actividad": "$Actividad15",
                                    "jornales": "$Jornales Actividad15",
                                    "codigo": "15"

                                    ,"actividad_maestro_enlazado_nombre": "$Actividad15_maestro_enlazado_nombre"
                                    ,"actividad_maestro_enlazado_valor": "$Actividad15_maestro_enlazado_valor"
                                }

                            ]
                        }
                    }
                    , {
                        "$addFields": {
                            "array_actividades": {
                                "$filter": {
                                    "input": "$array_actividades",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.actividad", ""]
                                    }
                                }
                            }
                        }
                    }



                    , {
                        "$project": {
                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "Point": 0

                            , "Actividad1": 0
                            , "Jornales Actividad1": 0

                            , "Actividad2": 0
                            , "Jornales Actividad2": 0

                            , "Actividad3": 0
                            , "Jornales Actividad3": 0

                            , "Actividad4": 0
                            , "Jornales Actividad4": 0

                            , "Actividad5": 0
                            , "Jornales Actividad5": 0

                            , "Actividad6": 0
                            , "Jornales Actividad6": 0

                            , "Actividad7": 0
                            , "Jornales Actividad7": 0

                            , "Actividad8": 0
                            , "Jornales Actividad8": 0

                            , "Actividad9": 0
                            , "Jornales Actividad9": 0

                            , "Actividad10": 0
                            , "Jornales Actividad10": 0

                            , "Actividad11": 0
                            , "Jornales Actividad11": 0

                            , "Actividad12": 0
                            , "Jornales Actividad12": 0

                            , "Actividad13": 0
                            , "Jornales Actividad13": 0

                            , "Actividad14": 0
                            , "Jornales Actividad14": 0

                            , "Actividad15": 0
                            , "Jornales Actividad15": 0


                        }
                    }



                    , { "$unwind": "$array_actividades" }

                    , {
                        "$addFields": {
                            "CODIGO_PLANEACION_ACTIVIDAD": {
                                "$concat": ["$CODIGO_PLANEACION", "$array_actividades.codigo"]
                            }
                        }
                    }

                    , {
                        "$project": {
                            "Finca": "$Finca",
                            "Lotes": "$Lotes",

                            "Actividad": "$array_actividades.actividad",

                            "Actividad_maestro_enlazado_nombre": "$array_actividades.actividad_maestro_enlazado_nombre",
                            "Actividad_maestro_enlazado_valor": "$array_actividades.actividad_maestro_enlazado_valor",

                            "Jornales": "$array_actividades.jornales",

                            "Fecha inicial": "$Fecha inicial",
                            "Fecha final": "$Fecha final",

                            "supervisor": "$supervisor",
                            "capture": "$capture",
                            "rgDate": "$rgDate"

                            ,"CODIGO_PLANEACION_ACTIVIDAD": "$CODIGO_PLANEACION_ACTIVIDAD"
                            ,"CODIGO_PLANEACION": "$CODIGO_PLANEACION"
                            , "Nota": "El CODIGO_PLANEACION_ACTIVIDAD se necesita para la Ejecucion"

                        }
                    }

                    , {
                        "$match": {
                            "Actividad": { "$exists": true }
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]

)
