db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_planeacionycumplimientodeactividades",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //========= GENERAR CODIGO UNICO POR REGISTRO
                    {
                        "$addFields": {
                            //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
                            "_id_date": { "$toDate": "$rgDate" }
                        }
                    }
                    //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

                    , {
                        "$addFields": {
                            "date_unix": { "$toLong": "$_id_date" }
                        }
                    }
                    //"date_unix" : 1618521716000,  // 13 numeros



                    //----Codigo a generar
                    //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
                    // 1618521716000 = XX18521716XXX = 18521716
                    , {
                        "$addFields": {
                            "date_unix_min": {
                                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                            }
                        }
                    }



                    //---------ASIGNAR CODIGO DE PLANEACION DE ACTIVIDADES
                    , {
                        "$addFields": {
                            "CODIGO_PLANEACION": "$date_unix_min"
                        }
                    }






                    //========= ESTANDARIZAR DATOS
                    //---array de actividades planeadas
                    , {
                        "$addFields": {
                            "array_actividades": [
                                {
                                    "actividad": "$Actividad1",
                                    "jornales": "$Jornales Actividad1",
                                    "codigo": "1"
                                },
                                {
                                    "actividad": "$Actividad2",
                                    "jornales": "$Jornales Actividad2",
                                    "codigo": "2"
                                },
                                {
                                    "actividad": "$Actividad3",
                                    "jornales": "$Jornales Actividad3",
                                    "codigo": "3"
                                },
                                {
                                    "actividad": "$Actividad4",
                                    "jornales": "$Jornales Actividad4",
                                    "codigo": "4"
                                },
                                {
                                    "actividad": "$Actividad5",
                                    "jornales": "$Jornales Actividad5",
                                    "codigo": "5"
                                },
                                //--
                                {
                                    "actividad": "$Actividad6",
                                    "jornales": "$Jornales Actividad6",
                                    "codigo": "6"
                                },
                                {
                                    "actividad": "$Actividad7",
                                    "jornales": "$Jornales Actividad7",
                                    "codigo": "7"
                                },
                                {
                                    "actividad": "$Actividad8",
                                    "jornales": "$Jornales Actividad8",
                                    "codigo": "8"
                                },
                                {
                                    "actividad": "$Actividad9",
                                    "jornales": "$Jornales Actividad9",
                                    "codigo": "9"
                                },
                                {
                                    "actividad": "$Actividad10",
                                    "jornales": "$Jornales Actividad10",
                                    "codigo": "10"
                                },
                                //--
                                {
                                    "actividad": "$Actividad11",
                                    "jornales": "$Jornales Actividad11",
                                    "codigo": "11"
                                },
                                {
                                    "actividad": "$Actividad12",
                                    "jornales": "$Jornales Actividad12",
                                    "codigo": "12"
                                },
                                {
                                    "actividad": "$Actividad13",
                                    "jornales": "$Jornales Actividad13",
                                    "codigo": "13"
                                },
                                {
                                    "actividad": "$Actividad14",
                                    "jornales": "$Jornales Actividad14",
                                    "codigo": "14"
                                },
                                {
                                    "actividad": "$Actividad15",
                                    "jornales": "$Jornales Actividad15",
                                    "codigo": "15"
                                },

                            ]
                        }
                    }

                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_actividades": {
                                "$filter": {
                                    "input": "$array_actividades",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.actividad", ""]
                                    }
                                }
                            }
                        }
                    }



                    //---no mostrar datos
                    , {
                        "$project": {
                            "uid": 0,
                            "uDate": 0,
                            "Formula": 0,
                            "Point": 0

                            //---variables despues de estandarizar datos
                            , "Actividad1": 0
                            , "Jornales Actividad1": 0

                            , "Actividad2": 0
                            , "Jornales Actividad2": 0

                            , "Actividad3": 0
                            , "Jornales Actividad3": 0

                            , "Actividad4": 0
                            , "Jornales Actividad4": 0

                            , "Actividad5": 0
                            , "Jornales Actividad5": 0
                            //--
                            , "Actividad6": 0
                            , "Jornales Actividad6": 0

                            , "Actividad7": 0
                            , "Jornales Actividad7": 0

                            , "Actividad8": 0
                            , "Jornales Actividad8": 0

                            , "Actividad9": 0
                            , "Jornales Actividad9": 0

                            , "Actividad10": 0
                            , "Jornales Actividad10": 0
                            //--
                            , "Actividad11": 0
                            , "Jornales Actividad11": 0

                            , "Actividad12": 0
                            , "Jornales Actividad12": 0

                            , "Actividad13": 0
                            , "Jornales Actividad13": 0

                            , "Actividad14": 0
                            , "Jornales Actividad14": 0

                            , "Actividad15": 0
                            , "Jornales Actividad15": 0


                        }
                    }



                    //============ GENERAR CODIGOS POR ACTIVIDAD

                    //--filtrar actividades vacias
                    , { "$unwind": "$array_actividades" }

                    //---concatenar codigos de planeacion con actividad (agregar -n al condigo)
                    , {
                        "$addFields": {
                            "CODIGO_PLANEACION_ACTIVIDAD": {
                                "$concat": ["$CODIGO_PLANEACION", "$array_actividades.codigo"]
                            }
                        }
                    }



                    //============ VALORES FINALES DE REPORTE
                    , {
                        "$project": {

                            //---variables base
                            "CODIGO_PLANEACION": "$CODIGO_PLANEACION", //general
                            "Finca": "$Finca",
                            "Lotes": "$Lotes",

                            "Actividad": "$array_actividades.actividad",
                            "Jornales": "$array_actividades.jornales",
                            "CODIGO_PLANEACION_ACTIVIDAD": "$CODIGO_PLANEACION_ACTIVIDAD", //unico-especifico


                            //---variables otras
                            "Fecha inicial": "$Fecha inicial",
                            "Fecha final": "$Fecha final",

                            "supervisor": "$supervisor",
                            "capture": "$capture",
                            "rgDate": "$rgDate"

                            //---variables extras
                            , "Nota": "El CODIGO_PLANEACION_ACTIVIDAD se necesita para la Ejecucion"

                        }
                    }



                    //---condicion final
                    , {
                        "$match": {
                            "Actividad": { "$exists": true }
                        }
                    }







                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
