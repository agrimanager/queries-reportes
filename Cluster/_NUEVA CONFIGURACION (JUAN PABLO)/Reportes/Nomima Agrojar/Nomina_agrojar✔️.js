[



    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_nominaagrojar",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca_point"
                    }
                },
                { "$unwind": "$finca_point" },
                { "$addFields": { "finca_point": { "$ifNull": ["$finca_point.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }


                , {
                    "$match": {
                        "Colaborador": { "$ne": "" }
                    }
                },

                {
                    "$addFields": {
                        "Empleados": {
                            "$cond": {
                                "if": { "$eq": [{ "$type": "$Colaborador" }, "array"] },
                                "then": "$Colaborador",
                                "else": { "$map": { "input": { "$objectToArray": "$Colaborador" }, "as": "employeeKV", "in": "$$employeeKV.v" } }
                            }
                        }
                    }
                }

                , {
                    "$unwind": {
                        "path": "$Empleados",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$addFields": {
                        "empleado_oid": { "$toObjectId": "$Empleados._id" }
                    }
                }

                , {
                    "$lookup": {
                        "from": "employees",
                        "localField": "empleado_oid",
                        "foreignField": "_id",
                        "as": "info_empleado"
                    }
                }
                , {
                    "$unwind": {
                        "path": "$info_empleado",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$addFields": {
                        "empleado seleccion": "$Empleados.name"

                        , "empleado nombre y apellidos": {
                            "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                        }

                        , "empleado codigo": "$info_empleado.code"
                        , "empleado cedula": "$info_empleado.numberID"
                    }
                }

                , {
                    "$project": {
                        "info_empleado": 0
                        , "Colaborador": 0
                        , "Empleados": 0

                        , "Nombre Empleado": 0
                        , "Point": 0
                        , "Formula": 0
                        , "uid": 0

                        , "empleado_oid": 0
                    }
                }



                , {
                    "$addFields": {
                        "Fecha": {
                            "$dateToString": {
                                "date": "$Fecha",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }
                    }
                }

            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
