[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_solicitudpedidoscluster",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$gte": ["$rgDate", "$$filtro_fecha_inicio"] },
                                { "$lte": ["$rgDate", "$$filtro_fecha_fin"] }
                            ]
                        }
                    }
                },

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }


                , {
                    "$addFields": {
                        "array_productos": [

                            {
                                "producto": { "$ifNull": ["$Producto 1", ""] },

                                "cantidad": { "$ifNull": ["$Cantidad Producto 1", 0] },


                                "observacion": { "$ifNull": ["$Observacion Producto 1", ""] }

                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 1", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 1", 0] }

                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 1", ""] }
                                , "num_producto": "Producto 1"
                            },

                            {
                                "producto": { "$ifNull": ["$Producto 2", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 2", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 2", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 2", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 2", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 2", ""] }
                                , "num_producto": "Producto 2"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 3", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 3", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 3", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 3", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 3", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 3", ""] }
                                , "num_producto": "Producto 3"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 4", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 4", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 4", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 4", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 4", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 4", ""] }
                                , "num_producto": "Producto 4"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 5", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 5", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 5", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 5", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 5", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 5", ""] }
                                , "num_producto": "Producto 5"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 6", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 6", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 6", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 6", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 6", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 6", ""] }
                                , "num_producto": "Producto 6"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 7", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 7", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 7", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 7", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 7", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 7", ""] }
                                , "num_producto": "Producto 7"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 8", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 8", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 8", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 8", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 8", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 8", ""] }
                                , "num_producto": "Producto 8"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 9", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 9", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 9", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 9", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 9", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 9", ""] }
                                , "num_producto": "Producto 9"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 10", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 10", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 10", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 10", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 10", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 10", ""] }
                                , "num_producto": "Producto 10"

                            }


                            , {
                                "producto": { "$ifNull": ["$Producto 11", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 11", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 11", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 11", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 11", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 11", ""] }
                                , "num_producto": "Producto 11"
                            },

                            {
                                "producto": { "$ifNull": ["$Producto 12", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 12", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 12", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 12", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 12", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 12", ""] }
                                , "num_producto": "Producto 12"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 13", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 13", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 13", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 13", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 13", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 13", ""] }
                                , "num_producto": "Producto 13"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 14", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 14", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 14", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 14", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 14", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 14", ""] }
                                , "num_producto": "Producto 14"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 15", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 15", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 15", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 15", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 15", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 15", ""] }
                                , "num_producto": "Producto 15"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 16", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 16", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 16", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 16", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 16", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 16", ""] }
                                , "num_producto": "Producto 16"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 17", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 17", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 17", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 17", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 17", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 17", ""] }
                                , "num_producto": "Producto 17"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 18", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 18", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 18", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 18", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 18", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 18", ""] }
                                , "num_producto": "Producto 18"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 19", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 19", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 19", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 19", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 19", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 19", ""] }
                                , "num_producto": "Producto 19"
                            },
                            {
                                "producto": { "$ifNull": ["$Producto 20", ""] },
                                "cantidad": { "$ifNull": ["$Cantidad Producto 20", 0] },
                                "observacion": { "$ifNull": ["$Observacion Producto 20", ""] }
                                , "despacho_producto": { "$ifNull": ["$Despacho Producto 20", "viejo"] }
                                , "cantidad_enviada": { "$ifNull": ["$Cantidad Enviada 20", 0] }
                                , "remplazo_producto": { "$ifNull": ["$Remplazo Producto 20", ""] }
                                , "num_producto": "Producto 20"

                            }
                        ]
                    }
                }



                , {
                    "$addFields": {
                        "array_productos": {
                            "$filter": {
                                "input": "$array_productos",
                                "as": "item",
                                "cond": {
                                    "$ne": ["$$item.producto", ""]
                                }
                            }
                        }
                    }
                }

                , {
                    "$unwind": {
                        "path": "$array_productos",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$lookup": {
                        "from": "supplies",
                        "localField": "array_productos.producto",
                        "foreignField": "name",
                        "as": "info_producto"
                    }
                },
                { "$unwind": "$info_producto" },
                { "$addFields": { "producto_sku": { "$ifNull": ["$info_producto.sku", ""] } } },



                {
                    "$addFields": {
                        "Fecha de solicitud": {
                            "$dateToString": {
                                "date": "$Fecha de solicitud",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }
                    }
                }


                , {
                    "$project": {
                        "finca_point": "$finca"
                        , "Finca": "$Finca"
                        , "Fecha de solicitud": "$Fecha de solicitud"


                        , "Producto": "$array_productos.producto"
                        , "Producto SKU": "$producto_sku"
                        , "Cantidad": "$array_productos.cantidad"
                        , "Observacion": "$array_productos.observacion"

                        , "Despacho": "$array_productos.despacho_producto"
                        , "Cantidad enviada": "$array_productos.cantidad_enviada"

                        , "Remplazo": "$array_productos.remplazo_producto"
                        , "num_producto": "$array_productos.num_producto"

                        , "supervisor": "$supervisor"
                        , "rgDate": "$rgDate"

                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



]
