//🧩📆Recomendacion Edafica V.S ⚽Ejecucion de Aplicacion
db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_recomendacionedafica",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //========= GENERAR CODIGO UNICO POR REGISTRO
                    {
                        "$addFields": {
                            //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
                            "_id_date": { "$toDate": "$rgDate" }
                        }
                    }
                    //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

                    , {
                        "$addFields": {
                            "date_unix": { "$toLong": "$_id_date" }
                        }
                    }
                    //"date_unix" : 1618521716000,  // 13 numeros



                    //----Codigo a generar
                    //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
                    // 1618521716000 = XX18521716XXX = 18521716
                    , {
                        "$addFields": {
                            "date_unix_min": {
                                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                            }
                        }
                    }



                    //---------ASIGNAR CODIGO
                    , {
                        "$addFields": {
                            "CODIGO_FECHA": "$date_unix_min"
                        }
                    }






                    //========= ESTANDARIZAR DATOS
                    //---array de productos
                    , {
                        "$addFields": {
                            "array_productos": [
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 1", ""] },
                                    "cantidad": "$Relacion 1",
                                    "observacion": "$Observacion 1"
                                },

                                {
                                    "producto": { "$ifNull": ["$Fertilizante 2", ""] },
                                    "cantidad": "$Relacion 2",
                                    "observacion": "$Observacion 2"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 3", ""] },
                                    "cantidad": "$Relacion 3",
                                    "observacion": "$Observacion 3"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 4", ""] },
                                    "cantidad": "$Relacion 4",
                                    "observacion": "$Observacion 4"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 5", ""] },
                                    "cantidad": "$Relacion 5",
                                    "observacion": "$Observacion 5"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 6", ""] },
                                    "cantidad": "$Relacion 6",
                                    "observacion": "$Observacion 6"
                                }


                            ]
                        }
                    }

                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_productos": {
                                "$filter": {
                                    "input": "$array_productos",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.producto", ""]
                                    }
                                }
                            }
                        }
                    }

                    // , { "$unwind": "$array_productos" }


                    , {
                        "$addFields": {
                            "array_productos_size": { "$size": "$array_productos" }
                        }
                    }

                    //---------CONCATENAR INFOMRACION
                    , {
                        "$addFields": {
                            "info_concat_productos": {
                                "$reduce": {
                                    "input": "$array_productos",
                                    "initialValue": "",
                                    "in": {
                                        "$concat": [

                                            "( "
                                            , "$$this.producto"
                                            , ":"
                                            , { "$toString": "$$this.cantidad" }
                                            , " ) "
                                            , "$$value"
                                        ]
                                    }
                                }
                            }
                        }
                    }




                    //============ VALORES FINALES DE REPORTE
                    , {
                        "$project": {

                            //---variables base
                            "Tipo Recomendacion": "Recomendacion Edafica",
                            "CODIGO DE LA APLICACION": "$CODIGO_FECHA",
                            "Finca": "$Finca",
                            "Tipo de Cultivo": "$Cultivo",
                            "Estado de Cultivo": "$Estado Del Cultivo",
                            "Lote": "$Lote",
                            // "Volumen de Agua Recomendado": "$Volumen de Agua Recomendado",

                            "Fecha Visita": "$Fecha Visita",
                            "Fecha de aplicacion": "$Fecha de aplicacion",

                            //reporte vs
                            "Numero de Productos": "$array_productos_size",
                            "Informacion de Productos": "$info_concat_productos",

                            //---variables otras
                            "supervisor": "$supervisor",
                            "capture": "$capture",
                            "rgDate": "$rgDate"

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }





        //============= CRUZAR CON EJECUCION
        , {
            "$lookup": {
                "from": "form_ejecuciondeaplicacion",
                "as": "data_ejecucion",
                "let": {
                    "codigo_aplicacion": "$CODIGO DE LA APLICACION"
                    // ,"finca":"$Finca"
                },
                //query
                "pipeline": [

                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                  // { "$eq": ["$$codigo_aplicacion", "$CODIGO DE LA APLICACION"] }//falla
                                  { "$eq": [{"$toString":"$$codigo_aplicacion"}, {"$toString":"$CODIGO DE LA APLICACION"}] }
                                    // ,{"$eq": ["$$finca", "$Finca"]}
                                ]
                            }
                        }
                    }

                    //LIMIT ??  -- duda

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_ejecucion",
                "preserveNullAndEmptyArrays": true
            }
        }


        //----->>>>>>>>> duda: hacer reporte de ejecucion
        //----DUDA POR QUE LOS EMPLEADOS DE LA EJECUCION NO HAY REPORTES

        , {
            "$addFields": {
                "tiene_empleados": { "$ifNull": ["$data_ejecucion.Empleados", "sin_empleados"] }
            }
        }

        //--sacar las variables de ejecucion
        , {
            "$addFields": {

                //--info base
                "Finca EJECUCION": { "$ifNull": ["$data_ejecucion.Finca", "--sin datos--"] }
                , "Fecha Inicio EJECUCION": { "$ifNull": ["$data_ejecucion.Fecha Inicio", "--sin datos--"] }
                , "Fecha final EJECUCION": { "$ifNull": ["$data_ejecucion.Fecha final", "--sin datos--"] }
                , "Estado del tiempo EJECUCION": { "$ifNull": ["$data_ejecucion.Estado del tiempo", "--sin datos--"] }
                , "Maquinaria EJECUCION": { "$ifNull": ["$data_ejecucion.Maquinaria", "--sin datos--"] }
                , "Cantidad Aplicada EJECUCION": { "$ifNull": ["$data_ejecucion.Cantidad Aplicada", "--sin datos--"] }
                , "Unidad EJECUCION": { "$ifNull": ["$data_ejecucion.Unidad", "--sin datos--"] }
                , "Observaciones EJECUCION": { "$ifNull": ["$data_ejecucion.Observaciones", "--sin datos--"] }

                //--info empleados
                , "Numero de Empleados EJECUCION": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_empleados", "sin_empleados"] },
                        "then": "--sin datos--",
                        "else": {
                            "$size": "$data_ejecucion.Empleados"
                        }
                    }
                }
                , "Informacion de Empleados EJECUCION": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_empleados", "sin_empleados"] },
                        "then": "--sin datos--",
                        //---concatenar info
                        "else": {
                            "$reduce": {
                                "input": "$data_ejecucion.Empleados",
                                "initialValue": "",
                                "in": {
                                    "$concat": [

                                        "( "
                                        , "$$this.name"
                                        , " ) "
                                        , "$$value"
                                    ]
                                }
                            }
                        }
                    }
                }


            }
        }


        , {
            "$project":{
                "data_ejecucion": 0
                ,"tiene_empleados": 0
            }
        }




    ]
)
