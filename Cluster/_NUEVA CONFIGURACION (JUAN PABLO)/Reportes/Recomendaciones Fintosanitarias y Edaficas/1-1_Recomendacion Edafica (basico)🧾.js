[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_recomendacionedafica",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [


                {
                    "$addFields": {
                        "_id_date": { "$toDate": "$rgDate" }
                    }
                }
                , {
                    "$addFields": {
                        "date_unix": { "$toLong": "$_id_date" }
                    }
                }

                , {
                    "$addFields": {
                        "date_unix_min": {
                            "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                        }
                    }
                }



                , {
                    "$addFields": {
                        "CODIGO_FECHA": "$date_unix_min"
                    }
                }






                , {
                    "$addFields": {
                        "array_productos": [
                              {
                                "producto": { "$ifNull": ["$Fertilizante 1", ""] },
                                "cantidad": "$Relacion 1",
                                "observacion": "$Observacion 1"
                            },

                            {
                                "producto": { "$ifNull": ["$Fertilizante 2", ""] },
                                "cantidad": "$Relacion 2",
                                "observacion": "$Observacion 2"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 3", ""] },
                                "cantidad": "$Relacion 3",
                                "observacion": "$Observacion 3"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 4", ""] },
                                "cantidad": "$Relacion 4",
                                "observacion": "$Observacion 4"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 5", ""] },
                                "cantidad": "$Relacion 5",
                                "observacion": "$Observacion 5"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 6", ""] },
                                "cantidad": "$Relacion 6",
                                "observacion": "$Observacion 6"
                            }


                        ]
                    }
                }
                , {
                    "$addFields": {
                        "array_productos": {
                            "$filter": {
                                "input": "$array_productos",
                                "as": "item",
                                "cond": {
                                    "$ne": ["$$item.producto", ""]
                                }
                            }
                        }
                    }
                }



                , {
                    "$project": {


                        "Tipo Recomendacion":"Recomendacion Edafica",
                        "CODIGO_EJECUCION": "$CODIGO_FECHA",
                        "Finca": "$Finca",
                        "Tipo de Cultivo": "$Cultivo",
                        "Estado de Cultivo": "$Estado Del Cultivo",
                        "Lote": "$Lote",

                        "Fecha Visita": "$Fecha Visita",
                        "Fecha de aplicacion": "$Fecha de aplicacion",



                        "Informacion de Productos": "$array_productos",


                        "supervisor": "$supervisor",
                        "capture": "$capture",
                        "rgDate": "$rgDate"


                        , "Nota": "El CODIGO_EJECUCION se necesita para la Ejecucion"



                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]
