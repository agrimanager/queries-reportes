db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_recomendacionfitosanitaria",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //========= GENERAR CODIGO UNICO POR REGISTRO
                    {
                        "$addFields": {
                            //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
                            "_id_date": { "$toDate": "$rgDate" }
                        }
                    }
                    //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

                    , {
                        "$addFields": {
                            "date_unix": { "$toLong": "$_id_date" }
                        }
                    }
                    //"date_unix" : 1618521716000,  // 13 numeros



                    //----Codigo a generar
                    //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
                    // 1618521716000 = XX18521716XXX = 18521716
                    , {
                        "$addFields": {
                            "date_unix_min": {
                                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                            }
                        }
                    }



                    //---------ASIGNAR CODIGO
                    , {
                        "$addFields": {
                            "CODIGO_FECHA": "$date_unix_min"
                        }
                    }






                    //========= ESTANDARIZAR DATOS
                    //---array de productos
                    , {
                        "$addFields": {
                            "array_productos": [
                                {
                                    "producto": { "$ifNull": ["$Producto comercial 1", ""] },
                                    "cantidad": "$Dosis 1",
                                    "unidad": "$Unidad 1",
                                    "observacion": "$Observacion 1"
                                },

                                {
                                    "producto": { "$ifNull": ["$Producto comercial 2", ""] },
                                    "cantidad": "$Dosis 2",
                                    "unidad": "$Unidad 2",
                                    "observacion": "$Observacion 2"
                                },
                                {
                                    "producto": { "$ifNull": ["$Producto comercial 3", ""] },
                                    "cantidad": "$Dosis 3",
                                    "unidad": "$Unidad 3",
                                    "observacion": "$Observacion 3"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 4", ""] },
                                    "cantidad": "$Dosis 4",
                                    "unidad": "$Unidad 4",
                                    "observacion": "$Observacion 4"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 5", ""] },
                                    "cantidad": "$Dosis 5",
                                    "unidad": "$Unidad 5",
                                    "observacion": "$Observacion 5"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 6", ""] },
                                    "cantidad": "$Dosis 6",
                                    "unidad": "$Unidad 6",
                                    "observacion": "$Observacion 6"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 7", ""] },
                                    "cantidad": "$Dosis 7",
                                    "unidad": "$Unidad 7",
                                    "observacion": "$Observacion 7"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 8", ""] },
                                    "cantidad": "$Dosis 8",
                                    "unidad": "$Unidad 8",
                                    "observacion": "$Observacion 8"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 9", ""] },
                                    "cantidad": "$Dosis 9",
                                    "unidad": "$Unidad 9",
                                    "observacion": "$Observacion 9"
                                }, {
                                    "producto": { "$ifNull": ["$Producto comercial 10", ""] },
                                    "cantidad": "$Dosis 10",
                                    "unidad": "$Unidad 10",
                                    "observacion": "$Observacion 10"
                                }


                            ]
                        }
                    }

                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_productos": {
                                "$filter": {
                                    "input": "$array_productos",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.producto", ""]
                                    }
                                }
                            }
                        }
                    }


                    , { "$unwind": "$array_productos" }




                    //============ VALORES FINALES DE REPORTE
                    , {
                        "$project": {

                            //---variables base
                            "Tipo Recomendacion": "Recomendacion Fitosanitaria",
                            "CODIGO DE LA APLICACION": "$CODIGO_FECHA", //general
                            "Finca": "$Finca",
                            "Tipo de Cultivo": "$Cultivo",
                            "Estado de Cultivo": "$Estado Del Cultivo",
                            "Lote": "$Lote",
                            "Volumen de Agua Recomendado": "$Volumen de Agua Recomendado",

                            "Fecha Visita": "$Fecha Visita",
                            "Fecha de aplicacion": "$Fecha de aplicacion",


                            //---info productos
                            //reporte basico
                            //"Informacion de Productos": "$array_productos",//---muestra [object,object]

                            //reporte detallado
                            "producto": "$array_productos.producto",
                            "cantidad": "$array_productos.cantidad",
                            "unidad": "$array_productos.unidad",
                            "observacion": "$array_productos.observacion",

                            //---variables otras
                            "supervisor": "$supervisor",
                            "capture": "$capture",
                            "rgDate": "$rgDate"

                            //---variables extras
                            , "Nota": "El CODIGO_EJECUCION se necesita para la Ejecucion"



                            //   //---info fechas
                            // "Fecha": {
                            //     "$dateToString": {
                            //         "date": "$rgDate",
                            //         "format": "%Y-%m-%d",
                            //         "timezone": "America/Bogota"
                            //     }
                            // },

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
