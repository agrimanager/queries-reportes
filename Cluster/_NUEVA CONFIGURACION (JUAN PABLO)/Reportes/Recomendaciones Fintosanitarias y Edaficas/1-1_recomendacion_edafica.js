db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------



        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_recomendacionedafica",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2
                },
                //query
                "pipeline": [


                    //========= GENERAR CODIGO UNICO POR REGISTRO
                    {
                        "$addFields": {
                            //"_id_date": { "$toDate": "$_id" }//-----❌ hubieron repetidos
                            "_id_date": { "$toDate": "$rgDate" }
                        }
                    }
                    //"_id_date" : ISODate("2021-04-15T16:21:56.000-05:00"),

                    , {
                        "$addFields": {
                            "date_unix": { "$toLong": "$_id_date" }
                        }
                    }
                    //"date_unix" : 1618521716000,  // 13 numeros



                    //----Codigo a generar
                    //--->>quitar lo los 2 primeros numeros y los 3 ultimos numeros
                    // 1618521716000 = XX18521716XXX = 18521716
                    , {
                        "$addFields": {
                            "date_unix_min": {
                                "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                            }
                        }
                    }



                    //---------ASIGNAR CODIGO
                    , {
                        "$addFields": {
                            "CODIGO_FECHA": "$date_unix_min"
                        }
                    }






                    //========= ESTANDARIZAR DATOS
                    //---array de productos
                    , {
                        "$addFields": {
                            "array_productos": [
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 1", ""] },
                                    "cantidad": "$Relacion 1",
                                    "observacion": "$Observacion 1"
                                },

                                {
                                    "producto": { "$ifNull": ["$Fertilizante 2", ""] },
                                    "cantidad": "$Relacion 2",
                                    "observacion": "$Observacion 2"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 3", ""] },
                                    "cantidad": "$Relacion 3",
                                    "observacion": "$Observacion 3"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 4", ""] },
                                    "cantidad": "$Relacion 4",
                                    "observacion": "$Observacion 4"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 5", ""] },
                                    "cantidad": "$Relacion 5",
                                    "observacion": "$Observacion 5"
                                },
                                {
                                    "producto": { "$ifNull": ["$Fertilizante 6", ""] },
                                    "cantidad": "$Relacion 6",
                                    "observacion": "$Observacion 6"
                                }


                            ]
                        }
                    }

                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_productos": {
                                "$filter": {
                                    "input": "$array_productos",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.producto", ""]
                                    }
                                }
                            }
                        }
                    }




                    //============ VALORES FINALES DE REPORTE
                    , {
                        "$project": {

                            //---variables base
                            "Tipo Recomendacion": "Recomendacion Edafica",
                            "CODIGO DE LA APLICACION": "$CODIGO_FECHA", //general
                            "Finca": "$Finca",
                            "Tipo de Cultivo": "$Cultivo",
                            "Estado de Cultivo": "$Estado Del Cultivo",
                            "Lote": "$Lote",
                            // "Volumen de Agua Recomendado": "$Volumen de Agua Recomendado",

                            "Fecha Visita": "$Fecha Visita",
                            "Fecha de aplicacion": "$Fecha de aplicacion",


                            // //---info productos
                            "Informacion de Productos": "$array_productos",//---muestra [object,object]
                            // "Informacion de Productos": {"$toString":"$array_productos"},

                            //---variables otras
                            "supervisor": "$supervisor",
                            "capture": "$capture",
                            "rgDate": "$rgDate"

                            //---variables extras
                            , "Nota": "El CODIGO_EJECUCION se necesita para la Ejecucion"

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }


    ]
)
