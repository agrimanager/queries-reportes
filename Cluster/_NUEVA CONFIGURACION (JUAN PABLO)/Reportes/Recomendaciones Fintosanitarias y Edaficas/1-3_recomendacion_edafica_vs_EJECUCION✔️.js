[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_recomendacionedafica",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [



                {
                    "$addFields": {
                        "_id_date": { "$toDate": "$rgDate" }
                    }
                }


                , {
                    "$addFields": {
                        "date_unix": { "$toLong": "$_id_date" }
                    }
                }

                , {
                    "$addFields": {
                        "date_unix_min": {
                            "$substr": [{ "$toString": "$date_unix" }, 2, 8]
                        }
                    }
                }



                , {
                    "$addFields": {
                        "CODIGO_FECHA": "$date_unix_min"
                    }
                }

                , {
                    "$addFields": {
                        "array_productos": [
                            {
                                "producto": { "$ifNull": ["$Fertilizante 1", ""] },
                                "cantidad": "$Relacion 1",
                                "observacion": "$Observacion 1"
                            },

                            {
                                "producto": { "$ifNull": ["$Fertilizante 2", ""] },
                                "cantidad": "$Relacion 2",
                                "observacion": "$Observacion 2"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 3", ""] },
                                "cantidad": "$Relacion 3",
                                "observacion": "$Observacion 3"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 4", ""] },
                                "cantidad": "$Relacion 4",
                                "observacion": "$Observacion 4"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 5", ""] },
                                "cantidad": "$Relacion 5",
                                "observacion": "$Observacion 5"
                            },
                            {
                                "producto": { "$ifNull": ["$Fertilizante 6", ""] },
                                "cantidad": "$Relacion 6",
                                "observacion": "$Observacion 6"
                            }


                        ]
                    }
                }


                , {
                    "$addFields": {
                        "array_productos": {
                            "$filter": {
                                "input": "$array_productos",
                                "as": "item",
                                "cond": {
                                    "$ne": ["$$item.producto", ""]
                                }
                            }
                        }
                    }
                }




                , {
                    "$addFields": {
                        "array_productos_size": { "$size": "$array_productos" }
                    }
                }


                , {
                    "$addFields": {
                        "info_concat_productos": {
                            "$reduce": {
                                "input": "$array_productos",
                                "initialValue": "",
                                "in": {
                                    "$concat": [

                                        "( "
                                        , "$$this.producto"
                                        , ":"
                                        , { "$toString": "$$this.cantidad" }
                                        , " ) "
                                        , "$$value"
                                    ]
                                }
                            }
                        }
                    }
                }





                , {
                    "$project": {


                        "Tipo Recomendacion": "Recomendacion Edafica",
                        "CODIGO DE LA APLICACION": "$CODIGO_FECHA",
                        "Finca": "$Finca",
                        "Tipo de Cultivo": "$Cultivo",
                        "Estado de Cultivo": "$Estado Del Cultivo",
                        "Lote": "$Lote",


                        "Fecha Visita": "$Fecha Visita",
                        "Fecha de aplicacion": "$Fecha de aplicacion",


                        "Numero de Productos": "$array_productos_size",
                        "Informacion de Productos": "$info_concat_productos",


                        "supervisor": "$supervisor",
                        "capture": "$capture",
                        "rgDate": "$rgDate"

                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    , {
        "$lookup": {
            "from": "form_ejecuciondeaplicacion",
            "as": "data_ejecucion",
            "let": {
                "codigo_aplicacion": "$CODIGO DE LA APLICACION"

            },
            "pipeline": [

                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": [{"$toString":"$$codigo_aplicacion"}, {"$toString":"$CODIGO DE LA APLICACION"}] }

                            ]
                        }
                    }
                }
            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_ejecucion",
            "preserveNullAndEmptyArrays": true
        }
    }



    , {
        "$addFields": {
            "tiene_empleados": { "$ifNull": ["$data_ejecucion.Empleados", "sin_empleados"] }
        }
    }


    , {
        "$addFields": {


            "Finca EJECUCION": { "$ifNull": ["$data_ejecucion.Finca", "--sin datos--"] }
            , "Fecha Inicio EJECUCION": { "$ifNull": ["$data_ejecucion.Fecha Inicio", "--sin datos--"] }
            , "Fecha final EJECUCION": { "$ifNull": ["$data_ejecucion.Fecha final", "--sin datos--"] }
            , "Estado del tiempo EJECUCION": { "$ifNull": ["$data_ejecucion.Estado del tiempo", "--sin datos--"] }
            , "Maquinaria EJECUCION": { "$ifNull": ["$data_ejecucion.Maquinaria", "--sin datos--"] }
            , "Cantidad Aplicada EJECUCION": { "$ifNull": ["$data_ejecucion.Cantidad Aplicada", "--sin datos--"] }
            , "Unidad EJECUCION": { "$ifNull": ["$data_ejecucion.Unidad", "--sin datos--"] }
            , "Observaciones EJECUCION": { "$ifNull": ["$data_ejecucion.Observaciones", "--sin datos--"] }


            , "Numero de Empleados EJECUCION": {
                "$cond": {
                    "if": { "$eq": ["$tiene_empleados", "sin_empleados"] },
                    "then": "--sin datos--",
                    "else": {
                        "$size": "$data_ejecucion.Empleados"
                    }
                }
            }
            , "Informacion de Empleados EJECUCION": {
                "$cond": {
                    "if": { "$eq": ["$tiene_empleados", "sin_empleados"] },
                    "then": "--sin datos--",

                    "else": {
                        "$reduce": {
                            "input": "$data_ejecucion.Empleados",
                            "initialValue": "",
                            "in": {
                                "$concat": [

                                    "( "
                                    , "$$this.name"
                                    , " ) "
                                    , "$$value"
                                ]
                            }
                        }
                    }
                }
            }


        }
    }


    , {
        "$project":{
            "data_ejecucion": 0
            ,"tiene_empleados": 0
        }
    }




]
