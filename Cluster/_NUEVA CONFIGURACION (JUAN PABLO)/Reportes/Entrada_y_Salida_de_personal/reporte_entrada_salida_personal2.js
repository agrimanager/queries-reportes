db.users.aggregate(
    [

        //------------------------------------------------------------------
        //---filtros de fechas
        {
            $addFields: {
                "Busqueda inicio": ISODate("2021-09-01T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date
            }
        },
        //----------------------------------------------------------------

        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_formularioentrada",
                "as": "data_entrada",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",  //--filtro_fecha1
                    "filtro_fecha_fin": "$Busqueda fin"         //--filtro_fecha2

                    , "user_timezone": "$timezone"               //--timeZone
                },
                //query
                "pipeline": [

                    //---finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                        }
                    }


                    //---empleados desde empleados_numerico
                    , { "$unwind": "$Empleado" }

                    , {
                        "$addFields": {
                            "empleado_oid": { "$toObjectId": "$Empleado._id" }
                        }
                    }

                    , {
                        "$lookup": {
                            "from": "employees",
                            "localField": "empleado_oid",
                            "foreignField": "_id",
                            "as": "info_empleado"
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$info_empleado",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , {
                        "$addFields": {
                            "empleado seleccion": "$Empleado.name"

                            , "empleado nombre y apellidos": {
                                "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                            }

                            , "empleado codigo": "$info_empleado.code"
                        }
                    }

                    , {
                        "$project": {
                            "info_empleado": 0
                            , "Empleado": 0

                            // , "empleado_oid": 0 //necesario para cruce simple
                        }
                    }



                    //---fecha y hora de ingreso
                    , {
                        "$addFields": {
                            "variable_fecha": "$Hora ingreso" ////----EDITAR
                        }
                    },

                    //comprobar fecha malas
                    // { "$match": { "variable_fecha": { "$ne": "" } } }, //---duda
                    { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
                    { "$match": { "anio": { "$gt": 2000 } } },
                    { "$match": { "anio": { "$lt": 3000 } } },



                    //dia_año
                    {
                        "$addFields": {
                            "num_dia_anio": { "$dayOfYear": { "date": "$variable_fecha" } }
                        }
                    },


                    //NOTA:!!! time_zone (para horas)
                    {
                        "$addFields": {
                            "user_timezone": "$$user_timezone"
                        }
                    },
                    {
                        "$addFields": {
                            "Fecha_entrada_txt": {
                                "$dateToString": {
                                    "date": "$variable_fecha",
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$user_timezone"
                                }
                            }
                        }
                    }



                    //----filtro de fechas
                    ,{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                            ,
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "rgDate": "$variable_fecha"
                        }
                    }

                    , {
                        "$project": {
                            "Punto": 0
                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data_entrada"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //----------------------------------------------
        //---CRUZAR INFO DE ENTRADAS CON INFO DE SALIDAS
        , {
            "$lookup": {
                "from": "form_formulariodesalida",
                "as": "data_salida",
                "let": {

                    //--finca
                    "finca": "$finca"

                    //--empleado
                    , "empleado_oid": "$empleado_oid"

                    //--fechas
                    , "user_timezone": "$user_timezone"
                    , "anio_entrada": "$anio"
                    , "num_dia_anio_entrada": "$num_dia_anio"

                },
                //query
                "pipeline": [

                    //---finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                        }
                    }

                    //============CONDICIONES DE CRUCE
                    ,{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //--finca
                                    { "$eq": ["$$finca", "$finca"] },
                                    // //--empleado
                                    // { "$eq": ["$$empleado_oid", "$empleado_oid"] },
                                    // //--fechas
                                    // { "$eq": ["$$anio_entrada", "$anio"] },
                                    // { "$eq": ["$$num_dia_anio_entrada", "$num_dia_anio"] },
                                ]
                            }
                        }
                    }



                    //---empleados desde empleados_numerico
                    , { "$unwind": "$Empleado" }

                    , {
                        "$addFields": {
                            "empleado_oid": { "$toObjectId": "$Empleado._id" }
                        }
                    }

                    //============CONDICIONES DE CRUCE
                    ,{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //--finca
                                    // { "$eq": ["$$finca", "$finca"] },
                                    // //--empleado
                                    { "$eq": ["$$empleado_oid", "$empleado_oid"] },
                                    // //--fechas
                                    // { "$eq": ["$$anio_entrada", "$anio"] },
                                    // { "$eq": ["$$num_dia_anio_entrada", "$num_dia_anio"] },
                                ]
                            }
                        }
                    }


                    //---fecha y hora de salida
                    , {
                        "$addFields": {
                            "variable_fecha": "$Hora salida" ////----EDITAR
                        }
                    },

                    //comprobar fecha malas
                    // { "$match": { "variable_fecha": { "$ne": "" } } }, //---duda
                    { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
                    { "$match": { "anio": { "$gt": 2000 } } },
                    { "$match": { "anio": { "$lt": 3000 } } },



                    //dia_año
                    {
                        "$addFields": {
                            "num_dia_anio": { "$dayOfYear": { "date": "$variable_fecha" } }
                        }
                    }


                    //============CONDICIONES DE CRUCE
                    ,{
                        "$match": {
                            "$expr": {
                                "$and": [
                                    //--finca
                                    // { "$eq": ["$$finca", "$finca"] },
                                    // //--empleado
                                    // { "$eq": ["$$empleado_oid", "$empleado_oid"] },
                                    // //--fechas
                                    { "$eq": ["$$anio_entrada", "$anio"] },
                                    { "$eq": ["$$num_dia_anio_entrada", "$num_dia_anio"] },
                                ]
                            }
                        }
                    }


                    //NOTA:!!! time_zone (para horas)
                    ,{
                        "$addFields": {
                            "Fecha_salida_txt": {
                                "$dateToString": {
                                    "date": "$variable_fecha",
                                    "format": "%Y-%m-%d %H:%M",
                                    "timezone": "$$user_timezone"
                                }
                            }
                        }
                    }



                    // //============CONDICIONES DE CRUCE
                    // {
                    //     "$match": {
                    //         "$expr": {
                    //             "$and": [
                    //                 //--finca
                    //                 { "$eq": ["$$finca", "$finca"] },
                    //                 //--empleado
                    //                 { "$eq": ["$$empleado_oid", "$empleado_oid"] },
                    //                 //--fechas
                    //                 { "$eq": ["$$anio_entrada", "$anio"] },
                    //                 { "$eq": ["$$num_dia_anio_entrada", "$num_dia_anio"] },
                    //             ]
                    //         }
                    //     }
                    // }


                    // , { "$limit": 1 }

                ]
            }
        }

        , {
            "$unwind": {
                "path": "$data_salida",
                "preserveNullAndEmptyArrays": true
            }
        }


        //---agregar hora_salida
        , {
            "$addFields": {
                "Fecha_salida_txt": { "$ifNull": ["$data_salida.Fecha_salida_txt", "--sin salida--"] }
            }
        }


        //============PROYECCION FINAL

        //---no mostrar
        , {
            "$project": {
                "data_salida": 0

                , "empleado_oid": 0
                , "uDate": 0
                , "Formula": 0
            }
        }



        //---otros fechas

        //---info fechas
        , {
            "$addFields": {
                // "num_anio": { "$year": { "date": "$rgDate" } },
                "num_mes": { "$month": { "date": "$variable_fecha" } },
                "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
                "num_semana": { "$week": { "date": "$variable_fecha" } },
                "num_dia_semana": { "$dayOfWeek": { "date": "$variable_fecha" } }

                //--horas
                //....
            }
        }

        , {
            "$addFields": {
                "Mes_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                            { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                            { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                            { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                            { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                            { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                            { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                            { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                            { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                            { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                            { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                            { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                        ],
                        "default": "Mes desconocido"
                    }
                }


                , "Dia_Txt": {
                    "$switch": {
                        "branches": [
                            { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                            { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                            { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                            { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                            { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                            { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                            { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                        ],
                        "default": "dia de la semana desconocido"
                    }
                }
            }
        },

        //--no mostrar num_dia_semana por que confunde solo mostrar Dia_txt
        { "$project": { "num_dia_semana": 0 } },

    ]
)
// .count()
