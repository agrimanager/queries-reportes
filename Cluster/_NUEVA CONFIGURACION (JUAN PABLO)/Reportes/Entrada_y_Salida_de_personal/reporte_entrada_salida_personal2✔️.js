[


    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_formularioentrada",
            "as": "data_entrada",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"

                , "user_timezone": "$timezone"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }


                , { "$unwind": "$Empleado" }

                , {
                    "$addFields": {
                        "empleado_oid": { "$toObjectId": "$Empleado._id" }
                    }
                }

                , {
                    "$lookup": {
                        "from": "employees",
                        "localField": "empleado_oid",
                        "foreignField": "_id",
                        "as": "info_empleado"
                    }
                }
                , {
                    "$unwind": {
                        "path": "$info_empleado",
                        "preserveNullAndEmptyArrays": false
                    }
                }


                , {
                    "$addFields": {
                        "empleado seleccion": "$Empleado.name"

                        , "empleado nombre y apellidos": {
                            "$concat": ["$info_empleado.firstName", " ", "$info_empleado.lastName"]
                        }

                        , "empleado codigo": "$info_empleado.code"
                    }
                }

                , {
                    "$project": {
                        "info_empleado": 0
                        , "Empleado": 0

                    }
                }



                , {
                    "$addFields": {
                        "variable_fecha": "$rgDate"
                    }
                },


                { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
                { "$match": { "anio": { "$gt": 2000 } } },
                { "$match": { "anio": { "$lt": 3000 } } },


                {
                    "$addFields": {
                        "num_dia_anio": { "$dayOfYear": { "date": "$variable_fecha" } }
                    }
                },

                {
                    "$addFields": {
                        "user_timezone": "$$user_timezone"
                    }
                },
                {
                    "$addFields": {
                        "Fecha_entrada_txt": {
                            "$dateToString": {
                                "date": "$Hora ingreso",
                                "format": "%Y-%m-%d %H:%M",
                                "timezone": "$user_timezone"
                            }
                        }
                    }
                }

                ,{
                  "$match": {
                      "$expr": {
                          "$and": [
                              {
                                  "$gte": [
                                      { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                      ,
                                      { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_inicio" } } }
                                  ]
                              },

                              {
                                  "$lte": [
                                      { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } } }
                                      ,
                                      { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                  ]
                              }
                          ]
                      }
                  }
              }

              , {
                  "$addFields": {
                      "rgDate": "$variable_fecha"
                  }
              }

              , {
                  "$project": {
                      "Punto": 0
                  }
              }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data_entrada"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }



    , {
        "$lookup": {
            "from": "form_formulariodesalida",
            "as": "data_salida",
            "let": {

                "finca": "$finca"

                , "empleado_oid": "$empleado_oid"

                , "user_timezone": "$user_timezone"
                , "anio_entrada": "$anio"
                , "num_dia_anio_entrada": "$num_dia_anio"

            },
            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }

                ,{
                    "$match": {
                        "$expr": {
                            "$and": [

                                { "$eq": ["$$finca", "$finca"] }
                            ]
                        }
                    }
                }


                , { "$unwind": "$Empleado" }

                , {
                    "$addFields": {
                        "empleado_oid": { "$toObjectId": "$Empleado._id" }
                    }
                }

                ,{
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$empleado_oid", "$empleado_oid"] }
                            ]
                        }
                    }
                }


                , {
                    "$addFields": {
                        "variable_fecha": "$Hora salida"
                    }
                },

                { "$addFields": { "anio": { "$year": "$variable_fecha" } } },
                { "$match": { "anio": { "$gt": 2000 } } },
                { "$match": { "anio": { "$lt": 3000 } } },



                {
                    "$addFields": {
                        "num_dia_anio": { "$dayOfYear": { "date": "$variable_fecha" } }
                    }
                },


                {
                    "$addFields": {
                        "Fecha_salida_txt": {
                            "$dateToString": {
                                "date": "$variable_fecha",
                                "format": "%Y-%m-%d %H:%M",
                                "timezone": "$$user_timezone"
                            }
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$anio_entrada", "$anio"] },
                                { "$eq": ["$$num_dia_anio_entrada", "$num_dia_anio"] }
                            ]
                        }
                    }
                }





            ]
        }
    }

    , {
        "$unwind": {
            "path": "$data_salida",
            "preserveNullAndEmptyArrays": true
        }
    }



    , {
        "$addFields": {
            "Fecha_salida_txt": { "$ifNull": ["$data_salida.Fecha_salida_txt", "--sin salida--"] }
        }
    }


    , {
        "$project": {
            "data_salida": 0

            , "empleado_oid": 0
            , "uDate": 0
            , "Formula": 0
        }
    }



    , {
        "$addFields": {

            "num_mes": { "$month": { "date": "$variable_fecha" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
            "num_semana": { "$week": { "date": "$variable_fecha" } },
            "num_dia_semana": { "$dayOfWeek": { "date": "$variable_fecha" } }


        }
    }

    , {
        "$addFields": {
            "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }


            , "Dia_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                    ],
                    "default": "dia de la semana desconocido"
                }
            }
        }
    },


    { "$project": { "num_dia_semana": 0 } }

]
