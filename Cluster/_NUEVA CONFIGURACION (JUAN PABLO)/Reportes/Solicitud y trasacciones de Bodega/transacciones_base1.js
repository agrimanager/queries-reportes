db.users.aggregate(
    [


        { "$limit": 1 },

        {
            "$addFields": {
                "user_timezone": "$timezone"
            }
        },

        {
            "$lookup": {
                "from": "form_transacciondebodega",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"

                    , "user_timezone": "$user_timezone"         //--filtro_timezone
                },

                "pipeline": [

                    //=============finca desde point
                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },
                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0
                        }
                    }


                    //============= (maestro enlazado)

                    //---!!!DANGER....OJO CON rgDate y uDate
                    //---llegan rgDate dia, rgDate mes, rgDate año.....
                    //-----!!!DANGER (desproyectar fechas)
                    //,rgDate día,rgDate mes,rgDate año,rgDate hora,uDate día,uDate mes,uDate año,uDate hora
                    , {
                        "$project": {
                            "rgDate día": 0,
                            "rgDate mes": 0,
                            "rgDate año": 0,
                            "rgDate hora": 0,

                            "uDate día": 0,
                            "uDate mes": 0,
                            "uDate año": 0,
                            "uDate hora": 0
                        }
                    },



                    //=====================================================
                    //--nombre_maestro : aaaaa
                    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
                    //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
                    //--valor_mestro_enlazado : ccc cc c
                    //=====================================================

                    //--Maestro principal
                    {
                        "$addFields": {
                            "nombre_maestro_principal": "Bodega_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal": {
                                "$strLenCP": "$nombre_maestro_principal"
                            }
                        }
                    }


                    //--Mestro enlazado
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    //"then": "$$dataKV.k",
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    //----FALLA EN LA WEB
                    /*
                    , {
                        "$unwind": {
                            "path": "$nombre_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    */

                    //---//obtener el primero (aveces falla)
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$arrayElemAt": ["$nombre_mestro_enlazado", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado": { "$ifNull": ["$nombre_mestro_enlazado", ""] }
                        }
                    }




                    //--Valor Mestro enlazado
                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                        }, "$nombre_maestro_principal"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal": 0,
                            "num_letras_nombre_maestro_principal": 0
                        }
                    }

                    //--maestro enlazado2
                    //=====================================================
                    //--nombre_maestro : aaaaa
                    //--num_letras_nombre_maestro_principal : 5+1 (1 = _)
                    //--nombre_mestro_enlazado : aaaaa_bbb bbb bbb
                    //--valor_mestro_enlazado : ccc cc c
                    //=====================================================

                    //--Maestro principal
                    , {
                        "$addFields": {
                            "nombre_maestro_principal2": "Bodega que se dirige_"
                        }
                    }

                    , {
                        "$addFields": {
                            "num_letras_nombre_maestro_principal2": {
                                "$strLenCP": "$nombre_maestro_principal2"
                            }
                        }
                    }


                    //--Mestro enlazado
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado2": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal2"]
                                                        }, "$nombre_maestro_principal2"]
                                                    },
                                                    //"then": "$$dataKV.k",
                                                    "then": {
                                                        "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal2",
                                                            { "$strLenCP": "$$dataKV.k" }]
                                                    },
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }


                    //----FALLA EN LA WEB
                    /*
                    , {
                        "$unwind": {
                            "path": "$nombre_mestro_enlazado",
                            "preserveNullAndEmptyArrays": true
                        }
                    }
                    */

                    //---//obtener el primero (aveces falla)
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado2": { "$arrayElemAt": ["$nombre_mestro_enlazado2", 0] },
                        }
                    }
                    , {
                        "$addFields": {
                            "nombre_mestro_enlazado2": { "$ifNull": ["$nombre_mestro_enlazado2", ""] }
                        }
                    }




                    //--Valor Mestro enlazado
                    , {
                        "$addFields": {
                            "valor_mestro_enlazado2": {
                                "$filter": {
                                    "input": {
                                        "$map": {
                                            "input": { "$objectToArray": "$$ROOT" },
                                            "as": "dataKV",
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$eq": [{
                                                            "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal2"]
                                                        }, "$nombre_maestro_principal2"]
                                                    },
                                                    "then": "$$dataKV.v",
                                                    "else": ""
                                                }
                                            }
                                        }
                                    },
                                    "as": "item",
                                    "cond": { "$ne": ["$$item", ""] }
                                }
                            }
                        }
                    }
                    , {
                        "$unwind": {
                            "path": "$valor_mestro_enlazado2",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , {
                        "$addFields": {
                            "valor_mestro_enlazado2": { "$ifNull": ["$valor_mestro_enlazado2", ""] }
                        }
                    }

                    , {
                        "$project": {
                            "nombre_maestro_principal2": 0,
                            "num_letras_nombre_maestro_principal2": 0
                        }
                    }


                    //=============productos (armar array)

                    //---array de productos
                    , {
                        "$addFields": {
                            "array_productos": [
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 1", ""] },
                                    "unidad": "$Unidad Producto 1",
                                    "cantidad": "$Cantidad Producto 1",
                                    "observacion": "$Observacion Producto 1"
                                },

                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 2", ""] },
                                    "unidad": "$Unidad Producto 2",
                                    "cantidad": "$Cantidad Producto 2",
                                    "observacion": "$Observacion Producto 2"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 3", ""] },
                                    "unidad": "$Unidad Producto 3",
                                    "cantidad": "$Cantidad Producto 3",
                                    "observacion": "$Observacion Producto 3"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 4", ""] },
                                    "unidad": "$Unidad Producto 4",
                                    "cantidad": "$Cantidad Producto 4",
                                    "observacion": "$Observacion Producto 4"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 5", ""] },
                                    "unidad": "$Unidad Producto 5",
                                    "cantidad": "$Cantidad Producto 5",
                                    "observacion": "$Observacion Producto 5"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 6", ""] },
                                    "unidad": "$Unidad Producto 6",
                                    "cantidad": "$Cantidad Producto 6",
                                    "observacion": "$Observacion Producto 6"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 7", ""] },
                                    "unidad": "$Unidad Producto 7",
                                    "cantidad": "$Cantidad Producto 7",
                                    "observacion": "$Observacion Producto 7"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 8", ""] },
                                    "unidad": "$Unidad Producto 8",
                                    "cantidad": "$Cantidad Producto 8",
                                    "observacion": "$Observacion Producto 8"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 9", ""] },
                                    "unidad": "$Unidad Producto 9",
                                    "cantidad": "$Cantidad Producto 9",
                                    "observacion": "$Observacion Producto 9"
                                },
                                {
                                    "producto": { "$ifNull": ["$Descripcion Producto 10", ""] },
                                    "unidad": "$Unidad Producto 10",
                                    "cantidad": "$Cantidad Producto 10",
                                    "observacion": "$Observacion Producto 10"
                                }
                            ]
                        }
                    }



                    //--filtrar vacias
                    , {
                        "$addFields": {
                            "array_productos": {
                                "$filter": {
                                    "input": "$array_productos",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.producto", ""]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$array_productos",
                            "preserveNullAndEmptyArrays": false
                        }
                    }

                    //=============proyeccion final

                    , {
                        "$project": {
                            "Finca": "$finca"

                            //---info base
                            , "Bodega": "$Bodega"
                            , "Bodega SOLICITUD": "$valor_mestro_enlazado"

                            , "Bodega que se dirige": "$Bodega que se dirige"
                            , "Bodega Bodega que se dirigeSOLICITUD": "$valor_mestro_enlazado2"

                            , "Accion": "$Accion"
                            , "Codigo Solicitud": { "$toString": "$Codigo Solicitud" }

                            //---info producto
                            , "Producto": "$array_productos.producto"
                            , "Unidad": "$array_productos.unidad"
                            , "Cantidad": "$array_productos.cantidad"
                            , "Observacion": "$array_productos.observacion"

                            , "supervisor": "$supervisor"


                            //---info fechas
                            , "Fecha": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%Y-%m-%d",
                                    "timezone": "$$user_timezone"//---con timezone
                                }
                            }

                            , "Horas": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%H:%M",
                                    "timezone": "$$user_timezone"//---con timezone
                                }
                            }

                            // , "rgDate": "$rgDate"
                            , "rgDate": {
                                "$toDate":{
                                    "$dateToString": {
                                        "date": "$rgDate",
                                        "timezone": "$$user_timezone"//---con timezone
                                    }
                                }
                            }

                        }
                    }


                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        //EGRESOS NEGATIVOS



    ]
)
