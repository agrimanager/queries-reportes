[

    { "$limit": 1 },
    {
        "$lookup": {
            "from": "form_solicituddeinsumosyherramientas",
            "as": "data",
            "let": {
                "filtro_fecha_inicio": "$Busqueda inicio",
                "filtro_fecha_fin": "$Busqueda fin"
            },

            "pipeline": [

                {
                    "$addFields": {
                        "point_farm_oid": { "$toObjectId": "$Point.farm" }
                    }
                },
                {
                    "$lookup": {
                        "from": "farms",
                        "localField": "point_farm_oid",
                        "foreignField": "_id",
                        "as": "finca"
                    }
                },
                { "$unwind": "$finca" },
                { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

                {
                    "$project": {
                        "Point": 0
                        , "point_farm_oid": 0
                        , "uid": 0
                    }
                }
                
                , {
                    "$project": {
                        "rgDate día": 0,
                        "rgDate mes": 0,
                        "rgDate año": 0,
                        "rgDate hora": 0,

                        "uDate día": 0,
                        "uDate mes": 0,
                        "uDate año": 0,
                        "uDate hora": 0
                    }
                },


                {
                    "$addFields": {
                        "nombre_maestro_principal": "Bodega_"
                    }
                }

                , {
                    "$addFields": {
                        "num_letras_nombre_maestro_principal": {
                            "$strLenCP": "$nombre_maestro_principal"
                        }
                    }
                }


                , {
                    "$addFields": {
                        "nombre_mestro_enlazado": {
                            "$filter": {
                                "input": {
                                    "$map": {
                                        "input": { "$objectToArray": "$$ROOT" },
                                        "as": "dataKV",
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [{
                                                        "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                    }, "$nombre_maestro_principal"]
                                                },
                                                
                                                "then": {
                                                    "$substr": ["$$dataKV.k", "$num_letras_nombre_maestro_principal",
                                                        { "$strLenCP": "$$dataKV.k" }]
                                                },
                                                "else": ""
                                            }
                                        }
                                    }
                                },
                                "as": "item",
                                "cond": { "$ne": ["$$item", ""] }
                            }
                        }
                    }
                }

                , {
                    "$addFields": {
                        "nombre_mestro_enlazado": { "$arrayElemAt": ["$nombre_mestro_enlazado", 0] }
                    }
                }
                , {
                    "$addFields": {
                        "nombre_mestro_enlazado": { "$ifNull": ["$nombre_mestro_enlazado", ""] }
                    }
                }



                , {
                    "$addFields": {
                        "valor_mestro_enlazado": {
                            "$filter": {
                                "input": {
                                    "$map": {
                                        "input": { "$objectToArray": "$$ROOT" },
                                        "as": "dataKV",
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$eq": [{
                                                        "$substr": ["$$dataKV.k", 0, "$num_letras_nombre_maestro_principal"]
                                                    }, "$nombre_maestro_principal"]
                                                },
                                                "then": "$$dataKV.v",
                                                "else": ""
                                            }
                                        }
                                    }
                                },
                                "as": "item",
                                "cond": { "$ne": ["$$item", ""] }
                            }
                        }
                    }
                }
                , {
                    "$unwind": {
                        "path": "$valor_mestro_enlazado",
                        "preserveNullAndEmptyArrays": true
                    }
                }


                , {
                    "$addFields": {
                        "valor_mestro_enlazado": { "$ifNull": ["$valor_mestro_enlazado", ""] }
                    }
                }

                , {
                    "$project": {
                        "nombre_maestro_principal": 0,
                        "num_letras_nombre_maestro_principal": 0
                    }
                }


                , {
                    "$addFields": {
                        "array_productos": [
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 1", ""] },
                                "unidad": "$Unidad Producto 1",
                                "cantidad": "$Cantidad Producto 1",
                                "observacion": "$Observacion Producto 1"
                            },

                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 2", ""] },
                                "unidad": "$Unidad Producto 2",
                                "cantidad": "$Cantidad Producto 2",
                                "observacion": "$Observacion Producto 2"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 3", ""] },
                                "unidad": "$Unidad Producto 3",
                                "cantidad": "$Cantidad Producto 3",
                                "observacion": "$Observacion Producto 3"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 4", ""] },
                                "unidad": "$Unidad Producto 4",
                                "cantidad": "$Cantidad Producto 4",
                                "observacion": "$Observacion Producto 4"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 5", ""] },
                                "unidad": "$Unidad Producto 5",
                                "cantidad": "$Cantidad Producto 5",
                                "observacion": "$Observacion Producto 5"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 6", ""] },
                                "unidad": "$Unidad Producto 6",
                                "cantidad": "$Cantidad Producto 6",
                                "observacion": "$Observacion Producto 6"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 7", ""] },
                                "unidad": "$Unidad Producto 7",
                                "cantidad": "$Cantidad Producto 7",
                                "observacion": "$Observacion Producto 7"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 8", ""] },
                                "unidad": "$Unidad Producto 8",
                                "cantidad": "$Cantidad Producto 8",
                                "observacion": "$Observacion Producto 8"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 9", ""] },
                                "unidad": "$Unidad Producto 9",
                                "cantidad": "$Cantidad Producto 9",
                                "observacion": "$Observacion Producto 9"
                            },
                            {
                                "producto": { "$ifNull": ["$Descripcion Producto 10", ""] },
                                "unidad": "$Unidad Producto 10",
                                "cantidad": "$Cantidad Producto 10",
                                "observacion": "$Observacion Producto 10"
                            }
                        ]
                    }
                }



                , {
                    "$addFields": {
                        "array_productos": {
                            "$filter": {
                                "input": "$array_productos",
                                "as": "item",
                                "cond": {
                                    "$ne": ["$$item.producto", ""]
                                }
                            }
                        }
                    }
                }

                , {
                    "$unwind": {
                        "path": "$array_productos",
                        "preserveNullAndEmptyArrays": false
                    }
                }




                , {
                    "$project": {
                        "Finca": "$finca"

                        
                        , "Bodega": "$Bodega"
                        , "Bodega SOLICITUD": "$valor_mestro_enlazado"
                        , "Tipo de Cultivo": "$Tipo de Cultivo"
                        , "Codigo Solicitud": { "$toString": "$Codigo Solicitud" }

                        
                        , "Producto": "$array_productos.producto"
                        , "Unidad": "$array_productos.unidad"
                        , "Cantidad": "$array_productos.cantidad"
                        , "Observacion": "$array_productos.observacion"
                        
                        
                        
                        , "Fecha": {
                            "$dateToString": {
                                "date": "$rgDate",
                                "format": "%Y-%m-%d",
                                "timezone": "America/Bogota"
                            }
                        }
                        
                        , "rgDate": "$rgDate"

                    }
                }


            ]
        }
    }


    , {
        "$project":
        {
            "datos": {
                "$concatArrays": [
                    "$data"
                    , []
                ]
            }
        }
    }

    , { "$unwind": "$datos" }
    , { "$replaceRoot": { "newRoot": "$datos" } }


]