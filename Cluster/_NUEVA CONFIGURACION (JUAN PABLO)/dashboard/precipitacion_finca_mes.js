db.form_registrodeprecipitacion.aggregate(
    [



        //----------------------------------------------------------------
        //---VARIABLES INYECTADAS
        {
            $addFields: {
                "Busqueda inicio": ISODate("2023-05-02T06:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                // "FincaID": ObjectId("5fac01ce246347247f068528"),
                //user,FincaNombre
                //rgDate -- rgDate día":0,"rgDate mes": 0,"rgDate año": 0,"rgDate hora": 0,
                //uDate --- "uDate día":0,"uDate mes": 0,"uDate año": 0,"uDate hora": 0
            }
        },
        //----FILTRO FECHAS Y FINCA
        {
            "$match": {
                "$expr": {
                    "$and": [

                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },
                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------------------------------------
        //....query reporte


        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_registrodeprecipitacion",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [


                    { "$addFields": { "variable_fecha": "$Fecha Medicion" } }
                    , { "$addFields": { "type_variable_fecha": { "$type": "$variable_fecha" } } }
                    , { "$match": { "type_variable_fecha": { "$eq": "date" } } },


                    { "$addFields": { "anio_variable_fecha": { "$year": "$variable_fecha" } } },
                    { "$match": { "anio_variable_fecha": { "$gt": 2000 } } },
                    { "$match": { "anio_variable_fecha": { "$lt": 3000 } } },

                    {
                        "$project": {
                            "variable_fecha": 0
                            , "type_variable_fecha": 0
                            , "anio_variable_fecha": 0
                        }
                    },

                    {
                        "$addFields": {
                            "filtro_fecha_fin_menos_n_dias": {
                                "$subtract": [
                                    "$$filtro_fecha_fin",
                                    { "$multiply": [180, 86400000] }
                                ]
                            }
                        }
                    },


                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Medicion" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha_fin_menos_n_dias" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha Medicion" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },




                    {
                        "$addFields": {
                            "point_farm_oid": { "$toObjectId": "$Point.farm" }
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "point_farm_oid",
                            "foreignField": "_id",
                            "as": "finca_point"
                        }
                    },
                    { "$unwind": "$finca_point" },
                    { "$addFields": { "finca_point": { "$ifNull": ["$finca_point.name", "no existe"] } } },

                    {
                        "$project": {
                            "Point": 0
                            , "point_farm_oid": 0
                            , "uid": 0

                            , "Semana": 0
                            , "Formula": 0
                            , "uDate": 0
                        }
                    }


                    , { "$addFields": { "rgDate": "$Fecha Medicion" } }



                    , { "$addFields": { "variable_fecha": "$Fecha Medicion" } }
                    , {
                        "$addFields": {
                            "num_anio": { "$year": { "date": "$variable_fecha" } },
                            "num_mes": { "$month": { "date": "$variable_fecha" } },
                            "num_dia_mes": { "$dayOfMonth": { "date": "$variable_fecha" } },
                            "num_semana": { "$week": { "date": "$variable_fecha" } },
                            "num_dia_semana": { "$dayOfWeek": { "date": "$variable_fecha" } }
                        }
                    }

                    , {
                        "$addFields": {
                            "Fecha Medicion": { "$dateToString": { "format": "%Y-%m-%d", "date": "$variable_fecha" } }

                            , "Mes_Txt": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                                    ],
                                    "default": "Mes desconocido"
                                }
                            }


                            , "Dia_Txt": {
                                "$switch": {
                                    "branches": [
                                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                                    ],
                                    "default": "dia de la semana desconocido"
                                }
                            }
                        }
                    },

                    { "$project": { "num_dia_semana": 0 } }



                    , {
                        "$match": {
                            "Finca": { "$ne": "" }
                        }
                    }

                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }








        , {
            "$addFields": {

                "variable_label": "$Finca"
                , "variable_dataset": {
                    "$concat": [{ "$toString": "$num_anio" }, "-", "$Mes_Txt"]
                }
            }
        }



        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$variable_label"
                    , "dashboard_dataset": "$variable_dataset"
                }
                , "dashboard_cantidad": { "$sum": "$Precipitacion" }
            }
        }

        , {
            "$group": {
                "_id": {
                    "dashboard_dataset": "$_id.dashboard_dataset"
                }
                , "data_group": { "$push": "$$ROOT" }

            }
        }

        , {
            "$sort": {
                "_id.dashboard_dataset": 1
            }
        }


        , {
            "$group": {
                "_id": null
                , "data_group": { "$push": "$$ROOT" }
                , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
            }
        }

        , { "$unwind": "$data_group" }

        , { "$unwind": "$data_group.data_group" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_group.data_group",
                        {
                            "array_dashboard_dataset": "$array_dashboard_dataset"
                        }
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$_id.dashboard_label"
                }
                , "data_group": { "$push": "$$ROOT" }

            }
        }
        , {
            "$sort": {
                "_id.dashboard_label": 1
            }
        }


        , {
            "$group": {
                "_id": null
                , "dashboard_data": {
                    "$push": "$$ROOT"
                }
            }
        }



        , {
            "$addFields": {
                "datos_dashboard": {
                    "$map": {
                        "input": "$dashboard_data",
                        "as": "item_dashboard_data",
                        "in": {
                            "$reduce": {
                                "input": "$$item_dashboard_data.data_group",
                                "initialValue": [],
                                "in": {
                                    "$concatArrays": [
                                        "$$value",
                                        [
                                            {
                                                "dashboard_label": "$$this._id.dashboard_label",
                                                "dashboard_dataset": "$$this._id.dashboard_dataset",
                                                "dashboard_cantidad": "$$this.dashboard_cantidad"
                                            }
                                        ]
                                    ]
                                }
                            }
                        }

                    }
                }
            }
        }


        , {
            "$addFields": {
                "datos_dashboard": {
                    "$reduce": {
                        "input": "$datos_dashboard",
                        "initialValue": [],
                        "in": {
                            "$concatArrays": [
                                "$$value",
                                "$$this"
                            ]
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }

            }
        }


        , {
            "$addFields": {
                "info_datasets": {
                    "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
                }
            }
        }

        , {
            "$addFields": {
                "array_colores": [
                    "#61c3ac",
                    "#773e91",
                    "#61c3ac",
                    "#fed330",
                    "#fc5c65",

                    "#d1d8e0",
                    "#635f6d",

                    "#474787",
                    "#218c74",
                    "#26de81",
                    "#b33939",
                    "#218c74",
                    "#227093",
                    "#cd6133",
                    "#84817a",
                    "#40407a",
                    "#33d9b2",
                    "#ffda79",
                    "#2c2c54",



                    "#a55eea",
                    "#2bcbba",
                    "#fd9644",
                    "#4b7bec",
                    "#20bf6b",
                    "#4b6584",
                    "#ffb142",


                    "#fc5c65",
                    "#20bf6b",
                    "#3867d6",
                    "#fed330",
                    "#a55eea",
                    "#45aaf2",
                    "#fa8231",
                    "#eb3b5a",
                    "#0fb9b1",
                    "#d1d8e0",

                    "#8854d0",


                    "#63b598",
                    "#ce7d78",
                    "#ea9e70",
                    "#a48a9e",
                    "#c6e1e8",
                    "#648177",
                    "#0d5ac1",
                    "#f205e6",
                    "#1c0365",
                    "#14a9ad",
                    "#4ca2f9",
                    "#a4e43f",
                    "#d298e2",
                    "#6119d0",
                    "#d2737d",
                    "#c0a43c",
                    "#f2510e",
                    "#651be6",
                    "#79806e",
                    "#61da5e",
                    "#cd2f00",
                    "#9348af",
                    "#01ac53",
                    "#c5a4fb",
                    "#996635",
                    "#b11573",
                    "#4bb473",
                    "#75d89e",
                    "#2f3f94",
                    "#2f7b99",
                    "#da967d",
                    "#34891f",
                    "#b0d87b",
                    "#ca4751",
                    "#7e50a8",
                    "#c4d647",
                    "#e0eeb8",
                    "#11dec1",
                    "#289812",
                    "#566ca0",
                    "#ffdbe1",
                    "#2f1179",
                    "#935b6d",
                    "#916988",
                    "#513d98",
                    "#aead3a",
                    "#9e6d71",
                    "#4b5bdc",
                    "#0cd36d",
                    "#250662",
                    "#cb5bea",
                    "#228916",
                    "#ac3e1b",
                    "#df514a",
                    "#539397",
                    "#880977",
                    "#f697c1",
                    "#ba96ce",
                    "#679c9d",
                    "#c6c42c",
                    "#5d2c52",
                    "#48b41b",
                    "#e1cf3b",
                    "#5be4f0",
                    "#57c4d8",
                    "#a4d17a",
                    "#be608b",
                    "#96b00c",
                    "#088baf",
                    "#f158bf",
                    "#e145ba",
                    "#ee91e3",
                    "#05d371",
                    "#5426e0",
                    "#4834d0",
                    "#802234",
                    "#6749e8",
                    "#0971f0",
                    "#8fb413",
                    "#b2b4f0",
                    "#c3c89d",
                    "#c9a941",
                    "#41d158",
                    "#fb21a3",
                    "#51aed9",
                    "#5bb32d",
                    "#21538e",
                    "#89d534",
                    "#d36647",
                    "#7fb411",
                    "#0023b8",
                    "#3b8c2a",
                    "#986b53",
                    "#f50422",
                    "#983f7a",
                    "#ea24a3",
                    "#79352c",
                    "#521250",
                    "#c79ed2",
                    "#d6dd92",
                    "#e33e52",
                    "#b2be57",
                    "#fa06ec",
                    "#1bb699",
                    "#6b2e5f",
                    "#64820f",
                    "#21538e",
                    "#89d534",
                    "#d36647",
                    "#7fb411",
                    "#0023b8",
                    "#3b8c2a",
                    "#986b53",
                    "#f50422",
                    "#983f7a",
                    "#ea24a3",
                    "#79352c",
                    "#521250",
                    "#c79ed2",
                    "#d6dd92",
                    "#e33e52",
                    "#b2be57",
                    "#fa06ec",
                    "#1bb699",
                    "#6b2e5f",
                    "#64820f",
                    "#9cb64a",
                    "#996c48",
                    "#9ab9b7",
                    "#06e052",
                    "#e3a481",
                    "#0eb621",
                    "#fc458e",
                    "#b2db15",
                    "#aa226d",
                    "#792ed8",
                    "#73872a",
                    "#520d3a",
                    "#cefcb8",
                    "#a5b3d9",
                    "#7d1d85",
                    "#c4fd57",
                    "#f1ae16",
                    "#8fe22a",
                    "#ef6e3c",
                    "#243eeb",
                    "#dd93fd",
                    "#3f8473",
                    "#e7dbce",
                    "#421f79",
                    "#7a3d93",

                    "#93f2d7",
                    "#9b5c2a",
                    "#15b9ee",
                    "#0f5997",
                    "#409188",
                    "#911e20",
                    "#1350ce",
                    "#10e5b1",
                    "#fff4d7",
                    "#cb2582",
                    "#ce00be",
                    "#32d5d6",
                    "#608572",
                    "#c79bc2",
                    "#00f87c",
                    "#77772a",
                    "#6995ba",
                    "#fc6b57",
                    "#f07815",
                    "#8fd883",
                    "#060e27",
                    "#96e591",
                    "#21d52e",
                    "#d00043",
                    "#b47162",
                    "#1ec227",
                    "#4f0f6f",
                    "#1d1d58",
                    "#947002",
                    "#bde052",
                    "#e08c56",
                    "#28fcfd",
                    "#36486a",
                    "#d02e29",
                    "#1ae6db",
                    "#3e464c",
                    "#a84a8f",
                    "#911e7e",
                    "#3f16d9",
                    "#0f525f",
                    "#ac7c0a",
                    "#b4c086",
                    "#c9d730",
                    "#30cc49",
                    "#3d6751",
                    "#fb4c03",
                    "#640fc1",
                    "#62c03e",
                    "#d3493a",
                    "#88aa0b",
                    "#406df9",
                    "#615af0",
                    "#2a3434",
                    "#4a543f",
                    "#79bca0",
                    "#a8b8d4",
                    "#00efd4",
                    "#7ad236",
                    "#7260d8",
                    "#1deaa7",
                    "#06f43a",
                    "#823c59",
                    "#e3d94c",
                    "#dc1c06",
                    "#f53b2a",
                    "#b46238",
                    "#2dfff6",
                    "#a82b89",
                    "#1a8011",
                    "#436a9f",
                    "#1a806a",
                    "#4cf09d",
                    "#c188a2",
                    "#67eb4b",
                    "#b308d3",
                    "#fc7e41",
                    "#af3101",
                    "#71b1f4",
                    "#a2f8a5",
                    "#e23dd0",
                    "#d3486d",
                    "#00f7f9",
                    "#474893",
                    "#3cec35",
                    "#1c65cb",
                    "#5d1d0c",
                    "#2d7d2a",
                    "#ff3420",
                    "#5cdd87",
                    "#a259a4",
                    "#e4ac44",
                    "#1bede6",
                    "#8798a4",
                    "#d7790f",
                    "#b2c24f",
                    "#de73c2",
                    "#d70a9c",
                    "#88e9b8",
                    "#c2b0e2",
                    "#86e98f",
                    "#ae90e2",
                    "#1a806b",
                    "#436a9e",
                    "#0ec0ff"

                ]
            }
        }




        , {
            "$addFields": {
                "DATA_ARRAY_DATASETS": {
                    "$map": {
                        "input": "$info_datasets",
                        "as": "item_info_datasets",
                        "in": {
                            "label": "$$item_info_datasets",
                            "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
                            "borderColor": "#000000",
                            "borderWidth": 1,
                            "data": {
                                "$map": {
                                    "input": "$DATA_LABELS",
                                    "as": "item_data_labels",
                                    "in": {
                                        "$reduce": {
                                            "input": "$datos_dashboard",
                                            "initialValue": 0,
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [
                                                            { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
                                                            , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
                                                        ]

                                                    },
                                                    "then": "$$this.dashboard_cantidad",
                                                    "else": "$$value"
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
        }




        , { "$project": { "_id": 0 } }


        , {
            "$project": {

                "data": {
                    "labels": "$DATA_LABELS",
                    "datasets": "$DATA_ARRAY_DATASETS"
                },
                "options": {
                    "title": {
                        "display": true,
                        "text": "Chart.js Bar Chart"
                    }
                }

            }
        }





    ]



)
