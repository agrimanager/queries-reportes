[




        { "$limit": 1 },
        {
            "$lookup": {
                "from": "form_formulariodesanidadaguacate",
                "as": "data",
                "let": {
                    "filtro_fecha_inicio": "$Busqueda inicio",
                    "filtro_fecha_fin": "$Busqueda fin"
                },
                "pipeline": [

                    {
                        "$addFields": {
                            "filtro_fecha_fin_menos_15_dias": {
                                "$subtract": [
                                    "$$filtro_fecha_fin",
                                    { "$multiply": [15, 86400000] }
                                ]
                            }
                        }
                    },


                    {
                        "$match": {
                            "$expr": {
                                "$and": [

                                    {
                                        "$gte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$filtro_fecha_fin_menos_15_dias" } } }
                                        ]
                                    },

                                    {
                                        "$lte": [
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } },
                                            { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$filtro_fecha_fin" } } }
                                        ]
                                    }
                                ]
                            }
                        }
                    },


                    {
                        "$addFields": {
                            "variable_cartografia": "$Lote"
                        }
                    },
                    { "$unwind": "$variable_cartografia.features" },

                    {
                        "$addFields": {
                            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
                        }
                    },
                    {
                        "$addFields": {
                            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
                        }
                    },
                    {
                        "$addFields": {
                            "split_path_oid": {
                                "$concatArrays": [
                                    "$split_path_padres_oid",
                                    "$variable_cartografia_oid"
                                ]
                            }
                        }
                    },

                    {
                        "$lookup": {
                            "from": "cartography",
                            "localField": "split_path_oid",
                            "foreignField": "_id",
                            "as": "objetos_del_cultivo"
                        }
                    },

                    {
                        "$addFields": {
                            "tiene_variable_cartografia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            { "$size": { "$ifNull": ["$split_path_oid", []] } }
                                            , { "$size": { "$ifNull": ["$objetos_del_cultivo", []] } }]
                                    },
                                    "then": "si",
                                    "else": "no"
                                }
                            }
                        }
                    },

                    {
                        "$addFields": {
                            "objetos_del_cultivo": {
                                "$cond": {
                                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                                    "then": "$objetos_del_cultivo",
                                    "else": {
                                        "$concatArrays": [
                                            "$objetos_del_cultivo",
                                            ["$variable_cartografia.features"]
                                        ]
                                    }
                                }
                            }
                        }
                    },



                    {
                        "$addFields": {
                            "finca": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$finca",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        "$lookup": {
                            "from": "farms",
                            "localField": "finca._id",
                            "foreignField": "_id",
                            "as": "finca"
                        }
                    },
                    { "$unwind": "$finca" },

                    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


                    {
                        "$addFields": {
                            "bloque": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$bloque",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

                    {
                        "$addFields": {
                            "lote": {
                                "$filter": {
                                    "input": "$objetos_del_cultivo",
                                    "as": "item_cartografia",
                                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                                }
                            }
                        }
                    },
                    {
                        "$unwind": {
                            "path": "$lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },




                    {
                        "$project": {
                            "variable_cartografia": 0,
                            "split_path_padres": 0,
                            "split_path_padres_oid": 0,
                            "variable_cartografia_oid": 0,
                            "split_path_oid": 0,
                            "objetos_del_cultivo": 0,
                            "tiene_variable_cartografia": 0

                            , "Point": 0
                            , "Lote": 0
                            , "Formula": 0
                            , "uid": 0
                            , "uDate": 0
                        }
                    }


                    , {
                        "$addFields": {
                            "Estado Fenologico": {
                                "$cond": {
                                    "if": { "$in": [{ "$type": "$Estado Fenologico" }, ["object", "string"]] },
                                    "then": [],
                                    "else": "$Estado Fenologico"
                                }
                            }
                        }
                    }
                    , {
                        "$addFields": {
                            "Estado Fenologico": {
                                "$reduce": {
                                    "input": "$Estado Fenologico",
                                    "initialValue": "",
                                    "in": {
                                        "$cond": {
                                            "if": { "$eq": [{ "$strLenCP": "$$value" }, 0] },
                                            "then": "$$this",
                                            "else": { "$concat": ["$$value", " -- ", "$$this"] }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "Fecha": {
                                "$dateToString": {
                                    "date": "$rgDate",
                                    "format": "%Y-%m-%d",
                                    "timezone": "America/Bogota"
                                }
                            }
                        }
                    }



                    , {
                        "$addFields": {

                            "array_info_sanidad": [



                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga1", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo1" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro1" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto1" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto1" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja1" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula1" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama1" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo1" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo1" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos1" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion1", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion1", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado1", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion1", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga2", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo2" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro2" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto2" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto2" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja2" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula2" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama2" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo2" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo2" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos2" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion2", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion2", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado2", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion2", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga3", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo3" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro3" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto3" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto3" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja3" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula3" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama3" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo3" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo3" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos3" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion3", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion3", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado3", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion3", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga4", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo4" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro4" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto4" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto4" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja4" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula4" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama4" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo4" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo4" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos4" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion4", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion4", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado4", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion4", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga5", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo5" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro5" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto5" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto5" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja5" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula5" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama5" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo5" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo5" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos5" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion5", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion5", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado5", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion5", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga6", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo6" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro6" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto6" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto6" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja6" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula6" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama6" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo6" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo6" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos6" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion6", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion6", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado6", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion6", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga7", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo7" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro7" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto7" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto7" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja7" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula7" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama7" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo7" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo7" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos7" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion7", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion7", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado7", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion7", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga8", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo8" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro8" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto8" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto8" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja8" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula8" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama8" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo8" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo8" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos8" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion8", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion8", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado8", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion8", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga9", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo9" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro9" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto9" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto9" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja9" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula9" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama9" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo9" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo9" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos9" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion9", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion9", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado9", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion9", ""] }
                                },
                                {
                                    "Enfermedad o Plaga": { "$ifNull": ["$Enfermedad o Plaga10", ""] },
                                    "Huevo": { "$ifNull": [{ "$toDouble": "$Huevo10" }, 0] },
                                    "Inmaduro": { "$ifNull": [{ "$toDouble": "$Inmaduro10" }, 0] },
                                    "Adulto": { "$ifNull": [{ "$toDouble": "$Adulto10" }, 0] },
                                    "Fruto": { "$ifNull": [{ "$toDouble": "$Fruto10" }, 0] },
                                    "Hoja": { "$ifNull": [{ "$toDouble": "$Hoja10" }, 0] },
                                    "Panicula": { "$ifNull": [{ "$toDouble": "$Panicula10" }, 0] },
                                    "Rama": { "$ifNull": [{ "$toDouble": "$Rama10" }, 0] },
                                    "Tallo": { "$ifNull": [{ "$toDouble": "$Tallo10" }, 0] },
                                    "Suelo": { "$ifNull": [{ "$toDouble": "$Suelo10" }, 0] },
                                    "Individuos": { "$ifNull": [{ "$toDouble": "$Individuos10" }, 0] },
                                    "Grado de afectacion": { "$ifNull": ["$Grado de afectacion10", ""] },
                                    "Porcentaje de afectacion": { "$ifNull": ["$Porcentaje de afectacion10", ""] },
                                    "Tercio Afectado": { "$ifNull": ["$Tercio Afectado10", ""] },
                                    "Observacion": { "$ifNull": ["$Observacion10", ""] }
                                }




                            ]

                        }
                    }


                    , {
                        "$addFields": {
                            "array_info_sanidad": {
                                "$filter": {
                                    "input": "$array_info_sanidad",
                                    "as": "item",
                                    "cond": {
                                        "$ne": ["$$item.Enfermedad o Plaga", ""]
                                    }
                                }
                            }
                        }
                    }


                    , {
                        "$match": {
                            "array_info_sanidad": { "$ne": [] }
                        }
                    }

                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca"
                                , "bloque": "$bloque"
                                , "lote": "$lote"

                            }
                            , "num_censos_x_lote": { "$sum": 1 }
                            , "data": { "$push": "$$ROOT" }
                        }
                    }

                    , { "$unwind": "$data" }


                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$data",
                                    {
                                        "num_censos_x_lote": "$num_censos_x_lote"
                                    }
                                ]
                            }
                        }
                    }


                    , {
                        "$unwind": {
                            "path": "$array_info_sanidad",
                            "preserveNullAndEmptyArrays": false
                        }
                    }




                    , {
                        "$project": {
                            "Fecha": "$Fecha"

                            , "finca": "$finca"
                            , "bloque": "$bloque"
                            , "lote": "$lote"
                            , "Estado Fenologico": "$Estado Fenologico"

                            , "Enfermedad o Plaga": "$array_info_sanidad.Enfermedad o Plaga"
                            , "Huevo": "$array_info_sanidad.Huevo"
                            , "Inmaduro": "$array_info_sanidad.Inmaduro"
                            , "Adulto": "$array_info_sanidad.Adulto"
                            , "Fruto": "$array_info_sanidad.Fruto"
                            , "Hoja": "$array_info_sanidad.Hoja"
                            , "Panicula": "$array_info_sanidad.Panicula"
                            , "Rama": "$array_info_sanidad.Rama"
                            , "Tallo": "$array_info_sanidad.Tallo"
                            , "Suelo": "$array_info_sanidad.Suelo"
                            , "Individuos": "$array_info_sanidad.Individuos"
                            , "Grado de afectacion": "$array_info_sanidad.Grado de afectacion"
                            , "Porcentaje de afectacion": "$array_info_sanidad.Porcentaje de afectacion"
                            , "Tercio Afectado": "$array_info_sanidad.Tercio Afectado"
                            , "Observacion": "$array_info_sanidad.Observacion"


                            , "supervisor": "$supervisor"
                            , "capture": "$capture"
                            , "rgDate": "$rgDate"


                            , "num_censos_x_lote": "$num_censos_x_lote"

                        }
                    }





                    , {
                        "$addFields": {
                            "suma_campos_evaluacion": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Oligonychus persea/Ácaro cristalino"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja"
                                                ]
                                            }
                                        },


                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Atta spp / Hormiga arriera"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Bombacoccus aguacatae / Protopulvinaria pyriformis / Pulvinaria psidii / Escamas blandas"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja",
                                                    "$Rama",
                                                    "$Tallo"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Bruggmanniella perseae / Mosca del ovario"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Colletotrichum gloeosporioides / Antracnosis"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Panicula",
                                                    "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Compsus sp / Vaquita"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Copturomimus hustachei / Perforador del tallo "
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                    , {

                                                        "$cond": {
                                                            "if": {
                                                                "$ne": [
                                                                    "$Grado de afectacion",
                                                                    ""
                                                                ]
                                                            },
                                                            "then": { "$toDouble": "$Grado de afectacion" },
                                                            "else": 0
                                                        }

                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Crisomelidos"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Enemigos naturales"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Falso medidor"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Frankliniella gardeniae / Trips"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Panicula"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Heilipus lauri / Barrenador de la semilla de aguacate"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Huevo",
                                                    "$Inmaduro",
                                                    "$Adulto"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Heilipus Trifasciatus / Barrenador de la semilla de aguacate"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Huevo",
                                                    "$Inmaduro",
                                                    "$Adulto"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Hemiberlesia cyanophylli / Pseudoparlatoria parlatorioides / Chrysomphalus dictyospermi /Escamas portegidas"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja",
                                                    "$Rama",
                                                    "$Tallo"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Larvas Coleópteros / Chizas "
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Monalonion velezangueli / Chinche del aguacate"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Rama"
                                                    , "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Myzus persicae / Pulgones"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja"
                                                    , "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Oligonychus yothersi / Acaro rojo"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Huevo",
                                                    "$Inmaduro",
                                                    "$Adulto"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Pandeleteius viticollis / Picudo del follaje"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja",
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Paraleyrodes sp / Mosca blanca"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Hoja"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Phyllophaga Obsoleta /Cucarrones marceños"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja",
                                                    "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Platinota spp / Pega pega "
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja"
                                                    , "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Polinizadores"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Polyphagotarsonemus latus / Acaro Blanco"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Individuos"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Pseudococcus jackbeardsleyi / Ferrisia Kondoi / Cochinillas harinosas"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja",
                                                    "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Stenoma catenifer / Polilla de la semilla de aguacate"
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Rama"
                                                ]
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "tatuador "
                                                ]
                                            },
                                            "then": {
                                                "$sum": [
                                                    "$Fruto",
                                                    "$Hoja"
                                                ]
                                            }
                                        },

                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Cercospora purpurea/Pseudocercospora purpurea / Mancha angular"
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Porcentaje de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Nectria galligena / Cancro bacteriano"
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Porcentaje de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        },

                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Lenticelosis"
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Porcentaje de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        },

                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Cylindrocarpon destructans / Cylindrocladium sp / Pudricion negra de la raiz "
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Grado de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Phytophthora cinnamomi /Pudricion de raices"
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Grado de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        },
                                        {
                                            "case": {
                                                "$eq": [
                                                    "$Enfermedad o Plaga",
                                                    "Verticillum Nees / Marchitez por Verticillium"
                                                ]
                                            },
                                            "then": {
                                                "$cond": {
                                                    "if": {
                                                        "$ne": [
                                                            "$Grado de afectacion",
                                                            ""
                                                        ]
                                                    },
                                                    "then": 1,
                                                    "else": 0
                                                }
                                            }
                                        }
                                    ],
                                    "default": 0
                                }
                            }
                        }
                    },
                    {
                        "$addFields": {
                            "presencia": {
                                "$cond": {
                                    "if": {
                                        "$eq": [
                                            "$suma_campos_evaluacion",
                                            0
                                        ]
                                    },
                                    "then": 0,
                                    "else": 1
                                }
                            }
                        }
                    }


                    , {
                        "$group": {
                            "_id": {
                                "finca": "$finca"
                                , "bloque": "$bloque"
                                , "lote": "$lote"

                                , "enfermedad_o_plaga": "$Enfermedad o Plaga"
                            }
                            , "num_censos_x_lote": { "$min": "$num_censos_x_lote" }
                            , "suma_campos_evaluacion": { "$sum": "$suma_campos_evaluacion" }
                            , "presencia": { "$sum": "$presencia" }

                        }
                    }



                    , {
                        "$addFields": {
                            "incidencia": {
                                "$cond": {
                                    "if": { "$eq": ["$num_censos_x_lote", 0] },
                                    "then": 0,
                                    "else": {
                                        "$multiply": [
                                            {
                                                "$divide": ["$presencia", "$num_censos_x_lote"]
                                            }
                                            , 100]
                                    }
                                }
                            }
                        }
                    }
                    , {
                        "$addFields": {
                            "severidad": {
                                "$cond": {
                                    "if": { "$eq": ["$presencia", 0] },
                                    "then": 0,
                                    "else": {
                                        "$divide": ["$suma_campos_evaluacion", "$presencia"]
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "incidencia": { "$divide": [{ "$subtract": [{ "$multiply": ["$incidencia", 100] }, { "$mod": [{ "$multiply": ["$incidencia", 100] }, 1] }] }, 100] }
                            , "severidad": { "$divide": [{ "$subtract": [{ "$multiply": ["$severidad", 100] }, { "$mod": [{ "$multiply": ["$severidad", 100] }, 1] }] }, 100] }
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_puentesanidadaguacatesemaforo",
                            "localField": "_id.enfermedad_o_plaga",
                            "foreignField": "Enfermedad o Plaga",
                            "as": "data_semaforo"
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$data_semaforo",
                            "preserveNullAndEmptyArrays": false
                        }
                    }


                    , {
                        "$addFields": {
                            "data_semaforo_tipo": "$data_semaforo.tipo"
                            , "data_semaforo_umbral_min": "$data_semaforo.Umbral min"
                            , "data_semaforo_umbral_max": "$data_semaforo.Umbral max"
                        }
                    }
                    , {
                        "$project": {
                            "data_semaforo": 0
                        }
                    }



                    , {
                        "$replaceRoot": {
                            "newRoot": {
                                "$mergeObjects": [
                                    "$_id",
                                    {
                                        "tipo": "$data_semaforo_tipo",
                                        "num_censos_x_lote": "$num_censos_x_lote",
                                        "suma_campos_evaluacion": "$suma_campos_evaluacion",
                                        "presencia": "$presencia",
                                        "incidencia": "$incidencia",
                                        "severidad": "$severidad",


                                        "umbral_accion_min": "$data_semaforo_umbral_min",
                                        "umbral_accion_max": "$data_semaforo_umbral_max"
                                    }
                                ]
                            }
                        }

                    }

                    , {
                        "$addFields": {
                            "umbral_incidencia": {
                                "$cond": {
                                    "if": { "$lt": ["$incidencia", "$umbral_accion_min"] },
                                    "then": "umbral_1",
                                    "else": {
                                        "$cond": {
                                            "if": { "$gte": ["$incidencia", "$umbral_accion_max"] },
                                            "then": "umbral_3",
                                            "else": "umbral_2"
                                        }
                                    }
                                }
                            }
                        }
                    }

                    , {
                        "$addFields": {
                            "umbral_tipo": {
                                "$cond": {
                                    "if": { "$lt": ["$incidencia", "$umbral_accion_min"] },
                                    "then": "umbral 1 (minimo)",
                                    "else": {
                                        "$cond": {
                                            "if": { "$gte": ["$incidencia", "$umbral_accion_max"] },
                                            "then": "umbral 3 (daño)",
                                            "else": "umbral 2 (accion)"
                                        }
                                    }

                                }
                            }
                        }
                    }

                    , {
                        "$sort": {
                            "enfermedad_o_plaga": 1
                        }
                    }


                    , {
                        "$lookup": {
                            "from": "form_puentecartografialoteagrupado",
                            "as": "data_lote",
                            "let": {
                                "finca": "$finca",
                                "lote": "$lote"
                            },

                            "pipeline": [

                                {
                                    "$match": {
                                        "$expr": {
                                            "$and": [
                                                { "$eq": ["$$finca", "$Finca"] }
                                                , { "$eq": ["$$lote", "$Lote"] }
                                            ]
                                        }
                                    }
                                }

                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$data_lote",
                            "preserveNullAndEmptyArrays": true
                        }
                    }


                    , { "$addFields": { "Lote Agrupado": { "$ifNull": ["$data_lote.Lote Agrupado", "$lote"] } } }


                    , {
                        "$project": {
                            "data_lote": 0
                        }
                    }


                    , {
                        "$match": {
                            "enfermedad_o_plaga": {
                                "$in": [
                                    "Colletotrichum gloeosporioides / Antracnosis",
                                    "Lenticelosis",
                                    "Phytophthora cinnamomi /Pudricion de raices"


                                ]
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "alias_enfermedad_o_plaga": {
                                "$switch": {
                                    "branches": [

                                        {
                                            "case": { "$eq": ["$enfermedad_o_plaga", "Colletotrichum gloeosporioides / Antracnosis"] },
                                            "then": "Antracnosis"
                                        },
                                        {
                                            "case": { "$eq": ["$enfermedad_o_plaga", "Lenticelosis"] },
                                            "then": "Lenticelosis"
                                        },
                                        {
                                            "case": { "$eq": ["$enfermedad_o_plaga", "Phytophthora cinnamomi /Pudricion de raices"] },
                                            "then": "Phytophthora cinnamomi"
                                        }




                                    ],
                                    "default": ""
                                }
                            }
                        }
                    }


                    , {
                        "$addFields": {
                            "array_data_normalizada": [
                                {
                                    "alias_enfermedad_o_plaga": "$alias_enfermedad_o_plaga",
                                    "tipo_incidencia_o_severidad": "Incidencia",
                                    "valor": "$incidencia"
                                },
                                {
                                    "alias_enfermedad_o_plaga": "$alias_enfermedad_o_plaga",
                                    "tipo_incidencia_o_severidad": "Severidad",
                                    "valor": "$severidad"
                                }

                            ]
                        }
                    }

                    , {
                        "$unwind": {
                            "path": "$array_data_normalizada",
                            "preserveNullAndEmptyArrays": false
                        }
                    }




                ]
            }
        }


        , {
            "$project":
            {
                "datos": {
                    "$concatArrays": [
                        "$data"
                        , []
                    ]
                }
            }
        }

        , { "$unwind": "$datos" }
        , { "$replaceRoot": { "newRoot": "$datos" } }



        , {
            "$match": {
                "array_data_normalizada.tipo_incidencia_o_severidad": "Severidad"
            }
        }


        , {
            "$addFields": {
                "variable_label": "$finca"

                , "variable_dataset": "$array_data_normalizada.alias_enfermedad_o_plaga"
            }
        }



        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$variable_label"
                    , "dashboard_dataset": "$variable_dataset"
                }
                , "dashboard_cantidad": { "$avg": "$array_data_normalizada.valor" }

            }
        }

        , {
            "$group": {
                "_id": {
                    "dashboard_dataset": "$_id.dashboard_dataset"
                }
                , "data_group": { "$push": "$$ROOT" }

            }
        }

        , {
            "$sort": {
                "_id.dashboard_dataset": 1
            }
        }


        , {
            "$group": {
                "_id": null
                , "data_group": { "$push": "$$ROOT" }
                , "array_dashboard_dataset": { "$push": "$_id.dashboard_dataset" }
            }
        }

        , { "$unwind": "$data_group" }

        , { "$unwind": "$data_group.data_group" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data_group.data_group",
                        {
                            "array_dashboard_dataset": "$array_dashboard_dataset"
                        }
                    ]
                }
            }
        }


        , {
            "$group": {
                "_id": {
                    "dashboard_label": "$_id.dashboard_label"
                }
                , "data_group": { "$push": "$$ROOT" }
            }
        }
        , {
            "$sort": {
                "_id.dashboard_label": 1
            }
        }


        , {
            "$group": {
                "_id": null
                , "dashboard_data": {
                    "$push": "$$ROOT"
                }
            }
        }


        , {
            "$addFields": {
                "datos_dashboard": {
                    "$map": {
                        "input": "$dashboard_data",
                        "as": "item_dashboard_data",
                        "in": {
                            "$reduce": {
                                "input": "$$item_dashboard_data.data_group",
                                "initialValue": [],
                                "in": {
                                    "$concatArrays": [
                                        "$$value",
                                        [
                                            {
                                                "dashboard_label": "$$this._id.dashboard_label",
                                                "dashboard_dataset": "$$this._id.dashboard_dataset",
                                                "dashboard_cantidad": "$$this.dashboard_cantidad"
                                            }
                                        ]
                                    ]
                                }
                            }
                        }

                    }
                }
            }
        }


        , {
            "$addFields": {
                "datos_dashboard": {
                    "$reduce": {
                        "input": "$datos_dashboard",
                        "initialValue": [],
                        "in": {
                            "$concatArrays": [
                                "$$value",
                                "$$this"
                            ]
                        }
                    }
                }
            }
        }



        , {
            "$addFields": {
                "DATA_LABELS": { "$map": { "input": "$dashboard_data", "as": "item", "in": "$$item._id.dashboard_label" } }
            }
        }


        , {
            "$addFields": {
                "info_datasets": {
                    "$arrayElemAt": [{ "$arrayElemAt": ["$dashboard_data.data_group.array_dashboard_dataset", 0] }, 0]
                }
            }
        }

        , {
            "$addFields": {
                "array_colores": [

                    "#773e91",
                    "#61c3ac",
                    "#fed330",
                    "#fc5c65",
                    "#45aaf2",
                    "#d1d8e0",
                    "#635f6d",

                    "#474787",
                    "#218c74",
                    "#26de81",
                    "#b33939",
                    "#218c74",
                    "#227093",
                    "#cd6133",
                    "#84817a",
                    "#40407a",
                    "#33d9b2",
                    "#ffda79",
                    "#2c2c54",



                    "#a55eea",
                    "#2bcbba",
                    "#fd9644",
                    "#4b7bec",
                    "#20bf6b",
                    "#4b6584",
                    "#ffb142",


                    "#fc5c65",
                    "#20bf6b",
                    "#3867d6",
                    "#fed330",
                    "#a55eea",
                    "#45aaf2",
                    "#fa8231",
                    "#eb3b5a",
                    "#0fb9b1",
                    "#d1d8e0",

                    "#8854d0",


                    "#63b598",
                    "#ce7d78",
                    "#ea9e70",
                    "#a48a9e",
                    "#c6e1e8",
                    "#648177",
                    "#0d5ac1",
                    "#f205e6",
                    "#1c0365",
                    "#14a9ad",
                    "#4ca2f9",
                    "#a4e43f",
                    "#d298e2",
                    "#6119d0",
                    "#d2737d",
                    "#c0a43c",
                    "#f2510e",
                    "#651be6",
                    "#79806e",
                    "#61da5e",
                    "#cd2f00",
                    "#9348af",
                    "#01ac53",
                    "#c5a4fb",
                    "#996635",
                    "#b11573",
                    "#4bb473",
                    "#75d89e",
                    "#2f3f94",
                    "#2f7b99",
                    "#da967d",
                    "#34891f",
                    "#b0d87b",
                    "#ca4751",
                    "#7e50a8",
                    "#c4d647",
                    "#e0eeb8",
                    "#11dec1",
                    "#289812",
                    "#566ca0",
                    "#ffdbe1",
                    "#2f1179",
                    "#935b6d",
                    "#916988",
                    "#513d98",
                    "#aead3a",
                    "#9e6d71",
                    "#4b5bdc",
                    "#0cd36d",
                    "#250662",
                    "#cb5bea",
                    "#228916",
                    "#ac3e1b",
                    "#df514a",
                    "#539397",
                    "#880977",
                    "#f697c1",
                    "#ba96ce",
                    "#679c9d",
                    "#c6c42c",
                    "#5d2c52",
                    "#48b41b",
                    "#e1cf3b",
                    "#5be4f0",
                    "#57c4d8",
                    "#a4d17a",
                    "#be608b",
                    "#96b00c",
                    "#088baf",
                    "#f158bf",
                    "#e145ba",
                    "#ee91e3",
                    "#05d371",
                    "#5426e0",
                    "#4834d0",
                    "#802234",
                    "#6749e8",
                    "#0971f0",
                    "#8fb413",
                    "#b2b4f0",
                    "#c3c89d",
                    "#c9a941",
                    "#41d158",
                    "#fb21a3",
                    "#51aed9",
                    "#5bb32d",
                    "#21538e",
                    "#89d534",
                    "#d36647",
                    "#7fb411",
                    "#0023b8",
                    "#3b8c2a",
                    "#986b53",
                    "#f50422",
                    "#983f7a",
                    "#ea24a3",
                    "#79352c",
                    "#521250",
                    "#c79ed2",
                    "#d6dd92",
                    "#e33e52",
                    "#b2be57",
                    "#fa06ec",
                    "#1bb699",
                    "#6b2e5f",
                    "#64820f",
                    "#21538e",
                    "#89d534",
                    "#d36647",
                    "#7fb411",
                    "#0023b8",
                    "#3b8c2a",
                    "#986b53",
                    "#f50422",
                    "#983f7a",
                    "#ea24a3",
                    "#79352c",
                    "#521250",
                    "#c79ed2",
                    "#d6dd92",
                    "#e33e52",
                    "#b2be57",
                    "#fa06ec",
                    "#1bb699",
                    "#6b2e5f",
                    "#64820f",
                    "#9cb64a",
                    "#996c48",
                    "#9ab9b7",
                    "#06e052",
                    "#e3a481",
                    "#0eb621",
                    "#fc458e",
                    "#b2db15",
                    "#aa226d",
                    "#792ed8",
                    "#73872a",
                    "#520d3a",
                    "#cefcb8",
                    "#a5b3d9",
                    "#7d1d85",
                    "#c4fd57",
                    "#f1ae16",
                    "#8fe22a",
                    "#ef6e3c",
                    "#243eeb",
                    "#dd93fd",
                    "#3f8473",
                    "#e7dbce",
                    "#421f79",
                    "#7a3d93",

                    "#93f2d7",
                    "#9b5c2a",
                    "#15b9ee",
                    "#0f5997",
                    "#409188",
                    "#911e20",
                    "#1350ce",
                    "#10e5b1",
                    "#fff4d7",
                    "#cb2582",
                    "#ce00be",
                    "#32d5d6",
                    "#608572",
                    "#c79bc2",
                    "#00f87c",
                    "#77772a",
                    "#6995ba",
                    "#fc6b57",
                    "#f07815",
                    "#8fd883",
                    "#060e27",
                    "#96e591",
                    "#21d52e",
                    "#d00043",
                    "#b47162",
                    "#1ec227",
                    "#4f0f6f",
                    "#1d1d58",
                    "#947002",
                    "#bde052",
                    "#e08c56",
                    "#28fcfd",
                    "#36486a",
                    "#d02e29",
                    "#1ae6db",
                    "#3e464c",
                    "#a84a8f",
                    "#911e7e",
                    "#3f16d9",
                    "#0f525f",
                    "#ac7c0a",
                    "#b4c086",
                    "#c9d730",
                    "#30cc49",
                    "#3d6751",
                    "#fb4c03",
                    "#640fc1",
                    "#62c03e",
                    "#d3493a",
                    "#88aa0b",
                    "#406df9",
                    "#615af0",
                    "#2a3434",
                    "#4a543f",
                    "#79bca0",
                    "#a8b8d4",
                    "#00efd4",
                    "#7ad236",
                    "#7260d8",
                    "#1deaa7",
                    "#06f43a",
                    "#823c59",
                    "#e3d94c",
                    "#dc1c06",
                    "#f53b2a",
                    "#b46238",
                    "#2dfff6",
                    "#a82b89",
                    "#1a8011",
                    "#436a9f",
                    "#1a806a",
                    "#4cf09d",
                    "#c188a2",
                    "#67eb4b",
                    "#b308d3",
                    "#fc7e41",
                    "#af3101",
                    "#71b1f4",
                    "#a2f8a5",
                    "#e23dd0",
                    "#d3486d",
                    "#00f7f9",
                    "#474893",
                    "#3cec35",
                    "#1c65cb",
                    "#5d1d0c",
                    "#2d7d2a",
                    "#ff3420",
                    "#5cdd87",
                    "#a259a4",
                    "#e4ac44",
                    "#1bede6",
                    "#8798a4",
                    "#d7790f",
                    "#b2c24f",
                    "#de73c2",
                    "#d70a9c",
                    "#88e9b8",
                    "#c2b0e2",
                    "#86e98f",
                    "#ae90e2",
                    "#1a806b",
                    "#436a9e",
                    "#0ec0ff"

                ]
            }
        }




        , {
            "$addFields": {
                "DATA_ARRAY_DATASETS": {
                    "$map": {
                        "input": "$info_datasets",
                        "as": "item_info_datasets",
                        "in": {
                            "label": "$$item_info_datasets",
                            "backgroundColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
                            "borderColor": { "$arrayElemAt": ["$array_colores", { "$indexOfArray": ["$info_datasets", "$$item_info_datasets"] }] },
                            "borderWidth": 0,
                            "data": {
                                "$map": {
                                    "input": "$DATA_LABELS",
                                    "as": "item_data_labels",
                                    "in": {
                                        "$reduce": {
                                            "input": "$datos_dashboard",
                                            "initialValue": 0,
                                            "in": {
                                                "$cond": {
                                                    "if": {
                                                        "$and": [
                                                            { "$eq": ["$$item_info_datasets", "$$this.dashboard_dataset"] }
                                                            , { "$eq": ["$$item_data_labels", "$$this.dashboard_label"] }
                                                        ]

                                                    },
                                                    "then": "$$this.dashboard_cantidad",
                                                    "else": "$$value"
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
        }




        , { "$project": { "_id": 0 } }


        , {
            "$project": {

                "data": {
                    "labels": "$DATA_LABELS",
                    "datasets": "$DATA_ARRAY_DATASETS"
                },
                "options": {
                    "title": {
                        "display": true,
                        "text": "Chart.js Bar Chart"
                    }
                }

            }
        }







    ]
