[
    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Palma.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Palma.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },
    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "Lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
            "Linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
            "Planta": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }
        }
    },
    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "Lote": "$Lote.properties.name",
            "Linea": "$Linea.properties.name",
            "Planta": "$Planta.properties.name"
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },
    {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    },
    { "$unwind": "$Finca" },
    {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0
        }
    }
    
    
    ,{
        "$addFields": {
            "Opcion PC Censo": {"$ifNull": [
                "$TIPO DE SEGUIMIENTO_Lista de Grados", 
                {"$ifNull": ["$PC CENSO", "---"]}
            ]},
            
            "Opcion Cirugia": {"$ifNull": [
                "$TIPO DE SEGUIMIENTO_Cirugia", 
                "---"
                ]},
            "Opcion Estado de Cirugia":{"$ifNull": [
                "$TIPO DE SEGUIMIENTO_Lista de Estado de la Cirugia",
                {"$ifNull": ["$ESTADO CIRUGIA", "---"]}
                ]}
        }
    }
    
    ,{
        "$project": {
            "TIPO DE SEGUIMIENTO_Lista de Grados": 0,
            "PC CENSO": 0,
            
            "TIPO DE SEGUIMIENTO_Cirugia": 0,
            
            
            "TIPO DE SEGUIMIENTO_Lista de Estado de la Cirugia": 0,
            "ESTADO CIRUGIA": 0
        }
    }
]   