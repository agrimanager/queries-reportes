db.form_monitoresdeplagas.aggregate(
    [

        //----------------------------------
        //-----VARIBLES IYECTADAS MAPA
        {
            $addFields: {
                "Busqueda inicio": ISODate("2020-05-20T19:00:00.000-05:00"),
                "Busqueda fin": new Date,
                "today": new Date,
                //------MAPA_VARIABLE_IDFORM
                "idform": "123",
            }
        },

        // //FILTRO FINCA
        // {
        //     "$match": {
        //         "$expr": {
        //             "$and": [
        //                 {
        //                     "$eq": ["$Point.farm", "605126833abafc311c16a3f3"]
        //                 }
        //             ]
        //         }
        //     }
        // },

        //FILTRO FECHAS
        {
            "$match": {
                "$expr": {
                    "$and": [
                        {
                            "$gte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda inicio" } } }
                            ]
                        },

                        {
                            "$lte": [
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$rgDate" } } }
                                ,
                                { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Busqueda fin" } } }
                            ]
                        }
                    ]
                }
            }
        },
        //----------------------------------



        //================================
        //-----QUERY REPORTE
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": {
                    "$split": [
                        {
                            "$trim": {
                                "input": "$variable_cartografia.path",
                                "chars": ","
                            }
                        },
                        ","
                    ]
                }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": {
                    "$map": {
                        "input": "$split_path_padres",
                        "as": "strid",
                        "in": { "$toObjectId": "$$strid" }
                    }
                }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [
                    { "$toObjectId": "$variable_cartografia.features._id" }
                ]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": "$split_path_oid" },
                                { "$size": "$objetos_del_cultivo" }
                            ]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "blocks"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] }
            }
        },
        {
            "$addFields": {
                "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "lot"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "lote": { "$ifNull": ["$lote.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "lines"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "linea": { "$ifNull": ["$linea.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "trees"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        //------MAPA_VARIABLE_ID_FEATURE
        {
            "$addFields": {
                "cartography_id": { "$ifNull": ["$arbol._id", null] }
            }
        },

        {
            "$addFields": {
                "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] }
            }
        },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Arbol": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0
            }
        },


        {
            "$addFields": {
                "plagas_dif": [

                    {
                        "plaga": "Cochinilla",
                        "numero": { "$toDouble": "$Cochinilla" }
                    },
                    {
                        "plaga": "Escama",
                        "numero": { "$toDouble": "$Escama" }
                    },
                    {
                        "plaga": "Marceno",
                        "numero": { "$toDouble": "$Marceno" }
                    },
                    {
                        "plaga": "Picudo Blanco",
                        "numero": { "$toDouble": "$Picudo Blanco" }
                    },
                    {
                        "plaga": "Monalonium",
                        "numero": { "$toDouble": "$Monalonium" }
                    },
                    {
                        "plaga": "Orthezia",
                        "numero": { "$toDouble": "$Orthezia" }
                    },
                    {
                        "plaga": "Afidos",
                        "numero": { "$toDouble": "$Afidos" }
                    },
                    {
                        "plaga": "Heilipus lauri",
                        "numero": { "$toDouble": "$Heilipus lauri" }
                    },
                    {
                        "plaga": "Stenoma catenifer",
                        "numero": { "$toDouble": "$Stenoma catenifer" }
                    },
                    {
                        "plaga": "Mosca del ovario",
                        "numero": { "$toDouble": "$Mosca del ovario" }
                    },
                    {
                        "plaga": "Barrenador del tallo",
                        "numero": { "$toDouble": "$Barrenador del tallo" }
                    },
                    {
                        "plaga": "Compsus sp",
                        "numero": { "$toDouble": "$Compsus sp" }
                    },
                    {
                        "plaga": "Mosca blanca",
                        "numero": { "$toDouble": "$Mosca blanca" }
                    },
                    {
                        "plaga": "Gusano canasta",
                        "numero": { "$toDouble": "$Gusano canasta" }
                    },
                    {
                        "plaga": "La Arriera",
                        "numero": { "$toDouble": "$La Arriera" }
                    },

                    {
                        "plaga": "Fitoseido",
                        "numero": { "$toDouble": "$Fitoseido" }
                    },
                    {
                        "plaga": "Chrysopa",
                        "numero": { "$toDouble": "$Chrysopa" }
                    },
                    {
                        "plaga": "Cicloneda",
                        "numero": { "$toDouble": "$Cicloneda" }
                    },
                    {
                        "plaga": "Coccinelida",
                        "numero": { "$toDouble": "$Coccinelida" }
                    },
                    {
                        "plaga": "Hymenoptero",
                        "numero": { "$toDouble": "$Hymenoptero" }
                    },
                    {
                        "plaga": "Reduviidae",
                        "numero": { "$toDouble": "$Reduviidae" }
                    },
                    {
                        "plaga": "Syrphidae",
                        "numero": { "$toDouble": "$Syrphidae" }
                    },


                    {
                        "plaga": "Trips Hojas",
                        "numero": { "$toDouble": "$Hojas" }
                    },
                    {
                        "plaga": "Trips Flores",
                        "numero": { "$toDouble": "$Flores" }
                    },
                    {
                        "plaga": "Trips Frutos",
                        "numero": { "$toDouble": "$Frutos" }
                    },
                    {
                        "plaga": "Trips Tallo",
                        "numero": { "$toDouble": "$Tallo" }
                    },


                    {
                        "plaga": "Acaros o arañas Hojas a",
                        "numero": { "$toDouble": "$Hojas a" }
                    },
                    {
                        "plaga": "Acaros o arañas Flores a",
                        "numero": { "$toDouble": "$Flores a" }
                    },
                    {
                        "plaga": "Acaros o arañas Frutos a",
                        "numero": { "$toDouble": "$Frutos a" }
                    }
                ]
            }
        },

        {
            "$addFields": {
                "date": {
                    "$dateToString": {
                        "date": "$rgDate",
                        "format": "%d-%m-%Y"
                    }
                }
            }
        },

        { "$unwind": "$plagas_dif" },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol",
                    "plagas_dif": "$plagas_dif",
                    "date": "$date"
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote",
                    "plagas_dif": "$_id.plagas_dif.plaga"
                },
                "count": { "$sum": "$_id.plagas_dif.numero" },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$addFields": {
                "plagas": {
                    "$mergeObjects": [
                        { "plaga": "$_id.plagas_dif" },
                        { "numero_total": "$count" }
                    ]
                }
            }
        },




        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data"
                        , { "plagas": "$plagas" }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "plagas": {
                    "$mergeObjects": [
                        "$plagas",
                        { "numero_de_arbol": "$plagas_dif.numero" }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "semana": { "$week": "$rgDate" },
                "año": { "$year": "$rgDate" }
            }
        },
        {
            "$addFields": {
                "semana_y_año": {
                    "$concat": [
                        { "$toString": "$semana" },
                        "-",
                        { "$toString": "$año" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": "$semana_y_año",
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": null,
                "semanas_arr": { "$push": "$_id" },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$addFields": {
                "num_semanas": { "$size": "$semanas_arr" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "num_semanas": "$num_semanas" }

                    ]
                }
            }
        },
        {
            "$addFields": {
                "const_num_arboles_muestra": { "$multiply": [34, "$num_semanas"] }
            }
        },



        {
            "$addFields": {
                "plagas": {
                    "$mergeObjects": [
                        "$plagas",
                        {
                            "porc_incidencia": {
                                "$cond": {
                                    "if": {
                                        "$eq": ["$const_num_arboles_muestra", 0]
                                    },
                                    "then": 0,
                                    "else": {
                                        "$divide": [
                                            {
                                                "$floor": {
                                                    "$multiply": [

                                                        {
                                                            "$multiply": [
                                                                {
                                                                    "$divide": [
                                                                        "$plagas.numero_total",
                                                                        "$const_num_arboles_muestra"
                                                                    ]
                                                                },
                                                                100
                                                            ]
                                                        },

                                                        100
                                                    ]
                                                }
                                            },
                                            100
                                        ]
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "_id": "$_id",

                    "Estado nutricional": "$Estado nutricional",
                    "supervisor": "$supervisor",
                    "rgDate": "$rgDate",
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol",

                    "idform": "$idform",
                    "cartography_id": "$cartography_id",

                    "const_num_arboles_muestra": "$const_num_arboles_muestra",
                    "semana": "$semana",
                    "num_semanas": "$num_semanas",
                    "plagas": "$plagas"
                }
            }
        },

        {
            "$group": {
                "_id": {
                    "_id": "$_id._id",

                    "Estado nutricional": "$_id.Estado nutricional",
                    "supervisor": "$_id.supervisor",
                    "rgDate": "$_id.rgDate",
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote",
                    "arbol": "$_id.arbol",

                    "idform": "$_id.idform",
                    "cartography_id": "$_id.cartography_id",

                    "semana": "$_id.semana",
                    "num_semanas": "$_id.num_semanas",
                    "const_num_arboles_muestra": "$_id.const_num_arboles_muestra"
                },
                "plagas": { "$push": "$_id.plagas" }
            }
        },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": ["$_id", { "plagas": "$plagas" }]
                }
            }
        },

        {
            "$unwind": "$plagas"
        },


        {
            "$addFields": {
                "semana del año": "$semana",
                "numero de semanas": "$num_semanas",
                "numero arboles de muestra": "$const_num_arboles_muestra",
                "plagas": "$plagas.plaga",
                "numero_de_arbol": "$plagas.numero_de_arbol",
                "numero_total": "$plagas.numero_total",
                "porc_incidencia": "$plagas.porc_incidencia"
            }
        },
        {
            "$project": {
                "semana": 0,
                "num_semanas": 0,
                "const_num_arboles_muestra": 0
            }
        }


        //================================


        //new
        //===>> filtrar ceros
        , {
            "$match": {
                "numero_total": { "$ne": 0 }
            }
        }



        //================================
        //-----MAPA

        //--indicador color
        , {
            "$addFields": {
                "indicador": "$porc_incidencia"//🚩EDITAR🚩
            }
        }
        , {
            "$addFields": {
                "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$indicador", 100] }, { "$mod": [{ "$multiply": ["$indicador", 100] }, 1] }] }, 100] }
            }
        }



        //--agrupacion lote x elemento_agrupacion
        , {
            "$addFields": {
                "elemento_agrupacion": "$plagas"//🚩EDITAR🚩
            }
        }
        , {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote"
                    , "arbol": "$arbol"

                    , "idform": "$idform"
                    , "elemento_agrupacion": "$elemento_agrupacion"
                }
                , "indicador": { "$min": "$indicador" }
                , "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote"
                    // ,"arbol": "$arbol"

                    , "idform": "$_id.idform"
                    , "elemento_agrupacion": "$_id.elemento_agrupacion"
                }
                , "indicador": { "$min": "$indicador" }
                , "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$_id",
                        {
                            "indicador": "$indicador"

                            , "cartography_id": {
                                "$arrayElemAt": [{ "$arrayElemAt": ["$data.data.cartography_id", 0] }, 0]
                            }

                        }
                    ]
                }
            }
        }


        //NOTA!!!!!!!!!
        //la geometria del lote rompia la agrupacion del lote
        //....===>>>entonces hacer lookup para sacar geometria
        //------MAPA_VARIABLE_GEOMETRY
        , {
            "$lookup": {
                "from": "cartography",
                "localField": "lote",
                "foreignField": "properties.name",
                "as": "info_lote"
            }
        }
        , { "$unwind": "$info_lote" }
        , { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } }
        , {
            "$project": {
                "info_lote": 0
            }
        }
        //----------------


        //--leyenda
        // % Incidencia Lote Plagas: #ffffff,[ 0% - 20% ): #0000ff,[ 20% - 40% ): #008000,[ 40% - 60% ): #ffff00,[ 60% - 80% ): #ffa500,[ 80% - >100% ): #ff0000


        // --color
        , {
            "$addFields": {
                "color": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lt": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "#0000ff"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 20] }
                                        , { "$lt": ["$indicador", 40] }
                                    ]
                                }
                                , "then": "#008000"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 40] }
                                        , { "$lt": ["$indicador", 60] }
                                    ]
                                }
                                , "then": "#ffff00"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 60] }
                                        , { "$lt": ["$indicador", 80] }
                                    ]
                                }
                                , "then": "#ffa500"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 80] }
                                    ]
                                }
                                , "then": "#ff0000"
                            }

                        ],
                        "default": "#000000"
                    }
                }

                , "rango": {
                    "$switch": {
                        "branches": [
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 0] }
                                        , { "$lt": ["$indicador", 20] }
                                    ]
                                }
                                , "then": "A-[ 0% - 20% )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 20] }
                                        , { "$lt": ["$indicador", 40] }
                                    ]
                                }
                                , "then": "B-[ 20% - 40% )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 40] }
                                        , { "$lt": ["$indicador", 60] }
                                    ]
                                }
                                , "then": "C-[ 40% - 60% )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 60] }
                                        , { "$lt": ["$indicador", 80] }
                                    ]
                                }
                                , "then": "D-[ 60% - 80% )"
                            },
                            {
                                "case": {
                                    "$and": [
                                        { "$gte": ["$indicador", 80] }
                                    ]
                                }
                                , "then": "E-[ 80% - >100% )"
                            }

                        ],
                        "default": "F-otro"
                    }
                }

            }
        }


        //=========================================
        //-----DATA_FINAL MAPA_VARIABLES !!! REQUERIDAS
        , {
            "$addFields": {
                "idform": "$idform"
                , "cartography_id": "$cartography_id"
                , "cartography_geometry": "$cartography_geometry"

                , "color": "$color"
                , "rango": "$rango"
            }
        }



        //--PROYECCION FINAL MAPA
        , {
            "$project": {
                //REQUERIDAS
                "_id": "$cartography_id",
                "idform": "$idform",
                "geometry": { "$ifNull": ["$cartography_geometry", {}] },

                "type": "Feature",

                //caracteristicas
                "properties": {
                    "Finca": "$finca",
                    "Bloque": "$bloque",
                    "Lote": "$lote",
                    "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },
                    "color": "$color"

                    , "Plagas": "$elemento_agrupacion" //🚩EDITAR🚩
                    , "% Incidencia": { "$concat": [{ "$toString": "$indicador" }, " %"] }//🚩EDITAR🚩
                }

            }
        }



    ]

)
