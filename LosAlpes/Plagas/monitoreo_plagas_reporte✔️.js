[

    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },

    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": {
                "$split": [
                    {
                        "$trim": {
                            "input": "$variable_cartografia.path",
                            "chars": ","
                        }
                    },
                    ","
                ]
            }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": {
                "$map": {
                    "input": "$split_path_padres",
                    "as": "strid",
                    "in": { "$toObjectId": "$$strid" }
                }
            }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [
                { "$toObjectId": "$variable_cartografia.features._id" }
            ]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": {
                        "$eq": [
                            { "$size": "$split_path_oid" },
                            { "$size": "$objetos_del_cultivo" }
                        ]
                    },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },

    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "blocks"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] }
        }
    },
    {
        "$addFields": {
            "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "lot"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "lote": { "$ifNull": ["$lote.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "lines"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "linea": { "$ifNull": ["$linea.properties.name", "no existe"] }
        }
    },

    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": {
                        "$eq": ["$$item_cartografia.properties.type", "trees"]
                    }
                }
            }
        }
    },

    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },

    {
        "$addFields": {
            "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] }
        }
    },

    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0,
            "Arbol": 0,
            "Point": 0,
            "bloque_id": 0,
            "Formula": 0
        }
    },


    {
        "$addFields": {
            "plagas_dif": [

                {
                    "plaga": "Cochinilla",
                    "numero": { "$toDouble": "$Cochinilla" }
                },
                {
                    "plaga": "Escama",
                    "numero": { "$toDouble": "$Escama" }
                },
                {
                    "plaga": "Marceno",
                    "numero": { "$toDouble": "$Marceno" }
                },
                {
                    "plaga": "Picudo Blanco",
                    "numero": { "$toDouble": "$Picudo Blanco" }
                },
                {
                    "plaga": "Monalonium",
                    "numero": { "$toDouble": "$Monalonium" }
                },
                {
                    "plaga": "Orthezia",
                    "numero": { "$toDouble": "$Orthezia" }
                },
                {
                    "plaga": "Afidos",
                    "numero": { "$toDouble": "$Afidos" }
                },
                {
                    "plaga": "Heilipus lauri",
                    "numero": { "$toDouble": "$Heilipus lauri" }
                },
                {
                    "plaga": "Stenoma catenifer",
                    "numero": { "$toDouble": "$Stenoma catenifer" }
                },
                {
                    "plaga": "Mosca del ovario",
                    "numero": { "$toDouble": "$Mosca del ovario" }
                },
                {
                    "plaga": "Barrenador del tallo",
                    "numero": { "$toDouble": "$Barrenador del tallo" }
                },
                {
                    "plaga": "Compsus sp",
                    "numero": { "$toDouble": "$Compsus sp" }
                },
                {
                    "plaga": "Mosca blanca",
                    "numero": { "$toDouble": "$Mosca blanca" }
                },
                {
                    "plaga": "Gusano canasta",
                    "numero": { "$toDouble": "$Gusano canasta" }
                },
                {
                    "plaga": "La Arriera",
                    "numero": { "$toDouble": "$La Arriera" }
                },

                {
                    "plaga": "Fitoseido",
                    "numero": { "$toDouble": "$Fitoseido" }
                },
                {
                    "plaga": "Chrysopa",
                    "numero": { "$toDouble": "$Chrysopa" }
                },
                {
                    "plaga": "Cicloneda",
                    "numero": { "$toDouble": "$Cicloneda" }
                },
                {
                    "plaga": "Coccinelida",
                    "numero": { "$toDouble": "$Coccinelida" }
                },
                {
                    "plaga": "Hymenoptero",
                    "numero": { "$toDouble": "$Hymenoptero" }
                },
                {
                    "plaga": "Reduviidae",
                    "numero": { "$toDouble": "$Reduviidae" }
                },
                {
                    "plaga": "Syrphidae",
                    "numero": { "$toDouble": "$Syrphidae" }
                },


                {
                    "plaga": "Trips Hojas",
                    "numero": { "$toDouble": "$Hojas" }
                },
                {
                    "plaga": "Trips Flores",
                    "numero": { "$toDouble": "$Flores" }
                },
                {
                    "plaga": "Trips Frutos",
                    "numero": { "$toDouble": "$Frutos" }
                },
                {
                    "plaga": "Trips Tallo",
                    "numero": { "$toDouble": "$Tallo" }
                },


                {
                    "plaga": "Acaros o arañas Hojas a",
                    "numero": { "$toDouble": "$Hojas a" }
                },
                {
                    "plaga": "Acaros o arañas Flores a",
                    "numero": { "$toDouble": "$Flores a" }
                },
                {
                    "plaga": "Acaros o arañas Frutos a",
                    "numero": { "$toDouble": "$Frutos a" }
                }
            ]
        }
    },

    {
        "$addFields": {
            "date": {
                "$dateToString": {
                    "date": "$rgDate",
                    "format": "%d-%m-%Y"
                }
            }
        }
    },

    { "$unwind": "$plagas_dif" },


    {
        "$group": {
            "_id": {
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "plagas_dif": "$plagas_dif",
                "date": "$date"
            },
            "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$group": {
            "_id": {
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "plagas_dif": "$_id.plagas_dif.plaga"
            },
            "count": { "$sum": "$_id.plagas_dif.numero" },
            "data": { "$push": "$$ROOT" }
        }
    },

    {
        "$addFields": {
            "plagas": {
                "$mergeObjects": [
                    { "plaga": "$_id.plagas_dif" },
                    { "numero_total": "$count" }
                ]
            }
        }
    },




    { "$unwind": "$data" },
    { "$unwind": "$data.data" },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data"
                    , { "plagas": "$plagas" }
                ]
            }
        }
    },

    {
        "$addFields": {
            "plagas": {
                "$mergeObjects": [
                    "$plagas",
                    { "numero_de_arbol": "$plagas_dif.numero" }
                ]
            }
        }
    },



    {
        "$addFields": {
            "semana": { "$week": "$rgDate" },
            "año": { "$year": "$rgDate" }
        }
    },
    {
        "$addFields": {
            "semana_y_año": {
                "$concat": [
                    { "$toString": "$semana" },
                    "-",
                    { "$toString": "$año" }
                ]
            }
        }
    },
    {
        "$group": {
            "_id": "$semana_y_año",
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$group": {
            "_id": null,
            "semanas_arr": { "$push": "$_id" },
            "data": { "$push": "$$ROOT" }
        }
    },
    {
        "$addFields": {
            "num_semanas": { "$size": "$semanas_arr" }
        }
    },
    { "$unwind": "$data" },
    { "$unwind": "$data.data" },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    { "num_semanas": "$num_semanas" }

                ]
            }
        }
    },
    {
        "$addFields": {
            "const_num_arboles_muestra": { "$multiply": [34, "$num_semanas"] }
        }
    },



    {
        "$addFields": {
            "plagas": {
                "$mergeObjects": [
                    "$plagas",
                    {
                        "porc_incidencia": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$const_num_arboles_muestra", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": [
                                        {
                                            "$floor": {
                                                "$multiply": [

                                                    {
                                                        "$multiply": [
                                                            {
                                                                "$divide": [
                                                                    "$plagas.numero_total",
                                                                    "$const_num_arboles_muestra"
                                                                ]
                                                            },
                                                            100
                                                        ]
                                                    },

                                                    100
                                                ]
                                            }
                                        },
                                        100
                                    ]
                                }
                            }
                        }
                    }
                ]
            }
        }
    },


    {
        "$group": {
            "_id": {
                "_id": "$_id",

                "Estado nutricional": "$Estado nutricional",
                "supervisor": "$supervisor",
                "rgDate": "$rgDate",
                "finca": "$finca",
                "bloque": "$bloque",
                "lote": "$lote",
                "arbol": "$arbol",
                "const_num_arboles_muestra": "$const_num_arboles_muestra",
                "semana": "$semana",
                "num_semanas": "$num_semanas",
                "plagas": "$plagas"
            }
        }
    },

    {
        "$group": {
            "_id": {
                "_id": "$_id._id",

                "Estado nutricional": "$_id.Estado nutricional",
                "supervisor": "$_id.supervisor",
                "rgDate": "$_id.rgDate",
                "finca": "$_id.finca",
                "bloque": "$_id.bloque",
                "lote": "$_id.lote",
                "arbol": "$_id.arbol",
                "semana": "$_id.semana",
                "num_semanas": "$_id.num_semanas",
                "const_num_arboles_muestra": "$_id.const_num_arboles_muestra"
            },
            "plagas": { "$push": "$_id.plagas" }
        }
    },

    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": ["$_id", { "plagas": "$plagas" }]
            }
        }
    },

    {
        "$unwind": "$plagas"
    },


    {
        "$addFields": {
            "semana del año": "$semana",
            "numero de semanas": "$num_semanas",
            "numero arboles de muestra": "$const_num_arboles_muestra",
            "plagas": "$plagas.plaga",
            "numero_de_arbol": "$plagas.numero_de_arbol",
            "numero_total": "$plagas.numero_total",
            "porc_incidencia": "$plagas.porc_incidencia"
        }
    },
    {
        "$project": {
            "semana": 0,
            "num_semanas": 0,
            "const_num_arboles_muestra": 0
        }
    }





]
