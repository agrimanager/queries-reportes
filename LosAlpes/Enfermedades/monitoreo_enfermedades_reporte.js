db.form_monitoreoenfermedades.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": {
                    "$split": [
                        {
                            "$trim": {
                                "input": "$variable_cartografia.path",
                                "chars": ","
                            }
                        },
                        ","
                    ]
                }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": {
                    "$map": {
                        "input": "$split_path_padres",
                        "as": "strid",
                        "in": { "$toObjectId": "$$strid" }
                    }
                }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [
                    { "$toObjectId": "$variable_cartografia.features._id" }
                ]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": {
                            "$eq": [
                                { "$size": "$split_path_oid" },
                                { "$size": "$objetos_del_cultivo" }
                            ]
                        },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },

        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "blocks"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] }
            }
        },
        {
            "$addFields": {
                "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "lot"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "lote": { "$ifNull": ["$lote.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "lines"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "linea": { "$ifNull": ["$linea.properties.name", "no existe"] }
            }
        },

        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": {
                            "$eq": ["$$item_cartografia.properties.type", "trees"]
                        }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },

        {
            "$addFields": {
                "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] }
            }
        },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "Arbol": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "lote": "$lote",
                    "arbol": "$arbol",
                    "enfermedad": "$Enfermedad"
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "lote": "$_id.lote",
                    "enfermedad": "$_id.enfermedad"
                },
                "count": { "$sum": 1 },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "numero_de_arboles_x_enfermedad": "$count" }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "semana": { "$week": "$rgDate" },
                "año": { "$year": "$rgDate" }
            }
        },
        {
            "$addFields": {
                "semana_y_año": {
                    "$concat": [
                        { "$toString": "$semana" },
                        "-",
                        { "$toString": "$año" }
                    ]
                }
            }
        },
        {
            "$group": {
                "_id": "$semana_y_año",
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$group": {
                "_id": null,
                "semanas_arr": { "$push": "$_id" },
                "data": { "$push": "$$ROOT" }
            }
        },
        {
            "$addFields": {
                "num_semanas": { "$size": "$semanas_arr" }
            }
        },
        { "$unwind": "$data" },
        { "$unwind": "$data.data" },
        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "num_semanas": "$num_semanas" }

                    ]
                }
            }
        },
        {
            "$addFields": {
                "const_num_arboles_muestra": { "$multiply": [34, "$num_semanas"] }
            }
        },



        {
            "$addFields": {
                "porc_incidencia_x_enfermedad": {
                    "$cond": {
                        "if": { "$eq": ["$const_num_arboles_muestra", 0] },
                        "then": 0,
                        "else": {
                            "$divide": [
                                {
                                    "$floor": {
                                        "$multiply": [


                                            {
                                                "$multiply": [
                                                    {
                                                        "$divide": [
                                                            "$numero_de_arboles_x_enfermedad",
                                                            "$const_num_arboles_muestra"
                                                        ]
                                                    },
                                                    100
                                                ]
                                            },


                                            100
                                        ]
                                    }
                                },
                                100
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "semana del año": "$semana",
                "numero de semanas": "$num_semanas",
                "numero arboles de muestra": "$const_num_arboles_muestra",
                "numero arboles por enfermedad": "$numero_de_arboles_x_enfermedad",
                "% incidencia por enfermedad": "$porc_incidencia_x_enfermedad"
            }
        },
        {
            "$project": {
                "semana_y_año": 0,
                "semana": 0,
                "num_semanas": 0,
                "numero_de_arboles_x_enfermedad": 0,
                "const_num_arboles_muestra": 0,
                "porc_incidencia_x_enfermedad": 0
            }
        }
    ]
)
