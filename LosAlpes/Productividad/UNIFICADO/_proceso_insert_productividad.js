
//1)insertar datos en coleccion auxiliar "form_productividad_aux"
//2)insertar datos en coleccion real "form_productividad" con cartografia

var data = db.form_productividad_aux.aggregate(
    [

        //NOTA: se creo un indice en cartografia de en "properties.name" para optimizar el tiempo
        {
            "$lookup": {
                "from": "cartography",
                "as": "info_cartografia",
                "let": {
                    "arbol": "$Arbol"
                },
                //query
                "pipeline": [

                    // {
                    //     "$match": {
                    //         "properties.type": "trees"
                    //     }
                    // },
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {
                                        "$eq": ["$properties.name", "$$arbol"]
                                    }
                                ]
                            }
                        }
                    }
                ]
            }
        }
        , {
            "$unwind": {
                "path": "$info_cartografia",
                "preserveNullAndEmptyArrays": false
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { "$split": [{ "$trim": { "input": "$info_cartografia.path", "chars": "," } }, ","] }
            }
        }

        , {
            "$addFields": {
                "id_str_farm": { $arrayElemAt: ["$id_str_farm", 0] }
            }
        }

        , {
            "$addFields": {
                "fecha_texto": {
                    "$cond": {
                        "if": { "$eq": ["$Semestre", 1] },
                        "then": {
                            $concat: [{ $toString: "$Anio" }, "-06", "-15"]
                        },
                        "else": {
                            $concat: [{ $toString: "$Anio" }, "-12", "-15"]
                        }
                    }
                }
            }
        }




        //datos
        , {
            $addFields: {
                item_insert_form:
                {
                    // "_id": ObjectId("64375a8f68c42eb0b7f32e57"),

                    //cartografia
                    "Arbol": {
                        "type": "selection",
                        "features": [
                            {
                                "_id": { $toString: "$info_cartografia._id" },
                                "type": "Feature",
                                "properties": {
                                    "name": "$info_cartografia.properties.name",
                                    "type": "$info_cartografia.properties.type",
                                    "production": "$info_cartografia.properties.production",
                                    "status": "$info_cartografia.properties.status",
                                    "custom": "$info_cartografia.properties.custom",
                                },
                                "geometry": {
                                    "type": "$info_cartografia.geometry.type",
                                    "coordinates": "$info_cartografia.geometry.coordinates"
                                },
                                "path": "$info_cartografia.path"
                            }
                        ],
                        "path": "$info_cartografia.path"
                    },

                    //datos
                    "Productividad": "$Productividad",
                    "Anio": "$Anio",
                    "Semestre": "$Semestre",

                    //otros
                    "Point": {
                        "type": "Feature",
                        "geometry": {
                            "coordinates": [
                                "-76.10868068220557",
                                "6.2727569969067645"
                            ],
                            "type": "point"
                        },
                        "farm": "$id_str_farm"
                    },
                    "uid": ObjectId("5f0329938a7d9a4957a96542"),
                    "supervisor": "Support team",
                    rgDate: {
                        $dateFromString: {
                            dateString: "$fecha_texto"
                        }
                    },
                    uDate: {
                        $dateFromString: {
                            dateString: "$fecha_texto"
                        }
                    },
                    "capture": "W"
                }



            }
        }

        , {
            "$project": {
                "item_insert_form": 1
            }
        }




    ]
    , { allowDiskUse: true }
)

// data

//--BULK INSERT
var bulk = db.form_productividad.initializeUnorderedBulkOp();

data.forEach(i => {

    bulk.insert(i.item_insert_form);
})


bulk.execute();



/*
// collection: form_productividad
{
	"_id" : ObjectId("64431bcfee3939acb207c45c"),
	"Arbol" : {
		"type" : "selection",
		"features" : [
			{
				"_id" : "5fd39dfc1802b276bde6b182",
				"type" : "Feature",
				"properties" : {
					"name" : "E1-L2-S31-25",
					"type" : "trees",
					"production" : true,
					"status" : true,
					"custom" : {

					}
				},
				"geometry" : {
					"type" : "Point",
					"coordinates" : [
						-76.107199504312,
						6.27466882030783
					]
				},
				"path" : ",5f03315b8a7d9a4957a96547,5fd283281802b276bde6ab2e,5f63ada91802b276bde5315f,5fd39dfc1802b276bde6b0bc,"
			}
		],
		"path" : ",5f03315b8a7d9a4957a96547,5fd283281802b276bde6ab2e,5f63ada91802b276bde5315f,5fd39dfc1802b276bde6b0bc,"
	},
	"Productividad" : 36,
	"Anio" : 2020,
	"Semestre" : 2,
	"Point" : {
		"type" : "Feature",
		"geometry" : {
			"coordinates" : [
				"-76.10868068220557",
				"6.2727569969067645"
			],
			"type" : "point"
		},
		"farm" : "5f03315b8a7d9a4957a96547"
	},
	"uid" : ObjectId("5f0329938a7d9a4957a96542"),
	"supervisor" : "Support team",
	"rgDate" : ISODate("2023-04-21T18:27:11.152-05:00"),
	"uDate" : ISODate("2023-04-21T18:27:11.152-05:00"),
	"capture" : "W"
}
*/
