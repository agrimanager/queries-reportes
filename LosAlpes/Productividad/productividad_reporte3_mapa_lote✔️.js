[




    {
        "$addFields": {
            "variable_cartografia": "$Arbol"
        }
    },
    { "$unwind": "$variable_cartografia.features" },

    {
        "$addFields": {
            "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_padres_oid",
                    "$variable_cartografia_oid"
                ]
            }
        }
    },
    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "tiene_variable_cartografia": {
                "$cond": {
                    "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                    "then": "si",
                    "else": "no"
                }
            }
        }
    },

    {
        "$addFields": {
            "objetos_del_cultivo": {
                "$cond": {
                    "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                    "then": "$objetos_del_cultivo",
                    "else": {
                        "$concatArrays": [
                            "$objetos_del_cultivo",
                            ["$variable_cartografia.features"]
                        ]
                    }
                }
            }
        }
    },



    {
        "$addFields": {
            "finca": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$finca",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$lookup": {
            "from": "farms",
            "localField": "finca._id",
            "foreignField": "_id",
            "as": "finca"
        }
    },
    { "$unwind": "$finca" },

    { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


    {
        "$addFields": {
            "bloque": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$bloque",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "lote": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$lote",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

    {
        "$addFields": {
            "linea": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$linea",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


    {
        "$addFields": {
            "arbol": {
                "$filter": {
                    "input": "$objetos_del_cultivo",
                    "as": "item_cartografia",
                    "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                }
            }
        }
    },
    {
        "$unwind": {
            "path": "$arbol",
            "preserveNullAndEmptyArrays": true
        }
    },
    { "$addFields": { "cartography_id": { "$ifNull": ["$arbol._id", null] } } },

    { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




    {
        "$project": {
            "variable_cartografia": 0,
            "split_path_padres": 0,
            "split_path_padres_oid": 0,
            "variable_cartografia_oid": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "tiene_variable_cartografia": 0

            , "uid": 0
            , "Point": 0
            , "Arbol": 0
        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$finca"
                , "bloque": "$bloque"
                , "lote": "$lote"

                , "idform": "$idform"




            }
            , "data": { "$push": "$$ROOT" }

            , "productividad_total_lote": { "$sum": { "$toDouble": "$Productividad" } }

        }
    }


    , {
        "$group": {
            "_id": {
                "finca": "$_id.finca"
                , "bloque": "$_id.bloque"
            }
            , "data": { "$push": "$$ROOT" }

            , "productividad_total_finca": { "$sum": "$productividad_total_lote" }

            , "productividad_total_lote_min": { "$min": "$productividad_total_lote" }
            , "productividad_total_lote_max": { "$max": "$productividad_total_lote" }

        }
    }


    , { "$unwind": "$data" }

    , {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data._id",
                    {

                        "productividad_total_lote": "$data.productividad_total_lote"

                        , "productividad_total_lote_min": "$productividad_total_lote_min"
                        , "productividad_total_lote_max": "$productividad_total_lote_max"
                        , "productividad_total_finca": "$productividad_total_finca"

                        , "cartography_id": {
                                "$arrayElemAt": ["$data.data.cartography_id", 0]
                            }
                    }
                ]
            }
        }
    }


    , {
        "$addFields": {

            "txt_productividad_lote": {
                "$cond": {
                    "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_min"] },
                    "then": "Lote Minimo Productivo",
                    "else": {
                        "$cond": {
                            "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_max"] },
                            "then": "Lote Maximo Productivo",
                            "else": "Rango intermedio"
                        }
                    }
                }
            }
        }
    }


    , {
        "$addFields": {
            "indicador": { "$divide": [{ "$subtract": [{ "$multiply": ["$productividad_total_lote", 100] }, { "$mod": [{ "$multiply": ["$productividad_total_lote", 100] }, 1] }] }, 100] }
        }
    }



    , {
        "$addFields": {
            "color": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 0] }
                                    , { "$lt": ["$indicador", 15000] }
                                ]
                            }
                            , "then": "#ff0000"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 15000] }
                                    , { "$lt": ["$indicador", 30000] }
                                ]
                            }
                            , "then": "#ffff00"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 30000] }
                                ]
                            }
                            , "then": "#008000"
                        }

                    ],
                    "default": "#000000"
                }
            }

            , "rango": {
                "$switch": {
                    "branches": [
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 0] }
                                    , { "$lt": ["$indicador", 15000] }
                                ]
                            }
                            , "then": "A-[ 0- 15000 )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 15000] }
                                    , { "$lt": ["$indicador", 30000] }
                                ]
                            }
                            , "then": "B-[ 15000- 30000 )"
                        },
                        {
                            "case": {
                                "$and": [
                                    { "$gte": ["$indicador", 30000] }
                                ]
                            }
                            , "then": "C-[ >= 30000 )"
                        }

                    ],
                    "default": "D-Otro"
                }
            }
        }
    }


    , {
        "$lookup": {
            "from": "cartography",
            "localField": "lote",
            "foreignField": "properties.name",
            "as": "info_lote"
        }
    }

    , { "$unwind": "$info_lote" },

    
    { "$addFields": { "cartography_geometry": { "$ifNull": ["$info_lote.geometry", {}] } } },

    {
        "$project": {
            "info_lote": 0
        }
    }




    , {
        "$project": {
            "_id": "$cartography_id",
            "idform": "$idform",

            "type": "Feature",
            "properties": {
                "Finca": "$finca",
                "Bloque": "$bloque",
                "Lote": "$lote",
                "Productividad": { "$concat": [{ "$toString": "$indicador" }, " Kg"] },

                "Tipo Productividad": { "$ifNull": ["$txt_productividad_lote", "SIN DATOS"] },
                "Rango": { "$ifNull": ["$rango", "SIN DATOS"] },

                "color": "$color"
            },
            "geometry": { "$ifNull": ["$cartography_geometry", {}] }
        }
    }



]
