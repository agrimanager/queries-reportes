db.form_productividad.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$Arbol"
            }
        },
        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },

        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },



        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },


        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "lote": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lot"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$lote",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "lote": { "$ifNull": ["$lote.properties.name", "no existe"] } } },

        {
            "$addFields": {
                "linea": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "lines"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$linea",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "linea": { "$ifNull": ["$linea.properties.name", "no existe"] } } },


        {
            "$addFields": {
                "arbol": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "trees"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$arbol",
                "preserveNullAndEmptyArrays": true
            }
        },
        { "$addFields": { "arbol": { "$ifNull": ["$arbol.properties.name", "no existe"] } } },




        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0

                , "uid": 0
                , "Point": 0
                , "Arbol": 0
            }
        }


        //========AGRUPAR
        //---agrupacion base
        , {
            "$group": {
                "_id": {
                    "finca": "$finca"
                    , "bloque": "$bloque"
                    , "lote": "$lote"
                    , "linea": "$linea"
                    , "arbol": "$arbol"
                }
                , "data": { "$push": "$$ROOT" }

                , "productividad_total_arbol": { "$sum": { "$toDouble": "$Productividad" } }

            }
        }

        //---agrupacion1
        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                    , "bloque": "$_id.bloque"
                    , "lote": "$_id.lote"
                    // , "linea": "$_id.linea"
                    // , "arbol": "$_id.arbol"
                }
                , "data": { "$push": "$$ROOT" }

                , "productividad_total_lote": { "$sum": "$productividad_total_arbol" }

                , "productividad_total_arbol_min": { "$min": "$productividad_total_arbol" }
                , "productividad_total_arbol_max": { "$max": "$productividad_total_arbol" }


            }
        }

        //---agrupacion2
        , {
            "$group": {
                "_id": {
                    "finca": "$_id.finca"
                    , "bloque": "$_id.bloque"
                    // , "lote": "$_id.lote"
                    // , "linea": "$_id.linea"
                    // , "arbol": "$_id.arbol"
                }
                , "data": { "$push": "$$ROOT" }

                , "productividad_total_finca": { "$sum": "$productividad_total_lote" }

                , "productividad_total_lote_min": { "$min": "$productividad_total_lote" }
                , "productividad_total_lote_max": { "$max": "$productividad_total_lote" }

            }
        }


        //========DES-AGRUPAR
        , { "$unwind": "$data" }
        , { "$unwind": "$data.data" }
        , { "$unwind": "$data.data.data" }

        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data._id",
                        {
                            //x arbol
                            "productividad_total_arbol": "$data.data.productividad_total_arbol"

                            //x lote
                            , "productividad_total_arbol_min": "$data.productividad_total_arbol_min"
                            , "productividad_total_arbol_max": "$data.productividad_total_arbol_max"
                            , "productividad_total_lote": "$data.productividad_total_lote"

                            //x finca
                            , "productividad_total_lote_min": "$productividad_total_lote_min"
                            , "productividad_total_lote_max": "$productividad_total_lote_max"
                            , "productividad_total_finca": "$productividad_total_finca"
                        }
                    ]
                }
            }
        }



        // //========ETIQUETAS
        // , {
        //     "$addFields": {
        //         //arbol
        //         "productividad_total_arbol_min_txt": {
        //             "$cond": {
        //                 "if": { "$eq": ["$productividad_total_arbol", "$productividad_total_arbol_min"] },
        //                 "then": "Arbol Minimo Productivo",
        //                 "else": "no cumple"
        //             }
        //         }
        //         , "productividad_total_arbol_max_txt": {
        //             "$cond": {
        //                 "if": { "$eq": ["$productividad_total_arbol", "$productividad_total_arbol_max"] },
        //                 "then": "Arbol Maximo Productivo",
        //                 "else": "no cumple"
        //             }
        //         }
        //         //lote
        //         , "productividad_total_lote_min_txt": {
        //             "$cond": {
        //                 "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_min"] },
        //                 "then": "Lote Minimo Productivo",
        //                 "else": "no cumple"
        //             }
        //         }
        //         , "productividad_total_lote_max_txt": {
        //             "$cond": {
        //                 "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_max"] },
        //                 "then": "Lote Maximo Productivo",
        //                 "else": "no cumple"
        //             }
        //         }
        //
        //     }
        // }

        //========ETIQUETAS
       , {
           "$addFields": {
               //arbol
               "txt_productividad_arbol": {
                   "$cond": {
                       "if": { "$eq": ["$productividad_total_arbol", "$productividad_total_arbol_min"] },
                       "then": "Arbol Minimo Productivo",
                       "else": {
                           "$cond": {
                               "if": { "$eq": ["$productividad_total_arbol", "$productividad_total_arbol_max"] },
                               "then": "Arbol Maximo Productivo",
                               "else": "Rango intermedio"
                           }
                       }
                   }
               }
               //lote
               , "txt_productividad_lote": {
                   "$cond": {
                       "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_min"] },
                       "then": "Lote Minimo Productivo",
                       "else": {
                           "$cond": {
                               "if": { "$eq": ["$productividad_total_lote", "$productividad_total_lote_max"] },
                               "then": "Lote Maximo Productivo",
                               "else": "Rango intermedio"
                           }
                       }
                   }
               }
           }
       }



    ]
)
