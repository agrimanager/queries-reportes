db.form_registrodelluvia.aggregate(
    [
        {
            "$addFields": {
                "variable_cartografia": "$cartografia"
            }
        },

        { "$unwind": "$variable_cartografia.features" },

        {
            "$addFields": {
                "split_path_padres": { "$split": [{ "$trim": { "input": "$variable_cartografia.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_padres_oid": { "$map": { "input": "$split_path_padres", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "variable_cartografia_oid": [{ "$toObjectId": "$variable_cartografia.features._id" }]
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_padres_oid",
                        "$variable_cartografia_oid"
                    ]
                }
            }
        },


        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },

        {
            "$addFields": {
                "tiene_variable_cartografia": {
                    "$cond": {
                        "if": { "$eq": [{ "$size": "$split_path_oid" }, { "$size": "$objetos_del_cultivo" }] },
                        "then": "si",
                        "else": "no"
                    }
                }
            }
        },

        {
            "$addFields": {
                "objetos_del_cultivo": {
                    "$cond": {
                        "if": { "$eq": ["$tiene_variable_cartografia", "si"] },
                        "then": "$objetos_del_cultivo",
                        "else": {
                            "$concatArrays": [
                                "$objetos_del_cultivo",
                                ["$variable_cartografia.features"]
                            ]
                        }
                    }
                }
            }
        },

        {
            "$addFields": {
                "finca": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.type", "Farm"] }
                    }
                }
            }
        },
        {
            "$unwind": {
                "path": "$finca",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "finca._id",
                "foreignField": "_id",
                "as": "finca"
            }
        },
        { "$unwind": "$finca" },

        { "$addFields": { "finca": { "$ifNull": ["$finca.name", "no existe"] } } },



        {
            "$addFields": {
                "bloque": {
                    "$filter": {
                        "input": "$objetos_del_cultivo",
                        "as": "item_cartografia",
                        "cond": { "$eq": ["$$item_cartografia.properties.type", "blocks"] }
                    }
                }
            }
        },

        {
            "$unwind": {
                "path": "$bloque",
                "preserveNullAndEmptyArrays": true
            }
        },

        { "$addFields": { "bloque_id": { "$ifNull": [{ "$toString": "$bloque._id" }, ""] } } },
        { "$addFields": { "bloque": { "$ifNull": ["$bloque.properties.name", "no existe"] } } },

        {
            "$project": {
                "variable_cartografia": 0,
                "split_path_padres": 0,
                "split_path_padres_oid": 0,
                "variable_cartografia_oid": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "tiene_variable_cartografia": 0,
                "cartografia": 0,
                "Point": 0,
                "bloque_id": 0,
                "Formula": 0,
                "uid": 0,
                "uDate": 0,
                "capture": 0
            }
        },



        {
            "$addFields": {
                "meses_arr": [
                    "01-Enero",
                    "02-Febrero",
                    "03-Marzo",
                    "04-Abril",
                    "05-Mayo",
                    "06-Junio",
                    "07-Julio",
                    "08-Agosto",
                    "09-Septiembre",
                    "10-Octubre",
                    "11-Noviembre",
                    "12-Diciembre"
                ],
                "Precipitacion mm": { "$toDouble": "$Precipitacion mm" }
            }
        },

        {
            "$addFields": {
                "mes": {
                    "$arrayElemAt": [
                        "$meses_arr",
                        { "$subtract": [{ "$month": "$Fecha" }, 1] }
                    ]
                },
                "dia": { "$dayOfMonth": "$Fecha" },
                "año": { "$year": "$Fecha" }
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "fecha": "$Fecha",
                    "año": "$año",
                    "mes": "$mes",
                    "precipitacion": "$Precipitacion mm"
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque",
                    "año": "$_id.año",
                    "mes": "$_id.mes"
                },
                "precipitaciones_x_mes": { "$sum": "$_id.precipitacion" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "precipitaciones_x_mes_mm": "$precipitaciones_x_mes" }
                    ]
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "finca": "$finca",
                    "bloque": "$bloque",
                    "mes": "$mes",
                    "precipitacion": "$precipitaciones_x_mes_mm"
                },
                "data": { "$push": "$$ROOT" }
            }
        },

        {
            "$group": {
                "_id": {
                    "finca": "$_id.finca",
                    "bloque": "$_id.bloque"
                },
                "precipitaciones_totales": { "$sum": "$_id.precipitacion" },
                "data": { "$push": "$$ROOT" }
            }
        },

        { "$unwind": "$data" },
        { "$unwind": "$data.data" },

        {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data.data",
                        { "precipitaciones_totales_mm": "$precipitaciones_totales" }
                    ]
                }
            }
        },



        {
            "$addFields": {
                "precipitacion por mes mm": "$precipitaciones_x_mes_mm",
                "total precipitacion mm": "$precipitaciones_totales_mm"
            }
        },

        {
            "$project": {
                "meses_arr": 0,
                "año": 0,
                "precipitaciones_x_mes_mm": 0,
                "precipitaciones_totales_mm": 0
            }
        }
    ]
)
