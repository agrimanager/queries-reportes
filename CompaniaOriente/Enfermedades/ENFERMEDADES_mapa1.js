db.form_censodeenfermedades.aggregate(
    [
        {
            "$addFields": {
                "split_path": { "$split": [{ "$trim": { "input": "$Arbol.path", "chars": "," } }, ","] }
            }
        },
        {
            "$addFields": {
                "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
            }
        },
        {
            "$addFields": {
                "features_oid": { "$map": { "input": "$Arbol.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
            }
        },
        {
            "$addFields": {
                "split_path_oid": {
                    "$concatArrays": [
                        "$split_path_oid",
                        "$features_oid"
                    ]
                }
            }
        },
        {
            "$lookup": {
                "from": "cartography",
                "localField": "split_path_oid",
                "foreignField": "_id",
                "as": "objetos_del_cultivo"
            }
        },
        {
            "$addFields": {
                "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
                "bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
                "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] },
                "linea": { "$arrayElemAt": ["$objetos_del_cultivo", 3] },
                "arbol": { "$arrayElemAt": ["$objetos_del_cultivo", 4] }


            }
        },

        //---Cartography = lote (geometria a dibujar)
        {
            "$addFields": {
                "Cartography": "$lote"
            }
        },

        {
            "$addFields": {
                "bloque": "$bloque.properties.name",
                "Numero Hectareas": "$lote.properties.custom.Numero de hectareas.value",
                "Numero de plantas": "$lote.properties.custom.Numero de plantas.value",
                "lote": "$lote.properties.name",
                "linea": "$linea.properties.name",
                "arbol": "$arbol.properties.name"


            }
        },
        {
            "$lookup": {
                "from": "farms",
                "localField": "Finca._id",
                "foreignField": "_id",
                "as": "Finca"
            }
        },
        {
            "$addFields": {
                "Finca": "$Finca.name"
            }
        },
        { "$unwind": "$Finca" },
        {
            "$project": {
                "split_path": 0,
                "split_path_oid": 0,
                "objetos_del_cultivo": 0,
                "features_oid": 0
            }
        },

        { "$unwind": "$Enfermedad" },


        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Enfermedad": "$Enfermedad",
                    "tiene_Erradicar": "$Estado"

                }

                , "data": {
                    "$push": "$$ROOT"
                }


                , "Cantidad_Erradicar": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Estado", "Erradicar"] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }

            }
        },


        { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "Cantidad_Erradicar": "$Cantidad_Erradicar"
                        }
                    ]
                }
            }
        },





        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Enfermedad": "$Enfermedad",
                    "tiene_Erradicar Urgente": "$Erradicar Urgente"

                }

                , "data": {
                    "$push": "$$ROOT"
                }


                , "Cantidad_Erradicar_Urgente": {
                    "$sum": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": { "$eq": ["$Estado", "Erradicar Urgente"] },
                                    "then": 1
                                }
                            ],
                            "default": 0
                        }
                    }
                }

            }
        },


        { "$unwind": "$data" }


        , {
            "$replaceRoot": {
                "newRoot": {
                    "$mergeObjects": [
                        "$data",
                        {
                            "Cantidad_Erradicar_Urgente": "$Cantidad_Erradicar_Urgente"
                        }
                    ]
                }
            }
        },

        {
            "$addFields": {
                "TotalA_Radical": {
                    "$sum": ["$Cantidad_Erradicar", "$Cantidad_Erradicar_Urgente"]
                }
            }

        },



        {
            "$addFields": {
                "pct_incidencia": {
                    "$multiply": [
                        { "$divide": ["$TotalA_Radical", "$Numero de plantas"] }
                        , 100
                    ]
                }
            }
        },


        {
            "$group": {
                "_id": {
                    "lote": "$lote",
                    "Enfermedad": "$Enfermedad"

                    , "idform": "$idform"
                }

                , "data": {
                    "$push": "$$ROOT"
                }


                , "pct_incidencia": {
                    "$sum": "$pct_incidencia"
                }


            }
        },

        {
            "$addFields": {
                "color": {
                    "$cond": {
                        "if": { "$eq": ["$pct_incidencia", 0] },
                        "then": "#2d572c",
                        "else": {
                            "$cond": {
                                "if": { "$and": [{ "$gt": ["$pct_incidencia", 0.1] }, { "$lte": ["$pct_incidencia", 1] }] },
                                "then": "##FFFF00",
                                "else": {
                                    "$cond": {
                                        "if": { "$and": [{ "$gt": ["$pct_incidencia", 1] }, { "$lte": ["$pct_incidencia", 2] }] },
                                        "then": "#ff8000",
                                        "else": "#ff0000"
                                    }
                                }
                            }
                        }
                    }
                },



                "rango": {
                    "$cond": {
                        "if": {
                            "$eq": ["$pct_incidencia", 0]
                        },
                        "then": "A-[0]",
                        "else": {
                            "$cond": {
                                "if": {
                                    "$and": [{ "$gt": ["$pct_incidencia", 0.1] }, { "$lte": ["$pct_incidencia", 1] }]
                                },
                                "then": "B-(0.1-1)",
                                "else": {
                                    "$cond": {
                                        "if": {
                                            "$and": [{ "$gt": ["$pct_incidencia", 1] }, { "$lte": ["$pct_incidencia", 2] }]

                                        },
                                        "then": "C-[1-2)",
                                        "else": "D-[>=2]"

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        , {
            "$project": {
                "_id": {
                    //"$arrayElemAt": ["$data.elemnq", { "$subtract": [{ "$size": "$data.elemnq" }, 1] }]
                    "$arrayElemAt": ["$data._id", { "$subtract": [{ "$size": "$data._id" }, 1] }]
                },
                "idform": "$_id.idform",
                "type": "Feature",
                "properties": {
                    "Lote": "$_id.lote",
                    "Enfermedad": "$_id.Enfermedad",
                    "rango": "$rango",
                    "color": "$color"
                },
                "geometry": {
                    "$arrayElemAt": ["$data.Cartography.geometry", { "$subtract": [{ "$size": "$data.Cartography.geometry" }, 1] }]
                }
            }
        },













    ]

)