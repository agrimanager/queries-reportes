[


    { "$addFields": { "anio_filtro": { "$year": "$Fecha de corte" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    {
        "$sort": {
            "Fecha de corte": -1
        }
    },
    {
        "$addFields": { "Cartography": "$Lote", "elemnq": "$_id" }
    },
    {
        "$unwind": "$Cartography.features"
    }
    , {
        "$group": {
            "_id": {
                "nombre_lote": "$Cartography.features.properties.name",
                "today": "$today",
                "idform": "$idform"
            },
            "data": {
                "$push": "$$ROOT"
            }
        }
    }

    , {
        "$addFields": {
            "data": { "$arrayElemAt": ["$data", 0] }
        }
    },

    {
        "$addFields": {
            "dias de ciclo": {
                "$floor": {
                    "$divide": [{ "$subtract": ["$_id.today", "$data.Fecha de corte"] }, 86400000]
                }
            }
        }
    },


    {
        "$addFields": {
            "color": {
                "$cond": {
                    "if": {
                        "$eq": ["$dias de ciclo", -1]
                    },
                    "then": "#666666",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "#2570A9",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                    },
                                    "then": "#9ACD32",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "#FFFD1F",
                                            "else": "#E84C3F"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "rango": {
                "$cond": {
                    "if": {
                        "$eq": ["$dias de ciclo", -1]
                    },
                    "then": "A-En proceso",
                    "else": {
                        "$cond": {
                            "if": {
                                "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                            },
                            "then": "B-Ciclo cosecha de 0 a 9 dias",
                            "else": {
                                "$cond": {
                                    "if": {
                                        "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                    },
                                    "then": "C-Ciclo cosecha de 10 a 12 dias",
                                    "else": {
                                        "$cond": {
                                            "if": {
                                                "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                            },
                                            "then": "D-Ciclo cosecha de 13 a 15 dias",
                                            "else": "E-Ciclo cosecha >= 16 dias"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    {
        "$project": {
            "_id": "$data.elemnq",
            "idform": "$_id.idform",
            "type": "Feature",
            "properties": {
                "Lote": "$_id.nombre_lote",
                "Rango": "$rango",
                "Dias Ciclo": {
                    "$cond": {
                        "if": { "$eq": ["$dias de ciclo", -1] },
                        "then": "-1",
                        "else": {
                            "$concat": [
                                { "$toString": "$dias de ciclo" },
                                " dias"
                            ]
                        }
                    }
                },
                "color": "$color"
            },
            "geometry": "$data.Cartography.features.geometry"
        }
    }
]