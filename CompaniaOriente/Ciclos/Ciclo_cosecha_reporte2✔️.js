[

      { "$addFields": { "anio_filtro": { "$year": "$Fecha de corte" } } },
      { "$match": { "anio_filtro": { "$gt": 2000 } } },
      { "$match": { "anio_filtro": { "$lt": 3000 } } },


      {
          "$sort": {
              "Fecha de corte": -1
          }
      },


      {
          "$addFields": {
              "num_anio": { "$year": { "date": "$Fecha de corte" } },
              "num_mes": { "$month": { "date": "$Fecha de corte" } },
              "num_dia_mes": { "$dayOfMonth": { "date": "$Fecha de corte" } },
              "num_semana": { "$week": { "date": "$Fecha de corte" } },
              "num_dia_semana": { "$dayOfWeek": { "date": "$Fecha de corte" } }
          }
      }

      , {
          "$addFields": {
              "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de corte" } }

              , "Mes_Txt": {
                  "$switch": {
                      "branches": [
                          { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                          { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                          { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                          { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                          { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                          { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                          { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                          { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                          { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                          { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                          { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                          { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                      ],
                      "default": "Mes desconocido"
                  }
              }


              , "Dia_Txt": {
                  "$switch": {
                      "branches": [
                          { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                          { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                          { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                          { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                          { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                          { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                          { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                      ],
                      "default": "dia de la semana desconocido"
                  }
              }
          }
      },


      { "$project": { "num_dia_semana": 0 } },


      { "$match": { "Lote.path": { "$ne": "" } } },


      {
          "$addFields": {
              "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
          }
      },
      {
          "$addFields": {
              "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
          }
      },
      {
          "$addFields": {
              "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
          }
      },
      {
          "$addFields": {
              "split_path_oid": {
                  "$concatArrays": [
                      "$split_path_oid",
                      "$features_oid"
                  ]
              }
          }
      },

      {
          "$lookup": {
              "from": "cartography",
              "localField": "split_path_oid",
              "foreignField": "_id",
              "as": "objetos_del_cultivo"
          }
      },

      {
          "$addFields": {
              "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
              "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
              "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
          }
      },

      {
          "$addFields": {
              "Bloque": "$Bloque.properties.name",

              "nombre_lote": "$lote.properties.custom.Nombre.value",
              "ha_lote": "$lote.properties.custom.Numero de hectareas.value",
              "palnatas_x_lote": "$lote.properties.custom.Numero de plantas.value",
              "lote": "$lote.properties.name"
          }
      },

      {
          "$lookup": {
              "from": "farms",
              "localField": "Finca._id",
              "foreignField": "_id",
              "as": "Finca"
          }
      },

      { "$unwind": "$Finca" }


      , {
          "$addFields": {
              "Finca": "$Finca.name"
          }
      }

      , {
          "$project": {
              "split_path": 0,
              "split_path_oid": 0,
              "objetos_del_cultivo": 0,
              "features_oid": 0,
              "Formula": 0


              , "Lote": 0
              , "Point": 0
          }
      }




      , {
          "$lookup": {
              "from": "form_pesospromediolote",
              "as": "info_lotes",
              "let": {
                  "nombre_lote": "$nombre_lote"
              },
              "pipeline": [
                  {
                      "$match": {
                          "$expr": {
                              "$and": [
                                  { "$eq": ["$$nombre_lote", "$Lote"] }
                              ]
                          }
                      }
                  },
                  {
                      "$sort": {
                          "rgDate": -1
                      }
                  },
                  {
                      "$limit": 1
                  }
              ]
          }
      },
      {
          "$unwind": {
              "path": "$info_lotes",
              "preserveNullAndEmptyArrays": true
          }
      },
      {
          "$addFields": {
              "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de siembra", 0] },
              "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso promedio Kg", 0] }
          }
      }

      , {
          "$project": {
              "info_lotes": 0
          }
      }






      , {
          "$group": {
              "_id": {
                  "lote": "$lote",
                  "today": "$today"
              },
              "data": {
                  "$push": "$$ROOT"
              }
          }
      }


      , {
          "$addFields": {
              "data": { "$arrayElemAt": ["$data", 0] }
          }
      },




      {
          "$addFields": {
              "dias de ciclo": {
                  "$floor": {
                      "$divide": [{ "$subtract": ["$_id.today", "$data.Fecha de corte"] }, 86400000]
                  }
              }
          }
      },


      {
          "$addFields": {
              "rango": {
                  "$cond": {
                      "if": {
                          "$eq": ["$dias de ciclo", -1]
                      },
                      "then": "A-En proceso",
                      "else": {
                          "$cond": {
                              "if": {
                                  "$and": [{ "$gte": ["$dias de ciclo", 0] }, { "$lte": ["$dias de ciclo", 9] }]
                              },
                              "then": "B-Ciclo cosecha de 0 a 9 dias",
                              "else": {
                                  "$cond": {
                                      "if": {
                                          "$and": [{ "$gte": ["$dias de ciclo", 10] }, { "$lte": ["$dias de ciclo", 12] }]

                                      },
                                      "then": "C-Ciclo cosecha de 10 a 12 dias",
                                      "else": {
                                          "$cond": {
                                              "if": {
                                                  "$and": [{ "$gte": ["$dias de ciclo", 13] }, { "$lte": ["$dias de ciclo", 15] }]
                                              },
                                              "then": "D-Ciclo cosecha de 13 a 15 dias",
                                              "else": "E-Ciclo cosecha >= 16 dias"
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
      },

      {
          "$replaceRoot": {
              "newRoot": {
                  "$mergeObjects": [
                      "$data",
                      {
                          "dias_ciclo": "$dias de ciclo",
                          "rango": "$rango"
                      }
                  ]
              }
          }
      }



      , {
          "$addFields": {
              "Fecha_Ultimo_Corte": "$Fecha_Txt",
              "Fecha_Actual_Hoy": { "$dateToString": { "format": "%Y-%m-%d", "date": "$today" } }
          }
      }




      , {
          "$project": {
              "_id": 0,
              "Observaciones": 0,
              "Racimos (#)": 0,
              "Sacos de pepa (#)": 0,
              "Jefe de cuadrilla": 0,
              "uid": 0,
              "supervisor": 0,
              "rgDate": 0,
              "uDate": 0,
              "capture": 0,
              "today": 0,
              "anio_filtro": 0,
              "Fecha de corte": 0,
              "num_mes": 0,
              "Fecha_Txt": 0

          }
      }


  ]