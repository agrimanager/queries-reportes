[


    { "$addFields": { "anio_filtro": { "$year": "$Fecha de corte" } } },
    { "$match": { "anio_filtro": { "$gt": 2000 } } },
    { "$match": { "anio_filtro": { "$lt": 3000 } } },


    {
        "$addFields": {
            "num_anio": { "$year": { "date": "$Fecha de corte" } },
            "num_mes": { "$month": { "date": "$Fecha de corte" } },
            "num_dia_mes": { "$dayOfMonth": { "date": "$Fecha de corte" } },
            "num_semana": { "$week": { "date": "$Fecha de corte" } },
            "num_dia_semana": { "$dayOfWeek": { "date": "$Fecha de corte" } }
        }
    }

    , {
        "$addFields": {
            "Fecha_Txt": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de corte" } }

            , "Mes_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_mes", 1] }, "then": "01-Enero" },
                        { "case": { "$eq": ["$num_mes", 2] }, "then": "02-Febrero" },
                        { "case": { "$eq": ["$num_mes", 3] }, "then": "03-Marzo" },
                        { "case": { "$eq": ["$num_mes", 4] }, "then": "04-Abril" },
                        { "case": { "$eq": ["$num_mes", 5] }, "then": "05-Mayo" },
                        { "case": { "$eq": ["$num_mes", 6] }, "then": "06-Junio" },
                        { "case": { "$eq": ["$num_mes", 7] }, "then": "07-Julio" },
                        { "case": { "$eq": ["$num_mes", 8] }, "then": "08-Agosto" },
                        { "case": { "$eq": ["$num_mes", 9] }, "then": "09-Septiembre" },
                        { "case": { "$eq": ["$num_mes", 10] }, "then": "10-Octubre" },
                        { "case": { "$eq": ["$num_mes", 11] }, "then": "11-Noviembre" },
                        { "case": { "$eq": ["$num_mes", 12] }, "then": "12-Diciembre" }
                    ],
                    "default": "Mes desconocido"
                }
            }


            , "Dia_Txt": {
                "$switch": {
                    "branches": [
                        { "case": { "$eq": ["$num_dia_semana", 2] }, "then": "01-Lunes" },
                        { "case": { "$eq": ["$num_dia_semana", 3] }, "then": "02-Martes" },
                        { "case": { "$eq": ["$num_dia_semana", 4] }, "then": "03-Miercoles" },
                        { "case": { "$eq": ["$num_dia_semana", 5] }, "then": "04-Jueves" },
                        { "case": { "$eq": ["$num_dia_semana", 6] }, "then": "05-Viernes" },
                        { "case": { "$eq": ["$num_dia_semana", 7] }, "then": "06-sabado" },
                        { "case": { "$eq": ["$num_dia_semana", 1] }, "then": "07-Domingo" }
                    ],
                    "default": "dia de la semana desconocido"
                }
            }
        }
    },


    { "$project": { "num_dia_semana": 0 } },





    { "$match": { "Lote.path": { "$ne": "" } } },


    {
        "$addFields": {
            "split_path": { "$split": [{ "$trim": { "input": "$Lote.path", "chars": "," } }, ","] }
        }
    },
    {
        "$addFields": {
            "split_path_oid": { "$map": { "input": "$split_path", "as": "strid", "in": { "$toObjectId": "$$strid" } } }
        }
    },
    {
        "$addFields": {
            "features_oid": { "$map": { "input": "$Lote.features", "as": "item", "in": { "$toObjectId": "$$item._id" } } }
        }
    },
    {
        "$addFields": {
            "split_path_oid": {
                "$concatArrays": [
                    "$split_path_oid",
                    "$features_oid"
                ]
            }
        }
    },

    {
        "$lookup": {
            "from": "cartography",
            "localField": "split_path_oid",
            "foreignField": "_id",
            "as": "objetos_del_cultivo"
        }
    },

    {
        "$addFields": {
            "Finca": { "$arrayElemAt": ["$objetos_del_cultivo", 0] },
            "Bloque": { "$arrayElemAt": ["$objetos_del_cultivo", 1] },
            "lote": { "$arrayElemAt": ["$objetos_del_cultivo", 2] }
        }
    },

    {
        "$addFields": {
            "Bloque": "$Bloque.properties.name",
            "nombre_lote": "$lote.properties.custom.Nombre.value",
            "lote": "$lote.properties.name"
        }
    },

    {
        "$lookup": {
            "from": "farms",
            "localField": "Finca._id",
            "foreignField": "_id",
            "as": "Finca"
        }
    },

    { "$unwind": "$Finca" }


    , {
        "$addFields": {
            "Finca": "$Finca.name"
        }
    }

    , {
        "$project": {
            "split_path": 0,
            "split_path_oid": 0,
            "objetos_del_cultivo": 0,
            "features_oid": 0,
            "Formula": 0


            , "Lote": 0
            , "Point": 0
        }
    }




    , {
        "$lookup": {
            "from": "form_pesospromediolote",
            "as": "info_lotes",
            "let": {
                "nombre_lote": "$nombre_lote"
            },
            "pipeline": [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$$nombre_lote", "$Lote"] }
                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": -1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$info_lotes",
            "preserveNullAndEmptyArrays": true
        }
    },
    {
        "$addFields": {
            "Fecha_Siembra": { "$ifNull": ["$info_lotes.Año de siembra", 0] },
            "Peso promedio lote": { "$ifNull": ["$info_lotes.Peso promedio Kg", 0] }
        }
    }

    , {
        "$project": {
            "info_lotes": 0
        }
    }


    , {
        "$addFields": {
            "total_racimos_alzados": "$Racimos (#)"
        }
    }



    , {
        "$addFields":
        {
            "Peso aproximado Alzados": {
                "$multiply": [
                    { "$ifNull": [{ "$toDouble": "$total_racimos_alzados" }, 0] },
                    { "$ifNull": [{ "$toDouble": "$Peso promedio lote" }, 0] }
                ]
            }
        }
    }





    , {
        "$lookup": {
            "from": "form_despacho",
            "as": "despacho_cosecha",
            "let": {
                "nombre_lote": "$nombre_lote",
                "fecha": "$Fecha de corte"
            },
            "pipeline": [


                { "$unwind": "$LOTE" },

                {
                    "$addFields": {
                        "LOTE": {
                            "$cond": {

                                "if": { "$eq": [{ "$substr": ["$LOTE", 0, 1] }, "\""] },

                                "then": { "$substr": ["$LOTE", 1, { "$subtract": [{ "$strLenCP": "$LOTE" }, 2] }] },
                                "else": "$LOTE"
                            }
                        }
                    }
                },


                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                { "$eq": ["$LOTE", "$$nombre_lote"] },

                                {
                                    "$gte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha incio de corte" } } }
                                    ]
                                },

                                {
                                    "$lte": [
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$$fecha" } } }
                                        ,
                                        { "$toDate": { "$dateToString": { "format": "%Y-%m-%d", "date": "$Fecha de fin de corte" } } }
                                    ]
                                }


                            ]
                        }
                    }
                },
                {
                    "$sort": {
                        "rgDate": 1
                    }
                },
                {
                    "$limit": 1
                }
            ]
        }
    },
    {
        "$unwind": {
            "path": "$despacho_cosecha",
            "preserveNullAndEmptyArrays": true
        }
    },


    {
        "$addFields":
        {
            "Total Peso despachados (ticket)": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Peso de la extractora Kg" }, -1] },
            "Numero de ticket": { "$ifNull": [{ "$toDouble": "$despacho_cosecha.Numero del ticket" }, -1] },

            "Extractora": { "$ifNull": ["$despacho_cosecha.Extractora", "--sin despacho--"] },
            "Placa": { "$ifNull": ["$despacho_cosecha.Placa", "--sin despacho--"] }
        }
    }

    , {
        "$project": {
            "despacho_cosecha": 0
        }
    }




    , {
        "$group": {
            "_id": {
                "ticket": "$Numero de ticket",
                "lote": "$lote"
            },
            "data": {
                "$push": "$$ROOT"
            },

            "total_racimos_alzados_viaje_lote": {
                "$sum": "$total_racimos_alzados"
            },

            "total_peso_aproximado_racimos_alzados_viaje_lote": {
                "$sum": "$Peso aproximado Alzados"
            }
        }
    }
    , {
        "$group": {
            "_id": {
                "ticket": "$_id.ticket"

            },
            "data": {
                "$push": "$$ROOT"
            },

            "total_peso_aproximado_racimos_alzados_viaje": {
                "$sum": "$total_peso_aproximado_racimos_alzados_viaje_lote"
            },
            "total_racimos_alzados_viaje": {
                "$sum": "$total_racimos_alzados_viaje_lote"
            }
        }
    }

    , { "$unwind": "$data" }

    , {
        "$addFields":
        {
            "total_peso_aproximado_racimos_alzados_viaje_lote": "$data.total_peso_aproximado_racimos_alzados_viaje_lote",
            "total_racimos_alzados_viaje_lote": "$data.total_racimos_alzados_viaje_lote"
        }
    },


    {
        "$unwind": "$data.data"
    },
    {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$data.data",
                    {

                        "total_racimos_alzados_viaje_lote": "$total_racimos_alzados_viaje_lote",


                        "total_peso_aproximado_racimos_alzados_viaje_lote": "$total_peso_aproximado_racimos_alzados_viaje_lote",


                        "total_peso_aproximado_racimos_alzados_viaje": "$total_peso_aproximado_racimos_alzados_viaje"




                        , "(%) Alzados x lote": {
                            "$cond": {
                                "if": {
                                    "$eq": ["$total_peso_aproximado_racimos_alzados_viaje", 0]
                                },
                                "then": 0,
                                "else": {
                                    "$divide": ["$total_peso_aproximado_racimos_alzados_viaje_lote", "$total_peso_aproximado_racimos_alzados_viaje"]
                                }
                            }
                        }


                        , "Total Peso ticket": "$data.data.Total Peso despachados (ticket)"
                    }
                ]
            }
        }
    }

    , { "$project": { "Total Peso despachados (ticket)": 0 } }

    , {
        "$addFields":
        {
            "total_peso_REAL_racimos_alzados_viaje_lote": {
                "$multiply": ["$Total Peso ticket", "$(%) Alzados x lote"]
            }
        }
    }


    , {
        "$addFields":
        {
            "Peso REAL lote": {
                "$divide": ["$total_peso_REAL_racimos_alzados_viaje_lote", "$total_racimos_alzados_viaje_lote"]
            }
        }
    }



    , {
        "$addFields":{
            "Kg REAL Alzados": {
                "$multiply": ["$total_racimos_alzados", "$Peso REAL lote"]
            }
        }
    }

    , {
        "$addFields":{
            "Ton REAL Alzados": {
                "$divide": ["$Kg REAL Alzados", 1000]
            }
        }
    }


]