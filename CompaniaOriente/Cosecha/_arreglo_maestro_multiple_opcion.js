db.form_despacho.aggregate(

    { "$unwind": "$LOTE" }

    , {
        "$addFields": {
            "LOTE2": {
                "$cond": {
                    "if": {
                        "$eq": [{ "$substr": ["$LOTE", 0, 1] }, "\""]
                    },
                    "then": "si tiene comillas",
                    "else": "no tiene comillas"
                }
            }
        }
    }

    , {
        "$addFields": {
            "LOTE3": {
                "$cond": {
                    //---si comieza con comillas dobles
                    "if": { "$eq": [{ "$substr": ["$LOTE", 0, 1] }, "\""] },
                    //---obtener texto desde posicion i+2 hasta n-2
                    "then": {"$substr": [ "$LOTE",1, { "$subtract": [{ "$strLenCP": "$LOTE" },2]}]},
                    "else": "$LOTE"
                }
            }
        }
    }

)