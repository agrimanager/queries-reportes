var data = db.form_mezclaypolen.aggregate(

    {
        $addFields: {
            "Mezcla gastada ANA y silicato gris": { $ifNull: ["$Mezcla gastada ANA y silicato gris", 0] },
            "Mezcla gastada Polen y silicato gris": { $ifNull: ["$Mezcla gastada Polen y silicato gris", 0] },
        }
    },


    {
        $addFields: {
            "Mezcla Asignada ANA y Silicato grs": { $toString: "$Mezcla Asignada ANA y Silicato grs" },
            "Mezcla gastada ANA y silicato gris": { $toString: "$Mezcla gastada ANA y silicato gris" },
            "Mezcla Sobrante ANA y Silicato grs": { $toString: "$Mezcla Sobrante ANA y Silicato grs" },

            "Mezcla Asignada Polen y Silicato grs": { $toString: "$Mezcla Asignada Polen y Silicato grs" },
            "Mezcla gastada Polen y silicato gris": { $toString: "$Mezcla gastada Polen y silicato gris" },
            "Mezcla Sobrante Polen y Silicato grs": { $toString: "$Mezcla Sobrante Polen y Silicato grs" },
        }
    },

)

// data


data.forEach(item => {

    //console.log(item["Mezcla Asignada ANA y Silicato grs"])

    db.form_mezclaypolen.update(
        {
            _id: item._id
        }, {
        $set: {

            "ANA asignada": item["Mezcla Asignada ANA y Silicato grs"],
            "Talco asignada": "0",
            "Polen asignada": item["Mezcla Asignada Polen y Silicato grs"],

            "ANA gastada": item["Mezcla gastada ANA y silicato gris"],
            "Talco gastada": "0",
            "Polen gastada": item["Mezcla gastada Polen y silicato gris"],

            "ANA sobrante": item["Mezcla Sobrante ANA y Silicato grs"],
            "Talco sobrante": "0",
            "Polen sobrante": item["Mezcla Sobrante Polen y Silicato grs"],

            "Mezcla asignada": "",
            "Mezcla gastada": "",
            "Mezcla sobrante": "",

        }
    }

    )

})





db.form_mezclaypolen.update(
    {}
    ,
    {
        $unset: {
            // "properties.custom.Mes de siembra": 1
            "Mezcla Asignada ANA y Silicato grs": 1,
            "Mezcla gastada ANA y silicato gris": 1,
            "Mezcla Sobrante ANA y Silicato grs": 1,
            "Mezcla Asignada Polen y Silicato grs": 1,
            "Mezcla gastada Polen y silicato gris": 1,
            "Mezcla Sobrante Polen y Silicato grs": 1,
        }

    }, false, true
);

/*
/---old
       "Mezcla Asignada ANA y Silicato grs" : 0,
 "Mezcla gastada ANA y silicato gris" : 0,---
 "Mezcla Sobrante ANA y Silicato grs" : 0,
 "Mezcla Asignada Polen y Silicato grs" : 0,
 "Mezcla gastada Polen y silicato gris" : 0,---
 "Mezcla Sobrante Polen y Silicato grs" : 0,
/---new
 "ANA asignada" : "1",
 "Talco asignada" : "2",
 "Polen asignada" : "3",
 "ANA gastada" : "4",
 "Talco gastada" : "5",
 "Polen gastada" : "6",
 "ANA sobrante" : "7",
 "Talco sobrante" : "8",
 "Polen sobrante" : "9",
 "Mezcla asignada" : "",
 "Mezcla gastada" : "",
 "Mezcla sobrante" : "",


 */
